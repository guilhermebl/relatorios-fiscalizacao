package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.VerticalAlign;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class Formatacao {

    private boolean bold = false;
    private int fontSize = 14;
    private String fontFamily = "Arial";
    private boolean underline = false;
    private boolean italic = false;
    private boolean capitalized = false;
    private ParagraphAlignment alignment = ParagraphAlignment.LEFT;
    private String color = "000000";
    private String highLightColor;
    private boolean tab = false;
    private boolean sobrescrito = false;

    public Formatacao() {

    }

    public Formatacao(int size) {
        this.fontSize = size;
    }




    public void formatRun(XWPFRun run) {
        run.setBold(isBold());
        run.setItalic(isItalic());
        run.setFontFamily(getFontFamily());
        run.setFontSize(getFontSize());
        if(isUnderline())
            run.setUnderline(UnderlinePatterns.SINGLE);
        run.setCapitalized(isCapitalized());
        run.setColor(getColor());
        if(highLightColor != null)
            run.setTextHighlightColor(getHighLightColor());
        if(tab)
            run.addTab();
        if(sobrescrito)
            run.setSubscript(VerticalAlign.SUPERSCRIPT);
    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public Formatacao bold() { this.bold = true; return this;}

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public boolean isUnderline() {
        return underline;
    }

    public void setUnderline(boolean underline) {
        this.underline = underline;
    }


    public boolean isItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
    }

    public Formatacao italic() { this.italic = true; return this;}

    public boolean isCapitalized() {
        return capitalized;
    }

    public void setCapitalized(boolean capitalized) {
        this.capitalized = capitalized;
    }

    public ParagraphAlignment getAlignment() {
        return alignment;
    }

    public void setAlignment(ParagraphAlignment alignment) {
        this.alignment = alignment;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHighLightColor() {
        return highLightColor;
    }

    public void setHighLightColor(String highLightColor) {
        this.highLightColor = highLightColor;
    }

    public boolean isTab() {
        return tab;
    }

    public void setTab(boolean tab) {
        this.tab = tab;
    }

    public boolean isSobrescrito() {
        return sobrescrito;
    }

    public void setSobrescrito(boolean sobrescrito) {
        this.sobrescrito = sobrescrito;
    }

    public Formatacao justify() {
        this.alignment = ParagraphAlignment.BOTH;
        return this;
    }
    public Formatacao center() {
        this.alignment = ParagraphAlignment.CENTER;
        return this;
    }

    public Formatacao size(int size) {
        this.fontSize = size;
        return this;
    }

    public Formatacao red() {
        this.color = "ff0000";
        return this;
    }

    public Formatacao blue() {
        this.color = "0000ff";
        return this;
    }

    public Formatacao bgRed() {
        this.highLightColor = "red";
        return this;
    }

    public Formatacao bgYellow() {
        this.highLightColor = "yellow";
        return this;
    }

    public Formatacao bgCcyan() {
        this.highLightColor = "cyan";
        return this;
    }

    public Formatacao bgBlue() {
        this.highLightColor = "blue";
        return this;
    }

    public Formatacao bgGreen() {
        this.highLightColor = "green";
        return this;
    }

    public Formatacao bgGray() {
        this.highLightColor = "lightGray";
        return this;
    }

    public Formatacao arial() {
        this.fontFamily = "Arial";
        return this;
    }

    public Formatacao arial(int size) {
        this.fontFamily = "Arial";
        this.fontSize = size;
        return this;
    }

    public Formatacao underline() {
        this.underline = true;
        return this;
    }
}