package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ApontamentoFOKey implements Serializable {

    @Column(name = "operacao_id")
    private Integer operacaoId;

    @Column(name = "objf_id")
    private Integer objetoDeFiscalizacaoId;

    @Column(name = "municipio_codibge")
    private Integer codigoIBGE;

    @Column(name = "apontamento_id")
    private Integer apontamentoId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApontamentoFOKey)) return false;
        ApontamentoFOKey that = (ApontamentoFOKey) o;
        return Objects.equals(operacaoId, that.operacaoId) &&
                Objects.equals(objetoDeFiscalizacaoId, that.objetoDeFiscalizacaoId) &&
                Objects.equals(codigoIBGE, that.codigoIBGE) &&
                Objects.equals(apontamentoId, that.apontamentoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(operacaoId, objetoDeFiscalizacaoId, codigoIBGE, apontamentoId);
    }
}

