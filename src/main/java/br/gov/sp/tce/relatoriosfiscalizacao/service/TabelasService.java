package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasParecer;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.repository.TabelasRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.repository.TcespBiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class TabelasService {

    @Autowired
    private TabelasRepository tabelasRepository;

    @Autowired
    private TcespBiRepository tcespBiRepository;

    public TabelasProtocolo getTabelasProtocoloPrefeituraByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        return tabelasRepository.getTabelasProtocoloPrefeituraByCodigoIbge(codigoIBGE, exercicio);
    }
    public TabelasProtocolo getTabelasProtocoloCamaraByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        return tabelasRepository.getTabelasProtocoloCamaraByCodigoIbge(codigoIBGE, exercicio);
    }

    public ConjuntoPareceresCertidao getConjuntoPareceresyCodigoIbge(Integer codigoIBGE) {
        List<TabelasParecer> pareceres = tabelasRepository.getPareceresTransitoEmJulgadoByCodigoIbge(codigoIBGE);
        TabelasParecer ultimoExercicio = null;
        int anoExercicioEmCurso = LocalDateTime.now().getYear();
        List<TabelasParecer> exerciciosNaoAnalisados = new ArrayList<>();
        ConjuntoPareceresCertidao conjunto = new ConjuntoPareceresCertidao();

        for(TabelasParecer p : pareceres ) {
            if(anoExercicioEmCurso == p.getExercicio())
                conjunto.setExercicioEmCurso(p);
            if(p.getPublicacaoDO() != null && ultimoExercicio == null){
                ultimoExercicio = p;
                conjunto.setUltimoExercicio(p);
            }
            if(p.getPublicacaoDO() == null && p.getExercicio() < anoExercicioEmCurso && ultimoExercicio == null) {
                exerciciosNaoAnalisados.add(p);
            }
        }

        Collections.reverse(exerciciosNaoAnalisados);
        conjunto.setExerciciosNaoAnalisados(exerciciosNaoAnalisados);

        return conjunto;
    }

}
