package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;


import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespSaude;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespEntidadeRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public AudespEntidade getEntidade(Integer codigoIBGE, Integer exercicio) {
        Query query =  entityManager.createNativeQuery("select m.municipio_id, e.entidade_id, " +
                "m.ds_municipio, p.ds_poder, e.nome_completo,  fe.tp_forma_envio_id as tp_balancete " +
                "from entidade as e inner join municipio as m on e.municipio_id=m.municipio_id " +
                "inner join tipo_entidade as te on e.tp_entidade_id=te.tp_entidade_id " +
                "inner join tipo_entidade_poder as p on te.tp_entidade_poder_id=p.tp_entidade_poder_id " +
                "inner join forma_envio fe on fe.municipio_id = m.municipio_id and vigencia_ini_ano <= :ano_exercicio " +
                "and (vigencia_fim_ano is null or vigencia_fim_ano >= :ano_exercicio) " +
                "where 1=1 and e.tp_entidade_id = 50 and m.cd_municipio_ibge = :codigoIBGE", AudespEntidade.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("ano_exercicio", exercicio);

        List<AudespEntidade> lista = query.getResultList();

        for(int i = lista.size(); i < 1 ; i++)
            lista.add(new AudespEntidade());
        return lista.get(0);
    }

}