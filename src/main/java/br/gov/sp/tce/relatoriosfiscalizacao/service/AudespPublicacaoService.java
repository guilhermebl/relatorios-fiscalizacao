
package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespHipotese;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacaoDocumento;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.ValorResult;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespOperacoesCreditoRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespPublicacaoRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AudespPublicacaoService {

    @Autowired
    private AudespPublicacaoRepository audespPublicacaoRepository;

    public List<AudespPublicacaoDocumento> getLRF52EmAtraso(Integer codigoIbge, Integer exercicio) {
        List<AudespPublicacaoDocumento> documentos = audespPublicacaoRepository
                .getPublicacoesByTipoDocumento(codigoIbge, exercicio, new Integer[]{70,71,72,73,74,75,76} );
        documentos = documentos.stream().filter(x -> x.getAtrasado().equals("Sim") ).collect(Collectors.toList());

        return documentos;
    }

    public List<AudespPublicacaoDocumento> getRGFPublicacaoExecutivoEmAtraso(Integer codigoIbge, Integer exercicio) {
        List<AudespPublicacaoDocumento> documentos = audespPublicacaoRepository.getPublicacoesByTipoDocumento(codigoIbge, exercicio, new Integer[]{80} );
        documentos = documentos.stream().filter(x -> x.getAtrasado().equals("Sim") ).collect(Collectors.toList());

        return documentos;
    }

    public List<AudespPublicacaoDocumento> getRGFPublicacaoLegislativoEmAtraso(Integer codigoIbge, Integer exercicio) {
        List<AudespPublicacaoDocumento> documentos = audespPublicacaoRepository.getPublicacoesByTipoDocumento(codigoIbge, exercicio, new Integer[]{81} );
        documentos = documentos.stream().filter(x -> x.getAtrasado().equals("Sim") ).collect(Collectors.toList());

        return documentos;
    }

    public String getLRF52EmAtrasoMensagem(Integer codigoIbge, Integer exercicio) {
        List<AudespPublicacaoDocumento> documentos = getLRF52EmAtraso(codigoIbge,exercicio);
        return emAtrasoMensagem(documentos);
    }

    public String getRGFPublicacaoExecutivoEmAtrasoMensagem(Integer codigoIbge, Integer exercicio) {
        List<AudespPublicacaoDocumento> documentos = getRGFPublicacaoExecutivoEmAtraso(codigoIbge,exercicio);
        return emAtrasoMensagem(documentos);
    }

    public String getRGFPublicacaoLegislativoEmAtrasoMensagem(Integer codigoIbge, Integer exercicio) {
        List<AudespPublicacaoDocumento> documentos = getRGFPublicacaoLegislativoEmAtraso(codigoIbge,exercicio);
        return emAtrasoMensagem(documentos);
    }

    public String emAtrasoMensagem(List<AudespPublicacaoDocumento> documentos) {
        List<AudespPublicacaoDocumento> documentosAtrasados = documentos.stream().filter(x -> x.getAtrasado().equals("Sim") ).collect(Collectors.toList());

        if(documentos.size() == 0) {
            return "Cumpriu";
        }
        List<AudespPublicacaoDocumento> documentosNaoPublicados = documentosAtrasados.stream().filter(x -> x.getEstadoPublicacaoId() == 2 ).collect(Collectors.toList());

        if(documentosNaoPublicados.size() > 0 ) {
            return "Não cumpriu";
        }

        if(documentosNaoPublicados.size() == 0) {
            return "Cumpriu em atraso";
        }

        return "";
    }

}