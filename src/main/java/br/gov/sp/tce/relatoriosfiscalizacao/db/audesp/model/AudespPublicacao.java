package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@Entity
@IdClass(AudespPublicacaoParametroKey.class)
public class AudespPublicacao {

    @Id
    private Integer tipoDocumentoId = 0;

    @Id
    private Integer exercicio;

    @Id
    @Column(name = "mes_referencia")
    private Integer mes;

    @Id
    @Column(name = "dt_publicacao")
    private LocalDate dataPublicacao;

    @Id
    @Column(name = "veiculo_publicacao")
    private String veiculoPublicacao;

    @Id
    @Column(name = "ds_estado_publicacao")
    private String ordemPublicacao;

    @Column (name = "municipio_id")
    private Integer municipioId;

    @Column (name = "ds_municipio")
    private String descricaoMunicipio;

    @Column(name = "entidade_id")
    private Integer entidadeId;

    @Column(name = "nm_tp_documento")
    private String nomeTipoDocumento;

    @Column(name = "dt_prest_informacao")
    private LocalDateTime dataPrestacaoInformacao;

    @Transient
    private boolean dentroDoprazo;

    public Integer getQuadrimestre() {
        if(mes <= 4) {
            return 1;
        } else if(mes <= 8) {
            return 2;
        } else if(mes <= 12) {
            return 3;
        } else {
            return 0;
        }
    }



    public boolean validaDentroDoPrazo() {
        if(this.getDataPublicacao() == null)
            return false;

        int mes = this.getMes();
        int ano = this.getExercicio();
        int mesReferencia = this.getMes();
        int anoReferencia = this.getExercicio();

        int anoPublicacao = this.getDataPublicacao().getYear();
        int mesPublicacao = this.getDataPublicacao().getMonthValue();

        List<Integer> tiposDocumentosAnuais = new ArrayList<>();
        tiposDocumentosAnuais.add(77);
        tiposDocumentosAnuais.add(78);
        tiposDocumentosAnuais.add(79);

        if(tiposDocumentosAnuais.contains(tipoDocumentoId)) {
            mesReferencia = 12;
        }

        LocalDate dataReferencia =  LocalDate.of(anoReferencia, mesReferencia, 1);
        dataReferencia = dataReferencia.with(TemporalAdjusters.lastDayOfMonth());

        LocalDate dataLimite = dataReferencia.plusDays(30);

        if(dataPublicacao.isAfter(dataLimite)) {
            return false;
        } else {
            return true;
        }



        //ajuste pradrao audesp
//        if(tipoDocumentoId == 79 )
//            mes = mes - 1;
//        if(tipoDocumentoId == 79 && mes == 0 )
//            mes = 12;
        //ajuste padrao audesp

//        if(ano == this.getDataPublicacao().getYear() && mes == this.getDataPublicacao().getMonthValue() ) {
//            return true;
//        } else if( ano == this.getDataPublicacao().getYear() &&
//                ( mes + 1 == this.getDataPublicacao().getMonthValue()
//                        || mes + 2 == this.getDataPublicacao().getMonthValue()
//                )
//        ) {
//            return true;
//        } else if( ano + 1  == this.getDataPublicacao().getYear()
//                && ( mes == 11 || mes == 12 )
//                && ( this.getDataPublicacao().getMonthValue() == 1 )
//        ) {
//            return true;
//        } else if( mes == 1  && this.getDataPublicacao().getMonthValue() == 1  ) { // problema no audesp
//            return true;
////        } else if( LocalDateTime.of(ano,mes, 1,0,0).compareTo(this.getDataPublicacao()) == 1 ) { // problema no audesp
////            return true;
//        } else {
//            return false;
//        }


    }

    public boolean validaDentroDoPrazo60() {
        if(this.getDataPublicacao() == null)
            return false;

        int mes = this.getMes();
        int ano = this.getExercicio();

        if(ano == this.getDataPublicacao().getYear() && mes == this.getDataPublicacao().getMonthValue() ) {
            return true;
        } else if( ano == this.getDataPublicacao().getYear() &&
                ( mes + 1  == this.getDataPublicacao().getMonthValue()
                        || mes + 2 == this.getDataPublicacao().getMonthValue()
                )
        ) {
            return true;
        } else if( ano + 1  == this.getDataPublicacao().getYear()
                && ( mes == 11 || mes == 12 )
                && ( this.getDataPublicacao().getMonthValue() == 1
                || this.getDataPublicacao().getMonthValue() == 2
                )
        ) {
            return true;
        } else {
            return false;
        }


    }

    public boolean validaDentroDoPrazo30dias() {
        LocalDate ref = LocalDate.of(this.getExercicio(), this.getMes(), 1);
        ref = ref.plusMonths(2);
        if(this.getDataPublicacao().minusDays(30).isBefore(ref) ){
            return true;
        } else {
            return false;
        }
    }









    public Integer getTipoDocumentoId() {
        return tipoDocumentoId;
    }

    public void setTipoDocumentoId(Integer tipoDocumentoId) {
        this.tipoDocumentoId = tipoDocumentoId;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public LocalDate getDataPublicacao() {
        return dataPublicacao;
    }

    public String getDataPublicacaoFormatado() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return this.dataPublicacao.format(formatter);
    }

    public void setDataPublicacao(LocalDate dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public String getVeiculoPublicacao() {
        return veiculoPublicacao;
    }

    public void setVeiculoPublicacao(String veiculoPublicacao) {
        this.veiculoPublicacao = veiculoPublicacao;
    }

    public String getOrdemPublicacao() {
        return ordemPublicacao;
    }

    public void setOrdemPublicacao(String ordemPublicacao) {
        this.ordemPublicacao = ordemPublicacao;
    }

    public Integer getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(Integer municipioId) {
        this.municipioId = municipioId;
    }

    public String getDescricaoMunicipio() {
        return descricaoMunicipio;
    }

    public void setDescricaoMunicipio(String descricaoMunicipio) {
        this.descricaoMunicipio = descricaoMunicipio;
    }

    public Integer getEntidadeId() {
        return entidadeId;
    }

    public void setEntidadeId(Integer entidadeId) {
        this.entidadeId = entidadeId;
    }

    public String getNomeTipoDocumento() {
        return nomeTipoDocumento == null ? "" : nomeTipoDocumento;
    }

    public void setNomeTipoDocumento(String nomeTipoDocumento) {
        this.nomeTipoDocumento = nomeTipoDocumento;
    }

    public LocalDateTime getDataPrestacaoInformacao() {
        return dataPrestacaoInformacao;
    }

    public void setDataPrestacaoInformacao(LocalDateTime dataPrestacaoInformacao) {
        this.dataPrestacaoInformacao = dataPrestacaoInformacao;
    }

    public boolean isDentroDoprazo() {
        if(validaDentroDoPrazo())
            return true;
        else {
            System.out.println(toString());
            return false;
        }
    }

    @Override
    public String toString() {

        return "AudespPublicacao{" +
                "tipoDocumentoId=" + tipoDocumentoId +
                ", exercicio=" + exercicio +
                ", mes=" + mes +
                ", muni=" + descricaoMunicipio +
                ", dataPublicacao=" + dataPublicacao +
                ", veiculoPublicacao='" + veiculoPublicacao + '\'' +
                ", nomeTipoDocumento='" + nomeTipoDocumento + '\'' +
                ", dentroDoprazo=" + validaDentroDoPrazo() +
                '}';
    }
}
