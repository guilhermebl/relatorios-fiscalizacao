package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class AudespPublicacaoDocumento {

    @Id
    @Column(name = "documento_id")
    private Integer documentoId;

    @Column(name = "ds_municipio")
    private String municipio;

    @Column(name = "nm_tp_documento")
    private String nomeTipoDocumento;

    @Column(name = "tp_documento_id")
    private Integer tipoDocumentoId;

    @Column(name = "ano_exercicio")
    private Integer exercicio;

    @Column(name = "mes_referencia")
    private Integer mesReferencia;

    @Column(name = "mes_referencia_descricao")
    private String mesReferenciaDescricao;

    @Column(name = "dt_publicacao")
    private LocalDate dataPublicacao;

    @Column(name = "entregue_em")
    private LocalDate entregueEm;

    @Column(name = "estado_publicacao_id")
    private Integer estadoPublicacaoId;

    @Column(name = "ds_estado_publicacao")
    private String descricaoEstadoPublicacao;

    private String atrasado;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AudespPublicacaoDocumento that = (AudespPublicacaoDocumento) o;
        return Objects.equals(documentoId, that.documentoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentoId);
    }

    public Integer getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(Integer documentoId) {
        this.documentoId = documentoId;
    }

    public Integer getTipoDocumentoId() {
        return tipoDocumentoId;
    }

    public void setTipoDocumentoId(Integer tipoDocumentoId) {
        this.tipoDocumentoId = tipoDocumentoId;
    }

    public String getNomeTipoDocumento() {
        return nomeTipoDocumento;
    }

    public void setNomeTipoDocumento(String nomeTipoDocumento) {
        this.nomeTipoDocumento = nomeTipoDocumento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getMesReferencia() {
        return mesReferencia;
    }

    public void setMesReferencia(Integer mesReferencia) {
        this.mesReferencia = mesReferencia;
    }

    public String getMesReferenciaDescricao() {
        return mesReferenciaDescricao;
    }

    public void setMesReferenciaDescricao(String mesReferenciaDescricao) {
        this.mesReferenciaDescricao = mesReferenciaDescricao;
    }

    public LocalDate getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(LocalDate dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public LocalDate getEntregueEm() {
        return entregueEm;
    }

    public void setEntregueEm(LocalDate entregueEm) {
        this.entregueEm = entregueEm;
    }

    public Integer getEstadoPublicacaoId() {
        return estadoPublicacaoId;
    }

    public void setEstadoPublicacaoId(Integer estadoPublicacaoId) {
        this.estadoPublicacaoId = estadoPublicacaoId;
    }

    public String getDescricaoEstadoPublicacao() {
        return descricaoEstadoPublicacao;
    }

    public void setDescricaoEstadoPublicacao(String descricaoEstadoPublicacao) {
        this.descricaoEstadoPublicacao = descricaoEstadoPublicacao;
    }

    public String getAtrasado() {
        return atrasado;
    }

    public void setAtrasado(String atrasado) {
        this.atrasado = atrasado;
    }
}
