package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(AudespFase3QuadroPessoalKey.class)
public class AudespFase3QuadroPessoal {

    @Id
    private Integer exercicio;

    @Id
    private Integer tipoVaga = 0;

    private Integer total= 0;

    private Integer providas= 0;

    @Column(name = "nao_providas")
    private Integer naoProvidas = 0;

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getTipoVaga() {
        return tipoVaga;
    }

    public void setTipoVaga(Integer tipoVaga) {
        this.tipoVaga = tipoVaga;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getProvidas() {
        return providas;
    }

    public void setProvidas(Integer providas) {
        this.providas = providas;
    }

    public Integer getNaoProvidas() {
        return naoProvidas;
    }

    public void setNaoProvidas(Integer naoProvidas) {
        this.naoProvidas = naoProvidas;
    }
}
