package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespEntidadeRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespResultadoExecucaoOrcamentariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AudespEntidadeService {

    @Autowired
    private AudespEntidadeRepository audespEntidadeRepository;

    public AudespEntidade getEntidade(Integer codigoIBGE, Integer exercicio) {
        return audespEntidadeRepository.getEntidade(codigoIBGE, exercicio);
    }


}
