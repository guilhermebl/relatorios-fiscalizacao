package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@IdClass(QuestionarioItemKey.class)
public class QuestionarioItem {

    @Id
    private Integer municipioIbge;

    @Id
    private Integer operacaoId;

    @Id
    private Integer respostaId;

    @Column(name = "grupo")
    private String grupo;

    @Column(name = "pergunta_codigo")
    private String perguntaCodigo;

    @Column(name = "texto_pergunta")
    private String textoPergunta;

    @Column(name = "opcao_resposta_codigo")
    private String opcaoRespostaCodigo;

    @Column(name = "opcao_resposta_escolhida")
    private String opcaoRespostaEscolhida;

    @Column(name = "data_resposta")
    private LocalDateTime dataResposta;

    @Column(name = "resposta_comentario")
    private String respostaComentario;

    @Column(name = "valor_hora")
    private String valorHora;

    @Column(name = "valor_numerico")
    private BigDecimal valorNumerico;

    @Column(name = "valor_texto")
    private String valorTexto;

    @Column(name = "valor_inteiro")
    private String valorInteiro;

    @Column(name = "opcao_resposta_id")
    private Integer opcaoRespostaId;


    public Integer getMunicipioIbge() {
        return municipioIbge;
    }

    public void setMunicipioIbge(Integer municipioIbge) {
        this.municipioIbge = municipioIbge;
    }

    public Integer getOperacaoId() {
        return operacaoId;
    }

    public void setOperacaoId(Integer operacaoId) {
        this.operacaoId = operacaoId;
    }

    public Integer getRespostaId() {
        return respostaId;
    }

    public void setRespostaId(Integer respostaId) {
        this.respostaId = respostaId;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getPerguntaCodigo() {
        return perguntaCodigo;
    }

    public void setPerguntaCodigo(String perguntaCodigo) {
        this.perguntaCodigo = perguntaCodigo;
    }

    public String getTextoPergunta() {
        return textoPergunta;
    }

    public void setTextoPergunta(String textoPergunta) {
        this.textoPergunta = textoPergunta;
    }

    public String getOpcaoRespostaCodigo() {
        return opcaoRespostaCodigo;
    }

    public void setOpcaoRespostaCodigo(String opcaoRespostaCodigo) {
        this.opcaoRespostaCodigo = opcaoRespostaCodigo;
    }

    public String getOpcaoRespostaEscolhida() {
        return opcaoRespostaEscolhida;
    }

    public void setOpcaoRespostaEscolhida(String opcaoRespostaEscolhida) {
        this.opcaoRespostaEscolhida = opcaoRespostaEscolhida;
    }

    public LocalDateTime getDataResposta() {
        return dataResposta;
    }

    public void setDataResposta(LocalDateTime dataResposta) {
        this.dataResposta = dataResposta;
    }

    public String getRespostaComentario() {
        return respostaComentario;
    }

    public void setRespostaComentario(String respostaComentario) {
        this.respostaComentario = respostaComentario;
    }

    public String getValorHora() {
        return valorHora;
    }

    public void setValorHora(String valorHora) {
        this.valorHora = valorHora;
    }

    public BigDecimal getValorNumerico() {
        return valorNumerico;
    }

    public void setValorNumerico(BigDecimal valorNumerico) {
        this.valorNumerico = valorNumerico;
    }

    public String getValorTexto() {
        return valorTexto;
    }

    public void setValorTexto(String valorTexto) {
        this.valorTexto = valorTexto;
    }

    public String getValorInteiro() {
        return valorInteiro;
    }

    public void setValorInteiro(String valorInteiro) {
        this.valorInteiro = valorInteiro;
    }

    public Integer getOpcaoRespostaId() {
        return opcaoRespostaId;
    }

    public void setOpcaoRespostaId(Integer opcaoRespostaId) {
        this.opcaoRespostaId = opcaoRespostaId;
    }

}
