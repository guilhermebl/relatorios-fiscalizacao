package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository.ApontamentoODSRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ApontamentosODSService {

    @Autowired
    private ApontamentoODSRepository apontamentoODSRepository;

    public Map<String, List<ApontamentoODS>> getApontamentosODS(Integer codigoIbge, Integer exercicio) {

        List<ApontamentoODS> apontamentosList = apontamentoODSRepository.getApontamentosODS(codigoIbge, exercicio);

        Map<String, List<ApontamentoODS>> mapApontamentosODS = new HashMap<>();

        List<ApontamentoODS> iGovTi = new ArrayList<>();
        List<ApontamentoODS> iAmb = new ArrayList<>();
        List<ApontamentoODS> iCidade = new ArrayList<>();
        List<ApontamentoODS> iEduc = new ArrayList<>();
        List<ApontamentoODS> iFiscal = new ArrayList<>();
        List<ApontamentoODS> iSaude = new ArrayList<>();
        List<ApontamentoODS> iPlanejamento = new ArrayList<>();

        for (ApontamentoODS apontamento : apontamentosList) {
            String indice = apontamento != null ? apontamento.getIegmIndice().toLowerCase() : "";
            switch (indice) {
                case "i-gov ti":
                    iGovTi.add(apontamento);
                    break;
                case "i-amb":
                    iAmb.add(apontamento);
                    break;
                case "i-cidade":
                    iCidade.add(apontamento);
                    break;
                case "i-educ":
                    iEduc.add(apontamento);
                    break;
                case "i-fiscal":
                    iFiscal.add(apontamento);
                    break;
                case "i-saúde":
                    iSaude.add(apontamento);
                    break;
                case "i-planejamento":
                    iPlanejamento.add(apontamento);
                    break;
            }
        }

        mapApontamentosODS.put("i-gov ti", iGovTi);
        mapApontamentosODS.put("i-amb", iAmb);
        mapApontamentosODS.put("i-cidade", iCidade);
        mapApontamentosODS.put("i-educ", iEduc);
        mapApontamentosODS.put("i-fiscal", iFiscal);
        mapApontamentosODS.put("i-saúde", iSaude);
        mapApontamentosODS.put("i-planejamento", iPlanejamento);


        return mapApontamentosODS;

    }
}
