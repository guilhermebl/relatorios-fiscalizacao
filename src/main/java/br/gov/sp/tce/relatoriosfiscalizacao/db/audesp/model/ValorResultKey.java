package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ValorResultKey implements Serializable {

    @Column(name = "municipio_id", nullable = false)
    private Integer municipioId;

    @Column(name = "ano", nullable = false)
    private Integer ano;

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(Integer municipioId) {
        this.municipioId = municipioId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValorResultKey)) return false;
        ValorResultKey that = (ValorResultKey) o;
        return Objects.equals(ano, that.ano) &&
                Objects.equals(municipioId, that.municipioId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ano, municipioId);
    }
}

