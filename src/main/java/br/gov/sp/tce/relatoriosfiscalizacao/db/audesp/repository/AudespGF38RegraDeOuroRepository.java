package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.ValorResult;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespGF38RegraDeOuroRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    @PersistenceContext(unitName = "tcespbi")
    private EntityManager entityManagerTcespBi;

    public List<AudespResultado> getAudespRegraDeOuroParametros(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query = entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vOpCred','vDespCapLiq') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio in ( :exercicio )" +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultado.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultado> lista = query.getResultList();
        for (int i = lista.size(); i < 2; i++)
            lista.add(new AudespResultado());
        return lista;

    }


    public ValorResult getAudespRegraDeOuroValorGF28(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query = entityManager.createNativeQuery("select " +
                "vw_valor_parametro.municipio_id as municipio_id, " +
                "vw_valor_parametro.ano_exercicio as ano, " +
                "sum(coalesce(cast(valor AS numeric), 0)) as valor " +
                "from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                    "select entidade.entidade_id   " +
                    "from municipio   " +
                    "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                    "where entidade.tp_entidade_id = 50   " +
                    "and entidade.municipio_id <= 644   " +
                    "and municipio.cd_municipio_ibge =   :codigoIBGE   )  " +
                "and parametro_analise.nm_parametro_analise in ('vOpCred','vDespCapLiq') " +
                "and mes_referencia in (  :mesReferencia  ) and ano_exercicio in (  :exercicio  )  " +
                "group by " +
                "vw_valor_parametro.municipio_id, " +
                "vw_valor_parametro.ano_exercicio ", ValorResult.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        ValorResult valor = (ValorResult) query.getResultList().stream().findFirst().orElse(null);
        return valor;

    }





}