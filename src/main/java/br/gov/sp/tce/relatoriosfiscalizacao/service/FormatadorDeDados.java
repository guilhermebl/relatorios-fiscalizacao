package br.gov.sp.tce.relatoriosfiscalizacao.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class FormatadorDeDados {

    public static String formatarMoeda(BigDecimal valorDecimal, boolean exibirSimbolo, boolean indicarVazio) {
//        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
//        otherSymbols.setDecimalSeparator(',');
//        otherSymbols.setGroupingSeparator('.');

        Locale local = new Locale( "pt", "BR" );
//        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(local).;

        NumberFormat.getNumberInstance(local);

        DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getNumberInstance(local);
        decimalFormat.applyLocalizedPattern("#.##0,00");
//        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        valorDecimal = valorDecimal != null ? valorDecimal.setScale(2, RoundingMode.DOWN) : valorDecimal;

        String valor = valorDecimal == null ? tratarNull(indicarVazio) : decimalFormat.format(valorDecimal);
        return tratarSimboloMoeda(exibirSimbolo, valor);
    }

    private static String alteraSeparador(BigDecimal valorDecimal, boolean exibirSimbolo, boolean indicarVazio) {
        Locale local = new Locale( "pt", "BR" );
        return NumberFormat.getInstance(local).format(valorDecimal);
    }

    private static String tratarSimboloMoeda(boolean exibirSimbolo, String valor) {
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");

        if(valor.toString().trim().length() == 0)
            exibirSimbolo = false;
        return exibirSimbolo ? "R$ " + valor.toString() : valor;
    }

    public static String formatarPercentual(BigDecimal valorDecimal, boolean exibirPercentual, boolean indicarVazio) {
        return formatarPercentual(valorDecimal, exibirPercentual, indicarVazio, 4);
    }

    public static String formatarPercentual(BigDecimal valorDecimal, boolean exibirPercentual, boolean indicarVazio, int precisacao) {
        String valor = valorDecimal == null ? tratarNull(indicarVazio) : valorDecimal.multiply(new BigDecimal(100))
                .setScale(precisacao, BigDecimal.ROUND_HALF_EVEN)
                .toString()
                .replace(".", ",");
        return tratarSimboloPercentual(exibirPercentual, valor);
    }

    private static String tratarNull(boolean indicarVazio) {
        return indicarVazio ? "-" : "";
    }

    private static String tratarSimboloPercentual(boolean exibirPercentual, String valor) {
        if(valor.toString().trim().length() == 0)
            exibirPercentual = false;
        return exibirPercentual ? valor.toString() + "%" : valor;
    }

    public static String formatarLocalDateTime(LocalDateTime localDateTime) {
        if(localDateTime == null)
            return "";
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dataFormatada = localDateTime.format(formatador);
        return ""+dataFormatada;
    }
    public static String formatarLocalDate(LocalDate localDate) {
        if(localDate == null)
            return "";
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dataFormatada = localDate.format(formatador);
        return ""+dataFormatada;
    }

}
