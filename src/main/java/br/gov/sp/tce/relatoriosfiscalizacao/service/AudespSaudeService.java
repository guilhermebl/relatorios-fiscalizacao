package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultadoExecucaoOrcamentaria;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespSaude;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespSaudeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespSaudeService {

    @Autowired
    private AudespSaudeRepository audespSaudeRepository;

    public Map<String, String> getAudespSaude(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespSaude>
                audespResultadoExecucaoOrcamentariaList = audespSaudeRepository
                .getAudespSaude(codigoIbge, exercicio, mesReferencia);

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for(AudespSaude resultado: audespResultadoExecucaoOrcamentariaList) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), false, false, 2));
        }

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, String> getAplicacoesEmSaudeFormatado(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespSaude> aplicacoesEmSaudeList = audespSaudeRepository.getAplicacoesEmSaude(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosValores = Arrays.asList(new String[]{"vTotRecImpSau", "vDespEmpAplicSaude", "vDespTotSau", "vRPNPagoSau",
                "vDespEmpAplicSaude", "vRecImpSaudePrevAtu", "vDotAtuSaude"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vPercEmpSaude", "vPercPrevAtuSaude"});

        Map<String, String> mapa = new HashMap<>();
        for (AudespSaude resultado : aplicacoesEmSaudeList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false, 2));


            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise(), FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));

            }
        }

        return mapa;

    }

    public Map<String, BigDecimal> getAplicacoesEmSaude(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespSaude> aplicacoesEmSaudeList = audespSaudeRepository.getAplicacoesEmSaude(codigoIbge, exercicio, mesReferencia);

        Map<String, BigDecimal> mapa = new HashMap<>();
        for (AudespSaude resultado : aplicacoesEmSaudeList) {
                mapa.put(resultado.getNomeParametroAnalise(), resultado.getValor());
        }

        return mapa;

    }

    public Map<String, String> getPencentualAplicadoSaude(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultado> audespEnsinoList = audespSaudeRepository.getPencentualAplicadoSaude(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vPercEmpSaude"});

        Map<String, String> mapa = new HashMap<>();

        for (AudespResultado saude : audespEnsinoList) {
            if (saude.getValor() != null && parametrosPercentuais.contains(saude.getNomeParametroAnalise())) {
                mapa.put(saude.getNomeParametroAnalise() + saude.getExercicio() + saude.getMes(),
                        FormatadorDeDados.formatarPercentual(saude.getValor(), true, false, 2));
                int comp15 = saude.getValor().compareTo(new BigDecimal("0.15"));
                if( comp15 >= 0) {
                    mapa.put(saude.getNomeParametroAnalise() + saude.getExercicio() + saude.getMes() + "texto", "cumprindo");
                } else if(comp15 == -1) {
                    mapa.put(saude.getNomeParametroAnalise() + saude.getExercicio() + saude.getMes() + "texto", "não cumprindo");
                }
            }
        }
        return mapa;
    }
}