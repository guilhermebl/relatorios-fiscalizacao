package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@IdClass(AudespHipoteseKey.class)
public class AudespHipotese {

    @Id
    private Integer idHipotese;

    @Id
    private Integer idItemAnalise;

    @Id
    private Integer idEntidade;

    @Id
    private Integer exercicio;

    @Id
    private Integer mes;

    @Column (name = "identificador")
    private String identificador;

    @Column (name = "ds_hipotese")
    private String descricaoHipotese;

    @Column (name = "situacao_item_id")
    private Integer situacaoItemId;

    @Column(name = "dt_processamento")
    private LocalDateTime dataProcessamento;

    @Column(name = "resultado_prejudicado")
    private Boolean resultadoPrejudicado;

    public Integer getIdHipotese() {
        return idHipotese;
    }

    public void setIdHipotese(Integer idHipotese) {
        this.idHipotese = idHipotese;
    }

    public Integer getIdItemAnalise() {
        return idItemAnalise;
    }

    public void setIdItemAnalise(Integer idItemAnalise) {
        this.idItemAnalise = idItemAnalise;
    }

    public Integer getIdEntidade() {
        return idEntidade;
    }

    public void setIdEntidade(Integer idEntidade) {
        this.idEntidade = idEntidade;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getDescricaoHipotese() {
        return descricaoHipotese;
    }

    public void setDescricaoHipotese(String descricaoHipotese) {
        this.descricaoHipotese = descricaoHipotese;
    }

    public LocalDateTime getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(LocalDateTime dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public Boolean getResultadoPrejudicado() {
        return resultadoPrejudicado;
    }

    public void setResultadoPrejudicado(Boolean resultadoPrejudicado) {
        this.resultadoPrejudicado = resultadoPrejudicado;
    }

    public Integer getSituacaoItemId() {
        return situacaoItemId;
    }

    public void setSituacaoItemId(Integer situacaoItemId) {
        this.situacaoItemId = situacaoItemId;
    }

    public Integer getQuadrimestre() {
        if(mes <= 4) {
            return 1;
        } else if(mes <= 8) {
            return 2;
        } else if(mes <= 12) {
            return 3;
        } else {
            return 0;
        }
    }

}
