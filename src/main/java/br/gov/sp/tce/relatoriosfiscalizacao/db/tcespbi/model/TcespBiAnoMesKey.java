package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TcespBiAnoMesKey implements Serializable {

    @Column(name = "ano", nullable = false)
    private Integer ano;

    @Column(name = "mes", nullable = false)
    private Integer mes;

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TcespBiAnoMesKey)) return false;
        TcespBiAnoMesKey that = (TcespBiAnoMesKey) o;
        return Objects.equals(ano, that.ano) &&
                Objects.equals(mes, that.mes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ano, mes);
    }
}

