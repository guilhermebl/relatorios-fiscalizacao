package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.repository.ParecerPrefeituraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParecerPrefeituraService {

    @Autowired
    private ParecerPrefeituraRepository parecerPrefeituraRepository;

    public List<ParecerPrefeitura> getParecerByCodigoIbge(Integer codigoIbge) {
        List<ParecerPrefeitura> pareceresPrefeituras = parecerPrefeituraRepository.getParecerByCodigoIbge(codigoIbge);
        for(int i = pareceresPrefeituras.size(); i < 3; i++) {
            pareceresPrefeituras.add(new ParecerPrefeitura());
        }
        pareceresPrefeituras.sort( (e1, e2) -> e1.getExercicio().compareTo(e2.getExercicio()) );
        return pareceresPrefeituras;

    }
}
