package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

@Entity
@IdClass(AudespParametroKey.class)
public class AudespResultadoExecucaoOrcamentaria {

    @Id
    private Integer idParametroAnalise;

    @Id
    private Integer exercicio;

    @Id
    private Integer mes;

    @Column (name = "municipio_id")
    private Integer municipioId;

    @Column (name = "ds_municipio")
    private String descricaoMunicipio;

    @Column(name = "entidade_id")
    private Integer entidadeId;

    @Column(name = "nm_parametro_analise")
    private String nomeParametroAnalise;

    @Column(name = "ds_parametro_analise")
    private String descricaoParametroAnalise;

    private BigDecimal valor;

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(Integer municipioId) {
        this.municipioId = municipioId;
    }

    public String getDescricaoMunicipio() {
        return descricaoMunicipio;
    }

    public void setDescricaoMunicipio(String descricaoMunicipio) {
        this.descricaoMunicipio = descricaoMunicipio;
    }

    public Integer getEntidadeId() {
        return entidadeId;
    }

    public void setEntidadeId(Integer entidadeId) {
        this.entidadeId = entidadeId;
    }

    public String getNomeParametroAnalise() {
        return nomeParametroAnalise;
    }

    public void setNomeParametroAnalise(String nomeParametroAnalise) {
        this.nomeParametroAnalise = nomeParametroAnalise;
    }

    public String getDescricaoParametroAnalise() {
        return descricaoParametroAnalise;
    }

    public void setDescricaoParametroAnalise(String descricaoParametroAnalise) {
        this.descricaoParametroAnalise = descricaoParametroAnalise;
    }

    public Integer getIdParametroAnalise() {
        return idParametroAnalise;
    }

    public void setIdParametroAnalise(Integer idParametroAnalise) {
        this.idParametroAnalise = idParametroAnalise;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getValorString() {
        if(idParametroAnalise == null) {
            return "";
        }
        if(idParametroAnalise == 107) {
            return valor == null ? "": valor.multiply(new BigDecimal(100))
                    .setScale(2, BigDecimal.ROUND_HALF_EVEN)
                    .toString()
                    .replace(".", ",");
        } else {
                Locale local = new Locale( "pt", "BR" );
                return NumberFormat.getCurrencyInstance(local).format(this.valor);
        }
    }
}
