package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NotaIegmKey implements Serializable {

    @Column(name = "cd_municipio_ibge", nullable = false)
    private Integer cd_municipio_ibge;

    @Column(name = "exercicio", nullable = false)
    private int exercicio;

    public Integer getCd_municipio_ibge() {
        return cd_municipio_ibge;
    }

    public void setCd_municipio_ibge(Integer cd_municipio_ibge) {
        this.cd_municipio_ibge = cd_municipio_ibge;
    }

    public int getExercicio() {
        return exercicio;
    }

    public void setExercicio(int exercicio) {
        this.exercicio = exercicio;
    }
}

