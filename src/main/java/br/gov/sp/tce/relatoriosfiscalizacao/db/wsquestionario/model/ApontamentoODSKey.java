package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ApontamentoODSKey implements Serializable {

    @Column(name = "respid")
    private Integer respid;

    @Column(name = "odsid")
    private Integer odsid;

    @Column(name = "exercicio")
    private Integer exercicio;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApontamentoODSKey)) return false;
        ApontamentoODSKey that = (ApontamentoODSKey) o;
        return Objects.equals(respid, that.respid) &&
                Objects.equals(odsid, that.odsid) &&
                Objects.equals(exercicio, that.exercicio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(respid, odsid, exercicio);
    }
}

