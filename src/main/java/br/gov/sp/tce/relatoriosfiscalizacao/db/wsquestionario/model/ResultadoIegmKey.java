package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ResultadoIegmKey implements Serializable {

    @Column(name = "cd_municipio_ibge", nullable = false)
    private Integer municipioIbge;

    @Column(name = "exercicio", nullable = false)
    private int exercicio;

    public Integer getMunicipioIbge() {
        return municipioIbge;
    }

    public void setMunicipioIbge(Integer municipioIbge) {
        this.municipioIbge = municipioIbge;
    }

    public int getExercicio() {
        return exercicio;
    }

    public void setExercicio(int exercicio) {
        this.exercicio = exercicio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResultadoIegmKey)) return false;
        ResultadoIegmKey that = (ResultadoIegmKey) o;
        return Objects.equals(exercicio, that.exercicio) &&
                Objects.equals(municipioIbge, that.municipioIbge);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exercicio, municipioIbge);
    }


}

