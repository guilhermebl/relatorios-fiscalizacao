package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class RelatorioCamaraFechamento2018Controller {

    private XWPFDocument document;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Integer exercicio;

    private Integer codigoIBGE;

    @Autowired
    private ResourceLoader resourceLoader;

    FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");

    private String heading1 = "Seção 1";
    private String heading2 = "Seção 2";
    private String heading3 = "Seção 3";
    private String heading4 = "Seção 4";

    private TabelasProtocolo tabelasProtocolo;

    @Autowired
    private TabelasService tabelasService;

    private List<AudespResponsavel> responsavelPrefeitura;

    private Map<String,List<AudespResponsavel>> responsavelSubstitutoPrefeitura;

    private Map<String, String> audespResultadoExecucaoOrcamentaria;

    @Autowired
    private AudespService audespService;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;




    private ResponseEntity<Resource> sendFile() throws IOException {
        // fim --------------------------------------
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-relatorio-cm-v1.0.0.docx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }


    public byte[] toByteArray(InputStream in) throws IOException {

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int len;

        // read bytes from the input stream and store them in buffer
        while ((len = in.read(buffer)) != -1) {
            // write bytes from the buffer into output stream
            os.write(buffer, 0, len);
        }

        return os.toByteArray();
    }








    private void addTabelaDadosIniciais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "5%", "75%"}, false);
        formatacaoTabela.setBorder(false);


        String vigenciasPrefeito = "";

        for(int i = 0; i < this.responsavelPrefeitura.size(); i++) {
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataInicioVigenciaFormatado(): "dado não informado";
            vigenciasPrefeito += " a ";
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataFimVigenciaFormatado() :  "dado não informado";
            if(i != this.responsavelPrefeitura.size() - 1){
                vigenciasPrefeito += "; ";
            }
        }

        String vigenciasSubstituto = "";

        AudespResponsavel prefeito = (this.responsavelPrefeitura != null
                && this.responsavelPrefeitura.size() > 0) ? this.responsavelPrefeitura.get(0) : new AudespResponsavel();

        if(prefeito == null)
            prefeito = new AudespResponsavel();

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Processo", formatacaoFactory.getBold(12)));
        linha1.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha1.add(new TextoFormatado("TC-" + tabelasProtocolo.getProcesso(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha1);

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Entidade", formatacaoFactory.getBold(12)));
        linha2.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha2.add(new TextoFormatado(tabelasProtocolo.getNomeOrgao(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha2);

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Assunto", formatacaoFactory.getBold(12)));
        linha3.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha3.add(new TextoFormatado("Contas Anuais", formatacaoFactory.getFormatacao(12)));
        dados.add(linha3);

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Exercício", formatacaoFactory.getBold(12)));
        linha4.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha4.add(new TextoFormatado("2018", formatacaoFactory.getFormatacao(12)));
        dados.add(linha4);

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Prefeito", formatacaoFactory.getBold(12)));
        linha5.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha5.add(new TextoFormatado(prefeito.getNome(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha5);

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
        linha6.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha6.add(new TextoFormatado(prefeito.getCpf(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha6);

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
        linha7.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha7.add(new TextoFormatado(vigenciasPrefeito, formatacaoFactory.getFormatacao(12)));
        dados.add(linha7);

        for (Map.Entry<String, List<AudespResponsavel>> item : this.responsavelSubstitutoPrefeitura.entrySet()) {
            String nome = item.getKey();
            List<AudespResponsavel> listaSubstitutos = item.getValue();
            String vigencias = "";
            for (int i = 0; i < listaSubstitutos.size(); i++) {
                vigencias += listaSubstitutos.get(i).getDataInicioVigenciaFormatado();
                vigencias += " a ";
                vigencias += listaSubstitutos.get(i).getDataFimVigenciaFormatado();
                if (i != listaSubstitutos.size() - 1) {
                    vigencias += "; ";
                }
            }

            AudespResponsavel subs = listaSubstitutos != null && listaSubstitutos.size() > 0 ?
                    listaSubstitutos.get(0) : new AudespResponsavel();


            List<TextoFormatado> linhaSub = new ArrayList<>();
            linhaSub.add(new TextoFormatado("Substituto", formatacaoFactory.getBold(12)));
            linhaSub.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaSub.add(new TextoFormatado(nome, formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaCpf = new ArrayList<>();
            linhaCpf.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
            linhaCpf.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaCpf.add(new TextoFormatado(subs.getCpf(), formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaPeriodo = new ArrayList<>();
            linhaPeriodo.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
            linhaPeriodo.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaPeriodo.add(new TextoFormatado(vigencias, formatacaoFactory.getFormatacao(12)));

            dados.add(linhaSub);
            dados.add(linhaCpf);
            dados.add(linhaPeriodo);
        }


        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Relatoria", formatacaoFactory.getBold(12)));
        linha11.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha11.add(new TextoFormatado(tabelasProtocolo.getRelator(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha11);

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Instrução", formatacaoFactory.getBold(12)));
        linha12.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha12.add(new TextoFormatado(tabelasProtocolo.getSecaoFiscalizadoraContas().trim() + " / " +
                tabelasProtocolo.getDsf(), formatacaoFactory.getFormatacao(12)));

        dados.add(linha12);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }









    private void addSecao(TextoFormatado textoFormatado, String heading) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle(heading);
        paragraph.setSpacingAfter(6 * 20);
        if (heading.equals(this.heading1)) {
            CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill("d9d9d9");
        }

        textoFormatado.setParagraphText(paragraph);
    }

    private void createDocumentStyles() {
        XWPFStyles styles = document.createStyles();


        addCustomHeadingStyle(styles, heading1, 1);
        addCustomHeadingStyle(styles, heading2, 2);
        addCustomHeadingStyle(styles, heading3, 3);
        addCustomHeadingStyle(styles, heading4, 4);
    }


    private void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("Arial");

        CTRPr rpr = CTRPr.Factory.newInstance();
//        rpr.setRFonts(fonts);

        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }

    public byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }

    private TextoFormatado addTab() {
        Formatacao formatacaoTab = formatacaoFactory.getTab();
        return new TextoFormatado("", formatacaoTab).concat("", formatacaoTab);
    }

    private String getQuadrimestreTituloComAno(Integer quadrimestre) {
        String quadrimestreTitulo = "1º/2º Quadrimestre de 2018";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre de 2018";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre de 2018";
        return quadrimestreTitulo;
    }

    private String getQuadrimestreTitulo(Integer quadrimestre) {
        String quadrimestreTitulo = "3º Quadrimestre";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre";
        return quadrimestreTitulo;
    }

    private Integer getMesReferencia(Integer quadrimestre) {
        Integer mesReferencia = 12;
        if (quadrimestre == 1)
            mesReferencia = 4;
        if (quadrimestre == 2)
            mesReferencia = 8;
        return mesReferencia;
    }



    private void getTabela(XWPFDocument document, String[][] itensTabela, boolean isBold, String fontFamily, int fontSize) {
        XWPFTable table = document.createTable(0, 0);
        table.setWidthType(TableWidthType.PCT);
        table.setWidth("98%");
        table.removeBorders();

        for (int i = 0; i < itensTabela.length; i++) {
            if (table.getRow(i) == null)
                table.createRow();


            for (int x = 0; x < itensTabela[i].length; x++) {
                XWPFTableCell cell;
                // verifica se já existe a cell 0
                if (table.getRow(i).getCell(x) != null)
                    cell = table.getRow(i).getCell(x);
                else
                    cell = table.getRow(i).createCell();

                cell.removeParagraph(0);
                XWPFParagraph cellPar = cell.addParagraph();
                XWPFRun cellParRun = cellPar.createRun();
                cellParRun.setFontFamily(fontFamily);
                cellParRun.setText(itensTabela[i][x]);
                cellParRun.setBold(isBold);
                cellParRun.setFontSize(fontSize);
            }
        }
    }

    private void getHeader() throws InvalidFormatException, IOException {
        // Header
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

        CTP ctpFooter = CTP.Factory.newInstance();
        CTR ctrFooter = ctpFooter.addNewR();
        CTText ctFooter = ctrFooter.addNewT();
//        String footerText = "This is footer";
//        ctFooter.setStringValue(footerText);
        XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
        footerParagraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun footerRun = footerParagraph.createRun();
        footerRun.setFontFamily("Arial");
        footerRun.setFontSize(8);
        footerRun.getCTR().addNewPgNum();


        XWPFParagraph[] parsFooter = new XWPFParagraph[1];
        parsFooter[0] = footerParagraph;
        headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

        XWPFParagraph headerPar = header.createParagraph();
        XWPFRun headerParRun = headerPar.createRun();

        headerParRun.setFontFamily("Arial");
        headerParRun.setFontSize(12);
        headerPar.setAlignment(ParagraphAlignment.RIGHT);
//        headerParRun.setText("Fls. ");
//        headerParRun.getCTR().addNewPgNum();
//        headerParRun.addBreak();
//        headerParRun.setText("Processo nº " + tabelasProtocolo.getProcesso());
//
        XWPFTable headerTable = header.createTable(1, 3);
        headerTable.removeBorders();
        headerTable.setWidthType(TableWidthType.PCT);
        headerTable.setWidth("98%");

        XWPFTableRow headerTableRow = headerTable.getRow(0);

        //coluna 1
        headerTableRow.getCell(0).removeParagraph(0);
        XWPFParagraph pImgEsquerda = headerTableRow.getCell(0).addParagraph();
        pImgEsquerda.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgEsquerda = pImgEsquerda.createRun();

        Resource resourceBrasaoSP = resourceLoader.getResource("classpath:static/imagens/brasao_sp.png");

        InputStream brasao_sp = resourceBrasaoSP.getInputStream();
        runPImgEsquerda.addPicture(brasao_sp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_sp.png",
                Units.toEMU(50), Units.toEMU(55));

        //coluna 2
        headerTableRow.getCell(1).removeParagraph(0);
        XWPFParagraph p1 = headerTableRow.getCell(1).addParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p1Run = p1.createRun();

        p1Run.setFontFamily("Arial");
        p1Run.setFontSize(12);
        p1Run.setBold(true);
        p1Run.setText("TRIBUNAL DE CONTAS DO ESTADO DE SÃO PAULO");

        XWPFParagraph p2 = headerTableRow.getCell(1).addParagraph();
        p2.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p2Run = p2.createRun();
        p2Run.setFontSize(12);
        p2Run.setFontFamily("Arial");
        p2Run.setText(tabelasProtocolo.getDescricaoArea());

        //coluna 3
        headerTableRow.getCell(2).removeParagraph(0);
        XWPFParagraph pImgDireita = headerTableRow.getCell(2).addParagraph();
        pImgDireita.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgDireita = pImgDireita.createRun();
        Resource resourceBrasaoTcesp = resourceLoader.getResource("classpath:static/imagens/brasao_tcesp.png");

        InputStream brasao_tcesp = resourceBrasaoTcesp.getInputStream();
        runPImgDireita.addPicture(brasao_tcesp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_tcesp.png", Units.toEMU(50), Units.toEMU(55));
    }



    private XWPFParagraph getTitulo(XWPFDocument document, String quadrimestre) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();
        run.setBold(true);
        run.setFontFamily("Arial");
        run.setFontSize(12);
        run.setText("RELATÓRIO DE FISCALIZAÇÃO\nCÂMARA MUNICIPAL");
        return paragraph;
    }


    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();

//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());

        paragraph.setSpacingAfter(6 * 20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafoSpacingAfter(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();
//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());
//        paragraph.setSpacingAfter(6*20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado, boolean breakPage) {
        XWPFParagraph paragraph = addParagrafo(textoFormatado);
        paragraph.setPageBreak(breakPage);
        return paragraph;
    }

    private XWPFTable addTabela(List<List<TextoFormatado>> dados, FormatacaoTabela formatacaoTabela) {

        int qdtLinhas = dados.size();
        int qtdColunas = 0;

        for (int i = 0; i < dados.size(); i++) {
            if (qtdColunas < dados.get(i).size())
                qtdColunas = dados.get(i).size();
        }

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        formatacaoTabela.formatarTabela(tabela);

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
//            row.setHeight((int) (twipsPerInch * 1 / 5)); //set height 1/10 inch.
//            row.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact"
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }

            for (int j = 0; j < dados.get(i).size(); j++) {
                XWPFTableCell cell = j == 0 ? row.getCell(j) : row.createCell();
                cell.setWidth(formatacaoTabela.getWidths().get(j));
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                if (i == 0 && formatacaoTabela.isFirstLineHeader())
                    cell.getCTTc().addNewTcPr().addNewShd().setFill("cccccc");

                XWPFRun run = cell.getParagraphs().get(0).createRun();

                XWPFParagraph paragrafo = cell.getParagraphs().get(0);
                paragrafo.setSpacingBetween(1);

                dados.get(i).get(j).setParagraphText(paragrafo);

            }

        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable addTabelaColunaUnica(List<TextoFormatado> dados, FormatacaoTabela formatacaoTabela) {

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        for(int k = 1; k < tabela.getRows().size(); k++ ) {
            tabela.removeRow(k);
        }

        formatacaoTabela.formatarTabela(tabela);

        if(dados.size() == 0) {
            return tabela;
        }

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }
            XWPFTableCell cell = null;

            if(row.getTableCells().size() == 0)
                cell = row.createCell();
            else
                cell = row.getCell(0);

            cell.setWidth("100%");
            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            if (i % 2 == 0 )
                cell.getCTTc().addNewTcPr().addNewShd().setFill("dce6f1");

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            XWPFParagraph paragrafo = cell.getParagraphs().get(0);
            paragrafo.setSpacingBetween(1);

            dados.get(i).setParagraphText(paragrafo);
        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable mergeCells(XWPFTable tabela, List<MergePosition> cellsToMerge) {
        tabela = mergeCellsHorizontal(tabela, cellsToMerge);
        tabela = mergeCellsVertical(tabela, cellsToMerge);
        return tabela;

    }

    private void addListaNumerada(List<TextoFormatado> listItens) {

        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
        //Next we set the AbstractNumId. This requires care.
        //Since we are in a new document we can start numbering from 0.
        //But if we have an existing document, we must determine the next free number first.
        cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));

        /* Bullet list
          CTLvl cTLvl = cTAbstractNum.addNewLvl();
          cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
          cTLvl.addNewLvlText().setVal("•");
        */

        ///* Decimal list
        CTLvl cTLvl = cTAbstractNum.addNewLvl();
        cTLvl.addNewNumFmt().setVal(STNumberFormat.DECIMAL);
        cTLvl.addNewPPr();
        CTInd ind = cTLvl.getPPr().addNewInd(); //Set the indent

        ind.setHanging(BigInteger.valueOf(360*2));
        ind.setLeft(BigInteger.valueOf(360*6));
        cTLvl.addNewLvlText().setVal("%1.");
        cTLvl.addNewStart().setVal(BigInteger.valueOf(1));
        //*/

        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

        XWPFNumbering numbering = document.createNumbering();

        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

        BigInteger numID = numbering.addNum(abstractNumID);

        for (TextoFormatado item : listItens) {
            item.setListNumId(numID);
            addParagrafo(item);
        }
    }



    private XWPFTable mergeCellsHorizontal(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTHMerge hMerge = CTHMerge.Factory.newInstance();
            hMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setHMerge(hMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeHorizontal().size(); j++) {
                CTHMerge hToMerge = CTHMerge.Factory.newInstance();
                hToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeHorizontal().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeHorizontal().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setHMerge(hToMerge);
            }
        }

        return tabela;
    }

    private XWPFTable mergeCellsVertical(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTVMerge vMerge = CTVMerge.Factory.newInstance();
            vMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setVMerge(vMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeVertical().size(); j++) {
                CTVMerge vToMerge = CTVMerge.Factory.newInstance();
                vToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeVertical().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeVertical().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setVMerge(vToMerge);
            }
        }

        return tabela;
    }

    private void addBreak() {
        this.document.createParagraph();//.createRun().addBreak();
    }

    private void addPageBreak() {
        XWPFParagraph paragraph = this.document.createParagraph();
        paragraph.setPageBreak(true);
    }

    public void addSaudacao() {
//        if(secaoFiscalizadoraContas != null && Character.isDigit(secaoFiscalizadoraContas.charAt(0))){
//            return "Senhor(a) Diretor(a) da "+ getDescricaoArea() + ",";
//        }
//        else{
//            return "Senhor(a) Diretor(a) da " + getDescricaoArea() + ",";
//        }
        addParagrafo(new TextoFormatado(tabelasProtocolo.getSaudacao(), formatacaoFactory.getBold(12)));
    }


    private void addAnaliseFontesDocumentais() {
        List<TextoFormatado> itensLista = new ArrayList<>();
        itensLista.add(new TextoFormatado("Ações fiscalizatórias desenvolvidas através da seletividade " +
                "(contratos e repasses) e da fiscalização ordenada; ", formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Prestações de contas mensais do exercício em exame, " +
                "encaminhada pela Chefia do Poder Legislativo;", formatacaoFactory.getFormatacao(12)));

        itensLista.add(new TextoFormatado("Resultado do acompanhamento simultâneo do Sistema AUDESP, bem como " +
                "acesso aos dados, informações e análises disponíveis no referido ambiente;",
                formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das denúncias, representações e expedientes diversos; ",
                formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Leitura analítica dos três últimos relatórios de fiscalização e " +
                "respectivas decisões desta Corte, sobretudo no tocante a assuntos relevantes nas ressalvas, " +
                "advertências e recomendações;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das informações disponíveis nos demais sistemas " +
                "de e. Tribunal de Contas do Estado.", formatacaoFactory.getFormatacao(12)));

        addListaNumerada(itensLista);
    }


    private void addTabelaVerificacaoPlanejamentoPoliticasPublicas() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"90%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Verificação", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ",
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("A Câmara realizou audiências para debater os três planos orçamentários? ",
                formatacaoFactory.getJustificado(9))
        .concat("(Lei Complementar Federal nº 101, de 4 de maio de 2000 [Lei de Responsabilidade Fiscal], " +
                "art. 48º, § 1º, inciso I)", formatacaoFactory.getItalicJustificadoAzul(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);
    }

    public void addTabelaVerificacaoControleInterno() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "85%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Verificação", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("1", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("O Sistema de Controle Interno foi regulamentado?",
                formatacaoFactory.getJustificado(9))
                .concat("(Constituição Federal, art. 31)", formatacaoFactory.getItalicJustificadoAzul(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getJustificado(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("O Responsável pelo Controle Interno ocupa cargo efetivo na Administração Municipal?",
                formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getJustificado(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("3", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("O Controle Interno, quanto às suas funções institucionais, " +
                "apresenta relatórios periódicos? ", formatacaoFactory.getJustificado(9))
                .concat("(Constituição Federal, art. 74)", formatacaoFactory.getItalicJustificadoAzul(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getJustificado(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("4", formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("Com base no relatório do Controle Interno, o Presidente da Câmara " +
                "determinou as providências cabíveis?", formatacaoFactory.getJustificado(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getJustificado(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));

        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);
    }

    public void addTabelaHistoricoDosRepassesFinanceirosRecebidos() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"7%", "20%", "20%", "20%", "13%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Ano", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Previsão Final", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Repassados (Bruto)", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Resultado", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Devolução", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("2014", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2015", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("2016", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("2017", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("2018", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("2019", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaResultadoFinanceiroEconomicoPatrimonial() {
        Map<String,String> resultado = this.audespResultadoExecucaoOrcamentaria;


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("Resultados", formatacaoFactory.getBold(9)));
        cabecalhoLinha1.add(new TextoFormatado("Exercício em exame", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Exercício anterior", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("Financeiro", formatacaoFactory.getBold(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("vResultadoFinanceiro" + exercicio), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("vResultadoFinanceiro" + (exercicio - 1)), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("vResFinanceiroAH" + exercicio), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("Econômico", formatacaoFactory.getBold(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("vResultadoEconomico" + exercicio), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("vResultadoEconomico" + (exercicio-1)), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("vResEconomicoAH" + exercicio), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("Patrimonial", formatacaoFactory.getBold(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("vResultadoPatrimonial" + exercicio), formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("vSalPatrAnt" + (exercicio -1)), formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("vResPatrimonialAH" + exercicio), formatacaoFactory.getCenter(9)));

        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);


        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "30%", "30%", "20%"}, true));


    }

    public ResponseEntity<Resource> download(ParametroBusca parametroBusca)
            throws Exception {

        this.document = new XWPFDocument();

        this.exercicio = parametroBusca.getExercicio();
        this.codigoIBGE = parametroBusca.getCodigoIBGE();

        this.tabelasProtocolo = tabelasService.getTabelasProtocoloPrefeituraByCodigoIbge(codigoIBGE, exercicio);
        this.responsavelPrefeitura = audespService.getResponsavelPrefeitura(codigoIBGE,exercicio, 3);
        this.responsavelSubstitutoPrefeitura = audespService.getResponsavelSubstitutoPrefeitura(codigoIBGE,exercicio, 3);
        this.audespResultadoExecucaoOrcamentaria = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoFinanceiroEconomicoSaldoPatrimonial(parametroBusca);

        // margin
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        CTPageMar pageMar = sectPr.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(1700L));
        //pageMar.setTop(BigInteger.valueOf(1440L));
        //pageMar.setRight(BigInteger.valueOf(1680L));
        pageMar.setRight(BigInteger.valueOf(1700L));
        pageMar.setBottom(BigInteger.valueOf(1700L));
        //pageMar.setBottom(BigInteger.valueOf(1440L));

        CTBody body = document.getDocument().getBody();

        if (!body.isSetSectPr()) {
            body.addNewSectPr();
        }
        CTSectPr section = body.getSectPr();

        if(!section.isSetPgSz()) {
            section.addNewPgSz();
        }
        CTPageSz pageSize = section.getPgSz();

        pageSize.setW(BigInteger.valueOf(595*20));
        pageSize.setH(BigInteger.valueOf(842*20));


        createDocumentStyles();

        getHeader();

        addParagrafo(new TextoFormatado("RELATÓRIO DE FISCALIZAÇÃO\nCÂMARA MUNICIPAL", formatacaoFactory.getBoldCenter(12)));
        addBreak();
        addBreak();

        addTabelaDadosIniciais();

        addBreak();

        addSaudacao();

        addParagrafo(addTab().concat("Trata-se das contas apresentadas em face do art. 2º, III, da Lei " +
                        "Complementar Estadual nº 709, de 14 de janeiro de 1993 (Lei Orgânica do Tribunal de " +
                        "Contas do Estado de São Paulo).",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("O resultado da fiscalização ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" apresenta-se neste Relatório, sendo isso antecedido por planejamento que indicou a " +
                        "necessária extensão dos exames.", formatacaoFactory.getJustificado(12)) );

        addParagrafo(addTab().concat("Para tanto, baseou-se a Fiscalização nas seguintes fontes documentais:",
                        formatacaoFactory.getJustificado(12)));

        addAnaliseFontesDocumentais();

        addBreak();

        addParagrafo(addTab().concat("Em atendimento ao TC-A-30973/026/00, registramos a notificação do(s) Sr.(s). ",
                        formatacaoFactory.getJustificado(12))
                        .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(", responsável(is) pelas contas em exame.",
                                formatacaoFactory.getJustificado(12)));

        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA A: PLANEJAMENTO DAS POLÍTICAS PÚBLICAS E O SISTEMA DE " +
                "CONTROLE INTERNO", formatacaoFactory.getBold(12)), heading1);

        addBreak();
        addSecao(new TextoFormatado("A.1. PLANEJAMENTO DAS POLÍTICAS PÚBLICAS", formatacaoFactory.getBold(12)), heading2);

        addBreak();

        addTabelaVerificacaoPlanejamentoPoliticasPublicas();

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÂO", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("A LINHA DEVERÁ SER PREENCHIDA PELA FISCALIZAÇÃO COM AS " +
                "OPÇÕES: “SIM”, “NÃO” ou “PARCIAL”", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addSecao(new TextoFormatado("A.2. CONTROLE INTERNO", formatacaoFactory.getBold(12)), heading2);

        addBreak();

        addTabelaVerificacaoControleInterno();

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÂO", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("AS LINHAS DEVERÃO SER PREENCHIDAS PELA FISCALIZAÇÃO COM AS OPÇÕES: " +
                "“SIM”, “NÃO”, “PARCIAL” OU “PREJUDICADO”", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addSecao(new TextoFormatado("A.3. FISCALIZAÇÃO ORDENADA", formatacaoFactory.getBold(12)), heading2);

        addParagrafo(addTab().concat("Consoante determinação contida no processo TC-A-7361/026/16 foi (foram) " +
                        "realizada(s) no exercício a(s) seguinte(s) Fiscalização(ções) Ordenada(s):",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("• TRANSPARÊNCIA", formatacaoFactory.getBoldJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("Destacamos os seguintes apontamentos no relatório da inspeção realizada:",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat(" - ;", formatacaoFactory.getBoldJustificado(12)));
        addParagrafo(addTab().concat(" - .", formatacaoFactory.getBoldJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("1ª HIPÓTESE: Não houve alteração de informações",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Contudo, verificamos que a Câmara não providenciou adequações ao" +
                        " que foi apontado pela fiscalização.",
                        formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("2ª HIPÓTESE: Houve alteração de informações",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("Contudo, verificamos que a Câmara tomou as seguintes medidas a fim de " +
                        "sanear os apontamentos:",
                        formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("• TERCEIRIZAÇÃO: LIMPEZA E VIGILÂNCIA", formatacaoFactory.getBoldJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("Destacamos os seguintes apontamentos no relatório da inspeção realizada:",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat(" - ;", formatacaoFactory.getBoldJustificado(12)));
        addParagrafo(addTab().concat(" - .", formatacaoFactory.getBoldJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("1ª HIPÓTESE: Não houve alteração de informações",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Contudo, verificamos que a Câmara não providenciou adequações ao" +
                        " que foi apontado pela fiscalização.",
                formatacaoFactory.getJustificado(12)));

        addParagrafo(new TextoFormatado("2ª HIPÓTESE: Houve alteração de informações",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("Contudo, verificamos que a Câmara tomou as seguintes medidas a fim de " +
                        "sanear os apontamentos:",
                formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat(" - ;", formatacaoFactory.getBoldJustificado(12)));
        addParagrafo(addTab().concat(" - .", formatacaoFactory.getBoldJustificado(12)));

        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA B: EXECUÇÃO ORÇAMENTÁRIA, FINANCEIRA E PATRIMONIAL",
                formatacaoFactory.getBold(12)), heading1);

        addBreak();

        addSecao(new TextoFormatado("B.1. ASPECTOS FINANCEIROS", formatacaoFactory.getBold(12)), heading2);

        addBreak();

        addSecao(new TextoFormatado("B.1.1. HISTÓRICO DOS REPASSES FINANCEIROS RECEBIDOS",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();

        addTabelaHistoricoDosRepassesFinanceirosRecebidos();

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.2. RESULTADOS FINANCEIRO, ECONÔMICO E SALDO PATRIMONIAL",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();

        addTabelaResultadoFinanceiroEconomicoPatrimonial();



        return sendFile();

    }




}