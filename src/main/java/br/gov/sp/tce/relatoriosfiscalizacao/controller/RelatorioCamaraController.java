package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RelatorioCamaraController {

    @Autowired
    private RelatorioCamaraFechamento2018Controller relatorioCamaraFechamento2018Controller;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/camara/codigoibge/{codigoIBGE}/exercicio/{exercicio}/quadrimestre/{quadrimestre}")
    public ResponseEntity<Resource> download(@PathVariable Integer codigoIBGE, @PathVariable Integer exercicio,
                                             @PathVariable Integer quadrimestre) throws Exception {
        log.info("[C] municipio: " + codigoIBGE + " - exercício: " + exercicio + " - quadrimestre: " + quadrimestre);

        ParametroBusca parametroBusca = getParametroBusca(codigoIBGE, exercicio, quadrimestre);


        if( exercicio == 2018 && quadrimestre == 3 )
            return relatorioCamaraFechamento2018Controller.download(parametroBusca);
        else
            return new ResponseEntity("Pré-relatório não encontrado para o período",
                    new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    private ParametroBusca getParametroBusca(Integer codigoIBGE, Integer exercicio, Integer quadrimestre) {
        ParametroBusca parametroBusca = new ParametroBusca();
        parametroBusca.setCodigoIBGE(codigoIBGE);
        parametroBusca.setExercicio(exercicio);
        parametroBusca.setQuadrimestre(quadrimestre);
        parametroBusca.setTipoEntidadeId(51);
        if(quadrimestre != null)
            parametroBusca.setMesReferencia(quadrimestre * 4);
        return parametroBusca;
    }
}