package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.repository;


import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ParecerPrefeituraRepository {

    @PersistenceContext(unitName = "tcespbi")
    private EntityManager entityManager;


    public List<ParecerPrefeitura> getParecerByCodigoIbge(Integer codigoIBGE) {
        Query query = entityManager.createNativeQuery("SELECT tk_pareceres_contas_prefeituras,tc,orgao," +
                "exercicio,codigo_ibge,municipio,codigo_parecer, " +
                "parecer,data_atualizacao,parecer_link,data_transito_julgado,voto_link " +
                "from painel_municipio.pareceres_contas_prefeituras where codigo_ibge =  :codigoIBGE " +
                "order by exercicio desc limit 3" , ParecerPrefeitura.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        List<ParecerPrefeitura> result = query.getResultList();
        entityManager.close();
        return result;
    }



}