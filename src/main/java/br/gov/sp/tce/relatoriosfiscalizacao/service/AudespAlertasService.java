package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespAlertasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AudespAlertasService {

    @Autowired
    private AudespAlertasRepository audespAlertasRepository;

    public Integer getAlertasDespesaComPessoal(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        return audespAlertasRepository.getAlertasDespesaComPessoal(codigoIbge, exercicio, mesReferencia);
    }

    public Integer getAlertasDesajusteExecucaoOrcamentaria(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        return audespAlertasRepository.getAlertasDesajusteExecucaoOrcamentaria(codigoIbge, exercicio, mesReferencia);
    }

    public Integer getAlertasByItemAnalise(Integer codigoIbge, Integer exercicio, Integer mesReferencia, String[] itensAnalise) {
        return audespAlertasRepository.getAlertasByItemAnalise(codigoIbge, exercicio, mesReferencia, itensAnalise);
    }



    public Integer getAlertasEducacao(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        return audespAlertasRepository.getAlertasEducacao(codigoIbge, exercicio, mesReferencia);
    }

    public Integer getAlertasSaude(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        return audespAlertasRepository.getAlertasSaude(codigoIbge, exercicio, mesReferencia);
    }

}