package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.repository.IegmSmartRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository.IegmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IegmService {

    @Autowired
    private IegmSmartRepository iegmSmartRepository;

    @Autowired
    private IegmRepository iegmRepository;

    public List<NotaIegm> getNotasByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        List<NotaIegm> listNotas =  iegmSmartRepository.getNotasByCodigoIbge(codigoIBGE, exercicio);
        listNotas.sort((e1, e2) -> e1.getExercicio().compareTo(e2.getExercicio()));
        return listNotas;
    }

    public Map<Integer, NotaIegm> getMapNotasByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        List<NotaIegm> listNotas =  iegmSmartRepository.getNotasByCodigoIbge(codigoIBGE, exercicio);
        listNotas.sort((e1, e2) -> e1.getExercicio().compareTo(e2.getExercicio()));

        Map<Integer, NotaIegm> mapNotasIegm = new HashMap<>();
        for(NotaIegm notaIegm : listNotas) {
            mapNotasIegm.put(notaIegm.getExercicio(), notaIegm);
        }

        return mapNotasIegm;
    }

    public ResultadoIegm getNotasByCodigoIbgePorExercicio(Integer codigoIBGE, Integer exercicio) {
        List<ResultadoIegm> resultadoIegmList =  iegmRepository.getNotasByCodigoIbge(codigoIBGE, exercicio);
        resultadoIegmList.sort((e1, e2) -> e1.getExercicio().compareTo(e2.getExercicio()));
        ResultadoIegm resultadoIegm = new ResultadoIegm();
        if(resultadoIegmList != null && resultadoIegmList.size() > 0) {
            resultadoIegm = resultadoIegmList.get(0);
        }
        return resultadoIegm;
    }

    public MunicipioIbge getMunicipioByCodigoIbge(Integer codigoIBGE) {
        return iegmSmartRepository.getMunicipioByCodigoIbge(codigoIBGE);
    }



}
