package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import br.gov.sp.tce.relatoriosfiscalizacao.controller._2019.RelatorioPrefeituraFechamento2019ExcelController;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2019.RelatorioPrefeituraQuadrimestral2019ExcelController;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2020.RelatorioPrefeituraFechamento2020ExcelController;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2020.RelatorioPrefeituraQuadrimestral2020ExcelController;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2021.RelatorioPrefeituraFechamento2021ExcelController;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2021.RelatorioPrefeituraQuadrimestral2021ExcelController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RelatorioPrefeituraExcelController {

    @Autowired
    private RelatorioPrefeituraFechamento2021ExcelController relatorioPrefeituraFechamento2021ExcelController;

    @Autowired
    private RelatorioPrefeituraFechamento2020ExcelController relatorioPrefeituraFechamento2020ExcelController;

    @Autowired
    private RelatorioPrefeituraFechamento2019ExcelController relatorioPrefeituraFechamento2019ExcelController;

    @Autowired
    private RelatorioPrefeituraFechamento2018ExcelController relatorioPrefeituraFechamento2018ExcelController;


    @Autowired
    private RelatorioPrefeituraQuadrimestral2021ExcelController relatorioPrefeituraQuadrimestral2021ExcelController;


    @Autowired
    private RelatorioPrefeituraQuadrimestral2020ExcelController relatorioPrefeituraQuadrimestral2020ExcelController;

    @Autowired
    private RelatorioPrefeituraQuadrimestral2019ExcelController relatorioPrefeituraQuadrimestral2019ExcelController;


    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/prefeitura/codigoibge/{codigoIBGE}/exercicio/{exercicio}/quadrimestre/{quadrimestre}/excel")
    public ResponseEntity<Resource> download(@PathVariable Integer codigoIBGE, @PathVariable Integer exercicio,
                                             @PathVariable Integer quadrimestre) throws Exception {
        log.info("excel - municipio: " + codigoIBGE + " - exercício: " + exercicio + " - quadrimestre: " + quadrimestre);

        ParametroBusca parametroBusca = getParametroBusca(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2021 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2021ExcelController.download(parametroBusca);
        if( exercicio == 2021 && ( quadrimestre == 1 || quadrimestre == 2 ) )
            return relatorioPrefeituraQuadrimestral2021ExcelController.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2020 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2020ExcelController.download(parametroBusca);
        if( exercicio == 2020 && ( quadrimestre == 1 || quadrimestre == 2 ) )
            return relatorioPrefeituraQuadrimestral2020ExcelController.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2019 && ( quadrimestre == 1 || quadrimestre == 2 ) )
            return relatorioPrefeituraQuadrimestral2019ExcelController.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2019 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2019ExcelController.download(parametroBusca);
        if( exercicio == 2018 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2018ExcelController.download(parametroBusca);


        else
            return new ResponseEntity("Pré-relatório não encontrado para o período", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    private ParametroBusca getParametroBusca(Integer codigoIBGE, Integer exercicio, Integer quadrimestre) {
        ParametroBusca parametroBusca = new ParametroBusca();
        parametroBusca.setCodigoIBGE(codigoIBGE);
        parametroBusca.setExercicio(exercicio);
        parametroBusca.setQuadrimestre(quadrimestre);
        parametroBusca.setTipoEntidadeId(50);
        if(quadrimestre != null)
            parametroBusca.setMesReferencia(quadrimestre * 4);
        return parametroBusca;
    }


}