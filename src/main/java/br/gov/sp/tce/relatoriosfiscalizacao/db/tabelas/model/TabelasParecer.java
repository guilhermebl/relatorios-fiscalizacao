package br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class TabelasParecer {

    @Id
    private String processo;

    private Integer exercicio;

    @Column(name = "dtPublicacaoDO")
    private LocalDateTime publicacaoDO;

    public String getProcesso() {
        return processo.trim();
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public LocalDateTime getPublicacaoDO() {
        return publicacaoDO;
    }

    public void setPublicacaoDO(LocalDateTime publicacaoDO) {
        this.publicacaoDO = publicacaoDO;
    }
}