package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

@Entity
public class AudespEntidade {

    @Id
    @Column(name = "entidade_id")
    private Integer entidadeId;

    @Column(name = "municipio_id")
    private  Integer municipioId;

    @Column(name = "ds_municipio")
    private String descricaoMunicipio;

    @Column(name = "nome_completo")
    private String nomeCompleto;

    @Column(name = "tp_balancete")
    private Integer tpBalancete;


    public Integer getEntidadeId() {
        return entidadeId;
    }

    public void setEntidadeId(Integer entidadeId) {
        this.entidadeId = entidadeId;
    }

    public Integer getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(Integer municipioId) {
        this.municipioId = municipioId;
    }

    public String getDescricaoMunicipio() {
        return descricaoMunicipio;
    }

    public void setDescricaoMunicipio(String descricaoMunicipio) {
        this.descricaoMunicipio = descricaoMunicipio;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public Integer getTpBalancete() {
        return tpBalancete;
    }

    public void setTpBalancete(Integer tpBalancete) {
        this.tpBalancete = tpBalancete;
    }
}
