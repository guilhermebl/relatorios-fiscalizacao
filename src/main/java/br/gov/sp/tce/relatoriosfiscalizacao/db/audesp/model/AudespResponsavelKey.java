package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Embeddable
public class AudespResponsavelKey implements Serializable {

    @Column(name = "pessoa_id")
    private Integer id;

    @Column(name = "dt_ini_vigencia")
    private LocalDateTime dataInicioVigencia;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AudespResponsavelKey)) return false;
        AudespResponsavelKey that = (AudespResponsavelKey) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(dataInicioVigencia, that.dataInicioVigencia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dataInicioVigencia);
    }
}

