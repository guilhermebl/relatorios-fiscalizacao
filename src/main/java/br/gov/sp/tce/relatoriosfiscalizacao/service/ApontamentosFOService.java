package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository.ApontamentoODSRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ApontamentosFOService {

    @Autowired
    private ApontamentoODSRepository apontamentoODSRepository;

    public Map<Integer, List<ApontamentoFO>> getApontamentosFO(Integer codigoIbge, Integer exercicio) {

        List<ApontamentoFO> apontamentoFOList = apontamentoODSRepository.getApontamentosFO2018(codigoIbge, exercicio);

        Map<Integer, List<ApontamentoFO>> mapa = new TreeMap<>();

        for(ApontamentoFO apontamentoFO : apontamentoFOList) {
            mapa.put(apontamentoFO.getOperacaoId(), new ArrayList<>());
        }
        for(ApontamentoFO apontamentoFO : apontamentoFOList) {
            mapa.get(apontamentoFO.getOperacaoId()).add(apontamentoFO);
        }

        return mapa;

    }
}
