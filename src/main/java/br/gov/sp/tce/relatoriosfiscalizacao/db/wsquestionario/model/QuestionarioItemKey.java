package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class QuestionarioItemKey implements Serializable {

    @Column(name = "municipio_ibge", nullable = false)
    private Integer municipioIbge;

    @Column(name = "operacao_id", nullable = false)
    private Integer operacaoId;

    @Column(name = "resposta_id", nullable = false)
    private int respostaId;

    public Integer getMunicipioIbge() {
        return municipioIbge;
    }

    public void setMunicipioIbge(Integer municipioIbge) {
        this.municipioIbge = municipioIbge;
    }

    public Integer getOperacaoId() {
        return operacaoId;
    }

    public void setOperacaoId(Integer operacaoId) {
        this.operacaoId = operacaoId;
    }

    public int getRespostaId() {
        return respostaId;
    }

    public void setRespostaId(int respostaId) {
        this.respostaId = respostaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuestionarioItemKey)) return false;
        QuestionarioItemKey that = (QuestionarioItemKey) o;
        return getRespostaId() == that.getRespostaId() &&
                Objects.equals(getMunicipioIbge(), that.getMunicipioIbge()) &&
                Objects.equals(getOperacaoId(), that.getOperacaoId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMunicipioIbge(), getOperacaoId(), getRespostaId());
    }
}

