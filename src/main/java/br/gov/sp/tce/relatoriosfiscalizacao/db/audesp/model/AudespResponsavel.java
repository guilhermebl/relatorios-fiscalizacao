package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import br.gov.sp.tce.relatoriosfiscalizacao.service.FormatadorDeDados;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.time.LocalDateTime;

@Entity
@IdClass(AudespResponsavelKey.class)
public class AudespResponsavel {

    @Id
    private Integer id;

    @Id
    private LocalDateTime dataInicioVigencia;

    @Column(name = "dt_fim_vigencia")
    private LocalDateTime dataFimVigencia;

    private String nome = "";

    private String cpf = "";

    @Column(name = "cd_municipio_ibge")
    private Integer codigoIbge;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        if(nome == null)
            return "";
        else
            return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        if(cpf == null)
            return "";
        else
            return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDateTime getDataInicioVigencia() {
        return dataInicioVigencia;
    }


    public String getDataInicioVigenciaFormatado() {
        return FormatadorDeDados.formatarLocalDateTime(this.dataInicioVigencia);
    }

    public void setDataInicioVigencia(LocalDateTime dataInicioVigencia) {
        this.dataInicioVigencia = dataInicioVigencia;
    }

    public LocalDateTime getDataFimVigencia() {
        return dataFimVigencia;
    }

    public String getDataFimVigenciaFormatado() {
        return dataFimVigencia == null ? "dado não informado" : FormatadorDeDados.formatarLocalDateTime(dataFimVigencia);
    }

    public void setDataFimVigencia(LocalDateTime dataFimVigencia) {
        this.dataFimVigencia = dataFimVigencia;
    }

    public Integer getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(Integer codigoIbge) {
        this.codigoIbge = codigoIbge;
    }
}
