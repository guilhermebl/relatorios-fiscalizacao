package br.gov.sp.tce.relatoriosfiscalizacao.controller._2022;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.*;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class RelatorioPrefeituraQuadrimestral2022Controller {

    private XWPFDocument document;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Integer exercicio;

    private Integer codigoIBGE;

    FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");

    @Autowired
    private IegmService iegmService;

    private ResultadoIegm resultadoIegmAnoBaseAnterior;
    private ResultadoIegm resultadoIegmAnoBaseRetrasado;
    private ResultadoIegm resultadoIegmAnoBaseReRetrasado;

    @Autowired
    private ParecerPrefeituraService parecerPrefeituraService;

    private List<ParecerPrefeitura> pareceresPrefeiturasList;

    @Autowired
    private TabelasService tabelasService;

    private TabelasProtocolo tabelasProtocolo;

    @Autowired
    private AudespEnsinoService audespEnsinoService;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    @Autowired
    private AudespSaudeService audespSaudeService;

    @Autowired
    private AudespLimiteLrfService audespLimiteLrfService;

    @Autowired
    private AudespService audespService;

    @Autowired
    private AudespAlertasService audespAlertasService;

    @Autowired
    private ApontamentosODSService apontamentosODSService;

    private Map<String, List<ApontamentoODS>> apontamentosODS;

    @Autowired
    private ResourceLoader resourceLoader;

    private Map<Integer, NotaIegm> notasIegm;

    private List<AudespResponsavel> responsavelPrefeitura;

    private Map<String, List<AudespResponsavel>> responsavelSubstitutoPrefeitura;

    private MunicipioIbge municipioIegmCodigoIbge;

    private Map<String, String> audespResultadoExecucaoOrcamentaria;

    private Map<String, String> audespResultadoExecucaoOrcamentariaExcercicioAnt;

    @Autowired
    private AudespEntidadeService audespEntidadeService;

    private AudespEntidade audespEntidade;

    @Autowired
    private DemonstrativosRaeeService demonstrativosRaeeService;

    private Map<String, String> anexo14AMap;

    private Map<String, String> audespEnsinoFundeb;

    private Map<String, String> audespSaude;

    private Map<String, String> audespDespesaPessoalMap;

    private Map<String, String> quadroGeralEnsinoMap;

    private Map<String, String> aplicacoesEmSaude;

    @Autowired
    private ApontamentosFOService apontamentosFOService;

    private Map<Integer, List<ApontamentoFO>> apontamentoFOMap;

    @Autowired
    private AudespDividaAtivaService audespDividaAtivaService;

    private Map<String, String> audespDividaAtivaMap;

    private Map<String, String> audespResultadoExecucaoOrcamentariaMap;

    @Autowired
    private AudespFase3Service audespFase3Service;

    private Map<String, AudespFase3QuadroPessoal> audespFase3QuadroDePessoalMap;

    @Autowired
    private AudespBiService audespBiService;

    @Autowired
    private TcespBiService tcespBiService;

    private Map<Integer, String> valorInvestimentoMunicipioMap;

    private Map<String, String> rclMunicipioDevedoresMap;

    private Map<String, String> limiteLRFMap;

    private Integer quadrimestre;

    private String heading1 = "Seção 1";
    private String heading2 = "Seção 2";
    private String heading3 = "Seção 3";
    private String heading4 = "Seção 4";
    private String heading5 = "Seção 5";


    private ResponseEntity<Resource> sendFile() throws IOException {
        // fim --------------------------------------
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-relatorio-pm-quadr-2022-v1.0.0.docx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }


    private void addSecao(TextoFormatado textoFormatado, String heading) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle(heading);
        paragraph.setSpacingAfter(6 * 20);
        if (heading.equals(this.heading1)) {
            CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill("d9d9d9");
        }

        textoFormatado.setParagraphText(paragraph);
    }

    private void createDocumentStyles() {
        XWPFStyles styles = document.createStyles();

        addCustomHeadingStyle(styles, heading1, 1);
        addCustomHeadingStyle(styles, heading2, 2);
        addCustomHeadingStyle(styles, heading3, 3);
        addCustomHeadingStyle(styles, heading4, 4);
        addCustomHeadingStyle(styles, heading5, 5);
    }


    private void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("Arial");

        CTRPr rpr = CTRPr.Factory.newInstance();
//        rpr.setRFonts(fonts);

        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }

    public byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }

    private TextoFormatado addTab() {
        Formatacao formatacaoTab = formatacaoFactory.getTab();
        return new TextoFormatado("", formatacaoTab).concat("", formatacaoTab);
    }

    private String getQuadrimestreTituloComAno(Integer quadrimestre) {
        String quadrimestreTitulo = "1º/2º Quadrimestre de 2018";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre de 2018";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre de 2018";
        return quadrimestreTitulo;
    }

    private String getQuadrimestreTitulo(Integer quadrimestre) {
        String quadrimestreTitulo = "3º Quadrimestre";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre";
        return quadrimestreTitulo;
    }

    private Integer getMesReferencia(Integer quadrimestre) {
        Integer mesReferencia = 12;
        if (quadrimestre == 1)
            mesReferencia = 4;
        if (quadrimestre == 2)
            mesReferencia = 8;
        return mesReferencia;
    }

    private void getTabela(XWPFDocument document, String[][] itensTabela, boolean isBold, String fontFamily,
                           int fontSize) {
        XWPFTable table = document.createTable(0, 0);
        table.setWidthType(TableWidthType.PCT);
        table.setWidth("98%");
        table.removeBorders();

        for (int i = 0; i < itensTabela.length; i++) {
            if (table.getRow(i) == null)
                table.createRow();


            for (int x = 0; x < itensTabela[i].length; x++) {
                XWPFTableCell cell;
                // verifica se já existe a cell 0
                if (table.getRow(i).getCell(x) != null)
                    cell = table.getRow(i).getCell(x);
                else
                    cell = table.getRow(i).createCell();

                cell.removeParagraph(0);
                XWPFParagraph cellPar = cell.addParagraph();
                XWPFRun cellParRun = cellPar.createRun();
                cellParRun.setFontFamily(fontFamily);
                cellParRun.setText(itensTabela[i][x]);
                cellParRun.setBold(isBold);
                cellParRun.setFontSize(fontSize);
            }
        }
    }


    private String formataTcManualRedacao(String tc) {
        while (tc.length() < 13) {
            tc = "0" + tc;
        }
        tc = tc.replaceAll("/", ".");
        return tc;
    }

    private void addTabelaDadosIniciais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "5%", "75%"},
                false);
        formatacaoTabela.setBorder(false);


        String vigenciasPrefeito = "";

        for (int i = 0; i < this.responsavelPrefeitura.size(); i++) {
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ?
                    this.responsavelPrefeitura.get(i).getDataInicioVigenciaFormatado() : "dado não informado";
            vigenciasPrefeito += " a ";
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ?
                    this.responsavelPrefeitura.get(i).getDataFimVigenciaFormatado() : "dado não informado";
            if (i != this.responsavelPrefeitura.size() - 1) {
                vigenciasPrefeito += "; ";
            }
        }

        String vigenciasSubstituto = "";

        AudespResponsavel prefeito = (this.responsavelPrefeitura != null
                && this.responsavelPrefeitura.size() > 0) ? this.responsavelPrefeitura.get(0) : new AudespResponsavel();

        if (prefeito == null)
            prefeito = new AudespResponsavel();

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Processo", formatacaoFactory.getBold(12)));
        linha1.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha1.add(new TextoFormatado("TC-" + formataTcManualRedacao(tabelasProtocolo.getProcesso()),
                formatacaoFactory.getFormatacao(12)));
        dados.add(linha1);

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Entidade", formatacaoFactory.getBold(12)));
        linha2.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha2.add(new TextoFormatado(tabelasProtocolo.getNomeOrgao(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha2);

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Assunto", formatacaoFactory.getBold(12)));
        linha3.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha3.add(new TextoFormatado("Acompanhamento das Contas Anuais", formatacaoFactory.getFormatacao(12)));
        dados.add(linha3);

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Período examinado", formatacaoFactory.getBold(12)));
        linha4.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha4.add(new TextoFormatado(this.quadrimestre + "º quadrimestre de " + exercicio,
                formatacaoFactory.getFormatacao(12)));
        dados.add(linha4);

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Prefeito", formatacaoFactory.getBold(12)));
        linha5.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha5.add(new TextoFormatado(prefeito.getNome(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha5);

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
        linha6.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha6.add(new TextoFormatado(prefeito.getCpf(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha6);

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
        linha7.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha7.add(new TextoFormatado(vigenciasPrefeito, formatacaoFactory.getFormatacao(12)));
        dados.add(linha7);

        for (Map.Entry<String, List<AudespResponsavel>> item : this.responsavelSubstitutoPrefeitura.entrySet()) {
            String nome = item.getKey();
            List<AudespResponsavel> listaSubstitutos = item.getValue();
            String vigencias = "";
            for (int i = 0; i < listaSubstitutos.size(); i++) {
                vigencias += listaSubstitutos.get(i).getDataInicioVigenciaFormatado();
                vigencias += " a ";
                vigencias += listaSubstitutos.get(i).getDataFimVigenciaFormatado();
                if (i != listaSubstitutos.size() - 1) {
                    vigencias += "; ";
                }
            }

            AudespResponsavel subs = listaSubstitutos != null && listaSubstitutos.size() > 0 ?
                    listaSubstitutos.get(0) : new AudespResponsavel();


            List<TextoFormatado> linhaSub = new ArrayList<>();
            linhaSub.add(new TextoFormatado("Substituto", formatacaoFactory.getBold(12)));
            linhaSub.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaSub.add(new TextoFormatado(nome, formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaCpf = new ArrayList<>();
            linhaCpf.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
            linhaCpf.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaCpf.add(new TextoFormatado(subs.getCpf(), formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaPeriodo = new ArrayList<>();
            linhaPeriodo.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
            linhaPeriodo.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaPeriodo.add(new TextoFormatado(vigencias, formatacaoFactory.getFormatacao(12)));

            dados.add(linhaSub);
            dados.add(linhaCpf);
            dados.add(linhaPeriodo);
        }


        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Relatoria", formatacaoFactory.getBold(12)));
        linha11.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha11.add(new TextoFormatado(tabelasProtocolo.getRelator(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha11);

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Instrução", formatacaoFactory.getBold(12)));
        linha12.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha12.add(new TextoFormatado(tabelasProtocolo.getSecaoFiscalizadoraContas().trim() + " / " +
                tabelasProtocolo.getDsf(), formatacaoFactory.getFormatacao(12)));

        dados.add(linha12);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void getHeader() throws InvalidFormatException, IOException {
        // Header
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

        CTP ctpFooter = CTP.Factory.newInstance();
        CTR ctrFooter = ctpFooter.addNewR();
        CTText ctFooter = ctrFooter.addNewT();

        XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
        footerParagraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun footerRun = footerParagraph.createRun();
        footerRun.setFontFamily("Arial");
        footerRun.setFontSize(8);
        footerRun.getCTR().addNewPgNum();

        XWPFParagraph[] parsFooter = new XWPFParagraph[1];
        parsFooter[0] = footerParagraph;
        headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

        XWPFParagraph headerPar = header.createParagraph();
        XWPFRun headerParRun = headerPar.createRun();

        headerParRun.setFontFamily("Arial");
        headerParRun.setFontSize(12);
        headerPar.setAlignment(ParagraphAlignment.RIGHT);

        XWPFTable headerTable = header.createTable(1, 3);
        headerTable.removeBorders();
        headerTable.setWidthType(TableWidthType.PCT);
        headerTable.setWidth("98%");

        XWPFTableRow headerTableRow = headerTable.getRow(0);

        //coluna 1
        headerTableRow.getCell(0).removeParagraph(0);
        XWPFParagraph pImgEsquerda = headerTableRow.getCell(0).addParagraph();
        pImgEsquerda.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgEsquerda = pImgEsquerda.createRun();

        Resource resourceBrasaoSP = resourceLoader.getResource("classpath:static/imagens/brasao_sp.png");

        InputStream brasao_sp = resourceBrasaoSP.getInputStream();
        runPImgEsquerda.addPicture(brasao_sp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_sp.png",
                Units.toEMU(50), Units.toEMU(55));

        //coluna 2
        headerTableRow.getCell(1).removeParagraph(0);
        XWPFParagraph p1 = headerTableRow.getCell(1).addParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p1Run = p1.createRun();

        p1Run.setFontFamily("Arial");
        p1Run.setFontSize(12);
        p1Run.setBold(true);
        p1Run.setText("TRIBUNAL DE CONTAS DO ESTADO DE SÃO PAULO");

        XWPFParagraph p2 = headerTableRow.getCell(1).addParagraph();
        p2.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p2Run = p2.createRun();
        p2Run.setFontSize(12);
        p2Run.setFontFamily("Arial");
        p2Run.setText(tabelasProtocolo.getDescricaoArea());

        //coluna 3
        headerTableRow.getCell(2).removeParagraph(0);
        XWPFParagraph pImgDireita = headerTableRow.getCell(2).addParagraph();
        pImgDireita.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgDireita = pImgDireita.createRun();
        Resource resourceBrasaoTcesp = resourceLoader.getResource("classpath:static/imagens/brasao_tcesp.png");

        InputStream brasao_tcesp = resourceBrasaoTcesp.getInputStream();
        runPImgDireita.addPicture(brasao_tcesp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_tcesp.png", Units.toEMU(50),
                Units.toEMU(55));
    }


    private XWPFParagraph getTitulo(XWPFDocument document, String quadrimestre) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();
        run.setBold(true);
        run.setFontFamily("Arial");
        run.setFontSize(12);
        run.setText("RELATÓRIO DE FISCALIZAÇÃO 1º / 2º QUADRIMESTRE\nPREFEITURA MUNICIPAL");
        return paragraph;
    }

    private void underLine(XWPFRun run) {
        run.setUnderline(UnderlinePatterns.SINGLE);
    }

    private XWPFRun addToParagrafoBreak(XWPFParagraph paragrafo, String texto) {
        XWPFRun run = paragrafo.getRuns().get(0);
        run.addBreak();
        run.setText(texto);
        return run;
    }

    private void addToParagrafoRed(XWPFParagraph paragrafo, String texto, String fontStyle, int fontSize) {
        XWPFRun run = paragrafo.createRun();
        run.setText(texto);
        run.setColor("ff0000");
        run.setFontFamily(fontStyle);
        run.setFontSize(fontSize);
        run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
    }

    private XWPFParagraph getParagrafoGrayHeader(XWPFDocument document, String texto, boolean isBold,
                                                 String fontFamily, int fontSize,
                                                 ParagraphAlignment paragraphAlignment, boolean haveTab) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(paragraphAlignment);
        XWPFRun run = paragraph.createRun();
        if (haveTab)
            run.addTab();

        run.setBold(isBold);
        run.setFontFamily(fontFamily);
        run.setFontSize(fontSize);
        run.setText(texto);

        // shading
        //run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
        //run.getCTR().addNewRPr().addNewShd();
        CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
        cTShd.setVal(STShd.CLEAR);
        cTShd.setFill("d9d9d9");

        return paragraph;
    }


    //////////////////////
    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();

//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());

        paragraph.setSpacingAfter(6 * 20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafoSpacingAfter(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();
//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());
//        paragraph.setSpacingAfter(6*20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado, boolean breakPage) {
        XWPFParagraph paragraph = addParagrafo(textoFormatado);
        paragraph.setPageBreak(breakPage);
        return paragraph;
    }

    private XWPFTable addTabela(List<List<TextoFormatado>> dados, FormatacaoTabela formatacaoTabela) {

        int qdtLinhas = dados.size();
        int qtdColunas = 0;

        for (int i = 0; i < dados.size(); i++) {
            if (qtdColunas < dados.get(i).size())
                qtdColunas = dados.get(i).size();
        }

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        formatacaoTabela.formatarTabela(tabela);

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
//            row.setHeight((int) (twipsPerInch * 1 / 5)); //set height 1/10 inch.
//            row.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact"
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }

            for (int j = 0; j < dados.get(i).size(); j++) {
                XWPFTableCell cell = j == 0 ? row.getCell(j) : row.createCell();
                cell.setWidth(formatacaoTabela.getWidths().get(j));
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                if (i == 0 && formatacaoTabela.isFirstLineHeader())
                    cell.getCTTc().addNewTcPr().addNewShd().setFill("cccccc");

                XWPFRun run = cell.getParagraphs().get(0).createRun();

                XWPFParagraph paragrafo = cell.getParagraphs().get(0);
                paragrafo.setSpacingBetween(1);

                dados.get(i).get(j).setParagraphText(paragrafo);

            }

        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable addTabelaColunaUnica(List<TextoFormatado> dados, FormatacaoTabela formatacaoTabela) {

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        for (int k = 1; k < tabela.getRows().size(); k++) {
            tabela.removeRow(k);
        }

        formatacaoTabela.formatarTabela(tabela);

        if (dados.size() == 0) {
            return tabela;
        }

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }
            XWPFTableCell cell = null;

            if (row.getTableCells().size() == 0)
                cell = row.createCell();
            else
                cell = row.getCell(0);

            cell.setWidth("100%");
            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            if (i % 2 == 0)
                cell.getCTTc().addNewTcPr().addNewShd().setFill("dce6f1");

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            XWPFParagraph paragrafo = cell.getParagraphs().get(0);
            paragrafo.setSpacingBetween(1);

            dados.get(i).setParagraphText(paragrafo);
        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable mergeCells(XWPFTable tabela, List<MergePosition> cellsToMerge) {
        tabela = mergeCellsHorizontal(tabela, cellsToMerge);
        tabela = mergeCellsVertical(tabela, cellsToMerge);
        return tabela;

    }

    private void addListaNumerada(List<TextoFormatado> listItens) {

        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
        //Next we set the AbstractNumId. This requires care.
        //Since we are in a new document we can start numbering from 0.
        //But if we have an existing document, we must determine the next free number first.
        cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));

        /* Bullet list
          CTLvl cTLvl = cTAbstractNum.addNewLvl();
          cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
          cTLvl.addNewLvlText().setVal("•");
        */

        ///* Decimal list
        CTLvl cTLvl = cTAbstractNum.addNewLvl();
        cTLvl.addNewNumFmt().setVal(STNumberFormat.DECIMAL);
        cTLvl.addNewPPr();
        CTInd ind = cTLvl.getPPr().addNewInd(); //Set the indent

        ind.setHanging(BigInteger.valueOf(360 * 2));
        ind.setLeft(BigInteger.valueOf(360 * 6));
        cTLvl.addNewLvlText().setVal("%1.");
        cTLvl.addNewStart().setVal(BigInteger.valueOf(1));
        //*/

        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

        XWPFNumbering numbering = document.createNumbering();

        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

        BigInteger numID = numbering.addNum(abstractNumID);

        for (TextoFormatado item : listItens) {
            item.setListNumId(numID);
            addParagrafo(item);
        }
    }

    private XWPFTable mergeCellsHorizontal(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTHMerge hMerge = CTHMerge.Factory.newInstance();
            hMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setHMerge(hMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeHorizontal().size(); j++) {
                CTHMerge hToMerge = CTHMerge.Factory.newInstance();
                hToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeHorizontal().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeHorizontal().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setHMerge(hToMerge);
            }
        }

        return tabela;
    }

    private XWPFTable mergeCellsVertical(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTVMerge vMerge = CTVMerge.Factory.newInstance();
            vMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setVMerge(vMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeVertical().size(); j++) {
                CTVMerge vToMerge = CTVMerge.Factory.newInstance();
                vToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeVertical().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeVertical().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setVMerge(vToMerge);
            }
        }

        return tabela;
    }

    private void addBreak() {
        this.document.createParagraph();//.createRun().addBreak();
    }

    private void addPageBreak() {
        XWPFParagraph paragraph = this.document.createParagraph();
        paragraph.setPageBreak(true);
    }

    public void addSaudacao() {
//        if(secaoFiscalizadoraContas != null && Character.isDigit(secaoFiscalizadoraContas.charAt(0))){
//            return "Senhor(a) Diretor(a) da "+ getDescricaoArea() + ",";
//        }
//        else{
//            return "Senhor(a) Diretor(a) da " + getDescricaoArea() + ",";
//        }
        addParagrafo(new TextoFormatado(tabelasProtocolo.getSaudacao(), formatacaoFactory.getBold(12)));
    }

    private void addTabelaIegm() {

//        NotaIegm notaIegm2016 = this.notasIegm.get(2016) != null ? this.notasIegm.get(2016) : new NotaIegm();
//        NotaIegm notaIegm2017 = this.notasIegm.get(2017) != null ? this.notasIegm.get(2017) : new NotaIegm();
        //NotaIegm notaIegm2018 = this.notasIegm.get(2018) != null ? this.notasIegm.get(2018) : new NotaIegm();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"25%", "25%", "25%",
                "25%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("EXERCÍCIOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado((exercicio - 3) + "", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado((exercicio - 2) + "", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado((exercicio - 1) + "", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("IEG-M", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIegm(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("i-Planejamento", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIPlanejamento(),
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIPlanejamento(),
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIPlanejamento(),
                formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("i-Fiscal", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIFiscal(),
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIFiscal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIFiscal(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("i-Educ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIEduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIEduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIEduc(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("i-Saúde", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaISaude(),
                formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaISaude(), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaISaude(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("i-Amb", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIAmb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIAmb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIAmb(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("i-Cidade", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaICidade(),
                formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaICidade(), formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaICidade(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("i-Gov-TI", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIGov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIGov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIGov(), formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void addTabelaDescricaoFonteDado() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"25%", "25%", "25%", "25%"},
                true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("DESCRIÇÃO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("FONTE/DATA", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DADOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("ANO DE REFERÊNCIA", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("POPULAÇÃO", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Site IBGE-Cidades", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(municipioIegmCodigoIbge.getPopulacao() + " habitantes",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("2021", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Densidade demográfica", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("Site IBGE-Cidades", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("ARRECADAÇÃO MUNICIPAL", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("Sistema Audesp (dd mmm. aaaa)", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespResultadoExecucaoOrcamentariaExcercicioAnt.get("vRecArrec"),
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("RCL", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("Sistema Audesp (dd mmm. aaaa)", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Extensão territorial", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("Município", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Atividade econômica predominante", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("Audesp", formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaPareceres() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "30%", "50%"},
                true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Exercícios", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Processos", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Pareceres", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getExercicio().toString(),
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getTc(), formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getParecer(),
                formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getExercicio().toString(),
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getTc(), formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getParecer(),
                formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getExercicio().toString(),
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getTc(), formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getParecer(),
                formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }

    private void addTabelaExecucaoOrcamentaria() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"60%", "25%", "15%"},
                true);

        Map<String, String> audespResultado = this.audespResultadoExecucaoOrcamentaria;


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Valores", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("(+) RECEITAS REALIZADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha2.add(new TextoFormatado(audespResultado.get("vSubTotRecRealPM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("(-) DESPESAS EMPENHADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado(audespResultado.get("despesaEmpenhadaTotal"),
                formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("(-) REPASSES DE DUODÉCIMOS À CÂMARA",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado(audespResultado.get("vRepDuodCM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha5 = new ArrayList<>();
        cabecalhoLinha5.add(new TextoFormatado("(+) DEVOLUÇÃO DE DUODÉCIMOS DA CÂMARA",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado(audespResultado.get("vDevDuod"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha6 = new ArrayList<>();
        cabecalhoLinha6.add(new TextoFormatado("(-) TRANSFERÊNCIAS FINANCEIRAS À ADMINISTRAÇÃO INDIRETA",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado(audespResultado.get("vTransfFinAdmIndExec"),
                formatacaoFactory.getRight(9)));


        List<TextoFormatado> cabecalhoLinha7 = new ArrayList<>();
        cabecalhoLinha7.add(new TextoFormatado("(+ ou -) AJUSTES DA FISCALIZAÇÃO", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha8 = new ArrayList<>();
        cabecalhoLinha8.add(new TextoFormatado("RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBold(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("resultadoExecucaoOrcamentaria"),
                formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("percentualExecucaoOrcamentaria"),
                formatacaoFactory.getBoldRight(9)));

        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);
        dados.add(cabecalhoLinha5);
        dados.add(cabecalhoLinha6);
        dados.add(cabecalhoLinha7);
        dados.add(cabecalhoLinha8);

        addTabela(dados, formatacaoTabela);
    }

    private void addAnaliseFontesDocumentais() {
        List<TextoFormatado> itensLista = new ArrayList<>();
        itensLista.add(new TextoFormatado("Indicadores finalísticos componentes do IEG-M – Índice de" +
                " Efetividade da Gestão Municipal;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Ações fiscalizatórias desenvolvidas através da seletividade " +
                "(contratos e repasses) e da fiscalização ordenada; ", formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Prestações de contas mensais do exercício em exame, encaminhadas pela " +
                "Chefia do Poder Executivo;", formatacaoFactory.getFormatacao(12)));

        itensLista.add(new TextoFormatado("Resultado do acompanhamento simultâneo do Sistema Audesp, bem como " +
                "acesso aos dados, informações e análises " +
                "disponíveis no referido ambiente;", formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER", new Formatacao(12).red().justify().bgGray()));
        itensLista.add(new TextoFormatado("Análise das denúncias, representações e expedientes diversos; ",
                formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER", formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Relatórios de fiscalização(ões) ordenada(s);",
                formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER", formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Análise do planejamento orçamentário/financeiro (PPA, LDO e " +
                "LOA) e do planejamento setorial (Planos Municipais);", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Leitura analítica dos dois últimos relatórios de fiscalização " +
                "e respectivas decisões desta Corte, sobretudo no tocante a assuntos relevantes nas ressalvas, " +
                "advertências e recomendações;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das informações disponíveis nos demais sistemas do E. Tribunal " +
                "de Contas do Estado", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Outros assuntos relevantes obtidos em pesquisa aos sítios de" +
                " transparência dos Órgãos Fiscalizados ou outras fontes da rede mundial " +
                "de computadores.", formatacaoFactory.getFormatacao(12)));

        addListaNumerada(itensLista);
    }

    private void addTabelaObrasParalizadas() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "15%", "15%", "15%", "15%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("OBRAS PARALISADAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("TC ", formatacaoFactory.getBold(9))
                .concat("(se houver)", formatacaoFactory.getVermelho(12)));
        linha2.add(new TextoFormatado("Valor inicial do Contrato (R$)", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Valor total pago (R$)", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Contratada", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Data da paralisação", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Descrição da obra", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 3));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 4));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 5));

        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaComparativoLimiteLRF() {


        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "20%", "10%"},
                true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("QUADRO COMPARATIVO COM OS LIMITES DA LRF", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Valores", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("RECEITA CORRENTE LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(limiteLRFMap.get("vRCL"), formatacaoFactory.getBoldRight(9)));
        linha2.add(new TextoFormatado(limiteLRFMap.get("rclPerc"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DÍVIDA CONSOLIDADA LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(limiteLRFMap.get("vDivConsolidLiq"), formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(limiteLRFMap.get("vPercDivConsolidLiq"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Limite Legal - Artigos 3º e 4º. Resolução 40 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(limiteLRFMap.get("divConsolidLimite"), formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(limiteLRFMap.get("divConsolidPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(limiteLRFMap.get("excessoDividaConsolidada"),
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("CONCESSÕES DE GARANTIAS",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Montante",
                formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(limiteLRFMap.get("vConcGar"), formatacaoFactory.getRight(9))); //VER
        linha8.add(new TextoFormatado(limiteLRFMap.get("vConcGar"), formatacaoFactory.getRight(9))); //VER

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Limite Legal - Artigo 9º. Resolução 43 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(limiteLRFMap.get("concGarantiaLimite"), formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(limiteLRFMap.get("concGarantiaPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(limiteLRFMap.get("excessoConcessaoGarantias"),
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO - Exceto ARO",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(limiteLRFMap.get("vOpCred"), formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(limiteLRFMap.get("opCredPercentual"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Limite Legal - Artigo 7º, I. Resolução 43 do Senado ",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(limiteLRFMap.get("vLimOpCred"), formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(limiteLRFMap.get("opCreditoExcAroPercLimite"),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(limiteLRFMap.get("excessoOperacoesCredito"),
                formatacaoFactory.getFormatacao(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO (Exceto ARO) > DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("ANTECIPAÇÃO DE RECEITAS ORÇAMENTÁRIAS - ARO",
                formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("Limite Legal - Artigo 10. Resolução 43 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha21.add(new TextoFormatado(limiteLRFMap.get("opAntecipacaoAroLimite"), formatacaoFactory.getRight(9)));
        linha21.add(new TextoFormatado(limiteLRFMap.get("opAntecipacaoAroPercLimite"),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha22.add(new TextoFormatado(limiteLRFMap.get("excessoARO"), formatacaoFactory.getFormatacao(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("RECURSOS OBTIDOS COM A ALIENAÇÃO DE ATIVOS",
                formatacaoFactory.getBold(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha24 = new ArrayList<>();
        linha24.add(new TextoFormatado("Saldo do exercício anterior",
                formatacaoFactory.getBold(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha25 = new ArrayList<>();
        linha25.add(new TextoFormatado("Valor arrecadado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha26 = new ArrayList<>();
        linha26.add(new TextoFormatado("Valor aplicado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha27 = new ArrayList<>();
        linha27.add(new TextoFormatado("Saldo a Aplicar",
                formatacaoFactory.getBold(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
//        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);
        dados.add(linha24);
        dados.add(linha25);
        dados.add(linha26);
        dados.add(linha27);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(2, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(2, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(2, 2));

        MergePosition mergeH2 = new MergePosition(6, 0);
        mergeH2.addToMergeHorizontal(new MergePosition(6, 1));
        mergeH2.addToMergeHorizontal(new MergePosition(6, 2));

        MergePosition mergeH3 = new MergePosition(10, 0);
        mergeH3.addToMergeHorizontal(new MergePosition(10, 1));
        mergeH3.addToMergeHorizontal(new MergePosition(10, 2));

        MergePosition mergeH4 = new MergePosition(14, 0);
        mergeH4.addToMergeHorizontal(new MergePosition(14, 1));
        mergeH4.addToMergeHorizontal(new MergePosition(14, 2));

        MergePosition mergeH5 = new MergePosition(16, 1);
        mergeH5.addToMergeHorizontal(new MergePosition(16, 2));

        MergePosition mergeH6 = new MergePosition(17, 0);
        mergeH6.addToMergeHorizontal(new MergePosition(17, 1));
        mergeH6.addToMergeHorizontal(new MergePosition(17, 2));

        MergePosition mergeH7 = new MergePosition(21, 0);
        mergeH7.addToMergeHorizontal(new MergePosition(21, 1));
        mergeH7.addToMergeHorizontal(new MergePosition(21, 2));

        MergePosition mergeV1 = new MergePosition(21, 2);
        mergeV1.addToMergeVertical(new MergePosition(22, 2));
        mergeV1.addToMergeVertical(new MergePosition(23, 2));
        mergeV1.addToMergeVertical(new MergePosition(24, 2));
        mergeV1.addToMergeVertical(new MergePosition(25, 2));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeH6);
        mergePositions.add(mergeH7);
        mergePositions.add(mergeV1);
        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesaDePessoalPrimeiroQuadrimestre() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%",
                "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("" + (exercicio - 1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio - 1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio - 1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("% Permitido Legal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Gasto Informado",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Gastos Ajustados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receita Corrente Líquida",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RCL Ajustada",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("% Gasto Informado",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 4),
                formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4),
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("% Gasto Ajustado",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 4),
                formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4),
                formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));


        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesaDePessoalSegundoQuadrimestre() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%",
                "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("" + (exercicio - 1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio - 1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("% Permitido Legal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 8),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Gasto Informado",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Gastos Ajustados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8),
                formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receita Corrente Líquida",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RCL Ajustada",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4),
                formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8),
                formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("% Gasto Informado",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4),
                formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8),
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("% Gasto Ajustado",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8),
                formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12),
                formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4),
                formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8),
                formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));


        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaPublicidadeEmAnoEleitoral() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%",
                "20%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Publicidade em ano Eleitoral ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Semestres:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("1º e 2º quadr./" + (exercicio - 3), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("1º e 2º quadr./" + (exercicio - 2), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("1º e 2º quadr./" + (exercicio - 1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("até 15/08/" + (exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Despesas", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Média apurada nos períodos dos exercícios anteriores",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

//        List<TextoFormatado> linha5 = new ArrayList<>();
//        linha5.add(new TextoFormatado("DESPESAS DO EXERCÍCIO ", formatacaoFactory.getJustificado(9))
//                        .concat("INFERIORES", formatacaoFactory.getBold(9))
//                        .concat(" À MÉDIA EM:", formatacaoFactory.getJustificado(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
//        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 3));
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 4));

        MergePosition mergeH2 = new MergePosition(3, 0);
        mergeH2.addToMergeHorizontal(new MergePosition(3, 1));
        mergeH2.addToMergeHorizontal(new MergePosition(3, 2));
        mergeH2.addToMergeHorizontal(new MergePosition(3, 3));

//        MergePosition mergeH3 = new MergePosition(4, 0);
//        mergeH3.addToMergeHorizontal(new MergePosition(4, 1));
//        mergeH3.addToMergeHorizontal(new MergePosition(4, 2));
//        mergeH3.addToMergeHorizontal(new MergePosition(4, 3));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
//        mergePositions.add(mergeH3);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesasCfFundeb() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 212 da Constituição Federal:", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO TESOURO (mínimo 25%)",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO TESOURO (mínimo 25%)",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespEnsinoFundeb.get("vPercLiqEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA - RECURSO TESOURO (mínimo 25%)",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespEnsinoFundeb.get("vPercPagoEnsino"), formatacaoFactory.getRight(9)));

        List<List<TextoFormatado>> dados2 = new ArrayList<>();

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("FUNDEB:", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 90%)",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 90%)",
                formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 90%)",
                formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 70%)",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundebMagist"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 70%)",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundebMagist"),
                formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 70%)",
                formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundebMagist"),
                formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados2.add(linha5);
        dados2.add(linha6);
        dados2.add(linha7);
        dados2.add(linha8);
        dados2.add(linha9);
        dados2.add(linha10);
        dados2.add(linha11);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
        XWPFTable tabela2 = addTabela(dados2, formatacaoTabela);

    }

    private void addTabelaApuracaoDisponibilidadesDeCaixa() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Evolução da liquidez entre 30.04 e 31.12 (projetado) do exercício de:",
                formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("" + exercicio, formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Disponbilidade de Caixa em 30.04", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("(-) Saldo de Restos a Pagar em 30.04", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("(-) Empenhos Liquidados a Pagar em 30.04", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("(-) Saldo da Despesa Empenhada a Liquidar", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5b = new ArrayList<>();
        linha5b.add(new TextoFormatado("(-) Valores Restituíveis", formatacaoFactory.getFormatacao(9)));
        linha5b.add(new TextoFormatado("", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Equilíbrio em 30.04:", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("R$", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("(+) Saldo da Receita Prevista a Realizar", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("(-) Saldo da Despesa Autorizada a Empenhar",
                formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("(-) Saldo das Transferências Financeiras a Realizar",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Liquidez projetada em 31.12", formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado("R$", formatacaoFactory.getBold(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha5b);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaVagasEscolares() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "20%", "20%",
                "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("NÍVEL", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DEMANDA POR VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("OFERTA DE VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("RESULTADO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Ens. Infantil (Creche)", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ens. Infantil (Pré escola)", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ens. Fundamental (Anos Iniciais)", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Ens. Fundamental (Anos Finais)", formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaEmpenhadaLiquidadaPaga() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 77, III c/c § 4º do ADCT", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespSaude.get("vPercEmpSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespSaude.get("vPercLiqSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespSaude.get("vPercPagoSaude"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaProcessoDeContasAnuais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "70%", "25%"},
                false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Número:", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC-XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Interessado:",
                formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Objeto:",
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Procedência:",
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }


    private void addTabelaFOPeriodo() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"30%", "70%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Mês: xx", new Formatacao(9)));
        linha1.add(new TextoFormatado("Tema: xxxx", new Formatacao(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Fiscalização Ordenada nº",
                new Formatacao(9)));
        linha2.add(new TextoFormatado("XX / XXXX(ano).", new Formatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("TC e evento da juntada",
                new Formatacao(9)));
        linha3.add(new TextoFormatado("TC-XXXXXX.989.XX, evento XX.", new Formatacao(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Irregularidades verificadas:",
                new Formatacao(9).bgYellow()));
        linha4.add(new TextoFormatado("Relatar sucintamente as ocorrências", new Formatacao(9).bgGray().red()));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }

    private void addTabelaDoisUltimosExerciciosApreciados(ParecerPrefeitura parecerPrefeitura) {

        if (parecerPrefeitura == null) {
            addTabelaDoisUltimosExerciciosApreciados();
            return;
        }

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%",
                "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();

        linha1.add(new TextoFormatado("Exercício\n " + parecerPrefeitura.getExercicio(),
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC\n " + parecerPrefeitura.getTc(), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DOE\n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data do Trânsito em julgado\n " + parecerPrefeitura.getDataTransitoJulgadoString(),
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Recomendações:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(1, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


        mergePositions.add(mergeH1);


        mergeCells(tabela, mergePositions);
    }

    private void addTabelaDoisUltimosExerciciosApreciados() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%", "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();


        linha1.add(new TextoFormatado("Exercícios", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Processos", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Pareceres", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Principais itens que ensejaram o parecer desfavorável",
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("20xx:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("XXXXXX.989.XX¹", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Favorável / Favorável com ressalvas / Desfavorável",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("20xx:", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("XXXXXX.989.XX²", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("Favorável / Favorável com ressalvas / Desfavorável",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

//        List<MergePosition> mergePositions = new ArrayList<>();
//        MergePosition mergeH1 = new MergePosition(1, 0);
//        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
//        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
//        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


//        mergePositions.add(mergeH1);


//        mergeCells(tabela, mergePositions);

    }


    public ResponseEntity<Resource> download(Integer codigoIBGE, Integer exercicio, Integer quadrimestre)
            throws IOException, InvalidFormatException, Exception {

        this.document = new XWPFDocument();

        this.quadrimestre = quadrimestre;

        this.exercicio = exercicio;
        this.codigoIBGE = codigoIBGE;

        this.pareceresPrefeiturasList = this.parecerPrefeituraService.getParecerByCodigoIbge(codigoIBGE);
        this.audespEntidade = this.audespEntidadeService.getEntidade(codigoIBGE, exercicio);
//        this.apontamentosODS = apontamentosODSService.getApontamentosODS(codigoIBGE, exercicio);
        this.notasIegm = iegmService.getMapNotasByCodigoIbge(codigoIBGE, exercicio - 1);
        this.resultadoIegmAnoBaseAnterior = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio - 1);
        this.resultadoIegmAnoBaseRetrasado = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio - 2);
        this.resultadoIegmAnoBaseReRetrasado = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio - 3);
        this.responsavelPrefeitura = audespService.getResponsavelPrefeitura(codigoIBGE, exercicio, quadrimestre);
        this.responsavelSubstitutoPrefeitura = audespService.getResponsavelSubstitutoPrefeitura(codigoIBGE, exercicio
                , quadrimestre);
        this.municipioIegmCodigoIbge = tcespBiService.getMunicipioByCodigoIbgeExercicio(codigoIBGE, exercicio-1);
        this.audespResultadoExecucaoOrcamentaria = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestralFormatado(codigoIBGE, exercicio, 4 * quadrimestre);
        this.audespResultadoExecucaoOrcamentariaExcercicioAnt = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestralFormatado(codigoIBGE, exercicio - 1, 12);
        this.tabelasProtocolo = tabelasService.getTabelasProtocoloPrefeituraByCodigoIbge(codigoIBGE, exercicio);
//        this.anexo14AMap = this.demonstrativosRaeeService.getAnexo14A(audespEntidade, codigoIBGE, exercicio, 12);
        this.audespEnsinoFundeb = audespEnsinoService.getAudespEnsinoFundeb(codigoIBGE, exercicio, 4 * quadrimestre);
        this.audespSaude = audespSaudeService.getAudespSaude(codigoIBGE, exercicio, 4 * quadrimestre);
//        this.aplicacoesEmSaude = audespSaudeService.getAplicacoesEmSaudeFormatado(codigoIBGE, exercicio, 12);
        this.audespDespesaPessoalMap =
                this.audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbgeFechamentoFormatado(codigoIBGE,
                        exercicio, 50);
//        this.quadroGeralEnsinoMap = this.audespEnsinoService.getQuadroGeralEnsinoFormatado(codigoIBGE,exercicio,12);
//        this.apontamentoFOMap = this.apontamentosFOService.getApontamentosFO(codigoIBGE,exercicio);
//        this.audespDividaAtivaMap = this.audespDividaAtivaService.getDividaAtivaFormatado(codigoIBGE,exercicio,12);
//        this.audespFase3QuadroDePessoalMap = this.audespFase3Service.getQuadroDePessoal(audespEntidade
//        .getEntidadeId(), exercicio);
//        this.valorInvestimentoMunicipioMap = this.audespBiService.getValorInvestimentoMunicipio(codigoIBGE,
//        exercicio, 12);
//        this.rclMunicipioDevedoresMap = this.tcespBiService.getRCLMunicipioFormatado(codigoIBGE, exercicio);
        Integer quantidadeAlertasDesajusteExecucaoOrcamentaria =
                audespAlertasService.getAlertasDesajusteExecucaoOrcamentaria(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasEducacao = audespAlertasService.getAlertasEducacao(codigoIBGE, exercicio,
                4 * quadrimestre);
        Integer quantidadeAlertasSaude = audespAlertasService.getAlertasSaude(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasLRFa59p1i1 = audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12,
                new String[]{"GF15"});
        Integer quantidadeAlertasLRFa59p1i2 = audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12,
                new String[]{"GF27"});
        Integer quantidadeAlertasLRFa59p1i5Cobertura2UltimosQuadrimestres =
                audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"GF37"});
        Integer quantidadeAlertasLRFa59p1i5PessoalUltimos180Mandato =
                audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"GF36"});
        Integer quantidadeAlertasLRFa59p1i5AplicacaoEnsino = audespAlertasService
                .getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"AE03", "AE05", "AE06"});
        Integer quantidadeAlertasLRFa59p1i5AplicacaoSaude = audespAlertasService
                .getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"AS03"});
        Integer quantidadeAlertasDespesaPessoal = audespAlertasService.getAlertasDespesaComPessoal(codigoIBGE,
                exercicio, 12);
        this.audespResultadoExecucaoOrcamentariaMap = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaUltimosTresExercicios(this.codigoIBGE, exercicio,
                        4 * quadrimestre);
        this.limiteLRFMap = audespLimiteLrfService
                .getAudespResultadoInfluenciaOrcamentarioFinanceiro(codigoIBGE, exercicio, 4 * quadrimestre);


        String quadrimestreTitulo = getQuadrimestreTitulo(quadrimestre);
        String quadrimestreTituloAno = getQuadrimestreTituloComAno(quadrimestre);


        // margin
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        CTPageMar pageMar = sectPr.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(1700L));
        pageMar.setRight(BigInteger.valueOf(1700L));
        pageMar.setBottom(BigInteger.valueOf(1700L));

        CTBody body = document.getDocument().getBody();

        if (!body.isSetSectPr()) {
            body.addNewSectPr();
        }
        CTSectPr section = body.getSectPr();

        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }
        CTPageSz pageSize = section.getPgSz();

        pageSize.setW(BigInteger.valueOf(595 * 20));
        pageSize.setH(BigInteger.valueOf(842 * 20));

        createDocumentStyles();

        getHeader();

        addParagrafo(new TextoFormatado("RELATÓRIO DE FISCALIZAÇÃO" + quadrimestreTitulo + " \nPREFEITURA MUNICIPAL ",
                formatacaoFactory.getBoldCenter(12)));
        addBreak();
        addBreak();

        addTabelaDadosIniciais();

        addBreak();
        addSaudacao();
        addBreak();
        addBreak();

        addParagrafo(addTab().concat("O presente relatório trata do acompanhamento periódico das Contas da " +
                                "Prefeitura Municipal em tela, selecionada pelo sistema ",
                        formatacaoFactory.getJustificado(12))
                .concat("Áquila", new Formatacao(12).justify().italic())
                .concat(" deste Tribunal de Contas, com base em critérios específicos previamente " +
                        "estabelecidos, para ser fiscalizada (  ", new Formatacao(12).justify())
                .concat("in loco ", new Formatacao(12).justify().italic())
                .concat("e/ou", new Formatacao(12).justify().red())
                .concat(" remotamente), neste período, em conformidade com a Ordem de Serviço SDG nº 01/2022.",
                        new Formatacao(12).justify())

        );

        addBreak();
        addParagrafo(addTab().concat("Em atendimento ao TC-A-30973/026/00, registramos a notificação do(s) Sr.(s). ",
                        formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", responsável(is) pelas contas em exame.", formatacaoFactory.getJustificado(12))
        );


        addBreak();

        addParagrafo(new TextoFormatado("A partir do diagnóstico preliminar apresentado abaixo e das " +
                "informações disponíveis nos Sistemas Informatizados desta Corte de Contas, a Fiscalização " +
                "planejou a execução de seus trabalhos de análises de conformidade e de resultado operacional " +
                "do período, destacando-se a análise das seguintes fontes documentais:",
                formatacaoFactory.getJustificado(12)));
//        addTabelaIegm();
//        addTextoStatusFaseIEGM();
//
//        addBreak();

//        addParagrafo(addTab().concat("SÓ INSERIR NO QUADRO APÓS A VALIDAÇÃO DO IEG-M PELA FISCALIZAÇÃO.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO DEVE APENAS MENCIONAR OS ÍNDICES EM RELATÓRIO, SENDO " +
//                "VEDADA A DIVULGAÇÃO AOS JURISDICIONADOS DOS ÍNDICES AINDA NÃO CHANCELADOS E TORNADOS PÚBLICOS PELA" +
//                " DIREÇÃO DA CASA." ,
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));
//
//        addBreak();
//
//        addParagrafo(new TextoFormatado("O IEG-M INSERIDO NO EXERCÍCIO ANTERIOR SERÁ AQUELE APURADO APÓS A " +
//                "VERIFICAÇÃO/VALIDAÇÃO DA FISCALIZAÇÃO.\n CASO NÃO TENHA SIDO CONCLUÍDA A VALIDAÇÃO, CONSTAR O ITEM" +
//                " COMO PREJUDICADO E INSERIR A OBSERVAÇÃO CONSTANTE NA ALTERNATIVA DO QUADRO ACIMA.\n" +
//                "\n",
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));
//
//        addBreak();
//        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO PODE APENAS MENCIONAR OS ÍNDICES EM RELATÓRIO, SENDO " +
//                "VEDADA A DIVULGAÇÃO AOS JURISDICIONADOS DOS ÍNDICES AINDA NÃO CHANCELADOS E TORNADOS PÚBLICOS " +
//                "PELA DIREÇÃO DA CASA.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("A Prefeitura analisada obteve, nos 03 (três) últimos exercícios" +
//                        " apreciados, os seguintes ",
//                formatacaoFactory.getJustificado(12))
//                .concat("PARECERES", formatacaoFactory.getBold(12))
//                .concat(" na apreciação de suas contas:", formatacaoFactory.getJustificado(12))
//        );
//
//        addTabelaPareceres();

        addBreak();

        addAnaliseFontesDocumentais();

        addBreak();

        addParagrafo(new TextoFormatado("No relatório periódico, caberá à Fiscalização planejar seus trabalhos" +
                " de forma seletiva, tendo em vista o regramento previsto no art. 7º da Resolução nº 04/2017. Nesse" +
                " sentido, importante relembrar que foram estabelecidos critérios de seleção, possibilitando " +
                "otimizar o tempo de fiscalização, que pode ser realizada remotamente e/ou in loco, racionalizando " +
                "os procedimentos, focando na análise do planejamento e do resultado da execução das políticas" +
                " públicas.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA A: ASPECTOS PRELIMINARES DE INTERESSE",
                formatacaoFactory.getBold(12)), heading1);

        addBreak();
        addSecao(new TextoFormatado("A.1. ÍNDICES E INDICADORES DA GESTÃO MUNICIPAL", formatacaoFactory.getBold(12)),
                heading2);
        addBreak();

        addParagrafo(addTab().concat("Consignamos as informações preliminares sobre o Município que auxiliaram " +
                "no planejamento da presente fiscalização. ", new Formatacao(12).justify()));

        addTabelaDescricaoFonteDado();

        addBreak();
        addParagrafo(addTab().concat("O Município possui, ainda, a seguinte série histórica de classificação" +
                " no Índice de Efetividade da Gestão Municipal (IEG-M):", new Formatacao(12).justify())
        );
        addTabelaIegm();
        addTextoStatusFaseIEGM();

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("O IEG-M INSERIDO NO EXERCÍCIO SERÁ AQUELE APURADO APÓS A " +
                "VERIFICAÇÃO/VALIDAÇÃO DA FISCALIZAÇÃO.",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
        );
        addParagrafo(new TextoFormatado("A Fiscalização deve apenas mencionar os índices em relatório, sendo" +
                " vedada a divulgação aos jurisdicionados dos índices ainda não chancelados e tornados públicos" +
                " pela direção da casa.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addSecao(new TextoFormatado("A.2. HISTÓRICO DE EXERCÍCIOS ANTERIORES DA GESTÃO MUNICIPAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(addTab().concat("Demonstramos a síntese do apurado pela Fiscalização nos 2 " +
                "(dois) últimos exercícios: ", new Formatacao(12).justify())
        );

        addTabelaSinteseDoapurado();

        addBreak();
        addBreak();

        addParagrafo(addTab().concat("A Prefeitura analisada obteve, nos dois últimos exercícios apreciados, os " +
                "seguintes Pareceres na apreciação de suas contas: ", new Formatacao(12).justify())
        );

        addBreak();
        addTabelaDoisUltimosExerciciosApreciados();
        addParagrafo(new TextoFormatado("1.  (Não) Transitado em julgado em", new Formatacao(9)));
        addParagrafo(new TextoFormatado("2.  (Não) Transitado em julgado em", new Formatacao(9)));


        addBreak();

        addBreak();
        addSecao(new TextoFormatado("A.3. DENÚNCIAS/REPRESENTAÇÕES/EXPEDIENTES",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": NA INEXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(addTab().concat("Não chegou ao nosso conhecimento a formalização de denúncias," +
                " representações ou expedientes.", new Formatacao(12).justify())
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": NA EXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES NÃO PASSÍVEIS DE" +
                                " VERIFICAÇÃO NO PERÍODO EM ANÁLISE. ",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();

        addParagrafo(addTab().concat("As denúncias / representações / expedientes serão tratados no " +
                "fechamento do exercício em exame, tendo em vista que, no momento, não concluímos a análise" +
                " da matéria.", new Formatacao(12).justify())
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": NA EXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES ",
                        new Formatacao(12).justify().red().bgYellow().bold())
                .concat("EM QUE SEJA OBRIGATÓRIA E/OU NECESSÁRIA",
                        new Formatacao(12).justify().red().bgYellow().bold())
                .concat(" A VERIFICAÇÃO NO PERÍODO EM ANÁLISE (QUADRIMESTRE OU SEMESTRE).",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(addTab().concat("Está referenciado ", new Formatacao(12).justify())
                .concat("OU", new Formatacao(12).justify().red().bgYellow().bold())
                .concat(" Estão referenciados ao presente processo de contas anuais, o(s) " +
                        "seguinte(s) protocolado(s):", new Formatacao(12).justify())
        );
        addTabelaProcessoDeContasAnuais();

        addParagrafo(addTab().concat("O(s) assunto(s) em tela foi(ram) tratado(s) no item(ns) ",
                        new Formatacao(12).justify())
                .concat("xx", new Formatacao(12).justify().red().bgGray())
                .concat(" deste relatório.", new Formatacao(12).justify())
        );


        addBreak();
        addSecao(new TextoFormatado("A.4. FISCALIZAÇÕES ORDENADAS DO PERÍODO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(addTab().concat("No período em exame, não foram realizadas fiscalizações ordenadas: ",
                new Formatacao(12).justify())
        );

        addParagrafo(new TextoFormatado("OU",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(addTab().concat("No período em exame, foram realizadas as seguintes fiscalizações ordenadas: ",
                new Formatacao(12).justify())
        );
        addBreak();
        addTabelaFOPeriodo();
        addBreak();
        addTabelaFOPeriodo();

        addParagrafo(new TextoFormatado("(SE POSSÍVEL, FAZER CONEXÃO COM EVENTUAIS FALHAS ENCONTRADAS NA PERSPECTIVA " +
                "“B”).",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addSecao(new TextoFormatado("A.5. FISCALIZAÇÃO DA ATUAÇÃO DO CONTROLE INTERNO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(addTab()
                .concat("No período analisado ", new Formatacao(12).justify())
                .concat("(não)", new Formatacao(12).justify())
                .concat(" foram encontradas ", new Formatacao(12).justify())
                .concat("(as seguintes)", new Formatacao(12).justify())
                .concat(" ocorrências dignas de nota.", new Formatacao(12).justify())
        );
        addBreak();
        addParagrafo(new TextoFormatado("AQUI SERÃO TRAZIDAS OCORRÊNCIAS RELEVANTES SOBRE O CONTROLE INTERNO E SUAS " +
                "ATRIBUIÇÕES.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("BUSCAR AFERIR SE O CONTROLE INTERNO TEM EXERCIDO DE MANEIRA EFETIVA SUAS " +
                "ATRIBUIÇÕES NO PERÍODO. ",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("ABORDAGENS POSSÍVEIS: ",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("- REGULAMENTAÇÃO DA MATÉRIA, NA INVESTIDURA NO CARGO, ",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("- RELACIONAMENTO COM O CONTROLE EXTERNO, ",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("- ATUANTE NO ACOMPANHAMENTO DA ELABORAÇÃO E EXECUÇÃO DAS POLÍTICAS" +
                " PÚBLICAS (COM REGISTRO DA QUALIDADE DO PLANEJAMENTO),",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(new TextoFormatado("- EMISSÃO PERIÓDICA DE RELATÓRIOS E ANÁLISES REALIZADAS, COERENTES COM A " +
                "REALIDADE DO ÓRGÃO,",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(new TextoFormatado("- SEMPRE QUE POSSÍVEL CORRELACIONANDO ÀS RECOMENDAÇÕES DO TCESP E AOS " +
                "APONTAMENTOS TRAZIDOS AO RELATÓRIO.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA B: FISCALIZAÇÃO OPERACIONAL DE PLANEJAMENTO E EXECUÇÃO " +
                "DAS POLÍTICAS PÚBLICAS ", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addParagrafo(addTab().concat("Sob o pressuposto da amostragem, não constatamos ocorrências dignas de " +
                        "nota no que se refere a esta perspectiva. ", new Formatacao(12).justify())
                .concat("(Neste caso, os itens com as dimensões abaixo podem ser apagados)",
                        new Formatacao(12).justify().bgYellow().red())
        );
        addBreak();
        addParagrafo(new TextoFormatado("OU",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(addTab().concat("Na fiscalização operacional realizada no período, observamos ocorrências " +
                        "dignas de nota nos itens abaixo descritos: ", new Formatacao(12).justify())
                .concat("(Manter no relatório somente os itens com as dimensões que apresentaram ocorrências, " +
                        "as demais podem ser apagadas)", new Formatacao(12).justify().bgYellow().red())
        );

        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES:",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
        );
        addBreak();

        addParagrafo(new TextoFormatado("CASO ALGUM APONTAMENTO TRATADO NOS ITENS A SEGUIR DESTA PERSPECTIVA" +
                " IMPLIQUE EM ALTERAÇÕES NOS CÁLCULOS DE ATENDIMENTO AOS LIMITES DA GESTÃO FISCAL ou  PROVOQUE " +
                "GLOSAS NA APLICAÇÃO DO ENSINO OU DA SAÚDE, TRAZIDOS PELO RELATÓRIO AUTOMÁTICO DO AUDESP, LEVAR " +
                "TAIS GLOSAS E/OU ALTERAÇÕES PARA A PERSPECTIVA “C”",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();
        addParagrafo(new TextoFormatado("O FOCO PRINCIPAL A SER OBSERVADO NOS ITENS ABAIXO (DESTA PERSPECTIVA) É" +
                " A APRECIAÇÃO DO PLANEJAMENTO E DOS RESULTADOS DAS POLÍTICAS PÚBLICAS, TAIS COMO:",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();
        addParagrafo(new TextoFormatado("- O PLANEJAMENTO DE POLÍTICAS PÚBLICAS (LOA, LDO e PPA) CONTEMPLA AÇÕES" +
                " E PROGRAMAS CAPAZES DE ATENDER ÀS DEMANDAS REAIS DA POPULAÇÃO? (VERIFICAR TAMBÉM AQUELAS DEMANDAS " +
                "EVENTUALMENTE IDENTIFICADAS EM FISCALIZAÇÕES ANTERIORES OU PELO CONTROLE INTERNO).",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();

        addParagrafo(new TextoFormatado("- HÁ PARTICIPAÇÃO POPULAR, DOS CONSELHOS MUNICIPAIS, DO CONTROLE" +
                " INTERNO, DA SOCIEDADE CIVIL ORGANIZADA, NA CONSTRUÇÃO DO PLANEJAMENTO E NO ACOMPANHAMENTO DA" +
                " EXECUÇÃO DAS POLÍTICAS PÚBLICAS?",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();

        addParagrafo(new TextoFormatado("- A FALTA DE PLANEJAMENTO E EXECUÇÃO ADEQUADA AFETARAM OS" +
                " ÍNDICES DE EFETIVIDADE DO MUNICÍPIO (IEG-M) – VERIFICAR EVENTUAL INVOLUÇÃO NAS DIMENSÕES DO ÍNDICE?",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();

        addParagrafo(new TextoFormatado("- A EXECUÇÃO DAS POLÍTICAS PÚBLICAS (DESPESAS CONTRATADAS E/OU DIRETAS)" +
                " OBEDECE AO PLANEJAMENTO?",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();

        addParagrafo(new TextoFormatado("- EVENTUAIS IRREGULARIDADES (FALHAS FORMAIS E DESVIOS) NA EXECUÇÃO " +
                "DAS POLÍTICAS PÚBLICAS (DESPESAS CONTRATADAS E/OU DIRETAS)  PREJUDICARAM O ATINGIMENTO DE " +
                "RESULTADOS FAVORÁVEIS À POPULAÇÃO? (SE FOR O CASO, ALÉM DO APONTAMENTO DE RESULTADO, PROPOR " +
                "SELETIVIDADE DE CONTRATOS)?\nExemplos: vagas em creches / escolas / pronto atendimento / " +
                "hospitais; conservação de via públicas; manutenção de próprios públicos; sinalizações de " +
                "trânsito; tratamento de esgoto; abastecimento de água; coleta de esgoto; redução do " +
                "analfabetismo; atividades culturais e esportivas; preservação/recuperação do meio " +
                "ambiente e mananciais de água; criação/manutenção de áreas verdes urbanas; índices " +
                "de ensino e saúde x qualidade; educação alimentar; amparo a pessoas em situação de " +
                "vulnerabilidade; violência  urbana; etc",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(new TextoFormatado("- AS FALHAS NO PLANEJAMENTO E NA EXECUÇÃO DE POLÍTICAS PÚBLICAS SÃO" +
                " AGRAVADAS SE HOUVER DESATENDIMENTO A RECOMENDAÇÕES/DETERMINAÇÃOES DO TRIBUNAL DE CONTAS.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(new TextoFormatado("- SE CABÍVEL, DENTRO DE CADA ITEM A SEGUIR, FAZER A CORRELAÇÃO DAS FALHAS " +
                "OPERACIONAIS ENCONTRADAS COM OS “ODS” QUE PODE COMPROMETER. \n" +
                "CORRELAÇÃO “IEG-M/ODS” DISPONÍVEL EM: ",
                new Formatacao(12).justify().red().bgYellow().bold())
                .concat("https://painel.tce.sp.gov.br/pentaho/api" +
                                "/repos/%3Apublic%3Aieg_m%3Aiegm.wcdf/generatedContent?userid=anony&password=zero" +
                                "#anchor-indices",
                        new Formatacao(12).justify().blue().bgYellow().bold())
        );


        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1. PLANEJAMENTO DAS POLÍTICAS PÚBLICAS (i-Plan/IEG-M)",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Orientações - ver Apêndice",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.2. ADEQUAÇÃO FISCAL DAS POLÍTICAS PÚBLICAS (i-Fiscal/IEG-M)",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Orientações - ver Apêndice",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.3. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DO ENSINO (i-Educ/IEG-M)",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Orientações - ver Apêndice",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.4. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DO SAÚDE (i-Saúde/IEG-M)",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Orientações - ver Apêndice",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.5. EXECUÇÃO DAS POLÍTICAS PÚBLICAS AMBIENTAIS (i-Amb/IEG-M)",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Orientações - ver Apêndice",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.6. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DE INFRAESTRUTURA (i-Cidade/IEG-M)",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Orientações - ver Apêndice",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.7. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DE TECNOLOGIA DA INFORMAÇÃO (i-Gov TI/IEG-M)",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Orientações - ver Apêndice",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA C: GESTÃO FISCAL DO PERÍODO", formatacaoFactory.getBold(12)),
                heading1);

        addBreak();
        addSecao(new TextoFormatado("C.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS – GESTÃO FISCAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE 1",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": FISCALIZAÇÃO DO 1º QUADRIMESTRE - SE NÃO HOUVER ANÁLISES AUTOMÁTICAS DO " +
                        "SISTEMA AUDESP.", new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(addTab().concat("A presente fiscalização periódica foi concluída antes do encerramento " +
                "do 1º quadrimestre, motivo pelo qual deixamos de realizar análises da gestão " +
                "fiscal do período. ", new Formatacao(12).justify())
        );
        addParagrafo(new TextoFormatado("OU",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(addTab().concat("A presente fiscalização periódica foi concluída antes do encerramento " +
                "do 1º quadrimestre, no entanto, identificamos ocorrências dignas de notas no(s)" +
                " subitem(ns) tratado(s) a seguir.", new Formatacao(12).justify())
        );
        addParagrafo(addTab().concat("Ressaltamos que a Administração Municipal deve atentar aos alertas " +
                "automáticos eventualmente emitidos pelo Sistema Audesp, no curso do" +
                " exercício.", new Formatacao(12).justify())
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE 2",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": FISCALIZAÇÃO DO 1º / 2º QUADRIMESTRE e 1º SEMESTRE: SE JÁ HOUVER ANÁLISES" +
                        " AUTOMÁTICAS DO SISTEMA AUDESP.", new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(addTab().concat("Face ao contido no art. 1º, § 1º, da Lei Complementar Federal nº 101, de " +
                        "04 de maio de 2000 (Lei de Responsabilidade Fiscal), o qual estabelece os pressupostos da" +
                        " responsabilidade da gestão fiscal, informamos que não foram identificadas, neste momento," +
                        " ocorrências dignas de notas", new Formatacao(12).justify())
                .concat(", a par dos alertas já emitidos automaticamente pelo Sistema Audesp (se houver). ",
                        new Formatacao(12).justify().red())
        );
        addParagrafo(new TextoFormatado("OU",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(addTab().concat("Face ao contido no art. 1º, § 1º, da Lei Complementar Federal nº 101, de " +
                "04 de maio de 2000 (Lei de Responsabilidade Fiscal), o qual estabelece os pressupostos da " +
                "responsabilidade da gestão fiscal, apuramos as ocorrências dignas de nota no(s) " +
                "subitem(ns) tratado(s) a seguir.", new Formatacao(12).justify())
        );
        addParagrafo(addTab().concat("Registramos que as análises das informações prestadas pelo Órgão ao " +
                "Sistema Audesp estão consignadas no relatório automático, cujo teor reproduzimos no Anexo " +
                "deste relatório. ", new Formatacao(12).justify())
        );
        addParagrafo(addTab().concat("Ressaltamos que a Administração Municipal deve atentar aos alertas" +
                " automáticos eventualmente emitidos pelo Sistema Audesp, no curso " +
                "do exercício.", new Formatacao(12).justify())
        );
        addBreak();
        addSecao(new TextoFormatado("C.1.1. RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA NO PERÍODO",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();

        addParagrafo(new TextoFormatado("OBS. 1",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": SÓ INSERIR ESSE SUBITEM, SE HOUVER RECÁLCULOS (ACRÉSCIMOS/EXCLUSÕES) A SEREM APRESENTADOS " +
                                "PELA FISCALIZAÇÃO.  DO CONTRÁRIO, APAGAR ESTE SUBITEM E RENUMERAR OS DEMAIS.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();

        addParagrafo(new TextoFormatado("OBS. 2",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": CONSIDERAR OS DADOS ISOLADOS DA PREFEITURA, CONFORME APURADO PELO SISTEMA AUDESP, NÃO " +
                                "DEVENDO SER INCLUÍDAS ADMINISTRAÇÃO INDIRETA, FUNDOS DE PREVIDÊNCIA ETC. NAS " +
                                "RECEITAS REALIZADAS E " +
                                "DESPESAS EMPENHADAS.\n" +
                                "DEPENDE DE SINAL (+ OU -) APENAS A LINHA DE “AJUSTES DA FISCALIZAÇÃO”.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();

        addParagrafo(new TextoFormatado("OBS. 3",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": PROCURAR TRAZER CONEXÃO DE RESULTADO COM A \"PERSPECTIVA B\".",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();
        addParagrafo(addTab().concat("Apuramos inconsistências/acréscimos/reduções nos números apresentados pela " +
                        "Origem ao Sistema AUDESP, que influenciam nos resultados conforme abaixo demonstrado:",
                new Formatacao(12).justify())
        );

        addTabelaExecucaoOrcamentaria();
        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp",
                formatacaoFactory.getBoldJustificado(10))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(10)));


        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": DEFICIT PELA DESPESA EMPENHADA, APURADO NO QUADRO ANTERIOR, INFORMAR O" +
                                " RESULTADO PELA DESPESA LIQUIDADA, CONFORME SEGUE.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(addTab().concat("Consideradas as despesas liquidadas, constata-se um (deficit/superavit) de R$ "
                        , new Formatacao(12).justify())
                .concat("XX.XXX", new Formatacao(12).justify().red().bgGray())
                .concat(", correspondente a ", new Formatacao(12).justify())
                .concat("XX", new Formatacao(12).justify().red().bgGray())
                .concat("%.", new Formatacao(12).justify())
        );
        addBreak();
        addParagrafo(new TextoFormatado("(RELATAR POSSÍVEIS CONEXÕES COM A \"PERSPECTIVA B\")",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();
        addSecao(new TextoFormatado("C.1.2. ANÁLISE DOS LIMITES E CONDIÇÕES DA LEI DE RESPONSABILIDADE FISCAL",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("OBS. 1",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": SÓ INSERIR ESSE SUBITEM, SE HOUVER RECÁLCULOS (ACRÉSCIMOS/EXCLUSÕES) A" +
                        " SEREM APRESENTADOS PELA FISCALIZAÇÃO.  DO CONTRÁRIO, APAGAR ESTE SUBITEM E " +
                        "RENUMERAR OS DEMAIS.", new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(new TextoFormatado("OBS. 2",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": NT SDG Nº 141", new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat("- VERIFICAR APURAÇÃO DA RCL, EM ESPECIAL EM CASOS DE " +
                        "DESCUMPRIMENTO DOS LIMITES DA LRF.", new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();
        addParagrafo(addTab().concat("Apuramos inconsistências/acréscimos/reduções nos números apresentados" +
                " pela Origem ao Sistema AUDESP, que influenciam nos resultados conforme " +
                "abaixo demonstrado:", new Formatacao(12).justify())
        );

        addBreak();
        addTabelaComparativoLimiteLRF();
        addBreak();

        addParagrafo(addTab().concat("Verificamos o não atendimento aos limites estabelecidos pela Lei de" +
                        " Responsabilidade Fiscal, isso em decorrência do que segue: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("(RELATAR POSSÍVEIS CONEXÕES COM A \"PERSPECTIVA B\")",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("C.1.2.1. DESPESA DE PESSOAL",
                new Formatacao(12).bold()), heading4);
        addBreak();

        addParagrafo(new TextoFormatado("Obs.",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": SÓ INSERIR ESSE SUBITEM, SE HOUVER RECÁLCULOS (ACRÉSCIMOS/EXCLUSÕES) " +
                        "A SEREM APRESENTADOS PELA FISCALIZAÇÃO.  DO CONTRÁRIO, APAGAR ESTE SUBITEM E " +
                        "RENUMERAR OS DEMAIS.", new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();
        addParagrafo(addTab().concat("Apuramos inconsistências/acréscimos/reduções nos números apresentados" +
                " pela Origem ao Sistema AUDESP, que influenciam nos resultados conforme " +
                "abaixo demonstrado:", new Formatacao(12).justify())
        );

        addBreak();

        addParagrafo(new TextoFormatado("1º QUADRIMESTRE e 1º SEMESTRE",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addTabelaDespesaDePessoalPrimeiroQuadrimestre();
        addBreak();
        addParagrafo(new TextoFormatado("2º QUADRIMESTRE (se já houver a análise)",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addTabelaDespesaDePessoalSegundoQuadrimestre();
        addBreak();

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(" NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO QUANTO AO AJUSTE DO MÊS DE DEZEMBRO DO EXERCÍCIO ANTERIOR. " +
                "SEMPRE BUSCAR SEGUIR O APURADO PELA FISCALIZAÇÃO ANTERIOR. ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("IMPORTANTE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": VERIFICAR A APLICABILIDADE DO ART. 66 DA LRF, DE DUPLICAÇÃO DO PRAZO DE " +
                                "RECONDUÇÃO EM CASOS DE BAIXO CRESCIMENTO DO PIB.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE 1",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": ACIMA DE 95% DE 54% (51,30%), QUANDO SE INICIAM AS VEDAÇÕES DA LRF",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Diante dos elementos apurados acima, verificamos que a despesa total com " +
                                "pessoal não superou o limite previsto no art. 20, III, da Lei de Responsabilidade " +
                                "Fiscal, " +
                                "porém ultrapassou aquele previsto no art. 22, parágrafo único, da Lei supracitada, " +
                                "nos ",
                        formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" quadrimestres.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Constatamos a infringência do inciso ",
                        formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", do citado dispositivo, tendo em vista que ", formatacaoFactory.getJustificado(12))
                .concat("relatar as ocorrências.", formatacaoFactory.getJustificadoVermelhoCinza(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal, o " +
                                "Executivo Municipal foi alertado tempestivamente, por  ",
                        formatacaoFactory.getJustificado(12))
                .concat(quantidadeAlertasLRFa59p1i2.toString(), formatacaoFactory.getJustificado(12))
                .concat(" vezes, quanto à superação de 90% do específico limite da despesa laboral.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("OBSERVAR A DATA DE EMISSÃO DO ALERTA (CONSTANTE NO FINAL DO " +
                "DOCUMENTO “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO " +
                "FINAL DO EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE 2",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": ACIMA DE 54% NO QUADRIMESTRE",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();


        addParagrafo(addTab().concat("É possível ver que a superação do limite da despesa laboral aconteceu no " +
                                "último quadrimestre do exercício, significando ",
                        formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da Receita Corrente Líquida.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal, " +
                                "o Executivo Municipal" +
                                " foi alertado tempestivamente, por ",
                        formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(quantidadeAlertasLRFa59p1i2 + " vezes, quanto à superação de 90% do específico limite" +
                                " da despesa laboral.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("OBSERVAR A DATA DE EMISSÃO DO ALERTA (CONSTANTE NO FINAL DO DOCUMENTO" +
                " “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("Aqui pode ser destacada a situação de Municípios que extrapolaram" +
                " os 54% em 2021 e decretaram (ou não) calamidade pública, ",
                new Formatacao(12).justify().red().bgYellow().bold())
                .concat("ainda em prazo de recondução:", new Formatacao(12).justify().red().bgYellow().bold())
                .concat(":", new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(addTab().concat("Cabe ressaltar que o Município ", new Formatacao(12).justify())
                .concat("não", new Formatacao(12).justify().red())
                .concat(" decretou estado de calamidade pública/emergência no exercício de 2021, devidamente " +
                        "reconhecido pela Assembleia Legislativa Estadual, assim, ", new Formatacao(12).justify())
                .concat("não", new Formatacao(12).justify().red())
                .concat(" foi aplicável a suspensão de contagem de prazo para recondução aos " +
                        "limites, conforme art. 65 da Lei de Responsabilidade Fiscal. ", new Formatacao(12).justify())
                .concat("(adaptar conforme o caso, indicando período limite para recondução, " +
                                "atentando para eventual duplicação de prazo)",
                        new Formatacao(12).justify().red().bgYellow().bold())

        );
        addBreak();
        addParagrafo(new TextoFormatado("Informamos, por oportuno, que o Município ",
                new Formatacao(12).justify())
                .concat("(não)", new Formatacao(12).justify().red().bgYellow())
                .concat(" aderiu ao Programa de Acompanhamento e Transparência Fiscal instituído pela Lei " +
                        "Complementa Federal nº 178, de 13 de janeiro de 2021.", new Formatacao(12).justify())
        );
        addParagrafo(new TextoFormatado("(OBSEVAR QUE SE TRATA DO EXCEDENTE DA DESPESA DE PESSOAL" +
                " (54% DO ART. 20 LRF) APURADO NO 3º QUADRIMESTRE DE 2021, CALCULADO COMO PERCENTUAL DA" +
                " RCL TAMBÉM DO 3º QUADRIMESTRE. A REDUÇÃO DESSE EXCEDENTE É DE NO MÍNIMO 10% a.a. A " +
                "PARTIR DE 2023, SENDO QUE, ATÉ O TÉRMINO DO EXERCÍCIO DE 2032 O PODER DEVE RETORNAR " +
                "AO SEU LIMITE FISCAL)",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(new TextoFormatado("EM 2021 NÃO HOUVE OBRIGATORIEDADE DE REDUÇÃO DO % DE DESPESA DE " +
                "PESSOAL PARA RECONDUÇÃO  E NÃO SE APLICA O ART. 23 §3º LRF.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("(RELATAR POSSÍVEIS CONEXÕES COM A \"PERSPECTIVA B\")",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("C.1.3. PRECATÓRIOS ",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();
        addParagrafo(new TextoFormatado("OBS. 1",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": SÓ INSERIR ESSE SUBITEM, SE HOUVER RECÁLCULOS (ACRÉSCIMOS/EXCLUSÕES) A " +
                                "SEREM APRESENTADOS PELA FISCALIZAÇÃO.  DO CONTRÁRIO, APAGAR ESTE SUBITEM.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();
        addParagrafo(new TextoFormatado("OBS. 2",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": ATENTAR À NOTA TÉCNICA SDG Nº 142 (CONSIDERANDO A EDIÇÃO DA EC Nº 109/2021), " +
                                "INCLUSIVE PARA NOTICIAR EVENTUAL OCORRÊNCIA NO QUADRIMESTRE EM ANÁLISE, DE:",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(new TextoFormatado("- ADESÃO IRREGULAR À NOVA SISTEMÁTICA TRAZIDA PELA EC Nº " +
                "99/2017, COM A ATUALIZAÇÃO PELA EC Nº 109/2021, CONSIDERANDO QUE NÃO PODEM ADERIR OS" +
                " ÓRGÃOS QUE ESTAVAM EM DIA COM O PAGAMENTO (OU SEJA, JÁ ENQUADRÁVEIS NO REGIME ORDINÁRIO);",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("- EVENTUAIS INFORMAÇÕES DE IRREGULARIDADES EMITIDAS PELO TJ," +
                " INCLUSIVE QUANTO AO PERCENTUAL INSUFICIENTE AO PAGAMENTO;",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("- ACORDOS (EX. PARCELAMENTO) HOMOLOGADOS PELO TJ;",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(new TextoFormatado("- EVENTUAIS IRREGULARIDADES QUANTO À GESTÃO DAS FONTES " +
                "ADICIONAIS PARA PAGAMENTOS (CRIAÇÃO DE FUNDOS GARANTIDORES ETC.);",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(new TextoFormatado("- DESAPROPRIAÇÃO NOS CASOS VEDADOS;",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(new TextoFormatado("- COMPENSAÇÕES IRREGULARES - AUSÊNCIA DE REGULAMENTAÇÃO DA LEI " +
                "NO PRAZO DE 120 DIAS DE 1/1/2018, OU SEJA, 1/5/2018.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();

        addParagrafo(new TextoFormatado("(RELATAR POSSÍVEIS CONEXÕES COM A \"PERSPECTIVA B\")",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();

        addBreak();
        addSecao(new TextoFormatado("PERSPECTIVA D: APLICAÇÃO NO ENSINO E NA SAÚDE",
                formatacaoFactory.getBold(12)), heading1);

        addParagrafo(new TextoFormatado("HIPÓTESE 1",
                new Formatacao(12).justify().red().bold().underline().bgCcyan())
                .concat(": FISCALIZAÇÃO DO 1º QUADRIMESTRE - SE NÃO HOUVER ANÁLISES AUTOMÁTICAS DO SISTEMA AUDESP.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(addTab().concat("A presente fiscalização periódica foi concluída antes do encerramento " +
                "do 1º quadrimestre, motivo pelo qual deixamos de realizar as análises das aplicações" +
                " mínimas no ensino e na saúde. ", new Formatacao(12).justify())
        );
        addParagrafo(new TextoFormatado("OU",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(addTab().concat("A presente fiscalização periódica foi concluída antes do encerramento do 1º " +
                "quadrimestre, no entanto, identificamos ocorrências dignas de notas no(s) " +
                "subitem(ns) tratado(s) a seguir.", new Formatacao(12).justify())
        );
        addParagrafo(addTab().concat("Ressaltamos que a Administração Municipal deve atentar aos alertas " +
                        "automáticos eventualmente emitidos pelo Sistema Audesp, no curso do exercício.",
                new Formatacao(12).justify())
        );

        addParagrafo(new TextoFormatado("HIPÓTESE 2",
                new Formatacao(12).justify().red().bold().underline().bgCcyan())
                .concat(": FISCALIZAÇÃO DO 1º / 2º QUADRIMESTRE e 1º SEMESTRE: SE JÁ HOUVER ANÁLISES AUTOMÁTICAS DO " +
                                "SISTEMA AUDESP.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(addTab().concat("No âmbito de nossa amostragem, informamos que não foram identificadas, " +
                        "neste momento, ocorrências dignas de notas, relativas à aplicação mínima constitucional" +
                        " e legal no ensino e na saúde", new Formatacao(12).justify())
                .concat(", a par dos alertas já emitidos automaticamente pelo Sistema Audesp (se houver).",
                        new Formatacao(12).justify().red().bgGray())
        );
        addParagrafo(new TextoFormatado("OU",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addParagrafo(addTab().concat("No âmbito de nossa amostragem, informamos que foram identificadas " +
                        "ocorrências dignas de notas no(s) subitem(ns) tratado(s) a seguir., relativas à " +
                        "aplicação mínima constitucional e legal no ensino e na saúde.",
                new Formatacao(12).justify())
        );
        addParagrafo(addTab().concat("Consignamos que as análises automáticas das informações prestadas pelo " +
                        "Órgão ao Sistema Audesp, no decorrer do 1º (e/ou) 2º quadrimestre(s) estão consignadas " +
                        "no relatório automático, cujo teor reproduzimos no Anexo deste relatório.",
                new Formatacao(12).justify())
        );
        addParagrafo(addTab().concat("Ressaltamos que a Administração Municipal deve atentar aos alertas" +
                        " automáticos eventualmente emitidos pelo Sistema Audesp, no curso do exercício.",
                new Formatacao(12).justify())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("D.1. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL NO ENSINO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("OBS. 1",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": SÓ INSERIR ESSE ITEM, SE HOUVER RECÁLCULOS (ACRÉSCIMOS/EXCLUSÕES) A " +
                        "SEREM APRESENTADOS PELA FISCALIZAÇÃO.  DO CONTRÁRIO, APAGAR ESTE SUBITEM E " +
                        "RENUMERAR O OUTRO ABAIXO.", new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addParagrafo(addTab().concat("Apuramos inconsistências/acréscimos/reduções nos números apresentados" +
                " pela Origem ao Sistema AUDESP, que influenciam nos resultados na aplicação do" +
                " ensino, conforme abaixo demonstrado:", new Formatacao(12).justify())
        );

        addTabelaDespesasCfFundeb();
        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp", formatacaoFactory.getBoldJustificado(10))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(10)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": EM CASO DE SITUAÇÃO DESFAVORÁVEL, REPORTAR E DOCUMENTAR NOS AUTOS, " +
                                "MENCIONANDO A EMISSÃO DE ALERTAS, CONFORME SEGUE.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );

        addParagrafo(new TextoFormatado("(RELATAR POSSÍVEIS CONEXÕES COM A \"PERSPECTIVA B\")",
                new Formatacao(12).justify().red().bgYellow().bold())
        );


        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, ",
                        formatacaoFactory.getJustificado(12))
                .concat("foi o Município alertado", formatacaoFactory.getBold(12))
                .concat(", por ", formatacaoFactory.getJustificado(12))
                .concat("" + quantidadeAlertasLRFa59p1i5AplicacaoEnsino,
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" vezes, consoante Notificações de Alertas juntados no presente evento.",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addBreak();
        addSecao(new TextoFormatado("D.2. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL NA SAÚDE",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("OBS. 1",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": SÓ INSERIR ESSE ITEM, SE HOUVER RECÁLCULOS (ACRÉSCIMOS/EXCLUSÕES) A " +
                                "SEREM APRESENTADOS PELA FISCALIZAÇÃO.  DO CONTRÁRIO, APAGAR ESTE ITEM.",
                        new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();
        addParagrafo(addTab().concat("Apuramos inconsistências/acréscimos/reduções nos números apresentados" +
                " pela Origem ao Sistema AUDESP, que influenciam nos resultados na " +
                "aplicação da saúde, conforme abaixo demonstrado:", new Formatacao(12).justify())
        );

        addTabelaEmpenhadaLiquidadaPaga();

        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp", formatacaoFactory.getBold(10))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(10)));
        addBreak();

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                new Formatacao(12).justify().red().bgYellow().bold().underline())
                .concat(": EM CASO DE SITUAÇÃO DESFAVORÁVEL, REPORTAR E DOCUMENTAR NOS AUTOS, " +
                        "MENCIONANDO A EMISSÃO DE ALERTAS.", new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();

        addBreak();

        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, ",
                        formatacaoFactory.getJustificado(12))
                .concat("foi o Município alertado", formatacaoFactory.getBold(12))
                .concat(", por ", formatacaoFactory.getJustificado(12))
                .concat("" + quantidadeAlertasLRFa59p1i5AplicacaoEnsino,
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" vezes, consoante Notificações de Alertas juntados no presente evento.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("(RELATAR POSSÍVEIS CONEXÕES COM A \"PERSPECTIVA B\")",
                new Formatacao(12).justify().red().bgYellow().bold())
        );
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("CONCLUSÃO", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addParagrafo(addTab().concat("Com relação aos assuntos tratados neste relatório, destacamos:",
                new Formatacao(12).justify())
        );
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("Descrever resumidamente, de maneira clara e objetiva, a ocorrência " +
                "caracterizadora de eventual irregularidade, inclusive indicando valor de possível dano, se for o " +
                "caso, bem como a legislação infringida.",
                new Formatacao(12).justify().red().bgYellow().bold())
        );

        addBreak();
        addBreak();

        addParagrafo(addTab().concat("À consideração de Vossa Senhoria.", new Formatacao(12).justify())
        );

        addBreak();
        addBreak();

        addParagrafo(addTab().concat("DF-X / UR-X, ... de ............ de 2022.", new Formatacao(12).justify())
                 );

//        addSecao(new TextoFormatado("APÊNDICE – PERSPECTIVA B – FISCALIZAÇÃO OPERACIONAL", formatacaoFactory.getBold(12)), heading1);
//
//        addBreak();
//        addParagrafo(new TextoFormatado("ESTE APÊNDICE TRAZ SOMENTE ORIENTAÇÕES PARA PREENCHIMENTO DO RELATÓRIO " +
//                "PERIÓDICO, RELACIONADAS À PERSPECTIVA B – FISCALIZAÇÃO OPERACIONAL DE PLANEJAMENTO E \tEXECUÇÃO DAS " +
//                "POLÍTICAS PÚBLICAS",
//                new Formatacao(12).justify().red().bgYellow().bold())
//        );
//        addParagrafo(new TextoFormatado("O APÊNDICE NÃO DEVE SER PARTE DO RELATÓRIO DA FISCALIZAÇÃO.",
//                new Formatacao(12).justify().red().bgYellow().bold())
//        );
//
//        addParagrafo(addTab().concat("Este Apêndice contém sugestões/padrões de abordagem para itens " +
//                                "específicos a serem, EVENTUALMENTE, inseridos no Relatório.", new Formatacao(12).justify())
//                .concat("EVENTUALMENTE", new Formatacao(12).justify().red().bgGray())
//                .concat(", inseridos no Relatório.", new Formatacao(12).justify())
//                 );
//        addBreak();
//
//        addParagrafo(new TextoFormatado("B.1. PLANEJAMENTO DAS POLÍTICAS PÚBLICAS (i-Plan/IEG-M)", new Formatacao(12).justify()) );
//        addParagrafo(new TextoFormatado("B.2. ADEQUAÇÃO FISCAL DAS POLÍTICAS PÚBLICAS (i-Fiscal/IEG-M)", new Formatacao(12).justify()) );
//
//        addParagrafo(new TextoFormatado("B.3. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DO ENSINO (i-Educ/IEG-M)", new Formatacao(12).justify()) );
//
//        addParagrafo(new TextoFormatado("B.4. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DO SAÚDE (i-Saúde/IEG-M)", new Formatacao(12).justify()) );
//
//        addParagrafo(new TextoFormatado("B.5. EXECUÇÃO DAS POLÍTICAS PÚBLICAS AMBIENTAIS (i-Amb/IEG-M)", new Formatacao(12).justify()) );
//
//        addParagrafo(new TextoFormatado("B.6. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DE " +
//                "INFRAESTRUTURA (i-Cidade/IEG-M)", new Formatacao(12).justify()) );
//
//        addParagrafo(new TextoFormatado("B.7. EXECUÇÃO DAS POLÍTICAS PÚBLICAS DE " +
//                "TECNOLOGIA DA INFORMAÇÃO (i-Gov TI/IEG-M)", new Formatacao(12).justify()) );
//
//        addBreak();
//        addParagrafo(addTab().concat("Utilizar como sugestão os textos adiante para cada item do i-EGM", new Formatacao(12).justify())
//                 );
//        addParagrafo(new TextoFormatado("OBSERVAÇÕES INICIAIS:",
//                new Formatacao(12).justify().red().bgYellow().bold())
//        );
//
//        addParagrafo(new TextoFormatado("1) A análise deve ser feita sob o aspecto da adequação do planejamento e da" +
//                " execução das Políticas Públicas, para as análises quadrimestrais e semestrais a partir do exercício de " +
//          "2022. É recomendado fazer a análise crítica da qualidade do planejamento, verificando a coerência das " +
//           "políticas públicas criadas com as demandas reais do município.\nNão há obrigatoriedade de fazer " +
//                "apontamentos em todos os 7 (sete) itens. Caso não haja ocorrências dignas de nota, basta " +
//                "informar na  introdução da Perspectiva “B”, constante no relatório, apagando os itens.",
//                new Formatacao(12).justify().red().bgYellow().bold())
//        );


        return sendFile();

    }

    private void addTextoStatusFaseIEGM() {
        String textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        Integer flagStatusIEGM = resultadoIegmAnoBaseAnterior.getFlagStatusFase();
        if (flagStatusIEGM == 1) {
            textoFlagStatusIEGM = "Índices do exercício em exame em planejamento, " +
                    "dados podem sofrer alterações.";
        } else if (flagStatusIEGM == 2) {
            textoFlagStatusIEGM = "Índices do exercício em exame em verificação/validação da Fiscalização, " +
                    "dados podem sofrer alterações.";
        } else { // flagStatus == 3
            textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        }
        addParagrafo(new TextoFormatado(textoFlagStatusIEGM,
                formatacaoFactory.getItalic(12)));
    }

    private void addTabelaSinteseDoapurado() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "30%", "30%"},
                false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("ITENS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("EXERCÍCIO " + (exercicio - 2), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("EXERCÍCIO " + (exercicio - 1), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("CONTROLE INTERNO", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("REGULAR/IRREGULAR PARCIALMENTE REGULAR",
                formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("REGULAR/IRREGULAR PARCIALMENTE REGULAR",
                formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA – Resultado no exercício",
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA – Percentual de investimentos",
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("DÍVIDA DE CURTO PRAZO",
                formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("FAVORÁVEL /DESFAVORÁVEL", formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("FAVORÁVEL /DESFAVORÁVEL", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("DÍVIDA DE LONGO PRAZO",
                formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado("FAVORÁVEL /DESFAVORÁVEL", formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado("FAVORÁVEL /DESFAVORÁVEL", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("PRECATÓRIOS - Foi suficiente o pagamento/depósito de precatórios judiciais?",
                formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado("SIM/NÃO /PREJUDICADO", formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado("SIM/NÃO /PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("PRECATÓRIOS - Foi suficiente o pagamento de requisitórios de baixa monta?",
                formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado("SIM/NÃO", formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado("SIM/NÃO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("ENCARGOS - Efetuados os recolhimentos ao Regime Geral de Previdência Social " +
                "(INSS)?",
                formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado("SIM/NÃO", formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado("SIM/NÃO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("ENCARGOS - Efetuados os recolhimentos ao Regime Próprio de Previdência Social?",
                formatacaoFactory.getCenter(9)));
        linha10.add(new TextoFormatado("SIM / NÃO /PREJUDICADO", formatacaoFactory.getCenter(9)));
        linha10.add(new TextoFormatado("SIM / NÃO /PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("ENCARGOS – Está cumprindo parcelamentos de débitos de encargos? ",
                formatacaoFactory.getCenter(9)));
        linha11.add(new TextoFormatado("SIM/ NÃO / PARCIALMENTE", formatacaoFactory.getCenter(9)));
        linha11.add(new TextoFormatado("SIM/ NÃO / PARCIALMENTE", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("TRANSFERÊNCIAS AO LEGISLATIVO - Os repasses atenderam ao limite " +
                "constitucional?",
                formatacaoFactory.getCenter(9)));
        linha12.add(new TextoFormatado("SIM / NÃO /PREJUDICADO", formatacaoFactory.getCenter(9)));
        linha12.add(new TextoFormatado("SIM / NÃO /PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("LEI DE RESPONSABILIDADE FISCAL - Despesa de pessoal em dezembro " +
                "do exercício em exame",
                formatacaoFactory.getCenter(9)));
        linha13.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));
        linha13.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("LEI DE RESPONSABILIDADE FISCAL - Atendido o artigo 42, da LRF?",
                formatacaoFactory.getCenter(9)));
        linha14.add(new TextoFormatado("SIM / NÃO / PREJUDICADO", formatacaoFactory.getCenter(9)));
        linha14.add(new TextoFormatado("SIM / NÃO / PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("LEI DE RESPONSABILIDADE FISCAL - Atendido o artigo 21, II, da LRF?",
                formatacaoFactory.getCenter(9)));
        linha15.add(new TextoFormatado("SIM / NÃO / PREJUDICADO", formatacaoFactory.getCenter(9)));
        linha15.add(new TextoFormatado("SIM / NÃO / PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("ENSINO - Aplicação na Educação - art. 212 da Constituição Federal (Limite " +
                "mínimo de 25%)",
                formatacaoFactory.getCenter(9)));
        linha16.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));
        linha16.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("ENSINO 2021: Fundeb aplicado nos profissionais da educação básica (Limite " +
                "mínimo de 70%)",
                formatacaoFactory.getCenter(9)));
        linha17.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));
        linha17.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("ENSINO - Recursos Fundeb aplicados no exercício",
                formatacaoFactory.getCenter(9)));
        linha18.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));
        linha18.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("ENSINO - Se diferida, a parcela residual (de até 5% no que se " +
                "refere a 2020, ou até 10% relativamente a 2021) foi aplicada até 31/03/2021 ou 30/04/2022, " +
                "respectivamente?",
                formatacaoFactory.getCenter(9)));
        linha19.add(new TextoFormatado("SIM / NÃO / PREJUDICADO", formatacaoFactory.getCenter(9)));
        linha19.add(new TextoFormatado("SIM / NÃO / PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("SAÚDE - Aplicação na Saúde (Limite mínimo de 15%)",
                formatacaoFactory.getCenter(9)));
        linha20.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));
        linha20.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("Atendimento à Lei Orgância, Instruções e Recomendações do Tribunal de " +
                "Contas do Estado de São Paulo",
                formatacaoFactory.getCenter(9)));
        linha21.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha21.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("Consignar outras apurações que julgar relevantes (ou excluir esta linha)",
                new Formatacao(9).justify().red().bgYellow()));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }

}