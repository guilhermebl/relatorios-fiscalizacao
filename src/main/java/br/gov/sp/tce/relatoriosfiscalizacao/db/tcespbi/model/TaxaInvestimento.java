package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;

@Entity
@IdClass(TaxaInvestimentoKey.class)
public class TaxaInvestimento {

    @Id
    private Integer codigoIbge;

    @Id
    private Integer exercicio;

    @Column(name = "desp_liquidada")
    private BigDecimal despesaLiquidada;

    @Column(name = "desp_empenhada")
    private BigDecimal despesaEmpenhada;

    @Column(name = "rec_total")
    private BigDecimal receitaTotal;

    @Column(name = "perc_taxa")
    private BigDecimal percentualTaxa;

    @Column(name = "rcl")
    private BigDecimal rcl;

    @Column(name = "perc_taxa_rp")
    private BigDecimal percentualTaxaComRestosAPagar;

    public Integer getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(Integer codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public BigDecimal getDespesaLiquidada() {
        return despesaLiquidada;
    }

    public void setDespesaLiquidada(BigDecimal despesaLiquidada) {
        this.despesaLiquidada = despesaLiquidada;
    }

    public BigDecimal getDespesaEmpenhada() {
        return despesaEmpenhada;
    }

    public void setDespesaEmpenhada(BigDecimal despesaEmpenhada) {
        this.despesaEmpenhada = despesaEmpenhada;
    }

    public BigDecimal getReceitaTotal() {
        return receitaTotal;
    }

    public void setReceitaTotal(BigDecimal receitaTotal) {
        this.receitaTotal = receitaTotal;
    }

    public BigDecimal getPercentualTaxa() {
        return percentualTaxa;
    }

    public void setPercentualTaxa(BigDecimal percentualTaxa) {
        this.percentualTaxa = percentualTaxa;
    }

    public BigDecimal getRcl() {
        return rcl;
    }

    public void setRcl(BigDecimal rcl) {
        this.rcl = rcl;
    }

    public BigDecimal getPercentualTaxaComRestosAPagar() {
        return percentualTaxaComRestosAPagar;
    }

    public void setPercentualTaxaComRestosAPagar(BigDecimal percentualTaxaComRestosAPagar) {
        this.percentualTaxaComRestosAPagar = percentualTaxaComRestosAPagar;
    }
}
