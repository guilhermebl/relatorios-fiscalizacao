package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacao;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespRGFRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AudespRGFService {

    @Autowired
    private AudespRGFRepository audespRGFRepository;

    public Map<String, String> getRCLDespesaPessoalRGF(Integer codigoIbge, Integer exercicio, Integer tipoEntidade) {
        List<AudespResultado> audespResultado = audespRGFRepository.getRCLDespesaPessoalRGF(codigoIbge, exercicio, tipoEntidade);
        Map<String, String> mapa = new HashMap<>();

        List<String> parametrosValores = Arrays.asList(new String[]{"vRCL", "vDespPessoalLiq"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vPercDespPessoal"});

        for(AudespResultado resultado : audespResultado) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise() + resultado.getExercicio() + resultado.getQuadrimestre(),
                     FormatadorDeDados.formatarPercentual(resultado.getValor(),true, false, 2));
            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise() + resultado.getExercicio() + resultado.getQuadrimestre(),
                     FormatadorDeDados.formatarMoeda(resultado.getValor(),true, false));
            }
        }

        return mapa;
    }


    public List<AudespPublicacao> getSituacaoEntregaDocumentosRGF(Integer codigoIbge, Integer exercicio, String tipoEntidade) {
        int tipoDocumento = 0;
        if(tipoEntidade == "Executivo") {
            tipoDocumento = 80;
        } else if(tipoEntidade == "Legislativo"){
            tipoDocumento = 81;
        }

        List<AudespPublicacao> audespPublicacao = audespRGFRepository.getSituacaoEntregaDocumentosRGF(codigoIbge, exercicio, tipoDocumento);

        List<AudespPublicacao> foraDoPrazo = new ArrayList<>();
        for(AudespPublicacao publicacao : audespPublicacao) {
            if(publicacao == null || publicacao.isDentroDoprazo() == false)
                foraDoPrazo.add(publicacao);
        }
        return foraDoPrazo;
    }

    public List<AudespPublicacao> getSituacaoEntregaDocumentosRREO(Integer codigoIbge, Integer exercicio, Integer tipoEntidade) {
        List<AudespPublicacao> audespPublicacao = audespRGFRepository.getSituacaoEntregaDocumentosRREO(codigoIbge, exercicio, tipoEntidade);
        List<AudespPublicacao> foraDoPrazo = new ArrayList<>();
        for(AudespPublicacao publicacao : audespPublicacao) {
            if(publicacao == null || publicacao.isDentroDoprazo() == false)
                foraDoPrazo.add(publicacao);
        }
        return foraDoPrazo;
    }

}