package br.gov.sp.tce.relatoriosfiscalizacao.controller._2022;

import br.gov.sp.tce.relatoriosfiscalizacao.service.AudespDespesaPessoalService;
import br.gov.sp.tce.relatoriosfiscalizacao.service.AudespResultadoExecucaoOrcamentariaService;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Map;

@RestController
public class RelatorioPrefeituraQuadrimestral2022ExcelController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private XSSFWorkbook xssfWorkbook;

    private Integer exercicio;

    private Integer mesReferenciaQ1;

    private Integer mesReferenciaQ2;

    private Integer codigoIBGE;


    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    private Map<String, BigDecimal> audespDespesaPessoalMap;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    private Map<String, BigDecimal> audespResultadoExecucaoOrcamentariaMapPrimeiroQuadrimestre;

    private Map<String, BigDecimal> audespResultadoExecucaoOrcamentariaMapSegundoQuadrimestre;

    public ResponseEntity<Resource> download(Integer codigoIBGE, Integer exercicio, Integer quadrimestre) throws Exception {

        this.exercicio = exercicio;
        this.codigoIBGE = codigoIBGE;
        this.mesReferenciaQ1 = quadrimestre * 4;
        this.mesReferenciaQ2 = quadrimestre * 8;

        audespDespesaPessoalMap = audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbge(codigoIBGE, exercicio, 50);
        audespResultadoExecucaoOrcamentariaMapPrimeiroQuadrimestre = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestral(codigoIBGE, exercicio, mesReferenciaQ1);
        audespResultadoExecucaoOrcamentariaMapSegundoQuadrimestre = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestral(codigoIBGE, exercicio, mesReferenciaQ2);


        Resource resourceRelatorioExcel = resourceLoader.getResource("classpath:static/xlsx/pre-relatorio-quadrimestral-pm-2021.xlsx");

        InputStream isRelatorioExcel = resourceRelatorioExcel.getInputStream();

        this.xssfWorkbook = new XSSFWorkbook(isRelatorioExcel);

        addTabelaDespesaDePessoalPrimeiroQuadrimestre();
        addTabelaDespesaDePessoalSegundoQuadrimestre();
        addTabelaExecucaoOrcamentariaQuadrimestral("Execução Orçamentária - 1ºQ",
                audespResultadoExecucaoOrcamentariaMapPrimeiroQuadrimestre);
        addTabelaExecucaoOrcamentariaQuadrimestral("Execução Orçamentária - 2ºQ",
                audespResultadoExecucaoOrcamentariaMapSegundoQuadrimestre);

        XSSFFormulaEvaluator.evaluateAllFormulaCells(xssfWorkbook);

        return sendFile();
    }

    private void addTabelaDespesaDePessoalPrimeiroQuadrimestre() {
        XSSFSheet despesaDePessoalSheet = xssfWorkbook.getSheet("Desp. de Pessoal - 1ºQ");

        XSSFRow row = despesaDePessoalSheet.getRow(5);
        XSSFCell gastoInformadoAntAbr = row.getCell(1);
        gastoInformadoAntAbr.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + ( exercicio - 1 ) + 4));
        XSSFCell gastoInformadoAntAgo = row.getCell(2);
        gastoInformadoAntAgo.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + ( exercicio - 1 ) + 8 ));
        XSSFCell gastoInformadoAntDez = row.getCell(3);
        gastoInformadoAntDez.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + ( exercicio - 1) + 12));
        XSSFCell gastoInformadoAbr = row.getCell(4);
        gastoInformadoAbr.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + ( exercicio ) + 4));

        XSSFRow rowRCL = despesaDePessoalSheet.getRow(10);
        XSSFCell rclAntAbr = rowRCL.getCell(1);
        rclAntAbr.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio - 1) + 4));
        XSSFCell rclAntAgo = rowRCL.getCell(2);
        rclAntAgo.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio - 1) + 8));
        XSSFCell rclAntDez = rowRCL.getCell(3);
        rclAntDez.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio - 1) + 12));
        XSSFCell rclAbr = rowRCL.getCell(4);
        rclAbr.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio) + 4));
    }

    private void addTabelaDespesaDePessoalSegundoQuadrimestre() {
        XSSFSheet despesaDePessoalSheet = xssfWorkbook.getSheet("Desp. de Pessoal - 2ºQ");

        XSSFRow row = despesaDePessoalSheet.getRow(5);
        XSSFCell gastoInformadoAntAgo = row.getCell(1);
        gastoInformadoAntAgo.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio - 1) + 8));
        XSSFCell gastoInformadoAntDez = row.getCell(2);
        gastoInformadoAntDez.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio - 1) + 12));
        XSSFCell gastoInformadoAbr = row.getCell(3);
        gastoInformadoAbr.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio ) + 4));
        XSSFCell gastoInformadoAgo = row.getCell(4);
        gastoInformadoAgo.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio ) + 8));

        XSSFRow rowRCL = despesaDePessoalSheet.getRow(10);
        XSSFCell rclAntAgo = rowRCL.getCell(1);
        rclAntAgo.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio - 1) + 8));
        XSSFCell rclAntDez = rowRCL.getCell(2);
        rclAntDez.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio - 1) + 12));
        XSSFCell rclAbr = rowRCL.getCell(3);
        rclAbr.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio) + 4));
        XSSFCell rclAgo = rowRCL.getCell(4);
        rclAgo.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio) + 8));
    }

    private void addTabelaExecucaoOrcamentariaQuadrimestral(String sheetName, Map<String, BigDecimal> resultado) {
        XSSFSheet execucaoOrcamentaria = xssfWorkbook.getSheet(sheetName);

        XSSFRow rowExecucaoOrcamentaria = execucaoOrcamentaria.getRow(1);
        XSSFCell receitaRealizada = rowExecucaoOrcamentaria.getCell(1);
        receitaRealizada.setCellValue(getValor(resultado, "vSubTotRecRealPM"));

        XSSFRow rowDespesasEmpenhadas = execucaoOrcamentaria.getRow(2);
        XSSFCell despesasEmpenhadas = rowDespesasEmpenhadas.getCell(1);
        despesasEmpenhadas.setCellValue(getValor(resultado, "despesaEmpenhadaTotal"));

        XSSFRow rowRepassesDeDuodecimos = execucaoOrcamentaria.getRow(3);
        XSSFCell repassesDeDuodecimos = rowRepassesDeDuodecimos.getCell(1);
        repassesDeDuodecimos.setCellValue(getValor(resultado, "vRepDuodCM"));

        XSSFRow rowDevolucaoDeDuodecimos = execucaoOrcamentaria.getRow(4);
        XSSFCell devolucaoDeDuodecimos = rowDevolucaoDeDuodecimos.getCell(1);
        devolucaoDeDuodecimos.setCellValue(getValor(resultado, "vDevDuod"));

        XSSFRow rowTransferenciasFinanceiras = execucaoOrcamentaria.getRow(5);
        XSSFCell tranferenciasFinanceiras = rowTransferenciasFinanceiras.getCell(1);
        tranferenciasFinanceiras.setCellValue(getValor(resultado, "vTransfFinAdmIndExec"));

    }

    private Double getValor(Map mapa, String chave) {
        if(mapa != null && chave != null && mapa.get(chave) != null) {
            Object o = mapa.get(chave);
            if(o instanceof BigDecimal) {
                return ((BigDecimal)o).doubleValue();
            }
        }
        return 0.0;
    }

    private ResponseEntity<Resource> sendFile() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        xssfWorkbook.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-relatorio-pm-quadr-2021-v1.0.0.xlsx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }
}
