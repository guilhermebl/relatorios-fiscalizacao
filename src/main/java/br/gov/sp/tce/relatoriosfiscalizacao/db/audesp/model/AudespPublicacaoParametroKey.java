package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Embeddable
public class AudespPublicacaoParametroKey implements Serializable {

    private Integer tipoDocumentoId;

    private Integer exercicio;

    @Column(name = "mes_referencia")
    private Integer mes;

    @Column(name = "dt_publicacao")
    private LocalDate dataPublicacao;

    @Column(name = "veiculo_publicacao")
    private String veiculoPublicacao;

    @Column(name = "ds_estado_publicacao")
    private String ordemPublicacao;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AudespPublicacaoParametroKey)) return false;
        AudespPublicacaoParametroKey that = (AudespPublicacaoParametroKey) o;
        return Objects.equals(tipoDocumentoId, that.tipoDocumentoId) &&
                Objects.equals(exercicio, that.exercicio) &&
                Objects.equals(mes, that.mes) &&
                Objects.equals(dataPublicacao, that.dataPublicacao) &&
                Objects.equals(veiculoPublicacao, that.veiculoPublicacao) &&
                Objects.equals(ordemPublicacao, that.ordemPublicacao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tipoDocumentoId, exercicio, mes, dataPublicacao, veiculoPublicacao, ordemPublicacao);
    }
}