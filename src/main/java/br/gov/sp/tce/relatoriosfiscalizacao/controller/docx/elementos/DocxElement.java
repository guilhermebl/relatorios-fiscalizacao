package br.gov.sp.tce.relatoriosfiscalizacao.controller.docx.elementos;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.FormatacaoTabela;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.MergePosition;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.TextoFormatado;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.certidao.proposta.DocxHeader;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.math.BigInteger;
import java.util.List;

public class DocxElement {

    private XWPFDocument document;

    private DocxHeader header;

    public DocxElement(XWPFDocument document) {
        if(document == null)
            throw new IllegalArgumentException();
        this.document = document;
    }

    public DocxHeader getHeader() {
        return header;
    }

    public void writeHeader() {
        if(header != null)
            header.getHeader();

    }

    public void setHeader(DocxHeader header) {
        this.header = header;
    }

    public XWPFParagraph addParagrafo(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();

//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());

        paragraph.setSpacingAfter(6 * 20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    public XWPFParagraph addParagrafo(String texto) {
        return addParagrafo(new TextoFormatado(texto));
    }

    public XWPFParagraph addParagrafoSpacingAfter(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();
//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());
//        paragraph.setSpacingAfter(6*20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    public XWPFParagraph addParagrafo(TextoFormatado textoFormatado, boolean breakPage) {
        XWPFParagraph paragraph = addParagrafo(textoFormatado);
        paragraph.setPageBreak(breakPage);
        return paragraph;
    }

    public XWPFTable addTabela(List<List<TextoFormatado>> dados, FormatacaoTabela formatacaoTabela) {

        int qdtLinhas = dados.size();
        int qtdColunas = 0;

        for (int i = 0; i < dados.size(); i++) {
            if (qtdColunas < dados.get(i).size())
                qtdColunas = dados.get(i).size();
        }

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        formatacaoTabela.formatarTabela(tabela);

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
//            row.setHeight((int) (twipsPerInch * 1 / 5)); //set height 1/10 inch.
//            row.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact"
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }

            for (int j = 0; j < dados.get(i).size(); j++) {
                XWPFTableCell cell = j == 0 ? row.getCell(j) : row.createCell();
                cell.setWidth(formatacaoTabela.getWidths().get(j));
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                if (i == 0 && formatacaoTabela.isFirstLineHeader())
                    cell.getCTTc().addNewTcPr().addNewShd().setFill("cccccc");

                XWPFRun run = cell.getParagraphs().get(0).createRun();

                XWPFParagraph paragrafo = cell.getParagraphs().get(0);
                paragrafo.setSpacingBetween(1);

                dados.get(i).get(j).setParagraphText(paragrafo);

            }

        }

        tabela.removeRow(0);

        return tabela;

    }

    public XWPFTable addTabelaColunaUnica(List<TextoFormatado> dados, FormatacaoTabela formatacaoTabela) {

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        for(int k = 1; k < tabela.getRows().size(); k++ ) {
            tabela.removeRow(k);
        }

        formatacaoTabela.formatarTabela(tabela);

        if(dados.size() == 0) {
            return tabela;
        }

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }
            XWPFTableCell cell = null;

            if(row.getTableCells().size() == 0)
                cell = row.createCell();
            else
                cell = row.getCell(0);

            cell.setWidth("100%");
            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            if (i % 2 == 0 )
                cell.getCTTc().addNewTcPr().addNewShd().setFill("dce6f1");

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            XWPFParagraph paragrafo = cell.getParagraphs().get(0);
            paragrafo.setSpacingBetween(1);

            dados.get(i).setParagraphText(paragrafo);
        }

        tabela.removeRow(0);

        return tabela;

    }

    public XWPFTable mergeCells(XWPFTable tabela, List<MergePosition> cellsToMerge) {
        tabela = mergeCellsHorizontal(tabela, cellsToMerge);
        tabela = mergeCellsVertical(tabela, cellsToMerge);
        return tabela;

    }

    public void addListaNumerada(List<TextoFormatado> listItens) {

        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
        //Next we set the AbstractNumId. This requires care.
        //Since we are in a new document we can start numbering from 0.
        //But if we have an existing document, we must determine the next free number first.
        cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));

        /* Bullet list
          CTLvl cTLvl = cTAbstractNum.addNewLvl();
          cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
          cTLvl.addNewLvlText().setVal("•");
        */

        ///* Decimal list
        CTLvl cTLvl = cTAbstractNum.addNewLvl();
        cTLvl.addNewNumFmt().setVal(STNumberFormat.DECIMAL);
        cTLvl.addNewPPr();
        CTInd ind = cTLvl.getPPr().addNewInd(); //Set the indent

        ind.setHanging(BigInteger.valueOf(360*2));
        ind.setLeft(BigInteger.valueOf(360*6));
        cTLvl.addNewLvlText().setVal("%1.");
        cTLvl.addNewStart().setVal(BigInteger.valueOf(1));
        //*/

        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

        XWPFNumbering numbering = document.createNumbering();

        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

        BigInteger numID = numbering.addNum(abstractNumID);

        for (TextoFormatado item : listItens) {
            item.setListNumId(numID);
            addParagrafo(item);
        }
    }

    public XWPFTable mergeCellsHorizontal(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTHMerge hMerge = CTHMerge.Factory.newInstance();
            hMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setHMerge(hMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeHorizontal().size(); j++) {
                CTHMerge hToMerge = CTHMerge.Factory.newInstance();
                hToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeHorizontal().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeHorizontal().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setHMerge(hToMerge);
            }
        }

        return tabela;
    }

    public XWPFTable mergeCellsVertical(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTVMerge vMerge = CTVMerge.Factory.newInstance();
            vMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setVMerge(vMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeVertical().size(); j++) {
                CTVMerge vToMerge = CTVMerge.Factory.newInstance();
                vToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeVertical().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeVertical().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setVMerge(vToMerge);
            }
        }

        return tabela;
    }

    private void getTabela(XWPFDocument document, String[][] itensTabela, boolean isBold, String fontFamily, int fontSize) {
        XWPFTable table = document.createTable(0, 0);
        table.setWidthType(TableWidthType.PCT);
        table.setWidth("98%");
        table.removeBorders();

        for (int i = 0; i < itensTabela.length; i++) {
            if (table.getRow(i) == null)
                table.createRow();


            for (int x = 0; x < itensTabela[i].length; x++) {
                XWPFTableCell cell;
                // verifica se já existe a cell 0
                if (table.getRow(i).getCell(x) != null)
                    cell = table.getRow(i).getCell(x);
                else
                    cell = table.getRow(i).createCell();

                cell.removeParagraph(0);
                XWPFParagraph cellPar = cell.addParagraph();
                XWPFRun cellParRun = cellPar.createRun();
                cellParRun.setFontFamily(fontFamily);
                cellParRun.setText(itensTabela[i][x]);
                cellParRun.setBold(isBold);
                cellParRun.setFontSize(fontSize);
            }
        }
    }

    public void addBreak() {
        this.document.createParagraph();//.createRun().addBreak();
    }

    public void addPageBreak() {
        XWPFParagraph paragraph = this.document.createParagraph();
        paragraph.setPageBreak(true);
    }



}
