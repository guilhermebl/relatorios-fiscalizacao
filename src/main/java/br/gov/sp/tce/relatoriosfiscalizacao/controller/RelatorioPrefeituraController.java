package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import br.gov.sp.tce.relatoriosfiscalizacao.controller._2019.RelatorioPrefeituraFechamento2019Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2019.RelatorioPrefeituraQuadrimestral2019Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2020.RelatorioPrefeituraEspecialCovid2020Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2020.RelatorioPrefeituraFechamento2020Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2020.RelatorioPrefeituraQuadrimestral2020Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2021.RelatorioPrefeituraFechamento2021Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2021.RelatorioPrefeituraQuadrimestral2021Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2022.RelatorioPrefeituraQuadrimestral2022Controller;
import br.gov.sp.tce.relatoriosfiscalizacao.controller._2022.RelatorioPrefeituraSemestral2022Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RelatorioPrefeituraController {

    @Autowired
    private RelatorioPrefeituraFechamento2018Controller relatorioPrefeituraFechamento2018Controller;

    @Autowired
    private RelatorioPrefeituraQuadrimestral2019Controller relatorioPrefeituraQuadrimestral2019Controller;

    @Autowired
    private RelatorioPrefeituraQuadrimestral2020Controller relatorioPrefeituraQuadrimestral2020Controller;

    @Autowired
    private RelatorioPrefeituraFechamento2019Controller relatorioPrefeituraFechamento2019Controller;

    @Autowired
    private RelatorioPrefeituraFechamento2020Controller relatorioPrefeituraFechamento2020Controller;

    @Autowired
    private RelatorioPrefeituraEspecialCovid2020Controller relatorioPrefeituraEspecialCovid2020Controller;

    @Autowired
    private RelatorioPrefeituraQuadrimestral2021Controller relatorioPrefeituraQuadrimestral2021Controller;

    @Autowired
    private RelatorioPrefeituraFechamento2021Controller relatorioPrefeituraFechamento2021Controller;

    @Autowired
    private RelatorioPrefeituraQuadrimestral2022Controller relatorioPrefeituraQuadrimestral2022Controller;

    @Autowired
    private RelatorioPrefeituraSemestral2022Controller relatorioPrefeituraSemestral2022Controller;



    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/prefeitura/codigoibge/{codigoIBGE}/exercicio/{exercicio}/quadrimestre/{quadrimestre}/semestre/{semestre}")
    public ResponseEntity<Resource> download(@PathVariable Integer codigoIBGE, @PathVariable Integer exercicio,
                                             @PathVariable Integer quadrimestre, @PathVariable Integer semestre) throws Exception {
        log.info("[M] municipio: " + codigoIBGE + " - exercício: " + exercicio + " - quadrimestre: " + quadrimestre);

        if( exercicio == 2022 && ( quadrimestre == 1 || quadrimestre == 2 || semestre == 1 ) )
            return relatorioPrefeituraSemestral2022Controller.download(codigoIBGE, exercicio, quadrimestre);
        else
            return new ResponseEntity("Pré-relatório não encontrado para o período", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/prefeitura/codigoibge/{codigoIBGE}/exercicio/{exercicio}/quadrimestre/{quadrimestre}")
    public ResponseEntity<Resource> download(@PathVariable Integer codigoIBGE, @PathVariable Integer exercicio,
                                             @PathVariable Integer quadrimestre) throws Exception {
        log.info("[M] municipio: " + codigoIBGE + " - exercício: " + exercicio + " - quadrimestre: " + quadrimestre);

//        if( exercicio == 2022 && quadrimestre == 3 )
//            return relatorioPrefeituraFechamento2022Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2022 && ( quadrimestre == 1 || quadrimestre == 2 ) )
            return relatorioPrefeituraQuadrimestral2022Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2021 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2021Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2021 && ( quadrimestre == 1 || quadrimestre == 2 ) )
            return relatorioPrefeituraQuadrimestral2021Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2020 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2020Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2020 && ( quadrimestre == 1 || quadrimestre == 2 ) )
            return relatorioPrefeituraQuadrimestral2020Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2019 && ( quadrimestre == 1 || quadrimestre == 2 ) )
            return relatorioPrefeituraQuadrimestral2019Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2019 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2019Controller.download(codigoIBGE, exercicio, quadrimestre);
        if( exercicio == 2018 && quadrimestre == 3 )
            return relatorioPrefeituraFechamento2018Controller.download(codigoIBGE, exercicio, quadrimestre);
        else
            return new ResponseEntity("Pré-relatório não encontrado para o período",
                    new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }



    @GetMapping("/prefeitura/especial/covid/codigoibge/{codigoIBGE}/exercicio/{exercicio}/quadrimestre/{mes}")
    public ResponseEntity<Resource> prefeituraEspecialCovid(@PathVariable Integer codigoIBGE, @PathVariable Integer exercicio,
                                                            @PathVariable Integer mes) throws Exception {
        if( exercicio == 2020 && ( mes == 8 || mes == 9 ) )
            return relatorioPrefeituraEspecialCovid2020Controller.download(codigoIBGE, exercicio, mes);
        else
            return new ResponseEntity("Pré-relatório não encontrado para o período",
                    new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}