package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.QuestionarioItem;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class QuestionarioCovidRepository {

    @PersistenceContext(unitName = "wsquestionario")
    private EntityManager entityManager;

    public List<QuestionarioItem> getRespostaPorCodigoPergunta(Integer codigoIBGE, Integer operacaoId, String codigoPergunta) {
        Query query = entityManager.createNativeQuery("select  " +
                "ent.municipio_id as municipio_ibge," +
                "obj.operacao_id as operacao_id," +
                "grupo.descricao as grupo,     " +
                "perg.codigo as pergunta_codigo,  " +
                "perg.texto as texto_pergunta,  " +
                "opresp.codigo as opcao_resposta_codigo, " +
                "opresp.descricao as opcao_resposta_escolhida, " +
                "resp.id as resposta_id," +
                "resp.data_resposta as data_resposta," +
                "resp.comentario as resposta_comentario,"+
                "resp.valor_hora as valor_hora,"+
                "round(cast(resp.valor_numerico as numeric), 2) as valor_numerico,"+
                "resp.valor_texto as valor_texto,"+
                "resp.valor_inteiro as valor_inteiro,"+
                "resp.opcao_resposta_id  as opcao_resposta_id "+
                "from " +
                "questionario.objeto_fiscalizacao obj, " +
                "questionario.grupo_perguntas grupo, " +
                "questionario.pergunta perg, " +
                "questionario.questionario quest, " +
                "questionario.item_pergunta item, " +
                "questionario.resposta resp , " +
                "questionario.opcao_resposta opresp, " +
                "questionario.entidade ent " +
                "where   " +
                "grupo.questionario_id = obj.questionario_id   " +
                "and perg.grupo_perguntas_id = grupo.id  " +
                "and quest.id = grupo.questionario_id  " +
                "and quest.id = obj.questionario_id  " +
                "and obj.id = resp.objeto_fiscalizacao_id " +
                "and  item.pergunta_id = perg.id  " +
                "and  opresp.id = resp.opcao_resposta_id " +
                "and  resp.item_pergunta_id = item.id   " +
                "and obj.entidade_id = ent.id " +
                "and obj.operacao_id =  :operacaoId " +
                "and ent.municipio_id =  :codigoIBGE  " +
                "and perg.codigo = :codigoPergunta  " +
                "order by  resp.data_resposta desc", QuestionarioItem.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("operacaoId", operacaoId);
        query.setParameter("codigoPergunta", codigoPergunta);

        List<QuestionarioItem> lista = query.getResultList();

        return lista;
    }

    public List<QuestionarioItem> getRespostaPorCodigoOpcaoResposta(Integer codigoIBGE, Integer operacaoId, String codigoOpcaoResposta) {
        Query query = entityManager.createNativeQuery("select  " +
                "ent.municipio_id as municipio_ibge," +
                "obj.operacao_id as operacao_id," +
                "grupo.descricao as grupo,     " +
                "perg.codigo as pergunta_codigo,  " +
                "perg.texto as texto_pergunta,  " +
                "opresp.codigo as opcao_resposta_codigo, " +
                "opresp.descricao as opcao_resposta_escolhida, " +
                "resp.id as resposta_id," +
                "resp.data_resposta as data_resposta," +
                "resp.comentario as resposta_comentario,"+
                "resp.valor_hora as valor_hora,"+
                "resp.valor_numerico as valor_numerico,"+
                "resp.valor_texto as valor_texto,"+
                "resp.valor_inteiro as valor_inteiro,"+
                "resp.opcao_resposta_id  as opcao_resposta_id "+
                "from " +
                "questionario.objeto_fiscalizacao obj, " +
                "questionario.grupo_perguntas grupo, " +
                "questionario.pergunta perg, " +
                "questionario.questionario quest, " +
                "questionario.item_pergunta item, " +
                "questionario.resposta resp , " +
                "questionario.opcao_resposta opresp, " +
                "questionario.entidade ent " +
                "where   " +
                "grupo.questionario_id = obj.questionario_id   " +
                "and perg.grupo_perguntas_id = grupo.id  " +
                "and quest.id = grupo.questionario_id  " +
                "and quest.id = obj.questionario_id  " +
                "and obj.id = resp.objeto_fiscalizacao_id " +
                "and  item.pergunta_id = perg.id  " +
                "and  opresp.id = resp.opcao_resposta_id " +
                "and  resp.item_pergunta_id = item.id   " +
                "and obj.entidade_id = ent.id " +
                "and obj.operacao_id =  :operacaoId " +
                "and ent.municipio_id =  :codigoIBGE  " +
                "and opresp.codigo = :codigoOpcaoResposta  " +
                "order by  resp.data_resposta desc", QuestionarioItem.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("operacaoId", operacaoId);
        query.setParameter("codigoOpcaoResposta", codigoOpcaoResposta);

        List<QuestionarioItem> lista = query.getResultList();

        return lista;
    }

}