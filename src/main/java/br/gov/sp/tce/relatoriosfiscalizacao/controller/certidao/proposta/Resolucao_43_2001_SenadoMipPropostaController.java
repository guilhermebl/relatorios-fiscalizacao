package br.gov.sp.tce.relatoriosfiscalizacao.controller.certidao.proposta;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.Formatacao;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.FormatacaoFactory;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.FormatacaoTabela;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.TextoFormatado;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.docx.elementos.DocxElement;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacao;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasParecer;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import org.apache.commons.text.WordUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
public class Resolucao_43_2001_SenadoMipPropostaController {

    private XWPFDocument document;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private DocxElement docx;

    private Integer exercicio;

    private Integer codigoIBGE;

    FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");

    @Autowired
    private IegmService iegmService;

    private ResultadoIegm resultadoIegm2018;

    @Autowired
    private ParecerPrefeituraService parecerPrefeituraService;

    private List<ParecerPrefeitura> pareceresPrefeiturasList;

    @Autowired
    private TabelasService tabelasService;

    private TabelasProtocolo tabelasProtocolo;

    @Autowired
    private AudespRGFService audespRGFService;

    @Autowired
    private AudespEnsinoService audespEnsinoService;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    @Autowired
    private AudespSaudeService audespSaudeService;

    @Autowired
    private AudespLimiteLrfService audespLimiteLrfService;

    @Autowired
    private AudespOperacoesDeCreditoService audespOperacoesDeCreditoService;

    @Autowired
    private AudespService audespService;

    @Autowired
    private AudespAlertasService audespAlertasService;

    @Autowired
    private ApontamentosODSService apontamentosODSService;

    private Map<String, List<ApontamentoODS>> apontamentosODS;

    @Autowired
    private ResourceLoader resourceLoader;

    private Map<Integer, NotaIegm> notasIegm;

    private List<AudespResponsavel> responsavelPrefeitura;

    private Map<String,List<AudespResponsavel>> responsavelSubstitutoPrefeitura;

    private MunicipioIbge municipioIegmCodigoIbge;

    private Map<String, String> audespResultadoExecucaoOrcamentaria;

    private Map<String, String> audespResultadoExecucaoOrcamentariaExcercicioAnt;

    @Autowired
    private AudespEntidadeService audespEntidadeService;

    private AudespEntidade audespEntidade;

    @Autowired
    private DemonstrativosRaeeService demonstrativosRaeeService;

    private  Map<String,String> anexo14AMap;

    private Map<String, String> audespEnsinoFundeb;

    private Map<String, String> audespSaude;

    private Map<String, String> audespDespesaPessoalMap;

    private Map<String, String> quadroGeralEnsinoMap;

    private Map<String, String> aplicacoesEmSaude;

    @Autowired
    private ApontamentosFOService apontamentosFOService;

    private Map<Integer, List<ApontamentoFO>> apontamentoFOMap;

    @Autowired
    private AudespDividaAtivaService audespDividaAtivaService;

    private Map<String, String> audespDividaAtivaMap;

    private Map<String,String> audespResultadoExecucaoOrcamentariaMap;

    @Autowired
    private AudespFase3Service audespFase3Service;

    private Map<String, AudespFase3QuadroPessoal> audespFase3QuadroDePessoalMap;

    @Autowired
    private AudespBiService audespBiService;

    @Autowired
    private TcespBiService tcespBiService;

    private ConjuntoPareceresCertidao conjuntoPareceresCertidao;



    private Map<Integer, String> valorInvestimentoMunicipioMap;

    private Map<String, String> rclMunicipioDevedoresMap;

    private Map<String, String> limiteLRFMap;

    private Integer quadrimestre;

    private String heading1 = "Seção 1";
    private String heading2 = "Seção 2";
    private String heading3 = "Seção 3";
    private String heading4 = "Seção 4";

    private DocxHeader docxHeader;


    private ResponseEntity<Resource> sendFile() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);

        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-resolucao-43-2001-senado-v2.0.0.docx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }





    private void addSecao(TextoFormatado textoFormatado, String heading) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle(heading);
        paragraph.setSpacingAfter(6 * 20);
        if (heading.equals(this.heading1)) {
            CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill("d9d9d9");
        }

        textoFormatado.setParagraphText(paragraph);
    }

    private void createDocumentStyles() {
        XWPFStyles styles = document.createStyles();


        addCustomHeadingStyle(styles, heading1, 1);
        addCustomHeadingStyle(styles, heading2, 2);
        addCustomHeadingStyle(styles, heading3, 3);
        addCustomHeadingStyle(styles, heading4, 4);
    }


    private void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("Arial");

        CTRPr rpr = CTRPr.Factory.newInstance();
//        rpr.setRFonts(fonts);

        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }

    public byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }

    private TextoFormatado addTab() {
        Formatacao formatacaoTab = formatacaoFactory.getTab();
        return new TextoFormatado("", formatacaoTab).concat("", formatacaoTab);
    }

    private String getQuadrimestreTituloComAno(Integer quadrimestre) {
        String quadrimestreTitulo = "1º/2º Quadrimestre de 2018";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre de 2018";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre de 2018";
        return quadrimestreTitulo;
    }

    private String getQuadrimestreTitulo(Integer quadrimestre) {
        String quadrimestreTitulo = "3º Quadrimestre";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre";
        return quadrimestreTitulo;
    }

    private Integer getMesReferencia(Integer quadrimestre) {
        Integer mesReferencia = 12;
        if (quadrimestre == 1)
            mesReferencia = 4;
        if (quadrimestre == 2)
            mesReferencia = 8;
        return mesReferencia;
    }
    private Integer getQuadrimestreDoMes(Integer mes) {
        if (mes <= 4)
            return 1;
        if (mes > 4 && mes <= 8 )
            return 2;
        return 3;
    }


    public void addTabelaPessoalExecutivoArt23(Integer exercicio, boolean calcular) {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(
                new String[]{"15%", "10%", "20%", "20%", "15%", "20%"}, true);



        Map<String, String> audespRGFMap = this.audespRGFService.getRCLDespesaPessoalRGF(codigoIBGE,  exercicio, 50);
        if(!calcular)
            audespRGFMap = new HashMap<>();



        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("QUADRIMESTRE", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("ANO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("GASTOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("RCL", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("% GASTO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("% Permitido Legal", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("1", formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(exercicio.toString(), formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(audespRGFMap.get("vDespPessoalLiq" + exercicio + 1), formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(audespRGFMap.get("vRCL" + exercicio + 1), formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(audespRGFMap.get("vPercDespPessoal" + exercicio + 1), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("54,00%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2", formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(exercicio.toString(), formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(audespRGFMap.get("vDespPessoalLiq" + exercicio + 2), formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(audespRGFMap.get("vRCL" + exercicio + 2), formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(audespRGFMap.get("vPercDespPessoal" + exercicio + 2), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("54,00%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("3", formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(exercicio.toString(), formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(audespRGFMap.get("vDespPessoalLiq" + exercicio + 3), formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(audespRGFMap.get("vRCL" + exercicio + 3), formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(audespRGFMap.get("vPercDespPessoal" + exercicio + 3), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("54,00%", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = docx.addTabela(dados, formatacaoTabela);


    }

    public void addTabelaPublicaoEmAtraso(List<AudespPublicacao> lista) {
        lista.removeIf(Objects::isNull);
        if(lista == null || lista.isEmpty())
            return;

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(
                new String[]{"30%", "10%", "15%", "25%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Publicação", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data da Publicação", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Veículo", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Situação", formatacaoFactory.getBoldCenter(9)));
        dados.add(linha1);

        for(AudespPublicacao publicacao : lista) {
            List<TextoFormatado> linha = new ArrayList<>();
            linha.add(new TextoFormatado(publicacao.getNomeTipoDocumento(), formatacaoFactory.getJustificado(9)));
            linha.add(new TextoFormatado(publicacao.getMes().toString(), formatacaoFactory.getJustificado(9)));
            linha.add(new TextoFormatado(publicacao.getDataPublicacaoFormatado(), formatacaoFactory.getJustificado(9)));
            linha.add(new TextoFormatado(publicacao.getVeiculoPublicacao(), formatacaoFactory.getJustificado(9)));
            linha.add(new TextoFormatado(publicacao.getOrdemPublicacao(), formatacaoFactory.getCenter(9)));
            dados.add(linha);
        }

        XWPFTable tabela = docx.addTabela(dados, formatacaoTabela);


    }

    public void addTabelaPessoalLegislativoArt23(Integer exercicio, boolean calcular) {

        Map<String, String> audespRGFMap = this.audespRGFService.getRCLDespesaPessoalRGF(codigoIBGE,  exercicio, 51);


        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(
                new String[]{"15%", "10%", "20%", "20%", "15%", "20%"}, true);

        if(!calcular)
            audespRGFMap = new HashMap<>();

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("QUADRIMESTRE", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("ANO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("GASTOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("RCL", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("% GASTO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("% Permitido Legal", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("1", formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(exercicio.toString(), formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(audespRGFMap.get("vDespPessoalLiq" + exercicio + 1), formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(audespRGFMap.get("vRCL" + exercicio + 1), formatacaoFactory.getJustificado(9)));
        linha2.add(new TextoFormatado(audespRGFMap.get("vPercDespPessoal" + exercicio + 1), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("6,00%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2", formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(exercicio.toString(), formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(audespRGFMap.get("vDespPessoalLiq" + exercicio + 2), formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(audespRGFMap.get("vRCL" + exercicio + 2), formatacaoFactory.getJustificado(9)));
        linha3.add(new TextoFormatado(audespRGFMap.get("vPercDespPessoal" + exercicio + 2), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("6,00%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("3", formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(exercicio.toString(), formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(audespRGFMap.get("vDespPessoalLiq" + exercicio + 3), formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(audespRGFMap.get("vRCL" + exercicio + 3), formatacaoFactory.getJustificado(9)));
        linha4.add(new TextoFormatado(audespRGFMap.get("vPercDespPessoal" + exercicio + 3), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("6,00%", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = docx.addTabela(dados, formatacaoTabela);


    }

    private String formataTcManualRedacao(String tc) {
        while(tc.length() < 13) {
            tc = "0" + tc;
        }
        tc = tc.replaceAll("/",".");
        return tc;
    }

    @GetMapping("/certidao/proposta/uniao/codigoibge/{codigoIBGE}/exercicio/{exercicio}/quadrimestre/{quadrimestre}")
    public ResponseEntity<Resource> resolucaoSenadoUniao(@PathVariable Integer codigoIBGE,
                                             @PathVariable Integer exercicio,
                                             @PathVariable Integer quadrimestre) throws IOException, InvalidFormatException, Exception {
        return preCertidao(codigoIBGE, exercicio, quadrimestre, true);
    }

    @GetMapping("/certidao/proposta/codigoibge/{codigoIBGE}/exercicio/{exercicio}/quadrimestre/{quadrimestre}")
    public ResponseEntity<Resource> resolucaoSenado(@PathVariable Integer codigoIBGE,
                                             @PathVariable Integer exercicio,
                                             @PathVariable Integer quadrimestre) throws IOException, InvalidFormatException, Exception{
        return preCertidao(codigoIBGE, exercicio, quadrimestre, false);

    }

    private ResponseEntity<Resource> preCertidao(Integer codigoIBGE, Integer exercicio, Integer quadrimestre, boolean uniao)
            throws IOException, InvalidFormatException, Exception {

        this.codigoIBGE = codigoIBGE;
        document = new XWPFDocument();
        docx = new DocxElement(document);
        docx.setHeader(new DocxHeaderTcespSimples(document));
        docx.writeHeader();
        //getHeader();

        int mesReferencia = getMesReferencia(quadrimestre);
        this.tabelasProtocolo = tabelasService.getTabelasProtocoloPrefeituraByCodigoIbge(codigoIBGE, exercicio);


//        if(uniao) {
//            docx.addParagrafo(new TextoFormatado("MODELO PARA FORNECIMENTO DAS INFORMAÇÕES VISANDO A EXPEDIÇÃO DE " +
//                    "CERTIDÕES VOLTADAS AO CUMPRIMENTO DA RESOLUÇÃO 43, DE 2001 DO SENADO FEDERAL COM AS ALTERAÇÕES " +
//                    "INTRODUZIDAS PELA RESOLUÇÃO Nº 3, DE 2002. ", formatacaoFactory.getBoldJustificado(10))
//                    .concat("(PARA OPERAÇÕES DE CRÉDITO COM GARANTIA DA UNIÃO – MIP/ABRIL-19)",
//                            formatacaoFactory.getBoldJustificadoFundoAmarelo(10)));
//        } else {
//            docx.addParagrafo(new TextoFormatado("MODELO PARA FORNECIMENTO DAS INFORMAÇÕES VISANDO A EXPEDIÇÃO DE " +
//                    "CERTIDÕES VOLTADAS AO CUMPRIMENTO DA RESOLUÇÃO 43, DE 2001 DO SENADO FEDERAL COM AS ALTERAÇÕES " +
//                    "INTRODUZIDAS PELA RESOLUÇÃO Nº 3, DE 2002. ", formatacaoFactory.getBoldJustificado(10)));
//        }
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("INTERESSADO: "
                + WordUtils.capitalizeFully(tabelasProtocolo.getNomeOrgao()) +"\n" +
                "ASSUNTO: Solicita Certidão atestando o cumprimento dos dispositivos legais constantes do Chamado " +
                "#SDG ______________\n", formatacaoFactory.getBoldJustificado(12)));

        docx.addBreak();

        docx.addParagrafo( new TextoFormatado("Senhor(a) Diretor(a) Técnico de Divisão,",
                formatacaoFactory.getBoldJustificado(12)));
        docx.addBreak();

        docx.addParagrafo( new TextoFormatado("Em atendimento ao solicitado, informamos os dados necessários para " +
                "elaboração da Certidão referente à ", formatacaoFactory.getJustificado(12))
                .concat(WordUtils.capitalizeFully(tabelasProtocolo.getNomeOrgao()),
                formatacaoFactory.getBoldJustificado(12)));

        docx.addBreak();

        this.conjuntoPareceresCertidao = tabelasService.getConjuntoPareceresyCodigoIbge(codigoIBGE);
        int ultimoExercicio = conjuntoPareceresCertidao.getUltimoExercicio().getExercicio();
//        int ultimoExercicio = -1;
        int exercicioEmCurso = LocalDateTime.now().getYear();
        List<Integer> exerciciosNaoAnalisados = new ArrayList<>();

        for(int i = ultimoExercicio + 1; i < exercicioEmCurso; i++) {
            exerciciosNaoAnalisados.add(i);
        }
        int anoExercicioUltimoExercicio = conjuntoPareceresCertidao.getUltimoExercicio().getExercicio();
        String processoUltimoExercicio = conjuntoPareceresCertidao.getUltimoExercicio().getProcesso();

        introducaoUltimoExercicioAnalisado(conjuntoPareceresCertidao.getUltimoExercicio());
        if(uniao) {
            art11LRF(ultimoExercicio, false);
//            art11LRF(ultimoExercicio);
        }

        art12LRF(ultimoExercicio, 12, false);
        art23Executivo(ultimoExercicio, false);
        art23Legislativo(ultimoExercicio, false);
        art33LRF(ultimoExercicio, false);
        art37LRF(ultimoExercicio, false);
        art52LRF(ultimoExercicio, false);
        art55Executivo(ultimoExercicio, false);
        art55Legislativo(ultimoExercicio, false);
        if(uniao) {
            art198CF(ultimoExercicio, 12, false);
            art212CF(ultimoExercicio, 12, false);
        }



        introducaoGeralExercicioNaoAnalisado();
        for(TabelasParecer exerciciosNaoAnalisado: conjuntoPareceresCertidao.getExerciciosNaoAnalisados()) {
            int anoExercicio = exerciciosNaoAnalisado.getExercicio();
            String processoNaoAnalisado = exerciciosNaoAnalisado.getProcesso();
            introducaoExercicioNaoAnalisado(anoExercicio, processoNaoAnalisado);
            if(uniao) {
                art11LRF(anoExercicio, true);
            }
            art12LRF(anoExercicio, 12, true);
            art23Executivo(anoExercicio, true);
            art23Legislativo(anoExercicio, true);
            art52LRF(anoExercicio, true);
            art55Executivo(anoExercicio, true);
            art55Legislativo(anoExercicio, true);
            if(uniao) {
                art198CF(anoExercicio, 12, true);
                art212CF(anoExercicio, 12, true);
            }
        }

        int anoExercicioEmCurso = conjuntoPareceresCertidao.getExercicioEmCurso().getExercicio();
        introducaoExercicioEmCurso(conjuntoPareceresCertidao.getExercicioEmCurso());
        if(uniao) {
            art11LRFUltimoExercicio();
        }
        art23Executivo(anoExercicioEmCurso, true);
        art23Legislativo(anoExercicioEmCurso, true);
        art52LRF(anoExercicioEmCurso, true);
        art55Executivo(anoExercicioEmCurso, true);
        art55Legislativo(anoExercicioEmCurso, true);

        mensagemFechamento();

        return sendFile();
    }

    private void mensagemFechamento() {
        docx.addBreak();

        docx.addParagrafo(addTab().concat("À consideração de Vossa Senhoria.",
                formatacaoFactory.getRight(12)));

//        docx.addParagrafo(addTab().concat("UR/DF-XXX, em 16 de dezembro de 2019.",
//                formatacaoFactory.getRight(12)));

        LocalDateTime agora = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd 'de' MMMM 'de' yyyy");

        docx.addParagrafo(new TextoFormatado("XXXXXXXX\nUR/DF-XXX, em " + agora.format(formatter),
                formatacaoFactory.getCenter(12)));

        //        docx.addParagrafo(new TextoFormatado("Data: " +
//                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
//                formatacaoFactory.getRight(12)));

    }

    private void introducaoExercicioNaoAnalisado(int exercicioNaoAnalisado, String processoNaoAnalisado) {
        docx.addParagrafo(new TextoFormatado("EXERCÍCIO: "+ exercicioNaoAnalisado +"– TC-" + processoNaoAnalisado,
                formatacaoFactory.getBoldUnderline(12)));
    }

    private void introducaoGeralExercicioNaoAnalisado() {
        // CONTAS DOS EXERCÍCIOS AINDA NÃO ANALISADAS
       docx.addBreak();
       docx.addParagrafo(new TextoFormatado("INFORMAÇÕES RELATIVAS ÀS CONTAS DOS EXERCÍCIOS AINDA NÃO ANALISADAS ",
                formatacaoFactory.getBold(12))
                .concat("(Caso haja mais de um exercício não examinado, repetir as informaçôes seguintes para cada exercício)",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        docx.addBreak();
    }

    private void introducaoUltimoExercicioAnalisado(TabelasParecer parecer) {

        List<ParecerPrefeitura> parecerPrefeituras = parecerPrefeituraService.getParecerByCodigoIbge(this.codigoIBGE);
        ParecerPrefeitura parecerPrefeitura = parecerPrefeituras.stream().filter(x->x.getExercicio().intValue() == parecer.getExercicio().intValue()).findAny().orElse(null);


        docx.addParagrafo(new TextoFormatado("INFORMAÇÕES RELATIVAS  AS CONTAS DO ÚLTIMO  EXERCÍCIO ANALISADO",
                formatacaoFactory.getBoldJustificado(12)));
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("EXERCÍCIO: "+ parecer.getExercicio()+ "- TC-" +
                parecer.getProcesso(), formatacaoFactory.getBold(12)));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        docx.addParagrafo(new TextoFormatado("Parecer publicado no D.O.E. em: " + parecer.getPublicacaoDO().format(formatter) +
            " - " + parecerPrefeitura.getParecer() , formatacaoFactory.getJustificado(12)));

        docx.addBreak();
    }

    private void introducaoExercicioEmCurso(TabelasParecer parecer) {
        docx.addParagrafo(new TextoFormatado("INFORMAÇÕES RELATIVAS ÀS CONTAS DO  EXERCÍCIO EM CURSO",
                formatacaoFactory.getBoldJustificado(12)));
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("EXERCÍCIO:" + parecer.getExercicio() +
                " – TC-" + parecer.getProcesso() + " - " +
                "informações até o "+ getQuadrimestreDoMes(LocalDateTime.now().getMonthValue()) +"º QUADRIMESTRE/"+parecer.getExercicio() , formatacaoFactory.getBold(12)));

        docx.addBreak();
    }

    private void art212CF(Integer exercicio, Integer mesReferencia, boolean calcular) {

        //Art. 212
        Map<String, String> mapa = audespEnsinoService
                .getPencentualAplicadoEnsino(this.codigoIBGE, exercicio, mesReferencia);

        docx.addParagrafo(new TextoFormatado("Art. 212 da CF", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL", formatacaoFactory.getBoldUnderline(12)));

        String texto = mapa.get("vPercEmpEnsino" + exercicio + mesReferencia + "texto");
        String textoFixo = ("(cumprindo, não cumprindo)");
        String textoPercentual = mapa.get("vPercEmpEnsino" + exercicio + mesReferencia);
        if(!calcular || texto == null || texto.isEmpty()) {
            texto = textoFixo;
            textoPercentual = "__%";
        }

        docx.addParagrafo(addTab().concat("O ente aplicou " +
                textoPercentual + " das receitas de impostos em gastos com ensino, " +
                texto + ", portanto, o Art. 212 da Constituição Federal.", formatacaoFactory.getJustificado(12)));

        docx.addBreak();
    }

    private void art198CF(Integer exercicio, Integer mesReferencia, boolean calcular) {

        //Art. 198
        Map<String, String> mapa = audespSaudeService
                .getPencentualAplicadoSaude(this.codigoIBGE, exercicio, mesReferencia);

        docx.addParagrafo(new TextoFormatado("Art. 198 da CF", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL", formatacaoFactory.getBoldUnderline(12)));

        String texto = mapa.get("vPercEmpSaude" + exercicio + mesReferencia + "texto");
        String textoFixo = ("(cumprindo, não cumprindo)");
        String textoPecentual = mapa.get("vPercEmpSaude" + exercicio + mesReferencia);
        if(!calcular || texto == null || texto.isEmpty()) {
            texto = textoFixo;
            textoPecentual = "__%";
        }

        docx.addParagrafo(addTab().concat("O ente aplicou " +
                textoPecentual +
                " das receitas de impostos em gastos da saúde, " +
                texto +  ", portanto, o Art. 198 da Constituição Federal. ",
                formatacaoFactory.getJustificado(12)));
        docx.addBreak();
    }

    private void art55Legislativo(Integer exercicio, boolean calcular) {
        //Art. 55 LRF - §2º - Legislativo
        List<AudespPublicacao> publicacoesEmAtraso =  audespRGFService.getSituacaoEntregaDocumentosRGF(codigoIBGE, exercicio, "Legislativo");
        publicacoesEmAtraso.removeIf(Objects::isNull);
        boolean cumpriuFlag = publicacoesEmAtraso.isEmpty();
        String cumpriuOuNao = cumpriuFlag == true ? "Cumpriu" : "Não cumpriu";
        if(!calcular)
            cumpriuOuNao = "_____________ (Cumpriu/Não cumpriu)";
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55 – da LRF ", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("LEGISLATIVO", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado(cumpriuOuNao + " o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - RGF, ")
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12)));


        if(!publicacoesEmAtraso.isEmpty() && calcular) {
            docx.addBreak();
            addTabelaPublicaoEmAtraso(publicacoesEmAtraso);
            docx.addBreak();
        }

        docx.addBreak();
//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÃO: Caso algum relatório não tenha sido publicado " +
//                "(ou divulgado) no prazo legal, indicar qual quadrimestre, especificando ainda se efetuou a publicação " +
//                "a posteriori.", formatacaoFactory.getJustificadoFundoAmarelo(12)));

        docx.addBreak();
    }

    private void art55Executivo(Integer exercicio, boolean calcular) {
        List<AudespPublicacao> publicacoesEmAtraso =  audespRGFService.getSituacaoEntregaDocumentosRGF(codigoIBGE, exercicio, "Legislativo");
        publicacoesEmAtraso.removeIf(Objects::isNull);
        boolean cumpriuFlag = publicacoesEmAtraso.isEmpty();
        String cumpriuOuNao = cumpriuFlag == true ? "Cumpriu" : "Não cumpriu";
        if(!calcular)
            cumpriuOuNao = "_____________ (Cumpriu/Não cumpriu)";

        //Art. 55 LRF - §2º - Executivo
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55 – da LRF ", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("EXECUTIVO", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado(cumpriuOuNao + " o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - RGF, ")
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12)));
        docx.addBreak();

        if(!publicacoesEmAtraso.isEmpty() && calcular) {
            docx.addBreak();
            addTabelaPublicaoEmAtraso(publicacoesEmAtraso);
            docx.addBreak();
        }

//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÃO: Caso algum relatório não tenha sido publicado " +
//                "(ou divulgado) no prazo legal, indicar qual quadrimestre, especificando ainda se efetuou a publicação " +
//                "a posteriori.", formatacaoFactory.getJustificadoFundoAmarelo(12)));
        docx.addBreak();
    }

    private void art52LRF(int exercicio, boolean calcular) {
        //Art.52 LRF
        List<AudespPublicacao> publicacoesEmAtraso = audespRGFService.getSituacaoEntregaDocumentosRREO(codigoIBGE, exercicio, 50);
        publicacoesEmAtraso.removeIf(Objects::isNull);
        boolean cumpriuFlag = publicacoesEmAtraso.isEmpty();
        String cumpriuOuNao = cumpriuFlag == true ? "Cumpriu" : "Não cumpriu";

        if(!calcular)
            cumpriuOuNao = "_____________ (Cumpriu/Não cumpriu)";

        docx.addParagrafo(new TextoFormatado("Art. 52 da LRF", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();

        docx.addParagrafo(addTab().concat(cumpriuOuNao + " o prazo legal para publicação do " +
                "Relatório Resumido de Execução Orçamentária - RREO.", formatacaoFactory.getJustificado(12)));
        docx.addBreak();

        if(!publicacoesEmAtraso.isEmpty() && calcular) {
            docx.addBreak();
            addTabelaPublicaoEmAtraso(publicacoesEmAtraso);
            docx.addBreak();
        }

//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÃO: Caso algum relatório não tenha sido publicado no prazo " +
//                "legal, indicar qual bimestre, especificando ainda, se efetuou a publicação a posteriori.",
//                formatacaoFactory.getJustificadoFundoAmarelo(10)));
        docx.addBreak();
    }

    private void art37LRF(int exercicio, boolean calcular) {
        //Art.37 LRF
        Map<String, String> constaOuNao = new HashMap<>();
        if(calcular)
            constaOuNao = audespOperacoesDeCreditoService.getGF31Garantias(this.codigoIBGE, exercicio, 12);
        String texto = "______________ (Consta/Não consta)";
        if(constaOuNao.get("art37") != null && !constaOuNao.get("art37").isEmpty())
            texto = constaOuNao.get("art37");

        docx.addParagrafo(new TextoFormatado("Art. 37 da LRF", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();



        docx.addParagrafo(new TextoFormatado( texto + " ocorrências de captação de " +
                "recursos ou assunção de compromissos com características similares às descritas no inciso I a III do " +
                "art. 5º da Resolução nº 43/01, do Senado Federal e no art. 37 da Lei de Responsabilidade Fiscal, de " +
                "acordo com os exames realizados.", formatacaoFactory.getJustificado(12)));
        docx.addBreak();

//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÃO: Em caso positivo, indique a data da realização, o " +
//                "valor, o credor e as providências adotadas para regularização.",
//                formatacaoFactory.getJustificadoFundoAmarelo(10)));
        docx.addBreak();
    }

    private void art33LRF(int exercicio, boolean calcular) {
        //Art.33 LRF
        Map<String, String> realizouNaoRealizou = new HashMap<>();
        if(calcular)
            realizouNaoRealizou = audespOperacoesDeCreditoService.getOperacoesArt33(this.codigoIBGE, exercicio, 12);
        String texto = "";
        if(realizouNaoRealizou.get("art33") == null)
            texto = "______________ (Realizou/Não Realizou)";
        else
            texto = realizouNaoRealizou.get("art33");

        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("Art. 33 da LRF", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("O Município " + texto + " operações de " +
                "crédito irregulares, de acordo com os exames realizados.", formatacaoFactory.getJustificado(12)));
        docx.addBreak();



        docx.addBreak();
//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÃO: Em caso positivo, informar se efetuou o cancelamento, " +
//                "a amortização ou a constituição de reserva para regularização.",
//                formatacaoFactory.getJustificadoFundoAmarelo(10)));

        docx.addBreak();
    }

    private void art23Legislativo(int exercicio, boolean calcular) {
        // Art. 23 - Legislativo - Ultimo Exercício

        List<ParecerPrefeitura> parecerPrefeituras = parecerPrefeituraService.getParecerByCodigoIbge(this.codigoIBGE);

        TabelasProtocolo protocolo = tabelasService.getTabelasProtocoloCamaraByCodigoIbge(this.codigoIBGE, exercicio);
//        ParecerPrefeitura parecerPrefeitura = parecerPrefeituras.stream().filter(x->x.getExercicio().intValue() == exercicio).findAny().orElse(null);


        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("Art. 23", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("LEGISLATIVO - TC-"+ protocolo.getProcesso(), formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo(new TextoFormatado("O Legislativo registrou os seguintes percentuais de gasto de " +
                "pessoal, conforme dados abaixo informados no Relatório de Gestão Fiscal - RGF:",
                formatacaoFactory.getJustificado(12)));

        docx.addBreak();

        this.addTabelaPessoalLegislativoArt23(exercicio, calcular);
        docx.addBreak();

//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÕES: \n• Se registrar índice superior ao estabelecido, " +
//                "indique o quadrimestre. \n" +
//                "• Indique se a Câmara acha-se no período de ajuste para regularização do excesso, ou especifique " +
//                "se foi extrapolado o período sem as providências de ajustes cabíveis.",
//                formatacaoFactory.getJustificadoFundoAmarelo(10)));
        docx.addBreak();

    }

    private void art23Executivo(int exercicio, boolean calcular) {
        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("Art. 23 ", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("EXECUTIVO", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();

        docx.addParagrafo(addTab().concat("O Executivo registrou os seguintes percentuais de gasto de pessoal, " +
                "conforme dados abaixo informados no Relatório de Gestão Fiscal - RGF:",
                formatacaoFactory.getJustificado(12)));

        docx.addBreak();
        this.addTabelaPessoalExecutivoArt23(exercicio, calcular);
        docx.addBreak();

//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÕES: \n• Se registrar índice superior ao estabelecido, " +
//                "indique o quadrimestre. \n" +
//                "• Indique se a Prefeitura acha-se no período de ajuste para regularização do excesso, ou especifique " +
//                "se foi extrapolado o período sem as providências de ajustes cabíveis.",
//            formatacaoFactory.getJustificadoFundoAmarelo(10)));
    }

    private void art11LRF(Integer exercicio, boolean calcular) {
        String cumpriuNaoCumpriu = "";

        if(!calcular)
            cumpriuNaoCumpriu = "____________ (Cumpriu, Não cumpriu)";
        docx.addParagrafo(new TextoFormatado("Art. 11 da LRF", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();

        docx.addParagrafo("____________ (Cumpriu, Não cumpriu) o Art. 11 da Lei de Responsabilidade Fiscal uma " +
                "vez que o ente exerceu pleno cumprimento das competências tributárias.");

        docx.addBreak();

    }

    private void art11LRFUltimoExercicio() {
        docx.addParagrafo(new TextoFormatado("Art. 11 da LRF", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL", formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();

        docx.addParagrafo(addTab().concat("O cumprimento do Art. 11 da Lei de Responsabilidade Fiscal não faz parte do rol das " +
                "análises quadrimestrais do exercício em curso, sendo aferido somente no relatório final de fechamento " +
                "do exercício. Dessa forma, conforme consta no MIP-STN-2019, página 101, a comprovação do atendimento " +
                "às exigências do Art.11 da LRF poderá ser efetuada por meio de declaração do Chefe Poder Executivo, " +
                "conforme orientação constante dos Pareceres: PGFN/COF/N° 468/2017, de 14/04/2017; e PGFN/COF/N° 1063/2017, " +
                "de 24/07/2017.", formatacaoFactory.getJustificado(12)));

        docx.addBreak();

    }



    private void art12LRF(Integer exercicio, Integer mesReferencia, boolean calcular) {
        //Art. 198
        Map<String, String> mapa = audespLimiteLrfService.getRegraDeOuro(this.codigoIBGE, exercicio, mesReferencia);

        String texto = mapa.get("vResOpCredAju" + exercicio + mesReferencia + "texto");
        String textoFixo = ("____________ (superior, inferior)");
        if( !calcular || (exercicio == null || texto == null || texto.isEmpty())) {
            texto = textoFixo;
        }

        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 12 da LRF (Inciso III do Art. 167 da CF)",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE MUNICIPAL",
                formatacaoFactory.getFormatacaoBoldUnderlineCaps(12)));
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("O montante previsto para as receitas de operações de crédito no exercício" +
                " analisado foi " + texto + " ao montante das despesas de capital constante da Lei Orçamentária."));

        docx.addBreak();
//        docx.addParagrafo(new TextoFormatado("OBSERVAÇÃO: Se superior, COMPLEMENTAR: (há) OU (não há)  autorização" +
//                " do Legislativo, por maioria absoluta, nos termos excetuados pelo inciso III, artigo 167, da Constituição",
//                formatacaoFactory.getJustificadoFundoAmarelo(10)));
    }
}