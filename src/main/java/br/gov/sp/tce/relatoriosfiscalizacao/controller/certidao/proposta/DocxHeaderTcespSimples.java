package br.gov.sp.tce.relatoriosfiscalizacao.controller.certidao.proposta;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.InputStream;

public class DocxHeaderTcespSimples implements DocxHeader {


    private XWPFDocument document;

    public DocxHeaderTcespSimples(XWPFDocument document) {
        if(document == null)
            throw new IllegalArgumentException();
        this.document = document;
    }

    public void getHeader()   {
        try {
            // Header
            CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
            XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
            XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

            CTP ctpFooter = CTP.Factory.newInstance();
            CTR ctrFooter = ctpFooter.addNewR();
            CTText ctFooter = ctrFooter.addNewT();

            XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
            footerParagraph.setAlignment(ParagraphAlignment.RIGHT);
            XWPFRun footerRun = footerParagraph.createRun();
            footerRun.setFontFamily("Arial");
            footerRun.setFontSize(8);
            footerRun.getCTR().addNewPgNum();

            XWPFParagraph[] parsFooter = new XWPFParagraph[1];
            parsFooter[0] = footerParagraph;
            headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

            XWPFParagraph headerPar = header.createParagraph();
            XWPFRun headerParRun = headerPar.createRun();

            headerParRun.setFontFamily("Arial");
            headerParRun.setFontSize(12);
            headerPar.setAlignment(ParagraphAlignment.RIGHT);

            XWPFTable headerTable = header.createTable(1, 3);
            headerTable.removeBorders();
            headerTable.setWidthType(TableWidthType.PCT);
            headerTable.setWidth("98%");

            XWPFTableRow headerTableRow = headerTable.getRow(0);

            //coluna 1
            headerTableRow.getCell(0).removeParagraph(0);
            XWPFParagraph pImgEsquerda = headerTableRow.getCell(0).addParagraph();
            pImgEsquerda.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun runPImgEsquerda = pImgEsquerda.createRun();

            ResourceLoader resourceLoader = new DefaultResourceLoader();
            Resource resourceBrasaoSP = resourceLoader.getResource("classpath:static/imagens/brasao_sp.png");

            InputStream brasao_sp = resourceBrasaoSP.getInputStream();
            runPImgEsquerda.addPicture(brasao_sp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_sp.png",
                    Units.toEMU(50), Units.toEMU(55));

            //coluna 2
            headerTableRow.getCell(1).removeParagraph(0);
            XWPFParagraph p1 = headerTableRow.getCell(1).addParagraph();
            p1.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun p1Run = p1.createRun();

            p1Run.setFontFamily("Arial");
            p1Run.setFontSize(12);
            p1Run.setBold(true);
            p1Run.setText("TRIBUNAL DE CONTAS DO ESTADO DE SÃO PAULO");

            XWPFParagraph p2 = headerTableRow.getCell(1).addParagraph();
            p2.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun p2Run = p2.createRun();
            p2Run.setFontSize(12);
            p2Run.setFontFamily("Arial");
            p2Run.setText("");

            //coluna 3
            headerTableRow.getCell(2).removeParagraph(0);
            XWPFParagraph pImgDireita = headerTableRow.getCell(2).addParagraph();
            pImgDireita.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun runPImgDireita = pImgDireita.createRun();
            Resource resourceBrasaoTcesp = resourceLoader.getResource("classpath:static/imagens/brasao_tcesp.png");

            InputStream brasao_tcesp = resourceBrasaoTcesp.getInputStream();
            runPImgDireita.addPicture(brasao_tcesp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_tcesp.png", Units.toEMU(50), Units.toEMU(55));
        } catch(Exception exception) {
            System.out.println(exception.getCause());
            exception.printStackTrace();
        }
    }
}
