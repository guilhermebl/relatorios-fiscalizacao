
package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespLimiteLrf;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultadoExecucaoOrcamentaria;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespLimiteLrfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespLimiteLrfService {

    @Autowired
    private AudespLimiteLrfRepository audespLimiteLrfRepository;

    public Map<String, String> getAudespLimiteLrf(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespLimiteLrf>
                audespLimiteLrfList = audespLimiteLrfRepository
                .getAudespLimiteLrf(codigoIbge, exercicio, mesReferencia);

        BigDecimal rcl = new BigDecimal(0);
        Map<String, String> mapLimiteLrf = new HashMap<>();
        for(AudespLimiteLrf resultado: audespLimiteLrfList) {
            if(resultado.getNomeParametroAnalise().equals("vPercDivConsolidLiq")) {
                mapLimiteLrf.put(resultado.getNomeParametroAnalise(), resultado.getValorPercentual());
            } else if(resultado.getNomeParametroAnalise().equals("vRCL")) {
                rcl = resultado.getValor();
                mapLimiteLrf.put(resultado.getNomeParametroAnalise(), resultado.getValorString());

                // Limite Legal Divida COnsolidada Liquida
                String limiteLegalDividaConsolidadaLiquida = resultado.getValor().multiply(new BigDecimal(1.2))
                        .setScale(2, BigDecimal.ROUND_HALF_DOWN)
                        .toString();
                mapLimiteLrf.put("limiteLegalDividaConsolidadaLiquida", limiteLegalDividaConsolidadaLiquida);
            }  else {
                mapLimiteLrf.put(resultado.getNomeParametroAnalise(), resultado.getValorString());
            }
        }


        for(AudespLimiteLrf resultado: audespLimiteLrfList) {
            if(resultado.getNomeParametroAnalise().equals("vLimConcGar")) {
                BigDecimal percentualLimiteGarantiaLRF = resultado.getValor().divide(rcl, BigDecimal.ROUND_DOWN).setScale(2, BigDecimal.ROUND_DOWN);
                mapLimiteLrf.put("percentualLimiteGarantiaLRF", percentualLimiteGarantiaLRF.toString().replace(".", ","));
            } else if(resultado.getNomeParametroAnalise().equals("vLimConcGar")) {

            }
        }
        if(mapLimiteLrf.get("vRCL") != null) {

        }

        return mapLimiteLrf;

    }




    public Map<String, String> getAudespResultadoInfluenciaOrcamentarioFinanceiro(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespLimiteLrf>
                audespResultadoLimiteLrf = audespLimiteLrfRepository
                .getAudespLimiteLrf(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosMoeda = Arrays.asList(new String[]{"vDivConsolidLiq", "vLimConcGar", "vLimOpCred","vDespCapFix",
                "vARO", "vRCL", "vOpCred", "vConcGar"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vPercDivConsolidLiq"});

        BigDecimal rclPerc = new BigDecimal(1);
        BigDecimal divConsolidPercLimite = new BigDecimal(1.2);
        BigDecimal concGarantiaPercLimite = new BigDecimal(0.22);
        BigDecimal opCreditoExcAroPercLimite = new BigDecimal(0.16);
        BigDecimal opAntecipacaoAroPercLimite = new BigDecimal(0.07);

        BigDecimal rcl = null;
        BigDecimal divConsolidLimite = null;
        BigDecimal concGarantiaLimite = null;
        BigDecimal opCreditoExcAroLimite = null;
        BigDecimal opAntecipacaoAroLimite = null;

        Map<String, String> mapResultadoLimiteLrf = new HashMap<>();
        for (AudespLimiteLrf resultado : audespResultadoLimiteLrf) {
            if (resultado.getValor() != null && parametrosMoeda.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoLimiteLrf.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), false, false));
            } else if(resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoLimiteLrf.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), false, false));
            }

            if(resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vRCL")) {
                rcl = resultado.getValor();
                divConsolidLimite = rcl.multiply(divConsolidPercLimite);
                concGarantiaLimite = rcl.multiply(concGarantiaPercLimite);
                opCreditoExcAroLimite = rcl.multiply(opCreditoExcAroPercLimite);
                opAntecipacaoAroLimite = rcl.multiply(opAntecipacaoAroPercLimite);

                mapResultadoLimiteLrf.put("divConsolidLimite", FormatadorDeDados.formatarMoeda(divConsolidLimite, false, false));
                mapResultadoLimiteLrf.put("concGarantiaLimite", FormatadorDeDados.formatarMoeda(concGarantiaLimite, false, false));
                mapResultadoLimiteLrf.put("opCreditoExcAroLimite", FormatadorDeDados.formatarMoeda(opCreditoExcAroLimite, false, false));
                mapResultadoLimiteLrf.put("opAntecipacaoAroLimite", FormatadorDeDados.formatarMoeda(opAntecipacaoAroLimite, false, false));


            }
        }

        for (AudespLimiteLrf resultado : audespResultadoLimiteLrf) {
            if(resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vDivConsolidLiq")) {
                mapResultadoLimiteLrf.put("excessoDividaConsolidada",
                        FormatadorDeDados.formatarMoeda(calculaExcessoaRegularizar(resultado.getValor(),
                                divConsolidLimite), false, true));
            }
            if(resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vConcGar")) {
                mapResultadoLimiteLrf.put("excessoConcessaoGarantias",
                        FormatadorDeDados.formatarMoeda(calculaExcessoaRegularizar(resultado.getValor(),
                                concGarantiaLimite), false, true));
            }
            if(resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vOpCred")) {
                mapResultadoLimiteLrf.put("excessoOperacoesCredito",
                        FormatadorDeDados.formatarMoeda(calculaExcessoaRegularizar(resultado.getValor(),
                                opCreditoExcAroLimite), false, true));
                mapResultadoLimiteLrf.put("opCredPercentual",
                        FormatadorDeDados.formatarPercentual(calculaPercentualRcl(rcl, resultado.getValor()), false, true));
            }
            if(resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vARO")) {
                mapResultadoLimiteLrf.put("excessoARO",
                        FormatadorDeDados.formatarMoeda(calculaExcessoaRegularizar(resultado.getValor(),
                                opAntecipacaoAroLimite), false, true));
            }
        }

        mapResultadoLimiteLrf.put("rclPerc", FormatadorDeDados.formatarPercentual(rclPerc, false, false ));
        mapResultadoLimiteLrf.put("divConsolidPercLimite", FormatadorDeDados.formatarPercentual(divConsolidPercLimite, false, false ));
        mapResultadoLimiteLrf.put("concGarantiaPercLimite", FormatadorDeDados.formatarPercentual(concGarantiaPercLimite, false, false ));
        mapResultadoLimiteLrf.put("opCreditoExcAroPercLimite", FormatadorDeDados.formatarPercentual(opCreditoExcAroPercLimite, false, false ));
        mapResultadoLimiteLrf.put("opAntecipacaoAroPercLimite", FormatadorDeDados.formatarPercentual(opAntecipacaoAroPercLimite, false, false ));

        return mapResultadoLimiteLrf;
    }

    public BigDecimal calculaExcessoaRegularizar(BigDecimal valorAMaior, BigDecimal valorReferencia) {
        if(valorAMaior == null || valorReferencia == null || valorReferencia.equals(0)){
            return null;
        }

        if(valorAMaior.compareTo(valorAMaior) > 0) {
            return valorAMaior.subtract(valorReferencia);
        }
        return null;
    }

    public BigDecimal calculaPercentualRcl(BigDecimal rcl, BigDecimal valor) {
        if(rcl == null || valor == null || rcl.equals(0) || valor.equals(0)){
            return null;
        }
        return valor.divide(rcl, 4, RoundingMode.DOWN);
    }

    public Map<String, String> getRegraDeOuro(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultado> audespRegraDeOuro = audespLimiteLrfRepository.getRegraDeOuro(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosMoeda = Arrays.asList(new String[]{"vResOpCredAju"});

        Map<String, String> mapa = new HashMap<>();

        for (AudespResultado regraDeOuro : audespRegraDeOuro) {
            if (regraDeOuro.getValor() != null && parametrosMoeda.contains(regraDeOuro.getNomeParametroAnalise())) {
                mapa.put(regraDeOuro.getNomeParametroAnalise() + regraDeOuro.getExercicio() + regraDeOuro.getMes(),
                        FormatadorDeDados.formatarPercentual(regraDeOuro.getValor(), true, false, 2));
                int diferencaOpCreditoDespCapital = regraDeOuro.getValor().compareTo(new BigDecimal("0"));
                if( diferencaOpCreditoDespCapital <= 0) {
                    mapa.put(regraDeOuro.getNomeParametroAnalise() + regraDeOuro.getExercicio() + regraDeOuro.getMes() + "texto", "inferior");
                } else if(diferencaOpCreditoDespCapital == 1) {
                    mapa.put(regraDeOuro.getNomeParametroAnalise() + regraDeOuro.getExercicio() + regraDeOuro.getMes() + "texto", "superior");
                }
            }
        }
        return mapa;
    }


}