package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.repository;


import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.*;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TcespBiRepository {

    @PersistenceContext(unitName = "tcespbi")
    private EntityManager entityManager;

    public List<TcespBiRclMensal> getRCLMunicipio(Integer skMunicipio, Integer exercicio) {
        Query query =  entityManager.createNativeQuery(
                "select mes,ano,valor " +
                        "from audesp_calc.rcl " +
                        "where 1=1 " +
                        "and " +
                        "(  mes = 11 and ano = :exercicio -1 " +
                        "or mes = 12 and ano = :exercicio -1 " +
                        "or mes = 1 and ano = :exercicio " +
                        "or mes = 2 and ano = :exercicio " +
                        "or mes = 3 and ano = :exercicio  " +
                        "or mes = 4 and ano = :exercicio  " +
                        "or mes = 5 and ano = :exercicio " +
                        "or mes = 6 and ano = :exercicio " +
                        "or mes = 7 and ano = :exercicio " +
                        "or mes = 8 and ano = :exercicio " +
                        "or mes = 9 and ano = :exercicio " +
                        "or mes = 10 and ano = :exercicio " +
                        ") and municipio_sk = :skMunicipio  ", TcespBiRclMensal.class);
        query.setParameter("exercicio", exercicio);
        query.setParameter("skMunicipio", skMunicipio);
        List<TcespBiRclMensal> lista = query.getResultList();
        for (int i = lista.size(); i < 7; i++)
            lista.add(new TcespBiRclMensal());
        return lista;
    }

    public AlicotaEntidadeDevedora getAlicotaEntidadeDevedora(Integer codigoIBGE) {
        Query query = entityManager.createNativeQuery("select tk_alicota, " +
                "entidade, regime, alicota, " +
                "tipo_alicota, ano, mes, codigo_ibge, tipo_entidade " +
                "from painel_municipio.alicotas_entidades_devedoras " +
                "where tipo_alicota = 'percentual' " +
                "and tipo_entidade = 1 " +
                "and codigo_ibge = :codigoIBGE ", AlicotaEntidadeDevedora.class);
        query.setParameter("codigoIBGE", codigoIBGE);

        AlicotaEntidadeDevedora alicotaEntidadeDevedora = (AlicotaEntidadeDevedora) query.getResultList()
                .stream().findFirst().orElse(null);
        if(alicotaEntidadeDevedora == null) {
            alicotaEntidadeDevedora = new AlicotaEntidadeDevedora();
        }
        return alicotaEntidadeDevedora;
    }

    public TaxaInvestimento getTaxaInvestimento(Integer codigoIBGE, Integer exercicio) {
        Query query = entityManager.createNativeQuery("select  " +
                "codigo_ibge " +
                ", exercicio " +
                ",desp_liquidada " +
                ",desp_empenhada " +
                ",rec_total " +
                ",perc_taxa " +
                ",rcl " +
                ",perc_taxa_rp " +
                "from audesp_calc.taxa_investimento  " +
                "where 1=1 " +
                "and codigo_ibge = :codigoIBGE " +
                "and exercicio = :exercicio", TaxaInvestimento.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);

        TaxaInvestimento taxaInvestimento = (TaxaInvestimento) query.getResultList()
                .stream().findFirst().orElse(new TaxaInvestimento());

        return taxaInvestimento;
    }

    public MunicipioIbge getMunicipioByCodigoIbgeExercicio(Integer codigoIBGE, Integer exercicio) {
        Query query =  entityManager.createNativeQuery("SELECT codigo_ibge  as codigoMunicipioIbge, " +
                "populacao as populacao " +
                "from painel_municipio.dados_ibge " +
                "where codigo_ibge = :codigoIBGE  and exercicio = :exercicio " +
                "limit 1", MunicipioIbge.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<MunicipioIbge>  lista = query.getResultList();

        if(lista.size() == 0)
            return new MunicipioIbge();
        else
            return lista.get(0);
    }

    public DadosResolucao_43_21_Senado getDadosResolucao_43_21_Senado(Integer codigoIBGE, Integer exercicio, Integer tipoEntidade) {
        Query query =  entityManager.createNativeQuery("SELECT id, nome_orgao, codigo_tipo_orgao, resultado_julgamento, "+
                "tc, tc_numero, exercicio, transitou_efetivamente,data_transito_em_julgado," +
                        "data_publicacao_do_transito_em_julgado,codigo_ibge " +
                " FROM paineis.resolucao_43_21_senado " +
                " where 1=1 " +
                " and codigo_ibge = :codigoIBGE  " +
                " and  exercicio =   :exercicio " +
                " and codigo_tipo_orgao = :tipoEntidade " +
                " limit 1 ", DadosResolucao_43_21_Senado.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("tipoEntidade", tipoEntidade);

        DadosResolucao_43_21_Senado dadosResolucao_43_21_Senado = (DadosResolucao_43_21_Senado) query.getResultList()
                .stream().findFirst().orElse(new DadosResolucao_43_21_Senado());
        return dadosResolucao_43_21_Senado;
    }

}