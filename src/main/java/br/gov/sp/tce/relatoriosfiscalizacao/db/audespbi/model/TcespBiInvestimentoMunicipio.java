package br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class TcespBiInvestimentoMunicipio {

    @Id
    @Column(name = "ano")
    private Integer exercicio;

    @Column (name = "investimento_municipio")
    private BigDecimal investimentoMunicipio;

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public BigDecimal getInvestimentoMunicipio() {
        return investimentoMunicipio;
    }

    public void setInvestimentoMunicipio(BigDecimal investimentoMunicipio) {
        this.investimentoMunicipio = investimentoMunicipio;
    }
}
