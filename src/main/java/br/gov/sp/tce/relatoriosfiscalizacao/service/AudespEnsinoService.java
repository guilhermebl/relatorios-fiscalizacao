package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.*;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespEnsinoRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.repository.ParecerPrefeituraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;

@Service
public class AudespEnsinoService {

    @Autowired
    private AudespEnsinoRepository audespEnsinoRepository;

    public List<AudespEnsino> getAudespEnsinoByCodigoIbge(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespEnsino> audespEnsinoList = audespEnsinoRepository.getAudespEnsinoByCodigoIbge(codigoIbge, exercicio, mesReferencia);
        return audespEnsinoList;
    }

    public Map<String, String> getAudespEnsinoFundeb(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespEnsino> audespEnsinoList = audespEnsinoRepository.getAudespEnsinoFundeb(codigoIbge, exercicio, mesReferencia);

        List<String> valoresParam = new ArrayList<>();
        //valoresParam.add("vRecFundeb");
        valoresParam.add("vDespLiqAplicFundeb");
        valoresParam.add("vDespLiqAplicFundebMagist");
        valoresParam.add("vDespPagaAplicFundeb");
        valoresParam.add("vDespPagaAplicFundebMagist");

        AudespEnsino receitaFundeb = new AudespEnsino();
        for (AudespEnsino audespEnsino : audespEnsinoList) {
            if (audespEnsino.getNomeParametroAnalise().equals("vRecFundeb"))
                receitaFundeb = audespEnsino;
        }

        for (AudespEnsino audespEnsino : audespEnsinoList) {
            if (valoresParam.contains(audespEnsino.getNomeParametroAnalise())) {
                BigDecimal valor = new BigDecimal(0);
                if (receitaFundeb.getValor().intValue() != 0)
                    valor = audespEnsino.getValor().divide(receitaFundeb.getValor(), 4, RoundingMode.HALF_UP);
                audespEnsino.setValor(valor);
            }
        }

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vPercEmpEnsino", "vPercLiqEnsino", "vPercPagoEnsino"});


        Map<String, String> mapResultadoEnsino = new HashMap<>();
        for (AudespEnsino resultado : audespEnsinoList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoEnsino.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), false, false, 2));
            } else {
                mapResultadoEnsino.put(resultado.getNomeParametroAnalise(), resultado.getValorPercentual());
            }
        }

        return mapResultadoEnsino;
    }


    public Map<String, String> getQuadroGeralEnsinoFormatado(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespEnsino> quadroGeralEnsinoList = audespEnsinoRepository.getQuadroGeralEnsino(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosValores = Arrays.asList(new String[]{"vRecImpEducArrec","vTotRecImpEdu",
                "vFundebRetido","vFundebRecebido", "vFundebRecApliFin", "vFundebRecTot",
                "vDespEmpFundebMagist", "vFundebDespMagTot",
                "vDespEmpFundebOutros", "vDemDespEduTot",
                "vDespEduBas", "vFundebRetido", "vGanApliFinEdu", "vFundebRetidoNaoAplicadoEmp", "vApliArt212Edu",
                "vFundebApliTri1", "vRPNPagoEdu", "vRecPropFundebAjuAud",
                "vApliFinEduBas",
                "vRecImpEducPrevAtu", "vDotAtuEnsino"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vFundebDespPerc", "vDemDespEduPerc", "vApliArt212EduPerc", "vApliFinEduBasPerc", "vPercPrevAtuEnsino"});
        Map<String, String> mapa = new HashMap<>();
        for (AudespEnsino resultado : quadroGeralEnsinoList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false, 2));


            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise(), FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));

            }
        }

        return mapa;
    }

    public Map<String, BigDecimal> getQuadroGeralEnsino(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespEnsino> quadroGeralEnsinoList = audespEnsinoRepository.getQuadroGeralEnsino(codigoIbge, exercicio, mesReferencia);

        Map<String, BigDecimal> mapa = new HashMap<>();
        for (AudespEnsino resultado : quadroGeralEnsinoList) {
            if (resultado.getValor() != null ) {
                mapa.put(resultado.getNomeParametroAnalise(), resultado.getValor());
            }
        }

        return mapa;
    }

    public Map<String, String> getPencentualAplicadoEnsino(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultado> audespEnsinoList = audespEnsinoRepository.getPencentualAplicadoEnsino(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vPercEmpEnsino"});

        Map<String, String> mapa = new HashMap<>();

        for (AudespResultado ensino : audespEnsinoList) {
            if (ensino.getValor() != null && parametrosPercentuais.contains(ensino.getNomeParametroAnalise())) {
                mapa.put(ensino.getNomeParametroAnalise() + ensino.getExercicio() + ensino.getMes(),
                        FormatadorDeDados.formatarPercentual(ensino.getValor(), true, false, 2));
                int comp25 = ensino.getValor().compareTo(new BigDecimal("0.25"));
                if( comp25 >= 0) {
                    mapa.put(ensino.getNomeParametroAnalise() + ensino.getExercicio() + ensino.getMes() + "texto", "cumprindo");
                } else if(comp25 == -1) {
                    mapa.put(ensino.getNomeParametroAnalise() + ensino.getExercicio() + ensino.getMes() + "texto", "não cumprindo");
                }
            }
        }
        return mapa;
    }
}
