package br.gov.sp.tce.relatoriosfiscalizacao.controller._test;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/test")
    public ResponseEntity<String> testConnection() {
        return ResponseEntity.ok().body("SERVER RUNNING...");
    }
}
