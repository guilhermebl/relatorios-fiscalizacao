package br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MunicipioIbge {

    @Id
    private Integer codigoMunicipioIbge;


    private String populacao;

//    @Column(name = "localidade")
//    private String localidade;

    public Integer getCodigoMunicipioIbge() {
        return codigoMunicipioIbge;
    }

    public void setCodigoMunicipioIbge(Integer codigoMunicipioIbge) {
        this.codigoMunicipioIbge = codigoMunicipioIbge;
    }

    public String getPopulacao() {
        return populacao;
    }

    public void setPopulacao(String populacao) {
        this.populacao = populacao;
    }

//    public String getLocalidade() {
//        return localidade;
//    }
//
//    public void setLocalidade(String localidade) {
//        this.localidade = localidade;
//    }
}
