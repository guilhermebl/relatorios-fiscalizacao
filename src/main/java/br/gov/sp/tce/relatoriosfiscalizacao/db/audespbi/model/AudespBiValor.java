package br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
public class AudespBiValor {

    @Id
    @Column(name = "ano")
    private Integer exercicio;

    @Column (name = "valor")
    private BigDecimal valor;


    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
