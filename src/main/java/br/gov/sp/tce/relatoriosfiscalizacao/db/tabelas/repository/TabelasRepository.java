package br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasParecer;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TabelasRepository {

    @PersistenceContext(unitName = "tabelas")
    private EntityManager entityManager;

    public TabelasProtocolo getTabelasProtocoloPrefeituraByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        Query query =  entityManager.createNativeQuery("select protocolo.processo , dbo.orgaos.codigoorgaomainframe, " +
                "dbo.orgaos.nomeorgao, protocolo.exercicio, secaoFiscalizadoraContas, descricaoArea, dbo.areas.Area03 as dsf, " +
                "protocolo.objeto, cdRelator, Relator, " +
                "  dbo.orgaos.cnpjorgao, porteMunicipio, codigoIbge " +
                "  from protocolo " +
                "  inner join dbo.orgaos on protocolo.cdparte2 = dbo.orgaos.CodigoOrgaoMainFrame " +
                "  inner join dbo.Municipios on dbo.orgaos.CodigoMunicipio = dbo.Municipios.CodigoMunicipio " +
                "  inner join dbo.areas on dbo.orgaos.secaofiscalizadoracontas = dbo.areas.codigoarea " +
                "  where 1=1 " +
                "  and dbo.orgaos.codigotipoorgao = 1 " +
                "  and protocolo.exercicio = :exercicio " +
                "  and dbo.Municipios.codigoibge = :codigoIBGE " +
                "  and protocolo.tpprocesso = 12", TabelasProtocolo.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<TabelasProtocolo>  lista = query.getResultList();
        if(lista.size() == 0)
            return new TabelasProtocolo();
        else
            return lista.get(0);
    }

    public TabelasProtocolo getTabelasProtocoloCamaraByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        Query query =  entityManager.createNativeQuery("select protocolo.processo , dbo.orgaos.codigoorgaomainframe, " +
                "dbo.orgaos.nomeorgao, protocolo.exercicio, secaoFiscalizadoraContas, descricaoArea, dbo.areas.Area03 as dsf, " +
                "protocolo.objeto, cdRelator, Relator, " +
                "  dbo.orgaos.cnpjorgao, porteMunicipio, codigoIbge " +
                "  from protocolo " +
                "  inner join dbo.orgaos on protocolo.cdparte2 = dbo.orgaos.CodigoOrgaoMainFrame " +
                "  inner join dbo.Municipios on dbo.orgaos.CodigoMunicipio = dbo.Municipios.CodigoMunicipio " +
                "  inner join dbo.areas on dbo.orgaos.secaofiscalizadoracontas = dbo.areas.codigoarea " +
                "  where 1=1 " +
                "  and dbo.orgaos.codigotipoorgao = 13 " +
                "  and protocolo.exercicio = :exercicio " +
                "  and dbo.Municipios.codigoibge = :codigoIBGE " +
                "  and protocolo.tpprocesso = 26", TabelasProtocolo.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<TabelasProtocolo>  lista = query.getResultList();
        if(lista.size() == 0)
            return new TabelasProtocolo();
        else
            return lista.get(0);
    }

    public List<TabelasParecer> getPareceresTransitoEmJulgadoByCodigoIbge(Integer codigoIBGE) {
        Query query =  entityManager.createNativeQuery("select " +
                "            exercicio, " +
                "            processo, " +
                "    SisCep.dbo.TransitoEmJulgado.dtPublicacaoDO " +
                "    from tabelas.protocolo.protocolo " +
                "    inner join tabelas.dbo.orgaos on dbo.orgaos.CodigoorgaoMainframe = tabelas.protocolo.protocolo.cdParte2 " +
                "    inner join tabelas.dbo.municipios on tabelas.dbo.municipios.CodigoMunicipio = dbo.orgaos.CodigoMunicipio " +
                "    left join SisCep.dbo.TransitoEmJulgado on SisCep.dbo.TransitoEmJulgado.NumeroProcesso = tabelas.protocolo.protocolo.processo " +
                "    where 1=1 " +
                "    and tpprocesso in(12)  " +
                "    and tabelas.dbo.municipios.codigoIBGE = :codigoIBGE " +
                "    order by exercicio desc", TabelasParecer.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        List<TabelasParecer>  lista = query.getResultList();
        return lista;
    }




}