package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AudespParametroKey implements Serializable {

    @Column(name = "parametro_analise_id")
    private Integer idParametroAnalise;

    @Column(name = "ano_exercicio")
    private Integer exercicio;

    @Column(name = "mes_referencia")
    private Integer mes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AudespParametroKey)) return false;
        AudespParametroKey that = (AudespParametroKey) o;
        return Objects.equals(idParametroAnalise, that.idParametroAnalise) &&
                Objects.equals(exercicio, that.exercicio) &&
                Objects.equals(mes, that.mes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idParametroAnalise, exercicio, mes);
    }
}

