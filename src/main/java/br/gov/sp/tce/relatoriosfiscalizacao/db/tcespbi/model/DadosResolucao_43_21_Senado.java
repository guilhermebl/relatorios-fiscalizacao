package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class DadosResolucao_43_21_Senado {

    private Integer id;

    @Column(name = "codigo_ibge")
    private Integer codigoMunicipioIbge;

    private Integer exercicio;

    @Column(name = "codigo_tipo_orgao")
    private Integer codigoTipoOrgao;

    @Id
    private String tc;

    @Column(name = "nome_orgao")
    private String nomeOrgao;

    @Column(name = "transitou_efetivamente")
    private String transitouEfetivamente;

    @Column(name = "data_transito_em_julgado")
    private LocalDate dataTransitoEmJulgado ;

    @Column(name = "data_publicacao_do_transito_em_julgado")
    private  LocalDate dataPublicacaoDoTransitoEmJulgado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigoMunicipioIbge() {
        return codigoMunicipioIbge;
    }

    public void setCodigoMunicipioIbge(Integer codigoMunicipioIbge) {
        this.codigoMunicipioIbge = codigoMunicipioIbge;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getCodigoTipoOrgao() {
        return codigoTipoOrgao;
    }

    public void setCodigoTipoOrgao(Integer codigoTipoOrgao) {
        this.codigoTipoOrgao = codigoTipoOrgao;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }

    public String getNomeOrgao() {
        return nomeOrgao;
    }

    public void setNomeOrgao(String nomeOrgao) {
        this.nomeOrgao = nomeOrgao;
    }

    public String getTransitouEfetivamente() {
        return transitouEfetivamente;
    }

    public void setTransitouEfetivamente(String transitouEfetivamente) {
        this.transitouEfetivamente = transitouEfetivamente;
    }

    public LocalDate getDataTransitoEmJulgado() {
        return dataTransitoEmJulgado;
    }

    public void setDataTransitoEmJulgado(LocalDate dataTransitoEmJulgado) {
        this.dataTransitoEmJulgado = dataTransitoEmJulgado;
    }

    public LocalDate getDataPublicacaoDoTransitoEmJulgado() {
        return dataPublicacaoDoTransitoEmJulgado;
    }

    public void setDataPublicacaoDoTransitoEmJulgado(LocalDate dataPublicacaoDoTransitoEmJulgado) {
        this.dataPublicacaoDoTransitoEmJulgado = dataPublicacaoDoTransitoEmJulgado;
    }
}

