package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacao;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespRGFRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<AudespResultado> getRCLDespesaPessoalRGF(Integer codigoIBGE, Integer exercicio, Integer tipoEntidade) {
        Query query = entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id, " +
                " vw_valor_parametro.nm_parametro_analise, " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id, " +
                "ds_parametro_analise from vw_valor_parametro " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = ( " +
                    "select entidade.entidade_id " +
                    "from municipio " +
                    "inner join entidade on entidade.municipio_id = municipio.municipio_id " +
                    "where entidade.tp_entidade_id =  :tipoEntidade " +
                    "and entidade.municipio_id <= 644 " +
                    "and municipio.cd_municipio_ibge =  :codigoIBGE ) " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vRCL', 'vDespPessoalLiq', 'vPercDespPessoal') " +
                "and mes_referencia in ( 4, 8, 12 ) and ano_exercicio =  :exercicio " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultado.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("tipoEntidade", tipoEntidade);
        List<AudespResultado> lista = query.getResultList();
        for (int i = lista.size(); i < 3; i++)
            lista.add(new AudespResultado());
        return lista;
    }


    public List<AudespPublicacao> getSituacaoEntregaDocumentosRGF(Integer codigoIBGE, Integer exercicio, Integer tipoDocumento) {
        Query query = entityManager.createNativeQuery("SELECT ano_exercicio as exercicio, " +
                "ds_municipio, " +
                "municipio.municipio_id, " +
                "nm_tp_documento, " +
                "entidade.entidade_id, " +
                "documento.tp_documento_id as tipoDocumentoId, " +
                "mes_referencia, " +
                "dt_publicacao, " +
                "veiculo_publicacao, " +
                "dt_prest_informacao, " +
                "ds_estado_publicacao " +
                "FROM " +
                "documento " +
                "INNER JOIN documento_publicacao ON documento_publicacao.documento_id = documento.documento_id "  +
                "INNER JOIN publicacao ON publicacao.publicacao_id = documento_publicacao.publicacao_id " +
                "INNER JOIN documento__tipo_estado ON documento.documento_id = documento__tipo_estado.documento_id AND tp_estado_id = 5 " +
                "INNER JOIN tipo_documento ON documento.tp_documento_id = tipo_documento.tp_documento_id " +
                "INNER JOIN entidade ON documento.entidade_id = entidade.entidade_id " +
                "INNER JOIN municipio ON entidade.municipio_id = municipio.municipio_id " +
                "INNER JOIN estado_publicacao ON estado_publicacao.estado_publicacao_id = publicacao.estado_publicacao_id " +
                "WHERE municipio.cd_municipio_ibge = :codigoIBGE  AND ano_exercicio = :exercicio  " +
                "AND documento.tp_documento_id in (:tipoDocumento)   " +
                "AND ds_estado_publicacao != 'Republicação' " +
                "-- RGF  " +
                "ORDER BY nm_tp_documento, mes_referencia, dt_publicacao; ", AudespPublicacao.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("tipoDocumento", tipoDocumento);
        List<AudespPublicacao> lista = query.getResultList();
//        for(int i = lista.size(); i < 1; i++)
//            lista.add(new AudespPublicacao());
        return lista;
    }

    public List<AudespPublicacao> getSituacaoEntregaDocumentosRREO(Integer codigoIBGE, Integer exercicio, Integer tipoEntidade) {
        Query query = entityManager.createNativeQuery("SELECT ano_exercicio as exercicio, " +
                "ds_municipio, " +
                "municipio.municipio_id, " +
                "coalesce(nm_tp_documento, '') as nm_tp_documento, " +
                "entidade.entidade_id, " +
                "documento.tp_documento_id as tipoDocumentoId, " +
                "mes_referencia, " +
                "dt_publicacao, " +
                "coalesce(veiculo_publicacao, '') as veiculo_publicacao, " +
                "dt_prest_informacao, " +
                "ds_estado_publicacao " +
                "FROM " +
                "documento " +
                "INNER JOIN documento_publicacao ON documento_publicacao.documento_id = documento.documento_id "  +
                "INNER JOIN publicacao ON publicacao.publicacao_id = documento_publicacao.publicacao_id " +
                "INNER JOIN documento__tipo_estado ON documento.documento_id = documento__tipo_estado.documento_id AND tp_estado_id = 5 " +
                "INNER JOIN tipo_documento ON documento.tp_documento_id = tipo_documento.tp_documento_id " +
                "INNER JOIN entidade ON documento.entidade_id = entidade.entidade_id " +
                "INNER JOIN municipio ON entidade.municipio_id = municipio.municipio_id " +
                "INNER JOIN estado_publicacao ON estado_publicacao.estado_publicacao_id = publicacao.estado_publicacao_id " +
                "WHERE municipio.cd_municipio_ibge = :codigoIBGE  AND ano_exercicio = :exercicio  " +
                "AND documento.tp_documento_id in (70,71,72,73,74,75,76,77,78,79,86,87) " +
                "AND ds_estado_publicacao != 'Republicação' " +
                "ORDER BY nm_tp_documento, mes_referencia, dt_publicacao; ", AudespPublicacao.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        //query.setParameter("tipoEntidade", tipoEntidade);
        List<AudespPublicacao> lista = query.getResultList();
//        for(int i = lista.size(); i < 3; i++)
//            lista.add(new AudespPublicacao());
        return lista;
    }

}