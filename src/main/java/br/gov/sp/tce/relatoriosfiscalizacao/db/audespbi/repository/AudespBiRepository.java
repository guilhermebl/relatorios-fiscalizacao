package br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model.AudespBiValor;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model.TcespBiInvestimentoMunicipio;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AudespBiRepository {

    @PersistenceContext(unitName = "audespbi")
    private EntityManager entityManager;

    public List<TcespBiInvestimentoMunicipio> getValorInvestimentoMunicipio(Integer codigoIBGE, Integer exercicio) {
        Query query =  entityManager.createNativeQuery(
                "select dim_periodicidade.ano, coalesce(sum(fato_empenho.vl_liquidacao), 0) as investimento_municipio " +
                        "from fato_empenho  " +
                        "inner join dim_classificacao_despesa on fato_empenho.classificacao_despesa_sk = dim_classificacao_despesa.classificacao_despesa_sk  " +
                        "inner join dim_periodicidade on fato_empenho.periodo_empenho_sk = dim_periodicidade.periodo_sk  " +
                        "inner join dim_entidade on fato_empenho.entidade_sk = dim_entidade.entidade_sk  " +
                        "where 1 = 1  " +
                        "and dim_classificacao_despesa.cd_grupo = 44000000  " +
                        "and dim_periodicidade.ano in ( :exercicio -3, :exercicio -2, :exercicio -1, :exercicio ) " +
                        "and dim_entidade.municipio_ibge = :codigoIBGE " +
                        "group by dim_periodicidade.ano ", TcespBiInvestimentoMunicipio.class );
        query.setParameter("exercicio", exercicio);
        query.setParameter("codigoIBGE", codigoIBGE);
        List<TcespBiInvestimentoMunicipio> lista = query.getResultList();
        if(lista.size() == 0)
            lista = new ArrayList<>();
        return lista;
    }

    public List<AudespBiValor> getValorDespesaEmpenhada(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        String meses = "";
        if(mesReferencia == 4)
            meses = " and dim_periodicidade.mes in (1,2,3,4) ";
        else if(mesReferencia == 8)
            meses = " and dim_periodicidade.mes in (5,6,7,8) ";


        Query query =  entityManager.createNativeQuery (
                "select " +
                        "dim_entidade.municipio_ibge, " +
                        "dim_entidade.ds_municipio, " +
                        "dim_periodicidade.ano, " +
                        "coalesce(sum(fato_despesa_orcamentaria.empenhamento), 0) as valor " +
                        "from fato_despesa_orcamentaria " +
                        "inner join dim_classificacao_despesa " +
                        "on fato_despesa_orcamentaria.classificacao_despesa_sk = dim_classificacao_despesa.classificacao_despesa_sk  " +
                        "inner join dim_periodicidade on fato_despesa_orcamentaria.periodo_sk = dim_periodicidade.periodo_sk  " +
                        "inner join dim_entidade on fato_despesa_orcamentaria.entidade_sk = dim_entidade.entidade_sk " +
                        "where 1 = 1 " +
                        "and dim_periodicidade.ano in ( :exercicio ) " +
                         meses +
                        "and dim_entidade.municipio_ibge = :codigoIBGE " +
                        "and dim_entidade.cd_orgao = 1 " +
                        "group by dim_periodicidade.ano, " +
                        "dim_entidade.municipio_ibge, " +
                        "dim_entidade.ds_municipio  ", AudespBiValor.class );
        query.setParameter("exercicio", exercicio);
        query.setParameter("codigoIBGE", codigoIBGE);
        List<AudespBiValor> lista = query.getResultList();
        if(lista.size() == 0)
            lista = new ArrayList<>();
        return lista;
    }



    public Integer getSkMunicipio(Integer codigoIBGE) {
        Query query = entityManager.createNativeQuery("select municipio_sk from dim_entidade where " +
                " municipio_ibge = :codigoIBGE " +
                " group by municipio_sk limit 1");
        query.setParameter("codigoIBGE", codigoIBGE);
        return (Integer)(query.getSingleResult());
    }


}
