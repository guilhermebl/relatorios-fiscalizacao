package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.Anexo14A;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespSaude;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class DemonstrativosRaeeRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<Anexo14A> getAnexo14A(AudespEntidade entidade, Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select * from ( " +
                "select 11 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, null as grupo2, 'Pessoal a Pagar' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2111101%', '211111600', '221111500']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2111101%', '211111600', '221111500']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 12 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, null as grupo2, 'Benefícios Previdenciários' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao in ('211210100', '211211600', '221211400') and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao in ('211210100', '211211600', '221211400') and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 13 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, null as grupo2, 'Benefícios Assistenciais' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao in ('211310100', '211311500', '221311400') and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao in ('211310100', '211311500', '221311400') and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 14 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, null as grupo2, 'Encargos Sociais' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like '2114%' and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like '2114%' and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 15 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, null as grupo2, 'Empréstimos e Financiamentos' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like '212%' and cod.codificacao not ilike all (array ['2125%', '2126%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like '212%' and cod.codificacao not ilike all (array ['2125%', '2126%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 16 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, null as grupo2, 'Fornecedores/Contas a Pagar' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and ( " +
                "  (cod.codificacao ilike any (array ['2131101%', '2131103%', '2132101%', '2132102%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') or " +
                "  cod.codificacao in ('213111900', '213112000') " +
                "  ) " +
                "  ) as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and ( " +
                "  (cod.codificacao ilike any (array ['2131101%', '2131103%', '2132101%', '2132102%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') or " +
                "  cod.codificacao in ('213111900', '213112000') " +
                "  ) " +
                "  ) as valor2 " +
                "union " +
                "select 17 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, null as grupo2, 'Demais Obrigações' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['218%', '228%']) and cod.codificacao not ilike all (array ['21881%', '22881%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['218%', '228%']) and cod.codificacao not ilike all (array ['21881%', '22881%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 19 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, 'Precatórios' as grupo2, 'Pessoal' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2111104%', '2111105%', '2211103%', '2211104%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2111104%', '2111105%', '2211103%', '2211104%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 20 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, 'Precatórios' as grupo2, 'Benefícios Previdênciários' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end)and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2112104%', '2112105%', '2212102%', '2212103%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2112104%', '2112105%', '2212102%', '2212103%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 20 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, 'Precatórios' as grupo2, 'Benefícios Assistenciais' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end)and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2113103%', '2113104%', '2213102%', '2213103%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2113103%', '2113104%', '2213102%', '2213103%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 21 as sort_field, 'Restos a Pagar Processados/Não Processados em Liquidação e Não Processados a Pagar' as grupo1, 'Precatórios' as grupo2, 'Fornecedores/Contas a Pagar' as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2131105%', '2131106%', '2131107%', '2131108%', '2231104%', '2231105%', '2231106%', '2231107%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao ilike any (array ['2131105%', '2131106%', '2131107%', '2131108%', '2231104%', '2231105%', '2231106%', '2231107%'])and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 22 as sort_field, 'Restos a Pagar não Processados' as grupo1, null as grupo2, null as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao = '631100000') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao = '631100000') as valor2 " +
                "union " +
                "select 23 as sort_field, 'Obrigações Fiscais' as grupo1, null as grupo2, null as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like '214%' and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like '214%' and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2  " +
                "union " +
                "select 24 as sort_field, 'Valores Restituíveis' as grupo1, null as grupo2, null as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like any(array['21881%', '22881%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1,  " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like any(array['21881%', '22881%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "union " +
                "select 26 as sort_field, 'Juros e Encargos a Pagar de Empréstimos e Financiamentos' as grupo1, null as grupo2, null as detalhe, " +
                " (select (-sum(cta.saldo_final)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia = (case when  :mes_referencia  = 12 then 14 else  :mes_referencia  end) and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like any(array['2125%', '2126%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor1, " +
                " (select (-sum(cta.saldo_inicial)) " +
                "  from conta_contabil cta inner join codigo_contabil cod on cta.cd_contabil_id=cod.cd_contabil_id " +
                "  where cta.municipio_id = :cod_municipio and cta.entidade_id= :cod_entidade  and cta.ano_exercicio= :exercicio  and mes_referencia= 1 and tp_balancete_id =  :tp_balancete  " +
                "  and cod.codificacao like any(array['2125%', '2126%']) and cod.tipo = 'A' and cod.indicador_superavit = 'F') as valor2 " +
                "order by sort_field " +
                ") as sql " +
                "where (valor1 != 0 OR valor2 != 0) ", Anexo14A.class);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mes_referencia", mesReferencia);
        query.setParameter("cod_entidade", entidade.getEntidadeId());
        query.setParameter("cod_municipio", entidade.getMunicipioId());
        query.setParameter("tp_balancete", entidade.getTpBalancete());

        List<Anexo14A> lista = query.getResultList();
        for(int i = lista.size(); i < 3 ; i++)
            lista.add(new Anexo14A());
        return lista;
    }

}