package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespDespesaPessoal;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespDespesaPessoalRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<AudespDespesaPessoal> getAudespDespesaPessoalByCodigoIbge(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        String whereQuadrimestre = "";
        if(mesReferencia == 4) {
            whereQuadrimestre = "and (mes_referencia in (4, 8, 12) and ano_exercicio = :exercicio - 1 or mes_referencia in (4) and ano_exercicio = :exercicio) ";
        } else {
            whereQuadrimestre = "and (mes_referencia in (8, 12) and ano_exercicio = :exercicio -1 or mes_referencia in (4, 8) and ano_exercicio = :exercicio) ";
        }

        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id, " +
                " vw_valor_parametro.nm_parametro_analise,  " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,  " +
                "ds_parametro_analise from vw_valor_parametro  " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise  " +
                "where  1 = 1  " +
                "and parametro_analise_id in (103, 49, 107)  " +
                "and vw_valor_parametro.entidade_id = (  " +
                "select entidade.entidade_id  " +
                "from municipio  " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50  " +
                "and entidade.municipio_id <= 644  " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                whereQuadrimestre +
                "order by parametro_analise_id, ano_exercicio, mes_referencia", AudespDespesaPessoal.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<AudespDespesaPessoal> lista = query.getResultList();
        for(int i = lista.size(); i < 12 ; i++)
            lista.add (new AudespDespesaPessoal());
        return lista;
    }

    public List<AudespDespesaPessoal> getAudespDespesaPessoalByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        String whereQuadrimestre = "and mes_referencia in (4, 8, 12) and ano_exercicio in ( :exercicio , :exercicio-1 )";

        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE )  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vDespPessoalLiq', 'vLimPermitido', 'vRCL', 'vPercDespPessoal' ) " +
                whereQuadrimestre +
                "order by parametro_analise_id, ano_exercicio, mes_referencia", AudespDespesaPessoal.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<AudespDespesaPessoal> lista = query.getResultList();
        for(int i = lista.size(); i < 12 ; i++)
            lista.add (new AudespDespesaPessoal());
        return lista;
    }

    public List<AudespDespesaPessoal> getAudespDespesaPessoalLegislativoByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        String whereQuadrimestre = "and mes_referencia in (4, 8, 12) and ano_exercicio in ( :exercicio , :exercicio-1 )";

        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 51   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE )  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vDespPessoalLiq', 'vLimPermitido', 'vRCL', 'vPercDespPessoal' ) " +
                whereQuadrimestre +
                "order by parametro_analise_id, ano_exercicio, mes_referencia", AudespDespesaPessoal.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<AudespDespesaPessoal> lista = query.getResultList();
        for(int i = lista.size(); i < 12 ; i++)
            lista.add (new AudespDespesaPessoal());
        return lista;
    }


//    public List<AudespDespesaPessoal> getAudespDespesaPessoalByCodigoIbgeQuadrimestral(Integer codigoIBGE, Integer exercicio) {
//        String whereQuadrimestre = "and (mes_referencia in (4, 8, 12) and ano_exercicio in ( :exercicio , :exercicio-1 )";
//
//        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
//                " vw_valor_parametro.nm_parametro_analise,   " +
//                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
//                "ds_parametro_analise from vw_valor_parametro   " +
//                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
//                "where 1 = 1 " +
//                "and vw_valor_parametro.entidade_id = (   " +
//                "select entidade.entidade_id   " +
//                "from municipio   " +
//                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
//                "where entidade.tp_entidade_id = 50   " +
//                "and entidade.municipio_id <= 644   " +
//                "and municipio.cd_municipio_ibge = :codigoIBGE )  " +
//                "and parametro_analise.nm_parametro_analise " +
//                "in ('vDespPessoalLiq', 'vLimPermitido', 'vRCL', 'vPercDespPessoal' ) " +
//                whereQuadrimestre +
//                "order by parametro_analise_id, ano_exercicio, mes_referencia", AudespDespesaPessoal.class);
//        query.setParameter("codigoIBGE", codigoIBGE);
//        query.setParameter("exercicio", exercicio);
//        List<AudespDespesaPessoal> lista = query.getResultList();
//        for(int i = lista.size(); i < 12 ; i++)
//            lista.add (new AudespDespesaPessoal());
//        return lista;
//    }

}