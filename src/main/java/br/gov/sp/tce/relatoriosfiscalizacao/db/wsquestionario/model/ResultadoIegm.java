package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(ResultadoIegmKey.class)
public class ResultadoIegm {

    @Id
    private Integer exercicio;

    @Id
    private Integer municipioIbge;

    private String cnpj;

    @Column(name = "ds_municipio")
    private String municipio;

    private String porte;

    private String prefeitura;

    @Column(name = "ur_df")
    private String urDf;

    @Column(name = "cd_relator")
    private String codigoRelator;

    @Column(name = "nm_conselheiro")
    private String nomeConselheiro;

    @Column(name = "iegm_final")
    private String notaIegm;

    @Column(name = "faixa_iegm_final")
    private String faixaIegm;

    @Column(name = "ieduc_final")
    private String notaIEduc;

    @Column(name = "faixa_ieduc_final")
    private String faixaIEduc;

    @Column(name = "isaude_final")
    private String notaISaude;

    @Column(name = "faixa_isaude_final")
    private String faixaISaude;

    @Column(name = "iplanejamento_final")
    private String notaIPlanejamento;

    @Column(name = "faixa_iplanejamento_final")
    private String faixaIPlanejamento;

    @Column(name = "ifiscal_final")
    private String notaIFiscal;

    @Column(name = "faixa_ifiscal_final")
    private String faixaIFiscal;

    @Column(name = "iamb_final")
    private String notaIAmb;

    @Column(name = "faixa_iamb_final")
    private String faixaIAmb;

    @Column(name = "icidade_final")
    private String notaICidade;

    @Column(name = "faixa_icidade_final")
    private String faixaICidade;

    @Column(name = "igov_final")
    private String notaIGov;

    @Column(name = "faixa_igov_final")
    private String faixaIGov;

    @Column(name = "cumpriuartigo29a")
    private String cumpriuArtigo29a;

    @Column(name = "cumpriupercentualeducacao")
    private String cumpriuPencentualEducacao;

    @Column(name = "cumpriupercentualsaude")
    private String cumpriuPercentualSaude;

    @Column(name = "flag_rebaixo")
    private Integer flagRebaixo;

    @Column(name = "status_fase")
    private Integer flagStatusFase;

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getMunicipioIbge() {
        return municipioIbge;
    }

    public void setMunicipioIbge(Integer municipioIbge) {
        this.municipioIbge = municipioIbge;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getPorte() {
        return porte;
    }

    public void setPorte(String porte) {
        this.porte = porte;
    }

    public String getPrefeitura() {
        return prefeitura;
    }

    public void setPrefeitura(String prefeitura) {
        this.prefeitura = prefeitura;
    }

    public String getUrDf() {
        return urDf;
    }

    public void setUrDf(String urDf) {
        this.urDf = urDf;
    }

    public String getCodigoRelator() {
        return codigoRelator;
    }

    public void setCodigoRelator(String codigoRelator) {
        this.codigoRelator = codigoRelator;
    }

    public String getNomeConselheiro() {
        return nomeConselheiro;
    }

    public void setNomeConselheiro(String nomeConselheiro) {
        this.nomeConselheiro = nomeConselheiro;
    }

    public String getNotaIegm() {
        return notaIegm;
    }

    public void setNotaIegm(String notaIegm) {
        this.notaIegm = notaIegm;
    }

    public String getFaixaIegm() {
        if(flagStatusFase == null)
            return "";
        return faixaIegm;
    }

    public void setFaixaIegm(String faixaIegm) {
        this.faixaIegm = faixaIegm;
    }

    public String getNotaIEduc() {
        return notaIEduc;
    }

    public void setNotaIEduc(String notaIEduc) {
        this.notaIEduc = notaIEduc;
    }

    public String getFaixaIEduc() {
        if (faixaIEduc == null) { return ""; }
        return faixaIEduc;
    }

    public void setFaixaIEduc(String faixaIEduc) {
        this.faixaIEduc = faixaIEduc;
    }

    public String getNotaISaude() {
        return notaISaude;
    }

    public void setNotaISaude(String notaISaude) {
        this.notaISaude = notaISaude;
    }

    public String getFaixaISaude() {
        if (faixaISaude == null) { return ""; }
        return faixaISaude;
    }

    public void setFaixaISaude(String faixaISaude) {
        this.faixaISaude = faixaISaude;
    }

    public String getNotaIPlanejamento() {
        return notaIPlanejamento;
    }

    public void setNotaIPlanejamento(String notaIPlanejamento) {
        this.notaIPlanejamento = notaIPlanejamento;
    }

    public String getFaixaIPlanejamento() {
        if (faixaIPlanejamento == null) { return ""; }
        return faixaIPlanejamento;
    }

    public void setFaixaIPlanejamento(String faixaIPlanejamento) {
        this.faixaIPlanejamento = faixaIPlanejamento;
    }

    public String getNotaIFiscal() {
        return notaIFiscal;
    }

    public void setNotaIFiscal(String notaIFiscal) {
        this.notaIFiscal = notaIFiscal;
    }

    public String getFaixaIFiscal() {
        if (faixaIFiscal == null) { return ""; }
        return faixaIFiscal;
    }

    public void setFaixaIFiscal(String faixaIFiscal) {
        this.faixaIFiscal = faixaIFiscal;
    }

    public String getNotaIAmb() {
        return notaIAmb;
    }

    public void setNotaIAmb(String notaIAmb) {
        this.notaIAmb = notaIAmb;
    }

    public String getFaixaIAmb() {
        if (faixaIAmb == null) { return ""; }
        return faixaIAmb;

    }

    public void setFaixaIAmb(String faixaIAmb) {
        this.faixaIAmb = faixaIAmb;
    }

    public String getNotaICidade() {
        return notaICidade;
    }

    public void setNotaICidade(String notaICidade) {
        this.notaICidade = notaICidade;
    }

    public String getFaixaICidade() {
        if (faixaICidade == null) { return ""; }
        return faixaICidade;
    }

    public void setFaixaICidade(String faixaICidade) {
        this.faixaICidade = faixaICidade;
    }

    public String getNotaIGov() {
        return notaIGov;
    }

    public void setNotaIGov(String notaIGov) {
        this.notaIGov = notaIGov;
    }

    public String getFaixaIGov() {
        if (faixaIGov == null) { return ""; }
        return faixaIGov;
    }

    public void setFaixaIGov(String faixaIGov) {
        this.faixaIGov = faixaIGov;
    }

    public String getCumpriuArtigo29a() {
        return cumpriuArtigo29a;
    }

    public void setCumpriuArtigo29a(String cumpriuArtigo29a) {
        this.cumpriuArtigo29a = cumpriuArtigo29a;
    }

    public String getCumpriuPencentualEducacao() {
        return cumpriuPencentualEducacao;
    }

    public void setCumpriuPencentualEducacao(String cumpriuPencentualEducacao) {
        this.cumpriuPencentualEducacao = cumpriuPencentualEducacao;
    }

    public String getCumpriuPercentualSaude() {
        return cumpriuPercentualSaude;
    }

    public void setCumpriuPercentualSaude(String cumpriuPercentualSaude) {
        this.cumpriuPercentualSaude = cumpriuPercentualSaude;
    }

    public Integer getFlagRebaixo() {
        return flagRebaixo;
    }

    public void setFlagRebaixo(Integer flagRebaixo) {
        this.flagRebaixo = flagRebaixo;
    }

    public Integer getFlagStatusFase() {
        if(flagStatusFase==null)
            return 0;
        else
            return flagStatusFase;
    }

    public void setFlagStatusFase(Integer flagStatusFase) {
        this.flagStatusFase = flagStatusFase;
    }
}
