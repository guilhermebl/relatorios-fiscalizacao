package br.gov.sp.tce.relatoriosfiscalizacao.controller._2021;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.*;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class RelatorioPrefeituraQuadrimestral2021Controller {

    private XWPFDocument document;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Integer exercicio;

    private Integer codigoIBGE;

    FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");

    @Autowired
    private IegmService iegmService;

    private ResultadoIegm resultadoIegmAnoBaseAnterior;
    private ResultadoIegm resultadoIegmAnoBaseRetrasado;
    private ResultadoIegm resultadoIegmAnoBaseReRetrasado;

    @Autowired
    private ParecerPrefeituraService parecerPrefeituraService;

    private List<ParecerPrefeitura> pareceresPrefeiturasList;

    @Autowired
    private TabelasService tabelasService;

    private TabelasProtocolo tabelasProtocolo;

    @Autowired
    private AudespEnsinoService audespEnsinoService;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    @Autowired
    private AudespSaudeService audespSaudeService;

    @Autowired
    private AudespLimiteLrfService audespLimiteLrfService;

    @Autowired
    private AudespService audespService;

    @Autowired
    private AudespAlertasService audespAlertasService;

    @Autowired
    private ApontamentosODSService apontamentosODSService;

    private Map<String, List<ApontamentoODS>> apontamentosODS;

    @Autowired
    private ResourceLoader resourceLoader;

    private Map<Integer, NotaIegm> notasIegm;

    private List<AudespResponsavel> responsavelPrefeitura;

    private Map<String,List<AudespResponsavel>> responsavelSubstitutoPrefeitura;

    private MunicipioIbge municipioIegmCodigoIbge;

    private Map<String, String> audespResultadoExecucaoOrcamentaria;

    private Map<String, String> audespResultadoExecucaoOrcamentariaExcercicioAnt;

    @Autowired
    private AudespEntidadeService audespEntidadeService;

    private AudespEntidade audespEntidade;

    @Autowired
    private DemonstrativosRaeeService demonstrativosRaeeService;

    private  Map<String,String> anexo14AMap;

    private Map<String, String> audespEnsinoFundeb;

    private Map<String, String> audespSaude;

    private Map<String, String> audespDespesaPessoalMap;

    private Map<String, String> quadroGeralEnsinoMap;

    private Map<String, String> aplicacoesEmSaude;

    @Autowired
    private ApontamentosFOService apontamentosFOService;

    private Map<Integer, List<ApontamentoFO>> apontamentoFOMap;

    @Autowired
    private AudespDividaAtivaService audespDividaAtivaService;

    private Map<String, String> audespDividaAtivaMap;

    private Map<String,String> audespResultadoExecucaoOrcamentariaMap;

    @Autowired
    private AudespFase3Service audespFase3Service;

    private Map<String, AudespFase3QuadroPessoal> audespFase3QuadroDePessoalMap;

    @Autowired
    private AudespBiService audespBiService;

    @Autowired
    private TcespBiService tcespBiService;

    private Map<Integer, String> valorInvestimentoMunicipioMap;

    private Map<String, String> rclMunicipioDevedoresMap;

    private Map<String, String> limiteLRFMap;

    private Integer quadrimestre;

    private String heading1 = "Seção 1";
    private String heading2 = "Seção 2";
    private String heading3 = "Seção 3";
    private String heading4 = "Seção 4";
    private String heading5 = "Seção 5";


    private ResponseEntity<Resource> sendFile() throws IOException {
        // fim --------------------------------------
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-relatorio-pm-quadr-2020-v1.0.0.docx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }





    private void addSecao(TextoFormatado textoFormatado, String heading) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle(heading);
        paragraph.setSpacingAfter(6 * 20);
        if (heading.equals(this.heading1)) {
            CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill("d9d9d9");
        }

        textoFormatado.setParagraphText(paragraph);
    }

    private void createDocumentStyles() {
        XWPFStyles styles = document.createStyles();

        addCustomHeadingStyle(styles, heading1, 1);
        addCustomHeadingStyle(styles, heading2, 2);
        addCustomHeadingStyle(styles, heading3, 3);
        addCustomHeadingStyle(styles, heading4, 4);
        addCustomHeadingStyle(styles, heading5, 5);
    }


    private void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("Arial");

        CTRPr rpr = CTRPr.Factory.newInstance();
//        rpr.setRFonts(fonts);

        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }

    public byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }

    private TextoFormatado addTab() {
        Formatacao formatacaoTab = formatacaoFactory.getTab();
        return new TextoFormatado("", formatacaoTab).concat("", formatacaoTab);
    }

    private String getQuadrimestreTituloComAno(Integer quadrimestre) {
        String quadrimestreTitulo = "1º/2º Quadrimestre de 2018";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre de 2018";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre de 2018";
        return quadrimestreTitulo;
    }

    private String getQuadrimestreTitulo(Integer quadrimestre) {
        String quadrimestreTitulo = "3º Quadrimestre";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre";
        return quadrimestreTitulo;
    }

    private Integer getMesReferencia(Integer quadrimestre) {
        Integer mesReferencia = 12;
        if (quadrimestre == 1)
            mesReferencia = 4;
        if (quadrimestre == 2)
            mesReferencia = 8;
        return mesReferencia;
    }

    private void getTabela(XWPFDocument document, String[][] itensTabela, boolean isBold, String fontFamily, int fontSize) {
        XWPFTable table = document.createTable(0, 0);
        table.setWidthType(TableWidthType.PCT);
        table.setWidth("98%");
        table.removeBorders();

        for (int i = 0; i < itensTabela.length; i++) {
            if (table.getRow(i) == null)
                table.createRow();


            for (int x = 0; x < itensTabela[i].length; x++) {
                XWPFTableCell cell;
                // verifica se já existe a cell 0
                if (table.getRow(i).getCell(x) != null)
                    cell = table.getRow(i).getCell(x);
                else
                    cell = table.getRow(i).createCell();

                cell.removeParagraph(0);
                XWPFParagraph cellPar = cell.addParagraph();
                XWPFRun cellParRun = cellPar.createRun();
                cellParRun.setFontFamily(fontFamily);
                cellParRun.setText(itensTabela[i][x]);
                cellParRun.setBold(isBold);
                cellParRun.setFontSize(fontSize);
            }
        }
    }


    private String formataTcManualRedacao(String tc) {
        while(tc.length() < 13) {
            tc = "0" + tc;
        }
        tc = tc.replaceAll("/",".");
        return tc;
    }

    private void addTabelaDadosIniciais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "5%", "75%"}, false);
        formatacaoTabela.setBorder(false);


        String vigenciasPrefeito = "";

        for(int i = 0; i < this.responsavelPrefeitura.size(); i++) {
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataInicioVigenciaFormatado(): "dado não informado";
            vigenciasPrefeito += " a ";
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataFimVigenciaFormatado() :  "dado não informado";
            if(i != this.responsavelPrefeitura.size() - 1){
                vigenciasPrefeito += "; ";
            }
        }

        String vigenciasSubstituto = "";

        AudespResponsavel prefeito = (this.responsavelPrefeitura != null
                && this.responsavelPrefeitura.size() > 0) ? this.responsavelPrefeitura.get(0) : new AudespResponsavel();

        if(prefeito == null)
            prefeito = new AudespResponsavel();

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Processo", formatacaoFactory.getBold(12)));
        linha1.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha1.add(new TextoFormatado("TC-" +  formataTcManualRedacao(tabelasProtocolo.getProcesso()),
                formatacaoFactory.getFormatacao(12)));
        dados.add(linha1);

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Entidade", formatacaoFactory.getBold(12)));
        linha2.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha2.add(new TextoFormatado(tabelasProtocolo.getNomeOrgao(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha2);

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Assunto", formatacaoFactory.getBold(12)));
        linha3.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha3.add(new TextoFormatado("Acompanhamento das Contas Anuais", formatacaoFactory.getFormatacao(12)));
        dados.add(linha3);

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Período examinado", formatacaoFactory.getBold(12)));
        linha4.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha4.add(new TextoFormatado(this.quadrimestre+"º quadrimestre de " + exercicio, formatacaoFactory.getFormatacao(12)));
        dados.add(linha4);

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Prefeito", formatacaoFactory.getBold(12)));
        linha5.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha5.add(new TextoFormatado(prefeito.getNome(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha5);

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
        linha6.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha6.add(new TextoFormatado(prefeito.getCpf(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha6);

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
        linha7.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha7.add(new TextoFormatado(vigenciasPrefeito, formatacaoFactory.getFormatacao(12)));
        dados.add(linha7);

        for (Map.Entry<String, List<AudespResponsavel>> item : this.responsavelSubstitutoPrefeitura.entrySet()) {
            String nome = item.getKey();
            List<AudespResponsavel> listaSubstitutos = item.getValue();
            String vigencias = "";
            for (int i = 0; i < listaSubstitutos.size(); i++) {
                vigencias += listaSubstitutos.get(i).getDataInicioVigenciaFormatado();
                vigencias += " a ";
                vigencias += listaSubstitutos.get(i).getDataFimVigenciaFormatado();
                if (i != listaSubstitutos.size() - 1) {
                    vigencias += "; ";
                }
            }

            AudespResponsavel subs = listaSubstitutos != null && listaSubstitutos.size() > 0 ?
                    listaSubstitutos.get(0) : new AudespResponsavel();


            List<TextoFormatado> linhaSub = new ArrayList<>();
            linhaSub.add(new TextoFormatado("Substituto", formatacaoFactory.getBold(12)));
            linhaSub.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaSub.add(new TextoFormatado(nome, formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaCpf = new ArrayList<>();
            linhaCpf.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
            linhaCpf.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaCpf.add(new TextoFormatado(subs.getCpf(), formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaPeriodo = new ArrayList<>();
            linhaPeriodo.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
            linhaPeriodo.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaPeriodo.add(new TextoFormatado(vigencias, formatacaoFactory.getFormatacao(12)));

            dados.add(linhaSub);
            dados.add(linhaCpf);
            dados.add(linhaPeriodo);
        }


        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Relatoria", formatacaoFactory.getBold(12)));
        linha11.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha11.add(new TextoFormatado(tabelasProtocolo.getRelator(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha11);

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Instrução", formatacaoFactory.getBold(12)));
        linha12.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha12.add(new TextoFormatado(tabelasProtocolo.getSecaoFiscalizadoraContas().trim() + " / " +
                tabelasProtocolo.getDsf(), formatacaoFactory.getFormatacao(12)));

        dados.add(linha12);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void getHeader() throws InvalidFormatException, IOException {
        // Header
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

        CTP ctpFooter = CTP.Factory.newInstance();
        CTR ctrFooter = ctpFooter.addNewR();
        CTText ctFooter = ctrFooter.addNewT();

        XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
        footerParagraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun footerRun = footerParagraph.createRun();
        footerRun.setFontFamily("Arial");
        footerRun.setFontSize(8);
        footerRun.getCTR().addNewPgNum();

        XWPFParagraph[] parsFooter = new XWPFParagraph[1];
        parsFooter[0] = footerParagraph;
        headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

        XWPFParagraph headerPar = header.createParagraph();
        XWPFRun headerParRun = headerPar.createRun();

        headerParRun.setFontFamily("Arial");
        headerParRun.setFontSize(12);
        headerPar.setAlignment(ParagraphAlignment.RIGHT);

        XWPFTable headerTable = header.createTable(1, 3);
        headerTable.removeBorders();
        headerTable.setWidthType(TableWidthType.PCT);
        headerTable.setWidth("98%");

        XWPFTableRow headerTableRow = headerTable.getRow(0);

        //coluna 1
        headerTableRow.getCell(0).removeParagraph(0);
        XWPFParagraph pImgEsquerda = headerTableRow.getCell(0).addParagraph();
        pImgEsquerda.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgEsquerda = pImgEsquerda.createRun();

        Resource resourceBrasaoSP = resourceLoader.getResource("classpath:static/imagens/brasao_sp.png");

        InputStream brasao_sp = resourceBrasaoSP.getInputStream();
        runPImgEsquerda.addPicture(brasao_sp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_sp.png",
                Units.toEMU(50), Units.toEMU(55));

        //coluna 2
        headerTableRow.getCell(1).removeParagraph(0);
        XWPFParagraph p1 = headerTableRow.getCell(1).addParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p1Run = p1.createRun();

        p1Run.setFontFamily("Arial");
        p1Run.setFontSize(12);
        p1Run.setBold(true);
        p1Run.setText("TRIBUNAL DE CONTAS DO ESTADO DE SÃO PAULO");

        XWPFParagraph p2 = headerTableRow.getCell(1).addParagraph();
        p2.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p2Run = p2.createRun();
        p2Run.setFontSize(12);
        p2Run.setFontFamily("Arial");
        p2Run.setText(tabelasProtocolo.getDescricaoArea());

        //coluna 3
        headerTableRow.getCell(2).removeParagraph(0);
        XWPFParagraph pImgDireita = headerTableRow.getCell(2).addParagraph();
        pImgDireita.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgDireita = pImgDireita.createRun();
        Resource resourceBrasaoTcesp = resourceLoader.getResource("classpath:static/imagens/brasao_tcesp.png");

        InputStream brasao_tcesp = resourceBrasaoTcesp.getInputStream();
        runPImgDireita.addPicture(brasao_tcesp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_tcesp.png", Units.toEMU(50), Units.toEMU(55));
    }


    private XWPFParagraph getTitulo(XWPFDocument document, String quadrimestre) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();
        run.setBold(true);
        run.setFontFamily("Arial");
        run.setFontSize(12);
        run.setText("RELATÓRIO DE FISCALIZAÇÃO 1º / 2º QUADRIMESTRE\nPREFEITURA MUNICIPAL");
        return paragraph;
    }

    private void underLine(XWPFRun run) {
        run.setUnderline(UnderlinePatterns.SINGLE);
    }

    private XWPFRun addToParagrafoBreak(XWPFParagraph paragrafo, String texto) {
        XWPFRun run = paragrafo.getRuns().get(0);
        run.addBreak();
        run.setText(texto);
        return run;
    }

    private void addToParagrafoRed(XWPFParagraph paragrafo, String texto, String fontStyle, int fontSize) {
        XWPFRun run = paragrafo.createRun();
        run.setText(texto);
        run.setColor("ff0000");
        run.setFontFamily(fontStyle);
        run.setFontSize(fontSize);
        run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
    }

    private XWPFParagraph getParagrafoGrayHeader(XWPFDocument document, String texto, boolean isBold,
                                                 String fontFamily, int fontSize, ParagraphAlignment paragraphAlignment, boolean haveTab) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(paragraphAlignment);
        XWPFRun run = paragraph.createRun();
        if (haveTab)
            run.addTab();

        run.setBold(isBold);
        run.setFontFamily(fontFamily);
        run.setFontSize(fontSize);
        run.setText(texto);

        // shading
        //run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
        //run.getCTR().addNewRPr().addNewShd();
        CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
        cTShd.setVal(STShd.CLEAR);
        cTShd.setFill("d9d9d9");

        return paragraph;
    }


    //////////////////////
    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();

//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());

        paragraph.setSpacingAfter(6 * 20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafoSpacingAfter(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();
//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());
//        paragraph.setSpacingAfter(6*20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado, boolean breakPage) {
        XWPFParagraph paragraph = addParagrafo(textoFormatado);
        paragraph.setPageBreak(breakPage);
        return paragraph;
    }

    private XWPFTable addTabela(List<List<TextoFormatado>> dados, FormatacaoTabela formatacaoTabela) {

        int qdtLinhas = dados.size();
        int qtdColunas = 0;

        for (int i = 0; i < dados.size(); i++) {
            if (qtdColunas < dados.get(i).size())
                qtdColunas = dados.get(i).size();
        }

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        formatacaoTabela.formatarTabela(tabela);

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
//            row.setHeight((int) (twipsPerInch * 1 / 5)); //set height 1/10 inch.
//            row.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact"
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }

            for (int j = 0; j < dados.get(i).size(); j++) {
                XWPFTableCell cell = j == 0 ? row.getCell(j) : row.createCell();
                cell.setWidth(formatacaoTabela.getWidths().get(j));
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                if (i == 0 && formatacaoTabela.isFirstLineHeader())
                    cell.getCTTc().addNewTcPr().addNewShd().setFill("cccccc");

                XWPFRun run = cell.getParagraphs().get(0).createRun();

                XWPFParagraph paragrafo = cell.getParagraphs().get(0);
                paragrafo.setSpacingBetween(1);

                dados.get(i).get(j).setParagraphText(paragrafo);

            }

        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable addTabelaColunaUnica(List<TextoFormatado> dados, FormatacaoTabela formatacaoTabela) {

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        for(int k = 1; k < tabela.getRows().size(); k++ ) {
            tabela.removeRow(k);
        }

        formatacaoTabela.formatarTabela(tabela);

        if(dados.size() == 0) {
            return tabela;
        }

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }
            XWPFTableCell cell = null;

            if(row.getTableCells().size() == 0)
                cell = row.createCell();
            else
                cell = row.getCell(0);

            cell.setWidth("100%");
            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            if (i % 2 == 0 )
                cell.getCTTc().addNewTcPr().addNewShd().setFill("dce6f1");

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            XWPFParagraph paragrafo = cell.getParagraphs().get(0);
            paragrafo.setSpacingBetween(1);

            dados.get(i).setParagraphText(paragrafo);
        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable mergeCells(XWPFTable tabela, List<MergePosition> cellsToMerge) {
        tabela = mergeCellsHorizontal(tabela, cellsToMerge);
        tabela = mergeCellsVertical(tabela, cellsToMerge);
        return tabela;

    }

    private void addListaNumerada(List<TextoFormatado> listItens) {

        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
        //Next we set the AbstractNumId. This requires care.
        //Since we are in a new document we can start numbering from 0.
        //But if we have an existing document, we must determine the next free number first.
        cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));

        /* Bullet list
          CTLvl cTLvl = cTAbstractNum.addNewLvl();
          cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
          cTLvl.addNewLvlText().setVal("•");
        */

        ///* Decimal list
        CTLvl cTLvl = cTAbstractNum.addNewLvl();
        cTLvl.addNewNumFmt().setVal(STNumberFormat.DECIMAL);
        cTLvl.addNewPPr();
        CTInd ind = cTLvl.getPPr().addNewInd(); //Set the indent

        ind.setHanging(BigInteger.valueOf(360*2));
        ind.setLeft(BigInteger.valueOf(360*6));
        cTLvl.addNewLvlText().setVal("%1.");
        cTLvl.addNewStart().setVal(BigInteger.valueOf(1));
        //*/

        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

        XWPFNumbering numbering = document.createNumbering();

        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

        BigInteger numID = numbering.addNum(abstractNumID);

        for (TextoFormatado item : listItens) {
            item.setListNumId(numID);
            addParagrafo(item);
        }
    }

    private XWPFTable mergeCellsHorizontal(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTHMerge hMerge = CTHMerge.Factory.newInstance();
            hMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setHMerge(hMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeHorizontal().size(); j++) {
                CTHMerge hToMerge = CTHMerge.Factory.newInstance();
                hToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeHorizontal().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeHorizontal().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setHMerge(hToMerge);
            }
        }

        return tabela;
    }

    private XWPFTable mergeCellsVertical(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTVMerge vMerge = CTVMerge.Factory.newInstance();
            vMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setVMerge(vMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeVertical().size(); j++) {
                CTVMerge vToMerge = CTVMerge.Factory.newInstance();
                vToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeVertical().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeVertical().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setVMerge(vToMerge);
            }
        }

        return tabela;
    }

    private void addBreak() {
        this.document.createParagraph();//.createRun().addBreak();
    }

    private void addPageBreak() {
        XWPFParagraph paragraph = this.document.createParagraph();
        paragraph.setPageBreak(true);
    }

    public void addSaudacao() {
//        if(secaoFiscalizadoraContas != null && Character.isDigit(secaoFiscalizadoraContas.charAt(0))){
//            return "Senhor(a) Diretor(a) da "+ getDescricaoArea() + ",";
//        }
//        else{
//            return "Senhor(a) Diretor(a) da " + getDescricaoArea() + ",";
//        }
        addParagrafo(new TextoFormatado(tabelasProtocolo.getSaudacao(), formatacaoFactory.getBold(12)));
    }

    private void addTabelaIegm() {

//        NotaIegm notaIegm2016 = this.notasIegm.get(2016) != null ? this.notasIegm.get(2016) : new NotaIegm();
//        NotaIegm notaIegm2017 = this.notasIegm.get(2017) != null ? this.notasIegm.get(2017) : new NotaIegm();
        //NotaIegm notaIegm2018 = this.notasIegm.get(2018) != null ? this.notasIegm.get(2018) : new NotaIegm();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"25%", "25%", "25%", "25%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("EXERCÍCIOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado((exercicio-3) + "", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado((exercicio-2) + "", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado((exercicio-1) + "", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("IEG-M", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIegm(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("i-Planejamento", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIPlanejamento(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIPlanejamento(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIPlanejamento(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("i-Fiscal", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIFiscal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIFiscal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIFiscal(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("i-Educ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIEduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIEduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado( resultadoIegmAnoBaseAnterior.getFaixaIEduc(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("i-Saúde", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaISaude(), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaISaude(), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaISaude(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("i-Amb", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIAmb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIAmb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIAmb(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("i-Cidade", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaICidade(), formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaICidade(), formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaICidade(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("i-Gov-TI", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(resultadoIegmAnoBaseReRetrasado.getFaixaIGov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(resultadoIegmAnoBaseRetrasado.getFaixaIGov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(resultadoIegmAnoBaseAnterior.getFaixaIGov(), formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void addTabelaDescricaoFonteDado() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "30%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("DESCRIÇÃO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("FONTE/DATA", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DADO/ANO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("POPULAÇÃO", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Site IBGE-Cidades", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(municipioIegmCodigoIbge.getPopulacao() + " habitantes", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("ARRECADAÇÃO MUNICIPAL", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("Audesp", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespResultadoExecucaoOrcamentariaExcercicioAnt.get("vRecArrec"), formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaPareceres() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "30%", "50%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Exercícios", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Processos", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Pareceres", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getTc(), formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getParecer(), formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getTc(), formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getParecer(), formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getTc(), formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getParecer(), formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }

    private void addTabelaExecucaoOrcamentaria() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"60%", "25%", "15%"}, true);

        Map<String, String> audespResultado = this.audespResultadoExecucaoOrcamentaria ;


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Valores", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("(+) RECEITAS REALIZADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha2.add(new TextoFormatado(audespResultado.get("vSubTotRecRealPM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("(-) DESPESAS EMPENHADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado(audespResultado.get("despesaEmpenhadaTotal"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("(-) REPASSES DE DUODÉCIMOS À CÂMARA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado(audespResultado.get("vRepDuodCM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha5 = new ArrayList<>();
        cabecalhoLinha5.add(new TextoFormatado("(+) DEVOLUÇÃO DE DUODÉCIMOS DA CÂMARA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado(audespResultado.get("vDevDuod"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha6 = new ArrayList<>();
        cabecalhoLinha6.add(new TextoFormatado("(-) TRANSFERÊNCIAS FINANCEIRAS À ADMINISTRAÇÃO INDIRETA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado(audespResultado.get("vTransfFinAdmIndExec"), formatacaoFactory.getRight(9)));


        List<TextoFormatado> cabecalhoLinha7 = new ArrayList<>();
        cabecalhoLinha7.add(new TextoFormatado("(+ ou -) AJUSTES DA FISCALIZAÇÃO", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha8 = new ArrayList<>();
        cabecalhoLinha8.add(new TextoFormatado("RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBold(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("resultadoExecucaoOrcamentaria"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("percentualExecucaoOrcamentaria"), formatacaoFactory.getBoldRight(9)));

        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);
        dados.add(cabecalhoLinha5);
        dados.add(cabecalhoLinha6);
        dados.add(cabecalhoLinha7);
        dados.add(cabecalhoLinha8);

        addTabela(dados, formatacaoTabela);
    }

    private void addAnaliseFontesDocumentais() {
        List<TextoFormatado> itensLista = new ArrayList<>();
        itensLista.add(new TextoFormatado("Indicadores finalísticos componentes do IEG-M – Índice de" +
                " Efetividade da Gestão Municipal;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Ações fiscalizatórias desenvolvidas através da seletividade " +
                "(contratos e repasses) e da fiscalização ordenada; ", formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Prestações de contas mensais do exercício em exame, encaminhadas pela " +
                "Chefia do Poder Executivo;", formatacaoFactory.getFormatacao(12)));

        itensLista.add(new TextoFormatado("Resultado do acompanhamento simultâneo do Sistema Audesp, bem como " +
                "acesso aos dados, informações e análises " +
                "disponíveis no referido ambiente;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das denúncias, representações e expedientes diversos; ",
                formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Leitura analítica dos três últimos relatórios de fiscalização e" +
                " respectivas decisões desta Corte, sobretudo no tocante a assuntos relevantes nas ressalvas, " +
                "advertências e recomendações;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das informações disponíveis nos demais " +
                "sistemas de e. Tribunal de Contas do Estado.", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Outros assuntos relevantes obtidos em pesquisa aos sítios de " +
                "transparência dos Órgãos Fiscalizados ou outras fontes da rede mundial " +
                "de computadores.", formatacaoFactory.getFormatacao(12)));

        addListaNumerada(itensLista);
    }

    private void addTabelaObrasParalizadas() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "15%", "15%",  "15%", "15%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("OBRAS PARALISADAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("TC ", formatacaoFactory.getBold(9))
                .concat("(se houver)", formatacaoFactory.getVermelho(12)));
        linha2.add(new TextoFormatado("Valor inicial do Contrato (R$)", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Valor total pago (R$)", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Contratada", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Data da paralisação", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Descrição da obra", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 3));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 4));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 5));

        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaComparativoLimiteLRF() {


        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "20%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("QUADRO COMPARATIVO COM OS LIMITES DA LRF", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("R$", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("RECEITA CORRENTE LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(limiteLRFMap.get("vRCL"), formatacaoFactory.getBoldRight(9)));
        linha2.add(new TextoFormatado(limiteLRFMap.get("rclPerc"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DÍVIDA CONSOLIDADA LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(limiteLRFMap.get("vDivConsolidLiq"), formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(limiteLRFMap.get("vPercDivConsolidLiq"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Limite Legal - Artigos 3º e 4º. Resolução 40 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(limiteLRFMap.get("divConsolidLimite"), formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(limiteLRFMap.get("divConsolidPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(limiteLRFMap.get("excessoDividaConsolidada"), formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("CONCESSÕES DE GARANTIAS",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Montante",
                formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(limiteLRFMap.get("vConcGar"), formatacaoFactory.getRight(9))); //VER
        linha8.add(new TextoFormatado(limiteLRFMap.get("vConcGar"), formatacaoFactory.getRight(9))); //VER

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Limite Legal - Artigo 9º. Resolução 43 do Senado", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(limiteLRFMap.get("concGarantiaLimite"), formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(limiteLRFMap.get("concGarantiaPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(limiteLRFMap.get("excessoConcessaoGarantias"), formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO - Exceto ARO",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(limiteLRFMap.get("vOpCred"), formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(limiteLRFMap.get("opCredPercentual"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Limite Legal - Artigo 7º, I. Resolução 43 do Senado ",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(limiteLRFMap.get("vLimOpCred"), formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(limiteLRFMap.get("opCreditoExcAroPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(limiteLRFMap.get("excessoOperacoesCredito"), formatacaoFactory.getFormatacao(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO (Exceto ARO) > DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("ANTECIPAÇÃO DE RECEITAS ORÇAMENTÁRIAS - ARO",
                formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("Limite Legal - Artigo 10. Resolução 43 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha21.add(new TextoFormatado(limiteLRFMap.get("opAntecipacaoAroLimite"), formatacaoFactory.getRight(9)));
        linha21.add(new TextoFormatado(limiteLRFMap.get("opAntecipacaoAroPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha22.add(new TextoFormatado(limiteLRFMap.get("excessoARO"), formatacaoFactory.getFormatacao(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("RECURSOS OBTIDOS COM A ALIENAÇÃO DE ATIVOS",
                formatacaoFactory.getBold(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha24 = new ArrayList<>();
        linha24.add(new TextoFormatado("Saldo do exercício anterior",
                formatacaoFactory.getBold(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha25 = new ArrayList<>();
        linha25.add(new TextoFormatado("Valor arrecadado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha26 = new ArrayList<>();
        linha26.add(new TextoFormatado("Valor aplicado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha27 = new ArrayList<>();
        linha27.add(new TextoFormatado("Saldo a Aplicar",
                formatacaoFactory.getBold(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
//        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);
        dados.add(linha24);
        dados.add(linha25);
        dados.add(linha26);
        dados.add(linha27);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(2, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(2, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(2, 2));

        MergePosition mergeH2 = new MergePosition(6, 0);
        mergeH2.addToMergeHorizontal(new MergePosition(6, 1));
        mergeH2.addToMergeHorizontal(new MergePosition(6, 2));

        MergePosition mergeH3 = new MergePosition(10, 0);
        mergeH3.addToMergeHorizontal(new MergePosition(10, 1));
        mergeH3.addToMergeHorizontal(new MergePosition(10, 2));

        MergePosition mergeH4 = new MergePosition(14, 0);
        mergeH4.addToMergeHorizontal(new MergePosition(14, 1));
        mergeH4.addToMergeHorizontal(new MergePosition(14, 2));

        MergePosition mergeH5 = new MergePosition(16, 1);
        mergeH5.addToMergeHorizontal(new MergePosition(16, 2));

        MergePosition mergeH6 = new MergePosition(17, 0);
        mergeH6.addToMergeHorizontal(new MergePosition(17, 1));
        mergeH6.addToMergeHorizontal(new MergePosition(17, 2));

        MergePosition mergeH7 = new MergePosition(21, 0);
        mergeH7.addToMergeHorizontal(new MergePosition(21, 1));
        mergeH7.addToMergeHorizontal(new MergePosition(21, 2));

        MergePosition mergeV1 = new MergePosition(21, 2);
        mergeV1.addToMergeVertical(new MergePosition(22, 2));
        mergeV1.addToMergeVertical(new MergePosition(23, 2));
        mergeV1.addToMergeVertical(new MergePosition(24, 2));
        mergeV1.addToMergeVertical(new MergePosition(25, 2));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeH6);
        mergePositions.add(mergeH7);
        mergePositions.add(mergeV1);
        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesaDePessoalPrimeiroQuadrimestre() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%", "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("" + (exercicio-1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio-1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio-1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("% Permitido Legal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Gasto Informado",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio ) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Gastos Ajustados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receita Corrente Líquida",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RCL Ajustada",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("% Gasto Informado",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 4), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("% Gasto Ajustado",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 4), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));


        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesaDePessoalSegundoQuadrimestre() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%", "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("" + (exercicio-1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio-1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("" + (exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("% Permitido Legal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Gasto Informado",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Gastos Ajustados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receita Corrente Líquida",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RCL Ajustada",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("% Gasto Informado",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("% Gasto Ajustado",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8), formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));


        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaPublicidadeEmAnoEleitoral() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%", "20%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Publicidade em ano Eleitoral ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Semestres:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("1º e 2º quadr./" + (exercicio-3), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("1º e 2º quadr./" + (exercicio-2), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("1º e 2º quadr./" + (exercicio-1), formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("até 15/08/" + (exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Despesas", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Média apurada nos períodos dos exercícios anteriores",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

//        List<TextoFormatado> linha5 = new ArrayList<>();
//        linha5.add(new TextoFormatado("DESPESAS DO EXERCÍCIO ", formatacaoFactory.getJustificado(9))
//                        .concat("INFERIORES", formatacaoFactory.getBold(9))
//                        .concat(" À MÉDIA EM:", formatacaoFactory.getJustificado(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
//        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
//        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 3));
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 4));

        MergePosition mergeH2 = new MergePosition(3, 0);
        mergeH2.addToMergeHorizontal(new MergePosition(3, 1));
        mergeH2.addToMergeHorizontal(new MergePosition(3, 2));
        mergeH2.addToMergeHorizontal(new MergePosition(3, 3));

//        MergePosition mergeH3 = new MergePosition(4, 0);
//        mergeH3.addToMergeHorizontal(new MergePosition(4, 1));
//        mergeH3.addToMergeHorizontal(new MergePosition(4, 2));
//        mergeH3.addToMergeHorizontal(new MergePosition(4, 3));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
//        mergePositions.add(mergeH3);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesasCfFundeb() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 212 da Constituição Federal:", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespEnsinoFundeb.get("vPercLiqEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespEnsinoFundeb.get("vPercPagoEnsino"), formatacaoFactory.getRight(9)));

        List<List<TextoFormatado>> dados2 = new ArrayList<>();

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("FUNDEB:", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundebMagist"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundebMagist"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundebMagist"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados2.add(linha5);
        dados2.add(linha6);
        dados2.add(linha7);
        dados2.add(linha8);
        dados2.add(linha9);
        dados2.add(linha10);
        dados2.add(linha11);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
        XWPFTable tabela2 = addTabela(dados2, formatacaoTabela);

    }

    private void addTabelaApuracaoDisponibilidadesDeCaixa() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Evolução da liquidez entre 30.04 e 31.12 (projetado) do exercício de:", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("" + exercicio, formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Disponbilidade de Caixa em 30.04", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("(-) Saldo de Restos a Pagar em 30.04", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("(-) Empenhos Liquidados a Pagar em 30.04", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("(-) Saldo da Despesa Empenhada a Liquidar", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5b = new ArrayList<>();
        linha5b.add(new TextoFormatado("(-) Valores Restituíveis", formatacaoFactory.getFormatacao(9)));
        linha5b.add(new TextoFormatado("", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Equilíbrio em 30.04:", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("R$", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("(+) Saldo da Receita Prevista a Realizar", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("(-) Saldo da Despesa Autorizada a Empenhar", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("(-) Saldo das Transferências Financeiras a Realizar", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Liquidez projetada em 31.12", formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado("R$", formatacaoFactory.getBold(9)));





        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha5b);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaVagasEscolares() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "20%", "20%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("NÍVEL", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DEMANDA POR VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("OFERTA DE VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("RESULTADO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Ens. Infantil (Creche)", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ens. Infantil (Pré escola)", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ens. Fundamental (Anos Iniciais)", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Ens. Fundamental (Anos Finais)", formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaEmpenhadaLiquidadaPaga() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 77, III c/c § 4º do ADCT", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespSaude.get("vPercEmpSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespSaude.get("vPercLiqSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespSaude.get("vPercPagoSaude"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaProcessoDeContasAnuais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "70%", "25%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Número:", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC-XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Interessado:",
                formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Objeto:",
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Procedência:",
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaDoisUltimosExerciciosApreciados(ParecerPrefeitura parecerPrefeitura){

        if(parecerPrefeitura == null) {
            addTabelaDoisUltimosExerciciosApreciados();
            return;
        }

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%", "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();

        linha1.add(new TextoFormatado("Exercício\n " + parecerPrefeitura.getExercicio(), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC\n " + parecerPrefeitura.getTc(), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DOE\n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data do Trânsito em julgado\n " + parecerPrefeitura.getDataTransitoJulgadoString(),
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Recomendações:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(1, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


        mergePositions.add(mergeH1);


        mergeCells(tabela, mergePositions);
    }

    private void addTabelaDoisUltimosExerciciosApreciados() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%", "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();






        linha1.add(new TextoFormatado("Exercício \n XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DOE \n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data do Trânsito em julgado \n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Recomendações:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));


        dados.add(linha1);
        dados.add(linha2);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(1, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


        mergePositions.add(mergeH1);


        mergeCells(tabela, mergePositions);

    }


    public ResponseEntity<Resource> download(Integer codigoIBGE, Integer exercicio, Integer quadrimestre)
            throws IOException, InvalidFormatException, Exception {

        this.document = new XWPFDocument();

        this.quadrimestre = quadrimestre;

        this.exercicio = exercicio;
        this.codigoIBGE = codigoIBGE;

        this.pareceresPrefeiturasList = this.parecerPrefeituraService.getParecerByCodigoIbge(codigoIBGE);
        this.audespEntidade = this.audespEntidadeService.getEntidade(codigoIBGE,exercicio);
//        this.apontamentosODS = apontamentosODSService.getApontamentosODS(codigoIBGE, exercicio);
        this.notasIegm = iegmService.getMapNotasByCodigoIbge(codigoIBGE, exercicio-1);
        this.resultadoIegmAnoBaseAnterior = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio-1);
        this.resultadoIegmAnoBaseRetrasado = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio-2);
        this.resultadoIegmAnoBaseReRetrasado = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio-3);
        this.responsavelPrefeitura = audespService.getResponsavelPrefeitura(codigoIBGE,exercicio,quadrimestre);
        this.responsavelSubstitutoPrefeitura = audespService.getResponsavelSubstitutoPrefeitura(codigoIBGE,exercicio, quadrimestre);
        this.municipioIegmCodigoIbge = tcespBiService.getMunicipioByCodigoIbgeExercicio(codigoIBGE, exercicio);
        this.audespResultadoExecucaoOrcamentaria = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestralFormatado(codigoIBGE, exercicio, 4 * quadrimestre);
        this.audespResultadoExecucaoOrcamentariaExcercicioAnt = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestralFormatado(codigoIBGE, exercicio-1, 12);
        this.tabelasProtocolo = tabelasService.getTabelasProtocoloPrefeituraByCodigoIbge(codigoIBGE, exercicio);
//        this.anexo14AMap = this.demonstrativosRaeeService.getAnexo14A(audespEntidade, codigoIBGE, exercicio, 12);
        this.audespEnsinoFundeb = audespEnsinoService.getAudespEnsinoFundeb(codigoIBGE, exercicio, 4 * quadrimestre);
        this.audespSaude = audespSaudeService.getAudespSaude(codigoIBGE, exercicio, 4 * quadrimestre);
//        this.aplicacoesEmSaude = audespSaudeService.getAplicacoesEmSaudeFormatado(codigoIBGE, exercicio, 12);
        this.audespDespesaPessoalMap = this.audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbgeFechamentoFormatado(codigoIBGE, exercicio, 50);
//        this.quadroGeralEnsinoMap = this.audespEnsinoService.getQuadroGeralEnsinoFormatado(codigoIBGE,exercicio,12);
//        this.apontamentoFOMap = this.apontamentosFOService.getApontamentosFO(codigoIBGE,exercicio);
//        this.audespDividaAtivaMap = this.audespDividaAtivaService.getDividaAtivaFormatado(codigoIBGE,exercicio,12);
//        this.audespFase3QuadroDePessoalMap = this.audespFase3Service.getQuadroDePessoal(audespEntidade.getEntidadeId(), exercicio);
//        this.valorInvestimentoMunicipioMap = this.audespBiService.getValorInvestimentoMunicipio(codigoIBGE,exercicio, 12);
//        this.rclMunicipioDevedoresMap = this.tcespBiService.getRCLMunicipioFormatado(codigoIBGE, exercicio);
        Integer quantidadeAlertasDesajusteExecucaoOrcamentaria = audespAlertasService.getAlertasDesajusteExecucaoOrcamentaria(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasEducacao = audespAlertasService.getAlertasEducacao(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasSaude = audespAlertasService.getAlertasSaude(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasLRFa59p1i1 = audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"GF15"});
        Integer quantidadeAlertasLRFa59p1i2 = audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"GF27"});
        Integer quantidadeAlertasLRFa59p1i5Cobertura2UltimosQuadrimestres = audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"GF37"});
        Integer quantidadeAlertasLRFa59p1i5PessoalUltimos180Mandato= audespAlertasService.getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"GF36"});
        Integer quantidadeAlertasLRFa59p1i5AplicacaoEnsino = audespAlertasService
                .getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"AE03", "AE05", "AE06"});
        Integer quantidadeAlertasLRFa59p1i5AplicacaoSaude = audespAlertasService
                .getAlertasByItemAnalise(codigoIBGE, exercicio, 12, new String[]{"AS03"});
        Integer quantidadeAlertasDespesaPessoal = audespAlertasService.getAlertasDespesaComPessoal(codigoIBGE, exercicio, 12);
        this.audespResultadoExecucaoOrcamentariaMap = audespResultadoExecucaoOrcamentariaService
        .getAudespResultadoExecucaoOrcamentariaUltimosTresExercicios(this.codigoIBGE, exercicio, 4 * quadrimestre);
        this.limiteLRFMap = audespLimiteLrfService
                .getAudespResultadoInfluenciaOrcamentarioFinanceiro(codigoIBGE, exercicio, 4 * quadrimestre);


        String quadrimestreTitulo = getQuadrimestreTitulo(quadrimestre);
        String quadrimestreTituloAno = getQuadrimestreTituloComAno(quadrimestre);


        // margin
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        CTPageMar pageMar = sectPr.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(1700L));
        pageMar.setRight(BigInteger.valueOf(1700L));
        pageMar.setBottom(BigInteger.valueOf(1700L));

        CTBody body = document.getDocument().getBody();

        if (!body.isSetSectPr()) {
            body.addNewSectPr();
        }
        CTSectPr section = body.getSectPr();

        if(!section.isSetPgSz()) {
            section.addNewPgSz();
        }
        CTPageSz pageSize = section.getPgSz();

        pageSize.setW(BigInteger.valueOf(595*20));
        pageSize.setH(BigInteger.valueOf(842*20));

        createDocumentStyles();

        getHeader();

        addParagrafo(new TextoFormatado("RELATÓRIO DE FISCALIZAÇÃO" + quadrimestreTitulo + " \nPREFEITURA MUNICIPAL " ,
                formatacaoFactory.getBoldCenter(12)));
        addBreak();
        addBreak();

        addTabelaDadosIniciais();

        addBreak();
        addSaudacao();
        addBreak();
        addBreak();

        addParagrafo(addTab().concat("Este relatório consolida o resultado do acompanhamento das informações " +
                        "prestadas a esta e. Corte de Contas pelo órgão, no período em epígrafe.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(addTab().concat("Em atendimento ao TC-A-30973/026/00, registramos a notificação do(s) Sr.(s). ",
                        formatacaoFactory.getJustificado(12))
                        .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(", responsável(is) pelas contas em exame.", formatacaoFactory.getJustificado(12))
                );


        addBreak();

        addParagrafo(new TextoFormatado("Informamos que o município possui a seguinte série histórica de " +
                "classificação no Índice de Efetividade da Gestão Municipal-IEG-M:",
            formatacaoFactory.getJustificado(12)));
        addTabelaIegm();
        addTextoStatusFaseIEGM();

        addBreak();

        addParagrafo(addTab().concat("SÓ INSERIR NO QUADRO APÓS A VALIDAÇÃO DO IEG-M PELA FISCALIZAÇÃO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO DEVE APENAS MENCIONAR OS ÍNDICES EM RELATÓRIO, SENDO " +
                "VEDADA A DIVULGAÇÃO AOS JURISDICIONADOS DOS ÍNDICES AINDA NÃO CHANCELADOS E TORNADOS PÚBLICOS PELA" +
                " DIREÇÃO DA CASA." ,
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));

        addBreak();
//
//        addParagrafo(new TextoFormatado("O IEG-M INSERIDO NO EXERCÍCIO ANTERIOR SERÁ AQUELE APURADO APÓS A " +
//                "VERIFICAÇÃO/VALIDAÇÃO DA FISCALIZAÇÃO.\n CASO NÃO TENHA SIDO CONCLUÍDA A VALIDAÇÃO, CONSTAR O ITEM" +
//                " COMO PREJUDICADO E INSERIR A OBSERVAÇÃO CONSTANTE NA ALTERNATIVA DO QUADRO ACIMA.\n" +
//                "\n",
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));
//
//        addBreak();
//        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO PODE APENAS MENCIONAR OS ÍNDICES EM RELATÓRIO, SENDO " +
//                "VEDADA A DIVULGAÇÃO AOS JURISDICIONADOS DOS ÍNDICES AINDA NÃO CHANCELADOS E TORNADOS PÚBLICOS " +
//                "PELA DIREÇÃO DA CASA.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("A Prefeitura analisada obteve, nos 03 (três) últimos exercícios" +
//                        " apreciados, os seguintes ",
//                formatacaoFactory.getJustificado(12))
//                .concat("PARECERES", formatacaoFactory.getBold(12))
//                .concat(" na apreciação de suas contas:", formatacaoFactory.getJustificado(12))
//        );
//
//        addTabelaPareceres();

        addBreak();
        addParagrafo(addTab().concat("A Fiscalização planejou a execução de seus trabalhos, agregando a" +
                        " análise das seguintes fontes documentais:",
                formatacaoFactory.getJustificado(12)));

        addAnaliseFontesDocumentais();

        addBreak();

        //############################################
        //# DETALHES PARECERES EXERCICIOS ANTERIORES #
        //############################################


        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": UTILIZAR SE FOR O CASO DO 2º QUADRIMESTRE",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(addTab().concat("O relatório do 1º quadrimestre está colacionado no evento ",
                formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" destes autos.", formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": UTILIZAR EM TODOS OS QUADRIMESTRES",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("O presente relatório quadrimestral visa contribuir para a tomada de " +
                        "providências dentro do próprio exercício, possibilitando a correção de eventuais falhas, " +
                        "resultando numa melhoria das contas apresentadas.",
                        formatacaoFactory.getJustificado(12)));
        addParagrafo(addTab().concat("Saliente-se, por oportuno, que os dados poderão ser reavaliados quando " +
                        "da fiscalização do fechamento do exercício, oportunidade em que todos os balanços " +
                        "contábeis estarão encerrados.",
                        formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": UTILIZAR CONFORME ORIENTAÇÃO DOS DSFs",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                        );
        addBreak();

        addParagrafo(addTab().concat("Ressaltamos, ainda, que a fiscalização, em virtude das limitações " +
                "de locomoção causadas pela pandemia do novo Coronavírus (COVID-19), foi efetivada " +
                "remotamente, por meio de todas as ferramentas e sistemas disponíveis. ",
                formatacaoFactory.getJustificadoFundoVerde(12)));
        addParagrafo(addTab().concat("Ademais, foi antecedida de criterioso planejamento, com base no princípio" +
                " da amostragem, que indicou a necessária extensão dos exames.",
                formatacaoFactory.getJustificadoFundoVerde(12)));
        addParagrafo(addTab().concat("Outrossim, consignamos que foi autuado o processo TC-XXXXXX.989.20, para " +
                        "fins de Acompanhamento Especial da gestão das medidas de combate à referida pandemia.  ",
                formatacaoFactory.getJustificadoFundoVerde(12)));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA A: PLANEJAMENTO", formatacaoFactory.getBold(12)), heading1);

        addBreak();
        addSecao(new TextoFormatado("A.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS - PLANEJAMENTO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addSecao(new TextoFormatado("A.1.1. CONTROLE INTERNO",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();

        TextoFormatado aquiSeraoTrazidas = new TextoFormatado("AQUI SERÃO TRAZIDAS CONSTATAÇÕES RELEVANTES " +
                "SOBRE O CONTROLE INTERNO E SUAS ATRIBUIÇÕES.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12));
        addParagrafo(aquiSeraoTrazidas);
        addBreak();

        TextoFormatado buscarAferir = new TextoFormatado("BUSCAR AFERIR SE O CONTROLE INTERNO TÊM EXERCIDO DE " +
                "MANEIRA EFETIVA SUAS ATRIBUIÇÕES NO PERÍODO",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12));
        addParagrafo(buscarAferir);
        addBreak();

        addParagrafo(new TextoFormatado("ABORDAGENS POSSÍVEIS: CORREÇÃO NA REGULAMENTAÇÃO DA " +
                "MATÉRIA, NA INVESTIDURA NO CARGO, NO EXERCÍCIO DAS ATRIBUIÇÕES (EMISSÃO PERIÓDICA DE RELATÓRIOS E " +
                "ANÁLISES REALIZADAS), SEMPRE QUE POSSÍVEL CORRELACIONANDO ÀS RECOMENDAÇÕES DO TCESP E AOS APONTAMENTOS" +
                " TRAZIDOS AO RELATÓRIO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("VERIFICAÇÃO OBRIGATÓRIA, A SER RELATADA SE O CASO (CONSIDERAR A REALIDADE" +
                " DO MUNICÍPIO): INFORMAR SE O CONTROLE INTERNO ESTÁ ATUANDO NO CONTROLE DOS ATOS E DESPESAS " +
                "RELACIONADAS À PANDEMIA COVID-19 (COMUNICADO SDG 17).",
                formatacaoFactory.getJustificadoFundoVerde(12)));
        addBreak();
        addBreak();
        addSecao(new TextoFormatado("A.2. IEG-M – I-PLANEJAMENTO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("SÓ INSERIR ESTE SUBITEM CASO TENHA SIDO EFETUADA A VALIDAÇÃO DO IEG-M " +
                "PELA FISCALIZAÇÃO OU CASO HAJA SITUAÇÕES RELACIONADAS A ELE QUE INDIQUEM RISCOS E JUSTIFIQUEM SUA INSERÇÃO. ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("PARA ISSO, UTILIZAR-SE DO RELATÓRIO SMART GERADO PELA DIVISÃO " +
                "AUDESP-INDICADORES APÓS A VALIDAÇÃO FEITA PELA FISCALIZAÇÃO. ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("SEGUEM ABAIXO SUGESTÕES DE CRÍTICAS SOBRE ESTE ITEM:",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("•\tAUSÊNCIA DE COERÊNCIA ENTRE ÍNDICES PREVISTOS E ATUAIS;",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("•\tESTRUTURA DO SETOR DE PLANEJAMENTO;",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("•\tAUDIÊNCIAS PÚBLICAS REALIZADAS NO EXERCÍCIO EM EXAME, PARA DISCUSSÃO" +
                " DAS PEÇAS ORÇAMENTÁRIAS EM HORÁRIOS INADEQUADOS À PARTICIPAÇÃO DA POPULAÇÃO. ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("VER ORIENTAÇÕES DO APÊNDICE II, AO FINAL DO MODELO DE FECHAMENTO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                );
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS OCORRÊNCIAS DIGNAS DE NOTA, EXCLUIR O CONTEÚDO E UTILIZAR" +
                                " O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa " +
                "dimensão do IEG-M.", formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º QUADRIMESTRE. ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto " +
                "de apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("A.3. OBRAS PARALISADAS",
                formatacaoFactory.getBold(12))
        );
        addBreak();

        addParagrafo(new TextoFormatado("Tendo em vista informações fornecidas pela Origem e também verificações " +
                "efetuadas durante o quadrimestre, há obras paralisadas no município, conforme segue:",
                formatacaoFactory.getJustificado(12))
        );

        addTabelaObrasParalizadas();
        addParagrafo(new TextoFormatado("Disponível em: ", formatacaoFactory.getFormatacao(10))
                .concat("https://paineldeobras.tce.sp.gov.br/pentaho/api/repos/%3Apublic%3AObra%3Apainel_obras.wcdf/generatedContent?userid=anony&password=zero",
                        formatacaoFactory.getBoldItalicAzulJustificado(10))
                .concat(". Acesso em: dd. mmm. aaaa.", formatacaoFactory.getJustificadoVermelhoCinza(10)));

        addBreak();

        addParagrafo(new TextoFormatado("Observações", formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": ESSAS INFORMAÇÕES ESTÃO DISPONÍVEIS NO PAINEL DE OBRAS PÚBLICAS.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
        );

        addParagrafo(new TextoFormatado("CASO SEJA UM CONTRATO ASSINADO E/OU ENCAMINHADO ", formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("NO EXERCÍCIO EM EXAME", formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(", DEVE SE INFORMAR NO QUADRO DE OBRAS PARALISADAS (RETRO) E NO “QUADRO COMPLETO” " +
                                "(CONFORME ESTÁ NO MODELO DE RELATÓRIO DE FECHAMENTO DE 2019), E NAS " +
                                "CORRESPONDENTES PERSPECTIVAS, ",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("SE", formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat("IMPACTAREM NA ANÁLISE DO ITEM [P. EX., OBRA PARALISADA DE CRECHE, HAVENDO DÉFICIT DE " +
                                "VAGAS]).", formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
        );

        addBreak();
        addParagrafo(addTab().concat("Constatamos ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("ou", formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat(" Não constatamos", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("inobservância ao art. 45 da Lei de Responsabilidade Fiscal, tendo em vista que ",
                        formatacaoFactory.getJustificado(12))
                .concat("descrever, caso irregular.",
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
        );

        addParagrafo(addTab().concat("Conforme calendário de obrigações do Sistema Audesp, a Prefeitura Municipal ",
                        formatacaoFactory.getJustificado(12))
                .concat("( não )",
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" vem atualizando a este Tribunal as informações sobre Obras Paralisadas e/ou Atrasadas.",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS OCORRÊNCIAS DIGNAS DE NOTA, EXCLUIR O CONTEÚDO E UTILIZAR O " +
                                "SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No acompanhamento do quadrimestre não constatamos ocorrências dignas de nota.",
            formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();


        addSecao(new TextoFormatado("PERSPECTIVA B: GESTÃO FISCAL", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addSecao(new TextoFormatado("B.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS - GESTÃO FISCAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(addTab().concat("Face ao contido no art. 1º, § 1º da Lei Complementar Federal nº 101, de " +
                        "4 de maio de 2000 (Lei de Responsabilidade Fiscal), o qual estabelece os pressupostos da " +
                        "responsabilidade da gestão fiscal, passamos a expor o que segue. ",
                formatacaoFactory.getJustificado(12)));
        addParagrafo(addTab().concat("Informamos, por oportuno, que o município ",
            formatacaoFactory.getJustificado(12))
                .concat("não", formatacaoFactory.getVermelhoAmarelo(12))
                .concat(" aderiu ao Programa de Acompanhamento e Transparência Fiscal instituído pela" +
                        " Lei nº 178, de 13 de janeiro de 2021.", formatacaoFactory.getJustificado(12))
        );
        addParagrafo(new TextoFormatado("ORIENTAÇÃO",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat("VER NT SDG Nº 159/2021 E QUESTÃO 45 DO QUESTIONÁRIO MENSAL DE ENFRENTAMENTO À " +
                                "PANDEMIA COVID-19.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addBreak();

        addSecao(new TextoFormatado("B.1.1. RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA NO PERÍODO",
                formatacaoFactory.getBold(12)), heading3);

//        addBreak();
//        addParagrafo(new TextoFormatado("TENDO EM VISTA A NT SDG Nº 154 DE 14 DE JANEIRO DE 2020 QUE TRATA DOS " +
//                "REPASSES EFETUADOS PELO ESTADO PARA OS MUNICÍPIOS NO FINAL DO MÊS DE DEZEMBRO DE 2019, DEVE A " +
//                "FISCALIZAÇÃO VERIFICAR A FORMA DE CONTABILIZAÇÃO E O IMPACTO NA RECEITA, CASO ESTES VALORES TENHAM " +
//                "SIDO CONTABILIZADOS EM 2020, DEVERÁ A FISCALIZAÇÃO EFETUAR O AJUSTE NO QUADRO DE ANÁLISE.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();
        addTabelaExecucaoOrcamentaria();
        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp",
                formatacaoFactory.getBoldJustificado(10))
            .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(10)));

        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": CONSIDERAR OS DADOS ISOLADOS DA PREFEITURA, CONFORME APURADO PELO SISTEMA " +
                                "AUDESP, NÃO DEVENDO SER INCLUÍDAS ADMINISTRAÇÃO INDIRETA, FUNDOS PREVIDÊNCIA ETC.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("DEPENDE DE SINAL (+ OU -) APENAS A LINHA DE “AJUSTES DA FISCALIZAÇÃO”.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

//        addParagrafo(new TextoFormatado("TENDO EM VISTA A NT SDG Nº 154 DE 14 DE JANEIRO DE 2020 QUE TRATA DOS " +
//                "REPASSES EFETUADOS PELO ESTADO PARA OS MUNICÍPIOS NO FINAL DO MÊS DE DEZEMBRO DE 2019, DEVE A " +
//                "FISCALIZAÇÃO BUSCAR IDENTIFICAR EVENTUAL CONTABILIZAÇÃO DE TAIS RECEITAS NO EXERCÍCIO DE 2020, " +
//                "O QUE DESATENDE AO REGIME DE CAIXA, UMA VEZ QUE ESTAS PERTENCEM AO EXERCÍCIO DE 2019, SEM PREJUÍZO " +
//                "DOS IMPACTOS ESPECIALMENTE NA DESPESA DE PESSOAL, ENSINO E SAÚDE. CASO OCORRAM, FAZER AJUSTES NA " +
//                "LINHA DO QUADRO ACIMA “AJUSTES DA FISCALIZAÇÃO”. SENDO O CASO, DEVERÁ SER ABERTO O ITEM B.3.X. " +
//                "FISCALIZAÇÃO DAS RECEITAS, ADAPTANDO A CRÍTICA, CONFORME O CASO.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("EM CASO DE SITUAÇÃO DESFAVORÁVEL, DOCUMENTAR NOS AUTOS, E MENCIONAR A " +
                "EMISSÃO DE ALERTAS, CONFORME SEGUE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, I, da Lei de Responsabilidade Fiscal, o " +
                        "Município foi alertado tempestivamente, por ",
                formatacaoFactory.getJustificado(12))
                .concat(quantidadeAlertasLRFa59p1i1.toString() ,
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" vezes, sobre desajustes em sua execução orçamentária.",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();


        addParagrafo(addTab().concat("OBSERVAR A ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("DATA DE EMISSÃO DO ALERTA",
                        formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(" (CONSTANTE NO FINAL DO DOCUMENTO “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, " +
                                "VISTO QUE, NORMALMENTE OS DO FINAL DO EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, " +
                                "PORTANTO, SEM EFEITO. FACE À ANÁLISE REALIZADA PELO SISTEMA AUDESP, EM REGRA " +
                                "CONSIDERAR APENAS AS ANÁLISES DA RECEITA E DESPESA.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
        );
        addBreak();

        addParagrafo(addTab().concat("HIPÓTESE",
                        formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": DÉFICIT PELA DESPESA EMPENHADA, APURADO NO QUADRO ANTERIOR, INFORMAR O RESULTADO PELA " +
                                "DESPESA LIQUIDADA, CONFORME SEGUE",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
        );
        addBreak();

        addParagrafo(addTab().concat("Consideradas as despesas liquidadas, constata-se um (déficit/superávit)" +
                        " de R$ ", formatacaoFactory.getJustificado(12))
                .concat("XX.XXX",
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", correspondente a ",
                        formatacaoFactory.getJustificado(12))
                .concat("XX",
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("%.",
                        formatacaoFactory.getJustificado(12))

        );
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineJustificadoVermelhoFundoVerde(12))
                .concat("OBRIGATÓRIA A INFORMAÇÃO EM CASO DE DEFICIT",
                        formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12)));
        addParagrafo(addTab().concat("Face à perspectiva de deficit orçamentário, conforme retro descrito, ")
                .concat("informamos que o município ", formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("não", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12))
                .concat(" decretou estado de calamidade pública/emergência, devidamente reconhecido pela Assembleia " +
                        "Legislativa Estadual (art. 65 da Lei de Responsabilidade Fiscal). ",
                        formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("(adaptar conforme o caso)", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12) ));


        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.2. ANÁLISE DOS LIMITES E CONDIÇÕES DA LEI DE RESPONSABILIDADE FISCAL",
                formatacaoFactory.getBoldJustificado(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO À NT SDG Nº 141, QUANDO DA APURAÇÃO DA RCL, EM ESPECIAL " +
                "EM CASOS DE DESCUMPRIMENTO DOS LIMITES DA LRF.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)
        ));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": CASO ATENDIDOS OS LIMITES ESTABELECIDOS NA LRF USAR O CONTEÚDO ADIANTE.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();


        addParagrafo(addTab().concat("No período, as análises automáticas não identificaram descumprimentos aos" +
                        " limites estabelecidos na Lei de Responsabilidade Fiscal, quanto à Dívida Consolidada " +
                        "Líquida, Concessões de Garantias e Operações de Crédito, inclusive - ARO.",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": CASO NÃO ATENDIDO ALGUM DOS LIMITES ESTABELECIDOS NA LRF USAR O CONTEÚDO ADIANTE.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addTabelaComparativoLimiteLRF();

        addBreak();

        addParagrafo(addTab().concat("Verificamos o não atendimento aos limites estabelecidos pela Lei de" +
                        " Responsabilidade Fiscal, isso em decorrência do que segue: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("DESCREVER AS IRREGULARIDADES.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineJustificadoVermelhoFundoVerde(12))
                .concat(": OBRIGATÓRIA A INFORMAÇÃO EM CASO EMISSÃO DE ALERTAS",
                        formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12)));
        addBreak();
        addBreak();
        addParagrafo(addTab().concat("Ademais, cabe consignar que o município ",
                formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("não", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12))
                .concat(" decretou estado de calamidade pública/emergência, devidamente reconhecido pela " +
                                "Assembleia Legislativa Estadual, assim, ",
                        formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("não", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12))
                .concat(" sendo aplicável a suspensão de contagem de prazo para recondução aos limites, conforme" +
                                " art. 65 da Lei de Responsabilidade Fiscal. ",
                        formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("(adaptar conforme o caso)", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12))

        );

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.2.1. DESPESA DE PESSOAL",
                formatacaoFactory.getBold(12)), heading4);
        addBreak();

//        addParagrafo(new TextoFormatado("TENDO EM VISTA A NT SDG Nº 154 DE 14 DE JANEIRO DE 2020 QUE TRATA DOS " +
//                "REPASSES EFETUADOS PELO ESTADO PARA OS MUNICÍPIOS NO FINAL DO MÊS DE DEZEMBRO DE 2019, DEVE A " +
//                "FISCALIZAÇÃO VERIFICAR A FORMA DE CONTABILIZAÇÃO E O IMPACTO NA RECEITA, CASO ESTES VALORES TENHAM " +
//                "SIDO CONTABILIZADOS EM 2020, DEVERÁ A FISCALIZAÇÃO EFETUAR O AJUSTE NO QUADRO DE ANÁLISE (RCL).",
//                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": REGULARIDADE NOS 1º e 2º QUADRIMESTRES",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Conforme Relatórios de Gestão Fiscal emitidos pelo Sistema Audesp, referentes ao ",
                        formatacaoFactory.getJustificado(12))
                        .concat("1º e/ou 2º quadrimestres", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(" do exercício analisado, é possível ver que o Poder Executivo atendeu ao limite " +
                                "da despesa de pessoal previsto no art. 20, III, alínea “b” da Lei de " +
                                "Responsabilidade Fiscal.", formatacaoFactory.getJustificado(12))
                );

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat("HAVENDO ALTERAÇÕES POR AJUSTES DA FISCALIZAÇÃO ", formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("OU", formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat("AINDA DESATENDIMENTO AO LIMITE PARA DESPESAS DE PESSOAL ", formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("OU", formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(" CASO A FISCALIZAÇÃO ENTENDA QUE DEVEM SER INFORMADOS OS DADOS DE TODOS OS QUADRIMESTRES ",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("1º QUADRIMESTRE",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addTabelaDespesaDePessoalPrimeiroQuadrimestre();

        addBreak();
        addParagrafo(new TextoFormatado("2º QUADRIMESTRE",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addTabelaDespesaDePessoalSegundoQuadrimestre();

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(" NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO QUANTO AO AJUSTE DO MÊS DE DEZEMBRO DO EXERCÍCIO ANTERIOR. " +
                "SEMPRE BUSCAR SEGUIR O APURADO PELA FISCALIZAÇÃO ANTERIOR. ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("IMPORTANTE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": VERIFICAR A APLICABILIDADE DO ART. 66 DA LRF, DE DUPLICAÇÃO DO PRAZO DE " +
                                "RECONDUÇÃO EM CASOS DE BAIXO CRESCIMENTO DO PIB.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": ACIMA DE 95% DE 54% (51,30%), QUANDO SE INICIAM AS VEDAÇÕES DA LRF",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Diante dos elementos apurados acima, verificamos que a despesa total com " +
                        "pessoal não superou o limite previsto no art. 20, III, da Lei de Responsabilidade Fiscal, " +
                        "porém ultrapassou aquele previsto no art. 22, parágrafo único, da Lei supracitada, nos ",
                formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" quadrimestres.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Constatamos a infringência do inciso ",
                formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", do citado dispositivo, tendo em vista que ", formatacaoFactory.getJustificado(12))
                .concat("relatar as ocorrências.", formatacaoFactory.getJustificadoVermelhoCinza(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal, o " +
                        "Executivo Municipal foi alertado tempestivamente, por  ",
                        formatacaoFactory.getJustificado(12))
                        .concat(quantidadeAlertasLRFa59p1i2.toString(), formatacaoFactory.getJustificado(12))
                        .concat(" vezes, quanto à superação de 90% do específico limite da despesa laboral.",
                                formatacaoFactory.getJustificado(12))
                );

        addParagrafo(new TextoFormatado("OBSERVAR A DATA DE EMISSÃO DO ALERTA (CONSTANTE NO FINAL DO " +
                "DOCUMENTO “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO " +
                "FINAL DO EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": ACIMA DE 54% NO QUADRIMESTRE",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();



        addParagrafo(addTab().concat("É possível ver que a superação do limite da despesa laboral aconteceu no " +
                        "último quadrimestre do exercício, significando ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da Receita Corrente Líquida.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal, " +
                        "o Executivo Municipal" +
                        " foi alertado tempestivamente, por ",
                formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(quantidadeAlertasLRFa59p1i2 + " vezes, quanto à superação de 90% do específico limite" +
                                " da despesa laboral.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("OBSERVAR A DATA DE EMISSÃO DO ALERTA (CONSTANTE NO FINAL DO DOCUMENTO" +
                " “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO FINAL DO " +
                "EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineJustificadoVermelhoFundoVerde(12))
                .concat(": OBRIGATÓRIA A INFORMAÇÃO EM CASO EMISSÃO DE ALERTAS",
                        formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12)));
        addParagrafo(addTab().concat("Ademais, cabe consignar que o município ",
                formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("não", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12))
                .concat(" decretou estado de calamidade pública/emergência, devidamente reconhecido pela " +
                                "Assembleia Legislativa Estadual, assim, ",
                        formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("não", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12))
                .concat(" sendo aplicável a suspensão de contagem de prazo para recondução aos limites, conforme" +
                                " art. 65 da Lei de Responsabilidade Fiscal. ",
                        formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("(adaptar conforme o caso)", formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12))

        );

        addBreak();

        addSecao(new TextoFormatado("B.1.2.2. CONTRATAÇÕES DE PESSOAL POR TEMPO DETERMINADO",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();
        addParagrafo(addTab().concat("o planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                        "que ensejasse o exame do item neste quadrimestre.",
                        formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("SOMENTE NAS HIPÓTESES DE IRREGULARIDADE OU REGULARIDADE QUE DEMANDE " +
                "PROPOSTA DE RECOMENDAÇÃO", formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(addTab().concat("No presente quadrimestre a fiscalização analisou por amostragem as admissões " +
                        "de pessoal por tempo determinado efetuadas no exercício quanto aos aspectos legais, formais e " +
                        "princípios gerais da administração pública, detectando as seguintes falhas:",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(new TextoFormatado("RELACIONAR AS FALHAS COM PROPOSTA AO E RELATOR PARA EMISSÃO DE" +
                " RECOMENDAÇÕES À ORIGEM.", formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("QUESTÕES QUE A SEREM CONSIDERADAS:",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("HÁ CASOS EM QUE SE UTILIZA UMA LISTA DE CLASSIFICAÇÃO DE CONCURSO PÚBLICO" +
                " VIGENTE PARA CONTRATAÇÕES POR TEMPO DETERMINADO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("AS CONTRATAÇÕES POR TEMPO DETERMINADO FORAM PRECEDIDAS DE PROCESSO DE SELEÇÃO?\n" +
                "EM CASO POSITIVO, ANALISAR AS JUSTIFICATIVAS PARA TAIS CONTRATAÇÕES, CONSIDERANDO SE HÁ CARGOS CRIADOS " +
                "NO QUADRO DE PESSOAL QUE CUMPREM AS FUNÇÕES CONTRATADAS? SE HÁ REALMENTE NECESSIDADE PERMANENTE DE TAIS" +
                " FUNÇÕES NA PREFEITURA? SE SIM, PODERIA TER HAVIDO PLANEJAMENTO PARA TAIS CONTRATAÇÕES FOSSEM FEITAS " +
                "POR CONCURSO PÚBLICO PARA OCUPAÇÃO DE CARGO PÚBLICO?",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("CONTRATAÇÕES POR URGÊNCIA, NÃO PRECEDIDAS DE PROCESSOS DE SELEÇÃO, MAS, " +
                "DEVIDAMENTE JUSTIFICADAS.\n" +
                "JUSTIFICATIVAS – LICENÇAS DE QUALQUER NATUREZA DE OCUPANTES DE CARGOS EFETIVOS, SALAS LIVRES NO " +
                "SETOR DE EDUCAÇÃO – CONSIDERAR AQUI MUNICÍPIOS QUE TEM POPULAÇÃO FLUTUANTE POR GRANDES OBRAS DE " +
                "ENGENHARIA OU USINAS SUCROALCOOLEIRAS, EMERGÊNCIAS NA ÁREA DA SAÚDE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();


        addSecao(new TextoFormatado("B.1.3. PRECATÓRIOS",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAR A NOTA TÉCNICA SDG Nº 142, INCLUSIVE PARA NOTICIAR" +
                " EVENTUAL OCORRÊNCIA NO QUADRIMESTRE EM ANÁLISE, DE:\n" +
                "- ADESÃO IRREGULAR À NOVA SISTEMÁTICA TRAZIDA PELO EC Nº 99/2017, CONSIDERANDO QUE NÃO " +
                "PODEM ADERIR OS ÓRGÃOS QUE ESTAVAM EM DIA COM O PAGAMENTO (OU SEJA, JÁ ENQUADRÁVEIS " +
                "NO REGIME ORDINÁRIO);\n" +
                "- EVENTUAIS INFORMAÇÕES DE IRREGULARIDADES EMITIDAS PELO TJ, INCLUSIVE QUANTO AO" +
                " PERCENTUAL INSUFICIENTE AO PAGAMENTO;\n" +
                "- ACORDOS (EX. PARCELAMENTO) HOMOLOGADOS PELO TJ;\n" +
                "- EVENTUAIS IRREGULARIDADES QUANTO À GESTÃO DAS FONTES ADICIONAIS PARA PAGAMENTOS " +
                "(CRIAÇÃO DE FUNDOS GARANTIDORES ETC.);\n" +
                "- DESAPROPRIAÇÃO NOS CASOS VEDADOS;\n" +
                "- AUSÊNCIA DE REGULAMENTAÇÃO DA LEI NO PRAZO DE 120 DIAS DE 1/1/2018, OU SEJA, 1/5/2018.\n",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));


        addParagrafo(new TextoFormatado("NÃO HAVENDO OCORRÊNCIAS DIGNAS DE NOTA, EXCLUIR O ITEM NOS QUADRIMESTRES, " +
                "DEIXANDO SUA ANÁLISE SOMENTE PARA O FECHAMENTO DO EXERCÍCIO.",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));

        addBreak();
        addBreak();

        addBreak();

//        addSecao(new TextoFormatado("B.1.4. RESTRIÇÕES DE ÚLTIMO ANO DE MANDATO",
//                formatacaoFactory.getBold(12)), heading3);
//        addBreak();
//        addSecao(new TextoFormatado("B.1.4.1. LEI DE RESPONSABILIDADE FISCAL",
//                formatacaoFactory.getBold(12)), heading4);
//        addBreak();
//        addSecao(new TextoFormatado("B.1.4.1.1. DOIS ÚLTIMOS QUADRIMESTRES – COBERTURA MONETÁRIA PARA DESPESAS " +
//                "EMPENHADAS E LIQUIDADAS",
//                formatacaoFactory.getBold(12)), heading5);
//        addBreak();
//
//        addParagrafo(addTab().concat("O quadro a seguir, consoante apurado pelo Sistema Audesp, demonstra a " +
//                        "projeção de atendimento do artigo 42 da Lei de Responsabilidade Fiscal:",
//                        formatacaoFactory.getJustificado(12)));
//
//        addBreak();
//        addTabelaApuracaoDisponibilidadesDeCaixa();
//
//
//        addParagrafo(new TextoFormatado("Apuração a partir de informações fornecidas pela Origem ao Sistema AUDESP",
//            formatacaoFactory.getBold(12)));
//        addBreak();
//        addParagrafo(addTab().concat("Considerando o disposto pelo art. 65, §1º, II da Lei de Responsabilidade " +
//                        "Fiscal, ao final do exercício será verificada eventual dispensa de observância da vedação do " +
//                        "art. 42 do mesmo diploma.",
//                        formatacaoFactory.getJustificado(12)));
//
//        addBreak();
//        addBreak();
//
//
//        addSecao(new TextoFormatado("B.1.4.1.2. OPERAÇÃO DE CRÉDITO POR ANTECIPAÇÃO DA RECEITA ORÇAMENTÁRIA – ARO",
//                formatacaoFactory.getBold(12)), heading5);
//        addBreak();
//
//        addParagrafo(new TextoFormatado("1ª HIPÓTESE: NÃO HAVENDO ARO",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("No quadrimestre em análise o Município não realizou operação de crédito " +
//                        "por antecipação da receita orçamentária - ARO.",
//                formatacaoFactory.getJustificado(12)));
//        addBreak();
//
//        addParagrafo(new TextoFormatado("2ª HIPÓTESE: HAVENDO ARO",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("No quadrimestre em análise o Município realizou esse empréstimo " +
//                        "extraorçamentário, não atendendo ao art. 38, IV, “b” da Lei de Responsabilidade Fiscal.",
//                formatacaoFactory.getJustificado(12)));
//        addBreak();
//
//        addParagrafo(new TextoFormatado("Link para consulta do calendário eleitoral: ",
//            formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
//        .concat("http://www.tse.jus.br/eleicoes/calendario-eleitoral/calendario-eleitoral",
//                formatacaoFactory.getJustificadoAzul(12))
//        );
//
//        addBreak();
//
//        addSecao(new TextoFormatado("B.1.4.2. LEI ELEITORAL (LEI FEDERAL Nº 9.504, DE 30 DE SETEMBRO DE 1997)",
//                formatacaoFactory.getBold(12)), heading4);
//        addBreak();
//        addParagrafo(new TextoFormatado("TENDO EM VISTA A EDIÇÃO DA TENDO EM VISTA A EDIÇÃO DA ",
//            formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
//        .concat("EMENDA CONSTITUCIONAL Nº 107, DE 2 DE JULHO DE 2020",
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
//        .concat("QUE ALTEROU A DATA DAS ELEIÇÕES EM 2020, O ITEM ADIANTE SÓ SERÁ TRATADO NO SEGUNDO " +
//                "QUADRIMESTRE DE 2020", formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//
//        addSecao(new TextoFormatado("B.1.4.2.1. ALTERAÇÕES SALARIAIS",
//                formatacaoFactory.getBold(12)), heading5);
//        addBreak();
//
//        addParagrafo(new TextoFormatado("1ª HIPÓTESE: NÃO HAVENDO ALTERAÇÕES ",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("A partir de 19 de maio, as alterações remuneratórias se limitaram à inflação" +
//                        " do período cumprindo-se o art. 73, VIII da Lei Eleitoral.",
//                formatacaoFactory.getJustificado(12)));
//        addBreak();
//
//        addParagrafo(new TextoFormatado("2ª HIPÓTESE: HAVENDO ALTERAÇÕES",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("A partir de 19 de maio, as alterações remuneratórias não se limitaram " +
//                        "à inflação do período descumprindo-se o art. 73, VIII da Lei Eleitoral.",
//                formatacaoFactory.getJustificado(12)));
//        addBreak();
//
//        addParagrafo(new TextoFormatado("ORIENTAÇÃO",
//                formatacaoFactory.getBoldUnderlineJustificadoVermelhoFundoVerde(12))
//                .concat(": observar o art. 8º da Lei Complementar nº 173/2020, no que toca à vedação da concessão" +
//                                " de alterações salariais, considerando a data da publicação da lei (28/05/2020).",
//                        formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12)));
//        addBreak();
//
//        addParagrafo(new TextoFormatado("(VERIFICAÇÕES, SEGUINTES, A PARTIR DO 2º QUADRIMESTRE DE 2020)",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//
//        addBreak();
//        addSecao(new TextoFormatado("B.1.4.2.2. DESPESAS COM PUBLICIDADE E PROPAGANDA OFICIAL",
//                formatacaoFactory.getBold(12)), heading5);
//        addBreak();
//
//        addParagrafo(new TextoFormatado("1ª HIPÓTESE: NÃO HAVENDO EMPENHAMENTO",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("A partir de 15 de agosto, o Município não empenhou gastos de publicidade " +
//                        "vedados pelo art. 73, VI, “b” da Lei Eleitoral.",
//                formatacaoFactory.getJustificado(12)));
//        addBreak();
//        addParagrafo(new TextoFormatado("2ª HIPÓTESE: HAVENDO EMPENHAMENTO",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("A partir de 15 de agosto, o Município empenhou gastos de publicidade vedados " +
//                        "pelo art. 73, VI, “b” da Lei Eleitoral, conforme segue:",
//                formatacaoFactory.getJustificado(12)));
//        addBreak();
//        addParagrafo(new TextoFormatado("1ª HIPÓTESE: NÃO HAVENDO GASTOS",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("Ainda, até 15 de agosto de 2020 não houve liquidação de gastos de publicidade" +
//                        " institucional, observando o inciso VII, do § 3º, do art. 1º, a Emenda Constitucional nº 107," +
//                        " de 2 de julho de 2020..",
//                formatacaoFactory.getJustificado(12)));
//        addBreak();
//        addParagrafo(new TextoFormatado("2ª HIPÓTESE: HAVENDO GASTOS",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("Ainda, até 15 de Agosto de 2020 os gastos liquidados de publicidade institucional  ",
//                formatacaoFactory.getJustificado(12))
//                        .concat("não", formatacaoFactory.getJustificadoVermelhoCinza(12))
//                .concat(" superaram a média dos dois primeiros quadrimestres dos três últimos exercícios financeiros (2017 a 2019), ",
//                        formatacaoFactory.getJustificado(12))
//                .concat("não", formatacaoFactory.getJustificadoVermelhoCinza(12))
//                .concat(" observando o inciso VII, do § 3º, do art. 1º, a Emenda Constitucional nº 107, de 2 de " +
//                                "julho de 2020, conforme demonstrado:",
//                        formatacaoFactory.getJustificado(12)));
//        addBreak();
//
//        addTabelaPublicidadeEmAnoEleitoral();
//
//        addBreak();
//
//        addParagrafo(new TextoFormatado("(VERIFICAÇÃO NO 1º E 2º QUADRIMESTRES DE 2020)",
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));
//
//        addParagrafo(new TextoFormatado("ALTERAR A NUMERAÇÃO DESTE SUBITEM TENDO EM VISTA QUE OS DEMAIS (ACIMA) " +
//                "SÓ SERÃO TRATADOS A PARTIR DO SEGUNDO QUADRIMESTRE DE 2020.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//
//
//        addBreak();
//        addBreak();
//
//        addSecao(new TextoFormatado("B.1.4.2.3. DISTRIBUIÇÃO GRATUITA DE BENS, VALORES E BENEFÍCIOS",
//                formatacaoFactory.getBold(12)), heading5);
//        addBreak();
//
//        addParagrafo(new TextoFormatado("1ª HIPÓTESE: NÃO HOUVE A CRIAÇÃO DE NOVOS PROGRAMAS ",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("No quadrimestre em análise, a Prefeitura não criou novos programas de " +
//                        "distribuição gratuita de bens, valores ou benefícios fiscais.",
//                formatacaoFactory.getJustificado(12)));
//
//        addBreak();
//        addParagrafo(new TextoFormatado("2ª HIPÓTESE: HOUVE A CRIAÇÃO DE NOVOS PROGRAMAS ",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("No quadrimestre em análise, a Prefeitura criou novos programas de " +
//                        "distribuição gratuita de bens, valores ou benefícios fiscais.",
//                formatacaoFactory.getJustificado(12)));
//        addParagrafo(new TextoFormatado("Orientação",
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
//                .concat(": detalhar a irregularidade",
//                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(new TextoFormatado("ORIENTAÇÃO",
//                formatacaoFactory.getBoldUnderlineJustificadoVermelhoFundoVerde(12))
//                .concat(": dobservar se os programas criados decorrem da pandemia (covid-19), relatando as ocorrências.",
//                        formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12)));
//        addBreak();
        addBreak();




        addSecao(new TextoFormatado("B.2. IEG-M – I-FISCAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("SÓ INSERIR ESTE SUBITEM CASO TENHA SIDO EFETUADA A VALIDAÇÃO DO " +
                "IEG-M PELA FISCALIZAÇÃO OU CASO HAJA SITUAÇÕES RELACIONADAS A ELE QUE INDIQUEM RISCOS E " +
                "JUSTIFIQUEM SUA INSERÇÃO. PARA ISSO, UTILIZAR-SE DO RELATÓRIO SMART GERADO PELA DIVISÃO" +
                " AUDESP-INDICADORES.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR O SEGUINTE" +
                                " TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                        formatacaoFactory.getJustificado(12)));
        addBreak();
//        addParagrafo(new TextoFormatado("VER ORIENTAÇÕES DO APÊNDICE II, AO FINAL DO MODELO DE FECHAMENTO.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//
//        addParagrafo(new TextoFormatado("HIPÓTESE: ",
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
//                .concat("SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR O SEGUINTE" +
//                                " TEXTO:", formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa " +
//                "dimensão do IEG-M.", formatacaoFactory.getJustificado(12)));
//        addBreak();
//
//        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                        "ensejasse o exame do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto " +
                "de apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.3. OUTROS PONTOS DE INTERESSE ",
                formatacaoFactory.getBold(12)).concat("(MANTER APENAS EM CASO DE OCORRÊNCIA, SENÃO " +
                "EXCLUIR ESTE ITEM)", formatacaoFactory.getBoldJustificadoFundoAmarelo(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("OUTRAS ANÁLISES, PREVISTAS NO ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("MODELO DE FECHAMENTO, APENAS",
                        formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(" COMPORÃO O RELATÓRIO EM CASO DE ",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("IRREGULARIDADES CONSTATADAS POR MEIO DO IEG-M, DENÚNCIAS FORMALIZADAS PERANTE ESTE" +
                                " TCESP, HISTÓRICO DO ÓRGÃO DE DESVIOS OU MALVERSAÇÃO DE RECURSOS",
                        formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(", ACHADOS CUJA RELEVÂNCIA/MATERIALIDADE JUSTIFIQUEM A ATUAÇÃO DO TCESP ETC., CONSIDERANDO" +
                                " QUE O INTUITO DA FISCALIZAÇÃO É EFETUAR ANÁLISES FINALÍSTICAS.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
        );

        addParagrafo(new TextoFormatado("PARA TANTO, VERIFICAR MODELOS/SUGESTÕES NO APÊNDICE I DO ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
                .concat("MODELO DE FECHAMENTO.",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("ORIENTAÇÃO",
                formatacaoFactory.getBoldUnderlineJustificadoVermelhoFundoVerde(12))
                .concat("APLICAÇÃO DA LEI COMPLEMENTAR 173/2020.  VERIFICAR A RELEVÂNCIA/MATERIALIDADE DOS REFLEXOS" +
                                " DA REFERIDA LEI, NO QUE CONCERNE À SUSPENSÃO DE PAGAMENTOS DE ENCARGOS/PARCELAMENTOS " +
                                "(ART. 9º), CONSOANTE COMUNICADO SDG Nº 25/2020. NARRAR NO QUADRIMESTRE, CONFORME A " +
                                "NECESSIDADE.",
                        formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12)));
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA C: ENSINO", formatacaoFactory.getBold(12)), heading1);

        addBreak();

        addSecao(new TextoFormatado("C.1. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL NO ENSINO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(addTab().concat("A aplicação de recursos, no período, conforme informado ao Sistema Audesp, " +
                        "apresentou os seguintes resultados:",
                        formatacaoFactory.getJustificado(12)));

        addTabelaDespesasCfFundeb();
        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp", formatacaoFactory.getBoldJustificado(10))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(10)));

        addBreak();

//        addParagrafo(new TextoFormatado("TENDO EM VISTA A NT SDG Nº 154 DE 14 DE JANEIRO DE 2020 QUE TRATA DOS " +
//                "REPASSES EFETUADOS PELO ESTADO PARA OS MUNICÍPIOS NO FINAL DO MÊS DE DEZEMBRO DE 2019, DEVE A " +
//                "FISCALIZAÇÃO VERIFICAR A FORMA DE CONTABILIZAÇÃO E OS IMPACTOS ESPECIALMENTE NA DESPESA DE " +
//                "PESSOAL, ENSINO E SAÚDE.",
//            formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": EM CASO DE SITUAÇÃO DESFAVORÁVEL, DOCUMENTAR NOS AUTOS E MENCIONAR A EMISSÃO " +
                                "DE ALERTAS, CONFORME SEGUE.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("CASO SEJAM REALIZADOS AJUSTES NOS ÍNDICES, RELATAR.",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));

        addBreak();
        addBreak();

        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, ",
                        formatacaoFactory.getJustificado(12))
        .concat("foi o Município alertado", formatacaoFactory.getBold(12))
        .concat(", por ", formatacaoFactory.getJustificado(12))
        .concat( "" + quantidadeAlertasLRFa59p1i5AplicacaoEnsino , formatacaoFactory.getJustificadoVermelhoCinza(12))
        .concat(" vezes, consoante Notificações de Alertas juntados no presente evento.",
                formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("ANÁLISE ELETIVA NO QUADRIMESTRE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": OFERTA DE VAGAS NO ENSINO SEM DÉFICIT",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Com base nos dados coletados junto à origem durante a inspeção, não " +
                        "constatamos demanda não atendida nos níveis de ensino ofertados pelo Município.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": OFERTA DE VAGAS NO ENSINO COM DÉFICIT",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Com base nos dados coletados junto à origem durante a inspeção, constatamos " +
                        "demanda não atendida no seguinte nível:",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addTabelaVagasEscolares();

        addBreak();

        addParagrafo(new TextoFormatado("EXCLUIR LINHAS DOS NÍVEIS REGULARES, MANTENDO APENAS OS IRREGULARES.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("NO CASO DE DÉFICIT ENTRE DEMANDA E OFERTA DE VAGAS, DEVERÁ A FISCALIZAÇÃO VERIFICAR:",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("- A RESPOSTA DADA À SEGUINTE QUESTÃO QUE CONSTA DO QUESTIONÁRIO DO " +
                "IEG-M – PERSPECTIVA I-EDUC : A PREFEITURA MUNICIPAL FEZ UMA PESQUISA/ESTUDO PARA LEVANTAR O NÚMERO DE " +
                "CRIANÇAS QUE NECESSITAVAM DE CRECHES, PRÉ-ESCOLA OU ENSINO FUNDAMENTAL?",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("- QUAIS AS MEDIDAS TEM SIDO ADOTADAS PELA PREFEITURA PARA ZERAR O DÉFICIT APURADO;",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("- SE HÁ PROJETOS NAS PEÇAS DE PLANEJAMENTO QUE COMTEMPLEM OBRAS DE" +
                " CONSTRUÇÃO OU AMPLIAÇÃO DE CRECHES OU ESCOLAS;",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("- SE HÁ OBRAS PARA CONSTRUÇÃO DE CRECHES OU ESCOLAS QUE ESTEJAM ATRASADAS" +
                " OU PARALISADAS, TRAZENDO NOTÍCIAS SOBRE QUAIS AS CAUSAS DE TAL SITUAÇÃO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));


        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("ORIENTAÇÃO",
                formatacaoFactory.getBoldUnderlineJustificadoVermelhoFundoVerde(12))
                .concat("VERIFICAR E INFORMAR CONFORME QUESTIONÁRIO REFERENTE À GESTÃO DO COVID-19. ACOMPANHAR AS" +
                                " MEDIDAS DE MITIGAÇÃO CONFORME A MATERIALIDADE/RELEVÂNCIA.",
                        formatacaoFactory.getBoldJustificadoVermelhoFundoVerde(12)));
        addBreak();

        addParagrafo(addTab().concat("A Secretaria Municipal de Educação (", formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("não", formatacaoFactory.getJustificadoVermelhoFundoVerde(12))
                .concat(") alterou a rotina escolar, com suspensão parcial ou total das aulas presenciais " +
                                "para os alunos da rede municipal de ensino. Contudo, (", formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("não", formatacaoFactory.getJustificadoVermelhoFundoVerde(12))
                .concat(") vem tomando medidas educacionais de emergência voltadas a mitigar " +
                        "os possíveis impactos sobre a aprendizagem, o que pode prejudicar o ciclo escolar" +
                        " dos alunos.\n" , formatacaoFactory.getJustificadoFundoVerde(12)));

        addParagrafo(addTab().concat("Das medidas informadas, destacamos: (", formatacaoFactory.getJustificadoFundoVerde(12))
                .concat("descrever", formatacaoFactory.getJustificadoVermelhoFundoVerde(12))
                .concat(") .", formatacaoFactory.getJustificadoFundoVerde(12)));




        addBreak();
        addBreak();

        addSecao(new TextoFormatado("C.2. IEG-M – I-EDUC",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("SÓ INSERIR ESTE SUBITEM CASO TENHA SIDO EFETUADA A VALIDAÇÃO DO IEG-M " +
                "PELA FISCALIZAÇÃO OU CASO HAJA SITUAÇÕES RELACIONADAS A ELE QUE INDIQUEM RISCOS E JUSTIFIQUEM " +
                "SUA INSERÇÃO. PARA ISSO, UTILIZAR-SE DO RELATÓRIO SMART GERADO PELA DIVISÃO AUDESP-INDICADORES.",
            formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("VER ORIENTAÇÕES DO APÊNDICE II, AO FINAL DO MODELO DE FECHAMENTO",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                        "que ensejasse o exame do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi " +
                        "objeto de apontamento no quadrimestre anterior.",
                        formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA D: SAÚDE", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("D.1. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL NA SAÚDE",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();



        addParagrafo(addTab().concat("Conforme informado ao Sistema Audesp, a aplicação na Saúde atingiu, no " +
                        "período, os seguintes resultados:",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addTabelaEmpenhadaLiquidadaPaga();

        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp", formatacaoFactory.getBold(10))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(10)));
        addBreak();

//        addParagrafo(new TextoFormatado("TENDO EM VISTA A NT SDG Nº 154 DE 14 DE JANEIRO DE 2020 QUE TRATA DOS " +
//                "REPASSES EFETUADOS PELO ESTADO PARA OS MUNICÍPIOS NO FINAL DO MÊS DE DEZEMBRO DE 2019, DEVE A " +
//                "FISCALIZAÇÃO VERIFICAR A FORMA DE CONTABILIZAÇÃO E OS IMPACTOS ESPECIALMENTE NA DESPESA DE " +
//                "PESSOAL, ENSINO E SAÚDE.",
//            formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//
//        addBreak();


        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": EM CASO DE SITUAÇÃO DESFAVORÁVEL, DOCUMENTAR NOS AUTOS E MENCIONAR A EMISSÃO " +
                                "DE ALERTAS, CONFORME SEGUE.",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("CASO SEJAM REALIZADOS AJUSTES NOS ÍNDICES, RELATAR.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();



        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, ",
                formatacaoFactory.getJustificado(12))
                .concat(" foi o Município foi alertado", formatacaoFactory.getBold(12))
                .concat(", por ", formatacaoFactory.getJustificado(12))
                .concat(quantidadeAlertasLRFa59p1i5AplicacaoSaude.toString(), formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" vezes, consoante Notificações de Alertas juntados no presente evento.", formatacaoFactory.getJustificado(12))
        );


        addBreak();
        addBreak();

        addSecao(new TextoFormatado("D.2. IEG-M – I-SAÚDE",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("SÓ INSERIR ESTE SUBITEM CASO TENHA SIDO EFETUADA A VALIDAÇÃO DO IEG-M " +
                "PELA FISCALIZAÇÃO OU CASO HAJA SITUAÇÕES RELACIONADAS A ELE QUE INDIQUEM RISCOS E JUSTIFIQUEM SUA " +
                "INSERÇÃO. PARA ISSO, UTILIZAR-SE DO RELATÓRIO SMART GERADO PELA DIVISÃO AUDESP-INDICADORES.",
            formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("VER ORIENTAÇÕES DO APÊNDICE II, AO FINAL DO MODELO DE FECHAMENTO",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º. ",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto " +
                "de apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();


        addSecao(new TextoFormatado("PERSPECTIVA E: GESTÃO AMBIENTAL",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("E.1. IEG-M – I-AMB",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("SÓ INSERIR ESTE SUBITEM CASO TENHA SIDO EFETUADA A VALIDAÇÃO DO IEG-M " +
                "PELA FISCALIZAÇÃO OU CASO HAJA SITUAÇÕES RELACIONADAS A ELE QUE INDIQUEM RISCOS E JUSTIFIQUEM SUA " +
                "INSERÇÃO. PARA ISSO, UTILIZAR-SE DO RELATÓRIO SMART GERADO PELA DIVISÃO AUDESP-INDICADORES.",
            formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("VER ORIENTAÇÕES DO APÊNDICE II, AO FINAL DO MODELO DE FECHAMENTO",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto " +
                "de apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA F: GESTÃO DA PROTEÇÃO À CIDADE",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("F.1. IEG-M – I-CIDADE",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("SÓ INSERIR ESTE SUBITEM CASO TENHA SIDO EFETUADA A VALIDAÇÃO DO IEG-M " +
                "PELA FISCALIZAÇÃO OU CASO HAJA SITUAÇÕES RELACIONADAS A ELE QUE INDIQUEM RISCOS E JUSTIFIQUEM SUA " +
                "INSERÇÃO. PARA ISSO, UTILIZAR-SE DO RELATÓRIO SMART GERADO PELA DIVISÃO AUDESP-INDICADORES.",
            formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("VER ORIENTAÇÕES DO APÊNDICE II, AO FINAL DO MODELO DE FECHAMENTO",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                " que ensejasse o exame do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto " +
                "de apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();



        addSecao(new TextoFormatado("PERSPECTIVA G: TECNOLOGIA DA INFORMAÇÃO",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("G.1. FIDEDIGNIDADE DOS DADOS INFORMADOS AO SISTEMA AUDESP",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SEM DIVERGÊNCIAS",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Nos trabalhos da fiscalização não foram encontradas divergências entre os " +
                        "dados da origem e os prestados ao Sistema Audesp.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": COM DIVERGÊNCIAS",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Como demonstrado no(s) item(ns) ",
                formatacaoFactory.getJustificado(12))
                .concat("xxx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" deste relatório, foram constatadas divergências entre os dados informados " +
                                "pela origem e aqueles apurados no Sistema Audesp.",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();
        addSecao(new TextoFormatado("G.2. IEG-M – I-GOV TI",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("SÓ INSERIR ESTE SUBITEM CASO TENHA SIDO EFETUADA A VALIDAÇÃO DO " +
                "IEG-M PELA FISCALIZAÇÃO OU CASO HAJA SITUAÇÕES RELACIONADAS A ELE QUE INDIQUEM RISCOS E JUSTIFIQUEM" +
                " SUA INSERÇÃO. PARA ISSO, UTILIZAR-SE DO RELATÓRIO SMART GERADO PELA DIVISÃO AUDESP-INDICADORES.",
            formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("VER ORIENTAÇÕES DO APÊNDICE II, AO FINAL DO MODELO DE FECHAMENTO",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                "que ensejasse o exame do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi " +
                "objeto de apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA H: OUTROS ASPECTOS RELEVANTES",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("H.1. DENÚNCIAS/REPRESENTAÇÕES/EXPEDIENTES",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();



        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": NA INEXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Não chegou ao nosso conhecimento a formalização de denúncias, " +
                        "representações ou expedientes.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": NA EXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES NÃO PASSÍVEIS DE " +
                                "VERIFICAÇÃO NO QUADRIMESTRE EM ANÁLISE",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("As denúncias / representações / expedientes serão tratados no fechamento do " +
                        "exercício em exame, tendo em vista que, no momento, não concluímos a análise da matéria.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": NA EXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES PASSÍVEIS DE VERIFICAÇÃO E" +
                                " FECHAMENTO NO QUADRIMESTRE EM ANÁLISE",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addBreak();

        addParagrafo(addTab().concat("Está referenciado ",
                        formatacaoFactory.getJustificado(12))
        .concat("OU", formatacaoFactory.getBoldVermelhoAmareloJustificado(12))
        .concat(" Estão referenciados ao presente processo de contas anuais, o(s) seguinte(s) protocolado(s):",
                formatacaoFactory.getJustificado(12)) );


        addBreak();

        addTabelaProcessoDeContasAnuais();

        addBreak();

        addParagrafo(addTab().concat("O(s) assunto(s) em tela foi(ram) tratado(s) no item(ns) ",
                        formatacaoFactory.getJustificado(12))
                        .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(" deste relatório.", formatacaoFactory.getJustificado(12))
                );

        addBreak();

        addSecao(new TextoFormatado("H.2. ATENDIMENTO À LEI ORGÂNICA, INSTRUÇÕES E RECOMENDAÇÕES DO TRIBUNAL DE " +
                "CONTAS DO ESTADO DE SÃO PAULO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": ATENDIMENTO, ADAPTAR CONFORME O CASO",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Não constatamos, no período, desatendimento à Lei Orgânica, Instruções, " +
                        "e/ou recomendações deste Tribunal.",
                        formatacaoFactory.getJustificado(12)));
        addParagrafo(addTab().concat("As recomendações/determinações emitidas em pareceres de contas anuais serão" +
                        " verificadas no relatório de fechamento do exercício.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": CASOS NÃO ATENDIDOS, ADAPTAR CONFORME O CASO",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Constatamos, no período, desatendimento à Lei Orgânica e às Instruções " +
                        "deste Tribunal, tendo em vista que:",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("EXPLICITAR E DOCUMENTAR AS FALHAS.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addParagrafo(new TextoFormatado("AQUI PODEM SER TRAZIDAS, POR EXEMPLO, FALHAS RELATIVAS A ATRASOS OU " +
                "OMISSÃO DE REMESSAS AO SISTEMA AUDESP (FASES I A IV) QUE PODEM LEVAR A AUTUAÇÃO DE PROCESSOS DE" +
                " CONTROLE DE PRAZOS.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
                .concat(": CASOS EM QUE O TEMA NÃO TENHA SIDO ELEITO NO PLANEJAMENTO, ADAPTAR CONFORME O CASO",
                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                        "que ensejasse o exame do item neste quadrimestre.",
                        formatacaoFactory.getJustificado(12)));
        addBreak();



//
//        addParagrafo(new TextoFormatado("OBSERVAÇÃO: Pode-se, desde as análises quadrimestrais, " +
//                "apontar desatendimento às recomendações.", formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//
//        addParagrafo(addTab().concat("Haja vista os dois últimos exercícios apreciados, verificamos que, no " +
//                        "período ora em análise, a Prefeitura descumpriu as seguintes recomendações/determinações deste Tribunal:",
//                        formatacaoFactory.getJustificado(12)));
//
//
//        ParecerPrefeitura ultimo = pareceresPrefeiturasList.size() > 0 ? pareceresPrefeiturasList.get(pareceresPrefeiturasList.size()-1) : null;
//        ParecerPrefeitura penultimo = pareceresPrefeiturasList.size() > 1 ? pareceresPrefeiturasList.get(pareceresPrefeiturasList.size()-2) : null;
//
//        addTabelaDoisUltimosExerciciosApreciados(ultimo);
//
//        addBreak();
//
//        addTabelaDoisUltimosExerciciosApreciados(penultimo);
//
//        addBreak();
//
//
//        addBreak();
//
//        addParagrafo(new TextoFormatado("e/ou", formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//
//        addBreak();
//
//        addParagrafo(new TextoFormatado("OBSERVAÇÃO: Pode-se, desde as análises quadrimestrais, apontar até " +
//                "mesmo a perspectiva de desatendimento às recomendações. Isso poderá ocorrer especialmente nos casos " +
//                "de itens que se concluem no final do exercício; mas que, ante o pontualmente constatado, tem-se a" +
//                " perspectiva de seu descumprimento (p. ex., déficits orçamentário e financeiro). Assim, o relatório" +
//                " quadrimestral reforçará o alerta, com a recomendação expedida.",
//                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//
//        addParagrafo(addTab().concat("Haja vista os dois últimos exercícios apreciados, verificamos que, face às " +
//                        "constatações do período ora em análise, a Prefeitura descumprirá as seguintes " +
//                        "recomendações/determinações deste Tribunal:",
//                        formatacaoFactory.getJustificado(12)));
//
//        addTabelaDoisUltimosExerciciosApreciados(ultimo);
//
//        addBreak();
//
//        addTabelaDoisUltimosExerciciosApreciados(penultimo);
//
//        addBreak();
//
//        addParagrafo(new TextoFormatado("HIPÓTESE",
//                formatacaoFactory.getBoldUnderlineVermelhoAmareloJustificado(12))
//                .concat(": CASOS NÃO ELEITO NO PLANEJAMENTO, ADAPTAR CONFORME O CASO",
//                        formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));
//        addBreak();
//
//        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
//                        "que ensejasse o exame ", formatacaoFactory.getJustificado(12))
//                .concat("in loco", formatacaoFactory.get(12))
//                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));
//        addBreak();
//        addBreak();

        addSecao(new TextoFormatado("CONCLUSÃO", formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addParagrafo(addTab().concat("Com relação aos assuntos tratados neste relatório, destacamos:",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("DESCREVER RESUMIDAMENTE, DE MANEIRA CLARA E OBJETIVA, " +
                "A OCORRÊNCIA CARACTERIZADORA DE EVENTUAL IRREGULARIDADE, INCLUSIVE INDICANDO VALOR DE" +
                " POSSÍVEL DANO, SE FOR O CASO.",
                formatacaoFactory.getBoldVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("À consideração de Vossa Senhoria.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat(tabelasProtocolo.getSecaoFiscalizadoraContas().trim() +
                        ", em ... de ............ de 2020.",
                        formatacaoFactory.getJustificado(12)));


        addBreak();

        addParagrafo(new TextoFormatado("Nome\nAgente da Fiscalização",
                        formatacaoFactory.getBoldItalicCenter(12)));


        return sendFile();

    }

    private void addTextoStatusFaseIEGM() {
        String textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        Integer flagStatusIEGM = resultadoIegmAnoBaseAnterior.getFlagStatusFase();
        if( flagStatusIEGM == 1) {
            textoFlagStatusIEGM = "Índices do exercício em exame em planejamento, " +
                    "dados podem sofrer alterações.";
        } else if(flagStatusIEGM == 2) {
            textoFlagStatusIEGM = "Índices do exercício em exame em verificação/validação da Fiscalização, " +
                    "dados podem sofrer alterações.";
        } else { // flagStatus == 3
            textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        }
        addParagrafo(new TextoFormatado(textoFlagStatusIEGM,
                formatacaoFactory.getItalic(12)));
    }

}