package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class IegmRepository {

    @PersistenceContext(unitName = "wsquestionario")
    private EntityManager entityManager;

    public List<ResultadoIegm> getNotasByCodigoIbge(Integer codigoIBGE, Integer exercicio) {
        Query query = entityManager.createNativeQuery("select cast(m.cd_municio_ibge as varchar(100)) as cd_municipio_ibge, " +
                " cast(m.cnpj as varchar(100)) as cnpj, " +
                " cast(m.ds_municipio as varchar(100)) as ds_municipio, " +
                " cast(m.porte as varchar(100)) as porte, " +
                " cast(m.prefeitura as varchar(100)) as prefeitura, " +
                " cast(m.area_fiscalizacao as varchar(100)) as ur_df, " +
                " cast(m.cd_relator as varchar(100)) as cd_relator, " +
                " cast(m.nome_conselheiro as varchar(100)) as nm_conselheiro, " +
                " cast(m.exercicio as varchar(100)) as exercicio, " +
                " cast(IEGM.percentual_original as numeric(5,2))     AS iegm, " +
                "  cast(IEGM.percentual_rebaixamento as numeric(5,2))    AS iegm_final, " +
                "  cast(IEDUC.percentual_original as numeric(5,2))     AS ieduc, " +
                "  IEDUC.nota_questionario_original      AS faixa_ieduc, " +
                " cast(coalesce(IEDUC.alteracao_acrescimo_decrescimo_nota,0) as numeric(5,2)) AS ieduc_acr_dec, " +
                " coalesce(IEDUC.alteracao_acrescimo_decrescimo_pp,0)    AS ieduc_acr_dec_pp, " +
                " cast(IEDUC.justificativa as varchar(1000))     AS ieduc_acr_dec_justif, " +
                " cast(IEDUC.percentual_rebaixamento as numeric(5,2))     AS ieduc_final,  " +
                " IEDUC.nota_questionario_rebaixamento       AS faixa_ieduc_final, " +
                "  cast(ISAUDE.percentual_original as numeric(5,2))    AS isaude, " +
                "  ISAUDE.nota_questionario_original      AS faixa_isaude, " +
                " cast(coalesce(ISAUDE.alteracao_acrescimo_decrescimo_nota,0) as numeric(5,2)) AS isaude_acr_dec, " +
                " coalesce(ISAUDE.alteracao_acrescimo_decrescimo_pp,0)    AS isaude_acr_dec_pp, " +
                " cast(ISAUDE.justificativa as varchar(1000))     AS isaude_acr_dec_justif, " +
                " cast(ISAUDE.percentual_rebaixamento as numeric(5,2))     AS isaude_final,  " +
                " ISAUDE.nota_questionario_rebaixamento       AS faixa_isaude_final, " +
                " cast(IPLANEJAMENTO.percentual_original as numeric(5,2))     AS iplanejamento, " +
                "  IPLANEJAMENTO.nota_questionario_original      AS faixa_iplanejamento, " +
                " cast(coalesce(IPLANEJAMENTO.alteracao_acrescimo_decrescimo_nota,0) as numeric(5,2)) AS iplanejamento_acr_dec, " +
                " coalesce(IPLANEJAMENTO.alteracao_acrescimo_decrescimo_pp,0)    AS iplanejamento_acr_dec_pp, " +
                " cast(IPLANEJAMENTO.justificativa as varchar(1000))     AS iplanejamento_acr_dec_justif, " +
                " cast(IPLANEJAMENTO.percentual_rebaixamento as numeric(5,2))     AS iplanejamento_final,  " +
                " IPLANEJAMENTO.nota_questionario_rebaixamento       AS faixa_iplanejamento_final, " +
                " cast(IFISCAL.percentual_original as numeric(5,2))    AS ifiscal, " +
                "  IFISCAL.nota_questionario_original      AS faixa_ifiscal, " +
                " cast(coalesce(IFISCAL.alteracao_acrescimo_decrescimo_nota,0) as numeric(5,2)) AS ifiscal_acr_dec, " +
                " coalesce(IFISCAL.alteracao_acrescimo_decrescimo_pp,0)    AS ifiscal_acr_dec_pp, " +
                " cast(IFISCAL.justificativa as varchar(1000))     AS ifiscal_acr_dec_justif, " +
                " cast(IFISCAL.percentual_rebaixamento as numeric(5,2))     AS ifiscal_final,  " +
                " IFISCAL.nota_questionario_rebaixamento       AS faixa_ifiscal_final, " +
                " cast(IAMB.percentual_original as numeric(5,2))     AS iamb, " +
                "  IAMB.nota_questionario_original       AS faixa_iamb, " +
                " cast(coalesce(IAMB.alteracao_acrescimo_decrescimo_nota,0) as numeric(5,2)) AS iamb_acr_dec, " +
                " coalesce(IAMB.alteracao_acrescimo_decrescimo_pp,0)    AS iamb_acr_dec_pp, " +
                " cast(IAMB.justificativa as varchar(1000))     AS iamb_acr_dec_justif, " +
                " cast(IAMB.percentual_rebaixamento as numeric(5,2))     AS iamb_final,  " +
                " IAMB.nota_questionario_rebaixamento       AS faixa_iamb_final, " +
                " cast(ICIDADE.percentual_original as numeric(5,2))    AS icidade, " +
                "  ICIDADE.nota_questionario_original      AS faixa_icidade, " +
                " cast(coalesce(ICIDADE.alteracao_acrescimo_decrescimo_nota,0) as numeric(5,2)) AS icidade_acr_dec, " +
                " coalesce(ICIDADE.alteracao_acrescimo_decrescimo_pp,0)    AS icidade_acr_dec_pp, " +
                " cast(ICIDADE.justificativa as varchar(1000))     AS icidade_acr_dec_justif, " +
                " cast(ICIDADE.percentual_rebaixamento as numeric(5,2))     AS icidade_final,  " +
                " ICIDADE.nota_questionario_rebaixamento       AS faixa_icidade_final, " +
                " cast(IGOVTI.percentual_original as numeric(5,2))    AS igov, " +
                "  IGOVTI.nota_questionario_original      AS faixa_igov, " +
                " cast(coalesce(IGOVTI.alteracao_acrescimo_decrescimo_nota,0) as numeric(5,2)) AS igov_acr_dec, " +
                " coalesce(IGOVTI.alteracao_acrescimo_decrescimo_pp,0)    AS igov_acr_dec_pp, " +
                " cast(IGOVTI.justificativa as varchar(1000))     AS igov_acr_dec_justif, " +
                " cast(IGOVTI.percentual_rebaixamento as numeric(5,2))     AS igov_final,  " +
                " IGOVTI.nota_questionario_rebaixamento       AS faixa_igov_final, " +
                " case coalesce(reb.rebaixamento_faixa_c,0) " +
                " when 1 then 'F' else 'T' end        as cumpriuartigo29a, " +
                " case coalesce(reb.rebaixamento_62,0) " +
                " when 1 then 'F' else 'T' end        as cumpriupercentualeducacao, " +
                " case coalesce(reb.rebaixamento_60,0) " +
                " when 1 then 'F' else 'T' end        as cumpriupercentualsaude, " +
                " IEGM.nota_questionario_original       AS faixa_iegm, " +
                " IEGM.nota_questionario_rebaixamento       AS faixa_iegm_FINAL, " +
                " case  " +
                " when   (coalesce(reb.rebaixamento_faixa_c,0) +  " +
                "  coalesce(reb.rebaixamento_62,0) +  " +
                "  coalesce(reb.rebaixamento_60,0)) > 0 then 1 else 0 end    as \"flag_rebaixo\", " +
                " status.status as status_fase " +
                "from ( " +
                " select distinct " +
                "  e.id as entidade_id, " +
                "  M.id as cd_municio_ibge, " +
                "  ia.cnpj, " +
                "  m.nome as ds_municipio, " +
                "  ia.porte, " +
                "  ia.prefeitura, " +
                "  ia.area_fiscalizacao, " +
                "  ia.cd_relator, " +
                "  ia.nome_conselheiro, " +
                "  ( :exercicio ) as exercicio " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  questionario.municipio   m  " +
                " on  nf.municipio_id = m.id " +
                " inner " +
                " join questionario.entidade   e  " +
                " on nf.entidade_id = e.id " +
                " inner " +
                " join iegm.informacoes_adicionais  ia  " +
                " on nf.entidade_id = ia.entidade_id  " +
                " and ia.exercicio_id = nf.exercicio_id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on nf.exercicio_id = ex.id " +
                " where ex.exercicio = ( :exercicio ) " +
                " ) as m " +
                "left  " +
                "join  (SELECT " +
                "  entidade_id " +
                "  , SUM(CAST(coalesce(rebaixamento_60, '0') AS smallint)) AS rebaixamento_60 " +
                "  , SUM(CAST(coalesce(rebaixamento_62, '0') AS smallint)) AS rebaixamento_62 " +
                "  , SUM(CAST(coalesce(rebaixamento_faixa_c, '0') AS smallint)) AS rebaixamento_faixa_c " +
                " FROM " +
                "  iegm.resposta " +
                " WHERE " +
                "  ( " +
                "   rebaixamento_60 = '1' " +
                "   OR rebaixamento_62 = '1' " +
                "   OR rebaixamento_faixa_c = '1' " +
                "  ) " +
                "  and operacao_id in (select  operacao_id  " +
                "        from  iegm.exercicio_operacao eo " +
                "        inner " +
                "        join iegm.exercicio ex " +
                "        on   eo.exercicio_id = ex.id " +
                "        where  ex.exercicio = ( :exercicio ) ) " +
                " GROUP BY " +
                "  entidade_id " +
                " ORDER BY " +
                "  entidade_id " +
                " ) as reb " +
                "on m.entidade_id = reb.entidade_id " +
                "inner " +
                "join  (select e.id as entidade_id, " +
                "  se.status_id as status,  " +
                "  se.data as status_data " +
                " from  questionario.entidade e " +
                " inner  " +
                " join  questionario.objeto_fiscalizacao  objf  " +
                " on  objf.entidade_id = e.id " +
                " inner  " +
                " join  questionario.objeto_fiscalizacao_status se  " +
                " on  se.objeto_fiscalizacao_id = objf.id " +
                " where objf.operacao_id in ( select  " +
                "      exop.operacao_id  " +
                "     from  iegm.exercicio    ex  " +
                "     inner  " +
                "     join  iegm.exercicio_operacao  exop  " +
                "     on  exop.exercicio_id = ex.id  " +
                "     where  ex.exercicio = ( :exercicio )  " +
                "     order by exop.operacao_id limit 1) " +
                " and se.data =  ( " +
                "   select  max(data) " +
                "   from  questionario.objeto_fiscalizacao_status  se1 " +
                "   where  se1.objeto_fiscalizacao_id = se.objeto_fiscalizacao_id " +
                "   ) " +
                " ) as status " +
                "on m.entidade_id = status.entidade_id " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 8  " +
                "      ) as IEGM " +
                "on m.cd_municio_ibge = IEGM.cd_municio_ibge " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 1  " +
                "      ) as IEDUC " +
                "on m.cd_municio_ibge = IEDUC.cd_municio_ibge " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 2  " +
                "      ) as ISAUDE " +
                "on m.cd_municio_ibge = ISAUDE.cd_municio_ibge " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 3  " +
                "      ) as IPLANEJAMENTO " +
                "on m.cd_municio_ibge = IPLANEJAMENTO.cd_municio_ibge " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 4   " +
                "      ) as IFISCAL " +
                "on m.cd_municio_ibge = IFISCAL.cd_municio_ibge " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 5 " +
                "      ) as IAMB " +
                "on m.cd_municio_ibge = IAMB.cd_municio_ibge " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 6  " +
                "      ) as ICIDADE " +
                "on m.cd_municio_ibge = ICIDADE.cd_municio_ibge " +
                "inner  " +
                "join ( " +
                " select  " +
                "  nf.municipio_id as cd_municio_ibge, " +
                "  nf.nota_questionario_original,  " +
                "  nota_questionario_acrescimo_decrescimo, " +
                "  nf.nota_questionario_rebaixamento,  " +
                "  nf.percentual_original_round/100 as percentual_original, " +
                "  round(nf.percentual_acrescimo_decrescimo_round, 0)/100 as percentual_acrescimo_decrescimo_round, " +
                "  round(nf.percentual_rebaixamento_round, 0)/100 as percentual_rebaixamento, " +
                "  justificativa_acrescimo_decrescimo as justificativa, " +
                "  alteracao_acrescimo_decrescimo_pp, " +
                "  (gpi.divisor*alteracao_acrescimo_decrescimo_pp)/100 as alteracao_acrescimo_decrescimo_nota " +
                " from  iegm.nota_final   nf " +
                " inner " +
                " join  iegm.grupo_perguntas_indices  gpi  " +
                " on  nf.grupo_perguntas_id = gpi.id " +
                " inner " +
                " join iegm.exercicio ex " +
                " on  nf.exercicio_id = ex.id " +
                " where  ex.exercicio = ( :exercicio )  " +
                " and  nf.grupo_perguntas_id = 7   " +
                "      ) as IGOVTI " +
                "on m.cd_municio_ibge = IGOVTI.cd_municio_ibge " +
                "where m.cd_municio_ibge = ( :codigoIBGE )", ResultadoIegm.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);

        List<ResultadoIegm> lista = query.getResultList();

        return lista;
    }

}