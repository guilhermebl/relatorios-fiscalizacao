package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.openxmlformats.schemas.wordprocessingml.x2006.main.STHighlightColor.LIGHT_GRAY;

public class FormatacaoFactory {

    private String fontFamily;

    public FormatacaoFactory(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public Formatacao getParagrafoDeAlerta(int fontSize) {
        return getBoldItalicUnderlineCapsVermelhoAmareloJustificado(fontSize);
    }

    public Formatacao getTab() {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setTab(true);
        return formatacao;
    }

    public Formatacao getBoldItalicUnderlineCapsVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        formatacao.setUnderline(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setCapitalized(true);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldItalicUnderlineVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        formatacao.setUnderline(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldUnderlineVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setUnderline(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldItalicJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldItalicVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }



    public Formatacao getItalicFundoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setItalic(true);
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldItalicUnderlineAzulJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        formatacao.setUnderline(true);
        formatacao.setColor("0000ff");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldItalicAzulJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        formatacao.setColor("0000ff");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldItalicCapsVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setCapitalized(true);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldVermelhoAmareloCenter(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.CENTER);
        return formatacao;
    }

    public Formatacao getBoldVermelhoJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setColor("ff0000");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getUnderlineVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setUnderline(true);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getVermelhoAmareloCenter(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.CENTER);
        return formatacao;
    }

    public Formatacao getVermelhoAmarelo(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        return formatacao;
    }

    public Formatacao getVermelhoAmareloJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("yellow");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getFormatacaoBoldUnderlineCaps(int fontSize) {
        Formatacao formatacaoDescricaoEvento = new Formatacao();
        formatacaoDescricaoEvento.setFontFamily(getFontFamily());
        formatacaoDescricaoEvento.setFontSize(fontSize);
        formatacaoDescricaoEvento.setBold(true);
        formatacaoDescricaoEvento.setUnderline(true);
        formatacaoDescricaoEvento.setCapitalized(true);
        return formatacaoDescricaoEvento;
    }

    public Formatacao getBoldUnderline(int fontSize) {
        Formatacao formatacaoDescricaoEvento = new Formatacao();
        formatacaoDescricaoEvento.setFontFamily(getFontFamily());
        formatacaoDescricaoEvento.setFontSize(fontSize);
        formatacaoDescricaoEvento.setBold(true);
        formatacaoDescricaoEvento.setUnderline(true);
        return formatacaoDescricaoEvento;
    }

    public Formatacao getFormatacaoBoldCaps(int fontSize) {
        Formatacao formatacaoDescricaoEvento = new Formatacao();
        formatacaoDescricaoEvento.setFontFamily(getFontFamily());
        formatacaoDescricaoEvento.setFontSize(fontSize);
        formatacaoDescricaoEvento.setBold(true);
        formatacaoDescricaoEvento.setCapitalized(true);
        return formatacaoDescricaoEvento;
    }

    public Formatacao getFormatacaoCaps(int fontSize) {
        Formatacao formatacaoDescricaoEvento = new Formatacao();
        formatacaoDescricaoEvento.setFontFamily(getFontFamily());
        formatacaoDescricaoEvento.setFontSize(fontSize);
        formatacaoDescricaoEvento.setCapitalized(true);
        return formatacaoDescricaoEvento;
    }

    public Formatacao getBold(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        return formatacao;
    }


    public Formatacao getItalic(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setItalic(true);
        return formatacao;
    }



    public Formatacao getBoldCenter(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setAlignment(ParagraphAlignment.CENTER);
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        return formatacao;
    }


    public Formatacao getBoldItalicCenter(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setAlignment(ParagraphAlignment.CENTER);
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setItalic(true);
        return formatacao;
    }

    public Formatacao getFormatacao(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        return formatacao;
    }

    public Formatacao getJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getItalicJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setItalic(true);
        return formatacao;
    }

    public Formatacao getItalicJustificadoAzul(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setItalic(true);
        formatacao.setColor("0000ff");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getJustificadoAzul(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("0000ff");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }

    public Formatacao getBoldJustificado(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }


    public Formatacao getJustificadoVermelho(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("ff0000");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        return formatacao;
    }



    public Formatacao getVermelho(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("ff0000");
        return formatacao;
    }

    public Formatacao getJustificadoVermelhoCinza(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setColor("ff0000");
        formatacao.setHighLightColor("lightGray");
        return formatacao;
    }

    public Formatacao getBoldJustificadoFundoAmarelo(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setBold(true);
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("yellow");
        return formatacao;
    }

    public Formatacao getJustificadoFundoAmarelo(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("yellow");
        return formatacao;
    }

    public Formatacao getJustificadoFundoAzulClaro(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("cyan");
        return formatacao;
    }

    public Formatacao getJustificadoFundoVerde(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("green");
        return formatacao;
    }

    public Formatacao getBoldJustificadoVermelhoFundoVerde(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setColor("ff0000");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("green");
        return formatacao;
    }

    public Formatacao getJustificadoVermelhoFundoVerde(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("ff0000");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("green");
        return formatacao;
    }

    public Formatacao getBoldUnderlineJustificadoVermelhoFundoVerde(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setColor("ff0000");
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("green");
        return formatacao;
    }


    public Formatacao getJustificadoUnderlineFundoVerde(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setUnderline(true);
        formatacao.setAlignment(ParagraphAlignment.BOTH);
        formatacao.setHighLightColor("green");
        return formatacao;
    }

    public Formatacao getFormatacaoSobrescrito(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setSobrescrito(true);
        return formatacao;
    }

    public Formatacao getCenter(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.CENTER);
        return formatacao;
    }

    public Formatacao getRight(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.RIGHT);
        return formatacao;
    }

    public Formatacao getRightVermelho(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setColor("ff0000");
        formatacao.setAlignment(ParagraphAlignment.RIGHT);
        return formatacao;
    }

    public Formatacao getBoldRight(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setBold(true);
        formatacao.setAlignment(ParagraphAlignment.RIGHT);
        return formatacao;
    }

    public Formatacao getCapsCenter(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.CENTER);
        formatacao.setCapitalized(true);
        return formatacao;
    }

    public Formatacao getCapsRight(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setAlignment(ParagraphAlignment.RIGHT);
        formatacao.setCapitalized(true);
        return formatacao;
    }

    public Formatacao getUnderline(int fontSize) {
        Formatacao formatacao = new Formatacao();
        formatacao.setFontFamily(getFontFamily());
        formatacao.setFontSize(fontSize);
        formatacao.setUnderline(true);
        return formatacao;
    }

    public FormatacaoTabela getTabela_10_90_noBorder() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setBorder(false);
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("10%");
        widths.add("90%");
        formatacao.setWidths(widths);
        return formatacao;
    }


    public FormatacaoTabela getFormatacaoTabela_40_15_15_15_15() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("40%");
        widths.add("15%");
        widths.add("15%");
        widths.add("15%");
        widths.add("15%");
        formatacao.setWidths(widths);
        return formatacao;
    }

    public FormatacaoTabela getFormatacaoTabela_16_42_42() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("16%");
        widths.add("42%");
        widths.add("42%");
        formatacao.setCellMarginTop(10);
        formatacao.setCellMarginRight(40);
        formatacao.setCellMarginBottom(10);
        formatacao.setCellMarginLeft(40);
        formatacao.setWidths(widths);
        return formatacao;
    }

    public FormatacaoTabela getFormatacaoTabela_80_5_5_5_5() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("80%");
        widths.add("5%");
        widths.add("5%");
        widths.add("5%");
        widths.add("5%");
        formatacao.setWidths(widths);
        return formatacao;
    }
    public FormatacaoTabela getFormatacaoTabela_67_11_11_11() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("67%");
        widths.add("11%");
        widths.add("11%");
        widths.add("11%");
        formatacao.setCellMarginTop(15);
        formatacao.setCellMarginRight(0);
        formatacao.setCellMarginBottom(0);
        formatacao.setCellMarginLeft(0);
        formatacao.setWidths(widths);
        return formatacao;
    }

    public FormatacaoTabela getFormatacaoTabela_11_67_11_11() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("11%");
        widths.add("67%");
        widths.add("11%");
        widths.add("11%");
        formatacao.setCellMarginTop(15);
        formatacao.setCellMarginRight(0);
        formatacao.setCellMarginBottom(0);
        formatacao.setCellMarginLeft(0);
        formatacao.setWidths(widths);
        return formatacao;
    }

    public FormatacaoTabela getFormatacaoTabela_20_30_30_20() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("20%");
        widths.add("30%");
        widths.add("30%");
        widths.add("20%");
        formatacao.setCellMarginTop(15);
        formatacao.setCellMarginRight(0);
        formatacao.setCellMarginBottom(0);
        formatacao.setCellMarginLeft(0);
        formatacao.setWidths(widths);
        return formatacao;
    }


    public FormatacaoTabela getFormatacaoTabela_tabela_70_20_10() {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths =  new ArrayList<>();
        widths.add("70%");
        widths.add("20%");
        widths.add("10%");
        formatacao.setWidths(widths);
        return formatacao;
    }

    public FormatacaoTabela getFormatacaoTabela(String[] larguraColunas, boolean firstLineHeader) {
        FormatacaoTabela formatacao = new FormatacaoTabela();
        formatacao.setWidthTabela("100%");
        List<String> widths = Arrays.asList(larguraColunas);
        formatacao.setCellMarginTop(15);
        formatacao.setCellMarginRight(0);
        formatacao.setCellMarginBottom(0);
        formatacao.setCellMarginLeft(0);
        formatacao.setWidths(widths);
        formatacao.setFirstLineHeader(firstLineHeader);
        return formatacao;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }
}
