package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacaoDocumento;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.ValorResult;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class AudespPublicacaoRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<AudespPublicacaoDocumento>  getLRF52(Integer codigoIBGE, Integer exercicio) {
        Query query = entityManager.createNativeQuery("SELECT  " +
                " documento.tp_documento_id, " +
                " documento.documento_id,   " +
                " municipio.ds_municipio,  " +
                " tipo_documento.nm_tp_documento,  " +
                " documento.ano_exercicio,   " +
                " documento.mes_referencia,  " +
                " CASE WHEN documento.mes_referencia = 1 THEN 'Janeiro' " +
                "      WHEN documento.mes_referencia = 2 THEN 'Fevereiro' " +
                "      WHEN documento.mes_referencia = 3 THEN 'Março' " +
                "      WHEN documento.mes_referencia = 4 THEN 'Abril' " +
                "      WHEN documento.mes_referencia = 5 THEN 'Maio' " +
                "      WHEN documento.mes_referencia = 6 THEN 'Junho' " +
                "      WHEN documento.mes_referencia = 7 THEN 'Julho' " +
                "      WHEN documento.mes_referencia = 8 THEN 'Agosto' " +
                "      WHEN documento.mes_referencia = 9 THEN 'Setembro' " +
                "      WHEN documento.mes_referencia = 10 THEN 'Outubro' " +
                "      WHEN documento.mes_referencia = 11 THEN 'Novembro' " +
                "      WHEN documento.mes_referencia = 12 THEN 'Dezembro' END AS mes_referencia_descricao,  " +
                "     publicacao.dt_publicacao,  " +
                "     publicacao.veiculo_publicacao,  " +
                "     publicacao.dt_prest_informacao AS entregue_em,  " +
                "     estado_publicacao.estado_publicacao_id, " +
                "     estado_publicacao.ds_estado_publicacao,  " +
                "     CASE WHEN documento.entregue_em_atraso IS true THEN 'Sim' " +
                "     WHEN documento.entregue_em_atraso IS false THEN 'Não' " +
                "     ELSE '' END AS atrasado   " +
                "FROM       documento   " +
                "INNER JOIN documento_publicacao ON documento_publicacao.documento_id = documento.documento_id " +
                "INNER JOIN publicacao ON publicacao.publicacao_id = documento_publicacao.publicacao_id " +
                "INNER JOIN documento__tipo_estado ON (documento.documento_id = documento__tipo_estado.documento_id " +
                "AND tp_estado_id = 5) " +
                "INNER JOIN tipo_documento ON documento.tp_documento_id = tipo_documento.tp_documento_id " +
                "INNER JOIN entidade ON documento.entidade_id = entidade.entidade_id " +
                "INNER JOIN municipio  ON entidade.municipio_id = municipio.municipio_id " +
                "INNER JOIN estado_publicacao ON estado_publicacao.estado_publicacao_id = publicacao.estado_publicacao_id " +
                "WHERE 1=1 " +
                "AND municipio.cd_municipio_ibge = :codigoIBGE AND ano_exercicio = :exercicio " +
                "AND ( (documento.tp_documento_id BETWEEN 70 AND 76)  )   " +
                "ORDER BY documento.ano_exercicio, documento.mes_referencia ", AudespPublicacaoDocumento.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<AudespPublicacaoDocumento> lista = query.getResultList();
        return lista;
    }

    public List<AudespPublicacaoDocumento>  getPublicacoesByTipoDocumento(Integer codigoIBGE, Integer exercicio, Integer[] tipos) {
        List<Integer> tiposList = Arrays.asList(tipos);

        Query query = entityManager.createNativeQuery("SELECT  " +
                " documento.tp_documento_id, " +
                " documento.documento_id,   " +
                " municipio.ds_municipio,  " +
                " tipo_documento.nm_tp_documento,  " +
                " documento.ano_exercicio,   " +
                " documento.mes_referencia,  " +
                " CASE WHEN documento.mes_referencia = 1 THEN 'Janeiro' " +
                "      WHEN documento.mes_referencia = 2 THEN 'Fevereiro' " +
                "      WHEN documento.mes_referencia = 3 THEN 'Março' " +
                "      WHEN documento.mes_referencia = 4 THEN 'Abril' " +
                "      WHEN documento.mes_referencia = 5 THEN 'Maio' " +
                "      WHEN documento.mes_referencia = 6 THEN 'Junho' " +
                "      WHEN documento.mes_referencia = 7 THEN 'Julho' " +
                "      WHEN documento.mes_referencia = 8 THEN 'Agosto' " +
                "      WHEN documento.mes_referencia = 9 THEN 'Setembro' " +
                "      WHEN documento.mes_referencia = 10 THEN 'Outubro' " +
                "      WHEN documento.mes_referencia = 11 THEN 'Novembro' " +
                "      WHEN documento.mes_referencia = 12 THEN 'Dezembro' END AS mes_referencia_descricao,  " +
                "     publicacao.dt_publicacao,  " +
                "     publicacao.veiculo_publicacao,  " +
                "     publicacao.dt_prest_informacao AS entregue_em, " +
                "     estado_publicacao.estado_publicacao_id, " +
                "     estado_publicacao.ds_estado_publicacao,  " +
                "     CASE WHEN documento.entregue_em_atraso IS true THEN 'Sim' " +
                "     WHEN documento.entregue_em_atraso IS false THEN 'Não' " +
                "     ELSE '' END AS atrasado   " +
                "FROM       documento   " +
                "INNER JOIN documento_publicacao ON documento_publicacao.documento_id = documento.documento_id " +
                "INNER JOIN publicacao ON publicacao.publicacao_id = documento_publicacao.publicacao_id " +
                "INNER JOIN documento__tipo_estado ON (documento.documento_id = documento__tipo_estado.documento_id " +
                "AND tp_estado_id = 5) " +
                "INNER JOIN tipo_documento ON documento.tp_documento_id = tipo_documento.tp_documento_id " +
                "INNER JOIN entidade ON documento.entidade_id = entidade.entidade_id " +
                "INNER JOIN municipio ON entidade.municipio_id = municipio.municipio_id " +
                "INNER JOIN estado_publicacao ON estado_publicacao.estado_publicacao_id = publicacao.estado_publicacao_id " +
                "WHERE 1=1 " +
                "AND municipio.cd_municipio_ibge = :codigoIBGE AND ano_exercicio = :exercicio " +
                "AND ( documento.tp_documento_id in ( :tipos )   )   " +
                "ORDER BY documento.ano_exercicio, documento.mes_referencia ", AudespPublicacaoDocumento.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("tipos", tiposList);
        List<AudespPublicacaoDocumento> lista = query.getResultList();
        return lista;
    }

}
