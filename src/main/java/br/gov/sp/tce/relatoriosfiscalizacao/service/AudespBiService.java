package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespResultadoExecucaoOrcamentariaRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model.AudespBiValor;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model.TcespBiInvestimentoMunicipio;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.repository.AudespBiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespBiService {

    @Autowired
    private TcespBiService tcespBiService;

    @Autowired
    private AudespBiRepository audespBiRepository;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaRepository audespResultadoExecucaoOrcamentariaRepository;


    public Integer getSkMunicipio(Integer codigoIBGE) {
        return audespBiRepository.getSkMunicipio(codigoIBGE);
    }

    public Map<Integer, String> getValorInvestimentoMunicipio(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {

        List<AudespResultado> receitaTotalList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespReceitaTotalMunicipio(codigoIbge, exercicio, mesReferencia);

        List<TcespBiInvestimentoMunicipio> lista = audespBiRepository.getValorInvestimentoMunicipio(codigoIbge, exercicio);

        Map<Integer, String> mapa = new HashMap<>();
        for (TcespBiInvestimentoMunicipio resultado : lista) {

            for (AudespResultado audespResultado : receitaTotalList) {
                if (audespResultado.getExercicio().equals(resultado.getExercicio())
                        && audespResultado.getValor().compareTo(new BigDecimal(0)) != 0) {
                    BigDecimal pencentualInvestimento = resultado.getInvestimentoMunicipio()
                            .divide(audespResultado.getValor(), 4, RoundingMode.HALF_DOWN);
                    mapa.put(resultado.getExercicio(), FormatadorDeDados.formatarPercentual(
                            pencentualInvestimento, true, false, 2));
                }
            }

        }
        return mapa;
    }


    public BigDecimal getValorDespesaEmpenhada(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {

        List<AudespResultado> despesaEmpenhadaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespReceitaTotalMunicipio(codigoIbge, exercicio, mesReferencia);

        List<AudespBiValor> lista = audespBiRepository.getValorDespesaEmpenhada(codigoIbge, exercicio, mesReferencia);
        if(lista.size() > 0 )
            return lista.get(0).getValor();
        else
            return new BigDecimal(0);
    }


}