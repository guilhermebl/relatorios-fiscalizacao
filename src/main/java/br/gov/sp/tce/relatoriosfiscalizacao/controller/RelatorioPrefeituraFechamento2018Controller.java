package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class RelatorioPrefeituraFechamento2018Controller {

    private XWPFDocument document;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Integer exercicio;

    private Integer codigoIBGE;

    FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");

    @Autowired
    private IegmService iegmService;

    private ResultadoIegm resultadoIegm2018;

    @Autowired
    private ParecerPrefeituraService parecerPrefeituraService;

    private List<ParecerPrefeitura> pareceresPrefeiturasList;

    @Autowired
    private TabelasService tabelasService;

    private TabelasProtocolo tabelasProtocolo;

    @Autowired
    private AudespEnsinoService audespEnsinoService;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    @Autowired
    private AudespSaudeService audespSaudeService;

    @Autowired
    private AudespLimiteLrfService audespLimiteLrfService;

    @Autowired
    private AudespService audespService;

    @Autowired
    private AudespAlertasService audespAlertasService;

    @Autowired
    private ApontamentosODSService apontamentosODSService;

    private Map<String, List<ApontamentoODS>> apontamentosODS;

    @Autowired
    private ResourceLoader resourceLoader;

    private Map<Integer, NotaIegm> notasIegm;

    private List<AudespResponsavel> responsavelPrefeitura;

    private Map<String,List<AudespResponsavel>> responsavelSubstitutoPrefeitura;

    private MunicipioIbge municipioIegmCodigoIbge;

    private Map<String, String> audespResultadoExecucaoOrcamentaria;

    @Autowired
    private AudespEntidadeService audespEntidadeService;

    private AudespEntidade audespEntidade;

    @Autowired
    private DemonstrativosRaeeService demonstrativosRaeeService;

    private  Map<String,String> anexo14AMap;

    private Map<String, String> audespEnsinoFundeb;

    private Map<String, String> audespSaude;

    private Map<String, String> audespDespesaPessoalMap;

    private Map<String, String> quadroGeralEnsinoMap;

    private Map<String, String> aplicacoesEmSaude;

    private Map<String, String>  audespResultadoExecucaoOrcamentariaMap;

    @Autowired
    private ApontamentosFOService apontamentosFOService;

    private Map<Integer, List<ApontamentoFO>> apontamentoFOMap;

    @Autowired
    private AudespDividaAtivaService audespDividaAtivaService;

    private Map<String, String> audespDividaAtivaMap;

    @Autowired
    private AudespFase3Service audespFase3Service;

    private Map<String, AudespFase3QuadroPessoal> audespFase3QuadroDePessoalMap;

    @Autowired
    private AudespBiService audespBiService;

    @Autowired
    private TcespBiService tcespBiService;

    private Map<Integer, String> valorInvestimentoMunicipioMap;

    private Map<String, String> rclMunicipioDevedoresMap;

    Formatacao boldItalicUnderlineCapsVermelhoAmareloJustificado12 = formatacaoFactory.getBoldItalicUnderlineCapsVermelhoAmareloJustificado(12);
    Formatacao boldItalicUnderlineVermelhoAmareloJustificado12 = formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12);
    Formatacao boldItalicVermelhoAmareloJustificado12 = formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12);
    Formatacao boldItalicCapsVermelhoAmareloJustificado12 = formatacaoFactory.getBoldItalicCapsVermelhoAmareloJustificado(12);
    Formatacao formatacao12 = formatacaoFactory.getFormatacao(12);
    Formatacao formatacaoBold12 = formatacaoFactory.getBold(12);
    FormatacaoTabela tabela_67_11_11_11 = formatacaoFactory.getFormatacaoTabela_67_11_11_11();


    private String heading1 = "Seção 1";
    private String heading2 = "Seção 2";
    private String heading3 = "Seção 3";
    private String heading4 = "Seção 4";


    private ResponseEntity<Resource> sendFile() throws IOException {
        // fim --------------------------------------
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-relatorio-pm-v1.6.0.docx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }


    public byte[] toByteArray(InputStream in) throws IOException {

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int len;

        // read bytes from the input stream and store them in buffer
        while ((len = in.read(buffer)) != -1) {
            // write bytes from the buffer into output stream
            os.write(buffer, 0, len);
        }

        return os.toByteArray();
    }

    private void addTabelaPassivoFinanceiroAnexo14A() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "25%", "25%", "10%"}, true);


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("PASSIVO FINANCEIRO-ANEXO 14 A",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Saldo Final\nExercício em exame", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Saldo Final\nExercício anterior", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("AH%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Restos a Pagar Processados / Não Processados em Liquidação e " +
                "Não Processados a Pagar", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Restos a Pagar Não Processados", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Demais Obrigações de Curto Prazo", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Outros", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Total", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Total Ajustado", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);

        addTabela(dados, formatacaoTabela);
    }

    private void addTabelaRegimeOrdinarioPrecatorios() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("REGIME ORDINÁRIO DE PAGAMENTO DE PRECATÓRIOS",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Mapas encaminhados no exerc. anterior para pag. no exerc. em exame",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Pagamentos efetuados no exercício em exame",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRightVermelho(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Saldo de precatórios para o exercício seguinte",
                formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela_40_15_15_15_15());

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);
        mergeCells(tabela, mergePositions);
    }

    private void addTabelaRegimeEspecialPrecatorios() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("REGIME ESPECIAL DE PAGAMENTO DE PRECATÓRIOS",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Saldo de Precatórios devidos e não pagos até 31/12 do exerc. " +
                "anterior no BP (passivo)",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Saldo das Contas do TJ para receber os depósitos em 31/12 do exercício" +
                " anterior no BP (ativo)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Saldo apurado em 31/12 do exercício anterior",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Mapas encaminhados no exerc. anterior para pag. no exerc. em exame",
                formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Depósitos efetuados no exercício em exame", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Pagamentos efetuados pelo TJ no exercício em exame", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRightVermelho(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Saldo Financeiro de Precatórios em aberto em 31/12 do exercício em exame",
                formatacaoFactory.getBold(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Saldo das Contas do TJ para receber os depósitos em 31/12 do exercício em exame",
                formatacaoFactory.getBold(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Saldo apurado em 31/12 do exercício em exame",
                formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);
        mergeCells(tabela, mergePositions);
    }

    private void addTabelaPendenciasJudiciais() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("REGIME ORDINÁRIO DE PAGAMENTO DE PRECATÓRIOS",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Mapas encaminhados no exerc. anterior para pag. no exerc. em exame",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Pagamentos efetuados no exercício em exame",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRightVermelho(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Saldo de precatórios para o exercício seguinte",
                formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela_40_15_15_15_15());

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);
        mergeCells(tabela, mergePositions);
    }

    private void addTabelaEndividamentoLongoPrazo() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "25%", "25%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Exercício em exame", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Exercício anterior", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("AH%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Dívida Mobiliária",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Dívida Contratual",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Precatórios",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Parcelamento de Dívidas:",
                formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("De Tributos",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("De Contribuições Sociais:",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("     Previdenciárias",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("     Demais contribuições sociais",
                formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Do FGTS",
                formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Outras Dívidas",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Dívida Consolidada",
                formatacaoFactory.getBold(9)));
        linha12.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha12.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha12.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Ajustes da Fiscalização",
                formatacaoFactory.getBold(9)));
        linha13.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Dívida Consolidada Ajustada",
                formatacaoFactory.getBold(9)));
        linha14.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha14.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha14.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        addTabela(dados, formatacaoTabela);

    }

    private void addTabelaSinteseDoApurado() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, false);



        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Itens", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("CONTROLE INTERNO",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("REGULAR/IRREGULAR\n" +
                "/PARCIALMENTE REGULAR\n", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA - Resultado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA - Percentual de investimentos ",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("DÍVIDA DE CURTO PRAZO ",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("FAVORÁVEL/DESFAVORÁVEL", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("DÍVIDA DE LONGO PRAZO ",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("FAVORÁVEL/DESFAVORÁVEL", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("ESTÁ CUMPRINDO PARCELAMENTOS DE DÉBITOS PREVIDENCIÁRIOS? ",
                formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("SIM/NÃO/PARCIALMENTE", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("PRECATÓRIOS - Foi suficiente o pagamento/depósito de precatórios judiciais? ",
                formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("SIM/NÃO/PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("PRECATÓRIOS - Foi suficiente o pagamento de requisitórios de baixa monta? ",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado("SIM/NÃO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("ENCARGOS - Efetuados os recolhimentos ao Regime Geral de Previdência Social (INSS)? ",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado("SIM/NÃO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("ENCARGOS - Efetuados os recolhimentos ao Regime Próprio de Previdência Social? ",
                formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado("SIM/NÃO/PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("TRANSFERÊNCIAS AO LEGISLATIVO - Os repasses atenderam ao limite constitucional? ",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado("SIM/NÃO/PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("LEI DE RESPONSABILIDADE FISCAL - Despesa de pessoal em dezembro do exercício em exame ",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("ENSINO - Aplicação na Educação - art. 212, Constituição Federal (Limite mínimo de 25%) ",
                formatacaoFactory.getFormatacao(9)));
        linha14.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("ENSINO - FUNDEB aplicado no magistério (Limite mínimo de 60%) ",
                formatacaoFactory.getFormatacao(9)));
        linha15.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("ENSINO - Recursos FUNDEB aplicados no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha16.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("ENSINO - Se diferida, a parcela residual (de até 5%) foi aplicada até" +
                "\n 31.03 do exercício subsequente? ",
                formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado("SIM/NÃO/PREJUDICADO", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("SAÚDE - Aplicação na Saúde (Limite mínimo de 15%)",
                formatacaoFactory.getFormatacao(9)));
        linha18.add(new TextoFormatado("%", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));


        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);


    }


    private void addTabelaRequisitosDeBaixaMonta() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("REQUISITÓRIOS DE BAIXA MONTA",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Saldo de requisitórios devidos e não pagos até 31/12 do exerc. anterior",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Requisitórios de baixa monta incidentes do exerc. em exame",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Pagamentos efetuados no exercício em exame",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRightVermelho(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Ajustes efetuados pela Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Saldo de requisitórios de baixa monta para o exercício seguinte",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);

        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela_40_15_15_15_15());

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);
        mergeCells(tabela, mergePositions);

    }


    private void addTabelaQuitacaoDosPrecatoorios() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("EC Nº 99/2017: QUITAÇÃO DOS PRECATÓRIOS ATÉ 2024",
                formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Saldo de precatórios até 31.12 de 2018",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Número  de  anos  restantes  até 2024",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Valor anual necessário para quitação até 6",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Montante depositado referente ao exercício de 2018",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Nesse ritmo, valor insuficiente para quitação até 2024 de",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);
        mergeCells(tabela, mergePositions);
    }

    private void addTabelaPrecatorioApuracaoPagamentoPiso() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%", "20%", "20%"}, true);
        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("APURAÇÃO DO PAGAMENTO DO PISO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("EXERCÍCIO EM EXAME", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("ALÍQUOTA (ref. dez/2017)", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("RCL-mês de ref.", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado("nov/2017", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("dez/2017", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("jan/2018", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("fev/2018", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("RCL - valor", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl201711"), formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl201712"), formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20181"), formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20182"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("MÊS DE COMPETÊNCIA", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("jan/2018", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado("fev/2018", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado("mar/2018", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado("abr/2018", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("ALÍQUOTA (ref. dez/2017)", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("VALOR CALCULADO PERCENTUALMENTE", formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor201711"), formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor201712"), formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20181"), formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20182"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("VALOR A SER DEPOSITADO (1/12 do VALOR CALCULADO)", formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar201711"), formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar201712"), formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20181"), formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20182"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("RCL-mês de ref.", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado("mar/2018", formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado("abr/2018", formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado("mai/2018", formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado("jun/2018", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("RCL - valor", formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20183"), formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20184"), formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20185"), formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20186"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("MÊS DE COMPETÊNCIA", formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado("mai/2018", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado("jun/2018", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado("jul/2018", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado("ago/2018", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("ALÍQUOTA (ref. dez/2017)", formatacaoFactory.getBold(9)));
        linha12.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha12.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha12.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha12.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("VALOR CALCULADO PERCENTUALMENTE", formatacaoFactory.getBold(9)));
        linha13.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20183"), formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20184"), formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20185"), formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20186"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("VALOR A SER DEPOSITADO (1/12 do VALOR CALCULADO)", formatacaoFactory.getBold(9)));
        linha14.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20183"), formatacaoFactory.getRight(9)));
        linha14.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20184"), formatacaoFactory.getRight(9)));
        linha14.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20185"), formatacaoFactory.getRight(9)));
        linha14.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20186"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("RCL-mês de ref.", formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado("jul/2018", formatacaoFactory.getCenter(9)));
        linha15.add(new TextoFormatado("ago/2018", formatacaoFactory.getCenter(9)));
        linha15.add(new TextoFormatado("set/2018", formatacaoFactory.getCenter(9)));
        linha15.add(new TextoFormatado("out/2018", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("RCL - valor", formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20187"), formatacaoFactory.getRight(9)));
        linha16.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20188"), formatacaoFactory.getRight(9)));
        linha16.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl20189"), formatacaoFactory.getRight(9)));
        linha16.add(new TextoFormatado(rclMunicipioDevedoresMap.get("rcl201810"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("MÊS DE COMPETÊNCIA", formatacaoFactory.getBold(9)));
        linha17.add(new TextoFormatado("set/2018", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado("out/2018", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado("nov/2018", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado("dez/2018", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("ALÍQUOTA (ref. dez/2017)", formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha18.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha18.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));
        linha18.add(new TextoFormatado(rclMunicipioDevedoresMap.get("alicota"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("VALOR CALCULADO PERCENTUALMENTE", formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20187"), formatacaoFactory.getRight(9)));
        linha19.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20188"), formatacaoFactory.getRight(9)));
        linha19.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor20189"), formatacaoFactory.getRight(9)));
        linha19.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valor201810"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("VALOR A SER DEPOSITADO (1/12 do VALOR CALCULADO)", formatacaoFactory.getBold(9)));
        linha20.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20187"), formatacaoFactory.getRight(9)));
        linha20.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20188"), formatacaoFactory.getRight(9)));
        linha20.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar20189"), formatacaoFactory.getRight(9)));
        linha20.add(new TextoFormatado(rclMunicipioDevedoresMap.get("depositar201810"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("VALOR MÍNIMO A SER DEPOSITADO REFERENTE AO EXERCÍCIO EM EXAME", formatacaoFactory.getBoldRight(9)));
        linha21.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha21.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha21.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha21.add(new TextoFormatado(rclMunicipioDevedoresMap.get("valorADepositarTotal"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("MONTANTE DEPOSITADO REFERENTE AO EXERCÍCIO EM EXAME", formatacaoFactory.getBoldRight(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha22.add(new TextoFormatado(" - ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("ATENDIMENTO AO PISO", formatacaoFactory.getBoldRight(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha23.add(new TextoFormatado("ATENDIDO / NÃO ATENDIDO", formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 3));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 4));

        MergePosition mergeH2 = new MergePosition(1, 2);
        mergeH2.addToMergeHorizontal(new MergePosition(1, 3));

        MergePosition mergeH3 = new MergePosition(20, 0);
        mergeH3.addToMergeHorizontal(new MergePosition(20, 1));
        mergeH3.addToMergeHorizontal(new MergePosition(20, 2));
        mergeH3.addToMergeHorizontal(new MergePosition(20, 3));

        MergePosition mergeH4 = new MergePosition(21, 0);
        mergeH4.addToMergeHorizontal(new MergePosition(21, 1));
        mergeH4.addToMergeHorizontal(new MergePosition(21, 2));
        mergeH4.addToMergeHorizontal(new MergePosition(21, 3));

        MergePosition mergeH5 = new MergePosition(22, 0);
        mergeH5.addToMergeHorizontal(new MergePosition(22, 1));
        mergeH5.addToMergeHorizontal(new MergePosition(22, 2));
        mergeH5.addToMergeHorizontal(new MergePosition(22, 3));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);

        mergeCells(tabela, mergePositions);
    }

    private void addTabelaVerificacaoPrecatorio() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"90%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Verificação", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("O Balanço Patrimonial registra, corretamente, as pendências judiciais?",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Sim/Não", formatacaoFactory.getFormatacao(9)));
        dados.add(linha1);
        dados.add(linha2);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);
        mergeCells(tabela, mergePositions);

    }

    private void addTabelaPosicaoRecolhimentosEncargos() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"10", "20%", "70%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Verificações", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        linha1.add(new TextoFormatado("Guias apresentadas", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("1",
                formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("INSS:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2",
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("FGTS:", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("3",
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("RPPS:", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("4",
                formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("PASEP:", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);
        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDividaAtiva() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"44%", "22%", "22%", "22%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Movimentação da Divida Ativa", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2016", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2017", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("AH%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Saldo inicial da Dívida Ativa", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaTotIni" + (exercicio-1) ), formatacaoFactory.getBoldRight(9)));
        linha2.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaTotIni" + exercicio), formatacaoFactory.getBoldRight(9)));
        linha2.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaTotIniAH" + exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Saldo inicial da Dívida Ativa ajustado", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaTotIniAju" + (exercicio-1) ), formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaTotIniAju" + exercicio), formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaTotIniAjuAH" + exercicio), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Saldo inicial da Provisão para Perdas", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerIni" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerIni" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerIniAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Saldo inicial Provisão para Perdas ajustado", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerIniAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerIniAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerIniAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Total", formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));
        linha10.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Total Ajustado", formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));
        linha11.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Recebimentos", formatacaoFactory.getBold(9)));
        linha12.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaRec" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha12.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaRec" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha12.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaRecAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Recebimentos Ajustados", formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaRecAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha15.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaRecAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha15.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaRecAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("Cancelamentos", formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaCanc" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha16.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaCanc" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha16.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaCancAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("Cancelamentos Ajustados", formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaCancAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha19.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaCancAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha19.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaCancAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Valores não Recebidos", formatacaoFactory.getBold(9)));
        linha20.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaNRec" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha20.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaNRec" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha20.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaNRecAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("Valores não Recebidos Ajustados", formatacaoFactory.getBold(9)));
        linha21.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaNRecAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha21.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaNRecAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha21.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaNRecAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("Inscrição", formatacaoFactory.getBold(9)));
        linha22.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaInsc" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha22.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaInsc" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha22.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaInscAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha24 = new ArrayList<>();
        linha24.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha25 = new ArrayList<>();
        linha25.add(new TextoFormatado("Inscrições Ajustadas", formatacaoFactory.getBold(9)));
        linha25.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaInscAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha25.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaInscAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha25.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaInscAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha26 = new ArrayList<>();
        linha26.add(new TextoFormatado("Juros e Atualizações da Dívida", formatacaoFactory.getBold(9)));
        linha26.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaAtu" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha26.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaAtu" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha26.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaAtuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha27 = new ArrayList<>();
        linha27.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha28 = new ArrayList<>();
        linha28.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha28.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha28.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha28.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha29 = new ArrayList<>();
        linha29.add(new TextoFormatado("Juros e Atualizações da Dívida Ajustada", formatacaoFactory.getBold(9)));
        linha29.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaAtuAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha29.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaAtuAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha29.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaAtuAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha30 = new ArrayList<>();
        linha30.add(new TextoFormatado("Saldo Final da Provisão para Perdas", formatacaoFactory.getBold(9)));
        linha30.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerFin" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha30.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerFin" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha30.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerFinAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha31 = new ArrayList<>();
        linha31.add(new TextoFormatado("Inclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha31.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha31.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha31.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha32 = new ArrayList<>();
        linha32.add(new TextoFormatado("Exclusões da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha32.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha32.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha32.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha33 = new ArrayList<>();
        linha33.add(new TextoFormatado("Saldo Final da Provisão p/ Perdas ajustado", formatacaoFactory.getBold(9)));
        linha33.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerFinAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha33.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerFinAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha33.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaProvPerFinAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha34 = new ArrayList<>();
        linha34.add(new TextoFormatado("Saldo Final da Dívida Ativa", formatacaoFactory.getBold(9)));
        linha34.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaFin" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha34.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaFin" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha34.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaFinAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha35 = new ArrayList<>();
        linha35.add(new TextoFormatado("Saldo Final da Dívida Ativa Ajustado", formatacaoFactory.getBold(9)));
        linha35.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaFinAju" + (exercicio-1) ) , formatacaoFactory.getBoldRight(9)));
        linha35.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaFinAju" + exercicio) , formatacaoFactory.getBoldRight(9)));
        linha35.add(new TextoFormatado(audespDividaAtivaMap.get("vDivAtivaFinAjuAH" + exercicio) , formatacaoFactory.getBoldCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);
        dados.add(linha24);
        dados.add(linha25);
        dados.add(linha26);
        dados.add(linha27);
        dados.add(linha28);
        dados.add(linha29);
        dados.add(linha30);
        dados.add(linha31);
        dados.add(linha32);
        dados.add(linha33);
        dados.add(linha34);
        dados.add(linha35);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaComparativoLimiteLRF() {

        Map<String, String> limiteLRF = audespLimiteLrfService.getAudespResultadoInfluenciaOrcamentarioFinanceiro(codigoIBGE, exercicio, 12);

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "20%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("QUADRO COMPARATIVO COM OS LIMITES DA LRF", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("R$", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("RECEITA CORRENTE LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(limiteLRF.get("vRCL"), formatacaoFactory.getBoldRight(9)));
        linha2.add(new TextoFormatado(limiteLRF.get("rclPerc"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DÍVIDA CONSOLIDADA LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(limiteLRF.get("vDivConsolidLiq"), formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(limiteLRF.get("vPercDivConsolidLiq"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Limite Legal - Artigos 3º e 4º. Resolução 40 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(limiteLRF.get("divConsolidLimite"), formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(limiteLRF.get("divConsolidPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(limiteLRF.get("excessoDividaConsolidada"), formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("CONCESSÕES DE GARANTIAS",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Montante",
                formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(limiteLRF.get("vConcGar"), formatacaoFactory.getRight(9))); //VER
        linha8.add(new TextoFormatado(limiteLRF.get("vConcGar"), formatacaoFactory.getRight(9))); //VER

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Limite Legal - Artigo 9º. Resolução 43 do Senado", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(limiteLRF.get("concGarantiaLimite"), formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(limiteLRF.get("concGarantiaPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(limiteLRF.get("excessoConcessaoGarantias"), formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO - Exceto ARO",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(limiteLRF.get("vOpCred"), formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(limiteLRF.get("opCredPercentual"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Limite Legal - Artigo 7º, I. Resolução 43 do Senado ",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(limiteLRF.get("vLimOpCred"), formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(limiteLRF.get("opCreditoExcAroPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(limiteLRF.get("excessoOperacoesCredito"), formatacaoFactory.getFormatacao(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO (Exceto ARO) > DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("ANTECIPAÇÃO DE RECEITAS ORÇAMENTÁRIAS - ARO",
                formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("Limite Legal - Artigo 10. Resolução 43 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha21.add(new TextoFormatado(limiteLRF.get("opAntecipacaoAroLimite"), formatacaoFactory.getRight(9)));
        linha21.add(new TextoFormatado(limiteLRF.get("opAntecipacaoAroPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha22.add(new TextoFormatado(limiteLRF.get("excessoARO"), formatacaoFactory.getFormatacao(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("RECURSOS OBTIDOS COM A ALIENAÇÃO DE ATIVOS",
                formatacaoFactory.getBold(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha24 = new ArrayList<>();
        linha24.add(new TextoFormatado("Saldo do exercício anterior",
                formatacaoFactory.getBold(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha25 = new ArrayList<>();
        linha25.add(new TextoFormatado("Valor arrecadado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha26 = new ArrayList<>();
        linha26.add(new TextoFormatado("Valor aplicado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha27 = new ArrayList<>();
        linha27.add(new TextoFormatado("Saldo a Aplicar",
                formatacaoFactory.getBold(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
//        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);
        dados.add(linha24);
        dados.add(linha25);
        dados.add(linha26);
        dados.add(linha27);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(2, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(2, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(2, 2));

        MergePosition mergeH2 = new MergePosition(6, 0);
        mergeH2.addToMergeHorizontal(new MergePosition(6, 1));
        mergeH2.addToMergeHorizontal(new MergePosition(6, 2));

        MergePosition mergeH3 = new MergePosition(10, 0);
        mergeH3.addToMergeHorizontal(new MergePosition(10, 1));
        mergeH3.addToMergeHorizontal(new MergePosition(10, 2));

        MergePosition mergeH4 = new MergePosition(14, 0);
        mergeH4.addToMergeHorizontal(new MergePosition(14, 1));
        mergeH4.addToMergeHorizontal(new MergePosition(14, 2));

        MergePosition mergeH5 = new MergePosition(16, 1);
        mergeH5.addToMergeHorizontal(new MergePosition(16, 2));

        MergePosition mergeH6 = new MergePosition(17, 0);
        mergeH6.addToMergeHorizontal(new MergePosition(17, 1));
        mergeH6.addToMergeHorizontal(new MergePosition(17, 2));

        MergePosition mergeH7 = new MergePosition(21, 0);
        mergeH7.addToMergeHorizontal(new MergePosition(21, 1));
        mergeH7.addToMergeHorizontal(new MergePosition(21, 2));

        MergePosition mergeV1 = new MergePosition(21, 2);
        mergeV1.addToMergeVertical(new MergePosition(22, 2));
        mergeV1.addToMergeVertical(new MergePosition(23, 2));
        mergeV1.addToMergeVertical(new MergePosition(24, 2));
        mergeV1.addToMergeVertical(new MergePosition(25, 2));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeH6);
        mergePositions.add(mergeH7);
        mergePositions.add(mergeV1);
        mergeCells(tabela, mergePositions);

    }

    private void addTabelaRepassesACamara() {

        Map<String, String>  resultado =
                audespResultadoExecucaoOrcamentariaService.getAudespResultadoRepassesCamara(codigoIBGE, exercicio, 12);

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Valor utilizado pela Câmara em 2018", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado(resultado.get("vCamUtil" + exercicio), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Despesas com inativos", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(resultado.get("vCamDespInativos" + exercicio), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Subtotal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(resultado.get("vCamSubTotal" + exercicio), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Receita Tributária ampliada do exercício anterior (" + (exercicio-1) + ")",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(resultado.get("vArt29ATotal"+ (exercicio-1)), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Percentual resultante",
                formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(resultado.get("vCamPercRepasse"+ exercicio), formatacaoFactory.getBoldRight(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaDespesaDePessoal() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%", "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("2017", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("% Permitido Legal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 12), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Gasto Informado",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 12), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Gastos Ajustados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 12), formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receita Corrente Líquida",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 12), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RCL Ajustada",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 12), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("% Gasto Informado",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 12), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("% Gasto Ajustado",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 12), formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));


        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaQuadroDePessoal() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"16%", "14%", "14%", "14%", "14%", "14%", "14%"}, false);

        AudespFase3QuadroPessoal audespFase3QuadroPessoal20171 = audespFase3QuadroDePessoalMap.get("20171") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20171");
        AudespFase3QuadroPessoal audespFase3QuadroPessoal20181 = audespFase3QuadroDePessoalMap.get("20181") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20181");
        AudespFase3QuadroPessoal audespFase3QuadroPessoal20172 = audespFase3QuadroDePessoalMap.get("20172") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20172");
        AudespFase3QuadroPessoal audespFase3QuadroPessoal20182 = audespFase3QuadroDePessoalMap.get("20182") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20182");
        AudespFase3QuadroPessoal audespFase3QuadroPessoal20175 = audespFase3QuadroDePessoalMap.get("20175") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20175");
        AudespFase3QuadroPessoal audespFase3QuadroPessoal20185 = audespFase3QuadroDePessoalMap.get("20185") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20185");
        AudespFase3QuadroPessoal audespFase3QuadroPessoal20179999 = audespFase3QuadroDePessoalMap.get("20179999") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20179999");
        AudespFase3QuadroPessoal audespFase3QuadroPessoal20189999 = audespFase3QuadroDePessoalMap.get("20189999") == null ? new AudespFase3QuadroPessoal(): audespFase3QuadroDePessoalMap.get("20189999");

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Natureza do cargo/emprego", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Quant. Total de Vagas", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Vagas Providas", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Vagas Não Providas", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado(" ",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("Ex. anterior", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Ex. em exame", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Ex. anterior", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Ex. em exame", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Ex. anterior", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Ex. em exame", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Efetivos",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado("" + audespFase3QuadroPessoal20171.getTotal(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("" + audespFase3QuadroPessoal20181.getTotal(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("" + audespFase3QuadroPessoal20171.getProvidas(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("" + audespFase3QuadroPessoal20181.getProvidas(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("" + audespFase3QuadroPessoal20171.getNaoProvidas(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("" + audespFase3QuadroPessoal20181.getNaoProvidas(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Em comissão",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado("" + audespFase3QuadroPessoal20175.getTotal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("" + audespFase3QuadroPessoal20185.getTotal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("" + audespFase3QuadroPessoal20175.getProvidas(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("" + audespFase3QuadroPessoal20185.getProvidas(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("" + audespFase3QuadroPessoal20175.getNaoProvidas(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("" + audespFase3QuadroPessoal20185.getNaoProvidas(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4eMeio = new ArrayList<>();
        linha4eMeio.add(new TextoFormatado("Efetivos em comissão",
                formatacaoFactory.getBold(9)));
        linha4eMeio.add(new TextoFormatado("" + audespFase3QuadroPessoal20172.getTotal(), formatacaoFactory.getCenter(9)));
        linha4eMeio.add(new TextoFormatado("" + audespFase3QuadroPessoal20182.getTotal(), formatacaoFactory.getCenter(9)));
        linha4eMeio.add(new TextoFormatado("" + audespFase3QuadroPessoal20172.getProvidas(), formatacaoFactory.getCenter(9)));
        linha4eMeio.add(new TextoFormatado("" + audespFase3QuadroPessoal20182.getProvidas(), formatacaoFactory.getCenter(9)));
        linha4eMeio.add(new TextoFormatado("" + audespFase3QuadroPessoal20172.getNaoProvidas(), formatacaoFactory.getCenter(9)));
        linha4eMeio.add(new TextoFormatado("" + audespFase3QuadroPessoal20182.getNaoProvidas(), formatacaoFactory.getCenter(9)));


        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Total",
                formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("" + audespFase3QuadroPessoal20179999.getTotal(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("" + audespFase3QuadroPessoal20189999.getTotal(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("" + audespFase3QuadroPessoal20179999.getProvidas(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("" + audespFase3QuadroPessoal20189999.getProvidas(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("" + audespFase3QuadroPessoal20179999.getNaoProvidas(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("" + audespFase3QuadroPessoal20189999.getNaoProvidas(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Temporários",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("Ex. anterior ", formatacaoFactory.getBoldCenter(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha6.add(new TextoFormatado("Ex. em exame ", formatacaoFactory.getBoldCenter(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha6.add(new TextoFormatado("Em 31.12 do Ex. em exame ", formatacaoFactory.getBoldCenter(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Nº de contratados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));

        MergePosition mergeH1 = new MergePosition(0, 1);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));

        MergePosition mergeH2 = new MergePosition(0, 3);
        mergeH2.addToMergeHorizontal(new MergePosition(0, 4));

        MergePosition mergeH3 = new MergePosition(0, 5);
        mergeH3.addToMergeHorizontal(new MergePosition(0, 6));

        MergePosition mergeH4 = new MergePosition(5, 1);
        mergeH4.addToMergeHorizontal(new MergePosition(5, 2));

        MergePosition mergeH5 = new MergePosition(5, 3);
        mergeH5.addToMergeHorizontal(new MergePosition(5, 4));

        MergePosition mergeH6 = new MergePosition(5, 5);
        mergeH6.addToMergeHorizontal(new MergePosition(5, 6));

        MergePosition mergeH7 = new MergePosition(6, 1);
        mergeH7.addToMergeHorizontal(new MergePosition(6, 2));

        MergePosition mergeH8 = new MergePosition(6, 3);
        mergeH8.addToMergeHorizontal(new MergePosition(6, 4));

        MergePosition mergeH9 = new MergePosition(6, 5);
        mergeH9.addToMergeHorizontal(new MergePosition(6, 6));


        mergePositions.add(mergeV1);
        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeH6);
        mergePositions.add(mergeH7);
        mergePositions.add(mergeH8);
        mergePositions.add(mergeH9);


        mergeCells(tabela, mergePositions);

    }

    private void addTabelaVerificacoesRGA() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "70%", "25%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Verificações", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("1",
                formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("A fixação decorre de lei de iniciativa da Câmara dos Vereadores, em " +
                "consonância com o art. 29, V da Constituição Federal?", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Sim/Não/Prejudicao", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2",
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("A revisão remuneratória se compatibiliza com a inflação dos 12 meses" +
                " anteriores?", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("3",
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("A RGA se deu no mesmo índice e na mesma data dos servidores do Executivo?",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("4",
                formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("Foram apresentadas as declarações de bens nos termos da Lei " +
                "Federal nº 8.429, de 2 de junho de 1992?", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("5",
                formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado("As situações de acúmulos de cargos/funções dos agentes políticos, sob " +
                "amostragem, estavam regulares?", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));


        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaPagamentosExcessivos() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"7%", "31%", "31%", "31%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Valor da fixação original:", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Fixação revisada até o exercício anterior:", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Percentual de revisão no exercício em exame:", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Fixação revisada para o exercício em exame:", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Mês inicial da fixação revisada:", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Mês", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("Fixação + Revisão", formatacaoFactory.getBoldCenter(9)));
        linha6.add(new TextoFormatado("Pagamentos", formatacaoFactory.getBoldCenter(9)));
        linha6.add(new TextoFormatado("Diferenças", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Jan", formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Fev", formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Mar", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Abr", formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Mai", formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Jun", formatacaoFactory.getBold(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Jul", formatacaoFactory.getBold(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Ago", formatacaoFactory.getBold(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Set", formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("Out", formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("Nov", formatacaoFactory.getBold(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("Dez", formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("Total", formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));

        MergePosition mergeH2 = new MergePosition(1, 0);
        mergeH2.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH2.addToMergeHorizontal(new MergePosition(1, 2));

        MergePosition mergeH3 = new MergePosition(2, 0);
        mergeH3.addToMergeHorizontal(new MergePosition(2, 1));
        mergeH3.addToMergeHorizontal(new MergePosition(2, 2));

        MergePosition mergeH4 = new MergePosition(3, 0);
        mergeH4.addToMergeHorizontal(new MergePosition(3, 1));
        mergeH4.addToMergeHorizontal(new MergePosition(3, 2));

        MergePosition mergeH5 = new MergePosition(4, 0);
        mergeH5.addToMergeHorizontal(new MergePosition(4, 1));
        mergeH5.addToMergeHorizontal(new MergePosition(4, 2));


        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);

        mergeCells(tabela, mergePositions);

    }


    private void addTabelaFiscalizacaoOrdenada() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"25%", "25%", "10%", "40%"}, false);

            if(this.apontamentoFOMap.isEmpty()) {
                addTabelaFiscalizacaoOrdenadaVazia();
                return;
            }



            for (Map.Entry<Integer, List<ApontamentoFO>> apontamento : apontamentoFOMap.entrySet()) {
                Integer operacaoId = apontamento.getKey();
                List<ApontamentoFO> list = apontamento.getValue();

                List<List<TextoFormatado>> dados = new ArrayList<>();
                List<TextoFormatado> linha1 = new ArrayList<>();
                linha1.add(new TextoFormatado(list.get(0).getOperacaoNome(), formatacaoFactory.getBoldCenter(9)));
                linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
                linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
                linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

                List<TextoFormatado> linha2 = new ArrayList<>();
                linha2.add(new TextoFormatado("Tema", formatacaoFactory.getBold(9)));
                linha2.add(new TextoFormatado(list.get(0).getOperacaoTema(), formatacaoFactory.getFormatacao(9)));
                linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

                List<TextoFormatado> linha3 = new ArrayList<>();
                linha3.add(new TextoFormatado("Evento destes autos em que o Relatório foi inserido", formatacaoFactory.getBold(9)));
                linha3.add(new TextoFormatado("(se for o caso; se houve menção a processo específico, não mencionar)",
                        formatacaoFactory.getJustificadoVermelhoCinza(9)));
                linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

                List<TextoFormatado> linha4 = new ArrayList<>();
                linha4.add(new TextoFormatado("Processo específico que trata da matéria nº", formatacaoFactory.getBold(9)));
                linha4.add(new TextoFormatado(list.get(0).getProcesso(), formatacaoFactory.getFormatacao(9)));
                linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

                List<TextoFormatado> linha5 = new ArrayList<>();
                linha5.add(new TextoFormatado("Outras observações", formatacaoFactory.getBold(9)));
                linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

                String apontamentosString  = "";
                for(ApontamentoFO apont: list) {
                    apontamentosString += "\n- " + apont.getApontamento();
                }

                List<TextoFormatado> linha6 = new ArrayList<>();
                linha6.add(new TextoFormatado("Irregularidades constatadas na inspeção da Ordenada:", formatacaoFactory.getFormatacao(9))
                        .concat(apontamentosString, formatacaoFactory.getFormatacao(9))
                );
                linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

                List<TextoFormatado> linha7 = new ArrayList<>();
                linha7.add(new TextoFormatado("Constatações", formatacaoFactory.getFormatacao(9))
                        .concat(" in loco: ", formatacaoFactory.getItalic(9))
                        .concat("\n RELATAR SUCINTAMENTE ", formatacaoFactory.getVermelho(9))
                );
                linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
                linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));


                dados.add(linha1);
                dados.add(linha2);
                dados.add(linha3);
                dados.add(linha4);
                dados.add(linha5);
                dados.add(linha6);
                dados.add(linha7);


                XWPFTable tabela = addTabela(dados, formatacaoTabela);

                List<MergePosition> mergePositions = new ArrayList<>();
                MergePosition mergeH1 = new MergePosition(0, 0);
                mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
                mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
                mergeH1.addToMergeHorizontal(new MergePosition(0, 3));

                MergePosition mergeH2 = new MergePosition(1, 1);
                mergeH2.addToMergeHorizontal(new MergePosition(1, 2));
                mergeH2.addToMergeHorizontal(new MergePosition(1, 3));

                MergePosition mergeH3 = new MergePosition(2, 1);
                mergeH3.addToMergeHorizontal(new MergePosition(2, 2));
                mergeH3.addToMergeHorizontal(new MergePosition(2, 3));

                MergePosition mergeH4 = new MergePosition(4, 1);
                mergeH4.addToMergeHorizontal(new MergePosition(4, 2));
                mergeH4.addToMergeHorizontal(new MergePosition(4, 3));

                MergePosition mergeH5 = new MergePosition(5, 0);
                mergeH5.addToMergeHorizontal(new MergePosition(5, 1));
                mergeH5.addToMergeHorizontal(new MergePosition(5, 2));
                mergeH5.addToMergeHorizontal(new MergePosition(5, 3));

                MergePosition mergeH6 = new MergePosition(6, 0);
                mergeH6.addToMergeHorizontal(new MergePosition(6, 1));
                mergeH6.addToMergeHorizontal(new MergePosition(6, 2));
                mergeH6.addToMergeHorizontal(new MergePosition(6, 3));


                mergePositions.add(mergeH1);
                mergePositions.add(mergeH2);
                mergePositions.add(mergeH3);
                mergePositions.add(mergeH4);
                mergePositions.add(mergeH5);
                mergePositions.add(mergeH6);


                mergeCells(tabela, mergePositions);

                addBreak();
            }

    }
    private void addTabelaFiscalizacaoOrdenadaVazia() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"25%", "25%", "10%", "40%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Fiscalização Ordenada nº XX de XX de XX de XXXX.", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Tema", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Evento destes autos em que o Relatório foi inserido", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado("(se for o caso; se houve menção a processo específico, não mencionar)",
                formatacaoFactory.getJustificadoVermelhoCinza(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Processo específico que trata da matéria nº", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado("Se houver", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Outras observações", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));


        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Irregularidades constatadas na inspeção da Ordenada:", formatacaoFactory.getFormatacao(9))
                .concat("\n RELATAR SUCINTAMENTE ", formatacaoFactory.getVermelho(9))
        );
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Constatações", formatacaoFactory.getFormatacao(9))
                .concat(" in loco: ", formatacaoFactory.getItalic(9))
                .concat("\n RELATAR SUCINTAMENTE ", formatacaoFactory.getVermelho(9))
        );
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 3));

        MergePosition mergeH2 = new MergePosition(1, 1);
        mergeH2.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH2.addToMergeHorizontal(new MergePosition(1, 3));

        MergePosition mergeH3 = new MergePosition(2, 1);
        mergeH3.addToMergeHorizontal(new MergePosition(2, 2));
        mergeH3.addToMergeHorizontal(new MergePosition(2, 3));

        MergePosition mergeH4 = new MergePosition(4, 1);
        mergeH4.addToMergeHorizontal(new MergePosition(4, 2));
        mergeH4.addToMergeHorizontal(new MergePosition(4, 3));

        MergePosition mergeH5 = new MergePosition(5, 0);
        mergeH5.addToMergeHorizontal(new MergePosition(5, 1));
        mergeH5.addToMergeHorizontal(new MergePosition(5, 2));
        mergeH5.addToMergeHorizontal(new MergePosition(5, 3));

        MergePosition mergeH6 = new MergePosition(6, 0);
        mergeH6.addToMergeHorizontal(new MergePosition(6, 1));
        mergeH6.addToMergeHorizontal(new MergePosition(6, 2));
        mergeH6.addToMergeHorizontal(new MergePosition(6, 3));


        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeH6);


        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesasCfFundeb() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 212 da Constituição Federal:", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespEnsinoFundeb.get("vPercLiqEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespEnsinoFundeb.get("vPercPagoEnsino"), formatacaoFactory.getRight(9)));

        List<List<TextoFormatado>> dados2 = new ArrayList<>();

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("FUNDEB:", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundebMagist"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundebMagist"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundebMagist"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados2.add(linha5);
        dados2.add(linha6);
        dados2.add(linha7);
        dados2.add(linha8);
        dados2.add(linha9);
        dados2.add(linha10);
        dados2.add(linha11);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
        XWPFTable tabela2 = addTabela(dados2, formatacaoTabela);

    }

    private void addTabelaSaudeReceitasDespesas() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("SAÚDE", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Valores - R$", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Receitas de impostos", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(aplicacoesEmSaude.get("vTotRecImpSau"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Total das Receitas", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(aplicacoesEmSaude.get("vTotRecImpSau"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Total das despesas empenhadas com recursos próprios", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(aplicacoesEmSaude.get("vDespTotSau"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Restos a Pagar Liquidados não pagos até 31.01 de 2019", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(aplicacoesEmSaude.get("vRPNPagoSau"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Valor e percentual aplicado em ações e serviços da Saúde", formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(aplicacoesEmSaude.get("vDespEmpAplicSaude"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado(aplicacoesEmSaude.get("vPercEmpSaude"), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Planejamento atualizado da Saúde", formatacaoFactory.getBold(9)));
        //linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Receita Prevista Atualizada", formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(aplicacoesEmSaude.get("vRecImpSaudePrevAtu"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Despesa Fixada Atualizada", formatacaoFactory.getBold(9)));
        linha12.add(new TextoFormatado(aplicacoesEmSaude.get("vDotAtuSaude"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Índice apurado", formatacaoFactory.getBold(9)));
        linha13.add(new TextoFormatado(aplicacoesEmSaude.get("vPercPrevAtuSaude"), formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(7, 0);
        mergeV1.addToMergeVertical(new MergePosition(8, 0));

        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaVagasEscolares() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "20%", "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("NÍVEL", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DEMANDA POR VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("OFERTA DE VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("RESULTADO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Ens. Infantil (Creche)", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ens. Infantil (Pré escola)", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ens. Fundamental", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaEmpenhadaLiquidadaPaga() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 77, III c/c § 4º do ADCT", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespSaude.get("vPercEmpSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespSaude.get("vPercLiqSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespSaude.get("vPercPagoSaude"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaMultasDeTransito() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Saldo do exercício anterior em 31.12", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Rendimentos de aplicações financeiras", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Valor arrecadado com multas de trânsito", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Subtotal", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Valor aplicado contabilizado ", formatacaoFactory.getBold(9))
                .concat("(artigo 320, LF 9.503/97-CTB)", formatacaoFactory.getFormatacao(9))
        );
        linha6.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Valor aplicado após ajustes", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Saldo no final do exercício fiscalizado", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaReceitadeRoyalties() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "20%", "10%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Saldo do exercício anterior em 31.12", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Rendimentos aplicações financeiras",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Valor arrecadado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Disponibilidade total", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Montante aplicado contabilizado conforme legislação pertinente",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Montante aplicado após ajustes da Fiscalização",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Montante total em desacordo com a legislação aplicável",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Montante gasto com pessoal e serviços da dívida",
                formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Saldo no final do exercício fiscalizado",
                formatacaoFactory.getBold(9)));
        linha12.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 2);
        mergeV1.addToMergeVertical(new MergePosition(1, 2));
        mergeV1.addToMergeVertical(new MergePosition(2, 2));
        mergeV1.addToMergeVertical(new MergePosition(3, 2));

        MergePosition mergeV2 = new MergePosition(6, 2);
        mergeV2.addToMergeVertical(new MergePosition(7, 2));

        mergePositions.add(mergeV1);
        mergePositions.add(mergeV2);

        mergeCells(tabela, mergePositions);

    }


    private void addTabelaIluminacaoPublica() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Saldo em 31.12.2016", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Rendimentos de aplicações financeiras", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Valor arrecadado com no exercício", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Disponibilidade total", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Despesas realizadas no exercício", formatacaoFactory.getBold(9))
        );
        linha6.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Despesas após ajustes", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Saldo no final do exercício fiscalizado", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaMultasDeTransito2() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Saldo do exercício anterior em 31.12", formatacaoFactory.getFormatacao(9)));
        linha1.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Rendimentos de aplicações financeiras", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Valor arrecadado", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Retenção de 1% para o PASEP", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Outros ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Subtotal", formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Valor aplicado contabilizado ", formatacaoFactory.getBold(9))
                .concat("(artigo 320, LF 9.503/97-CTB)", formatacaoFactory.getFormatacao(9))
        );
        linha7.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Valor aplicado após ajustes", formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado("-", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Saldo no final do exercício fiscalizado", formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }


    private void addTabelaDoisUltimosExerciciosApreciados(ParecerPrefeitura parecerPrefeitura){

        if(parecerPrefeitura == null) {
            addTabelaDoisUltimosExerciciosApreciados();
            return;
        }

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%", "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();

        linha1.add(new TextoFormatado("Exercício\n " + parecerPrefeitura.getExercicio(), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC\n " + parecerPrefeitura.getTc(), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DOE\n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data do Trânsito em julgado\n " + parecerPrefeitura.getDataTransitoJulgadoString(),
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Recomendações:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(1, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


        mergePositions.add(mergeH1);


        mergeCells(tabela, mergePositions);
    }

    private void addTabelaDoisUltimosExerciciosApreciados() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%", "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();






        linha1.add(new TextoFormatado("Exercício \n XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DOE \n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data do Trânsito em julgado \n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Recomendações:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));


        dados.add(linha1);
        dados.add(linha2);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(1, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


        mergePositions.add(mergeH1);


        mergeCells(tabela, mergePositions);

    }

    private void addTabelaOcorrenciaDeContratos(String tipoEntidade) {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "35%", "45%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado(tipoEntidade, formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Objeto",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Relator",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Processo nº",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado("TC-XXXXXX.XXX.XX",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("Contrato etc.", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Conclusão da Fiscalização",
                formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Processo nº",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado("TC-XXXXXX.XXX.XX", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("Acompanhamento da Execução", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Datas das visitas",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Última conclusão da Fiscalização ",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Outras observações",
                formatacaoFactory.getBold(9)));
        linha9.add(new TextoFormatado("Relatar sucintamente as ocorrências", formatacaoFactory.getJustificadoVermelho(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Decisão",
                formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Publicação DOE",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Trânsito em julgado",
                formatacaoFactory.getBold(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 1);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));

        MergePosition mergeH2 = new MergePosition(1, 1);
        mergeH2.addToMergeHorizontal(new MergePosition(1, 2));

        MergePosition mergeH3 = new MergePosition(2, 1);
        mergeH3.addToMergeHorizontal(new MergePosition(2, 2));

        MergePosition mergeH4 = new MergePosition(4, 1);
        mergeH4.addToMergeHorizontal(new MergePosition(4, 2));

        MergePosition mergeH5 = new MergePosition(6, 1);
        mergeH5.addToMergeHorizontal(new MergePosition(6, 2));

        MergePosition mergeH6 = new MergePosition(7, 1);
        mergeH6.addToMergeHorizontal(new MergePosition(7, 2));

        MergePosition mergeH7 = new MergePosition(8, 1);
        mergeH7.addToMergeHorizontal(new MergePosition(8, 2));

        MergePosition mergeH8 = new MergePosition(9, 1);
        mergeH8.addToMergeHorizontal(new MergePosition(9, 2));

        MergePosition mergeH9 = new MergePosition(10, 1);
        mergeH9.addToMergeHorizontal(new MergePosition(10, 2));

        MergePosition mergeH10 = new MergePosition(11, 1);
        mergeH10.addToMergeHorizontal(new MergePosition(11, 2));


        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeH6);
        mergePositions.add(mergeH7);
        mergePositions.add(mergeH8);
        mergePositions.add(mergeH9);
        mergePositions.add(mergeH10);

        mergeCells(tabela, mergePositions);

    }


    private void addTabelaProcessoDeContasAnuais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "70%", "25%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Número:", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC-XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Interessado:",
                formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Objeto:",
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Procedência:",
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }



    private void addTabelaIegm() {

        NotaIegm notaIegm2016 = this.notasIegm.get(2016) != null ? this.notasIegm.get(2016) : new NotaIegm();
        NotaIegm notaIegm2017 = this.notasIegm.get(2017) != null ? this.notasIegm.get(2017) : new NotaIegm();
        //NotaIegm notaIegm2018 = this.notasIegm.get(2018) != null ? this.notasIegm.get(2018) : new NotaIegm();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"25%", "25%", "25%", "25%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("EXERCÍCIOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2016", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2017", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("IEG-M", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(notaIegm2016.getFaixa_iegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(notaIegm2017.getFaixa_iegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(resultadoIegm2018.getFaixaIegm(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("i-Planejamento", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(notaIegm2016.getFaixa_iplanejamento(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(notaIegm2017.getFaixa_iplanejamento(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(resultadoIegm2018.getFaixaIPlanejamento(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("i-Fiscal", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(notaIegm2016.getFaixa_ifiscal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(notaIegm2017.getFaixa_ifiscal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(resultadoIegm2018.getFaixaIFiscal(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("i-Educ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(notaIegm2016.getFaixa_ieduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(notaIegm2017.getFaixa_ieduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(resultadoIegm2018.getFaixaIEduc(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("i-Saúde", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(notaIegm2016.getFaixa_isaude(), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(notaIegm2017.getFaixa_isaude(), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(resultadoIegm2018.getFaixaISaude(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("i-Amb", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(notaIegm2016.getFaixa_iamb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(notaIegm2017.getFaixa_iamb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(resultadoIegm2018.getFaixaIAmb(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("i-Cidade", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(notaIegm2016.getFaixa_icidade(), formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(notaIegm2017.getFaixa_icidade(), formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(resultadoIegm2018.getFaixaICidade(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("i-Gov-TI", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(notaIegm2016.getFaixa_igov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(notaIegm2017.getFaixa_igov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(resultadoIegm2018.getFaixaIGov(), formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void addTabelaPareceres() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "30%", "50%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Exercícios", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Processos", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Pareceres", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getTc(), formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getParecer(), formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getTc(), formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getParecer(), formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getTc(), formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getParecer(), formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void addTabelaDescricaoFonteDado() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "30%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("DESCRIÇÃO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("FONTE/DATA", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DADO/ANO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("POPULAÇÃO", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Site IBGE-Cidades", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(municipioIegmCodigoIbge.getPopulacao() + " habitantes", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("ARRECADAÇÃO MUNICIPAL", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("Audesp", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespResultadoExecucaoOrcamentaria.get("vRecArrec"), formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaAjustesFiscalizacaoEnsino() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"55%", "15%", "15%", "15%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Inclusões 2018", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("REC. PRÓPRIOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("FUNDEB 60%", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("FUNDEB 40%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Total das inclusões", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Exclusões 2018", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado("REC. PRÓPRIOS", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado("FUNDEB 60%", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado("FUNDEB 40%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Cancelamento de Restos a Pagar", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Pessoal: desvio de função (salário/encargos)", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Despesas com Ensino Médio", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Despesas com Ensino Superior", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Despesas não amparadas pelo art. 70, LDB", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RP Próprios não pagos até 31.01 de 2019", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("RP Fundeb não pagos até 31.03 de 2019", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Outras", formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Total das exclusões", formatacaoFactory.getBold(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Total dos ajustes: Inclusões - Exclusões", formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("Informações adicionais", formatacaoFactory.getBoldCenter(9)));
        linha16.add(new TextoFormatado("REC. PRÓPRIOS", formatacaoFactory.getBoldCenter(9)));
        linha16.add(new TextoFormatado("FUNDEB 60%", formatacaoFactory.getBoldCenter(9)));
        linha16.add(new TextoFormatado("FUNDEB 40%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("R P Próprios pagos entre 01.02.2019 e a inspeção", formatacaoFactory.getBold(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("Saldo de RP Próprios não quitados até a inspeção",
                formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("R P Fundeb pagos entre 01.04.2019 e a inspeção",
                formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Saldo de RP Fundeb não quitados até a inspeção",
                formatacaoFactory.getBold(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaAjustesFiscalizacaoSaude() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Inclusões 2018", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Total das inclusões", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Exclusões 2018", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Cancelamento de Restos a Pagar", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Pessoal: desvio de função (salário/encargos)", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Plano de Saúde fechado", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Ações de Saúde não promovidas pelo SUS", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Demais despesas não elegíveis - Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RP Liquidados não pagos até 31.01.2019", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Outras", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Total das exclusões", formatacaoFactory.getBold(9)));
        linha14.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Total dos ajustes: Inclusões - Exclusões", formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("Informações adicionais", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("R Pagar pagos entre 01.02.2019 e a fiscalização", formatacaoFactory.getBold(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("Saldo de RP não quitados até a fiscalização",
                formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        //dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaInformacoesFranqueadas() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "30%", "30%", "25%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Repasse", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Valor informado", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Valor contabilizado", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Diferença", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("FPM", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("ITR", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("L.C. 87/96", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("ICMS", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("IPVA", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("IPI/Exp.", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("FUNDEB", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("CIDE", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado("-", formatacaoFactory.getCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaLicenciamentoAmbiental() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Município Licenciador", formatacaoFactory.getBoldVermelhoAmareloCenter(9)));
        linha1.add(new TextoFormatado("DF / UR", formatacaoFactory.getBoldVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Americana", formatacaoFactory.getVermelhoAmarelo(9)));
        linha2.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Atibaia", formatacaoFactory.getVermelhoAmarelo(9)));
        linha3.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Barueri", formatacaoFactory.getVermelhoAmarelo(9)));
        linha4.add(new TextoFormatado("DF-08", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Cajamar", formatacaoFactory.getVermelhoAmarelo(9)));
        linha5.add(new TextoFormatado("DF-08", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Campinas", formatacaoFactory.getVermelhoAmarelo(9)));
        linha6.add(new TextoFormatado("UR-10", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Caraguatatuba", formatacaoFactory.getVermelhoAmarelo(9)));
        linha7.add(new TextoFormatado("UR-07", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha7eMeio = new ArrayList<>();
        linha7eMeio.add(new TextoFormatado("Catanduva", formatacaoFactory.getVermelhoAmarelo(9)));
        linha7eMeio.add(new TextoFormatado("UR-08", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Guarulhos", formatacaoFactory.getVermelhoAmarelo(9)));
        linha8.add(new TextoFormatado("DF-03", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Hortolândia", formatacaoFactory.getVermelhoAmarelo(9)));
        linha9.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Indaiatuba", formatacaoFactory.getVermelhoAmarelo(9)));
        linha10.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Itatiba", formatacaoFactory.getVermelhoAmarelo(9)));
        linha11.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Lorena", formatacaoFactory.getVermelhoAmarelo(9)));
        linha12.add(new TextoFormatado("UR-14", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Mauá", formatacaoFactory.getVermelhoAmarelo(9)));
        linha13.add(new TextoFormatado("DF-04", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Piracicaba", formatacaoFactory.getVermelhoAmarelo(9)));
        linha14.add(new TextoFormatado("UR-10", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Ribeirão Pires", formatacaoFactory.getVermelhoAmarelo(9)));
        linha15.add(new TextoFormatado("UR-20", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("Ribeirão Preto", formatacaoFactory.getVermelhoAmarelo(9)));
        linha16.add(new TextoFormatado("UR-17", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("S. B. do Campo", formatacaoFactory.getVermelhoAmarelo(9)));
        linha17.add(new TextoFormatado("DF-04", formatacaoFactory.getVermelhoAmareloCenter(9)));


        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("Santana de Parnaíba", formatacaoFactory.getVermelhoAmarelo(9)));
        linha18.add(new TextoFormatado("DF-08", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("Santo André", formatacaoFactory.getVermelhoAmarelo(9)));
        linha19.add(new TextoFormatado("DF-09", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Santos", formatacaoFactory.getVermelhoAmarelo(9)));
        linha20.add(new TextoFormatado("DF-06", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("São Sebastião", formatacaoFactory.getVermelhoAmarelo(9)));
        linha21.add(new TextoFormatado("UR-07", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("São Vicente", formatacaoFactory.getVermelhoAmarelo(9)));
        linha22.add(new TextoFormatado("UR-20", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("Sorocaba", formatacaoFactory.getVermelhoAmarelo(9)));
        linha23.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha24 = new ArrayList<>();
        linha24.add(new TextoFormatado("Sumaré", formatacaoFactory.getVermelhoAmarelo(9)));
        linha24.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha25 = new ArrayList<>();
        linha25.add(new TextoFormatado("Tatuí", formatacaoFactory.getVermelhoAmarelo(9)));
        linha25.add(new TextoFormatado("UR-09", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha26 = new ArrayList<>();
        linha26.add(new TextoFormatado("Valinhos", formatacaoFactory.getVermelhoAmarelo(9)));
        linha26.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));

        List<TextoFormatado> linha27 = new ArrayList<>();
        linha27.add(new TextoFormatado("Vinhedo", formatacaoFactory.getVermelhoAmarelo(9)));
        linha27.add(new TextoFormatado("UR-03", formatacaoFactory.getVermelhoAmareloCenter(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha7eMeio);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);
        dados.add(linha24);
        dados.add(linha25);
        dados.add(linha26);
        dados.add(linha27);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaVerificacaoContratoDeConcessao() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "80%", "15%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Verificações: PPP", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("1", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("O Município tem contratos de concessão e permissão de serviços públicos?",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("Houve regulamentação do serviço concedido?", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("3", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("Houve mecanismos de manutenção da qualidade do serviço, bem com apuração " +
                "e solução de queixas e reclamações dos usuários?", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("4", formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("O Poder Concedente tem observado o cumprimento das disposições " +
                "regulamentares do serviço e as cláusulas pactuadas?", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("5", formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado("Houve aplicação de penalidades regulamentares e " +
                "contratuais?", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));

        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);
    }

    private void addTabelaVerificacaoPPP() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "80%", "15%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Verificações: PPP", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("1", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado("O Município tem contratação de Parcerias Público-Privada (PPP)?",
                formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("2", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado("Houve regulamentação do serviço concedido?", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("3", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado("Houve mecanismos de manutenção da qualidade do serviço, bem com apuração " +
                "e solução de queixas e reclamações dos usuários?", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("4", formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado("O Poder Concedente tem observado o cumprimento das disposições " +
                "regulamentares do serviço e as cláusulas pactuadas?", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("5", formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado("Houve aplicação de penalidades regulamentares e " +
                "contratuais?", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));

        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);
    }

    private void addTabelaModalidadeValoresPercentual() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "30%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Modalidade", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Valores - R$", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Percentual", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Concorrência", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Tomada de Preços", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Convite", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Pregão", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Concurso", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("BEC - Bolsa Eletrônica de Compras", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Dispensa de licitação", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inexigibilidade", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Outros / Não aplicável", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Total geral", formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void addTabelaAplicacaoFundebResidual_31_03_19() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "20%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Aplicação  do  FUNDEB  residual  até  31.03  do exercício  seguinte: 2019",
                formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Receitas de Impostos e Transferências de Impostos",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Retenções ao FUNDEB", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Receitas de transferências do FUNDEB sem rendimentos financeiros",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Receitas de aplicações financeiras", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Despesas com recursos do FUNDEB", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Saldo FUNDEB para aplicação no 1º trimestre de: 2019",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Máximo de até 5% do FUNDEB acrescentável aos 25% (art. 212, CF)",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Empenho  e  pagamento  com  FUNDEB   residual  feitos no  primeiro  trimestre  de 2019",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Saldo   do  FUNDEB residual  não empenhado  e pago  até  o primeiro  trimestre   de 2019",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Valor a ser  adicionado à  aplicação de 2018 para compor o mínimo de 25%",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Aplicação na Educação até  31.12 de 2018", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Aplicação em 31.12 de 2018 mais FUNDEB  utilizado  até  31.03  de 2019",
                formatacaoFactory.getBold(9)));
        linha13.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeH1 = new MergePosition(0, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));

        mergePositions.add(mergeH1);

        mergeCells(tabela, mergePositions);

    }


    private void addTabelaFundebNossaParte() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "20%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();

        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("IMPOSTOS E TRANSFERÊNCIAS DE IMPOSTOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("RECEITAS", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(quadroGeralEnsinoMap.get("vRecImpEducArrec"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Total de Receitas de Impostos - T.R.I.", formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(quadroGeralEnsinoMap.get("vTotRecImpEdu"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("FUNDEB  - RECEITAS", formatacaoFactory.getBoldCenter(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Retenções", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebRetido"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Transferências recebidas", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebRecebido"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receitas de aplicações financeiras", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebRecApliFin"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Ajustes da Fiscalização", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado("", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Total das Receitas do FUNDEB - T.R.F.", formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebRecTot"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("FUNDEB  - DESPESAS", formatacaoFactory.getBoldCenter(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Despesas com Magistério", formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(quadroGeralEnsinoMap.get("vDespEmpFundebMagist"), formatacaoFactory.getRight(9)));


        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Outros ajustes da Fiscalização (60%)", formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado((""), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha14 = new ArrayList<>();
        linha14.add(new TextoFormatado("Total das Despesas Líquidas com Magistério (mínimo: 60%)", formatacaoFactory.getFormatacao(9)));
        linha14.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebDespMagTot"), formatacaoFactory.getRight(9)));
        linha14.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebDespPerc"), formatacaoFactory.getRight(9)));

        //DEMAIS DESPESAS
        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Demais Despesas", formatacaoFactory.getFormatacao(9)));
        linha15.add(new TextoFormatado(quadroGeralEnsinoMap.get("vDespEmpFundebOutros"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("Outros ajustes da Fiscalização (40%)", formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("Total das Demais Despesas Líquidas (máximo: 40%)", formatacaoFactory.getBold(9)));
        linha17.add(new TextoFormatado(quadroGeralEnsinoMap.get("vDemDespEduTot"), formatacaoFactory.getBoldRight(9)));
        linha17.add(new TextoFormatado(quadroGeralEnsinoMap.get("vDemDespEduPerc"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("Total aplicado  no FUNDEB", formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        linha18.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));


        //DESPESAS PROPRIAS EM EDUCACAO
        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("DESPESAS PRÓPRIAS EM EDUCAÇÃO", formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Educação Básica (exceto FUNDEB)", formatacaoFactory.getFormatacao(9)));
        linha20.add(new TextoFormatado(quadroGeralEnsinoMap.get("vDespEduBas"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("Acréscimo: FUNDEB retido", formatacaoFactory.getFormatacao(9)));
        linha21.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebRetido"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("Dedução: Ganhos de aplicações financeiras (Ficha de Receita 29)", formatacaoFactory.getFormatacao(9)));
        linha22.add(new TextoFormatado(quadroGeralEnsinoMap.get("vGanApliFinEdu"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("Deducão: FUNDEB retido e não aplicado no retorno", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebRetidoNaoAplicadoEmp"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha24 = new ArrayList<>();
        linha24.add(new TextoFormatado("Aplicação  apurada   até  o  dia   31.12.18", formatacaoFactory.getBold(9)));
        linha24.add(new TextoFormatado(quadroGeralEnsinoMap.get("vApliArt212Edu"), formatacaoFactory.getBoldRight(9)));
        linha24.add(new TextoFormatado(quadroGeralEnsinoMap.get("vApliArt212EduPerc"), formatacaoFactory.getBoldRight(9)));

        //APLICADO 1o TRIMESTRE
        List<TextoFormatado> linha25 = new ArrayList<>();
        linha25.add(new TextoFormatado("Acréscimo: FUNDEB: retenção até 5%: (________) Aplic. no 1º trim. de 2019", formatacaoFactory.getFormatacao(9)));
        linha25.add(new TextoFormatado(quadroGeralEnsinoMap.get("vFundebApliTri1"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha26 = new ArrayList<>();
        linha26.add(new TextoFormatado("Dedução: Restos a Pagar não pagos - recursos próprios - até 31.01.19", formatacaoFactory.getFormatacao(9)));
        linha26.add(new TextoFormatado(quadroGeralEnsinoMap.get("vRPNPagoEdu"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha27 = new ArrayList<>();
        linha27.add(new TextoFormatado("Outros ajustes da Fiscalização - Recursos Próprios", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(quadroGeralEnsinoMap.get("vRecPropFundebAjuAud"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha28 = new ArrayList<>();
        linha28.add(new TextoFormatado("Aplicação final na Educação Básica", formatacaoFactory.getBold(9)));
        linha28.add(new TextoFormatado(quadroGeralEnsinoMap.get("vApliFinEduBas"), formatacaoFactory.getBoldRight(9)));
        linha28.add(new TextoFormatado(quadroGeralEnsinoMap.get("vApliFinEduBasPerc"), formatacaoFactory.getBoldRight(9)));


        //PLANEJAMENTO DO ENSINO
        List<TextoFormatado> linha29 = new ArrayList<>();
        linha29.add(new TextoFormatado("PLANEJAMENTO ATUALIZADO DA EDUCAÇÃO", formatacaoFactory.getBold(9)));
        linha29.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha30 = new ArrayList<>();
        linha30.add(new TextoFormatado("Receita Prevista Realizada", formatacaoFactory.getBold(9)));
        linha30.add(new TextoFormatado(quadroGeralEnsinoMap.get("vRecImpEducPrevAtu"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha31 = new ArrayList<>();
        linha31.add(new TextoFormatado("Despesa Fixada Atualizada", formatacaoFactory.getBold(9)));
        linha31.add(new TextoFormatado(quadroGeralEnsinoMap.get("vDotAtuEnsino"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha32 = new ArrayList<>();
        linha32.add(new TextoFormatado("Índice Apurado", formatacaoFactory.getBold(9)));
        linha32.add(new TextoFormatado(quadroGeralEnsinoMap.get("vPercPrevAtuEnsino"), formatacaoFactory.getBoldRight(9)));


        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);
        dados.add(linha24);
        dados.add(linha25);
        dados.add(linha26);
        dados.add(linha27);
        dados.add(linha28);
        dados.add(linha29);
        dados.add(linha30);
        dados.add(linha31);
        dados.add(linha32);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

//        List<MergePosition> mergePositions = new ArrayList<>();
//
//        MergePosition mergeH1 = new MergePosition(0, 0);
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
//
//        mergePositions.add(mergeH1);
//
//        mergeCells(tabela, mergePositions);
    }


    private void addTabelaDadosIniciais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "5%", "75%"}, false);
        formatacaoTabela.setBorder(false);


        String vigenciasPrefeito = "";

        for(int i = 0; i < this.responsavelPrefeitura.size(); i++) {
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataInicioVigenciaFormatado(): "dado não informado";
            vigenciasPrefeito += " a ";
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataFimVigenciaFormatado() :  "dado não informado";
            if(i != this.responsavelPrefeitura.size() - 1){
                vigenciasPrefeito += "; ";
            }
        }

        String vigenciasSubstituto = "";

        AudespResponsavel prefeito = (this.responsavelPrefeitura != null
                && this.responsavelPrefeitura.size() > 0) ? this.responsavelPrefeitura.get(0) : new AudespResponsavel();

        if(prefeito == null)
            prefeito = new AudespResponsavel();

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Processo", formatacaoFactory.getBold(12)));
        linha1.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha1.add(new TextoFormatado("TC-" + tabelasProtocolo.getProcesso(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha1);

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Entidade", formatacaoFactory.getBold(12)));
        linha2.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha2.add(new TextoFormatado(tabelasProtocolo.getNomeOrgao(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha2);

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Assunto", formatacaoFactory.getBold(12)));
        linha3.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha3.add(new TextoFormatado("Contas Anuais", formatacaoFactory.getFormatacao(12)));
        dados.add(linha3);

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Exercício", formatacaoFactory.getBold(12)));
        linha4.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha4.add(new TextoFormatado("2018", formatacaoFactory.getFormatacao(12)));
        dados.add(linha4);

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Prefeito", formatacaoFactory.getBold(12)));
        linha5.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha5.add(new TextoFormatado(prefeito.getNome(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha5);

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
        linha6.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha6.add(new TextoFormatado(prefeito.getCpf(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha6);

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
        linha7.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha7.add(new TextoFormatado(vigenciasPrefeito, formatacaoFactory.getFormatacao(12)));
        dados.add(linha7);

        for (Map.Entry<String, List<AudespResponsavel>> item : this.responsavelSubstitutoPrefeitura.entrySet()) {
            String nome = item.getKey();
            List<AudespResponsavel> listaSubstitutos = item.getValue();
            String vigencias = "";
            for (int i = 0; i < listaSubstitutos.size(); i++) {
                vigencias += listaSubstitutos.get(i).getDataInicioVigenciaFormatado();
                vigencias += " a ";
                vigencias += listaSubstitutos.get(i).getDataFimVigenciaFormatado();
                if (i != listaSubstitutos.size() - 1) {
                    vigencias += "; ";
                }
            }

            AudespResponsavel subs = listaSubstitutos != null && listaSubstitutos.size() > 0 ?
                    listaSubstitutos.get(0) : new AudespResponsavel();


            List<TextoFormatado> linhaSub = new ArrayList<>();
            linhaSub.add(new TextoFormatado("Substituto", formatacaoFactory.getBold(12)));
            linhaSub.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaSub.add(new TextoFormatado(nome, formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaCpf = new ArrayList<>();
            linhaCpf.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
            linhaCpf.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaCpf.add(new TextoFormatado(subs.getCpf(), formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaPeriodo = new ArrayList<>();
            linhaPeriodo.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
            linhaPeriodo.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaPeriodo.add(new TextoFormatado(vigencias, formatacaoFactory.getFormatacao(12)));

            dados.add(linhaSub);
            dados.add(linhaCpf);
            dados.add(linhaPeriodo);
        }


        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Relatoria", formatacaoFactory.getBold(12)));
        linha11.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha11.add(new TextoFormatado(tabelasProtocolo.getRelator(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha11);

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Instrução", formatacaoFactory.getBold(12)));
        linha12.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha12.add(new TextoFormatado(tabelasProtocolo.getSecaoFiscalizadoraContas().trim() + " / " +
                tabelasProtocolo.getDsf(), formatacaoFactory.getFormatacao(12)));

        dados.add(linha12);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void addTabelaSubsidioAgentePoliticos() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"55%", "15%", "15%", "15%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("CARGOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("SECRETÁRIOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("VICE-PREFEITO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("PREFEITO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Valor subsídio inicial fixado para a legislatura", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("(+)       % = RGA 2017 em            /16 – Lei Municipal nº XX, de XX de XXXXXXXX de XXXX", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("(+)       % = RGA 201__ em     ___/___/___ – Lei Municipal nº XX, de XX de XXXXXXXX de XXXX", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("(+)       % = RGA 201__ em     ___/___/___– Lei Municipal nº XX, de XX de XXXXXXXX de XXXX", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("(+)       % = RGA 201__ em     ___/___/___– Lei Municipal nº XX, de XX de XXXXXXXX de XXXX", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("R$", formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaIndiceDeLiquidezImediata() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "20%", "20%", "20%"}, false);
        Map<String, String>  resultado =
                audespResultadoExecucaoOrcamentariaService.getIndiceDeLiquidezImediata(codigoIBGE, exercicio, 12);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Índice de Liquidez Imediata", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Disponível", formatacaoFactory.getFormatacao(9)));
        linha1.add(new TextoFormatado(resultado.get("vSalFinAtivoDispAjuAud"), formatacaoFactory.getRight(9)));
        linha1.add(new TextoFormatado(resultado.get("indiceLiquidezImediata"), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Passivo Circulante", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(resultado.get("vPassivoCircAju"), formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));

        MergePosition mergeV2 = new MergePosition(0, 3);
        mergeV2.addToMergeVertical(new MergePosition(1, 3));

        mergePositions.add(mergeV1);
        mergePositions.add(mergeV2);

        mergeCells(tabela, mergePositions);
    }

    private void addTabelaResultadoFinanceiroAnterior() {

        Map<String,String> resultado = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoInfluenciaOrcamentarioFinanceiro(this.codigoIBGE, exercicio, 12);

        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"60%", "10%", "30%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Resultado financeiro do exercício anterior",
                formatacaoFactory.getFormatacao(9)));
        linha1.add(new TextoFormatado("" + (exercicio - 1), formatacaoFactory.getCenter(9)));
        linha1.add(new TextoFormatado(resultado.get("vResFinAnt"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Ajustes por Variações Ativas", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("" + exercicio, formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(resultado.get("vAjuVarAtiv"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ajustes por Variações Passivas", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("" + exercicio, formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(resultado.get("vAjuVarPas"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Resultado Financeiro retificado do exercício de", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado("" + (exercicio - 1), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(resultado.get("vResFinAntAju"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Resultado Orçamentário do exercício de", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado("" + exercicio, formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(resultado.get("vResMovOrc"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Resultado Financeiro do exercício de ", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado("" + exercicio, formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(resultado.get("vResFinAtuApu"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);

        addTabela(dados, formatacaoTabela);
    }


    private void addTabelaExecucaoOrcamentaria() {
        Map<String, String> audespResultado = this.audespResultadoExecucaoOrcamentaria ;


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("R$", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("(+) RECEITAS REALIZADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha2.add(new TextoFormatado(audespResultado.get("vTotRecRealPM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("(-) DESPESAS EMPENHADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado(audespResultado.get("vTotDespEmpBO"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("(-) REPASSES DE DUODÉCIMOS À CÂMARA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado(audespResultado.get("vRepDuodCM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha5 = new ArrayList<>();
        cabecalhoLinha5.add(new TextoFormatado("(+) DEVOLUÇÃO DE DUODÉCIMOS DA CÂMARA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado(audespResultado.get("vDevDuod"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha6 = new ArrayList<>();
        cabecalhoLinha6.add(new TextoFormatado("(-) TRANSFERÊNCIAS FINANCEIRAS À ADMINISTRAÇÃO INDIRETA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado(audespResultado.get("vTransfFinAdmIndExec"), formatacaoFactory.getRight(9)));


        List<TextoFormatado> cabecalhoLinha7 = new ArrayList<>();
        cabecalhoLinha7.add(new TextoFormatado("(+ ou -) AJUSTES DA FISCALIZAÇÃO", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha8 = new ArrayList<>();
        cabecalhoLinha8.add(new TextoFormatado("RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBold(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("vResExecOrcGeralPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("vResExecOrcGeralPMAV"), formatacaoFactory.getBoldRight(9)));

        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);
        dados.add(cabecalhoLinha5);
        dados.add(cabecalhoLinha6);
        dados.add(cabecalhoLinha7);
        dados.add(cabecalhoLinha8);

        addTabela(dados, formatacaoFactory.getFormatacaoTabela_tabela_70_20_10());
    }

    private void addTabelaReceitas_AV_AH() {
        Map<String,String> receitas = audespResultadoExecucaoOrcamentariaService
                .getEquilibrioOrcamentarioReceitasFormatado(this.codigoIBGE, exercicio, 12);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("Receitas", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Previsão", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Realização", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("AH%", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("AV%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("Receitas Correntes", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha2.add(new TextoFormatado(receitas.get("vRecCorPrev"), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(receitas.get("vRecCorReal"), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(receitas.get("vRecCorAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado(receitas.get("vRecCorPMAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("Receitas de Capital", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado(receitas.get("vRecCapPrev"), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(receitas.get("vRecCapReal"), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(receitas.get("vRecCapOpCredAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha3.add(new TextoFormatado(receitas.get("vRecCapOpCredAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("Receitas Intraorçamentárias", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado(receitas.get("vRecIntraOrcPrev"), formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(receitas.get("vRecIntraOrcReal"), formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(receitas.get("vRecIntraOrcAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha4.add(new TextoFormatado(receitas.get("vRecIntraOrcPMAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha5 = new ArrayList<>();
        cabecalhoLinha5.add(new TextoFormatado("Deduções da Receita", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado(receitas.get("vDedRecPrev"), formatacaoFactory.getRight(9)));
        cabecalhoLinha5.add(new TextoFormatado(receitas.get("vDedRecReal"), formatacaoFactory.getRight(9)));
        cabecalhoLinha5.add(new TextoFormatado(receitas.get("vDedRecAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha5.add(new TextoFormatado(receitas.get("vDedRecPMAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha6 = new ArrayList<>();
        cabecalhoLinha6.add(new TextoFormatado("Subtotal das Receitas", formatacaoFactory.getBold(9)));
        cabecalhoLinha6.add(new TextoFormatado(receitas.get("vSubTotRecPrevPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha6.add(new TextoFormatado(receitas.get("vSubTotRecRealPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha7 = new ArrayList<>();
        cabecalhoLinha7.add(new TextoFormatado("Outros Ajustes", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha8 = new ArrayList<>();
        cabecalhoLinha8.add(new TextoFormatado("Total das Receitas", formatacaoFactory.getBold(9)));
        cabecalhoLinha8.add(new TextoFormatado(receitas.get("vTotRecPrevPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha8.add(new TextoFormatado(receitas.get("vTotRecRealPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha8.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha8.add(new TextoFormatado(receitas.get("vTotRecPMAV"), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha9 = new ArrayList<>();
        cabecalhoLinha9.add(new TextoFormatado("Déficit de arrecadação", formatacaoFactory.getBold(9)));
        cabecalhoLinha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha9.add(new TextoFormatado(receitas.get("vResExecOrcRecPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha9.add(new TextoFormatado(receitas.get("vResExecOrcRecPMAV"), formatacaoFactory.getBoldCenter(9)));


        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);
        dados.add(cabecalhoLinha5);
        dados.add(cabecalhoLinha6);
        dados.add(cabecalhoLinha7);
        dados.add(cabecalhoLinha8);
        dados.add(cabecalhoLinha9);

        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "18%", "18%", "12%", "12%"}, true));

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(5, 3);
        mergeH1.addToMergeHorizontal(new MergePosition(5, 4));

        MergePosition mergeH2 = new MergePosition(6, 3);
        mergeH2.addToMergeHorizontal(new MergePosition(6, 4));

        MergePosition mergeH3 = new MergePosition(6, 0);
        mergeH3.addToMergeHorizontal(new MergePosition(6, 1));

        MergePosition mergeV1 = new MergePosition(5, 3);
        mergeV1.addToMergeVertical(new MergePosition(6, 3));
        mergeV1.addToMergeVertical(new MergePosition(7, 3));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);


    }

    private void addTabelaDespesas_AV_AH() {

        Map<String,String> despesas = audespResultadoExecucaoOrcamentariaService
                .getEquilibrioOrcamentarioDespesasFormatado(this.codigoIBGE, exercicio, 12);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("Despesas Empenhadas", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Fixação Final", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Execução", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("AH%", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("AV%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("Despesas Correntes", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha2.add(new TextoFormatado(despesas.get("vDespCorFix"), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(despesas.get("vDespCorExec"), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(despesas.get("vDespCorAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado(despesas.get("vDespCorPMAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("Despesas de Capital", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado(despesas.get("vDespCapAmortFix"), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(despesas.get("vDespCapAmortExec"), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(despesas.get("vDespCapAmortAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha3.add(new TextoFormatado(despesas.get("vDespCapAmortAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("Reserva de Contingência", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado(despesas.get("vResCont"), formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));
        cabecalhoLinha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha5 = new ArrayList<>();
        cabecalhoLinha5.add(new TextoFormatado("Despesas Intraorçamentárias", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado(despesas.get("vDespIntraOrcFix"), formatacaoFactory.getRight(9)));
        cabecalhoLinha5.add(new TextoFormatado(despesas.get("vDespIntraOrcExec"), formatacaoFactory.getRight(9)));
        cabecalhoLinha5.add(new TextoFormatado(despesas.get("vDespIntraOrcAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha5.add(new TextoFormatado(despesas.get("vDespIntraOrcPMAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha6 = new ArrayList<>();
        cabecalhoLinha6.add(new TextoFormatado("Repasses de duodécimos à CM", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado(despesas.get("vRepDuodFix"), formatacaoFactory.getRight(9)));
        cabecalhoLinha6.add(new TextoFormatado(despesas.get("vRepDuodExec"), formatacaoFactory.getRight(9)));
        cabecalhoLinha6.add(new TextoFormatado(despesas.get("vRepDuodAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha6.add(new TextoFormatado(despesas.get("vRepDuodAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha7 = new ArrayList<>();
        cabecalhoLinha7.add(new TextoFormatado("Transf. Financeiras à Adm. Indireta", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado(despesas.get("vTransfFinAdmIndFix"), formatacaoFactory.getRight(9)));
        cabecalhoLinha7.add(new TextoFormatado(despesas.get("vTransfFinAdmIndExec"), formatacaoFactory.getRight(9)));
        cabecalhoLinha7.add(new TextoFormatado(despesas.get("vTransfFinAdmIndAH"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha7.add(new TextoFormatado(despesas.get("vTransfFinAdmIndAV"), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha8 = new ArrayList<>();
        cabecalhoLinha8.add(new TextoFormatado("Dedução: devolução de duodécimos", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha8.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        cabecalhoLinha8.add(new TextoFormatado(despesas.get("vDevDuod"), formatacaoFactory.getRight(9)));
        cabecalhoLinha8.add(new TextoFormatado("", formatacaoFactory.getCenter(9)));
        cabecalhoLinha8.add(new TextoFormatado("", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha9 = new ArrayList<>();
        cabecalhoLinha9.add(new TextoFormatado("Subtotal das Despesas", formatacaoFactory.getBold(9)));
        cabecalhoLinha9.add(new TextoFormatado(despesas.get("vSubTotDespFixPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha9.add(new TextoFormatado(despesas.get("vSubTotDespExecPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha9.add(new TextoFormatado(despesas.get("vSubTotDespPMAH"), formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha9.add(new TextoFormatado(despesas.get("vSubTotDespPMAV"), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha10 = new ArrayList<>();
        cabecalhoLinha10.add(new TextoFormatado("Outros Ajustes", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha10.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        cabecalhoLinha10.add(new TextoFormatado("", formatacaoFactory.getRight(9)));
        cabecalhoLinha10.add(new TextoFormatado("", formatacaoFactory.getCenter(9)));
        cabecalhoLinha10.add(new TextoFormatado("", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha11 = new ArrayList<>();
        cabecalhoLinha11.add(new TextoFormatado("Total das Despesas", formatacaoFactory.getBold(9)));
        cabecalhoLinha11.add(new TextoFormatado(despesas.get("vTotDespFixPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha11.add(new TextoFormatado(despesas.get("vTotDespExecPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha11.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha11.add(new TextoFormatado(despesas.get("vTotDespPMAV"), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha12 = new ArrayList<>();
        cabecalhoLinha12.add(new TextoFormatado("Economia Orçamentária", formatacaoFactory.getBold(9)));
        cabecalhoLinha12.add(new TextoFormatado("", formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha12.add(new TextoFormatado(despesas.get("vEconOrc"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha12.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha12.add(new TextoFormatado(despesas.get("vEconOrcAV"), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha13 = new ArrayList<>();
        cabecalhoLinha13.add(new TextoFormatado("Resultado Ex. Orçamentária:", formatacaoFactory.getBold(9)));
        cabecalhoLinha13.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha13.add(new TextoFormatado(despesas.get("vResExecOrcGeralPM"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha13.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha13.add(new TextoFormatado(despesas.get("vResExecOrcGeralPMAV"), formatacaoFactory.getBoldCenter(9)));

        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);
        dados.add(cabecalhoLinha5);
        dados.add(cabecalhoLinha6);
        dados.add(cabecalhoLinha7);
        dados.add(cabecalhoLinha8);
        dados.add(cabecalhoLinha9);
        dados.add(cabecalhoLinha10);
        dados.add(cabecalhoLinha11);
        dados.add(cabecalhoLinha12);
        dados.add(cabecalhoLinha13);

        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "18%", "18%", "12%", "12%"}, true));


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(7, 3);
        mergeH1.addToMergeHorizontal(new MergePosition(7, 4));

        MergePosition mergeH2 = new MergePosition(8, 3);
        mergeH2.addToMergeHorizontal(new MergePosition(8, 4));

        MergePosition mergeH3 = new MergePosition(9, 3);
        mergeH3.addToMergeHorizontal(new MergePosition(9, 4));

        MergePosition mergeH4 = new MergePosition(7, 0);
        mergeH4.addToMergeHorizontal(new MergePosition(7, 1));

        MergePosition mergeH5 = new MergePosition(9, 0);
        mergeH5.addToMergeHorizontal(new MergePosition(9, 1));

        MergePosition mergeV1 = new MergePosition(7, 3);
        mergeV1.addToMergeVertical(new MergePosition(8, 3));
        mergeV1.addToMergeVertical(new MergePosition(9, 3));
        mergeV1.addToMergeVertical(new MergePosition(10, 3));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);


    }

    private void addTabelaResultadoConsignado() {
        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("ITENS", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("EXERCÍCIOS", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado("2014", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado("2015", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado("2016", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("Aplicação na Educação - art. 212, Constituição Federal" +
                " (Limite mínimo de 25%)", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("FUNDEB aplicado no magistério " +
                "(Limite mínimo de 60%)", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha5 = new ArrayList<>();
        cabecalhoLinha5.add(new TextoFormatado("Recursos FUNDEB aplicados no exercício (incluindo diferimento de até 5%)",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha6 = new ArrayList<>();
        cabecalhoLinha6.add(new TextoFormatado("Aplicação na Saúde (Limite mínimo de 15%)",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha7 = new ArrayList<>();
        cabecalhoLinha7.add(new TextoFormatado("Execução Orçamentária - Prefeitura",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha8 = new ArrayList<>();
        cabecalhoLinha8.add(new TextoFormatado("Gerenciamento de Precatórios em ordem?",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha8.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha8.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha8.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha9 = new ArrayList<>();
        cabecalhoLinha9.add(new TextoFormatado("Recolhimentos previdenciários em ordem?",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha9.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha9.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha9.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha10 = new ArrayList<>();
        cabecalhoLinha10.add(new TextoFormatado("Regularidade nos repasses ao Legislativo?",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha10.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha10.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha10.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> cabecalhoLinha11 = new ArrayList<>();
        cabecalhoLinha11.add(new TextoFormatado("Despesas com Pessoal (Limite máximo de 54%)",
                formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha11.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha11.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha11.add(new TextoFormatado("", formatacaoFactory.getFormatacao(9)));

        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);
        dados.add(cabecalhoLinha5);
        dados.add(cabecalhoLinha6);
        dados.add(cabecalhoLinha7);
        dados.add(cabecalhoLinha8);
        dados.add(cabecalhoLinha9);
        dados.add(cabecalhoLinha10);
        dados.add(cabecalhoLinha11);

        XWPFTable tabela = addTabela(dados, tabela_67_11_11_11);


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeExercicio = new MergePosition(0, 1);
        mergeExercicio.addToMergeHorizontal(new MergePosition(0, 2));
        mergeExercicio.addToMergeHorizontal(new MergePosition(0, 3));

        MergePosition mergeItens = new MergePosition(0, 0);
        mergeItens.addToMergeVertical(new MergePosition(1, 0));

        mergePositions.add(mergeExercicio);
        mergePositions.add(mergeItens);

        mergeCells(tabela, mergePositions);
    }

    private void addTabelaResultadoExecucaoOrcamentariaPercentuais() {

        Map<String,String> resultado = audespResultadoExecucaoOrcamentariaMap;


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("Exercícios", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Resultado da execução orçamentária", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Percentual do resultado da execução orçamentária", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Percentual de investimento", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("2017", formatacaoFactory.getCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("superavitDeficit2017"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("vResExecOrcGeralPMAV2017"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha2.add(new TextoFormatado(valorInvestimentoMunicipioMap.get(exercicio-1), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("2016", formatacaoFactory.getCenter(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("superavitDeficit2016"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("vResExecOrcGeralPMAV2016"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha3.add(new TextoFormatado(valorInvestimentoMunicipioMap.get(exercicio-2), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("2015", formatacaoFactory.getCenter(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("superavitDeficit2015"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("vResExecOrcGeralPMAV2015"), formatacaoFactory.getCenter(9)));
        cabecalhoLinha4.add(new TextoFormatado(valorInvestimentoMunicipioMap.get(exercicio-3), formatacaoFactory.getCenter(9)));


        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);


        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela_20_30_30_20());


    }

    private void addTabelaResultadoFinanceiroEconomicoPatrimonial() {
        Map<String,String> resultado = audespResultadoExecucaoOrcamentaria;


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("Resultados", formatacaoFactory.getBold(9)));
        cabecalhoLinha1.add(new TextoFormatado("Exercício em exame", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("Exercício anterior", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("Financeiro", formatacaoFactory.getBold(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("vResultadoFinanceiro" + exercicio), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("vResultadoFinanceiro" + (exercicio - 1)), formatacaoFactory.getRight(9)));
        cabecalhoLinha2.add(new TextoFormatado(resultado.get("vResFinanceiroAH" + exercicio), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("Econômico", formatacaoFactory.getBold(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("vResultadoEconomico" + exercicio), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("vResultadoEconomico" + (exercicio-1)), formatacaoFactory.getRight(9)));
        cabecalhoLinha3.add(new TextoFormatado(resultado.get("vResEconomicoAH" + exercicio), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("Patrimonial", formatacaoFactory.getBold(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("vResultadoPatrimonial" + exercicio), formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("vResultadoPatrimonial" + (exercicio -1)), formatacaoFactory.getRight(9)));
        cabecalhoLinha4.add(new TextoFormatado(resultado.get("vResPatrimonialAH" + exercicio), formatacaoFactory.getCenter(9)));


        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);


        XWPFTable tabela = addTabela(dados, formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "30%", "30%", "20%"}, true));


    }

    private void addSecao(TextoFormatado textoFormatado, String heading) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle(heading);
        paragraph.setSpacingAfter(6 * 20);
        if (heading.equals(this.heading1)) {
            CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill("d9d9d9");
        }

        textoFormatado.setParagraphText(paragraph);
    }

    private void createDocumentStyles() {
        XWPFStyles styles = document.createStyles();


        addCustomHeadingStyle(styles, heading1, 1);
        addCustomHeadingStyle(styles, heading2, 2);
        addCustomHeadingStyle(styles, heading3, 3);
        addCustomHeadingStyle(styles, heading4, 4);
    }


    private void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("Arial");

        CTRPr rpr = CTRPr.Factory.newInstance();
//        rpr.setRFonts(fonts);

        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }

    public byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }

    private TextoFormatado addTab() {
        Formatacao formatacaoTab = formatacaoFactory.getTab();
        return new TextoFormatado("", formatacaoTab).concat("", formatacaoTab);
    }

    private String getQuadrimestreTituloComAno(Integer quadrimestre) {
        String quadrimestreTitulo = "1º/2º Quadrimestre de 2018";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre de 2018";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre de 2018";
        return quadrimestreTitulo;
    }

    private String getQuadrimestreTitulo(Integer quadrimestre) {
        String quadrimestreTitulo = "3º Quadrimestre";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre";
        return quadrimestreTitulo;
    }

    private Integer getMesReferencia(Integer quadrimestre) {
        Integer mesReferencia = 12;
        if (quadrimestre == 1)
            mesReferencia = 4;
        if (quadrimestre == 2)
            mesReferencia = 8;
        return mesReferencia;
    }

//    private void getDespesaPessoalPrimeiroQuadrimestre(XWPFDocument document, List<AudespDespesaPessoal> audespDespesaPessoalList) {
//        getParagrafo(document, "1º QUADRIMESTRE", true, "Courier New", 12, ParagraphAlignment.LEFT, false, true, false);
//
//        ParagraphAlignment[] paragraphAlignments = {ParagraphAlignment.LEFT, ParagraphAlignment.CENTER, ParagraphAlignment.CENTER, ParagraphAlignment.CENTER, ParagraphAlignment.CENTER};
//        String[] headers = {"Período", "Abr/2017", "Ago/2017", "Dez/2017", "Abr/2018"};
//        getDespesaPessoalQuadrimestreTabela(document, audespDespesaPessoalList, paragraphAlignments, headers);
//    }
//
//    private void getDespesaPessoalSegundoQuadrimestre(XWPFDocument document, List<AudespDespesaPessoal> audespDespesaPessoalList) {
//        ParagraphAlignment[] paragraphAlignments = {ParagraphAlignment.LEFT, ParagraphAlignment.CENTER, ParagraphAlignment.CENTER, ParagraphAlignment.CENTER, ParagraphAlignment.CENTER};
//
//        getParagrafo(document, "2º QUADRIMESTRE", true, "Courier New", 12, ParagraphAlignment.LEFT, false, true, false);
//
//        String[] headers = {"Período", "Ago/2017", "Dez/2017", "Abr/2018", "Ago/2018"};
//
//        getDespesaPessoalQuadrimestreTabela(document, audespDespesaPessoalList, paragraphAlignments, headers);
//    }
//
//    private void getDespesaPessoalQuadrimestreTabela(XWPFDocument document, List<AudespDespesaPessoal> audespDespesaPessoalList, ParagraphAlignment[] paragraphAlignments, String[] header12) {
//        String[][] itensTabela14 = new String[][]{};
//        String[] header13 = {"% Permitido Legal", "54,00%", "54,00%", "54,00%", "54,00%"};
//        String[][] itensTabela15 = new String[][]{{"Gasto Informado", audespDespesaPessoalList.get(4).getValorString(), audespDespesaPessoalList.get(5).getValorString(), audespDespesaPessoalList.get(6).getValorString(), audespDespesaPessoalList.get(7).getValorString()}, {"Inclusões da Fiscalização", "", "", "", ""}, {"Exclusões da Fiscalização", "", "", "", ""}, {"Gastos Ajustados", "", "", "", ""}};
//
//        String[] header14 = {"", "", "", "", ""};
//        String[][] itensTabela16 = new String[][]{{"Receita Corrente Líquida", audespDespesaPessoalList.get(0).getValorString(), audespDespesaPessoalList.get(1).getValorString(), audespDespesaPessoalList.get(2).getValorString(), audespDespesaPessoalList.get(3).getValorString()}, {"Inclusões da Fiscalização", "", "", "", ""}, {"Exclusões da Fiscalização", "", "", "", ""}, {"RCL Ajustada", "", "", "", ""}};
//
//        String[][] itensTabela17 = new String[][]{{"% Gasto Informado", audespDespesaPessoalList.get(8).getValorString(), audespDespesaPessoalList.get(9).getValorString(), audespDespesaPessoalList.get(10).getValorString(), audespDespesaPessoalList.get(11).getValorString()}};
//
//        String[][] itensTabela18 = new String[][]{{"% Gasto Ajustado", "", "", "", ""}};
//
//        getTabelaTipo2(document, header12, itensTabela14, "Calibri", 9, paragraphAlignments, true, "1000", TableWidthType.DXA);
//        getTabelaTipo2(document, header13, itensTabela15, "Calibri", 9, paragraphAlignments, true, "1000", TableWidthType.DXA);
//        getTabelaTipo2(document, header14, itensTabela16, "Calibri", 9, paragraphAlignments, true, "1000", TableWidthType.DXA);
//        getTabelaTipo2(document, header14, itensTabela17, "Calibri", 9, paragraphAlignments, true, "1000", TableWidthType.DXA);
//        getTabelaTipo2(document, header14, itensTabela18, "Calibri", 9, paragraphAlignments, true, "1000", TableWidthType.DXA);
//    }

    private void getTabela(XWPFDocument document, String[][] itensTabela, boolean isBold, String fontFamily, int fontSize) {
        XWPFTable table = document.createTable(0, 0);
        table.setWidthType(TableWidthType.PCT);
        table.setWidth("98%");
        table.removeBorders();

        for (int i = 0; i < itensTabela.length; i++) {
            if (table.getRow(i) == null)
                table.createRow();


            for (int x = 0; x < itensTabela[i].length; x++) {
                XWPFTableCell cell;
                // verifica se já existe a cell 0
                if (table.getRow(i).getCell(x) != null)
                    cell = table.getRow(i).getCell(x);
                else
                    cell = table.getRow(i).createCell();

                cell.removeParagraph(0);
                XWPFParagraph cellPar = cell.addParagraph();
                XWPFRun cellParRun = cellPar.createRun();
                cellParRun.setFontFamily(fontFamily);
                cellParRun.setText(itensTabela[i][x]);
                cellParRun.setBold(isBold);
                cellParRun.setFontSize(fontSize);
            }
        }



        /*for (int i = 0; i < quantidadeLinhas; i++) {
            XWPFTableRow linhaTabela = tableDadosIniciais.getRow(i);
            for (int j = 0; j < linhaTabela.getTableCells().size(); j++) {
                XWPFTableCell cell = linhaTabela.getCell(j);
                cell.removeParagraph(0);
                XWPFParagraph paragraph = cell.addParagraph();
                XWPFRun run = paragraph.createRun();
                run.setFontFamily("Courier New");
                run.setText("Linha " + i + " / Coluna" + j);
            }
        }*/
    }

    private void getHeader() throws InvalidFormatException, IOException {
        // Header
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

        CTP ctpFooter = CTP.Factory.newInstance();
        CTR ctrFooter = ctpFooter.addNewR();
        CTText ctFooter = ctrFooter.addNewT();
//        String footerText = "This is footer";
//        ctFooter.setStringValue(footerText);
        XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
        footerParagraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun footerRun = footerParagraph.createRun();
        footerRun.setFontFamily("Arial");
        footerRun.setFontSize(8);
        footerRun.getCTR().addNewPgNum();


        XWPFParagraph[] parsFooter = new XWPFParagraph[1];
        parsFooter[0] = footerParagraph;
        headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

        XWPFParagraph headerPar = header.createParagraph();
        XWPFRun headerParRun = headerPar.createRun();

        headerParRun.setFontFamily("Arial");
        headerParRun.setFontSize(12);
        headerPar.setAlignment(ParagraphAlignment.RIGHT);
//        headerParRun.setText("Fls. ");
//        headerParRun.getCTR().addNewPgNum();
//        headerParRun.addBreak();
//        headerParRun.setText("Processo nº " + tabelasProtocolo.getProcesso());
//
        XWPFTable headerTable = header.createTable(1, 3);
        headerTable.removeBorders();
        headerTable.setWidthType(TableWidthType.PCT);
        headerTable.setWidth("98%");

        XWPFTableRow headerTableRow = headerTable.getRow(0);

        //coluna 1
        headerTableRow.getCell(0).removeParagraph(0);
        XWPFParagraph pImgEsquerda = headerTableRow.getCell(0).addParagraph();
        pImgEsquerda.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgEsquerda = pImgEsquerda.createRun();

        Resource resourceBrasaoSP = resourceLoader.getResource("classpath:static/imagens/brasao_sp.png");

        InputStream brasao_sp = resourceBrasaoSP.getInputStream();
        runPImgEsquerda.addPicture(brasao_sp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_sp.png",
                Units.toEMU(50), Units.toEMU(55));

        //coluna 2
        headerTableRow.getCell(1).removeParagraph(0);
        XWPFParagraph p1 = headerTableRow.getCell(1).addParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p1Run = p1.createRun();

        p1Run.setFontFamily("Arial");
        p1Run.setFontSize(12);
        p1Run.setBold(true);
        p1Run.setText("TRIBUNAL DE CONTAS DO ESTADO DE SÃO PAULO");

        XWPFParagraph p2 = headerTableRow.getCell(1).addParagraph();
        p2.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p2Run = p2.createRun();
        p2Run.setFontSize(12);
        p2Run.setFontFamily("Arial");
        p2Run.setText(tabelasProtocolo.getDescricaoArea());

        //coluna 3
        headerTableRow.getCell(2).removeParagraph(0);
        XWPFParagraph pImgDireita = headerTableRow.getCell(2).addParagraph();
        pImgDireita.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgDireita = pImgDireita.createRun();
        Resource resourceBrasaoTcesp = resourceLoader.getResource("classpath:static/imagens/brasao_tcesp.png");

        InputStream brasao_tcesp = resourceBrasaoTcesp.getInputStream();
        runPImgDireita.addPicture(brasao_tcesp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_tcesp.png", Units.toEMU(50), Units.toEMU(55));
    }

    private void addTituloTabelaODS(String logo, String texto) throws InvalidFormatException, IOException {

        XWPFTable table = document.createTable(1, 2);
        table.removeBorders();
        table.setWidthType(TableWidthType.PCT);
        table.setWidth("98%");

        XWPFTableRow tableRow = table.getRow(0);

        //coluna 1
        tableRow.getCell(0).removeParagraph(0);
        XWPFParagraph pImgEsquerda = tableRow.getCell(0).addParagraph();
        pImgEsquerda.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgEsquerda = pImgEsquerda.createRun();

        Resource resourceLogo = resourceLoader.getResource("classpath:static/imagens/ods/apendice/" + logo);

        InputStream logoODS = resourceLogo.getInputStream();
        runPImgEsquerda.addPicture(logoODS, XWPFDocument.PICTURE_TYPE_PNG, logo,
                Units.toEMU(50), Units.toEMU(50));

        //coluna 2
        tableRow.getCell(1).removeParagraph(0);
        tableRow.getCell(1).setVerticalAlignment(XWPFTableCell.XWPFVertAlign.BOTH);
        XWPFParagraph p1 = tableRow.getCell(1).addParagraph();
        p1.setAlignment(ParagraphAlignment.BOTH);
        XWPFRun p1Run = p1.createRun();

        p1Run.setFontFamily("Arial");
        p1Run.setFontSize(11);
        p1Run.setBold(true);
        p1Run.setText(texto);
    }

    private XWPFParagraph getTitulo(XWPFDocument document, String quadrimestre) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();
        run.setBold(true);
        run.setFontFamily("Arial");
        run.setFontSize(12);
        run.setText("RELATÓRIO DE FISCALIZAÇÃO\nPREFEITURA MUNICIPAL");
        return paragraph;
    }

    private XWPFParagraph getParagrafo(XWPFDocument document, String texto, boolean isBold, String fontFamily, int fontSize, ParagraphAlignment paragraphAlignment, boolean haveTab, boolean yellowRed, boolean underLine) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(paragraphAlignment);
        XWPFRun run = paragraph.createRun();
        if (haveTab)
            run.addTab();
        run.setBold(isBold);
        run.setFontFamily(fontFamily);
        run.setFontSize(fontSize);
        run.setText(texto);

        if (yellowRed) {
            printYellowRed(run);
            /*run.setColor("ff0000");
            run.setTextHighlightColor("yellow");
            run.setItalic(true);*/
        }

        if (underLine)
            underLine(run);

        return paragraph;
    }

    private void printYellowRed(XWPFRun run) {
        run.setColor("ff0000");
        run.setTextHighlightColor("yellow");
        run.setItalic(true);
    }

    private void underLine(XWPFRun run) {
        run.setUnderline(UnderlinePatterns.SINGLE);
    }

    private XWPFRun addToParagrafoBreak(XWPFParagraph paragrafo, String texto) {
        XWPFRun run = paragrafo.getRuns().get(0);
        run.addBreak();
        run.setText(texto);
        return run;
    }

    private void addToParagrafoRed(XWPFParagraph paragrafo, String texto, String fontStyle, int fontSize) {
        XWPFRun run = paragrafo.createRun();
        run.setText(texto);
        run.setColor("ff0000");
        run.setFontFamily(fontStyle);
        run.setFontSize(fontSize);
        run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
    }

    private XWPFParagraph getParagrafoGrayHeader(XWPFDocument document, String texto, boolean isBold, String fontFamily, int fontSize, ParagraphAlignment paragraphAlignment, boolean haveTab) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(paragraphAlignment);
        XWPFRun run = paragraph.createRun();
        if (haveTab)
            run.addTab();

        run.setBold(isBold);
        run.setFontFamily(fontFamily);
        run.setFontSize(fontSize);
        run.setText(texto);

        // shading
        //run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
        //run.getCTR().addNewRPr().addNewShd();
        CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
        cTShd.setVal(STShd.CLEAR);
        cTShd.setFill("d9d9d9");

        return paragraph;
    }


    //////////////////////
    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();

//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());

        paragraph.setSpacingAfter(6 * 20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafoSpacingAfter(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();
//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());
//        paragraph.setSpacingAfter(6*20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado, boolean breakPage) {
        XWPFParagraph paragraph = addParagrafo(textoFormatado);
        paragraph.setPageBreak(breakPage);
        return paragraph;
    }

    private XWPFTable addTabela(List<List<TextoFormatado>> dados, FormatacaoTabela formatacaoTabela) {

        int qdtLinhas = dados.size();
        int qtdColunas = 0;

        for (int i = 0; i < dados.size(); i++) {
            if (qtdColunas < dados.get(i).size())
                qtdColunas = dados.get(i).size();
        }

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        formatacaoTabela.formatarTabela(tabela);

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
//            row.setHeight((int) (twipsPerInch * 1 / 5)); //set height 1/10 inch.
//            row.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact"
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }

            for (int j = 0; j < dados.get(i).size(); j++) {
                XWPFTableCell cell = j == 0 ? row.getCell(j) : row.createCell();
                cell.setWidth(formatacaoTabela.getWidths().get(j));
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                if (i == 0 && formatacaoTabela.isFirstLineHeader())
                    cell.getCTTc().addNewTcPr().addNewShd().setFill("cccccc");

                XWPFRun run = cell.getParagraphs().get(0).createRun();

                XWPFParagraph paragrafo = cell.getParagraphs().get(0);
                paragrafo.setSpacingBetween(1);

                dados.get(i).get(j).setParagraphText(paragrafo);

            }

        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable addTabelaColunaUnica(List<TextoFormatado> dados, FormatacaoTabela formatacaoTabela) {

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        for(int k = 1; k < tabela.getRows().size(); k++ ) {
            tabela.removeRow(k);
        }

        formatacaoTabela.formatarTabela(tabela);

        if(dados.size() == 0) {
            return tabela;
        }

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }
            XWPFTableCell cell = null;

            if(row.getTableCells().size() == 0)
                cell = row.createCell();
            else
                cell = row.getCell(0);

            cell.setWidth("100%");
            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            if (i % 2 == 0 )
                cell.getCTTc().addNewTcPr().addNewShd().setFill("dce6f1");

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            XWPFParagraph paragrafo = cell.getParagraphs().get(0);
            paragrafo.setSpacingBetween(1);

            dados.get(i).setParagraphText(paragrafo);
        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable mergeCells(XWPFTable tabela, List<MergePosition> cellsToMerge) {
        tabela = mergeCellsHorizontal(tabela, cellsToMerge);
        tabela = mergeCellsVertical(tabela, cellsToMerge);
        return tabela;

    }

    private void addListaNumerada(List<TextoFormatado> listItens) {

        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
        //Next we set the AbstractNumId. This requires care.
        //Since we are in a new document we can start numbering from 0.
        //But if we have an existing document, we must determine the next free number first.
        cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));

        /* Bullet list
          CTLvl cTLvl = cTAbstractNum.addNewLvl();
          cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
          cTLvl.addNewLvlText().setVal("•");
        */

        ///* Decimal list
        CTLvl cTLvl = cTAbstractNum.addNewLvl();
        cTLvl.addNewNumFmt().setVal(STNumberFormat.DECIMAL);
        cTLvl.addNewPPr();
        CTInd ind = cTLvl.getPPr().addNewInd(); //Set the indent

        ind.setHanging(BigInteger.valueOf(360*2));
        ind.setLeft(BigInteger.valueOf(360*6));
        cTLvl.addNewLvlText().setVal("%1.");
        cTLvl.addNewStart().setVal(BigInteger.valueOf(1));
        //*/

        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

        XWPFNumbering numbering = document.createNumbering();

        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

        BigInteger numID = numbering.addNum(abstractNumID);

        for (TextoFormatado item : listItens) {
            item.setListNumId(numID);
            addParagrafo(item);
        }
    }

    private XWPFTable mergeCellsHorizontal(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTHMerge hMerge = CTHMerge.Factory.newInstance();
            hMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setHMerge(hMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeHorizontal().size(); j++) {
                CTHMerge hToMerge = CTHMerge.Factory.newInstance();
                hToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeHorizontal().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeHorizontal().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setHMerge(hToMerge);
            }
        }

        return tabela;
    }

    private XWPFTable mergeCellsVertical(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTVMerge vMerge = CTVMerge.Factory.newInstance();
            vMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setVMerge(vMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeVertical().size(); j++) {
                CTVMerge vToMerge = CTVMerge.Factory.newInstance();
                vToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeVertical().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeVertical().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setVMerge(vToMerge);
            }
        }

        return tabela;
    }

    private void addBreak() {
        this.document.createParagraph();//.createRun().addBreak();
    }

    private void addPageBreak() {
        XWPFParagraph paragraph = this.document.createParagraph();
        paragraph.setPageBreak(true);
    }

    public void addSaudacao() {
//        if(secaoFiscalizadoraContas != null && Character.isDigit(secaoFiscalizadoraContas.charAt(0))){
//            return "Senhor(a) Diretor(a) da "+ getDescricaoArea() + ",";
//        }
//        else{
//            return "Senhor(a) Diretor(a) da " + getDescricaoArea() + ",";
//        }
        addParagrafo(new TextoFormatado(tabelasProtocolo.getSaudacao(), formatacaoFactory.getBold(12)));
    }

    private void addAnaliseFontesDocumentais() {
        List<TextoFormatado> itensLista = new ArrayList<>();
        itensLista.add(new TextoFormatado("Indicadores finalísticos componentes do IEG-M – Índice de" +
                " Efetividade da Gestão Municipal;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Ações fiscalizatórias desenvolvidas através da seletividade " +
                "(contratos e repasses) e da fiscalização ordenada; ", formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Prestações de contas mensais do exercício em exame, encaminhadas pela " +
                "Chefia do Poder Executivo;", formatacaoFactory.getFormatacao(12)));

        itensLista.add(new TextoFormatado("Resultado do acompanhamento simultâneo do Sistema Audesp, bem como " +
                "acesso aos dados, informações e análises " +
                "disponíveis no referido ambiente;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das denúncias, representações e expedientes diversos; ",
                formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Leitura analítica dos três últimos relatórios de fiscalização e" +
                " respectivas decisões desta Corte, sobretudo no tocante a assuntos relevantes nas ressalvas, " +
                "advertências e recomendações;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das informações disponíveis nos demais sistemas" +
                " do e. Tribunal de Contas do Estado.", formatacaoFactory.getFormatacao(12)));

        addListaNumerada(itensLista);
    }


    public ResponseEntity<Resource> download(Integer codigoIBGE, Integer exercicio, Integer quadrimestre)
            throws IOException, InvalidFormatException, Exception {

        this.document = new XWPFDocument();

        this.exercicio = exercicio;
        this.codigoIBGE = codigoIBGE;

        ParametroBusca parametroBusca = new ParametroBusca();
        parametroBusca.setCodigoIBGE(codigoIBGE);
        parametroBusca.setExercicio(exercicio);
        parametroBusca.setMesReferencia(12);
        parametroBusca.setTipoEntidadeId(50);

        this.pareceresPrefeiturasList = this.parecerPrefeituraService.getParecerByCodigoIbge(codigoIBGE);
        this.audespEntidade = this.audespEntidadeService.getEntidade(codigoIBGE,exercicio);
        this.apontamentosODS = apontamentosODSService.getApontamentosODS(codigoIBGE, exercicio);
        this.notasIegm = iegmService.getMapNotasByCodigoIbge(codigoIBGE, exercicio);
        this.resultadoIegm2018 = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio);
        this.responsavelPrefeitura = audespService.getResponsavelPrefeitura(codigoIBGE,exercicio, 3);
        this.responsavelSubstitutoPrefeitura = audespService.getResponsavelSubstitutoPrefeitura(codigoIBGE,exercicio, 3);
        this.municipioIegmCodigoIbge = iegmService.getMunicipioByCodigoIbge(codigoIBGE);
        this.audespResultadoExecucaoOrcamentaria = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaFormatado(parametroBusca);
        this.tabelasProtocolo = tabelasService.getTabelasProtocoloPrefeituraByCodigoIbge(codigoIBGE, exercicio);
        this.anexo14AMap = this.demonstrativosRaeeService.getAnexo14A(audespEntidade, codigoIBGE, exercicio, 12);
        this.audespEnsinoFundeb = audespEnsinoService.getAudespEnsinoFundeb(codigoIBGE, exercicio, 12);
        this.audespSaude = audespSaudeService.getAudespSaude(codigoIBGE, exercicio, 12);
        this.aplicacoesEmSaude = audespSaudeService.getAplicacoesEmSaudeFormatado(codigoIBGE, exercicio, 12);
        this.audespDespesaPessoalMap = this.audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbgeFechamentoFormatado(codigoIBGE, exercicio, 50);
        this.quadroGeralEnsinoMap = this.audespEnsinoService.getQuadroGeralEnsinoFormatado(codigoIBGE,exercicio,12);
        this.apontamentoFOMap = this.apontamentosFOService.getApontamentosFO(codigoIBGE,exercicio);
        this.audespDividaAtivaMap = this.audespDividaAtivaService.getDividaAtivaFormatado(codigoIBGE,exercicio,12);
        this.audespFase3QuadroDePessoalMap = this.audespFase3Service.getQuadroDePessoal(audespEntidade.getEntidadeId(), exercicio);
        this.valorInvestimentoMunicipioMap = this.audespBiService.getValorInvestimentoMunicipio(codigoIBGE,exercicio, 12);
        this.rclMunicipioDevedoresMap = this.tcespBiService.getRCLMunicipioFormatado(codigoIBGE, exercicio);
        Integer quantidadeAlertasDesajusteExecucaoOrcamentaria = audespAlertasService.getAlertasDesajusteExecucaoOrcamentaria(codigoIBGE, exercicio, 12);
        Integer quantidadeAlertasDespesaPessoal = audespAlertasService.getAlertasDespesaComPessoal(codigoIBGE, exercicio, 12);
        this.audespResultadoExecucaoOrcamentariaMap = audespResultadoExecucaoOrcamentariaService
        .getAudespResultadoExecucaoOrcamentariaUltimosTresExercicios(this.codigoIBGE, exercicio, 12);

//        this.notasIegm = iegmService.getMapNotasByCodigoIbge(codigoIBGE, exercicio);

//        Resource r = resourceLoader.getResource("classpath:static/EmbeddedDocument.docx");

        //InputStream is = r.getInputStream();

        //UpdateEmbeddedDoc ued = new UpdateEmbeddedDoc();
        //ued.setDoc(docx);
        //ued.updateEmbeddedDoc();
//        ued.checkUpdatedDoc();

//        XWPFDocument doc2 = new XWPFDocument();
//        XWPFParagraph ppppp = doc2.createParagraph();
//        ppppp.createRun().setText("DOC2");
//
//
//        XWPFParagraph  aaaaa = doc2.getParagraphs().get(0);
//        document.setParagraph(aaaaa, 0);
//        Integer mesReferencia = getMesReferencia(quadrimestre);
//
        String quadrimestreTitulo = getQuadrimestreTitulo(quadrimestre);
//
        String quadrimestreTituloAno = getQuadrimestreTituloComAno(quadrimestre);


        // margin
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        CTPageMar pageMar = sectPr.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(1700L));
        //pageMar.setTop(BigInteger.valueOf(1440L));
        //pageMar.setRight(BigInteger.valueOf(1680L));
        pageMar.setRight(BigInteger.valueOf(1700L));
        pageMar.setBottom(BigInteger.valueOf(1700L));
        //pageMar.setBottom(BigInteger.valueOf(1440L));

        CTBody body = document.getDocument().getBody();

        if (!body.isSetSectPr()) {
            body.addNewSectPr();
        }
        CTSectPr section = body.getSectPr();

        if(!section.isSetPgSz()) {
            section.addNewPgSz();
        }
        CTPageSz pageSize = section.getPgSz();

        pageSize.setW(BigInteger.valueOf(595*20));
        pageSize.setH(BigInteger.valueOf(842*20));


        createDocumentStyles();

        getHeader();

        addParagrafo(new TextoFormatado("RELATÓRIO DE FISCALIZAÇÃO\nPREFEITURA MUNICIPAL", formatacaoFactory.getBoldCenter(12)));
        addBreak();
        addBreak();

        addTabelaDadosIniciais();

        addBreak();
        addSaudacao();
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("PREÂMBULO – APLICÁVEL A TODOS OS CASOS (FISCALIZAÇÃO ORDINÁRIA NORMAL " +
                "OU VALIDAÇÃO E ACOMPANHAMENTO NORMAL OU VALIDAÇÃO)",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Trata-se das contas apresentadas em face do art. 2º, II, da Lei Complementar Estadual nº 709, de" +
                        " 14 de janeiro de 1993 (Lei Orgânica do Tribunal de Contas do Estado de São Paulo).",
                formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("Em atendimento ao TC-A-30973/026/00, registramos a notificação do(s) Sr.(s). ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", responsável(is) pelas contas em exame.", formatacaoFactory.getJustificado(12))
        );
        addParagrafo(addTab().concat("Consignamos os dados e índices considerados relevantes para um diagnóstico inicial do município:",
                formatacaoFactory.getJustificado(12)));

        addTabelaDescricaoFonteDado();

        addBreak();

        addParagrafo(new TextoFormatado("*POPULAÇÃO: buscar a última informação da população estimada para o exercício em exame no site do IEG-M ",
                boldItalicVermelhoAmareloJustificado12)
                .concat("http://iegm.tce.sp.gov.br/", formatacaoFactory.getBoldItalicUnderlineAzulJustificado(12))
                .concat(" ou no site do IBGE", boldItalicVermelhoAmareloJustificado12)
                .concat("https://cidades.ibge.gov.br/", formatacaoFactory.getBoldItalicUnderlineAzulJustificado(12))
                .concat(" , se disponível.", boldItalicVermelhoAmareloJustificado12));


        addBreak();
        addParagrafo(addTab().concat("Informamos que o município possui a seguinte série histórica de " +
                        "classificação no Índice de Efetividade da Gestão Municipal (IEG-M):",
                formatacaoFactory.getJustificado(12)));

        addTabelaIegm();
        addTextoStatusFaseIEGM();

        addBreak();
        addParagrafo(new TextoFormatado("O IEG-M INSERIDO NO EXERCÍCIO EM EXAME SERÁ AQUELE APURADO APÓS A " +
                "VERIFICAÇÃO/VALIDAÇÃO DA FISCALIZAÇÃO.",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO PODE APENAS MENCIONAR OS ÍNDICES EM RELATÓRIO, " +
                "SENDO VEDADA A DIVULGAÇÃO AOS JURISDICIONADOS DOS ÍNDICES AINDA NÃO CHANCELADOS E TORNADOS " +
                "PÚBLICOS PELA DIREÇÃO DA CASA.", boldItalicVermelhoAmareloJustificado12));

        addParagrafo(addTab().concat("A Prefeitura analisada obteve, nos 03 (três) últimos exercícios" +
                        " apreciados, os seguintes ",
                formatacaoFactory.getJustificado(12))
                .concat("PARECERES", formatacaoFactory.getBold(12))
                .concat(" na apreciação de suas contas:", formatacaoFactory.getBold(12))
        );

        addTabelaPareceres();

        addBreak();
        addParagrafo(addTab().concat("A partir de tais premissas, a Fiscalização planejou a execução de seus " +
                        "trabalhos, agregando a análise das seguintes fontes documentais:",
                formatacaoFactory.getJustificado(12)));

        addAnaliseFontesDocumentais();

        addBreak();

        //############################################
        //# DETALHES PARECERES EXERCICIOS ANTERIORES #
        //############################################


        TextoFormatado hipotesePMsNaoValidacao = new TextoFormatado("HIPÓTESE", boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(" UTILIZAR O TEXTO ADIANTE PARA PM’S SOB FISCALIZAÇÃO ", boldItalicCapsVermelhoAmareloJustificado12)
                .concat("ORDINÁRIA ", boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("DE CONTAS ANUAIS, QUE ", boldItalicCapsVermelhoAmareloJustificado12)
                .concat("NÃO ESTÃO SOB VALIDAÇÃO", boldItalicUnderlineCapsVermelhoAmareloJustificado12);

        addParagrafo(hipotesePMsNaoValidacao);

        addBreak();


        TextoFormatado resultadoInLoco = addTab().concat("O resultado da fiscalização in loco apresenta-se " +
                        "neste Relatório, antecedido pelo citado planejamento que indicou a necessária extensão dos exames.",
                formatacaoFactory.getJustificado(12));
        addParagrafo(resultadoInLoco);

        addBreak();

        TextoFormatado hipotesePMsSobValidacao = new TextoFormatado("HIPÓTESE", boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(" UTILIZAR O TEXTO ADIANTE PARA PM’S SOB FISCALIZAÇÃO ", boldItalicCapsVermelhoAmareloJustificado12)
                .concat("ORDINÁRIA ", boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("DE CONTAS ANUAIS, QUE ", boldItalicCapsVermelhoAmareloJustificado12)
                .concat("ESTÃO SOB VALIDAÇÃO", boldItalicUnderlineCapsVermelhoAmareloJustificado12);
        addParagrafo(hipotesePMsSobValidacao);

        addBreak();

        TextoFormatado nosCasosDeValidacao = new TextoFormatado("Nos casos de ", boldItalicVermelhoAmareloJustificado12)
                .concat("validação", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(", caberá aos Agentes da Fiscalização em conjunto com os Chefes Técnicos da Fiscalização e Diretores de Fiscalização," +
                        " planejarem seus trabalhos de forma seletiva tendo em vista o regramento previsto no Art. 7º da Resolução nº 04/2017." +
                        " Lembrando que foram estabelecidos critérios de seleção para tais municípios com redução do tempo de trabalho “in loco” o" +
                        " que requer racionalização dos procedimentos e ajustes na extensão de suas análises." +
                        "\n\nEm função do critério de seletividade acima mencionado, a Fiscalização poderá não selecionar" +
                        " itens não considerados relevantes para o município sob fiscalização e que não comprometam o juízo para emissão de parecer" +
                        " pelos E. Conselheiros Relatores.");
        addParagrafo(nosCasosDeValidacao);

        addBreak();

        TextoFormatado prefeituraMunicipalDenota = addTab()
                .concat("A Prefeitura Municipal denota boa ordem, considerando que obteve, nos" +
                        " 03 (três) últimos exercícios apreciados, ", formatacao12)
                .concat("PARECERES FAVORÁVEIS", formatacaoBold12)
                .concat(", assim como à vista dos resultados consignados no quadro abaixo:", formatacaoFactory.getJustificado(12));
        addParagrafo(prefeituraMunicipalDenota);

        addBreak();

        addTabelaResultadoConsignado();

        addBreak();

        TextoFormatado oConjuntoDeInformacoes = addTab()
                .concat("O conjunto de informações retro transcritas", formatacaoFactory.getJustificado(12))
                .concat("1", formatacaoFactory.getFormatacaoSobrescrito(12))
                .concat(", bem como o volume das receitas arrecadadas pela Prefeitura Municipal" +
                                " permitiram optar, com amparo no regramento previsto no art. 7º da Resolução" +
                                " nº 04/2017, pela realização de um procedimento fiscalizatório seletivo.",
                        formatacaoFactory.getJustificado(12));

        addParagrafo(oConjuntoDeInformacoes);


        TextoFormatado combaseNoPermissivo = addTab()
                .concat("Com base no permissivo previsto no TC-A-39.686/026/15, apresentam-se os resultados " +
                        "considerados essenciais para emissão do parecer, bem como outros detectados no" +
                        " transcorrer da fiscalização ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(", os quais seguem transcritos neste relatório.", formatacaoFactory.getJustificado(12));


        addParagrafo(combaseNoPermissivo);
        addBreak();

        TextoFormatado hipoteseAindaQueValidacao = new TextoFormatado("HIPÓTESE", boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(" UTILIZAR O TEXTO ADIANTE PARA FECHAMENTO DE PM’S SOB ACOMPANHAMENTO DE CONTAS ANUAIS," +
                        " AINDA QUE SOB VALIDAÇÃO ", boldItalicCapsVermelhoAmareloJustificado12);
        addParagrafo(hipoteseAindaQueValidacao);

        addBreak();

        addParagrafo(nosCasosDeValidacao);

        addBreak();

        TextoFormatado osResultadosDasFiscalizacoes = addTab()
                .concat("Os resultados das fiscalizações ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" apresentam-se nos relatórios quadrimestrais e no presente (fechamento do exercício), antecedidos " +
                        "pelo citado planejamento que indicou a necessária extensão dos exames.", formatacaoFactory.getJustificado(12));
        addParagrafo(osResultadosDasFiscalizacoes);

        TextoFormatado destaqueSeQueOsRelatorios =
                addTab().concat("Destaque-se que os relatórios quadrimestrais estão juntados nos eventos nº xx e xx destes" +
                        " autos. Estes foram submetidos a Excelentíssima Relatoria, sendo dada ciência à Chefia do " +
                        "Poder Executivo, responsável pelas contas em exame, para conhecimento dos apontamentos, sem " +
                        "a necessidade de apresentação de justificativas. Tal procedimento visou contribuir para a " +
                        "tomada de providências dentro do próprio exercício, possibilitando a correção de eventuais " +
                        "falhas, resultando numa melhoria das contas apresentadas. ", formatacaoFactory.getJustificado(12));

        addParagrafo(destaqueSeQueOsRelatorios);

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA A: PLANEJAMENTO", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addSecao(new TextoFormatado("A.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addSecao(new TextoFormatado("A.1.1. CONTROLE INTERNO",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();

        TextoFormatado aquiSeraoTrazidas = new TextoFormatado("AQUI SERÃO TRAZIDAS CONSTATAÇÕES RELEVANTES" +
                " SOBRE O CONTROLE INTERNO E SUAS ATRIBUIÇÕES", boldItalicCapsVermelhoAmareloJustificado12);
        addParagrafo(aquiSeraoTrazidas);
        addBreak();

        TextoFormatado buscarAferir = new TextoFormatado("BUSCAR AFERIR SE O CONTROLE INTERNO TÊM EXERCIDO " +
                "DE MANEIRA EFETIVA SUAS ATRIBUIÇÕES NO PERÍODO", boldItalicCapsVermelhoAmareloJustificado12);
        addParagrafo(buscarAferir);
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("A.2. IEG-M – I-PLANEJAMENTO – Índice " + resultadoIegm2018.getFaixaIPlanejamento(),
                formatacaoFactory.getBold(12)), heading2);
        addBreak();


        TextoFormatado verOrientacoes = new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo.",
                boldItalicVermelhoAmareloJustificado12);
        addParagrafo(verOrientacoes);
        addBreak();

        TextoFormatado hipoteseSeNaoForem = new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        boldItalicCapsVermelhoAmareloJustificado12);
        addParagrafo(hipoteseSeNaoForem);
        addBreak();

        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa " +
                "dimensão do IEG-M.", formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();

        addApontamentosODS("i-planejamento");
        addBreak();
        addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice III - ODS",
                formatacaoFactory.getJustificado(12)));
        addBreak();


//        apontamentoODSPlanejamento.forEach( (apontamento) -> {
//
//            addParagrafo(addTab().concat(apontamento.getOdsApontamento(),
//                    formatacaoFactory.getJustificado(12)));
//        });


        addSecao(new TextoFormatado("PERSPECTIVA B: GESTÃO FISCAL", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addSecao(new TextoFormatado("B.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(addTab().concat("Face ao contido no art. 1º, § 1º da Lei Complementar Federal nº 101, de " +
                        "4 de maio de 2000 (Lei de Responsabilidade Fiscal), o qual estabelece os pressupostos da " +
                        "responsabilidade da gestão fiscal, passamos a expor o que segue. ",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addSecao(new TextoFormatado("B.1.1. RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": SUPERÁVIT DA EXECUÇÃO ORÇAMENTÁRIA:",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("Com base nos dados gerados pelo Sistema Audesp, conforme abaixo apurado," +
                " o resultado da execução orçamentária da Prefeitura evidenciou ", formatacaoFactory.getJustificado(12))
                .concat("superávit.", formatacaoFactory.getItalic(12)));


        addTabelaExecucaoOrcamentaria();

        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO: CONSIDERAR OS DADOS ISOLADOS DA PREFEITURA, CONFORME APURADO" +
                " PELO SISTEMA Audesp, NÃO DEVENDO SER INCLUÍDAS ADMININISTRAÇÃO INDIRETA, FUNDOS PREVIDÊNCIA ETC.",
                boldItalicUnderlineVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("DEPENDE DE SINAL (+ OU -) APENAS A LINHA DE “AJUSTES DA FISCALIZAÇÃO”.",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": DÉFICIT DA EXECUÇÃO ORÇAMENTÁRIA AMPARADO NO SUPERÁVIT FINANCEIRO DO EXERCÍCIO ANTERIOR:",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Com base nos dados gerados pelo Sistema Audesp, conforme abaixo apurado," +
                " o resultado da execução orçamentária da Prefeitura evidenciou ", formatacaoFactory.getJustificado(12))
                .concat("déficit ", formatacaoFactory.getItalic(12))
                .concat("que se encontrou totalmente ", formatacaoFactory.getJustificado(12))
                .concat("amparado ", formatacaoFactory.getBold(12))
                .concat("no ", formatacaoFactory.getJustificado(12))
                .concat("superávit ", formatacaoFactory.getItalic(12))
                .concat("financeiro proveniente do exercício anterior, consoante item seguinte deste Relatório.",
                        formatacaoFactory.getJustificado(12)));
        addBreak();
        addTabelaExecucaoOrcamentaria();
        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO: CONSIDERAR OS DADOS ISOLADOS DA PREFEITURA, CONFORME APURADO" +
                " PELO SISTEMA Audesp, NÃO DEVENDO SER INCLUÍDAS ADMININISTRAÇÃO INDIRETA, FUNDOS PREVIDÊNCIA ETC.",
                boldItalicUnderlineVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("DEPENDE DE SINAL (+ OU -) APENAS A LINHA DE “AJUSTES DA FISCALIZAÇÃO”.",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": CASO OCORRA DÉFICIT DA EXECUÇÃO ORÇAMENTÁRIA NÃO AMPARADO, EM SUA TOTALIDADE, POR " +
                                "SUPERÁVIT FINANCEIRO DO EXERCÍCIO ANTERIOR:",
                        boldItalicCapsVermelhoAmareloJustificado12));


        addBreak();

        addTabelaReceitas_AV_AH();
        addTabelaDespesas_AV_AH();


        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES:", boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES/DEDUÇÕES/AJUSTES",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": RESULTADO DEFICITÁRIO E INSUFICIENTE SUPERÁVIT FINANCEIRO DO ANO ANTERIOR ",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("O déficit da execução orçamentária não está totalmente amparado " +
                "pelo superávit financeiro do ano anterior.", formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("Tal déficit provém da superestimativa de receita, visto que a arrecadação foi ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% inferior à previsão.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("CONSIDERAR O PERCENTUAL DA ANÁLISE HORIZONTAL (AH) ",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, I, da Lei de Responsabilidade Fiscal, " +
                        "o Município foi alertado tempestivamente, por ",
                formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("" + quantidadeAlertasDesajusteExecucaoOrcamentaria +" vezes, sobre desajustes em sua execução orçamentária.", formatacaoFactory.getJustificado(12))
        );
        addParagrafo(new TextoFormatado("OBSERVAR A ", boldItalicVermelhoAmareloJustificado12)
                .concat("DATA DE EMISSÃO DO ALERTA", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(" (CONSTANTE NO FINAL DO DOCUMENTO “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, " +
                        "VISTO QUE, NORMALMENTE OS DO FINAL DO EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, " +
                        "SEM EFEITO. FACE À ANÁLISE REALIZADA PELO SISTEMA Audesp, EM REGRA CONSIDERAR APENAS AS" +
                        " ANÁLISES DA RECEITA E DESPESA.", boldItalicVermelhoAmareloJustificado12)
        );
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                        boldItalicUnderlineVermelhoAmareloJustificado12)
                        .concat("RESULTADO DEFICITÁRIO E DÉFICIT FINANCEIRO DO ANO ANTERIOR",
                                boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("O déficit da execução orçamentária aumentou o déficit financeiro do ano " +
                        "anterior, consoante detalhado no item seguinte.",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("Tal déficit provém da superestimativa de receita, visto que a arrecadação foi ",
                        formatacaoFactory.getJustificado(12))
                        .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat("% inferior à previsão.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("CONSIDERAR O PERCENTUAL DA ANÁLISE HORIZONTAL (AH)",
                boldItalicVermelhoAmareloJustificado12));

        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, I, da Lei de Responsabilidade Fiscal, o " +
                        "Município foi alertado tempestivamente, por  ",
                        formatacaoFactory.getJustificado(12))
                        .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat("" + quantidadeAlertasDesajusteExecucaoOrcamentaria + " vezes, sobre desajustes em sua execução orçamentária.", formatacaoFactory.getJustificado(12))
                );

        addParagrafo(new TextoFormatado("OBSERVAR A DATA DE EMISSÃO DO ALERTA (CONSTANTE NO FINAL DO " +
                "DOCUMENTO “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE " +
                "OS DO FINAL DO EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO. FACE À ANÁLISE " +
                "REALIZADA PELO SISTEMA Audesp, EM REGRA CONSIDERAR APENAS AS ANÁLISES DA RECEITA E DESPESA .",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": OBSERVAR O CONTIDO NO COMUNICADO SDG Nº 18/2015 QUE TRATA DAS ALTERAÇÕES NA EXECUÇÃO" +
                                " ORÇAMENTÁRIA. CASO HAJA A NECESSIDADE, INSERIR ASPECTOS APLICÁVEIS, CONFORME PARÁGRAFO SEGUINTE.",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Constatamos que o Município, considerando todos os órgãos componentes do" +
                        " Orçamento Anual, procedeu à abertura de créditos adicionais e a realização de transferências," +
                        " remanejamentos e/ou transposições no valor total de R$ ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", o que corresponde a ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da Despesa Fixada (inicial).", formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("CONSIDERAR O MUNICÍPIO, VISTO QUE AO EXECUTIVO CABE A RESPONSABILIDADE " +
                "PELA EXECUÇÃO DO ORÇAMENTO DE TODO ENTE. A ANÁLISE TRAZIDA PELO Audesp E GERALMENTE APRESENTADA PELOS " +
                "ÓRGÃOS, É COMPOSTA POR TODO O MUNICÍPIO, JÁ QUE A BASE É A LOA.",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addParagrafo(new TextoFormatado("SE CONSTATADA ALTERAÇÃO ORÇAMENTÁRIA SIGNIFICATIVA, APONTADA NESTE " +
                "ITEM, DEVEM SER EXPLORADAS EVENTUAIS DEFICIÊNCIAS NO SETOR DE PLANEJAMENTO DO ÓRGÃO " +
                "NO ITEM A.2 DO RELATÓRIO. OU SEJA, DAS DEFICIÊNCIAS DO ÓRGÃO PARA PLANEJAR, RESULTOU-SE EM" +
                " ALTERAÇÕES SIGNIFICATIVAS NO ORÇAMENTO, ", boldItalicVermelhoAmareloJustificado12)
                .concat("ESPECIALMENTE FACE AO DÉFICIT ORÇAMENTÁRIO.", boldItalicUnderlineVermelhoAmareloJustificado12));

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": APLICÁVEL EM TODOS OS CASOS DE DÉFICIT ORÇAMENTÁRIO NÃO AMPARADO EM SUPERÁVIT FINANCEIRO",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("O Município realizou investimento, com base na despesa liquidada, correspondente a ",
                formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(valorInvestimentoMunicipioMap.get(exercicio) + " da receita arrecadada total.", formatacaoFactory.getJustificado(12))
        );
        addParagrafo(new TextoFormatado("CONSIDERAR O MUNICÍPIO COMO UM TODO, DEVENDO SER CONSIDERADOS OS VALORES " +
                "CONSTANTES NO RREO.",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Nos três últimos exercícios, o resultado da execução orçamentária e o " +
                        "investimento apresentaram os seguintes percentuais:",
                formatacaoFactory.getJustificado(12)));

        addTabelaResultadoExecucaoOrcamentariaPercentuais();

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.2. RESULTADOS FINANCEIRO, ECONÔMICO E SALDO PATRIMONIAL",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addTabelaResultadoFinanceiroEconomicoPatrimonial();

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NO CASO DE RESULTADOS NEGATIVOS.",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": UTILIZAR ESSA ANÁLISE EM CASO DE DÉFICIT FINANCEIRO NO EXERCÍCIO EM EXAME",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("O resultado da execução orçamentária assim influenciou o resultado financeiro:",
                formatacaoFactory.getJustificado(12)));


        addTabelaResultadoFinanceiroAnterior();
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NO CASO DE RESULTADOS NEGATIVOS.",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": RESULTADO ORÇAMENTÁRIO SUPERAVITÁRIO INSUFICIENTE PARA REVERTER O DÉFICIT" +
                                " FINANCEIRO DO EXERCÍCIO ATUAL",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addBreak();

        addParagrafo(addTab().concat("Haja vista esses números, o superávit orçamentário do exercício em exame " +
                        "não foi suficiente para reverter o déficit financeiro vindo do exercício anterior.",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": RESULTADO ORÇAMENTÁRIO DEFICITÁRIO E DÉFICIT FINANCEIRO DO EXERCÍCIO ANTERIOR",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Haja vista esses números, o déficit orçamentário do exercício em " +
                        "exame fez aumentar, em ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("%, o déficit financeiro ",
                        formatacaoFactory.getJustificado(12))
                .concat("(retificado)  ",
                        formatacaoFactory.getJustificadoVermelho(12))
                .concat("do exercício anterior, embora tenha sido a Prefeitura alertada tempestivamente por ",
                        formatacaoFactory.getJustificado(12))
                .concat("",
                        formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("" + quantidadeAlertasDesajusteExecucaoOrcamentaria + " vezes, por esta Corte de Contas. ", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": RESULTADO ORÇAMENTÁRIO DEFICITÁRIO E INSUFICIENTE SUPERÁVIT FINANCEIRO DO " +
                        "EXERCÍCIO ANTERIOR", boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();


        addParagrafo(addTab().concat("Haja vista esses números, o déficit orçamentário do exercício em exame " +
                "fez surgir um antes inexistente déficit financeiro, embora tenha sido a Prefeitura alertada " +
                "tempestivamente por ", formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("" + quantidadeAlertasDesajusteExecucaoOrcamentaria + " vezes, por esta Corte de Contas.", formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.3. DÍVIDA DE CURTO PRAZO",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": EM CASO DE SUPERÁVIT FINANCEIRO (Lei Federal nº 4.320/1964)",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Conforme demonstrado no item anterior, a Prefeitura apresentou, " +
                "no encerramento do exercício examinado, um superávit financeiro, evidenciando, com isso, a " +
                "existência de recursos disponíveis para o total pagamento de suas dívidas de curto prazo, " +
                "registradas no Passivo Financeiro.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": EM CASO DE DÉFICIT FINANCEIRO (Lei Federal nº 4.320/1964)",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addTabelaPassivoFinanceiroAnexo14A();
        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NO CASO DE RESULTADOS NEGATIVOS.",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Considerando o resultado financeiro deficitário apurado, verifica-se que" +
                " a Prefeitura não possui recursos disponíveis para o total pagamento de suas dívidas de curto prazo, " +
                "registradas no Passivo Financeiro.", formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("ANÁLISE DA LIQUIDEZ IMEDIATA (NCASP), A PRINCÍPIO, APENAS EM CASO DE " +
                "DÉFICIT FINANCEIRO:",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Ademais, constatamos que o Índice de Liquidez Imediata do órgão é o seguinte:",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addTabelaIndiceDeLiquidezImediata();

        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": ÍNDICE DE LIQUIDEZ IMEDIATA MAIOR OU IGUAL A 1",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Considerando o índice apurado, verifica-se que a Prefeitura possui liquidez" +
                        " face aos compromissos de curto prazo, registrados no Passivo Circulante.",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": ÍNDICE DE LIQUIDEZ IMEDIATA MENOR QUE 1",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Considerando o índice apurado, verifica-se que a Prefeitura não possui " +
                        "liquidez face aos compromissos de curto prazo, registrados no Passivo Circulante.",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("COMENTAR CASO NÃO OCORRA REDUÇÃO DO PASSIVO DE CURTO PRAZO. FAZER A" +
                " ANÁLISE EM FUNÇÃO DA LIQUIDEZ E DA EVOLUÇÃO DO RESULTADO FINANCEIRO APURADO.",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addBreak();
        addSecao(new TextoFormatado("B.1.4. DÍVIDA DE LONGO PRAZO",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": INEXISTÊNCIA DE ENDIVIDAMENTO DE LONGO PRAZO",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Conforme Balanço Patrimonial gerado pelo Sistema Audesp, a Prefeitura não" +
                        " possui dívidas registradas em seu Passivo Permanente e/ou Não-Circulante.",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": EXISTÊNCIA DE ENDIVIDAMENTO DE LONGO PRAZO:",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addTabelaEndividamentoLongoPrazo();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NO CASO DE RESULTADOS NEGATIVOS.",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("IDENTIFICAR E INFORMAR AS CAUSAS DO AUMENTO DA DÍVIDA DE LONGO PRAZO " +
                "(CONFISSÃO DE DÍVIDA JUNTO AO INSS; ATUALIZAÇÃO MONETÁRIA DE EMPRÉSTIMOS TOMADOS; " +
                "OPERAÇÕES DE CRÉDITO DE LONGO PRAZO ETC.)",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("Para fins de acompanhamento deverá a fiscalização verificar in loco se" +
                " a Prefeitura está cumprindo os parcelamentos de Encargos Sociais junto à Receita Federal do Brasil" +
                " e aos Regimes Próprios de Previdência (Portaria nº 333 de 11 de julho de 2017, MP nº 778 de 16 de" +
                " maio de 2017 convertida na Lei Federal nº 13.485/2017) trazendo informação a este item do Relatório " +
                "de Acompanhamento, uma vez que reflete no nível de endividamento de longo prazo do município," +
                " conforme segue.",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addSecao(new TextoFormatado("B.1.4.1. PARCELAMENTOS DE DÉBITOS PREVIDENCIÁRIOS",
                formatacaoFactory.getBold(12)), heading4);
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": INEXISTINDO PARCELAMENTOS DE DÉBITOS PREVIDENCIÁRIOS JUNTO A INSS/RPPS.",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();


        addParagrafo(addTab().concat("A Prefeitura não possui parcelamento ou reparcelamento de débitos " +
                        "previdenciários junto ao INSS e ao RPPS.",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": EXISTINDO PARCELAMENTOS DE DÉBITOS PREVIDENCIÁRIOS JUNTO A INSS/RPPS, EMBASADOS NA " +
                                "LEI Nº 13.485/2017 E PELA PORTARIA Nº 333/2017, RESPECTIVAMENTE.",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Demonstramos abaixo a situação dos parcelamentos/reparcelamentos de débitos" +
                        " previdenciários autorizados pela Lei Federal nº 13.485, de 2 de outubro de 2017 e/ou pela " +
                        "Portaria MF nº 333, de 11 de julho de 2017:",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("Descrever sucintamente as características de cada acordo, sugerindo-se " +
                "os campos abaixo ",
                boldItalicVermelhoAmareloJustificado12)
                .concat("(no caso de RPPS, informar obrigatoriamente a Lei Municipal autorizadora):",
                        boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("Perante o INSS", formatacaoFactory.getBold(12))
                .concat("\nnº do acordo:", formatacaoFactory.getFormatacao(12))
                .concat("\nvalor total parcelado:", formatacaoFactory.getFormatacao(12))
                .concat("\nquantidade de parcelas:", formatacaoFactory.getFormatacao(12))
                .concat("\nparcelas devidas no exercício:", formatacaoFactory.getFormatacao(12))
                .concat("\npagas no exercício:", formatacaoFactory.getFormatacao(12)));

        addParagrafo(new TextoFormatado("nº do acordo:", formatacaoFactory.getFormatacao(12))
                .concat("\nvalor total parcelado:", formatacaoFactory.getFormatacao(12))
                .concat("\nquantidade de parcelas:", formatacaoFactory.getFormatacao(12))
                .concat("\nparcelas devidas no exercício:", formatacaoFactory.getFormatacao(12))
                .concat("\npagas no exercício:", formatacaoFactory.getFormatacao(12)));

        addBreak();
        addParagrafo(new TextoFormatado("Perante o RPPS", formatacaoFactory.getBold(12))
                .concat("\nLei Municipal autorizadora nº:", formatacaoFactory.getFormatacao(12))
                .concat("\nnº do acordo:", formatacaoFactory.getFormatacao(12))
                .concat("\nvalor total parcelado:", formatacaoFactory.getFormatacao(12))
                .concat("\nquantidade de parcelas:", formatacaoFactory.getFormatacao(12))
                .concat("\nparcelas devidas no exercício:", formatacaoFactory.getFormatacao(12))
                .concat("\npagas no exercício:", formatacaoFactory.getFormatacao(12)));

        addParagrafo(new TextoFormatado("Lei Municipal autorizadora nº:", formatacaoFactory.getFormatacao(12))
                .concat("\nnº do acordo:", formatacaoFactory.getFormatacao(12))
                .concat("\nvalor total parcelado:", formatacaoFactory.getFormatacao(12))
                .concat("\nquantidade de parcelas:", formatacaoFactory.getFormatacao(12))
                .concat("\nparcelas devidas no exercício:", formatacaoFactory.getFormatacao(12))
                .concat("\npagas no exercício:", formatacaoFactory.getFormatacao(12)));

        addBreak();

        addParagrafo(addTab().concat("Do acima exposto, constatamos que no exercício em exame a Prefeitura ",
                formatacaoFactory.getJustificado(12))
                .concat("não cumpriu / cumpriu parcialmente / cumpriu", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" o acordado", formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("Caso não tenha cumprido especificar.",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addBreak();
        addSecao(new TextoFormatado("B.1.5. PRECATÓRIOS",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();


        addParagrafo(new TextoFormatado("VERIFICAR A EXISTÊNCIA DE EVENTUAIS PRECATÓRIOS A RECEBER PELO ÓRGÃO," +
                " OCASIÃO EM QUE DEVERÁ SER INSERIDO SUBITEM ESPECÍFICO PARA TRATAR DO ASSUNTO.",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": INEXISTÊNCIA DE DÍVIDAS JUDICIAIS",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("De acordo com informações prestadas pela origem, e in loco confirmadas, " +
                        "o Município não possui dívidas judiciais.",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": EXISTÊNCIA DE DÍVIDAS JUDICIAIS",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();


        addParagrafo(new TextoFormatado("VERIFICAR CONTEÚDO NA NTI SDG Nº 25",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": REGIME ORDINÁRIO",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addTabelaRegimeOrdinarioPrecatorios();
        addTabelaRequisitosDeBaixaMonta();


        addBreak();
        addBreak();

        addParagrafo(addTab().concat("Em relação à contabilização dos precatórios apuramos: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addTabelaVerificacaoPrecatorio();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO DEVERÁ PREENCHER COM A OPÇÃO: \"SIM\" OU \"NÂO\".",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": REGIME ESPECIAL",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addTabelaVerificacaoPrecatorio();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO DEVERÁ PREENCHER COM A OPÇÃO: \"SIM\" OU \"NÂO\".",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": REGIME ESPECIAL",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();


        addTabelaRegimeEspecialPrecatorios();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NO CASO DE AJUSTES DA" +
                " FISCALIZAÇÃO SEJA DE EXCLUSÃO.",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addTabelaRequisitosDeBaixaMonta();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("PAGAMENTOS = DEPÓSITOS EM CONTAS VINCULADAS DO TJSP E/OU PAGAMENTOS " +
                "DIRETAMENTE NO PROCESSO.",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("MENCIONAR EVENTUAIS ACORDOS PARA PAGAMENTOS JUNTO AO TJ/SP",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("É DE BOM ALVITRE REQUISITAR À PREFEITURA CERTIDÃO DE REGULARIDADE " +
                "EXPEDIDA PELO DEPRE, RESPONSÁVEL PELA GESTÃO DOS PAGAMENTOS DE PRECATÓRIOS.",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAR A NOTA TÉCNICA SDG Nº 142, INCLUSIVE PARA NOTICIAR EVENTUAL " +
                "DESAPROPRIAÇÃO NOS CASOS VEDADOS.",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(addTab().concat("Em relação à contabilização dos precatórios apuramos: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addTabelaVerificacaoPrecatorio();
        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO DEVERÁ PREENCHER COM A OPÇÃO: \"SIM\" OU \"NÂO\".",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("EFETUAR ANÁLISE DOS PRECATÓRIOS EFETIVAMENTE CONTABILIZADOS E OS " +
                "INSERIDOS NO MAPA DE PRECATÓRIOS DO SISTEMA Audesp. HAVENDO INCONSISTÊNCIA RELATAR A FALHA.",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();


        addParagrafo(new TextoFormatado("APURAÇÕES REFERENTES À EMENDA CONSTITUCIONAL Nº 99, DE 14 DE DEZEMBRO DE 2017",
                        formatacaoFactory.getBoldJustificado(12)));


        addParagrafo(new TextoFormatado("OBSERVAÇÕES",
                        boldItalicUnderlineVermelhoAmareloJustificado12)
                        .concat(":\nA APURAÇÃO DESTE TÓPICO SE DÁ SOB O ASPECTO ORÇAMENTÁRIO, TENDO EM VISTA QUE " +
                                        "A APURAÇÃO É MENSAL, COM BASE NA RCL DO SEGUNDO MÊS ANTERIOR, NOS TERMOS DO" +
                                        " ART. 101 DO ADCT/CF. POR ISSO, O VALOR PODERÁ NÃO COINCIDIR COM O MONTANTE" +
                                        " DE “DEPÓSITOS EFETUADOS NO EXERCÍCIO EM EXAME”, DO QUADRO DE “REGIME ESPECIAL”, " +
                                        "VISTO QUE ESTA APURAÇÃO (REGIME ESPECIAL) É PATRIMONIAL. \n PORTANTO, CONSIDERAR " +
                                        "COMO “MONTANTE DEPOSITADO REFERENTE AO EXERCÍCIO EM EXAME” (DEPÓSITOS AO" +
                                        " TJSP) A QUITAÇÃO DOS EMPENHOS ATINENTES AOS MESES DO EXERCÍCIO FISCALIZADO" +
                                        " (REGIME DE COMPETÊNCIA). NESSE SENTIDO, DEVE-SE AVERIGUAR A QUITAÇÃO " +
                                        "DE EVENTUAIS RESTOS A PAGAR QUITADOS NO EXERCÍCIO CORRENTE ATÉ A DATA " +
                                        "DA INSPEÇÃO (POR EXEMPLO, EMPENHO DE 2018 INSCRITOS EM RESTOS A PAGAR, " +
                                        "QUITADO EM 2019\nCASO ESSES RESTOS A PAGAR NÃO TENHAM SIDO PAGOS, " +
                                        "EXPLICAR, COMO EXEMPLIFICADO AO FINAL DESTE TÓPICO.).",
                                boldItalicVermelhoAmareloJustificado12));
                addBreak();

                addParagrafo(addTab().concat("Considerando o valor dos depósitos referentes ao exercício em exame, o " +
                                "quadro a seguir procura demonstrar se nesse ritmo as dívidas com precatórios estariam " +
                                "liquidadas até o exercício de 2024, conforme Emenda Constitucional nº 99, de 14 de" +
                                " dezembro de 2017.",
                                formatacaoFactory.getJustificado(12)));
                addBreak();

                addTabelaQuitacaoDosPrecatoorios();

//        QUITAÇÃO DE PRECATÓRIOS ATÉ 2024 (EMENDA CONSTITUCIONAL nº 99, DE 14 DE DEZEMBRO DE 2017)
//        Considerando o valor dos depósitos no presente exercício, o quadro a seguir procura
//        demonstrar se nesse ritmo as dívidas com precatórios estariam liquidadas até o exercício
//        de 2024, conforme Emenda Constitucional nº 99, de 14 de dezembro de 2017.
//        TRECHO COM POSSÍVEL ALTERAÇÃO/INCLUSÃO DE VERIFICAÇÃO

        addBreak();
        addParagrafo(addTab().concat("Ainda, face à redação dada pela citada Emenda Constitucional ao art. 101 da " +
                        "Constituição Federal, o quadro seguinte demonstra se os depósitos referentes ao exercício " +
                        "em exame atenderam ao percentual praticado em dezembro de 2017:",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addTabelaPrecatorioApuracaoPagamentoPiso();


        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÕES:",
                        boldItalicUnderlineVermelhoAmareloJustificado12));
                addParagrafo(new TextoFormatado("NO “EXERCÍCIO EM EXAME” DA TABELA RETRO, PARA EFEITO DE FÓRMULA, SEMPRE ESCREVER 01/01/20XX.\n" +
                                        "ALÍQUOTA CONSIDERADA É A FORNECIDA PELA DEPRE/TJSP.",
                                boldItalicVermelhoAmareloJustificado12));
                addBreak();
                addParagrafo(addTab()
                                .concat("Cabe ressaltar que não foi considerado como ",
                                formatacaoFactory.getJustificado(12)
                )
                                .concat("\"Montante depositado referente ao exercício em exame\" ", formatacaoFactory.getBold(12))
                        .concat(" o valor de R$ ",
                                formatacaoFactory.getJustificado(12))
                        .concat("XXX,XX",
                                formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(", atinente a Restos a Pagar do exercício de 2018 não quitados até a data da fiscalização, bem como R$ ",
                                formatacaoFactory.getJustificado(12))
                        .concat("XXX,XX",
                                formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(", concernentes a depósitos e/ou insuficiências de exercícios anteriores, consoante documentos colacionados ",
                                formatacaoFactory.getJustificado(12))
                        .concat("XXX.",
                                formatacaoFactory.getJustificadoVermelhoCinza(12))


                );


                addParagrafo(new TextoFormatado("OBSERVAÇÃO:", boldItalicUnderlineVermelhoAmareloJustificado12));
                addParagrafo(new TextoFormatado("CASO CONSTATADA A POSSIBILIDADE DE NÃO QUITAÇÃO ATÉ 2024, OU O NÃO " +
                                                "ATENDIMENTO AO PISO, REQUISITAR À ORIGEM INFORMAÇÕES E DOCUMENTOS " +
                                                "ACERCA DE EVENTUAL NOVO PLANO DE PAGAMENTO HOMOLOGADO PELO TJSP. " +
                                                "ADAPTAR O PARÁGRAFO SEGUINTE, CONFORME O CASO.:",
                                        boldItalicVermelhoAmareloJustificado12));
                addBreak();

                addParagrafo(addTab().concat("Considerando as apurações retro - a perspectiva de que o órgão" +
                                " não quitará o estoque de precatórios até 2024 ",
                                formatacaoFactory.getJustificado(12))
                                .concat("e/ou", formatacaoFactory.getJustificadoVermelhoCinza(12))
                                .concat(" de que não atendeu ao piso de pagamentos no exercício examinado -, " +
                                        "requisitamos informações acerca de eventual novo plano de pagamento" +
                                        " proposto e homologado junto ao e. Tribunal de Justiça do Estado de São " +
                                        "Paulo, tendo sido esclarecido que (", formatacaoFactory.getJustificado(12))
                                .concat("relatar", formatacaoFactory.getJustificadoVermelhoCinza(12))
                                .concat(")", formatacaoFactory.getJustificado(12))
                        );

        addBreak();
        addBreak();


        addSecao(new TextoFormatado("B.1.6. ENCARGOS",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();


        addParagrafo(new TextoFormatado("ANÁLISE OBRIGATÓRIA:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Os recolhimentos apresentaram a seguinte posição: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addTabelaPosicaoRecolhimentosEncargos();


        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("AS LINHAS 1 A 4 DEVERÃO SER PREENCHIDAS PELA FISCALIZAÇÃO COM AS OPÇÕES:" +
                " “SIM’, “NÃO”, “PARCIAL” OU “PREJUDICADO”.",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": SE HOUVER RPPS",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Destacamos que o Regime Próprio de Previdência - RPPS é administrado por  ",
                formatacaoFactory.getJustificado(12))
                .concat("(dizer o nome do fundo ou da autarquia de previdência)", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", cujas contas estão abrigadas no Processo TC-XXXXXX.989.XX.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("O Município ",
                formatacaoFactory.getJustificado(12))
                .concat("dispõe/não dispõe ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("do Certificado de Regularidade Previdenciária.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": SE FOR O CASO, UTILIZAR O PARÁGRAFO ABAIXO",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Os parcelamentos de valores devidos à ",
                formatacaoFactory.getJustificado(12))
                .concat("Receita Federal do Brasil e/ou ao Regime Próprio de Previdência ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("estão sendo tratados no item B.1.4. DÍVIDA DE LONGO PRAZO do presente Relatório.", formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addSecao(new TextoFormatado("B.1.7. TRANSFERÊNCIA À CÂMARA DOS VEREADORES",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAR SE A INCLUSÃO/EXCLUSÃO DA CIP NO CÁLCULO DA RECEITA TRIBUTÁRIA" +
                " AMPLIADA (RTA) ENSEJA REGULARIDADE/IRREGULARIDADE DA TRANSFERÊNCIA, NOTICIANDO, CONFORME O CASO." +
                " ATENÇÃO: OS RELATÓRIOS DA PREFEITURA E DA CÂMARA (ITENS DE TRANSFERÊNCIAS E DE SUBSÍDIOS DOS AGENTES POLÍTICOS)" +
                " DEVEM GUARDAR CONSONÂNCIA! ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": CASO REGULAR",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Os repasses à Câmara obedeceram ao limite do art. 29-A, da Constituição Federal. ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": CASO IRREGULAR",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Os repasses à Câmara assim se mostraram no exercício examinado: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addTabelaRepassesACamara();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                boldItalicCapsVermelhoAmareloJustificado12));
        addParagrafo(new TextoFormatado("NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NO CAMPO “DESPESAS COM INATIVOS”",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("DESCREVER AS IRREGULARIDADES",
                boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.8. ANÁLISE DOS LIMITES E CONDIÇÕES DA LEI DE RESPONSABILIDADE FISCAL",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO À NT SDG Nº 141 E AO COMUNICADO SDG Nº 30/2018, QUANDO DA APURAÇÃO DA RCL," +
                " EM ESPECIAL EM CASOS DE DESCUMPRIMENTO DOS LIMITES DA LRF," +
                " VISTO QUE A DECISÃO SOBRE A CONSULTA É DE SETEMBRO DE 2018, OU SEJA, AINDA PODE HAVER REFLEXO," +
                " O QUE SERÁ EVENTUALMENTE DIRIMIDO PELOS CONSELHEIROS.",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": CASO ATENDIDOS OS LIMITES ESTABELECIDOS NA LRF USAR O CONTEÚDO ADIANTE.",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();


        addParagrafo(addTab().concat("No período, as análises automáticas não identificaram descumprimentos aos" +
                        " limites estabelecidos na Lei de Responsabilidade Fiscal, quanto à Dívida Consolidada " +
                        "Líquida, Concessões de Garantias e Operações de Crédito, inclusive ARO.",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("CASO NÃO ATENDIDO ALGUM DOS LIMITES ESTABELECIDOS NA LRF USAR O CONTEÚDO ADIANTE.",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addTabelaComparativoLimiteLRF();

        addBreak();

        addParagrafo(addTab().concat("Verificamos o não atendimento aos limites estabelecidos pela Lei de" +
                        " Responsabilidade Fiscal, isso em decorrência do que segue: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("DESCREVER AS IRREGULARIDADES: ",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.8.1. DESPESA DE PESSOAL",
                formatacaoFactory.getBold(12)), heading4);
        addBreak();

addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("HIPÓTESE: REGULARIDADE NOS TRÊS QUADRIMESTRES ",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addBreak();
        addParagrafo(addTab().concat("Conforme Relatórios de Gestão Fiscal emitidos pelo Sistema Audesp, o Poder Executivo atendeu ao limite" +
                        " da despesa de pessoal previsto no art. 20, III, alínea “b” da Lei de Responsabilidade Fiscal, registrando no 3° quadrimestre" +
                        " o valor de R$  ",
                formatacaoFactory.getJustificado(12))
                .concat("XXX.XXX,XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", o que representa um percentual de ", formatacaoFactory.getJustificado(12))
                .concat("XX,XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("%.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("HAVENDO DESRESPEITO AO LIMITE PARA DESPESAS DE PESSOAL",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addTabelaDespesaDePessoal();

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES ",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO QUANTO AO AJUSTE DO MÊS DE DEZEMBRO DO EXERCÍCIO ANTERIOR." +
                " SEMPRE BUSCAR SEGUIR O APURADO PELA FISCALIZAÇÃO ANTERIOR.  ",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("IMPORTANTE: VERIFICAR A APLICABILIDADE DO ART. 66 DA LRF, DE DUPLICAÇÃO" +
                " DO PRAZO DE RECONDUÇÃO EM CASOS DE BAIXO CRESCIMENTO DO PIB",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("CASO EXTRAPOLADO O LIMITE NO 2º OU NO 3º QUADRIMESTRE, AVERIGUAR E NOTICIAR EVENTUAL RECONDUÇÃO," +
                " OU NÃO, NOS QUADRIMESTRES SEGUINTES, AINDA QUE DO EXERCÍCIO POSTERIOR.",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("ACIMA DE 95% DE 54% (51,30%), QUANDO SE INICIAM AS VEDAÇÕES DA LRF",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Diante dos elementos apurados, verificamos que a despesa total com pessoal não superou o limite " +
                        "previsto no art. 20, III, da Lei de Responsabilidade Fiscal, porém ultrapassou aquele previsto no art. 22, parágrafo único," +
                        " da Lei supracitada, nos  ",
                formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" quadrimestres.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Constatamos ",
                formatacaoFactory.getJustificado(12))
                .concat("OU não ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("a infringência do inciso ", formatacaoFactory.getJustificado(12))
                .concat("XX ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", do citado dispositivo, tendo em vista que ", formatacaoFactory.getJustificado(12))
                .concat("relatar as ocorrências.", formatacaoFactory.getJustificadoVermelhoCinza(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal," +
                        " o Executivo Municipal foi alertado tempestivamente, por ",
                formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(quantidadeAlertasDespesaPessoal + " vezes, quanto à superação de 90% do específico limite" +
                        " da despesa laboral.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("OBSERVAR A DATA DE EMISSÃO DO ALERTA (CONSTANTE NO FINAL DO DOCUMENTO “NOTIFICAÇÃO DE ALERTA”), " +
                "PARA CONSIDERÁ-LO " +
                "TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO FINAL DO EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("ACIMA DE 54% NOS DOIS PRIMEIROS QUADRIMESTRES",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("É possível ver que o gasto excessivo com pessoal no 1º ou 2º quadrimestre ",
                formatacaoFactory.getJustificado(12))
                .concat("(não)/foi ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("resolvido no prazo legal, eis que, no último quadrimestre do exercício, a despesa laboral do Executivo Municipal " +
                        "significou ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da Receita Corrente Líquida.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal," +
                        " o Executivo Municipal foi alertado tempestivamente, por ",
                formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(quantidadeAlertasDespesaPessoal + " vezes, quanto à superação de 90% do específico limite da despesa laboral.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("OBSERVAR A ",
                boldItalicCapsVermelhoAmareloJustificado12)
                .concat("DATA DE EMISSÃO DO ALERTA ",
                        boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("(CONSTANTE NO FINAL DO DOCUMENTO “NOTIFICAÇÃO DE ALERTA”)," +
                                " PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO FINAL DO" +
                                " EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("ACIMA DE 54% NO ÚLTIMO QUADRIMESTRE",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("É possível ver que a superação do limite da despesa laboral aconteceu no " +
                        "último quadrimestre do exercício, significando ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da Receita Corrente Líquida.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal, o Executivo Municipal" +
                        " foi alertado tempestivamente, por ",
                formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(quantidadeAlertasDespesaPessoal + " vezes, quanto à superação de 90% do específico limite da despesa laboral.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("OBSERVAR A ",
                boldItalicCapsVermelhoAmareloJustificado12)
                .concat("DATA DE EMISSÃO DO ALERTA ",
                        boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("(CONSTANTE NO FINAL DO DOCUMENTO “NOTIFICAÇÃO DE ALERTA”)," +
                                " PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO FINAL DO" +
                                " EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addSecao(new TextoFormatado("B.1.9. DEMAIS ASPECTOS SOBRE RECURSOS HUMANOS",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("1ª HIPÓTESE - REGULARIDADE",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Não constatamos, sob amostragem, ocorrências dignas de nota sobre o assunto neste exercício. ",
                formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("2ª HIPÓTESE - IRREGULARIDADE",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Eis o quadro de pessoal existente no final do exercício:",
                formatacaoFactory.getJustificado(12))
        );

        addTabelaQuadroDePessoal();
        addBreak();

        addParagrafo(addTab().concat("No exercício examinado foram nomeados ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" servidores para cargos em comissão, cujas atribuições ", formatacaoFactory.getJustificado(12))
                .concat("possuem / não possuem", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" características de direção, chefia e assessoramento (art. 37, V, da Constituição Federal). ",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("As atribuições dos mencionados cargos ", formatacaoFactory.getJustificado(12))
                .concat("foram / não foram", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" definidas através de lei(s). ", formatacaoFactory.getJustificado(12))
                .concat("(CITAR QUAL-IS) ", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("DESCREVER AS IRREGULARIDADES", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addSecao(new TextoFormatado("B.1.10. SUBSÍDIOS DOS AGENTES POLÍTICOS",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addTabelaSubsidioAgentePoliticos();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
        );
        addBreak();
        addParagrafo(new TextoFormatado("1)\tNO CASO DE A FIXAÇÃO SER ANTERIOR, BASTA ACRESCER LINHAS COM MAIS RGAs; ",
                boldItalicVermelhoAmareloJustificado12)
                .concat("\n2)\tCASO NÃO SEJA APLICÁVEL A SECRETÁRIOS MUNICIPAIS EXCLUIR A COLUNA.",
                        boldItalicVermelhoAmareloJustificado12)
        );
        addBreak();

        addTabelaVerificacoesRGA();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES: ",
                boldItalicUnderlineVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("1)\tAS LINHAS DEVERÃO SER PREENCHIDAS PELA FISCALIZAÇÃO COM A OPÇÃO:" +
                " “SIM” OU “NÃO”;",
                boldItalicVermelhoAmareloJustificado12)
                .concat("\n2)\tCASO AS OPÇÕES DAS LINHAS 1, 2 3, 4 E 5 SEJAM ”NÃO”, DETALHAR AS INFORMAÇÕES.",
                        boldItalicVermelhoAmareloJustificado12)
        );
        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("QUANDO NÃO HOUVER IRREGULARIDADES NOS PAGAMENTOS",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Conforme nossos cálculos, não foram constatados pagamentos maiores " +
                        "que os fixados.",
                formatacaoFactory.getJustificado(12))
        );


        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("QUANDO HOUVER IRREGULARIDADES NOS PAGAMENTOS DEVEREMOS INSERIR O TEXTO E TABELA " +
                                "(REPLICADA CONFORME A NECESSIDADE PARA PREFEITO, VICE-PREFEITO E SECRETÁRIOS)",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("De acordo com nossos cálculos, constatamos os seguintes pagamentos excessivos: ",
                formatacaoFactory.getJustificado(12))
        );


        addBreak();
        addTabelaPagamentosExcessivos();
        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO: OBSERVAR O LIMITE (MÍNIMO) ESTABELECIDO DE 500 UFESP´S PARA " +
                "ABERTURA DE APARTADO (RES. 4/2015 C/C RES. 6/2016) ", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("No intuito de melhor analisar sobredito excesso remuneratório, " +
                "protocolamos o Expediente TC-XXXXXX.989.XX.", formatacaoFactory.getJustificado(12)));
        addBreak();

        addSecao(new TextoFormatado("B.2. IEG-M – I-FISCAL – Índice " + resultadoIegm2018.getFaixaIFiscal(),
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo. ",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat("SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa" +
                " dimensão do IEG-M.", formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();

        addApontamentosODS("i-fiscal");
        addBreak();
        addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice III - ODS",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addSecao(new TextoFormatado("B.3. OUTROS PONTOS DE INTERESSE ",
                formatacaoFactory.getBold(12))
                .concat("(manter apenas em caso de ocorrência. Senão excluir item)",
                        formatacaoFactory.getBoldJustificadoFundoAmarelo(12)), heading2);
        addBreak();

        addBreak();
        addParagrafo(new TextoFormatado("ESSAS ANÁLISES ", boldItalicVermelhoAmareloJustificado12)
                .concat("APENAS", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(" COMPORÃO O RELATÓRIO EM CASO DE ", boldItalicVermelhoAmareloJustificado12)
                .concat("IRREGULARIDADES CONSTATADAS POR MEIO DO IEG-M, DENÚNCIAS FORMALIZADAS PERANTE ESTE " +
                        "TCESP, HISTÓRICO DO ÓRGÃO DE DESVIOS OU MALVERSAÇÃO DE RECURSOS", boldItalicVermelhoAmareloJustificado12)
                .concat(", ACHADOS CUJA RELEVÂNCIA/MATERIALIDADE JUSTIFIQUEM A ATUAÇÃO DO TCESP ETC., " +
                                "CONSIDERANDO QUE O INTUITO DA FISCALIZAÇÃO É EFETUAR ANÁLISES FINALÍSTICAS.",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("PARA TANTO, VERIFICAR MODELOS/SUGESTÕES NO APÊNDICE II NO FINAL " +
                "DO PRESENTE MODELO.", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA C: ENSINO", formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("C.1. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": QUANDO DETECTADO PELA FISCALIZAÇÃO QUE FOI EMPENHADO, LIQUIDADO E ",
                        boldItalicVermelhoAmareloJustificado12)
                .concat("PAGO",
                        boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(" MAIS QUE O MÍNIMO CONSTITUCIONAL DE 25%, APLICAÇÃO TOTAL DO FUNDEB " +
                                "(MÍNIMO DE 95% E PARCELA DIFERIDA) E 60% DO FUNDEB, " +
                                "UTILIZAR A REDAÇÃO ADIANTE.\nOBSERVAÇÃO: NOS CASOS DE APLICAÇÃO DO FUNDEB," +
                                " NOTAR QUE OS RESTOS PAGAR DEVEM SER QUITADOS ATÉ 31/3 DO EXERCÍCIO SEGUINTE, " +
                                "MESMO PRAZO DE APLICAÇÃO DOS 5% DIFERIDOS.",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();
//        addParagrafo(addTab().concat("Quanto à aplicação de recursos ao final do exercício em exame, conforme " +
//                        "informado ao Sistema Audesp e apurado pela Fiscalização, os resultados assim se apresentaram: ",
//                formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(addTab().concat("Quanto à aplicação de recursos ao final do exercício em exame, conforme " +
                        "informado ao Sistema Audesp e apurado pela Fiscalização, os resultados assim se apresentaram: ",
                formatacaoFactory.getJustificado(12))
        );

        addTabelaDespesasCfFundeb();

        addBreak();
        addParagrafo(addTab().concat("Conforme acima exposto, a despesa educacional empenhada, liquidada " +
                        "e paga cumpriu o art. 212 da Constituição Federal.",
                formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(addTab().concat("Ainda, houve utilização de todo o FUNDEB recebido, inclusive pagamentos dos" +
                " Restos a Pagar (", formatacaoFactory.getJustificado(12))
                .concat("se for o caso", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("), observando-se o art. 21, da Lei Federal nº 11.494, de 20 de junho de 2007.",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("OU", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(addTab().concat("No exercício em exame foi observado o percentual mínimo de 95% de aplicação" +
                        " dos recursos do FUNDEB recebido, inclusive pagamentos dos Restos a Pagar ",
                formatacaoFactory.getJustificado(12))
                .concat("se for o caso", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("), sendo que, por meio de conta bancária vinculada, constatamos a utilização da parcela " +
                                "diferida no 1º trimestre do exercício corrente (",
                        formatacaoFactory.getJustificado(12))
                .concat("máximo de 5%", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("), atendendo-se ao § 2º do art. 21 da Lei Federal nº 11.494, de 20 de junho de 2007.",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();


        //////////////
        addParagrafo(addTab().concat("Demais disso, verificamos que houve aplicação superior ao mínimo de 60% do FUNDEB" +
                        " na remuneração dos profissionais do magistério da Educação Básica, dando cumprimento ao " +
                        "art. 60, XII, do Ato das Disposições Constitucionais Transitórias.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                        boldItalicUnderlineVermelhoAmareloJustificado12)
                        .concat(": HAVENDO GASTOS COM INATIVOS",
                                boldItalicVermelhoAmareloJustificado12));
                addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO: PARA CUMPRIMENTO DA NT SDG 131/2016, A FISCALIZAÇÃO ",
                boldItalicVermelhoAmareloJustificado12)
                .concat("DEVERÁ IDENTIFICAR",
                        boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("OS VALORES EFETIVAMENTE DESPENDIDOS COM INATIVOS NO MAGISTÉRIO INCLUÍDOS NOS MÍNIMOS " +
                                "CONSTITUCIONAIS, A QUALQUER TÍTULO. (INCLUSIVE REPASSE FINANCEIRO OU APORTE " +
                                "PARA COBERTURA DE DÉFICIT PREVIDENCIÁRIO)",
                        boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("A Fiscalização identificou valores despendidos com inativos do magistério " +
                        "incluídos nos mínimos constitucionais do Ensino, na seguinte conformidade:",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("DESCREVER", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("NÃO HAVENDO GASTOS COM INATIVOS", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("A Fiscalização não identificou valores despendidos com inativos do " +
                        "magistério incluídos nos mínimos constitucionais do Ensino.",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("ANÁLISE OBRIGATÓRIA",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": OFERTA DE VAGAS NO ENSINO",
                        boldItalicVermelhoAmareloJustificado12)
                .concat("\nOBSERVAÇÃO: A VERIFICAÇÃO INICIAL DEVERÁ SER EFETUADA NA VISITA DO 1º QUADRIMESTRE DE ACOMPANHAMENTO " +
                        "2019 OU NO FECHAMENTO DE 2018 (ORDINÁRIAS E FECHAMENTOS DE ACOMPANHAMENTOS 2018). " +
                        "O ASSUNTO TAMBÉM DEVERÁ CONSTAR DOS RELATÓRIOS DE FECHAMENTO DOS ACOMPANHAMENTOS DE 2019 E " +
                        "ORDINÁRIAS A SEREM REALIZADOS NO EXERCÍCIO DE 2020.", boldItalicUnderlineVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(addTab().concat("Não obstante os percentuais apurados, a fiscalização colheu ",
                formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" informações sobre a situação da oferta de vagas escolares, com discriminação por faixas " +
                                "etárias, conforme consta da tabela adiante:",
                        formatacaoFactory.getJustificado(12)));

        addTabelaVagasEscolares();

        addBreak();

        addParagrafo(new TextoFormatado("NÃO SENDO CONSTATADO DÉFICIT EM QUALQUER DOS NÍVEIS DA TABELA ACIMA USAR" +
                " O TEXTO ADIANTE:", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Na verificação das informações fornecidas pelo setor de educação do município," +
                        " não constatamos a ocorrência de déficit em qualquer dos níveis de ensino.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("NO CASO DE DÉFICIT ENTRE DEMANDA E OFERTA DE VAGAS, DEVERÁ A FISCALIZAÇÃO" +
                " VERIFICAR:", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("- A RESPOSTA DADA À SEGUINTE QUESTÃO QUE CONSTA DO QUESTIONÁRIO DO " +
                "IEG-M – PERSPECTIVA I-EDUC : A PREFEITURA MUNICIPAL FEZ UMA PESQUISA/ESTUDO PARA LEVANTAR O NÚMERO DE " +
                "CRIANÇAS QUE NECESSITAVAM DE CRECHES, PRÉ-ESCOLA OU ENSINO FUNDAMENTAL?", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("- QUAIS AS MEDIDAS TEM SIDO ADOTADAS PELA PREFEITURA PARA ZERAR O" +
                " DÉFICIT APURADO;", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("- SE HÁ PROJETOS NAS PEÇAS DE PLANEJAMENTO QUE COMTEMPLEM OBRAS DE" +
                " CONTRUÇÃO OU AMPLIAÇÃO DE CRECHES OU ESCOLAS;", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("- SE HÁ OBRAS PARA CONSTRUÇÃO DE CRECHES OU ESCOLAS QUE ESTEJAM ATRASADAS" +
                " OU PARALISADAS, TRAZENDO NOTÍCIAS SOBRE QUAIS AS CAUSAS DE TAL SITUAÇÃO.", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("UTILIZAR O CONTEÚDO ADIANTE SOMENTE QUANDO CONSTATADO PELA FISCALIZAÇÃO QUE NÃO " +
                                "FOI EMPENHADO/LIQUIDADO/PAGO O MÍNIMO CONSTITUCIONAL DE 25% OU NÃO APLICADO " +
                                "TODO O FUNDEB (SEJA PELA APLICAÇÃO DE MENOS DE 95% ATÉ 31/12, OU PELA NÃO " +
                                "APLICAÇÃO DA PARCELA DIFERIDA) OU 60% DO FUNDEB (FALTA DE EMPENHAMENTO)." +
                                " VERIFICAR POSSÍVEIS EXCLUSÕES DE RESTOS A PAGAR NÃO QUITADOS E DESPESAS " +
                                "NÃO AFETAS AO ENSINO.",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Conforme informado ao Sistema Audesp, a despesa educacional atingiu ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da receita resultante de impostos, ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% do FUNDEB recebido, sendo ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% na aplicação com magistério.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("De nossa parte, verificamos o que segue:",
                formatacaoFactory.getJustificado(12)));


        addTabelaFundebNossaParte();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO: É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES/DEDUÇÕES",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": CASO EXISTIR PARCELA DIFERIDA DO FUNDEB (ATÉ 5%), PREENCHER O QUADRO A SEGUIR:",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();


        addTabelaAplicacaoFundebResidual_31_03_19();

        addBreak();
        addParagrafo(addTab().concat("Conforme apurado pela Fiscalização, o Município aplicou ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% ", formatacaoFactory.getJustificado(12))
                .concat("não ", formatacaoFactory.getBold(12))
                .concat("cumprindo o art. 212 da Constituição Federal.", formatacaoFactory.getJustificado(12))
        );
        addParagrafo(addTab().concat("Com base no art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, foi o " +
                        "Município alertado tempestivamente, por  ",
                formatacaoFactory.getJustificado(12))
                .concat("(nº de alertas Audesp)", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("vezes, sobre possível não atendimento dos mínimos constitucionais e legais da Educação.",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("VERIFICAÇÃO DO ART.21 FUNDEB",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("QUANDO FOR APLICADO 100% DO FUNDEB RECEBIDO",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Houve utilização de todo o FUNDEB recebido, inclusive pagamentos dos Restos a Pagar (",
                formatacaoFactory.getJustificado(12))
                .concat("se for o caso", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("), cumprindo o Município o art. 21 da Lei Federal nº 11.494, de 20 de junho de 2007.",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("QUANDO NÃO FOR APLICADO 100% DO FUNDEB RECEBIDO",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("No exercício em exame foi aplicado ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% do FUNDEB recebido, ", formatacaoFactory.getJustificado(12))
                .concat("não observando / observando", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("o percentual mínimo de 95%, sendo que, por meio de conta bancária vinculada (", formatacaoFactory.getJustificado(12))
                .concat("ou não", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("), constatamos a ", formatacaoFactory.getJustificado(12))
                .concat("não utilização / utilização ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("da parcela diferida no 1º trimestre do exercício seguinte, ", formatacaoFactory.getJustificado(12))
                .concat("atendendo-se / não se atendendo ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("ao § 2º do art. 21 da Lei Federal nº 11.494, de 20 de junho de 2007.", formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("VERIFICAÇÃO DO ART.60 ADCT",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("CUMPRIMENTO AO ART. 60, XII, DO ADCT",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Demais disso, (após os ajustes efetuados pela Fiscalização, ",
                formatacaoFactory.getJustificado(12))
                .concat("se for o caso", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(") verificamos que relativamente ao FUNDEB, empregou o Município ",
                        formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% na remuneração dos profissionais do magistério da Educação Básica, dando cumprimento " +
                                "ao art. 60, XII, do Ato das Disposições Constitucionais Transitórias. ",
                        formatacaoFactory.getJustificado(12))
        );


        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("NÃO CUMPRIMENTO AO ART. 60, XII, DO ADCT",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Demais disso, (após os ajustes efetuados pela Fiscalização, ",
                formatacaoFactory.getJustificado(12))
                .concat("se for o caso", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(") verificamos que relativamente ao FUNDEB, empregou o Município ",
                        formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% na remuneração dos profissionais do magistério da Educação Básica, ",
                        formatacaoFactory.getJustificado(12))
                .concat("não ",
                        formatacaoFactory.getBold(12))
                .concat("dando cumprimento " +
                                "ao art. 60, XII, do Ato das Disposições Constitucionais Transitórias. ",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO: PARA CUMPRIMENTO DA NT SDG 131/2016, A FISCALIZAÇÃO ",
                boldItalicVermelhoAmareloJustificado12)
                .concat("DEVERÁ IDENTIFICAR",
                        boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(" OS VALORES EFETIVAMENTE DESPENDIDOS COM INATIVOS NO MAGISTÉRIO INCLUÍDOS NOS MÍNIMOS " +
                                "CONSTITUCIONAIS, A QUALQUER TÍTULO. (INCLUSIVE REPASSE FINANCEIRO OU APORTE PARA COBERTURA " +
                                "DE DÉFICIT PREVIDENCIÁRIO)",
                        boldItalicUnderlineVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("HAVENDO GASTOS COM INATIVOS", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO: PARA CUMPRIMENTO DA NT SDG 131/2016, A FISCALIZAÇÃO ",
                boldItalicVermelhoAmareloJustificado12)
                .concat("DEVERÁ IDENTIFICAR",
                        boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(" OS VALORES EFETIVAMENTE DESPENDIDOS COM INATIVOS NO MAGISTÉRIO INCLUÍDOS NOS MÍNIMOS " +
                                "CONSTITUCIONAIS, A QUALQUER TÍTULO. (INCLUSIVE REPASSE FINANCEIRO OU APORTE PARA COBERTURA " +
                                "DE DÉFICIT PREVIDENCIÁRIO)",
                        boldItalicUnderlineVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("A Fiscalização identificou valores despendidos com inativos do magistério incluídos nos" +
                        " mínimos constitucionais do Ensino, na seguinte conformidade:",
                formatacaoFactory.getJustificado(12)));

        addParagrafo(new TextoFormatado("DESCREVER", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("NÃO HAVENDO GASTOS COM INATIVOS",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("A Fiscalização não identificou valores despendidos com inativos do " +
                        "magistério incluídos nos mínimos constitucionais do Ensino.",
                formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("ANÁLISE OBRIGATÓRIA",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": OFERTA DE VAGAS NO ENSINO",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO: A VERIFICAÇÃO INICIAL DEVERÁ SER EFETUADA NA VISITA " +
                "DO 1º QUADRIMESTRE DE ACOMPANHAMENTO 2019 OU NO FECHAMENTO DE 2018 (ORDINÁRIAS E FECHAMENTOS DE " +
                "ACOMPANHAMENTOS 2018). O ASSUNTO TAMBÉM DEVERÁ CONSTAR DOS RELATÓRIOS DE FECHAMENTO DOS ACOMPANHAMENTOS " +
                "DE 2019 E ORDINÁRIAS A SEREM REALIZADOS NO EXERCÍCIO DE 2020.",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();
        addBreak();
        addParagrafo(addTab().concat("Não obstante os percentuais apurados, a fiscalização colheu ",
                formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" informações sobre a situação da oferta de vagas escolares, com discriminação por " +
                        "faixas etárias, conforme consta da tabela adiante:", formatacaoFactory.getJustificado(12)));

        addTabelaVagasEscolares();

        addBreak();
        addParagrafo(new TextoFormatado("NÃO SENDO CONSTATADO DÉFICIT EM QUALQUER DOS NÍVEIS DA TABELA ACIMA USAR O TEXTO ADIANTE:",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Na verificação das informações fornecidas pelo setor de educação do município, não " +
                        "constatamos a ocorrência de déficit em qualquer dos níveis de ensino.",
                formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("NO CASO DE DÉFICIT ENTRE DEMANDA E OFERTA DE VAGAS, DEVERÁ A FISCALIZAÇÃO VERIFICAR:",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("- A RESPOSTA DADA À SEGUINTE QUESTÃO QUE CONSTA DO QUESTIONÁRIO DO IEG-M – PERSPECTIVA " +
                "I-EDUC : A PREFEITURA MUNICIPAL FEZ UMA PESQUISA/ESTUDO PARA LEVANTAR O NÚMERO DE CRIANÇAS QUE " +
                "NECESSITAVAM DE CRECHES, PRÉ-ESCOLA OU ENSINO FUNDAMENTAL?", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("- QUAIS AS MEDIDAS TEM SIDO ADOTADAS PELA PREFEITURA PARA ZERAR " +
                "O DÉFICIT APURADO;", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("- SE HÁ PROJETOS NAS PEÇAS DE PLANEJAMENTO QUE COMTEMPLEM OBRAS DE CONTRUÇÃO " +
                "OU AMPLIAÇÃO DE CRECHES OU ESCOLAS;", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("- SE HÁ OBRAS PARA CONSTRUÇÃO DE CRECHES OU ESCOLAS QUE ESTEJAM ATRASADAS OU " +
                "PARALISADAS, TRAZENDO NOTÍCIAS SOBRE QUAIS AS CAUSAS DE TAL SITUAÇÃO.", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("AJUSTES DA FISCALIZAÇÃO", formatacaoFactory.getBold(12)));
        addBreak();

        addTabelaAjustesFiscalizacaoEnsino();

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÕES:",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES/DEDUÇÕES",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO: O TRIBUNAL TEM ENTENDIDO QUE, MESMO QUE APLICADO 100% DO " +
                "FUNDEB (SEM PARCELA DIFERIDA), PODE-SE PAGAR, ATÉ O LIMITE DE 5%, RESTOS A PAGAR DO FUNDEB " +
                "ATÉ 31.03 DO EXERCÍCIO SEGUINTE, E NÃO SOMENTE ATÉ 31.01. VIDE PROCESSOS TC-2915/026/10; " +
                "TC-210/026/09; TC-74/026/09 E TC-2159/026/08.", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("ESPECIFICAR OS AJUSTES DA SEGUINTE FORMA:",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("AJUSTES: DESPESAS COM FUNDEB - 60%", formatacaoFactory.getBold(12)));
        addBreak();
        addBreak();


        addParagrafo(new TextoFormatado("AJUSTES: DESPESAS COM FUNDEB - 40%", formatacaoFactory.getBold(12)));
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("AJUSTES: DESPESAS COM RECURSOS PRÓPRIOS", formatacaoFactory.getBold(12)));
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("C.2. IEG-M – I-EDUC – Índice " + resultadoIegm2018.getFaixaIEduc(),
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo. ", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("É possível consultar no link ",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("https://pne.tce.mg.gov.br/#/public/inicio",
                        formatacaoFactory.getBoldItalicUnderlineAzulJustificado(12))
                .concat(" se os municípios atingiram as metas previstas no Plano Nacional de Educação-PNE. " +
                                "Os resultados se referem ao exercício de 2016.",
                        boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addApontamentosODS("i-educ");
        addBreak();
        addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice III - ODS",
                formatacaoFactory.getJustificado(12)));
        addBreak();


        addSecao(new TextoFormatado("PERSPECTIVA D: SAÚDE", formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("D.1. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": SE CONSTATADO PELA FISCALIZAÇÃO QUE FOI EMPENHADO MAIS QUE O MÍNIMO UTILIZAR " +
                                "O CONTEÚDO ADIANTE.",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Conforme informado ao Sistema Audesp, a aplicação na Saúde atingiu, no " +
                        "período, os seguintes resultados, cumprindo a referida determinação constitucional/legal:",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addTabelaEmpenhadaLiquidadaPaga();

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": PAGAMENTO SUPERIOR A 15%:",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Tendo em vista que foi liquidado e pago montante acima de 15% da receita " +
                        "de impostos, atendendo ao piso constitucional, deixamos de efetuar o acompanhamento " +
                        "previsto no art. 24 da Lei Complementar Federal nº 141, de 13 de janeiro de 2012.",
                formatacaoFactory.getJustificado(12)));

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": PAGAMENTO INFERIOR A 15%, MAS COM RP NÃO PROCESSADO COM LASTRO:",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("De anotar que R$ ",
                formatacaoFactory.getJustificado(12))
                .concat("xx,xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% daquela receita, ingressaram por se referirem a Restos a Pagar Não " +
                        "Processados com lastro nas contas bancárias da Saúde em 31 de dezembro do " +
                        "exercício em exame, conforme possibilita o inciso II do art. 24 da Lei Complementar " +
                        "Federal nº 141, de 13 de janeiro de 2012.", formatacaoFactory.getJustificado(12))
        );


        addParagrafo(addTab().concat("Do sobredito montante, R$ ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" foram pagos até o momento da fiscalização, sendo que, ainda não quitado, o valor ",
                        formatacaoFactory.getJustificado(12))
                .concat("faltante (ou parte dele) permanece", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" depositado em conta bancária da Saúde, suficientes a perfazer a aplicação do mínimo exigido.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": QUANDO CONSTATADO PELA FISCALIZAÇÃO QUE NÃO FOI APLICADO O MÍNIMO CONSTITUCIONAL " +
                                "(SEJA POR FALTA DE EMPENHAMENTO OU POR EXCLUSÃO DE RESTOS A PAGAR [INCLUSIVE DE RP " +
                                "NÃO PROCESSADOS, INICIALMENTE COM LASTRO EM 31/12, MAS INEXISTENTE NO MOMENTO DA " +
                                "FISCALIZAÇÃO], E DESPESAS NÃO AFETAS À SAÚDE)",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Conforme informado ao Sistema Audesp, a aplicação em ações e serviços de Saúde alcançou  ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("%", formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(addTab().concat("De nossa parte, informamos o apurado após a presente fiscalização:",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addTabelaSaudeReceitasDespesas();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÕES:", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Conforme apuramos, o Município aplicou ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da receita de impostos na Saúde, não observando o piso constitucional de 15%.",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, o" +
                " Município foi alertado tempestivamente, por ", formatacaoFactory.getJustificado(12))
                .concat("(nº de alertas Audesp)", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" vezes, sobre possível não atendimento do mínimo constitucional da Saúde.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("UTILIZAR OS PARÁGRAFOS ABAIXO, SE NECESSÁRIO, ADEQUANDO CONFORME A APURAÇÃO DO CASO",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("UTILIZAR OS PARÁGRAFOS ABAIXO, SE NECESSÁRIO, ADEQUANDO CONFORME A APURAÇÃO DO CASO",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("De anotar que R$ ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% daquela receita, ingressaram por se referirem a Restos a Pagar Não Processados " +
                                "com lastro nas contas bancárias da Saúde em 31 de dezembro do exercício em exame, conforme " +
                                "possibilita o inciso II do art. 24 da Lei Complementar Federal nº 141, de 13 de janeiro de 2012.",
                        formatacaoFactory.getJustificado(12)));


        addParagrafo(addTab().concat("Do sobredito montante, R$ ", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" foram pagos até o momento da fiscalização, sendo que, ainda não quitado, o valor ",
                        formatacaoFactory.getJustificado(12))
                .concat("faltante (ou parte dele) não permanece", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" depositado em conta bancária da Saúde, resultando na insuficiência para perfazer " +
                                "a aplicação do mínimo exigido.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO: ATENÇÃO AO CÔMPUTO DOS RESTOS A PAGAR NO MÍNIMO DE APLICAÇÃO, E A " +
                "CONTINUIDADE DE SEU ACOMPANHAMENTO, TENDO EM VISTA O PREVISTO NO ART. 24., §§ 1º E 2º DA LCF Nº 141/2012",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("AJUSTES DA FISCALIZAÇÃO", formatacaoFactory.getBold(12)));

        addBreak();
        addTabelaAjustesFiscalizacaoSaude();

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÕES:", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addSecao(new TextoFormatado("D.2. IEG-M – I-SAÚDE – Índice "+ resultadoIegm2018.getFaixaISaude(),
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo. ", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat("SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addApontamentosODS("i-saúde");
        addBreak();
        addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice III - ODS",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addBreak();
        addSecao(new TextoFormatado("PERSPECTIVA E: GESTÃO AMBIENTAL",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("E.1. IEG-M – I-AMB – Índice " + resultadoIegm2018.getFaixaIAmb(),
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo. ", boldItalicVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addParagrafo(new TextoFormatado("O ITEM E.2. PROCESSOS DE LICENCIAMENTO AMBIENTAL SOMENTE SERÁ " +
                "PREENCHIDO NOS RELATÓRIOS DOS MINICÍPIOS LISTADOS NA TABELA ADIANTE. NOS DEMAIS, " +
                "O ITEM “E.2” DEVERÁ SER EXCLUÍDO.", boldItalicVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("(O papel de trabalho, a ser preenchido durante a fiscalização" +
                " in loco e inserido no processo eletrônico das contas anuais, encontra-se disponível, " +
                "na pasta pública dos DSFs - P:\\DSFs\\FISCALIZAÇÃO\\MODELOS DE RELATÓRIOS\\PREFEITURA).",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addApontamentosODS("i-amb");
        addBreak();
        addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice III - ODS",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA F: GESTÃO DA PROTEÇÃO À CIDADE",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("F.1. IEG-M – I-CIDADE – Índice " + resultadoIegm2018.getFaixaICidade(),
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo. ", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();
        addApontamentosODS("i-cidade");
        addBreak();
        addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice III - ODS",
                formatacaoFactory.getJustificado(12)));
        addBreak();


        /////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////
        ////////////////////////// FAZER O MERGE ////////////////////////////
        /////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////
        addSecao(new TextoFormatado("PERSPECTIVA G: TECNOLOGIA DA INFORMAÇÃO", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addSecao(new TextoFormatado("G.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addSecao(new TextoFormatado("G.1.1. A LEI DE ACESSO À INFORMAÇÃO E A LEI DA TRANSPARÊNCIA FISCAL",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();
        addParagrafo(new TextoFormatado("AQUI SERÃO INFORMADAS CONSTATAÇÕES SOBRE SIC, TRANSPARÊNCIA E PUBLICIDADE DE GESTÃO DAS PM’S",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addSecao(new TextoFormatado("G.2. FIDEDIGNIDADE DOS DADOS INFORMADOS AO SISTEMA Audesp",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": SEM DIVERGÊNCIAS",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Nos trabalhos da fiscalização não foram encontradas " +
                        "divergências entre os dados da origem e os prestados ao Sistema Audesp.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": COM DIVERGÊNCIAS",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Como demonstrado no(s) item(ns) ",
                formatacaoFactory.getJustificado(12))
                .concat("xxx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" deste relatório, foram " +
                                "constatadas divergências entre os dados informados pela origem e aqueles apurados " +
                                "no Sistema Audesp.",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addBreak();
        addSecao(new TextoFormatado("G.3. IEG-M – I-GOV TI – Índice " + resultadoIegm2018.getFaixaIGov(),
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo. ",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota" +
                        " nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();

        addApontamentosODS("i-gov ti");
        addBreak();
        addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice III - ODS",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA H: OUTROS ASPECTOS RELEVANTES",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addSecao(new TextoFormatado("H.1. DENÚNCIAS/REPRESENTAÇÕES/EXPEDIENTES",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": NA INEXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Não chegou ao nosso conhecimento a formalização de denúncias," +
                        " representações ou expedientes.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": NA EXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Está referenciado ",
                formatacaoFactory.getJustificado(12))
                .concat("OU", boldItalicCapsVermelhoAmareloJustificado12)
                .concat(" Estão referenciados ao presente processo de contas anuais, o(s)" +
                                " seguinte(s) protocolado(s):",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();

        addTabelaProcessoDeContasAnuais();

        addParagrafo(addTab().concat("O(s) assunto(s) em tela foi(ram) tratado(s) no item(ns) ",
                formatacaoFactory.getJustificado(12))
                .concat("xx ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("deste relatório.",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addBreak();
        addSecao(new TextoFormatado("H.2. ATENDIMENTO À LEI ORGÂNICA, INSTRUÇÕES E RECOMENDAÇÕES DO TRIBUNAL" +
                " DE CONTAS DO ESTADO DE SÃO PAULO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": ATENDIMENTO, ADAPTAR CONFORME O CASO",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("No decorrer do exercício em análise, constatamos" +
                        " o atendimento à Lei Orgânica e às Instruções deste Tribunal, bem como não constatamos" +
                        " desatendimento às recomendações exaradas nos dois últimos exercícios analisados. ",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": CASOS NÃO ATENDIDOS, ADAPTAR CONFORME O CASO",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Por outro lado, constatamos o não atendimento à Lei" +
                        " Orgânica e às Instruções deste Tribunal, conforme a seguir:",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("(mencionar o nº do processo de controle de prazos e julgador, se houver)",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("No que se refere às recomendações desta Corte, haja vista os dois últimos" +
                        " exercícios apreciados, verificamos que, no exercício em exame, a Prefeitura descumpriu as seguintes:",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();


        ParecerPrefeitura ultimo = pareceresPrefeiturasList.size() > 0 ? pareceresPrefeiturasList.get(pareceresPrefeiturasList.size()-1) : null;
        ParecerPrefeitura penultimo = pareceresPrefeiturasList.size() > 1 ? pareceresPrefeiturasList.get(pareceresPrefeiturasList.size()-2) : null;

        addTabelaDoisUltimosExerciciosApreciados(ultimo);

        addBreak();

        addTabelaDoisUltimosExerciciosApreciados(penultimo);

        addBreak();

        addSecao(new TextoFormatado("SÍNTESE DO APURADO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addTabelaSinteseDoApurado();
        addBreak();
        addSecao(new TextoFormatado("CONCLUSÃO",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addParagrafo(addTab().concat("Observada a instrução constante" +
                        " no art. 24 da Lei Orgânica do Tribunal de Contas do Estado de São Paulo," +
                        " a Fiscalização, em conclusão a seus trabalhos, aponta as seguintes ocorrências:",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("Descrever resumidamente as falhas",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("À consideração de Vossa Senhoria.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(addTab().concat("DF-X / UR-X, em ... de ............ de 2019.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addBreak();
        addBreak();
        addParagrafo(new TextoFormatado("Nome",
                formatacaoFactory.getBoldCenter(12))
        );
        addParagrafo(new TextoFormatado("Agente da Fiscalização",
                formatacaoFactory.getBoldCenter(12))
        );
        addBreak();

        addSecao(new TextoFormatado("APÊNDICES AO MODELO DE RELATÓRIO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addSecao(new TextoFormatado("APÊNDICE I",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addSecao(new TextoFormatado("Este Apêndice contém sugestões/padrões de abordagem" +
                " para itens específicos a serem, eventualmente, inseridos no Relatório.",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(new TextoFormatado("A.2. IEG-M – I-PLANEJAMENTO – Índice " + resultadoIegm2018.getFaixaIPlanejamento(),
                formatacaoFactory.getBold(12))
        );
        addParagrafo(new TextoFormatado("B.2. IEG-M – I-FISCAL – Índice "+ resultadoIegm2018.getFaixaIFiscal(),
                formatacaoFactory.getBold(12))
        );
        addParagrafo(new TextoFormatado("C.2.\tIEG-M – I-EDUC – Índice "+ resultadoIegm2018.getFaixaIEduc(),
                formatacaoFactory.getBold(12))
        );
        addParagrafo(new TextoFormatado("D.2. IEG-M – I-SAÚDE – Índice "+ resultadoIegm2018.getFaixaISaude(),
                formatacaoFactory.getBold(12))
        );
        addParagrafo(new TextoFormatado("E.1. IEG-M – I-AMB – Índice "+ resultadoIegm2018.getFaixaIAmb(),
                formatacaoFactory.getBold(12))
        );

        addParagrafo(new TextoFormatado("E.2. PROCESSOS DE LICENCIAMENTO AMBIENTAL",
                formatacaoFactory.getBold(12))
        );
        addParagrafo(new TextoFormatado("F.1. IEG-M – I-CIDADE – Índice "+ resultadoIegm2018.getFaixaICidade(),
                formatacaoFactory.getBold(12))
        );
        addParagrafo(new TextoFormatado("G.3. IEG-M – I-GOV TI – Índice "+ resultadoIegm2018.getFaixaIGov(),
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(addTab().concat("Utilizar os quadros abaixo para informar a" +
                        " ocorrência de Contratos, Repasses, Ordenadas, conforme o caso, nos itens acima elencados:",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();

        addTabelaOcorrenciaDeContratos("Contratada");
        addBreak();
        addTabelaOcorrenciaDeContratos("Convenente");
        addBreak();
        addTabelaFiscalizacaoOrdenada();

        addBreak();

        addParagrafo(new TextoFormatado("B.3. OUTROS PONTOS DE INTERESSE",
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(addTab().concat("Os exames efetuados ",
                formatacaoFactory.getJustificado(12))
                .concat("in loco: ", formatacaoFactory.getItalic(12))
                .concat("evidenciaram, ainda, as seguintes impropriedades dignas de nota: ",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();

        addParagrafo(new TextoFormatado("B.3.X. FISCALIZAÇÃO DAS RECEITAS ",
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": CASO O SUBITEM SEJA ELEITO PELA FISCALIZAÇÃO, MAS " +
                                "CONSIDERADO IRREGULAR, O SUBITEM SERÁ INICIADO PELO TEXTO" +
                                " COMPLEMENTADO PELO QUADRO",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Mediante confronto do Balancete da Receita com" +
                        " as informações franqueadas pela Secretaria Estadual da Fazenda, Fundo Nacional" +
                        " de Saúde – FNS, Ministério da Fazenda/STN, Banco do Brasil/DAF (Distribuição de Arrecadação" +
                        " Federal) e Portal da Transparência, observamos as seguintes diferenças:",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();

        addTabelaInformacoesFranqueadas();

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": CASO O SUBITEM SEJA ELEITO PELA FISCALIZAÇÃO," +
                                " MAS CONSIDERADO IRREGULAR EM RELAÇÃO A COBRANÇA DO " +
                                "ISSQN- COMUNICADO SDG Nº 37/2009",
                        boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Verificamos que, no tocante à atividade dos cartórios," +
                        " não adotou a Municipalidade as providências para a cobrança do Imposto sobre" +
                        " Serviços de Qualquer Natureza – ISSQN, desatendendo ao art. 11 da Lei de Responsabilidade Fiscal.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. RENÚNCIA DE RECEITAS",
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(addTab().concat("No exercício examinado, o Município efetivou" +
                        " renúncia de receita irregular, pois que nisso ocorreram os seguintes desacertos:",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. DÍVIDA ATIVA",
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addTabelaDividaAtiva();
        addBreak();
        addParagrafo(new TextoFormatado("Constatamos as seguintes irregularidades: ",
                formatacaoFactory.getCenter(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. MULTAS DE TRÂNSITO",
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(addTab().concat("A Prefeitura ",
                formatacaoFactory.getJustificado(12))
                .concat("não cumpriu / cumpriu ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("as disposições do art. 320 da Lei Federal nº 9.503 de 23 de setembro de 1997" +
                                " (Código de Trânsito Brasileiro).",
                        formatacaoFactory.getJustificado(12))
        );
        addParagrafo(addTab().concat("Verificamos, também, o ",
                formatacaoFactory.getJustificado(12))
                .concat("recolhimento / não recolhimento ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("ao FUNSET, correspondente a 5% das multas arrecadadas (art. 320, § 1º, do sobredito Código)." +
                        "", formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addTabelaMultasDeTransito();
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO:", boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.", boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. CIDE - CONTRIBUIÇÃO DE INTERVENÇÃO NO DOMÍNIO ECONÔMICO",
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(addTab().concat("Essa receita ",
                formatacaoFactory.getJustificado(12))
                .concat("não foi / foi ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("aplicada tal qual prescrevem os arts. 1º-A e 1º-B da Lei Federal nº 10.336," +
                                " de 19 de dezembro de 2001.",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addTabelaMultasDeTransito2();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO:", boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.", boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. ROYALTIES", formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": HÁ CONTA BANCÁRIA VINCULADA", boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("Por meio de conta bancária vinculada, o Município não aplicou corretamente" +
                        " tal receita, nos moldes do art. 8º da Lei Federal nº 7.990, de 28 de dezembro de 1989.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                boldItalicUnderlineCapsVermelhoAmareloJustificado12)
                .concat(": NÂO HÁ CONTA BANCÁRIA VINCULADA", boldItalicCapsVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(addTab().concat("O Município não movimenta, em conta vinculada, sua receita de Royalties," +
                        " daí ensejando o desvio de finalidade combatido no parágrafo único" +
                        " do art. 8º da Lei de Responsabilidade Fiscal.",
                formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addTabelaReceitadeRoyalties();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO:", boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.", boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Nesse contexto, verifica-se que R$ ",
                formatacaoFactory.getJustificado(12))
                .concat("xx ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("(", formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da disponibilidade total) foram indevidamente gastos em despesas" +
                                " de pessoal e de serviço da dívida (juros e principal, desde que não pagos à União)," +
                                " desatendendo-se o art. 8º da Lei Federal nº 7.990, de 28 de dezembro de 1989.",
                        formatacaoFactory.getJustificado(12))
        );
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. ILUMINAÇÃO PÚBLICA",
                formatacaoFactory.getBold(12))
        );
        addBreak();
        addParagrafo(addTab().concat("O Município instituiu a CIP – Contribuição para Custeio da Iluminação Pública," +
                " por meio de Lei Complementar Municipal nº ", formatacaoFactory.getJustificado(12))
                .concat("xx ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("Lei Municipal nº", formatacaoFactory.getJustificado(12))
                .concat(" XX ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", de", formatacaoFactory.getJustificado(12))
                .concat(" XX ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("de", formatacaoFactory.getJustificado(12))
                .concat(" XX ", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("de", formatacaoFactory.getJustificado(12))
                .concat(" XXXX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", cuja arrecadação e despesas relacionadas à manutenção dos serviços correlatos," +
                        " tiveram a seguinte configuração no exercício em exame:", formatacaoFactory.getJustificado(12))

        );
        addBreak();
        addTabelaIluminacaoPublica();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO:", boldItalicUnderlineCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES.", boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("DESCREVER AS IRREGULARIDADES CONSTATADAS.", boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. DEMAIS DESPESAS ELEGÍVEIS PARA ANÁLISE ", formatacaoFactory.getBold(12)));
        addBreak();
        addParagrafo(new TextoFormatado("ATENÇÃO: OBSERVAR O LIMITE (MÍNIMO) ESTABELECIDO DE 500 UFESP´S" +
                " PARA ABERTURA DE APARTADO (RES. 4/2015 C/C RES. 6/2016) ", boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Na amostra, o exame documental mostrou falhas relevantes que para melhor análise, protocolamos" +
                " o(s) Expedientes (s) 0000/000/00 que abriga(m) o(s) seguinte(s) desacerto(s):", formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. TESOURARIA / ALMOXARIFADO / BENS PATRIMONIAIS ", formatacaoFactory.getBold(12)));
        addBreak();
        addParagrafo(new TextoFormatado("ADAPTAR O TÍTULO DO ITEM PARA O SETOR EM QUE SE CONSTATOU FALHAS ",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(addTab().concat("Segundo nossos testes, verificamos as seguintes impropriedades no setor:",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("B.3.X. ORDEM CRONOLÓGICA DE PAGAMENTOS", formatacaoFactory.getBold(12)));
        addBreak();
        addParagrafo(addTab().concat("Constatamos a inobservância da ordem cronológica de pagamentos, tendo em vista .",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("MENCIONAR SE HOUVE EMISSÃO DE ALERTAR",
                boldItalicCapsVermelhoAmareloJustificado12));
        addBreak();


        addBreak();
        addSecao(new TextoFormatado("B.3.X. FORMALIZAÇÃO DAS LICITAÇÕES, INEXIGIBILIDADES E DISPENSAS",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("Neste item serão tratados especialmente casos sobre os quais recaiam Denúncias " +
                "(formalizadas perante este Tribunal), porém, sem falhas suficientes para seleção do contrato, ou para " +
                "os quais não seja possível sua seleção (p. ex. contrato encerrado). OS CASOS ENVOLVENDO OUTROS ITENS" +
                " DO RELATÓRIO, P. EX., ENSINO, SAÚDE, AMBIENTAL (RESÍDUOS SÓLIDOS), DEVERÃO SER TRATADOS NOS " +
                "RESPECTIVOS ITENS, E NÃO NESTE.", boldItalicUnderlineVermelhoAmareloJustificado12));
        addParagrafo(new TextoFormatado("O intuito do relatório sempre é diagnosticar a efetividade das ações " +
                "governamentais, de modo que, uma falha de execução contratual que impacte determinado " +
                "programa/ação, deve estar relacionada ao seu tema.", boldItalicUnderlineVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("PARA OS CONTRATOS AQUI EXCEPCIONADOS DEVERÁ SER SOLICITADA AUTORIZAÇÃO VIA DSF " +
                "PARA QUE ESTE(S) SEJAM INSERIDOS NAQUELES A SEREM ACOMPANHADOS APÓS SELEÇÃO",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addBreak();

        addParagrafo(addTab().concat("Conforme dados encaminhados ao Sistema Audesp, assim se compôs a despesa da Prefeitura:",
                formatacaoFactory.getJustificado(12)));

        addTabelaModalidadeValoresPercentual();

        addBreak();
        addSecao(new TextoFormatado("B.3.X. CONTRATOS DE CONCESSÃO / PERMISSÃO DE SERVIÇOS PÚBLICOS / PARCERIAS PÚBLICO-PRIVADA (PPP)",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();


        addTabelaVerificacaoPPP();

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO", boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("AS LINHAS DEVERÃO SER PREENCHIDAS PELA FISCALIZAÇÃO COM A OPÇÃO: “SIM”, " +
                "“NÃO” OU “PREJUDICADO”;", boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("SE AS OPÇÕES DAS LINHAS FOREM: LINHA 2, 3, 4 “NÃO” OU LINHA 5 “SIM”, ESPECIFICAR:",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addTabelaVerificacaoContratoDeConcessao();

        addBreak();
        addParagrafo(new TextoFormatado("OBSERVAÇÃO", boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("AS LINHAS DEVERÃO SER PREENCHIDAS PELA FISCALIZAÇÃO COM A " +
                "OPÇÃO: “SIM”, “NÃO” OU “PREJUDICADO”;", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(new TextoFormatado("SE AS OPÇÕES DAS LINHAS FOREM: LINHA 2, 3, 4 “NÃO” OU LINHA 5 “SIM”, ESPECIFICAR.",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addSecao(new TextoFormatado("E.2. PROCESSOS DE LICENCIAMENTO AMBIENTAL",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("Item de preenchimento obrigatório somente para os seguintes municípios:",
                boldItalicVermelhoAmareloJustificado12));

        addTabelaLicenciamentoAmbiental();


        addBreak();

        addParagrafo(addTab().concat("Realizamos o exame amostral da legalidade dos processos de licenciamento ambiental" +
                        " instaurados pela Administração Municipal, no decorrer do exercício examinado, nos termos " +
                        "do disposto no inciso XIV, do art. 9º, da Lei Complementar Federal nº 140, de 8 de dezembro " +
                        "de 2011, não sendo falhas significativas.",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("(OU)", boldItalicVermelhoAmareloJustificado12));
        addBreak();

        addParagrafo(addTab().concat("Realizamos o exame amostral da legalidade dos processos de licenciamento ambiental " +
                        "instaurados pela Administração Municipal, no decorrer do exercício examinado, nos termos do " +
                        "disposto no inciso XIV, do art. 9º, da Lei Complementar Federal nº 140, de 8 de dezembro de" +
                        " 2011, sendo encontradas as falhas a seguir relacionadas:",
                formatacaoFactory.getJustificado(12)));


        addBreak();

        addParagrafo(new TextoFormatado("(RELACIONAR AS FALHAS ENCONTRADAS E LEVAR PARA A CONCLUSÃO)",
                boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addSecao(new TextoFormatado("APÊNDICE II",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addParagrafo(new TextoFormatado("Estas orientações e aplicam a todas as dimensões do IEG-M.",
                formatacaoFactory.getVermelhoAmarelo(12)));
        addParagrafo(new TextoFormatado("(CONSTAR O ÍNDICE E TRAÇAR COMPARATIVO COM O RESULTADO DAS VERIFICAÇÕES “IN LOCO”).",
                boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("Neste item deverão ser relatadas as irregularidades constatadas a partir dos " +
                "pontos eleitos para verificação, conforme relevância, especialmente baseando-se em questões do IEG-M " +
                "que revelam pontos fracos (ou pontos sensíveis) da Administração e/ou Município.", boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("Analisar, conforme a relevância e o planejamento da fiscalização, " +
                "aspectos envolvendo o planejamento setorial (Planos Municipais), o planejamento orçamentário/financeiro " +
                "(PPA, LDO e LOA), bem como sua eventual execução (despesas decorrentes), notadamente buscando" +
                " averiguar aspectos de (Manual ANOP-TCU):", boldItalicVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("- ", boldItalicVermelhoAmareloJustificado12)
                .concat("economicidade", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": minimização dos custos dos recursos utilizados na consecução de uma atividade, sem comprometimento dos padrões de qualidade.",
                        boldItalicVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("- ", boldItalicVermelhoAmareloJustificado12)
                .concat("eficácia", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": o grau de alcance das metas programadas (bens e serviços) em um determinado período de tempo, " +
                                "independentemente dos custos implicados.",
                        boldItalicVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("- ", boldItalicVermelhoAmareloJustificado12)
                .concat("eficiência", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": a relação entre os produtos (bens e serviços)4 gerados por uma atividade e os custos " +
                                "dos insumos empregados para produzi-los, em um determinado período de tempo, " +
                                "mantidos os padrões de qualidade.",
                        boldItalicVermelhoAmareloJustificado12));

        addParagrafo(new TextoFormatado("- ", boldItalicVermelhoAmareloJustificado12)
                .concat("efetividade", boldItalicUnderlineVermelhoAmareloJustificado12)
                .concat(": alcance dos resultados pretendidos, a médio e longo prazo. Refere-se à relação entre " +
                                "os resultados de uma intervenção ou programa, em termos de efeitos sobre a " +
                                "população-alvo (impactos observados), e os objetivos pretendidos (impactos esperados), " +
                                "traduzidos pelos objetivos finalísticos da intervenção. Trata-se de verificar a " +
                                "ocorrência de mudanças na população-alvo que se poderia razoavelmente atribuir " +
                                "às ações do programa avaliado.",
                        boldItalicVermelhoAmareloJustificado12));

        addBreak();

        addParagrafo(new TextoFormatado("As questões que prejudicam a política pública deste item do relatório " +
                "deverão ser aqui relatadas, ainda que abranjam outros itens (p. ex., mau planejamento " +
                "de programas e ações no PPA). Pois, o que se busca é demonstrar o sucesso ou não da" +
                " execução finalística da ação governamental, correlacionado ao IEG-M. ", boldItalicVermelhoAmareloJustificado12));
        addBreak();
        addParagrafo(new TextoFormatado("Caso existam contratos e repasses selecionados (via seletividade) e fiscalizações " +
                "ordenadas relacionadas à perspectiva em análise, utilizar-se dos quadros exemplificativos disponíveis " +
                "no APÊNDICE I. Para fins do presente item, focar na análise finalística, ou seja, no resultado que a " +
                "execução da política pública está gerando (efetividade).", boldItalicVermelhoAmareloJustificado12));

        addBreak();
        addParagrafo(new TextoFormatado("SE CONSTATADA ALTERAÇÃO ORÇAMENTÁRIA SIGNIFICATIVA, NOTADAMENTE SE APONTADA" +
                " EM CASO DÉFICIT NO ITEM B.1.1 DO RELATÓRIO, DEVEM SER EXPLORADAS EVENTUAIS DEFICIÊNCIAS NO SETOR " +
                "DE PLANEJAMENTO DO ÓRGÃO NESTE ITEM. OU SEJA, DAS DEFICIÊNCIAS DO ÓRGÃO PARA PLANEJAR, RESULTOU-SE " +
                "EM ALTERAÇÕES SIGNIFICATIVAS NO ORÇAMENTO, ESPECIALMENTE FACE AO DÉFICIT ORÇAMENTÁRIO.",
                boldItalicUnderlineVermelhoAmareloJustificado12));
        addBreak();

        addBreak();
        addSecao(new TextoFormatado("APÊNDICE III - ODS",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addParagrafo(new TextoFormatado("METAS DOS OBJETIVOS DE DESENVOLVIMENTO SUSTENTÁVEL (ODS)",
                        formatacaoFactory.getBold(12)));
        addBreak();

        addTabelaODSAcabarComAPobreza();
        addBreak();addBreak(); addBreak();
        addTabelaODSFomeZero();
        addBreak();addBreak(); addBreak();
        addTabelaODSBoaSaude();
        addBreak();addBreak(); addBreak();
        addTabelaODSAssegurarAEducacao();
        addBreak();addBreak(); addBreak();
        addTabelaODSAlcancarAIgualdadeDeGenero();
        addBreak();addBreak(); addBreak();
        addTabelaODSGarantirDisponibilidade();
        addBreak();addBreak(); addBreak();
        addTabelaODSGarantirAcessoAEnergia();
        addBreak();addBreak(); addBreak();
        addTabelaODSPromoverOCrescimentoEconomico();
        addBreak();addBreak(); addBreak();
        addTabelaODSConstruirInfraResiliente();
        addBreak();addBreak(); addBreak();
        addTabelaODSReduzirADesigualdade();
        addBreak();addBreak(); addBreak();
        addTabelaODSCidadesEComunidades();
        addBreak();addBreak(); addBreak();
        addTabelaODSAssegurarPadroes();
        addBreak();addBreak(); addBreak();
        addTabelaODSTomarMedidasUrgentes();
        addBreak();addBreak(); addBreak();
        addTabelaODSConservacaoeUsoSustentavel();
        addBreak();addBreak(); addBreak();
        addTabelaODSProtegerRecuperar();
        addBreak();addBreak(); addBreak();
        addTabelaODSPromoverSociedadesPacificas();
        addBreak();addBreak(); addBreak();
        addTabelaODSParceriasEMeios();


        return sendFile();

    }

    private void addTextoStatusFaseIEGM() {
        String textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        Integer flagStatusIEGM = resultadoIegm2018.getFlagStatusFase();
        if( flagStatusIEGM == 1) {
            textoFlagStatusIEGM = "Índices do exercício em exame em planejamento, " +
                    "dados podem sofrer alterações.";
        } else if(flagStatusIEGM == 2) {
            textoFlagStatusIEGM = "Índices do exercício em exame em verificação/validação da Fiscalização, " +
                    "dados podem sofrer alterações.";
        } else { // flagStatus == 3
            textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        }
        addParagrafo(new TextoFormatado(textoFlagStatusIEGM,
                formatacaoFactory.getItalic(12)));
    }

    private void addTabelaODSAcabarComAPobreza() throws InvalidFormatException, IOException {
        addTituloTabelaODS("01.png", "ODS 1 - Acabar com a pobreza em todas as suas formas, em todos os lugares");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("1.1 - Até 2030, erradicar a pobreza extrema para todas as pessoas em todos" +
                " os lugares, atualmente medida como pessoas vivendo com menos de US$ 1,25 por dia",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("1.2 - Até 2030, reduzir pelo menos à metade a proporção de homens, " +
                "mulheres e crianças, de todas as idades, que vivem na pobreza, em todas as suas dimensões, " +
                "de acordo com as definições nacionais", formatacaoFactory.getJustificado(11)));

        dados.add(new TextoFormatado("1.3 -  Implementar, em nível nacional, medidas e sistemas de proteção " +
                "social adequados, para todos, incluindo pisos, e até 2030 atingir a cobertura substancial dos pobres " +
                "e vulneráveis", formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("1.4 - Até 2030, garantir que todos os homens e mulheres, particularmente os " +
                "pobres e vulneráveis, tenham direitos iguais aos recursos econômicos, bem como o acesso a serviços " +
                "básicos, propriedade e controle sobre a terra e outras formas de propriedade, herança, " +
                "recursos naturais, novas tecnologias apropriadas e serviços financeiros, incluindo microfinanças",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("1.5 - Até 2030, construir a resiliência dos pobres e daqueles em situação " +
                "de vulnerabilidade, e reduzir a exposição e vulnerabilidade destes a eventos extremos relacionados " +
                "com o clima e outros choques e desastres econômicos, sociais e ambientais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("1.a - Garantir uma mobilização significativa de recursos a partir de uma " +
                "variedade de fontes, inclusive por meio do reforço da cooperação para o desenvolvimento, para " +
                "proporcionar meios adequados e previsíveis para que os países em desenvolvimento, em particular " +
                "os países menos desenvolvidos, implementem programas e políticas para acabar com a pobreza em " +
                "todas as suas dimensões",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("1.b - Criar marcos políticos sólidos em níveis nacional, regional e " +
                "internacional, com base em estratégias de desenvolvimento a favor dos pobres e sensíveis a " +
                "gênero, para apoiar investimentos acelerados nas ações de erradicação da pobreza",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSFomeZero() throws InvalidFormatException, IOException {
        addTituloTabelaODS("02.png", "ODS 2 - Fome Zero e Agricultura Sustentável");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("2.1 - Até 2030, acabar com a fome e garantir o acesso de todas as pessoas," +
                " em particular os pobres e pessoas em situações vulneráveis, incluindo crianças, a " +
                "alimentos seguros, nutritivos e suficientes durante todo o ano",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("2.2 - Até 2030, acabar com todas as formas de má-nutrição, incluindo " +
                "atingir, até 2025, as metas acordadas internacionalmente sobre nanismo e caquexia em crianças" +
                " menores de cinco anos de idade, e atender às necessidades nutricionais dos adolescentes, " +
                "mulheres grávidas e lactantes e pessoas idosas", formatacaoFactory.getJustificado(11)));

        dados.add(new TextoFormatado("2.3 - Até 2030, dobrar a produtividade agrícola e a renda dos pequenos " +
                "produtores de alimentos, particularmente das mulheres, povos indígenas, agricultores familiares, " +
                "pastores e pescadores, inclusive por meio de acesso seguro e igual à terra, outros recursos produtivos " +
                "e insumos, conhecimento, serviços financeiros, mercados e oportunidades de agregação de valor e de" +
                " emprego não agrícola", formatacaoFactory.getJustificado(11)));

        dados.add(new TextoFormatado("2.4 - Até 2030, garantir sistemas sustentáveis de produção de alimentos " +
                "e implementar práticas agrícolas resilientes, que aumentem a produtividade e a produção, que" +
                " ajudem a manter os ecossistemas, que fortaleçam a capacidade de adaptação às mudanças climáticas, " +
                "às condições meteorológicas extremas, secas, inundações e outros desastres, e que melhorem " +
                "progressivamente a qualidade da terra e do solo.",
                formatacaoFactory.getJustificado(11)));

        dados.add(new TextoFormatado("2.5 - Até 2020, manter a diversidade genética de sementes, plantas " +
                "cultivadas, animais de criação e domesticados e suas respectivas espécies selvagens, inclusive " +
                "por meio de bancos de sementes e plantas diversificados e bem geridos em nível nacional, " +
                "regional e internacional, e garantir o acesso e a repartição justa e equitativa dos benefícios " +
                "decorrentes da utilização dos recursos genéticos e conhecimentos tradicionais associados, " +
                "como acordado internacionalmente.",
                formatacaoFactory.getJustificado(11)));

        dados.add(new TextoFormatado("2.a - Aumentar o investimento, inclusive via o reforço da cooperação " +
                "internacional, em infraestrutura rural, pesquisa e extensão de serviços agrícolas, desenvolvimento " +
                "de tecnologia, e os bancos de genes de plantas e animais, para aumentar a capacidade de produção " +
                "agrícola nos países em desenvolvimento, em particular nos países menos desenvolvidos.",
                formatacaoFactory.getJustificado(11)));

        dados.add(new TextoFormatado("2.b - Corrigir e prevenir as restrições ao comércio e distorções nos " +
                "mercados agrícolas mundiais, incluindo a eliminação paralela de todas as formas de subsídios à " +
                "exportação e todas as medidas de exportação com efeito equivalente, de acordo com o mandato da " +
                "Rodada de Desenvolvimento de Doha.",
                formatacaoFactory.getJustificado(11)));

        dados.add(new TextoFormatado("2.c - Adotar medidas para garantir o funcionamento adequado dos mercados " +
                "de commodities de alimentos e seus derivados, e facilitar o acesso oportuno à informação de mercado, " +
                "inclusive sobre as reservas de alimentos, a fim de ajudar a limitar a volatilidade extrema dos" +
                " preços dos alimentos.",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSBoaSaude() throws InvalidFormatException, IOException {
        addTituloTabelaODS("03.png", "ODS 3 -   Boa saúde e bem estar: assegurar uma vida saudável e " +
                "promover o bem-estar para todos, em todas as idades");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("3.1 - Até 2030, reduzir a taxa de mortalidade materna global para menos de " +
                "70 mortes por 100.000 nascidos vivos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.2 - Até 2030, acabar com as mortes evitáveis de recém-nascidos e crianças" +
                " menores de 5 anos, com todos os países objetivando reduzir a mortalidade neonatal para pelo menos" +
                " 12 por 1.000 nascidos vivos e a mortalidade de crianças menores de 5 anos para pelo menos 25 " +
                "por 1.000 nascidos vivos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.3 - Até 2030, acabar com as epidemias de AIDS, tuberculose, malária e " +
                "doenças tropicais negligenciadas, e combater a hepatite, doenças transmitidas pela água, e outras " +
                "doenças transmissíveis",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.4 - Até 2030, reduzir em um terço a mortalidade prematura por doenças " +
                "não transmissíveis via prevenção e tratamento, e promover a saúde mental e o bem-estar",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.5 - Reforçar a prevenção e o tratamento do abuso de substâncias, " +
                "incluindo o abuso de drogas entorpecentes e uso nocivo do álcool",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.6 - Até 2020, reduzir pela metade as mortes e os ferimentos globais " +
                "por acidentes em estradas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.7 - Até 2030, assegurar o acesso universal aos serviços de saúde sexual" +
                " e reprodutiva, incluindo o planejamento familiar, informação e educação, bem como a " +
                "integração da saúde reprodutiva em estratégias e programas nacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.8 - Atingir a cobertura universal de saúde, incluindo a proteção do " +
                "risco financeiro, o acesso a serviços de saúde essenciais de qualidade e o acesso a medicamentos " +
                "e vacinas essenciais seguros, eficazes, de qualidade e a preços acessíveis para todos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.9 - Até 2030, reduzir substancialmente o número de mortes e doenças por " +
                "produtos químicos perigosos, contaminação e poluição do ar e água do solo",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.a - Fortalecer a implementação da Convenção-Quadro para o Controle do " +
                "Tabaco em todos os países, conforme apropriado",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.b - Apoiar a pesquisa e o desenvolvimento de vacinas e medicamentos para " +
                "as doenças transmissíveis e não transmissíveis, que afetam principalmente os países em desenvolvimento," +
                " proporcionar o acesso a medicamentos e vacinas essenciais a preços acessíveis, de acordo com a " +
                "Declaração de Doha, que afirma o direito dos países em desenvolvimento de utilizarem plenamente as " +
                "disposições do acordo TRIPS sobre flexibilidades para proteger a saúde pública e, em particular," +
                " proporcionar o acesso a medicamentos para todos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.c - Aumentar substancialmente o financiamento da saúde e o recrutamento, " +
                "desenvolvimento e formação, e retenção do pessoal de saúde nos países em desenvolvimento, " +
                "especialmente nos países menos desenvolvidos e nos pequenos Estados insulares em desenvolvimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("3.d - Reforçar a capacidade de todos os países, particularmente os " +
                "países em desenvolvimento, para o alerta precoce, redução de riscos e gerenciamento de riscos " +
                "nacionais e globais de saúde",
                formatacaoFactory.getJustificado(11)));
        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSAssegurarAEducacao() throws InvalidFormatException, IOException {
        addTituloTabelaODS("04.png", "ODS 4 - Assegurar a educação inclusiva e equitativa e de qualidade, " +
                "e promover oportunidades de aprendizagem ao longo da vida para todas e todos");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("4.1 - Até 2030, garantir que todas as meninas e meninos completem o ensino" +
                " primário e secundário gratuito, equitativo e de qualidade, que conduza a resultados de aprendizagem " +
                "relevantes e eficazes",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.2 - Até 2030, garantir que todos as meninas e meninos tenham acesso a " +
                "um desenvolvimento de qualidade na primeira infância, cuidados e educação pré-escolar, de modo que" +
                " eles estejam prontos para o ensino primário",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.3 Até 2030, assegurar a igualdade de acesso para todos os homens e " +
                "mulheres à educação técnica, profissional e superior de qualidade, a preços acessíveis, incluindo universidade",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.4 Até 2030, aumentar substancialmente o número de jovens e adultos " +
                "que tenham habilidades relevantes, inclusive competências técnicas e profissionais, para emprego, " +
                "trabalho decente e empreendedorismo",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.5 Até 2030, eliminar as disparidades de gênero na educação e garantir " +
                "a igualdade de acesso a todos os níveis de educação e formação profissional para os mais vulneráveis," +
                " incluindo as pessoas com deficiência, povos indígenas e as crianças em situação de vulnerabilidade",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.6 - Até 2030, garantir que todos os jovens e uma substancial proporção " +
                "dos adultos, homens e mulheres estejam alfabetizados e tenham adquirido o conhecimento básico de matemática",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.7 - Até 2030, garantir que todos os alunos adquiram conhecimentos e " +
                "habilidades necessárias para promover o desenvolvimento sustentável, inclusive, entre outros, por " +
                "meio da educação para o desenvolvimento sustentável e estilos de vida sustentáveis, direitos " +
                "humanos, igualdade de gênero, promoção de uma cultura de paz e não violência, cidadania global e" +
                " valorização da diversidade cultural e da contribuição da cultura para o desenvolvimento sustentável",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.a - Construir e melhorar instalações físicas para educação, apropriadas" +
                " para crianças e sensíveis às deficiências e ao gênero, e que proporcionem ambientes de aprendizagem" +
                " seguros e não violentos, inclusivos e eficazes para todos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.b - Até 2020, substancialmente ampliar globalmente o número de bolsas " +
                "de estudo para os países em desenvolvimento, em particular os países menos desenvolvidos, pequenos" +
                " Estados insulares em desenvolvimento e os países africanos, para o ensino superior, incluindo" +
                " programas de formação profissional, de tecnologia da informação e da comunicação, técnicos, de " +
                "engenharia e programas científicos em países desenvolvidos e outros países em desenvolvimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("4.c - Até 2030, substancialmente aumentar o contingente de professores " +
                "qualificados, inclusive por meio da cooperação internacional para a formação de professores, nos " +
                "países em desenvolvimento, especialmente os países menos desenvolvidos e pequenos Estados insulares" +
                " em desenvolvimento",
                formatacaoFactory.getJustificado(11)));


        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSAlcancarAIgualdadeDeGenero() throws InvalidFormatException, IOException {
        addTituloTabelaODS("05.png", "ODS 5 - Alcançar a igualdade de gênero e empoderar todas as mulheres" +
                " e meninas");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("5.1 - Acabar com todas as formas de discriminação contra todas as mulheres e meninas em toda partes",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.2 - Eliminar todas as formas de violência contra todas as mulheres e " +
                "meninas nas esferas públicas e privadas, incluindo o tráfico e exploração sexual e de outros tipos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.3 - Eliminar todas as práticas nocivas, como os casamentos prematuros, forçados " +
                "e de crianças e mutilações genitais femininas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.4 - Reconhecer e valorizar o trabalho de assistência e doméstico não " +
                "remunerado, por meio da disponibilização de serviços públicos, infraestrutura e políticas de proteção " +
                "social, bem como a promoção da responsabilidade compartilhada dentro do lar e da família, conforme " +
                "os contextos nacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.5 - Garantir a participação plena e efetiva das mulheres e a igualdade de " +
                "oportunidades para a liderança em todos os níveis de tomada de decisão na vida política, econômica e pública",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.6 - Assegurar o acesso universal à saúde sexual e reprodutiva e os " +
                "direitos reprodutivos, como acordado em conformidade com o Programa de Ação da Conferência Internacional " +
                "sobre População e Desenvolvimento e com a Plataforma de Ação de Pequim e os documentos resultantes " +
                "de suas conferências de revisão",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.a - Realizar reformas para dar às mulheres direitos iguais aos recursos " +
                "econômicos, bem como o acesso a propriedade e controle sobre a terra e outras formas de propriedade, " +
                "serviços financeiros, herança e os recursos naturais, de acordo com as leis nacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.b - Aumentar o uso de tecnologias de base, em particular as tecnologias " +
                "de informação e comunicação, para promover o empoderamento das mulheres",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("5.c - Adotar e fortalecer políticas sólidas e legislação aplicável para " +
                "a promoção da igualdade de gênero e o empoderamento de todas as mulheres e meninas em todos os níveis",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSGarantirDisponibilidade() throws InvalidFormatException, IOException {
        addTituloTabelaODS("06.png", "ODS 6 - Garantir disponibilidade e manejo sustentável da água" +
                " e saneamento para todos");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("6.1 - Até 2030, alcançar o acesso universal e equitativo a água potável " +
                "e segura para todos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("6.2 - Até 2030, alcançar o acesso a saneamento e higiene adequados e " +
                "equitativos para todos, e acabar com a defecação a céu aberto, com especial atenção para as " +
                "necessidades das mulheres e meninas e daqueles em situação de vulnerabilidade",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("6.3 - Até 2030, melhorar a qualidade da água, reduzindo a poluição, " +
                "eliminando despejo e minimizando a liberação de produtos químicos e materiais perigosos, " +
                "reduzindo à metade a proporção de águas residuais não tratadas e aumentando substancialmente a " +
                "reciclagem e reutilização segura globalmente",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("6.4 - Até 2030, aumentar substancialmente a eficiência do uso da água " +
                "em todos os setores e assegurar retiradas sustentáveis e o abastecimento de água doce para enfrentar " +
                "a escassez de água, e reduzir substancialmente o número de pessoas que sofrem com a escassez de água",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("6.5 - Até 2030, implementar a gestão integrada dos recursos hídricos em " +
                "todos os níveis, inclusive via cooperação transfronteiriça, conforme apropriado",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("6.6 - Até 2020, proteger e restaurar ecossistemas relacionados " +
                "com a água, incluindo montanhas, florestas, zonas úmidas, rios, aquíferos e lagos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("6.a - Até 2030, ampliar a cooperação internacional e o apoio à capacitação" +
                " para os países em desenvolvimento em atividades e programas relacionados à água e saneamento, " +
                "incluindo a coleta de água, a dessalinização, a eficiência no uso da água, o tratamento de efluentes, " +
                "a reciclagem e as tecnologias de reuso",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("6.b - Apoiar e fortalecer a participação das comunidades locais, para " +
                "melhorar a gestão da água e do saneamento",
                formatacaoFactory.getJustificado(11)));


        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSGarantirAcessoAEnergia() throws InvalidFormatException, IOException {
        addTituloTabelaODS("07.png", "ODS 7 - Garantir acesso à energia barata, confiável, sustentável e " +
                "renovável para todos");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("7.1 - Até 2030, assegurar o acesso universal, confiável, moderno e a preços" +
                " acessíveis a serviços de energia",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("7.2 - Até 2030, aumentar substancialmente a participação de energias " +
                "renováveis na matriz energética global",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("7.3 - Até 2030, dobrar a taxa global de melhoria da eficiência energética",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("7.a - Até 2030, reforçar a cooperação internacional para facilitar o " +
                "acesso a pesquisa e tecnologias de energia limpa, incluindo energias renováveis, eficiência energética" +
                " e tecnologias de combustíveis fósseis avançadas e mais limpas, e promover o investimento em " +
                "infraestrutura de energia e em tecnologias de energia limpa",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("7.b - Até 2030, expandir a infraestrutura e modernizar a tecnologia para o" +
                " fornecimento de serviços de energia modernos e sustentáveis para todos nos países em desenvolvimento, " +
                "particularmente nos países menos desenvolvidos, nos pequenos Estados insulares em desenvolvimento e" +
                " nos países em desenvolvimento sem litoral, de acordo com seus respectivos programas de apoio",
                formatacaoFactory.getJustificado(11)));


        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSPromoverOCrescimentoEconomico() throws InvalidFormatException, IOException {
        addTituloTabelaODS("08.png", "ODS 8 - Promover o crescimento econômico sustentado, " +
                "inclusivo e sustentável, emprego pleno e produtivo, e trabalho decente para todos");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("8.1 - Sustentar o crescimento econômico per capita de acordo com as " +
                "circunstâncias nacionais e, em particular, um crescimento anual de pelo menos 7% do produto interno " +
                "bruto [PIB] nos países menos desenvolvidos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.2 - Atingir níveis mais elevados de produtividade das economias por meio " +
                "da diversificação, modernização tecnológica e inovação, inclusive por meio de um foco em setores " +
                "de alto valor agregado e dos setores intensivos em mão de obra",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.3 - Promover políticas orientadas para o desenvolvimento que apoiem as " +
                "atividades produtivas, geração de emprego decente, empreendedorismo, criatividade e inovação, e " +
                "incentivar a formalização e o crescimento das micro, pequenas e médias empresas, inclusive por " +
                "meio do acesso a serviços financeiros",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.4 - Melhorar progressivamente, até 2030, a eficiência dos recursos " +
                "globais no consumo e na produção, e empenhar-se para dissociar o crescimento econômico da degradação " +
                "ambiental, de acordo com o Plano Decenal de Programas sobre Produção e Consumo Sustentáveis, com os" +
                " países desenvolvidos assumindo a liderança",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.5 - Até 2030, alcançar o emprego pleno e produtivo e trabalho decente " +
                "todas as mulheres e homens, inclusive para os jovens e as pessoas com deficiência, e remuneração " +
                "igual para trabalho de igual valor",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.6 - Até 2020, reduzir substancialmente a proporção de jovens sem " +
                "emprego, educação ou formação",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.7 - Tomar medidas imediatas e eficazes para erradicar o trabalho " +
                "forçado, acabar com a escravidão moderna e o tráfico de pessoas, e assegurar a proibição e " +
                "eliminação das piores formas de trabalho infantil, incluindo recrutamento e utilização de " +
                "crianças-soldado, e até 2025 acabar com o trabalho infantil em todas as suas formas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.8 - Proteger os direitos trabalhistas e promover ambientes de trabalho " +
                "seguros e protegidos para todos os trabalhadores, incluindo os trabalhadores migrantes, em particular" +
                " as mulheres migrantes, e pessoas em empregos precários",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.9 - Até 2030, elaborar e implementar políticas para promover o turismo " +
                "sustentável, que gera empregos e promove a cultura e os produtos locais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.10 - Fortalecer a capacidade das instituições financeiras nacionais para " +
                "incentivar a expansão do acesso aos serviços bancários, de seguros e financeiros para todos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.a - Aumentar o apoio da Iniciativa de Ajuda para o Comércio [Aid for Trade] " +
                "para os países em desenvolvimento, particularmente os países menos desenvolvidos, inclusive por meio" +
                " do Quadro Integrado Reforçado para a Assistência Técnica Relacionada com o Comércio para os países " +
                "menos desenvolvidos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("8.b - Até 2020, desenvolver e operacionalizar uma estratégia global para o " +
                "emprego dos jovens e implementar o Pacto Mundial para o Emprego da Organização Internacional do Trabalho [OIT]",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSConstruirInfraResiliente() throws InvalidFormatException, IOException {
        addTituloTabelaODS("09.png", "ODS 9 - Construir infraestrutura resiliente, promover a " +
                "industrialização inclusiva e sustentável, e fomentar a inovação");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("9.1 - Desenvolver infraestrutura de qualidade, confiável, sustentável e " +
                "resiliente, incluindo infraestrutura regional e transfronteiriça, para apoiar o desenvolvimento " +
                "econômico e o bem-estar humano, com foco no acesso equitativo e a preços acessíveis para todos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("9.2 - Promover a industrialização inclusiva e sustentável e, até 2030, aumentar" +
                " significativamente a participação da indústria no emprego e no produto interno bruto, de acordo com" +
                " as circunstâncias nacionais, e dobrar sua participação nos países de menor desenvolvimento relativo",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("9.3 - Aumentar o acesso das pequenas indústrias e outras empresas, " +
                "particularmente em países em desenvolvimento, aos serviços financeiros, incluindo crédito acessível " +
                "e propiciar sua integração em cadeias de valor e mercados",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("9.4 - Até 2030, modernizar a infraestrutura e reabilitar as indústrias para" +
                " torná-las sustentáveis, com eficiência aumentada no uso de recursos e maior adoção de tecnologias e" +
                " processos industriais limpos e ambientalmente adequados; com todos os países atuando de acordo com " +
                "suas respectivas capacidades",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("9.5 - Fortalecer a pesquisa científica, melhorar as capacidades tecnológicas" +
                " de setores industriais em todos os países, particularmente nos países em desenvolvimento, inclusive, " +
                "até 2030, incentivando a inovação e aumentando substancialmente o número de trabalhadores de pesquisa" +
                " e desenvolvimento por milhão de pessoas e os gastos público e privado em pesquisa e desenvolvimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("9.a - Facilitar o desenvolvimento de infraestrutura sustentável e " +
                "resiliente em países em desenvolvimento, por meio de maior apoio financeiro, tecnológico e técnico " +
                "aos países africanos, aos países de menor desenvolvimento relativo, aos países em desenvolvimento" +
                " sem litoral e aos pequenos Estados insulares em desenvolvimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("9.b - Apoiar o desenvolvimento tecnológico, a pesquisa e a inovação " +
                "nacionais nos países em desenvolvimento, inclusive garantindo um ambiente político propício para, " +
                "entre outras coisas, diversificação industrial e agregação de valor às commodities",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("9.c - Aumentar significativamente o acesso às tecnologias de informação e " +
                "comunicação e empenhar-se para procurar ao máximo oferecer acesso universal e a preços acessíveis à " +
                "internet nos países menos desenvolvidos, até 2020",
                formatacaoFactory.getJustificado(11)));


        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSReduzirADesigualdade() throws InvalidFormatException, IOException {
        addTituloTabelaODS("10.png", "ODS 10 - Reduzir a desigualdade dentro dos países e entre eles");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("10.1 - Até 2030, progressivamente alcançar e sustentar o crescimento da " +
                "renda dos 40% da população mais pobre a uma taxa maior que a média nacional",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.2 - Até 2030, empoderar e promover a inclusão social, econômica e " +
                "política de todos, independentemente da idade, gênero, deficiência, raça, etnia, origem, " +
                "religião, condição econômica ou outra",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.3 - Garantir a igualdade de oportunidades e reduzir as desigualdades de" +
                " resultados, inclusive por meio da eliminação de leis, políticas e práticas discriminatórias e da " +
                "promoção de legislação, políticas e ações adequadas a este respeito",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.4 - Adotar políticas, especialmente fiscal, salarial e de proteção " +
                "social, e alcançar progressivamente uma maior igualdade",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.5 - Melhorar a regulamentação e monitoramento dos mercados e instituições" +
                " financeiras globais e fortalecer a implementação de tais regulamentações",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.6 - Assegurar uma representação e voz mais forte dos países em " +
                "desenvolvimento em tomadas de decisão nas instituições econômicas e financeiras internacionais " +
                "globais, a fim de produzir instituições mais eficazes, críveis, responsáveis e legítimas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.7.1 - Custo de recrutamento suportado pelo empregado em proporção do " +
                "rendimento anual auferido no país de destino",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.a - Implementar o princípio do tratamento especial e diferenciado para " +
                "países em desenvolvimento, em particular os países menos desenvolvidos, em conformidade com os " +
                "acordos da OMC",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.b - Incentivar a assistência oficial ao desenvolvimento e fluxos " +
                "financeiros, incluindo o investimento externo direto, para os Estados onde a necessidade é maior, em " +
                "particular os países menos desenvolvidos, os países africanos, os pequenos Estados insulares em " +
                "desenvolvimento e os países em desenvolvimento sem litoral, de acordo com seus planos e programas nacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("10.c - Até 2030, reduzir para menos de 3% os custos de transação de remessas" +
                " dos migrantes e eliminar os corredores de remessas com custos superiores a 5%",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSCidadesEComunidades() throws InvalidFormatException, IOException {
        addTituloTabelaODS("11.png", "ODS 11 - Cidades e Comunidades Sustentáveis : Tornar as cidades e os " +
                "assentamentos humanos inclusivos, seguros, resilientes e sustentáveis");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("11.1 - Até 2030, garantir o acesso de todos à habitação segura, adequada e " +
                "a preço acessível, e aos serviços básicos e urbanizar as favelas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.2 - Até 2030, proporcionar o acesso a sistemas de transporte seguros, " +
                "acessíveis, sustentáveis e a preço acessível para todos, melhorando a segurança rodoviária por meio " +
                "da expansão dos transportes públicos, com especial atenção para as necessidades das pessoas em " +
                "situação de vulnerabilidade, mulheres, crianças, pessoas com deficiência e idosos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.3 - Até 2030, aumentar a urbanização inclusiva e sustentável, e as" +
                " capacidades para o planejamento e gestão de assentamentos humanos participativos, integrados e " +
                "sustentáveis, em todos os países",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.4 - Fortalecer esforços para proteger e salvaguardar o patrimônio " +
                "cultural e natural do mundo",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.5 - Até 2030, reduzir significativamente o número de mortes e o " +
                "número de pessoas afetadas por catástrofes e substancialmente diminuir as perdas econômicas diretas " +
                "causadas por elas em relação ao produto interno bruto global, incluindo os desastres relacionados à" +
                " água, com o foco em proteger os pobres e as pessoas em situação de vulnerabilidade",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.6 - Até 2030, reduzir o impacto ambiental negativo per capita das " +
                "cidades, inclusive prestando especial atenção à qualidade do ar, gestão de resíduos municipais e outros",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.7 - Até 2030, proporcionar o acesso universal a espaços públicos " +
                "seguros, inclusivos, acessíveis e verdes, particularmente para as mulheres e crianças, pessoas " +
                "idosas e pessoas com deficiência",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.a - Apoiar relações econômicas, sociais e ambientais positivas entre " +
                "áreas urbanas, peri-urbanas e rurais, reforçando o planejamento nacional e regional de desenvolvimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.b - Até 2020, aumentar substancialmente o número de cidades e " +
                "assentamentos humanos adotando e implementando políticas e planos integrados para a inclusão, a" +
                " eficiência dos recursos, mitigação e adaptação às mudanças climáticas, a resiliência a desastres; e " +
                "desenvolver e implementar, de acordo com o Marco de Sendai para a Redução do Risco de Desastres " +
                "2015-2030, o gerenciamento holístico do risco de desastres em todos os níveis",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("11.c - Apoiar os países menos desenvolvidos, inclusive por meio de " +
                "assistência técnica e financeira, para construções sustentáveis e resilientes, utilizando materiais locais",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSAssegurarPadroes() throws InvalidFormatException, IOException {
        addTituloTabelaODS("12.png", "ODS 12 - Assegurar padrões de produção e de consumo sustentáveis");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("12.1 - Implementar o Plano Decenal de Programas sobre Produção e Consumo " +
                "Sustentáveis, com todos os países tomando medidas, e os países desenvolvidos assumindo a liderança, " +
                "tendo em conta o desenvolvimento e as capacidades dos países em desenvolvimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.2 - Até 2030, alcançar a gestão sustentável e o uso eficiente dos " +
                "recursos naturais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.3 - Até 2030, reduzir pela metade o desperdício de alimentos per capita " +
                "mundial, nos níveis de varejo e do consumidor, e reduzir as perdas de alimentos ao longo das cadeias " +
                "de produção e abastecimento, incluindo as perdas pós-colheita",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.4 - Até 2020, alcançar o manejo ambientalmente saudável dos produtos " +
                "químicos e todos os resíduos, ao longo de todo o ciclo de vida destes, de acordo com os marcos " +
                "internacionais acordados, e reduzir significativamente a liberação destes para o ar, água e solo, " +
                "para minimizar seus impactos negativos sobre a saúde humana e o meio ambiente",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.5 - Até 2030, reduzir substancialmente a geração de resíduos por meio " +
                "da prevenção, redução, reciclagem e reuso",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.6 - Incentivar as empresas, especialmente as empresas grandes e " +
                "transnacionais, a adotar práticas sustentáveis e a integrar informações de sustentabilidade em seu " +
                "ciclo de relatórios",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.7 - Promover práticas de compras públicas sustentáveis, de acordo com " +
                "as políticas e prioridades nacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.8 - Até 2030, garantir que as pessoas, em todos os lugares, tenham " +
                "informação relevante e conscientização para o desenvolvimento sustentável e estilos de vida em harmonia" +
                " com a natureza",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.a - Apoiar países em desenvolvimento a fortalecer suas capacidades " +
                "científicas e tecnológicas para mudar para padrões mais sustentáveis de produção e consumo",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.b - Desenvolver e implementar ferramentas para monitorar os impactos " +
                "do desenvolvimento sustentável para o turismo sustentável, que gera empregos, promove a cultura e " +
                "os produtos locais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("12.c - Racionalizar subsídios ineficientes aos combustíveis fósseis, " +
                "que encorajam o consumo exagerado, eliminando as distorções de mercado, de acordo com as circunstâncias" +
                " nacionais, inclusive por meio da reestruturação fiscal e a eliminação gradual desses subsídios" +
                " prejudiciais, caso existam, para refletir os seus impactos ambientais, tendo plenamente em conta " +
                "as necessidades específicas e condições dos países em desenvolvimento e minimizando os possíveis " +
                "impactos adversos sobre o seu desenvolvimento de uma forma que proteja os pobres e as comunidades " +
                "afetadas",
                formatacaoFactory.getJustificado(11)));


        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSTomarMedidasUrgentes() throws InvalidFormatException, IOException {
        addTituloTabelaODS("13.png", "ODS 13 - Tomar medidas urgentes para combater a mudança do clima e " +
                "seus impactos");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("13.1 - Reforçar a resiliência e a capacidade de adaptação a riscos " +
                "relacionados ao clima e às catástrofes naturais em todos os países",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("13.2 - Integrar medidas da mudança do clima nas políticas, estratégias e " +
                "planejamentos nacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("13.3 - Melhorar a educação, aumentar a conscientização e a capacidade " +
                "humana e institucional sobre mitigação, adaptação, redução de impacto e alerta precoce da mudança do clima",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("13.a - Implementar o compromisso assumido pelos países desenvolvidos " +
                "partes da Convenção Quadro das Nações Unidas sobre Mudança do Clima [UNFCCC] para a meta de mobilizar " +
                "conjuntamente US$ 100 bilhões por ano a partir de 2020, de todas as fontes, para atender às " +
                "necessidades dos países em desenvolvimento, no contexto das ações de mitigação significativas e " +
                "transparência na implementação; e operacionalizar plenamente o Fundo Verde para o Clima por meio " +
                "de sua capitalização o mais cedo possível\n",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("13.b - Promover mecanismos para a criação de capacidades para o " +
                "planejamento relacionado à mudança do clima e à gestão eficaz, nos países menos desenvolvidos, " +
                "inclusive com foco em mulheres, jovens, comunidades locais e marginalizadas",
                formatacaoFactory.getJustificado(11)));


        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSConservacaoeUsoSustentavel() throws InvalidFormatException, IOException {
        addTituloTabelaODS("14.png", "ODS 14 - Conservação e uso sustentável dos oceanos, dos mares e " +
                "dos recursos marinhos para o desenvolvimento sustentável");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("14.1 - Conservação e uso sustentável dos oceanos, dos mares e dos " +
                "recursos marinhos para o desenvolvimento sustentável",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.2 - Até 2020, gerir de forma sustentável e proteger os ecossistemas" +
                " marinhos e costeiros para evitar impactos adversos significativos, inclusive por meio do reforço " +
                "da sua capacidade de resiliência, e tomar medidas para a sua restauração, a fim de assegurar oceanos " +
                "saudáveis e produtivos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.3 - Minimizar e enfrentar os impactos da acidificação dos oceanos, " +
                "inclusive por meio do reforço da cooperação científica em todos os níveis",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.4 - Até 2020, efetivamente regular a coleta, e acabar com a sobrepesca," +
                " ilegal, não reportada e não regulamentada e as práticas de pesca destrutivas, e implementar " +
                "planos de gestão com base científica, para restaurar populações de peixes no menor tempo possível, " +
                "pelo menos a níveis que possam produzir rendimento máximo sustentável, como determinado por suas" +
                " características biológicas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.5 - Até 2020, conservar pelo menos 10% das zonas costeiras e marinhas, " +
                "de acordo com a legislação nacional e internacional, e com base na melhor informação científica disponível",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.6 - Até 2020, proibir certas formas de subsídios à pesca, que " +
                "contribuem para a sobrecapacidade e a sobrepesca, e eliminar os subsídios que contribuam para a" +
                " pesca ilegal, não reportada e não regulamentada, e abster-se de introduzir novos subsídios como " +
                "estes, reconhecendo que o tratamento especial e diferenciado adequado e eficaz para os países em " +
                "desenvolvimento e os países menos desenvolvidos deve ser parte integrante da negociação sobre " +
                "subsídios à pesca da Organização Mundial do Comércio",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.7 - Até 2030, aumentar os benefícios econômicos para os pequenos " +
                "Estados insulares em desenvolvimento e os países menos desenvolvidos, a partir do uso sustentável" +
                " dos recursos marinhos, inclusive por meio de uma gestão sustentável da pesca, aquicultura e turismo",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.a - Aumentar o conhecimento científico, desenvolver capacidades de " +
                "pesquisa e transferir tecnologia marinha, tendo em conta os critérios e orientações sobre a " +
                "Transferência de Tecnologia Marinha da Comissão Oceanográfica Intergovernamental, a fim de melhorar " +
                "a saúde dos oceanos e aumentar a contribuição da biodiversidade marinha para o desenvolvimento dos" +
                " países em desenvolvimento, em particular os pequenos Estados insulares em desenvolvimento e os" +
                " países menos desenvolvidos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.b - Proporcionar o acesso dos pescadores artesanais de pequena escala " +
                "aos recursos marinhos e mercados",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("14.c - Assegurar a conservação e o uso sustentável dos oceanos e seus" +
                " recursos pela implementação do direito internacional, como refletido na UNCLOS [Convenção das Nações " +
                "Unidas sobre o Direito do Mar], que provê o arcabouço legal para a conservação e utilização sustentável" +
                " dos oceanos e dos seus recursos, conforme registrado no parágrafo 158 do “Futuro Que Queremos”",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSProtegerRecuperar() throws InvalidFormatException, IOException {
        addTituloTabelaODS("15.png", "ODS 15 - Proteger, recuperar e promover o uso sustentável dos " +
                "ecossistemas terrestres, gerir de forma sustentável as florestas, combater a desertificação, deter" +
                " e reverter a degradação da terra e deter a perda de biodiversidade");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("15.1 - Até 2020, assegurar a conservação, recuperação e uso sustentável " +
                "de ecossistemas terrestres e de água doce interiores e seus serviços, em especial florestas, zonas " +
                "úmidas, montanhas e terras áridas, em conformidade com as obrigações decorrentes dos acordos internacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.2 - Até 2020, promover a implementação da gestão sustentável de todos " +
                "os tipos de florestas, deter o desmatamento, restaurar florestas degradadas e aumentar substancialmente " +
                "o florestamento e o reflorestamento globalmente",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.3 - Até 2030, combater a desertificação, restaurar a terra e o solo " +
                "degradado, incluindo terrenos afetados pela desertificação, secas e inundações, e lutar para alcançar " +
                "um mundo neutro em termos de degradação do solo",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.4 - Até 2030, assegurar a conservação dos ecossistemas de montanha, " +
                "incluindo a sua biodiversidade, para melhorar a sua capacidade de proporcionar benefícios que são " +
                "essenciais para o desenvolvimento sustentável",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.5 - Tomar medidas urgentes e significativas para reduzir a degradação " +
                "de habitat naturais, deter a perda de biodiversidade e, até 2020, proteger e evitar a extinção de " +
                "espécies ameaçadas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.6 - Garantir uma repartição justa e equitativa dos benefícios derivados " +
                "da utilização dos recursos genéticos e promover o acesso adequado aos recursos genéticos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.7 - Tomar medidas urgentes para acabar com a caça ilegal e o tráfico de " +
                "espécies da flora e fauna protegidas e abordar tanto a demanda quanto a oferta de produtos ilegais da " +
                "vida selvagem",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.8 - Até 2020, implementar medidas para evitar a introdução e reduzir " +
                "significativamente o impacto de espécies exóticas invasoras em ecossistemas terrestres e aquáticos, e " +
                "controlar ou erradicar as espécies prioritárias",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.9 - Até 2020, integrar os valores dos ecossistemas e da biodiversidade " +
                "ao planejamento nacional e local, nos processos de desenvolvimento, nas estratégias de redução da pobreza " +
                "e nos sistemas de contas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.a - Mobilizar e aumentar significativamente, a partir de todas as " +
                "fontes, os recursos financeiros para a conservação e o uso sustentável da biodiversidade e dos " +
                "ecossistemas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.b - Mobilizar recursos significativos de todas as fontes e em todos os " +
                "níveis para financiar o manejo florestal sustentável e proporcionar incentivos adequados aos países " +
                "em desenvolvimento para promover o manejo florestal sustentável, inclusive para a conservação e" +
                " o reflorestamento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("15.c - Reforçar o apoio global para os esforços de combate à caça ilegal " +
                "e ao tráfico de espécies protegidas, inclusive por meio do aumento da capacidade das comunidades " +
                "locais para buscar oportunidades de subsistência sustentável",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSPromoverSociedadesPacificas() throws InvalidFormatException, IOException {
        addTituloTabelaODS("16.png", "ODS 16 - Promover sociedades pacíficas e inclusivas para o " +
                "desenvolvimento sustentável, proporcionar o acesso à justiça para todos e construir" +
                " instituições eficazes, responsáveis e inclusivas em todos os níveis");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("16.1 - Reduzir significativamente todas as formas de violência e as taxas " +
                "de mortalidade relacionada em todos os lugares",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.2 - Acabar com abuso, exploração, tráfico e todas as formas de violência" +
                " e tortura contra crianças",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.3 - Promover o Estado de Direito, em nível nacional e internacional, " +
                "e garantir a igualdade de acesso à justiça para todos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.4 - Até 2030, reduzir significativamente os fluxos financeiros e de " +
                "armas ilegais, reforçar a recuperação e devolução de recursos roubados e combater todas as formas de" +
                " crime organizado",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.5 - Reduzir substancialmente a corrupção e o suborno em todas as suas " +
                "formas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.6 - Desenvolver instituições eficazes, responsáveis e transparentes em" +
                " todos os níveis",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.7 - Garantir a tomada de decisão responsiva, inclusiva, participativa" +
                " e representativa em todos os níveis",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.8 - Ampliar e fortalecer a participação dos países em desenvolvimento" +
                " nas instituições de governança global",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.9 - Até 2030, fornecer identidade legal para todos, incluindo o" +
                " registro de nascimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.10 - Assegurar o acesso público à informação e proteger as liberdades" +
                " fundamentais, em conformidade com a legislação nacional e os acordos internacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.a - Fortalecer as instituições nacionais relevantes, inclusive por " +
                "meio da cooperação internacional, para a construção de capacidades em todos os níveis, em particular" +
                " nos países em desenvolvimento, para a prevenção da violência e o combate ao terrorismo e ao crime",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("16.b - Promover e fazer cumprir leis e políticas não discriminatórias" +
                " para o desenvolvimento sustentável",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }

    private void addTabelaODSParceriasEMeios() throws InvalidFormatException, IOException {
        addTituloTabelaODS("17.png", "ODS 17 - Parcerias e Meios de Implementação: fortalecer os meios de " +
                "implementação e revitalizar a parceria global para o desenvolvimento sustentável");

        addBreak();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"100%"}, false);
        List<TextoFormatado> dados = new ArrayList<>();
        dados.add(new TextoFormatado("17.1 - Fortalecer a mobilização de recursos internos, inclusive por meio" +
                " do apoio internacional aos países em desenvolvimento, para melhorar a capacidade nacional para " +
                "arrecadação de impostos e outras receitas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.2 - Países desenvolvidos implementarem plenamente os seus compromissos" +
                " em matéria de assistência oficial ao desenvolvimento [AOD], inclusive fornecer 0,7% da renda nacional" +
                " bruta [RNB] em AOD aos países em desenvolvimento, dos quais 0,15% a 0,20% para os países menos " +
                "desenvolvidos; provedores de AOD são encorajados a considerar a definir uma meta para fornecer pelo" +
                " menos 0,20% da renda nacional bruta em AOD para os países menos desenvolvidos (NÃO SE APLICA AO BRASIL)",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.3 - Mobilizar recursos financeiros adicionais para os países em" +
                " desenvolvimento a partir de múltiplas fontes",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.4 - Ajudar os países em desenvolvimento a alcançar a sustentabilidade " +
                "da dívida de longo prazo por meio de políticas coordenadas destinadas a promover o financiamento, " +
                "a redução e a reestruturação da dívida, conforme apropriado, e tratar da dívida externa dos países " +
                "pobres altamente endividados para reduzir o superendividamento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.5 - Adotar e implementar regimes de promoção de investimentos para os" +
                " países menos desenvolvidos",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.6 - Melhorar a cooperação Norte-Sul, Sul-Sul e triangular regional e " +
                "internacional e o acesso à ciência, tecnologia e inovação, e aumentar o compartilhamento de " +
                "conhecimentos em termos mutuamente acordados, inclusive por meio de uma melhor coordenação entre os " +
                "mecanismos existentes, particularmente no nível das Nações Unidas, e por meio de um mecanismo de " +
                "facilitação de tecnologia global",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.7 - Promover o desenvolvimento, a transferência, a disseminação e a " +
                "difusão de tecnologias ambientalmente corretas para os países em desenvolvimento, em condições " +
                "favoráveis, inclusive em condições concessionais e preferenciais, conforme mutuamente acordado",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.8 - Operacionalizar plenamente o Banco de Tecnologia e o mecanismo " +
                "de capacitação em ciência, tecnologia e inovação para os países menos desenvolvidos até 2017, e" +
                " aumentar o uso de tecnologias de capacitação, em particular das tecnologias de informação e " +
                "comunicação",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.9 - Reforçar o apoio internacional para a implementação eficaz e " +
                "orientada da capacitação em países em desenvolvimento, a fim de apoiar os planos nacionais para " +
                "implementar todos os objetivos de desenvolvimento sustentável, inclusive por meio da cooperação " +
                "Norte-Sul, Sul-Sul e triangular",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.10 - Promover um sistema multilateral de comércio universal, baseado em " +
                "regras, aberto, não discriminatório e equitativo no âmbito da Organização Mundial do Comércio, " +
                "inclusive por meio da conclusão das negociações no âmbito de sua Agenda de Desenvolvimento de Doha",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.11 - Aumentar significativamente as exportações dos países em " +
                "desenvolvimento, em particular com o objetivo de duplicar a participação dos países menos " +
                "desenvolvidos nas exportações globais até 2020",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.12 - Concretizar a implementação oportuna de acesso a mercados" +
                " livres de cotas e taxas, de forma duradoura, para todos os países menos desenvolvidos, de acordo " +
                "com as decisões da OMC, inclusive por meio de garantias de que as regras de origem preferenciais" +
                " aplicáveis às importações provenientes de países menos desenvolvidos sejam transparentes e " +
                "simples, e contribuam para facilitar o acesso ao mercado",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.13 - Aumentar a estabilidade macroeconômica global, inclusive por " +
                "meio da coordenação e da coerência de políticas",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.14 - Aumentar a coerência das políticas para o desenvolvimento " +
                "sustentável",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.15 - Respeitar o espaço político e a liderança de cada país para " +
                "estabelecer e implementar políticas para a erradicação da pobreza e o desenvolvimento sustentável",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.16 - Reforçar a parceria global para o desenvolvimento sustentável, " +
                "complementada por parcerias multissetoriais que mobilizem e compartilhem conhecimento, expertise, " +
                "tecnologia e recursos financeiros, para apoiar a realização dos objetivos do desenvolvimento " +
                "sustentável em todos os países, particularmente nos países em desenvolvimento",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.17 - Incentivar e promover parcerias públicas, público-privadas e " +
                "com a sociedade civil eficazes, a partir da experiência das estratégias de mobilização de recursos " +
                "dessas parcerias",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.18 - Até 2020, reforçar o apoio à capacitação para os países em " +
                "desenvolvimento, inclusive para os países menos desenvolvidos e pequenos Estados insulares em " +
                "desenvolvimento, para aumentar significativamente a disponibilidade de dados de alta qualidade, " +
                "atuais e confiáveis, desagregados por renda, gênero, idade, raça, etnia, status migratório, " +
                "deficiência, localização geográfica e outras características relevantes em contextos nacionais",
                formatacaoFactory.getJustificado(11)));
        dados.add(new TextoFormatado("17.19 - Até 2030, valer-se de iniciativas existentes para desenvolver" +
                " medidas do progresso do desenvolvimento sustentável que complementem o produto interno " +
                "bruto [PIB] e apoiem a capacitação estatística nos países em desenvolvimento",
                formatacaoFactory.getJustificado(11)));

        XWPFTable tabela = addTabelaColunaUnica(dados, formatacaoTabela);

    }


    private void addApontamentosODS(String indice) {
        List<ApontamentoODS> apontamentoODSList = apontamentosODS.get(indice);
        if(apontamentoODSList != null && apontamentoODSList.size() > 0) {
//            XWPFFootnote footnote = document.createFootnote();
//            footnote.createParagraph().createRun().setText("TESTE");
//                    .createParagraph().createRun()
//                    .setText("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice");

            addParagrafo(addTab().concat("De acordo com o apurado na comparação dos índices dos quesitos do IEG-M " +
                            "com os ODS, foram constatadas as seguintes inadequações às metas propostas pela " +
                            "Agenda 2030, que requerem atuação da Administração Municipal:",
            formatacaoFactory.getJustificado(12)));
            addBreak();
            apontamentoODSList.stream().map( (apontamento)-> apontamento.getOdsApontamento() )
                    .distinct()
                    .forEach((apontamento) -> {
                        addParagrafo(addTab().concat(apontamento,
                                formatacaoFactory.getJustificado(12)));
                    });
//            addParagrafo(addTab().concat("Para consulta ao texto integral da(s) referida(s) meta(s), vide Apêndice.",
//                            formatacaoFactory.getJustificado(12)));
        }
    }
}