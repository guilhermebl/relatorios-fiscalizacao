package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import java.util.ArrayList;
import java.util.List;

public class MergePosition {

    private Integer linha;
    private Integer coluna;
    private List<MergePosition> toMergeHorizontal = new ArrayList<>();
    private List<MergePosition> toMergeVertical = new ArrayList<>();


    public MergePosition(Integer linha, Integer coluna) {
        this.linha = linha;
        this.coluna = coluna;
    }

    public Integer getLinha() {
        return linha;
    }

    public void setLinha(Integer linha) {
        this.linha = linha;
    }

    public Integer getColuna() {
        return coluna;
    }

    public void setColuna(Integer coluna) {
        this.coluna = coluna;
    }

    public List<MergePosition> getToMergeHorizontal() {
        return toMergeHorizontal;
    }

    public void setToMergeHorizontal(List<MergePosition> toMergeHorizontal) {
        this.toMergeHorizontal = toMergeHorizontal;
    }

    public List<MergePosition> getToMergeVertical() {
        return toMergeVertical;
    }

    public void setToMergeVertical(List<MergePosition> toMergeVertical) {
        this.toMergeVertical = toMergeVertical;
    }

    public void addToMergeHorizontal(MergePosition mergePosition) {
        toMergeHorizontal.add(mergePosition);
    }

    public void addToMergeVertical(MergePosition mergePosition) {
        toMergeVertical.add(mergePosition);
    }
}
