package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEnsino;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

@Repository
public class AudespAlertasRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public Integer getAlertasByItemAnalise(Integer codigoIBGE, Integer exercicio, Integer mesReferencia, String[] itens) {
        String dataInicio = getDataInicioAlertas(exercicio, mesReferencia);
        String dataFim = getDataFimAlertas(exercicio, mesReferencia);
        String where = getAlertaWhere(mesReferencia, dataInicio, dataFim);
        String itensAnalise = "( ";
        for(int i = 0; i < itens.length; i++) {
            itensAnalise += " item_analise.nm_item_analise like '"+itens[i].trim()+"%' ";
            if( i < itens.length - 1)
                itensAnalise += " or ";
        }
        itensAnalise += " )";

        Query query = entityManager.createNativeQuery("SELECT count(resultado_analise_regra_id)  " +
                "from resultado_analise_regra   " +
                "inner join hipotese on resultado_analise_regra.hipotese_id = hipotese.hipotese_id  " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id  " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id  " +
                "inner join item_analise on resultado_analise_regra.item_analise_id = item_analise.item_analise_id  " +
                "where situacao_alerta is true " +
                "and resultado_analise_regra.ano_exercicio >= :exercicio  " +
                "and entidade.municipio_id <= 644  " +
                "and entidade.tp_entidade_id in (50)  " +
                "and municipio.cd_municipio_ibge = :codigoIBGE  " +
                where +
                " and  " +
                itensAnalise );
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        BigInteger quantidadeAlertas = (BigInteger) query.getSingleResult();
        return quantidadeAlertas.intValue();
    }

    public Integer getAlertasDesajusteExecucaoOrcamentaria(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        String dataInicio = getDataInicioAlertas(exercicio, mesReferencia);
        String dataFim = getDataFimAlertas(exercicio, mesReferencia);
        String where = getAlertaWhere(mesReferencia, dataInicio, dataFim);

        Query query = entityManager.createNativeQuery("SELECT count(resultado_analise_regra_id)  " +
                "from resultado_analise_regra   " +
                "inner join hipotese on resultado_analise_regra.hipotese_id = hipotese.hipotese_id  " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id  " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id  " +
                "inner join item_analise on resultado_analise_regra.item_analise_id = item_analise.item_analise_id  " +
                "where situacao_alerta is true " +
                "and resultado_analise_regra.ano_exercicio >= :exercicio  " +
                "and entidade.municipio_id <= 644  " +
                "and entidade.tp_entidade_id in (50)  " +
                "and municipio.cd_municipio_ibge = :codigoIBGE  " +
                where +
                " and  " +
                "( item_analise.nm_item_analise like 'GF15%' " +
                "or item_analise.nm_item_analise like 'GF16%' )" );
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        BigInteger quantidadeAlertas = (BigInteger) query.getSingleResult();
        return quantidadeAlertas.intValue();
    }

    public Integer getAlertasEducacao(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        String dataInicio = getDataInicioAlertas(exercicio, mesReferencia);
        String dataFim = getDataFimAlertas(exercicio, mesReferencia);
        String where = getAlertaWhere(mesReferencia, dataInicio, dataFim);

        Query query = entityManager.createNativeQuery("SELECT count(resultado_analise_regra_id)  " +
                "from resultado_analise_regra   " +
                "inner join hipotese on resultado_analise_regra.hipotese_id = hipotese.hipotese_id  " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id  " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id  " +
                "inner join item_analise on resultado_analise_regra.item_analise_id = item_analise.item_analise_id  " +
                "where situacao_alerta is true " +
                "and resultado_analise_regra.ano_exercicio >= :exercicio  " +
                "and entidade.municipio_id <= 644  " +
                "and entidade.tp_entidade_id in (50)  " +
                "and municipio.cd_municipio_ibge = :codigoIBGE  " +
                where +
                " and  " +
                "( item_analise.nm_item_analise like 'AE03%' " +
                "or item_analise.nm_item_analise like 'AE04%' " +
                "or item_analise.nm_item_analise like 'AE05%' " +
                "or item_analise.nm_item_analise like 'AE06%' )" );
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        BigInteger quantidadeAlertas = (BigInteger) query.getSingleResult();
        return quantidadeAlertas.intValue();
    }

    public Integer getAlertasSaude(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        String dataInicio = getDataInicioAlertas(exercicio, mesReferencia);
        String dataFim = getDataFimAlertas(exercicio, mesReferencia);
        String where = getAlertaWhere(mesReferencia, dataInicio, dataFim);

        Query query = entityManager.createNativeQuery("SELECT count(resultado_analise_regra_id)  " +
                "from resultado_analise_regra   " +
                "inner join hipotese on resultado_analise_regra.hipotese_id = hipotese.hipotese_id  " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id  " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id  " +
                "inner join item_analise on resultado_analise_regra.item_analise_id = item_analise.item_analise_id  " +
                "where situacao_alerta is true " +
                "and resultado_analise_regra.ano_exercicio >= :exercicio  " +
                "and entidade.municipio_id <= 644  " +
                "and entidade.tp_entidade_id in (50)  " +
                "and municipio.cd_municipio_ibge = :codigoIBGE  " +
                where +
                " and  " +
                "( item_analise.nm_item_analise like 'AS03%' " +
                "or item_analise.nm_item_analise like 'AS04%' ) "  );
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        BigInteger quantidadeAlertas = (BigInteger) query.getSingleResult();
        return quantidadeAlertas.intValue();
    }

    public Integer getAlertasDespesaComPessoal(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {

        String dataInicio = getDataInicioAlertas(exercicio, mesReferencia);
        String dataFim = getDataFimAlertas(exercicio, mesReferencia);
        String where = getAlertaWhere(mesReferencia, dataInicio, dataFim);

        Query query = entityManager.createNativeQuery("SELECT count(resultado_analise_regra_id)  " +
                "from resultado_analise_regra   " +
                "inner join hipotese on resultado_analise_regra.hipotese_id = hipotese.hipotese_id  " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id  " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id  " +
                "inner join item_analise on resultado_analise_regra.item_analise_id = item_analise.item_analise_id  " +
                "where situacao_alerta is true  " +
                "and resultado_analise_regra.ano_exercicio >= :exercicio  " +
                "and entidade.municipio_id <= 644  " +
                "and entidade.tp_entidade_id in (50)  " +
                "and municipio.cd_municipio_ibge = :codigoIBGE  " +
                where +
                "and  " +
                "( item_analise.nm_item_analise like 'GF27%'  " +
                " )" );
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        BigInteger quantidadeAlertas = (BigInteger) query.getSingleResult();
        return quantidadeAlertas.intValue();
    }

    private String getAlertaWhere(Integer mesReferencia, String dataInicio, String dataFim) {
        String where;
        if(mesReferencia.equals(4))
            where = " and mes_referencia in (1,2,3,4) and ano_exercicio = :exercicio ";
        else if(mesReferencia.equals(8))
            where = " and mes_referencia in (1,2,3,4,5,6,7,8) and ano_exercicio = :exercicio ";
        else
            where = " and resultado_analise_regra.dt_processamento between '" + dataInicio + "' and '" + dataFim  + "' " ;
        return where;
    }

    private String getDataFimAlertas(Integer exercicio, Integer mesReferencia) {
        String dataFim;
        if(mesReferencia.equals(4)){
            dataFim = exercicio + "-04-30 23:59:59";
        } else if(mesReferencia.equals(8)) {
            dataFim = exercicio + "-08-31 23:59:59";
        } else {
            dataFim = exercicio + "-10-31 23:59:59";
        }
        return dataFim;
    }

    private String getDataInicioAlertas(Integer exercicio, Integer mesReferencia) {
        String dataInicio;
        if(mesReferencia.equals(4)){
            dataInicio = exercicio + "-01-01 00:00:00";
        } else if(mesReferencia.equals(8)) {
            dataInicio = exercicio + "-01-01 00:00:00";
        } else {
            dataInicio = exercicio + "-01-01 00:00:00";
        }
        return dataInicio;
    }

}
