package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.ParametroBusca;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEnsino;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultadoExecucaoOrcamentaria;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespResultadoExecucaoOrcamentariaRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoExecucaoOrcamentaria(ParametroBusca parametroBusca) {

        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = :tipoEntidadeId " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vDevDuod', 'vTotDespEmpBO', 'vRepDuodCM', 'vRecArrec', 'vTotDespExec', 'vTransfAdmIndireta', 'vResExecOrcGeralPM', 'vResExecOrcGeralPMAV', 'vTransfFinAdmIndExec', 'vResExecOrcGeralPMAV', 'vTotRecRealPM') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio = :exercicio " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", parametroBusca.getCodigoIBGE());
        query.setParameter("exercicio", parametroBusca.getExercicio());
        query.setParameter("mesReferencia", parametroBusca.getMesReferencia());
        query.setParameter("tipoEntidadeId", parametroBusca.getTipoEntidadeId());
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 8 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }

    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoExecucaoOrcamentariaQuadrimestral(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {

        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vRecRealEnt','vDedRecRealEnt','vDespLiqEnt','vRepDuodExec','vRepDuodCM','vDevDuod','vTransfFinAdmIndExec','vResExeOrcLiq', 'vPercResExecOrc', 'vRecArrec', 'vSubTotRecRealPM', 'vDespCorExec', 'vDespCapExec', 'vDespIntraOrcExec') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio = :exercicio " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 8 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }

    public List<AudespResultado> getAudespReceitaTotalMunicipio(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vTotRecRealCons') " +
                "and mes_referencia in ( :mesReferencia ) " +
                "and ano_exercicio in( :exercicio , :exercicio -1, :exercicio -2, :exercicio -3 ) " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultado.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultado> lista = query.getResultList();
        for(int i = lista.size(); i < 3 ; i++)
            lista.add(new AudespResultado());
        return lista;
    }

//    public List<AudespResultado> getRCL(Integer codigoIBGE, Integer exercicio) {
//        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
//                " vw_valor_parametro.nm_parametro_analise,   " +
//                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
//                "ds_parametro_analise from vw_valor_parametro   " +
//                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
//                "where 1 = 1 " +
//                "and vw_valor_parametro.entidade_id = (   " +
//                "select entidade.entidade_id   " +
//                "from municipio   " +
//                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
//                "where entidade.tp_entidade_id = 50   " +
//                "and entidade.municipio_id <= 644   " +
//                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
//                "and parametro_analise.nm_parametro_analise " +
//                "in ('vRCL') " +
//                "and ano_exercicio in( :exercicio , :exercicio -1, :exercicio -2, :exercicio -3 ) " +
//                "group by mes_referencia " +
//                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultado.class);
//        query.setParameter("codigoIBGE", codigoIBGE);
//        query.setParameter("exercicio", exercicio);
//        List<AudespResultado> lista = query.getResultList();
//        for(int i = lista.size(); i < 12 ; i++)
//            lista.add(new AudespResultado());
//        return lista;
//    }

    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoExecucaoOrcamentariaReceitasNaoAmparado(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vRecCorPrev' , 'vRecCorReal', 'vRecCorAH', 'vRecCorPMAV', " +
                "'vRecCapPrev', 'vRecCapReal', 'vRecCapOpCredAH', 'vRecCapOpCredAV', " +
                "'vRecIntraOrcPrev', 'vRecIntraOrcReal', 'vRecIntraOrcAH', 'vRecIntraOrcPMAV', "  +
                "'vDedRecPrev', 'vDedRecPrev', 'vDedRecReal', 'vDedRecAH', 'vDedRecPMAV', " +
                "'vSubTotRecPrevPM', 'vSubTotRecRealPM', 'vSubTotRecPMAH' ,'vSubTotRecPMAV', " +
                "'vTotRecPrevPM', 'vTotRecRealPM', 'vTotRecPMAV', " +
                "'vResExecOrcRecPM', 'vResExecOrcRecPMAV') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio = :exercicio " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 26 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }

    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoExecucaoOrcamentariaDespesasNaoAmparado(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vDespCorFix', 'vDespCorExec', 'vDespCorAH', 'vDespCorPMAV', " +
                "'vDespCapAmortFix','vDespCapAmortExec','vDespCapAmortAH','vDespCapAmortAV', " +
                "'vResCont', " +
                "'vDespIntraOrcFix','vDespIntraOrcExec','vDespIntraOrcAH','vDespIntraOrcPMAV', " +
                "'vRepDuodFix', 'vRepDuodExec', 'vRepDuodAH', 'vRepDuodAV', " +
                "'vDevDuod','vDevDuodAV', " +
                "'vTransfFinAdmIndFix', 'vTransfFinAdmIndExec', 'vTransfFinAdmIndAH', 'vTransfFinAdmIndAV', " +
                "'vSubTotDespFixPM','vSubTotDespExecPM','vSubTotDespPMAH','vSubTotDespPMAV', " +
                "'vTotDespFixPM', 'vTotDespExecPM','vTotDespPMAV', " +
                "'vEconOrc', 'vEconOrcAV', " +
                "'vResExecOrcGeralPM', 'vResExecOrcGeralPMAV') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio = :exercicio " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 34 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }

    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoExecucaoOrcamentariaUltimosTresExercicios(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE )  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vResExecOrcGeralPMAV') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio in( :exercicio, :exercicio-1, :exercicio-2, :exercicio-3 )  " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 4 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }

    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoFinanceiroEconomicoSaldoPatrimonial(ParametroBusca parametroBusca) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = :tipoEntidadeId   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vResultadoFinanceiro', 'vResFinanceiroAH', 'vResultadoEconomico','vResEconomicoAH','vResultadoPatrimonial','vResPatrimonialAH', 'vSalPatrAtu', 'vSalPatrAnt', 'vRecArrec') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio in(:exercicio, :exercicio-1)  " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", parametroBusca.getCodigoIBGE());
        query.setParameter("exercicio", parametroBusca.getExercicio());
        query.setParameter("mesReferencia", parametroBusca.getMesReferencia());
        query.setParameter("tipoEntidadeId", parametroBusca.getTipoEntidadeId());
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 12 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }

    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoInfluenciaOrcamentarioFinanceiro(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vResFinAnt', 'vAjuVarAtiv', 'vAjuVarPas', 'vResFinAntAju', 'vResMovOrc', 'vResFinAtuApu') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio in( :exercicio)  " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 6 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }


    public List<AudespResultadoExecucaoOrcamentaria> getAudespResultadoRepassesCamara(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vCamUtil', 'vCamDespInativos', 'vCamSubTotal', 'vArt29ATotal', 'vCamPercRepasse') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio in( :exercicio, :exercicio -1)  " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 10 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }

    public List<AudespResultadoExecucaoOrcamentaria> getIndiceDeLiquidezImediata(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vSalFinAtivoDispAjuAud', 'vPassivoCircAju') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio in ( :exercicio, :exercicio -1)  " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultadoExecucaoOrcamentaria.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultadoExecucaoOrcamentaria> lista = query.getResultList();
        for(int i = lista.size(); i < 2 ; i++)
            lista.add(new AudespResultadoExecucaoOrcamentaria());
        return lista;
    }




}