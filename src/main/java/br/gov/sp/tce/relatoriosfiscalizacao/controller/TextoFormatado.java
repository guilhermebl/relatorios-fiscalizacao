package br.gov.sp.tce.relatoriosfiscalizacao.controller;


import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;

import java.math.BigInteger;

public class TextoFormatado {

    private String texto = "";
    private Formatacao formatacao;
    private TextoFormatado next;
    private BigInteger listNumId;


    public void setParagraphText(XWPFParagraph paragraphText) {



        XWPFRun run = paragraphText.createRun();
        this.formatacao.formatRun(run);



        if(next == null) {
            paragraphText.setAlignment(formatacao.getAlignment());
        }
        if(listNumId != null){
            paragraphText.setNumID(this.listNumId);
        }


        if (texto.contains("\n")) {
            String[] lines = texto.split("\n");
            run.setText(lines[0]);
            for(int i = 1; i < lines.length; i++) {
                run.addBreak();
                run.setText(lines[i]);
            }
        } else {
            run.setText(texto);
        }



        if(next != null)
            next.setParagraphText(paragraphText);

    }



    public TextoFormatado(String texto, Formatacao formatacao) {
        if(formatacao == null)
            throw new IllegalArgumentException("Valores nulos não permitidos");
        this.texto = validaTexto(texto);
        this.formatacao = formatacao;
    }

    public TextoFormatado(String texto) {
        this.texto = validaTexto(texto);
        this.formatacao = formatoPadrao();
    }

    private Formatacao formatoPadrao() {
        FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");
        Formatacao formatacao = formatacaoFactory.getJustificado(12);
        return formatacao;
    }

    private String validaTexto(String texto) {
        return texto == null ? " " : texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Formatacao getFormatacao() {
        return formatacao;
    }

    public void setFormatacao(Formatacao formatacao) {
        this.formatacao = formatacao;
    }

    public TextoFormatado concat(String texto, Formatacao formatacao) {
        TextoFormatado textoFormatado = new TextoFormatado(texto, formatacao);

        return concat(textoFormatado);
    }

    public TextoFormatado concat(TextoFormatado textoFormatado) {
        TextoFormatado next = this.next;
        if(next == null)
            this.next = next = textoFormatado;
        else {
            while (next.getNext() != null)
                next = next.getNext();
            next.setNext(textoFormatado);
        }

        return this;
    }

    public TextoFormatado concat(String texto) {
        return concat(texto, this.formatacao);
    }

    public TextoFormatado getNext() {
        return next;
    }

    public void setNext(TextoFormatado next) {
        this.next = next;
    }

    public BigInteger getListNumId() {
        return listNumId;
    }

    public void setListNumId(BigInteger listNumId) {
        this.listNumId = listNumId;
    }
}
