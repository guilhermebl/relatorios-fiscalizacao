package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.TaxaInvestimento;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.TcespBiRclMensal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.AlicotaEntidadeDevedora;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.repository.TcespBiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TcespBiService {

    @Autowired
    private TcespBiRepository tcespBiRepository;

    @Autowired
    private AudespBiService audespBiService;

    public List<TcespBiRclMensal> getRCLMunicipioList(Integer codigoIbge, Integer exercicio) {

        Integer skMunicipio = audespBiService.getSkMunicipio(codigoIbge);

        List<TcespBiRclMensal> listaRCL = tcespBiRepository.getRCLMunicipio(skMunicipio, exercicio);

        return listaRCL;
    }

    public Map<String,String> getRCLMunicipioFormatado(Integer codigoIbge, Integer exercicio) {

        List<TcespBiRclMensal> listaRCL = getRCLMunicipioList(codigoIbge, exercicio);

        AlicotaEntidadeDevedora alicota = tcespBiRepository.getAlicotaEntidadeDevedora(codigoIbge);

        String alicotaFormatada = FormatadorDeDados
                .formatarPercentual(alicota.getAlicota(), true, false, 3);

        Map<String, String> mapa = new HashMap<>();

        mapa.put("alicota", alicotaFormatada);

        BigDecimal valorADepositarTotal = new BigDecimal(0);

        for(TcespBiRclMensal tcespBiRclMensal : listaRCL) {
            BigDecimal valorAlicota = alicota.getAlicota();

            BigDecimal rcl = tcespBiRclMensal.getValor();
            BigDecimal valor = valorAlicota == null ? null : tcespBiRclMensal.getValor().multiply(valorAlicota);
            BigDecimal valorADepositar =  valorAlicota == null ? null : valor.divide(new BigDecimal(12), RoundingMode.HALF_DOWN);
            valorADepositarTotal = valorAlicota == null ? valorADepositarTotal : valorADepositarTotal.add(valorADepositar);

            String rclFormatado = FormatadorDeDados.formatarMoeda(rcl,true,false);
            String valorFormatado = FormatadorDeDados.formatarMoeda(valor,true,false);
            String valorADepositarFormatado = FormatadorDeDados.formatarMoeda(valorADepositar,true,false);

            mapa.put("rcl" + tcespBiRclMensal.getAno() + tcespBiRclMensal.getMes() , rclFormatado);
            mapa.put("valor" + tcespBiRclMensal.getAno() + tcespBiRclMensal.getMes() , valorFormatado);
            mapa.put("depositar" + tcespBiRclMensal.getAno() + tcespBiRclMensal.getMes() , valorADepositarFormatado);
        }

        mapa.put("valorADepositarTotal", FormatadorDeDados.formatarMoeda(valorADepositarTotal,true,true));


        return mapa;
    }


    public String getTaxaIvestimentoFormatada(Integer codigoIbge, Integer exercicio) {

        TaxaInvestimento taxaInvestimento = tcespBiRepository.getTaxaInvestimento(codigoIbge, exercicio);

        String taxaInvestimentoFormatada = FormatadorDeDados
                .formatarPercentual(taxaInvestimento.getPercentualTaxaComRestosAPagar(), true, false, 2);

        return taxaInvestimentoFormatada;
    }

    public Map<String,BigDecimal> getRCLMunicipio(Integer codigoIbge, Integer exercicio) {

        List<TcespBiRclMensal> listaRCL = getRCLMunicipioList(codigoIbge, exercicio);

        AlicotaEntidadeDevedora alicota = tcespBiRepository.getAlicotaEntidadeDevedora(codigoIbge);

        Map<String, BigDecimal> mapa = new HashMap<>();

        mapa.put("alicota", alicota.getAlicota());

        BigDecimal valorADepositarTotal = new BigDecimal(0);

        for(TcespBiRclMensal tcespBiRclMensal : listaRCL) {
            BigDecimal valorAlicota = alicota.getAlicota();

            BigDecimal rcl = tcespBiRclMensal.getValor();
            BigDecimal valor = valorAlicota == null ? null : tcespBiRclMensal.getValor().multiply(valorAlicota);
            BigDecimal valorADepositar =  valorAlicota == null ? null : valor.divide(new BigDecimal(12), RoundingMode.HALF_DOWN);
            valorADepositarTotal = valorAlicota == null ? valorADepositarTotal : valorADepositarTotal.add(valorADepositar);

            String rclFormatado = FormatadorDeDados.formatarMoeda(rcl,true,false);
            String valorFormatado = FormatadorDeDados.formatarMoeda(valor,true,false);
            String valorADepositarFormatado = FormatadorDeDados.formatarMoeda(valorADepositar,true,false);

            mapa.put("rcl" + tcespBiRclMensal.getAno() + tcespBiRclMensal.getMes() , rcl);
            mapa.put("valor" + tcespBiRclMensal.getAno() + tcespBiRclMensal.getMes() , valor);
            mapa.put("depositar" + tcespBiRclMensal.getAno() + tcespBiRclMensal.getMes() , valorADepositar);
        }

        mapa.put("valorADepositarTotal", valorADepositarTotal);


        return mapa;
    }


    public AlicotaEntidadeDevedora getAlicotaEntidadeDevedora(Integer codigoIbge) {
        return tcespBiRepository.getAlicotaEntidadeDevedora(codigoIbge);
    }

    public MunicipioIbge getMunicipioByCodigoIbgeExercicio(Integer codigoIbge, Integer exercicio) {
        return tcespBiRepository.getMunicipioByCodigoIbgeExercicio(codigoIbge, exercicio);
    }

}
