package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODSKey;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Locale;

@Entity
@IdClass(ApontamentoODSKey.class)
public class ApontamentoODS {

    @Id
    private Integer respid;

    @Id
    private Integer odsid;

    @Id
    private Integer exercicio;

    @Column(name = "municipio_ibge")
    private Integer municipioIbge;

    @Column(name = "municipio_nome")
    private String municipioNome;

    @Column(name = "orgao")
    private String orgao;

    @Column(name = "iegm_indice")
    private String iegmIndice = "";

    @Column(name = "iegm_pergunta_codigo")
    private String iegmPerguntaCodigo;

    @Column(name = "iegm_pergunta_numero")
    private String iegmPerguntaNumero;

    @Column(name = "iegm_pergunta_tipo")
    private String iegmPerguntaTipo;

    @Column(name = "iegm_pergunta_texto")
    private String iegmPerguntaTexto;

    @Column(name = "iegm_opcao_resposta_codigo")
    private String iegmOpcaoRespostaCodigo;

    @Column(name = "iegm_opcao_resposta_descricao")
    private String iegmOpcaoRespostaDescricao;

    @Column(name = "iegm_data_resposta")
    private LocalDateTime iegmDataResposta;

    @Column(name = "iegm_valor_resposta")
    private String iegmValorResposta;

    @Column(name = "iegm_valor_comentario")
    private String iegmValorComentario;

    @Column(name = "ods_item")
    private String odsItem;

    @Column(name = "ods_influencia")
    private String odsInfluencia;

    @Column(name = "ods_apontamento")
    private String odsApontamento;

    public Integer getRespid() {
        return respid;
    }

    public void setRespid(Integer respid) {
        this.respid = respid;
    }

    public Integer getOdsid() {
        return odsid;
    }

    public void setOdsid(Integer odsid) {
        this.odsid = odsid;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getMunicipioIbge() {
        return municipioIbge;
    }

    public void setMunicipioIbge(Integer municipioIbge) {
        this.municipioIbge = municipioIbge;
    }

    public String getMunicipioNome() {
        return municipioNome;
    }

    public void setMunicipioNome(String municipioNome) {
        this.municipioNome = municipioNome;
    }

    public String getOrgao() {
        return orgao;
    }

    public void setOrgao(String orgao) {
        this.orgao = orgao;
    }

    public String getIegmIndice() {
        if(iegmIndice != null)
            return iegmIndice;
        else
            return "";
    }

    public void setIegmIndice(String iegmIndice) {
        this.iegmIndice = iegmIndice;
    }

    public String getIegmPerguntaCodigo() {
        return iegmPerguntaCodigo;
    }

    public void setIegmPerguntaCodigo(String iegmPerguntaCodigo) {
        this.iegmPerguntaCodigo = iegmPerguntaCodigo;
    }

    public String getIegmPerguntaNumero() {
        return iegmPerguntaNumero;
    }

    public void setIegmPerguntaNumero(String iegmPerguntaNumero) {
        this.iegmPerguntaNumero = iegmPerguntaNumero;
    }

    public String getIegmPerguntaTipo() {
        return iegmPerguntaTipo;
    }

    public void setIegmPerguntaTipo(String iegmPerguntaTipo) {
        this.iegmPerguntaTipo = iegmPerguntaTipo;
    }

    public String getIegmPerguntaTexto() {
        return iegmPerguntaTexto;
    }

    public void setIegmPerguntaTexto(String iegmPerguntaTexto) {
        this.iegmPerguntaTexto = iegmPerguntaTexto;
    }

    public String getIegmOpcaoRespostaCodigo() {
        return iegmOpcaoRespostaCodigo;
    }

    public void setIegmOpcaoRespostaCodigo(String iegmOpcaoRespostaCodigo) {
        this.iegmOpcaoRespostaCodigo = iegmOpcaoRespostaCodigo;
    }

    public String getIegmOpcaoRespostaDescricao() {
        return iegmOpcaoRespostaDescricao;
    }

    public void setIegmOpcaoRespostaDescricao(String iegmOpcaoRespostaDescricao) {
        this.iegmOpcaoRespostaDescricao = iegmOpcaoRespostaDescricao;
    }

    public LocalDateTime getIegmDataResposta() {
        return iegmDataResposta;
    }

    public void setIegmDataResposta(LocalDateTime iegmDataResposta) {
        this.iegmDataResposta = iegmDataResposta;
    }

    public String getIegmValorResposta() {
        return iegmValorResposta;
    }

    public void setIegmValorResposta(String iegmValorResposta) {
        this.iegmValorResposta = iegmValorResposta;
    }

    public String getIegmValorComentario() {
        return iegmValorComentario;
    }

    public void setIegmValorComentario(String iegmValorComentario) {
        this.iegmValorComentario = iegmValorComentario;
    }

    public String getOdsItem() {
        return odsItem;
    }

    public void setOdsItem(String odsItem) {
        this.odsItem = odsItem;
    }

    public String getOdsInfluencia() {
        return odsInfluencia;
    }

    public void setOdsInfluencia(String odsInfluencia) {
        this.odsInfluencia = odsInfluencia;
    }

    public String getOdsApontamento() {
        return odsApontamento;
    }

    public void setOdsApontamento(String odsApontamento) {
        this.odsApontamento = odsApontamento;
    }
}
