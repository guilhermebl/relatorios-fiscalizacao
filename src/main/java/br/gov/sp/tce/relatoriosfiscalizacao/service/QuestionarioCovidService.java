package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.repository.IegmSmartRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.QuestionarioItem;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository.IegmRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository.QuestionarioCovidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QuestionarioCovidService {

    @Autowired
    private QuestionarioCovidRepository questionarioCovidRepository;

    public QuestionarioItem getRespostaPorCodigoPergunta(Integer codigoIBGE, Integer operacaoId, String codigoPergunta) {
        List<QuestionarioItem> lista = questionarioCovidRepository.getRespostaPorCodigoPergunta(codigoIBGE, operacaoId, codigoPergunta);
        QuestionarioItem item = new QuestionarioItem();
        if(!lista.isEmpty()) {
            item = lista.get(0);
        }
        return item;
    }

    public QuestionarioItem getRespostaPorCodigoOpcaoResposta(Integer codigoIBGE, Integer operacaoId, String codigoPergunta) {
        List<QuestionarioItem> lista = questionarioCovidRepository.getRespostaPorCodigoOpcaoResposta(codigoIBGE, operacaoId, codigoPergunta);
        QuestionarioItem item = new QuestionarioItem();
        if(!lista.isEmpty()) {
            item = lista.get(0);
        }
        return item;
    }

    public Boolean hasQuedaArrecadacao(Integer codigoIBGE, Integer operacaoId) {
        List<QuestionarioItem> listaReceitaTotalArrecadada = questionarioCovidRepository
                .getRespostaPorCodigoPergunta(codigoIBGE, operacaoId, "COVID192002G01Q00100");
        QuestionarioItem receitaTotalArrecadada = new QuestionarioItem();
        if(!listaReceitaTotalArrecadada.isEmpty()) {
            receitaTotalArrecadada = listaReceitaTotalArrecadada.get(0);
        }
        List<QuestionarioItem> listaReceitaTotalEstimada = questionarioCovidRepository
                .getRespostaPorCodigoPergunta(codigoIBGE, operacaoId, "COVID192002G01Q00200");
        QuestionarioItem receitaTotalEstimada = new QuestionarioItem();
        if(!listaReceitaTotalEstimada.isEmpty()) {
            receitaTotalEstimada = listaReceitaTotalEstimada.get(0);
        }

        System.out.println(receitaTotalEstimada.getValorNumerico());
        System.out.println(receitaTotalArrecadada.getValorNumerico());
        if(receitaTotalEstimada.getValorNumerico().compareTo(receitaTotalArrecadada.getValorNumerico()) > 0 )
            return true;
        else
            return false;
    }

    public String getReceitaTotalEstimadaFormatada(Integer codigoIBGE, Integer operacaoId) {
        List<QuestionarioItem> listaReceitaTotalEstimada = questionarioCovidRepository
                .getRespostaPorCodigoPergunta(codigoIBGE, operacaoId, "COVID192002G01Q00200");
        QuestionarioItem receitaTotalEstimada = new QuestionarioItem();
        if(!listaReceitaTotalEstimada.isEmpty()) {
            receitaTotalEstimada = listaReceitaTotalEstimada.get(0);
        }

        return FormatadorDeDados.formatarMoeda(receitaTotalEstimada.getValorNumerico(), true, true);

    }

    public String getReceitaTotalArrecadadaFormatada(Integer codigoIBGE, Integer operacaoId) {
        List<QuestionarioItem> listaReceitaTotalArrecadada = questionarioCovidRepository
                .getRespostaPorCodigoPergunta(codigoIBGE, operacaoId, "COVID192002G01Q00100");
        QuestionarioItem receitaTotalArrecadada = new QuestionarioItem();
        if(!listaReceitaTotalArrecadada.isEmpty()) {
            receitaTotalArrecadada = listaReceitaTotalArrecadada.get(0);
        }

        return FormatadorDeDados.formatarMoeda(receitaTotalArrecadada.getValorNumerico(), true, true);
    }

}