package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespDividaAtivaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespDividaAtivaService {

    @Autowired
    private AudespDividaAtivaRepository audespDividaAtivaRepository;

    public Map<String, String> getDividaAtivaFormatado(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultado> dividaAtivaList = audespDividaAtivaRepository.getDividaAtiva(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosValores = Arrays.asList(new String[]{
                "vDivAtivaTotIni",
                "vDivAtivaTotIniAju",
                "vDivAtivaProvPerIni",
                "vDivAtivaProvPerIniAju",
                "vDivAtivaRec",
                "vDivAtivaRecAju",
                "vDivAtivaCanc",
                "vDivAtivaCancAju",
                "vDivAtivaNRec",
                "vDivAtivaNRecAju",
                "vDivAtivaInsc",
                "vDivAtivaInscAju",
                "vDivAtivaAtu",
                "vDivAtivaAtuAju",
                "vDivAtivaProvPerFin",
                "vDivAtivaProvPerFinAju",
                "vDivAtivaFin",
                "vDivAtivaFinAju"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{
                "vDivAtivaTotIniAH",
                "vDivAtivaTotIniAjuAH",
                "vDivAtivaProvPerIniAH",
                "vDivAtivaProvPerIniAjuAH",
                "vDivAtivaRecAH",
                "vDivAtivaRecAjuAH",
                "vDivAtivaCancAH",
                "vDivAtivaCancAjuAH",
                "vDivAtivaNRecAH",
                "vDivAtivaNRecAjuAH",
                "vDivAtivaInscAH",
                "vDivAtivaInscAjuAH",
                "vDivAtivaAtuAH",
                "vDivAtivaAtuAjuAH",
                "vDivAtivaProvPerFinAH",
                "vDivAtivaProvPerFinAjuAH",
                "vDivAtivaFinAH",
                "vDivAtivaFinAjuAH"});

        Map<String, String> mapa = new HashMap<>();
        for (AudespResultado resultado : dividaAtivaList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false, 2));
            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(), FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));

            }
        }

        return mapa;

    }

    public Map<String, BigDecimal> getDividaAtiva(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultado> dividaAtivaList = audespDividaAtivaRepository.getDividaAtiva(codigoIbge, exercicio, mesReferencia);

        Map<String, BigDecimal> mapa = new HashMap<>();

        for (AudespResultado resultado : dividaAtivaList) {
                  mapa.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(), resultado.getValor());
        }

        return mapa;

    }
}