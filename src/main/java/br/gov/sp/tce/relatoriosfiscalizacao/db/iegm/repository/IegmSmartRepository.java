package br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class IegmSmartRepository {

    @PersistenceContext(unitName = "iegm")
    private EntityManager entityManager;

    public List<NotaIegm> getNotasByCodigoIbge(Integer codigoIBGE,  Integer exercicio ) {

        Query query =  entityManager.createNativeQuery("select * from (select exercicio,cd_municipio_ibge,ds_municipio, " +
                "iegm,faixa_iegm,ieduc,faixa_ieduc,isaude,faixa_isaude, " +
                "iplanejamento,faixa_iplanejamento,ifiscal,faixa_ifiscal, " +
                "iamb,faixa_iamb,icidade,faixa_icidade,igov,faixa_igov " +
                "from dbo.VW_IEGM " +
                "where " +
                "cd_municipio_ibge = :codigoIBGE " +
                "and exercicio in ( :exercicio, :exercicio -1, :exercicio -2 ) " +
                "union all " +
                "select exercicio,cd_municipio_ibge,ds_municipio, " +
                "iegm_final as iegm, faixa_iegm_final as faixa_iegm, ieduc_final as ieduc,faixa_ieduc_final as faixa_ieduc," +
                "isaude_final as isaude,faixa_isaude_final as faixa_isaude, " +
                "iplanejamento_final as iplanejamento,faixa_iplanejamento_final as faixa_iplanejamento,ifiscal_final as ifiscal," +
                "faixa_ifiscal_final as faixa_ifiscal, " +
                "iamb_final as iamb,faixa_iamb_final as faixa_iamb,icidade_final as icidade,faixa_icidade_final as faixa_icidade, " +
                "igov_final as igov,faixa_igov_final as faixa_igov " +
                "from dbo.IEGM_2018 " +
                "where " +
                "cd_municipio_ibge = :codigoIBGE " +
                "and exercicio in ( :exercicio ) " +
                ") as notaIEGM order by exercicio desc" , NotaIegm.class);
        query.setParameter("exercicio", exercicio);
        query.setParameter("codigoIBGE", codigoIBGE);
        return query.getResultList();
    }


    public MunicipioIbge getMunicipioByCodigoIbge(Integer codigoIBGE) {
        Query query =  entityManager.createNativeQuery("SELECT TOP (1) [codigo_IBGE]  as codigoMunicipioIbge, " +
                "[População 2018] as populacao " +
                "from [Iegm].[dbo].[CARACTERISTICAS_GERAIS_2_2018] " +
                "where [codigo_IBGE]  = :codigoIBGE", MunicipioIbge.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        List<MunicipioIbge>  lista = query.getResultList();
        if(lista.size() == 0)
            return new MunicipioIbge();
        else
            return lista.get(0);
    }
}