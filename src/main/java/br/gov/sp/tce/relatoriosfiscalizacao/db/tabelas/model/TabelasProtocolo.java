package br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TabelasProtocolo {

    @Id
    private String processo;

    @Column(name = "codigoOrgaoMainframe")
    private String procedencia;

    @Column(name = "nomeOrgao")
    private String nomeOrgao;

    private Integer exercicio;

    @Column(name = "secaoFiscalizadoraContas")
    private String secaoFiscalizadoraContas;

    @Column(name = "dsf")
    private String dsf;

    private String descricaoArea;

    private String objeto;

    @Column(name = "cdRelator")
    private String siglaRelator;

    private String relator;

    @Column(name = "cnpjOrgao")
    private String cnpjOrgao;

    @Column(name = "porteMunicipio")
    private String porteMunicipio;

    @Column(name = "codigoIbge")
    private Integer codigoIbge;

    public String getProcesso() {
        if(processo != null)
            return processo.trim();
        else
            return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getNomeOrgao() {
        return nomeOrgao;
    }

    public void setNomeorgao(String nomeOrgao) {
        this.nomeOrgao = nomeOrgao;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public String getSecaoFiscalizadoraContas() {
        return secaoFiscalizadoraContas;
    }

    public void setSecaoFiscalizadoraContas(String secaoFiscalizadoraContas) {
        this.secaoFiscalizadoraContas = secaoFiscalizadoraContas;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    public String getSiglaRelator() {
        return siglaRelator;
    }

    public void setSiglaRelator(String siglaRelator) {
        this.siglaRelator = siglaRelator;
    }

    public String getRelator() {
        return relator;
    }

    public void setRelator(String relator) {
        this.relator = relator;
    }

    public String getCnpjOrgao() {
        return cnpjOrgao;
    }

    public void setCnpjOrgao(String cnpjOrgao) {
        this.cnpjOrgao = cnpjOrgao;
    }

    public String getPorteMunicipio() {
        return porteMunicipio;
    }

    public void setPorteMunicipio(String porteMunicipio) {
        this.porteMunicipio = porteMunicipio;
    }

    public Integer getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(Integer codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public void setNomeOrgao(String nomeOrgao) {
        this.nomeOrgao = nomeOrgao;
    }

    public String getDescricaoArea() {
        return descricaoArea;
    }

    public void setDescricaoArea(String descricaoArea) {
        this.descricaoArea = descricaoArea;
    }

    public String getSaudacao() {
        if(secaoFiscalizadoraContas != null && Character.isDigit(secaoFiscalizadoraContas.charAt(0))){
            return "Senhor(a) Diretor(a) da "+ getDescricaoArea()
                    + ",\nSenhor(a) Chefe Técnico da Fiscalização,";
        }
        else {
            return "Senhor(a) Diretor(a) da " + getDescricaoArea() +
                    ",\nSenhor(a) Chefe Técnico da Fiscalização,";
        }
    }

    public String getDsf() {
        return dsf;
    }

    public void setDsf(String dsf) {
        this.dsf = dsf;
    }
}