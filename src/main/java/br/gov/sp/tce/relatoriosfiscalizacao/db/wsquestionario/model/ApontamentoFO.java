package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.time.LocalDateTime;

@Entity
@IdClass(ApontamentoFOKey.class)
public class ApontamentoFO {

    @Id
    private Integer operacaoId;

    @Id
    private Integer objetoDeFiscalizacaoId;

    @Id
    private Integer codigoIBGE;

    @Id
    private Integer apontamentoId;

    @Column(name = "operacao_nome")
    private String operacaoNome;

    @Column(name = "operacao_data")
    private LocalDateTime operacaoData;

    @Column(name = "operacao_tema")
    private String operacaoTema;

    private String municipio;

    @Column(name = "objf_descricao")
    private String objetoDeFiscalizacaoDescricao;

    @Column(name = "objf_processo")
    private String processo;

    private String apontamento;

    public Integer getOperacaoId() {
        return operacaoId;
    }

    public void setOperacaoId(Integer operacaoId) {
        this.operacaoId = operacaoId;
    }

    public Integer getObjetoDeFiscalizacaoId() {
        return objetoDeFiscalizacaoId;
    }

    public void setObjetoDeFiscalizacaoId(Integer objetoDeFiscalizacaoId) {
        this.objetoDeFiscalizacaoId = objetoDeFiscalizacaoId;
    }

    public Integer getCodigoIBGE() {
        return codigoIBGE;
    }

    public void setCodigoIBGE(Integer codigoIBGE) {
        this.codigoIBGE = codigoIBGE;
    }

    public Integer getApontamentoId() {
        return apontamentoId;
    }

    public void setApontamentoId(Integer apontamentoId) {
        this.apontamentoId = apontamentoId;
    }

    public String getOperacaoNome() {
        return operacaoNome;
    }

    public void setOperacaoNome(String operacaoNome) {
        this.operacaoNome = operacaoNome;
    }

    public LocalDateTime getOperacaoData() {
        return operacaoData;
    }

    public void setOperacaoData(LocalDateTime operacaoData) {
        this.operacaoData = operacaoData;
    }

    public String getOperacaoTema() {
        return operacaoTema;
    }

    public void setOperacaoTema(String operacaoTema) {
        this.operacaoTema = operacaoTema;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getObjetoDeFiscalizacaoDescricao() {
        return objetoDeFiscalizacaoDescricao;
    }

    public void setObjetoDeFiscalizacaoDescricao(String objetoDeFiscalizacaoDescricao) {
        this.objetoDeFiscalizacaoDescricao = objetoDeFiscalizacaoDescricao;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo;
    }

    public String getApontamento() {
        return apontamento;
    }

    public void setApontamento(String apontamento) {
        this.apontamento = apontamento;
    }
}
