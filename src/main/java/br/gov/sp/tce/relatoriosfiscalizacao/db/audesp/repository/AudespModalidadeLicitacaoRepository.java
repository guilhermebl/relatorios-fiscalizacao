package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacao;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespModalidadeLicitacaoRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<AudespResultado> getDespesaModalidade(Integer codigoIBGE, Integer exercicio, Integer mes, Integer tipoEntidade) {
        Query query = entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id, " +
                " vw_valor_parametro.nm_parametro_analise, " +
                "ano_exercicio, mes_referencia, coalesce(valor, '0') as valor, parametro_analise_id, " +
                "ds_parametro_analise from vw_valor_parametro " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = ( " +
                    "select entidade.entidade_id " +
                    "from municipio " +
                    "inner join entidade on entidade.municipio_id = municipio.municipio_id " +
                    "where entidade.tp_entidade_id =  :tipoEntidade " +
                    "and entidade.municipio_id <= 644 " +
                    "and municipio.cd_municipio_ibge =  :codigoIBGE ) " +
                "and parametro_analise.nm_parametro_analise " +
                "in ('vDespPasLicConcor', 'vDespPasLicTomPrec', 'vDespPasLicConv', 'vDespPasLicPreg', " +
                "'vDespPasLicConcur', 'vDespPasLicBEC', 'vDespPasLicDisp', 'vDespPasLicInexig', " +
                "'vDespPasLicOutros', 'vDespPasLicRDC', " +
                "'vDespPasLicConcorAV', 'vDespPasLicTomPrecAV', 'vDespPasLicConvAV', 'vDespPasLicPregAV', " +
                "'vDespPasLicConcurAV', 'vDespPasLicBECAV', 'vDespPasLicDispAV', 'vDespPasLicInexigAV', " +
                "'vDespPasLicOutrosAV', 'vDespPasLicRDCAV', 'vTotDespPasLic')  " +
                "and mes_referencia =  :mes  and ano_exercicio =  :exercicio " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultado.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mes", mes);
        query.setParameter("tipoEntidade", tipoEntidade);
        List<AudespResultado> lista = query.getResultList();
        for (int i = lista.size(); i < 3; i++)
            lista.add(new AudespResultado());
        return lista;
    }



}