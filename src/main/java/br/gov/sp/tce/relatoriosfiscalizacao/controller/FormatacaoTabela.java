package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;

import java.util.List;

public class FormatacaoTabela {


    private List<String> widths;
    private boolean border = true;
    private String widthTabela = "100%";
    private int cellMarginTop = 72;
    private int cellMarginRight = 72;
    private int cellMarginBottom = 72;
    private int cellMarginLeft = 72;
    private boolean firstLineHeader = true;

    public List<String> getWidths() {
        return widths;
    }

    public void setWidths(List<String> widths) {
        this.widths = widths;
    }

    public boolean hasBorder() {
        return border;
    }

    public void setBorder(boolean border) {
        this.border = border;
    }

    public String getWidthTabela() {
        return widthTabela;
    }

    public void setWidthTabela(String widthTabela) {
        this.widthTabela = widthTabela;
    }

    public void formatarTabela(XWPFTable tabela) {
        tabela.setWidth(this.widthTabela);
        if(hasBorder() == false) {
            CTTblPr tblpro = tabela.getCTTbl().getTblPr();

            CTTblBorders borders = tblpro.addNewTblBorders();
            borders.addNewBottom().setVal(STBorder.NONE);
            borders.addNewLeft().setVal(STBorder.NONE);
            borders.addNewRight().setVal(STBorder.NONE);
            borders.addNewTop().setVal(STBorder.NONE);
            //also inner borders
            borders.addNewInsideH().setVal(STBorder.NONE);
            borders.addNewInsideV().setVal(STBorder.NONE);
        }

        tabela.setCellMargins(this.cellMarginTop, this.cellMarginLeft, this.cellMarginBottom, this.cellMarginRight);


    }

    public int getCellMarginTop() {
        return cellMarginTop;
    }

    public void setCellMarginTop(int cellMarginTop) {
        this.cellMarginTop = cellMarginTop;
    }

    public int getCellMarginRight() {
        return cellMarginRight;
    }

    public void setCellMarginRight(int cellMarginRight) {
        this.cellMarginRight = cellMarginRight;
    }

    public int getCellMarginBottom() {
        return cellMarginBottom;
    }

    public void setCellMarginBottom(int cellMarginBottom) {
        this.cellMarginBottom = cellMarginBottom;
    }

    public int getCellMarginLeft() {
        return cellMarginLeft;
    }

    public void setCellMarginLeft(int cellMarginLeft) {
        this.cellMarginLeft = cellMarginLeft;
    }

    public boolean isBorder() {
        return border;
    }

    public boolean isFirstLineHeader() {
        return firstLineHeader;
    }

    public void setFirstLineHeader(boolean firstLineHeader) {
        this.firstLineHeader = firstLineHeader;
    }
}
