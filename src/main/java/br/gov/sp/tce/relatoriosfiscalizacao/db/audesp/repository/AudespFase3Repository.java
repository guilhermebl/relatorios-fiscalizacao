package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespSaude;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespFase3Repository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<AudespFase3QuadroPessoal> getQuadroDePessoal(Integer entidadeId, Integer exercicio) {
        Query query = entityManager.createNativeQuery("select  :exercicio-1  as exercicio, " +
                " sum(qp.quantidade_total_vagas) as \"total\", " +
                " sum(qp.quantidade_vagas_providas) as \"providas\", " +
                " sum(qp.quantidade_vagas_nao_providas) as \"nao_providas\", car.tipo_exercicio_atividade_id as \"tipo_vaga\" " +
                "from audesp3.quadro_pessoal qp " +
                "natural join audesp.documento_master dm " +
                "natural join audesp3.cargo car " +
                "where dm.entidade_id in ( :entidadeId ) " +
                "and dm.ano_exercicio in ( :exercicio -1 ) " +
                "and dm.mes_referencia in (12) " +
                "and dm.tp_documento_id in (130) " +
                "and  " +
                "(car.tipo_exercicio_atividade_id in (1) " +
                "or car.tipo_exercicio_atividade_id in (2)  " +
                "or car.tipo_exercicio_atividade_id in (5)  " +
                ") " +
                "group by car.tipo_exercicio_atividade_id " +
                "union  " +
                "select :exercicio as exercicio, sum(qp.quantidade_total_vagas) as \"total\", sum(qp.quantidade_vagas_providas) as \"providas\",  " +
                "  sum(qp.quantidade_vagas_nao_providas) as \"nao_providas\", car.tipo_exercicio_atividade_id as \"tipo_vaga\" " +
                "from audesp3.quadro_pessoal qp " +
                "natural join audesp.documento_master dm " +
                "natural join audesp3.cargo car " +
                "where dm.entidade_id in ( :entidadeId ) " +
                "and dm.ano_exercicio in ( :exercicio ) " +
                "and dm.mes_referencia in (12) " +
                "and dm.tp_documento_id in (130) " +
                "and  " +
                "(car.tipo_exercicio_atividade_id in (1)  " +
                "or car.tipo_exercicio_atividade_id in (2)  " +
                "or car.tipo_exercicio_atividade_id in (5)  " +
                ") " +
                "group by car.tipo_exercicio_atividade_id ", AudespFase3QuadroPessoal.class);
        query.setParameter("entidadeId", entidadeId);
        query.setParameter("exercicio", exercicio);
        List<AudespFase3QuadroPessoal> lista = query.getResultList();
        for (int i = lista.size(); i < 6; i++)
            lista.add(new AudespFase3QuadroPessoal());
        return lista;

    }


    public List<AudespFase3QuadroPessoal> getQuadroDePessoalTotal(Integer entidadeId, Integer exercicio) {
        Query query = entityManager.createNativeQuery("select :exercicio-1 as exercicio, " +
                " sum(qp.quantidade_total_vagas) as \"total\", sum(qp.quantidade_vagas_providas) as \"providas\", " +
                "  sum(qp.quantidade_vagas_nao_providas) as \"nao_providas\", 9999 as tipo_vaga  " +
                "from audesp3.quadro_pessoal qp " +
                "natural join audesp.documento_master dm " +
                "natural join audesp3.cargo car " +
                "where dm.entidade_id in ( :entidadeId ) " +
                "and dm.ano_exercicio in ( :exercicio -1 ) " +
                "and dm.mes_referencia in (12) " +
                "and dm.tp_documento_id in (130) " +
                "and  " +
                "(car.tipo_exercicio_atividade_id in (1) " +
                "or car.tipo_exercicio_atividade_id in (2)  " +
                "or car.tipo_exercicio_atividade_id in (5)  " +
                ") " +
                "union  " +
                "select :exercicio as exercicio, sum(qp.quantidade_total_vagas) as \"total\", sum(qp.quantidade_vagas_providas) as \"providas\",  " +
                "  sum(qp.quantidade_vagas_nao_providas) as \"nao_providas\", 9999 as tipo_vaga  " +
                "from audesp3.quadro_pessoal qp " +
                "natural join audesp.documento_master dm " +
                "natural join audesp3.cargo car " +
                "where dm.entidade_id in ( :entidadeId ) " +
                "and dm.ano_exercicio in ( :exercicio ) " +
                "and dm.mes_referencia in (12) " +
                "and dm.tp_documento_id in (130) " +
                "and  " +
                "(car.tipo_exercicio_atividade_id in (1)  " +
                "or car.tipo_exercicio_atividade_id in (2)  " +
                "or car.tipo_exercicio_atividade_id in (5)  " +
                ") " , AudespFase3QuadroPessoal.class);
        query.setParameter("entidadeId", entidadeId);
        query.setParameter("exercicio", exercicio);
        List<AudespFase3QuadroPessoal> lista = query.getResultList();
        for (int i = lista.size(); i < 2; i++)
            lista.add(new AudespFase3QuadroPessoal());
        return lista;

    }


    public List<AudespFase3QuadroPessoal> getQuadroDePessoalTemporarios(Integer entidadeId, Integer exercicio) {
        Query query = entityManager.createNativeQuery("select count(lot.lotacao_agente_publico_id) as contratados " +
                "from audesp3.lotacao_agente_publico lot " +
                "join audesp3.historico_lotacao_agente_publico hist on hist.lotacao_agente_publico_id = lot.lotacao_agente_publico_id " +
                "join audesp3.funcao fun on fun.funcao_id = lot.funcao_id " +
                "where 1=1 " +
                "and lot.entidade_id in ( :entidadeId  ) " +
                "and lot.funcao_id is not null " +
                "and hist.tipo_situacao_agente_id not in (2,4,5,6,7,8,9,12,13) " +
                "and hist.data_historico = ( " +
                " select max(hist2.data_historico) " +
                " from audesp3.historico_lotacao_agente_publico hist2 " +
                " where hist.lotacao_agente_publico_id = hist2.lotacao_agente_publico_id " +
                " and hist2.data_historico <= '2017-12-31'  " +
                ") " +
                "union " +
                "select count(lot.lotacao_agente_publico_id) as \"Nº Contratados\" " +
                "from audesp3.lotacao_agente_publico lot " +
                "join audesp3.historico_lotacao_agente_publico hist on hist.lotacao_agente_publico_id = lot.lotacao_agente_publico_id " +
                "join audesp3.funcao fun on fun.funcao_id = lot.funcao_id " +
                "where 1=1 " +
                "and lot.entidade_id in ( :entidadeId ) " +
                "and lot.funcao_id is not null " +
                "and hist.tipo_situacao_agente_id not in (2,4,5,6,7,8,9,12,13) " +
                "and hist.data_historico = ( " +
                " select max(hist2.data_historico) " +
                " from audesp3.historico_lotacao_agente_publico hist2 " +
                " where hist.lotacao_agente_publico_id = hist2.lotacao_agente_publico_id " +
                " and hist2.data_historico <= '2018-12-31'  " +
                ")" , AudespFase3QuadroPessoal.class);
        query.setParameter("entidadeId", entidadeId);
        query.setParameter("exercicio", exercicio);
        List<AudespFase3QuadroPessoal> lista = query.getResultList();
        for (int i = lista.size(); i < 2; i++)
            lista.add(new AudespFase3QuadroPessoal());
        return lista;

    }
}
