package br.gov.sp.tce.relatoriosfiscalizacao.controller;

public class ParametroBusca {

    private Integer codigoIBGE;
    private Integer exercicio;
    private Integer quadrimestre;
    private Integer mesReferencia;
    private Integer tipoEntidadeId;

    public Integer getCodigoIBGE() {
        return codigoIBGE;
    }

    public void setCodigoIBGE(Integer codigoIBGE) {
        this.codigoIBGE = codigoIBGE;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getQuadrimestre() {
        return quadrimestre;
    }

    public void setQuadrimestre(Integer quadrimestre) {
        this.quadrimestre = quadrimestre;
    }

    public Integer getMesReferencia() {
        return mesReferencia;
    }

    public void setMesReferencia(Integer mesReferencia) {
        this.mesReferencia = mesReferencia;
    }

    public Integer getTipoEntidadeId() {
        return tipoEntidadeId;
    }

    public void setTipoEntidadeId(Integer tipoEntidadeId) {
        this.tipoEntidadeId = tipoEntidadeId;
    }
}
