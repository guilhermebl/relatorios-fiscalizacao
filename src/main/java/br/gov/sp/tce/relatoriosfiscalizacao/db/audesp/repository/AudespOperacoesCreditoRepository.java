package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespHipotese;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacao;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.ValorResult;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespOperacoesCreditoRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    //GF 29
    public List<AudespHipotese> getGF29AnaliseOperacoesDeCredito(Integer codigoIBGE, Integer exercicio, Integer mes) {
        Query query = entityManager.createNativeQuery("select  " +
                "hipotese.hipotese_id , " +
                "hipotese.identificador, " +
                "hipotese.ds_hipotese, " +
                "hipotese.situacao_item_id, " +
                "resultado_analise_regra.item_analise_id, " +
                "resultado_analise_regra.entidade_id, " +
                "situacao_item_id " +
                "ano_exercicio, " +
                "mes_referencia, " +
                "dt_processamento, " +
                "resultado_prejudicado," +
                "resultado_analise_regra.dt_processamento " +
                "from hipotese " +
                "inner join resultado_analise_regra on hipotese.hipotese_id = resultado_analise_regra.hipotese_id " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id " +
                "where 1 = 1 " +
                "and hipotese.item_analise_id in (37)  " +
                "and resultado_analise_regra.ano_exercicio = :exercicio " +
                "and municipio.cd_municipio_ibge = :codigoIBGE " +
                "and mes_referencia = 12 " +
                "order by resultado_analise_regra.dt_processamento desc ", AudespHipotese.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        //query.setParameter("tipoEntidade", tipoEntidade);
        List<AudespHipotese> lista = query.getResultList();
//        for (int i = lista.size(); i < 3; i++)
//            lista.add(new AudespResultado());
        return lista;
    }

    //GF 29
    public List<AudespHipotese> getGF30ARO(Integer codigoIBGE, Integer exercicio, Integer mes) {
        Query query = entityManager.createNativeQuery("select  " +
                "hipotese.hipotese_id , " +
                "hipotese.identificador, " +
                "hipotese.ds_hipotese, " +
                "hipotese.situacao_item_id, " +
                "resultado_analise_regra.item_analise_id, " +
                "resultado_analise_regra.entidade_id, " +
                "situacao_item_id " +
                "ano_exercicio, " +
                "mes_referencia, " +
                "dt_processamento, " +
                "resultado_prejudicado, " +
                "resultado_analise_regra.dt_processamento " +
                "from hipotese " +
                "inner join resultado_analise_regra on hipotese.hipotese_id = resultado_analise_regra.hipotese_id " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id " +
                "where 1 = 1 " +
                "and hipotese.item_analise_id in (38)  " +
                "and resultado_analise_regra.ano_exercicio = :exercicio " +
                "and municipio.cd_municipio_ibge = :codigoIBGE " +
                "and mes_referencia = 12 " +
                "order by resultado_analise_regra.dt_processamento desc ", AudespHipotese.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        //query.setParameter("tipoEntidade", tipoEntidade);
        List<AudespHipotese> lista = query.getResultList();
//        for (int i = lista.size(); i < 3; i++)
//            lista.add(new AudespResultado());
        return lista;
    }

    //GF 29
    public List<AudespHipotese> getGF31Garantias(Integer codigoIBGE, Integer exercicio, Integer mes) {
        Query query = entityManager.createNativeQuery("select  " +
                "hipotese.hipotese_id , " +
                "hipotese.identificador, " +
                "hipotese.ds_hipotese, " +
                "hipotese.situacao_item_id, " +
                "resultado_analise_regra.item_analise_id, " +
                "resultado_analise_regra.entidade_id, " +
                "situacao_item_id " +
                "ano_exercicio, " +
                "mes_referencia, " +
                "dt_processamento, " +
                "resultado_prejudicado, " +
                "resultado_analise_regra.dt_processamento " +
                "from hipotese " +
                "inner join resultado_analise_regra on hipotese.hipotese_id = resultado_analise_regra.hipotese_id " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id " +
                "where 1 = 1 " +
                "and hipotese.item_analise_id in (39)  " +
                "and resultado_analise_regra.ano_exercicio = :exercicio " +
                "and municipio.cd_municipio_ibge = :codigoIBGE " +
                "and mes_referencia = 12 " +
                "order by resultado_analise_regra.dt_processamento desc ", AudespHipotese.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        //query.setParameter("tipoEntidade", tipoEntidade);
        List<AudespHipotese> lista = query.getResultList();
//        for (int i = lista.size(); i < 3; i++)
//            lista.add(new AudespResultado());
        return lista;
    }

    //GF 38
    public List<AudespHipotese> getGF38(Integer codigoIBGE, Integer exercicio, Integer mes) {
        Query query = entityManager.createNativeQuery("select  " +
                "hipotese.hipotese_id , " +
                "hipotese.identificador, " +
                "hipotese.ds_hipotese, " +
                "hipotese.situacao_item_id, " +
                "resultado_analise_regra.item_analise_id, " +
                "resultado_analise_regra.entidade_id, " +
                "situacao_item_id " +
                "ano_exercicio, " +
                "mes_referencia, " +
                "dt_processamento, " +
                "resultado_prejudicado, " +
                "resultado_analise_regra.dt_processamento " +
                "from hipotese " +
                "inner join resultado_analise_regra on hipotese.hipotese_id = resultado_analise_regra.hipotese_id " +
                "inner join entidade on resultado_analise_regra.entidade_id = entidade.entidade_id " +
                "inner join municipio on entidade.municipio_id = municipio.municipio_id " +
                "where 1 = 1 " +
                "and hipotese.item_analise_id in (45)  " +
                "and resultado_analise_regra.ano_exercicio = :exercicio " +
                "and municipio.cd_municipio_ibge = :codigoIBGE " +
                "and mes_referencia = 12 " +
                "order by resultado_analise_regra.dt_processamento desc ", AudespHipotese.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        //query.setParameter("tipoEntidade", tipoEntidade);
        List<AudespHipotese> lista = query.getResultList();
//        for (int i = lista.size(); i < 3; i++)
//            lista.add(new AudespResultado());
        return lista;
    }


}