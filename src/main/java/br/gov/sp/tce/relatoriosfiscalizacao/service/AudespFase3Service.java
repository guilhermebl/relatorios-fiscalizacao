package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultadoExecucaoOrcamentaria;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespFase3Repository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespResultadoExecucaoOrcamentariaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespFase3Service {

    @Autowired
    private AudespFase3Repository audespFase3Repository;

    public  Map<String, AudespFase3QuadroPessoal> getQuadroDePessoal(Integer entidadeId, Integer exercicio) {

        List<AudespFase3QuadroPessoal> audespFase3QuadroPessoalList  = audespFase3Repository.getQuadroDePessoal(entidadeId, exercicio);

        List<AudespFase3QuadroPessoal> audespFase3QuadroPessoalTotalList  = audespFase3Repository.getQuadroDePessoalTotal(entidadeId, exercicio);

        Map<String, AudespFase3QuadroPessoal> mapa = new HashMap<>();

        for (AudespFase3QuadroPessoal resultado : audespFase3QuadroPessoalList) {
            mapa.put("" + resultado.getExercicio() + resultado.getTipoVaga(), resultado);
        }

        for (AudespFase3QuadroPessoal resultado : audespFase3QuadroPessoalTotalList) {
            mapa.put("" + resultado.getExercicio() + resultado.getTipoVaga(), resultado);
        }

        return mapa;

    }


}
