package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.ValorResult;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespGF38RegraDeOuroRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.DadosResolucao_43_21_Senado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.repository.TcespBiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class Resolucao_43_21_SenadoService {

    @Autowired
    private TcespBiRepository tcespBiRepository;

    @Autowired
    private AudespGF38RegraDeOuroRepository audespGF38RegraDeOuroRepository;


    public DadosResolucao_43_21_Senado getDadosResolucao_43_21_Senado(Integer codigoIbge, Integer exercicio, Integer tipoEntidade) {
        DadosResolucao_43_21_Senado dadosResolucao_43_21_Senado = tcespBiRepository.getDadosResolucao_43_21_Senado(codigoIbge, exercicio, tipoEntidade);

        return dadosResolucao_43_21_Senado;
    }

    public String getAudespRegraDeOuroSuperioInferior(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        ValorResult result  =
                audespGF38RegraDeOuroRepository.getAudespRegraDeOuroValorGF28(codigoIbge, exercicio, mesReferencia);
        if(result == null) return "";

        String superiorInferior =  result.getValor().compareTo(BigDecimal.ZERO) == 1 ? "Superior" : "Inferior";


        return superiorInferior;
    }



}