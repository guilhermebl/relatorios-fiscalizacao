package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.ValorResult;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AudespRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public AudespResponsavel getAudespResponsavel(Integer codigoIBGE) {
        Query query = entityManager.createNativeQuery("select audesp.pessoa.pessoa_id, audesp.pessoa.nome, audesp.pessoa.cpf, municipio.cd_municipio_ibge from audesp3.cargo  " +
                "inner join audesp.entidade on audesp3.cargo.entidade_id = audesp.entidade.entidade_id " +
                "inner join audesp.entidade_pessoa_relacionamento on audesp.entidade.entidade_id = audesp.entidade_pessoa_relacionamento.entidade_id  " +
                "inner join audesp.pessoa on audesp.entidade_pessoa_relacionamento.pessoa_id = audesp.pessoa.pessoa_id " +
                "inner join audesp.municipio on audesp.entidade.municipio_id = audesp.municipio.municipio_id  " +
                "where 1=1  " +
                "and audesp3.cargo.nome_cargo ilike 'PREFEITO%' " +
                "and dt_ini_mandato >= '2017-01-01'  " +
                "and responsavel_entidade is true " +
                "and audesp.municipio.cd_municipio_ibge = :codigoIBGE " +
                "group by audesp.pessoa.pessoa_id, audesp.pessoa.nome, audesp.pessoa.cpf, municipio.cd_municipio_ibge " +
                "limit 1 ", AudespResponsavel.class);
        query.setParameter("codigoIBGE", codigoIBGE);
//        query.setParameter("exercicio", exercicio);
//        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResponsavel> lista = query.getResultList();
        if (lista.size() == 0)
            return new AudespResponsavel();
        return lista.get(0);
    }

    public List<AudespResponsavel> getResponsavelPrefeitura(Integer codigoIBGE, Integer exercicio, Integer quadrimestre) {
        Integer mes = quadrimestre * 4;
        Integer dia = 31;
        if(mes == 4)
            dia = 30;

        Query query = entityManager.createNativeQuery("select audesp.pessoa.pessoa_id, " +
                "audesp.pessoa.nome, " +
                "case when audesp.entidade_pessoa_relacionamento.dt_ini_vigencia < date '"+exercicio+"-01-01' then date '"+exercicio+"-01-01' else audesp.entidade_pessoa_relacionamento.dt_ini_vigencia end as dt_ini_vigencia, "+
                "case when audesp.entidade_pessoa_relacionamento.dt_fim_vigencia > date '"+exercicio+"-"+mes+"-"+dia+"' then date '"+exercicio+"-"+mes+"-"+dia+"' else audesp.entidade_pessoa_relacionamento.dt_fim_vigencia end as dt_fim_vigencia, "+
                "audesp.pessoa.cpf, " +
                "audesp.municipio.cd_municipio_ibge " +
                "from audesp.entidade_pessoa_relacionamento " +
                "inner join audesp.pessoa on audesp.entidade_pessoa_relacionamento.pessoa_id = pessoa.pessoa_id " +
                "inner join audesp.entidade on audesp.entidade_pessoa_relacionamento.entidade_id = audesp.entidade.entidade_id " +
                "inner join audesp.municipio on audesp.entidade.municipio_id = audesp.municipio.municipio_id " +
                "where 1 = 1 " +
                "and audesp.entidade_pessoa_relacionamento.responsavel_entidade is true " +
                "and (audesp.entidade_pessoa_relacionamento.dt_ini_vigencia, " +
                "coalesce(audesp.entidade_pessoa_relacionamento.dt_fim_vigencia, date '"+exercicio+"-"+mes+"-"+dia+"')) " +
                "overlaps (date '"+exercicio+"-01-01', date '"+exercicio+"-"+mes+"-"+dia+"') "+
                "and audesp.entidade_pessoa_relacionamento.tp_vigencia_id = 1 " +
                "and audesp.entidade_pessoa_relacionamento.tp_entidade_id = 50 " +
                "and audesp.municipio.cd_municipio_ibge = :codigoIBGE " +
                "order by audesp.entidade_pessoa_relacionamento.dt_ini_vigencia ", AudespResponsavel.class);
        query.setParameter("codigoIBGE", codigoIBGE);
//        query.setParameter("exercicio", exercicio);
        List<AudespResponsavel> lista = query.getResultList();
        if (lista.size() == 0)
            lista.add(new AudespResponsavel());
        return lista;
    }



    public List<AudespResponsavel> getResponsavelSubstitutoPrefeitura(Integer codigoIBGE, Integer exercicio, Integer quadrimestre) {
        Integer mes = quadrimestre * 4;
        Integer dia = 31;
        if(mes == 4)
            dia = 30;
        Query query = entityManager.createNativeQuery("select audesp.pessoa.pessoa_id, " +
                "audesp.pessoa.nome, " +
                "case when audesp.entidade_pessoa_relacionamento.dt_ini_vigencia < date '"+exercicio+"-01-01' then date '"+exercicio+"-01-01' else audesp.entidade_pessoa_relacionamento.dt_ini_vigencia end as dt_ini_vigencia, "+
                "case when audesp.entidade_pessoa_relacionamento.dt_fim_vigencia > date '"+exercicio+"-"+mes+"-"+dia+"' then date '"+exercicio+"-"+mes+"-"+dia+"' else audesp.entidade_pessoa_relacionamento.dt_fim_vigencia end as dt_fim_vigencia, "+
                "audesp.pessoa.cpf, " +
                "audesp.municipio.cd_municipio_ibge " +
                "from audesp.entidade_pessoa_relacionamento " +
                "inner join audesp.pessoa on audesp.entidade_pessoa_relacionamento.pessoa_id = pessoa.pessoa_id " +
                "inner join audesp.entidade on audesp.entidade_pessoa_relacionamento.entidade_id = audesp.entidade.entidade_id " +
                "inner join audesp.municipio on audesp.entidade.municipio_id = audesp.municipio.municipio_id " +
                "where 1 = 1 " +
                "and audesp.entidade_pessoa_relacionamento.responsavel_entidade is true " +
                "and (audesp.entidade_pessoa_relacionamento.dt_ini_vigencia, " +
                "coalesce(audesp.entidade_pessoa_relacionamento.dt_fim_vigencia, date '"+exercicio+"-"+mes+"-"+dia+"')) " +
                "overlaps (date '"+exercicio+"-01-01', date '"+exercicio+"-"+mes+"-"+dia+"') "+
                "and audesp.entidade_pessoa_relacionamento.tp_vigencia_id = 2 " +
                "and audesp.entidade_pessoa_relacionamento.tp_entidade_id = 50 " +
                "and audesp.municipio.cd_municipio_ibge = :codigoIBGE " +
                "order by audesp.entidade_pessoa_relacionamento.dt_ini_vigencia ", AudespResponsavel.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        List<AudespResponsavel> lista = query.getResultList();

        if (lista.size() == 0 || lista.get(0) == null) {
            lista = new ArrayList<>();
            lista.add(new AudespResponsavel());
        }

        return lista;
    }


    public List<AudespResponsavel> getResponsavelSubstitutoPrefeituraQuery(Integer codigoIBGE, Integer exercicio) {
        Query query = entityManager.createNativeQuery("select audesp.pessoa.pessoa_id, " +
                "audesp.pessoa.nome, " +
                "audesp.entidade_pessoa_relacionamento.dt_ini_vigencia, " +
                "audesp.entidade_pessoa_relacionamento.dt_fim_vigencia, " +
                "audesp.pessoa.cpf, " +
                "audesp.municipio.cd_municipio_ibge " +
                "from audesp.entidade_pessoa_relacionamento " +
                "inner join audesp.pessoa on audesp.entidade_pessoa_relacionamento.pessoa_id = pessoa.pessoa_id " +
                "inner join audesp.entidade on audesp.entidade_pessoa_relacionamento.entidade_id = audesp.entidade.entidade_id " +
                "inner join audesp.municipio on audesp.entidade.municipio_id = audesp.municipio.municipio_id " +
                "where 1 = 1 " +
                "and audesp.entidade_pessoa_relacionamento.responsavel_entidade is true " +
                "and " +
                "   (audesp.entidade_pessoa_relacionamento.dt_ini_vigencia >= ':exercicio-01-01' " +
                "   and audesp.entidade_pessoa_relacionamento.dt_ini_vigencia <= ':exercicio-12-31' " +
                "       or " +
                "   audesp.entidade_pessoa_relacionamento.dt_fim_vigencia >= ':exercicio-01-01' " +
                "   and audesp.entidade_pessoa_relacionamento.dt_fim_vigencia <= ':exercicio-12-31' " +

                "       or " +
                "   audesp.entidade_pessoa_relacionamento.dt_ini_vigencia >= ':exercicio-01-01' " +
                "   and  audesp.entidade_pessoa_relacionamento.dt_fim_vigencia is null " +
                "        or " +
                "   audesp.entidade_pessoa_relacionamento.dt_ini_vigencia = ':exercicio-01-01' " +
                "   and  audesp.entidade_pessoa_relacionamento.dt_fim_vigencia = '2020-12-31' " +
                ")" +
                "and audesp.entidade_pessoa_relacionamento.tp_vigencia_id = 2 " +
                "and audesp.entidade_pessoa_relacionamento.tp_entidade_id = 50 " +
                "and audesp.municipio.cd_municipio_ibge = :codigoIBGE " +
                "order by audesp.entidade_pessoa_relacionamento.dt_ini_vigencia ", AudespResponsavel.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<AudespResponsavel> lista = query.getResultList();

        if (lista.size() == 0 || lista.get(0) == null) {
            lista = new ArrayList<>();
            lista.add(new AudespResponsavel());
        }

        return lista;
    }

    public ValorResult getParametroPrefeitura(Integer codigoIBGE, Integer exercicio, Integer mesReferencia, String parametro) {
        Query query = entityManager.createNativeQuery("select " +
                "vw_valor_parametro.municipio_id as municipio_id, " +
                "vw_valor_parametro.ano_exercicio as ano, " +
                "sum(coalesce(cast(valor AS numeric), 0)) as valor " +
                "from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge =   :codigoIBGE   )  " +
                "and parametro_analise.nm_parametro_analise in (:parametro) " +
                "and mes_referencia in (  :mesReferencia  ) and ano_exercicio in (  :exercicio  )  " +
                "group by " +
                "vw_valor_parametro.municipio_id, " +
                "vw_valor_parametro.ano_exercicio ", ValorResult.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        query.setParameter("parametro", parametro);
        ValorResult valor = (ValorResult) query.getResultList().stream().findFirst().orElse(null);
        return valor;

    }

}
