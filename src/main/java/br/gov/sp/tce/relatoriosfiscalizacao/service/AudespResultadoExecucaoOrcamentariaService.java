package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.ParametroBusca;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultadoExecucaoOrcamentaria;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespResultadoExecucaoOrcamentariaRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model.AudespBiValor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespResultadoExecucaoOrcamentariaService {

    @Autowired
    private AudespResultadoExecucaoOrcamentariaRepository audespResultadoExecucaoOrcamentariaRepository;

    @Autowired
    private AudespBiService audespBiService;

    public Map<String, String> getAudespResultadoExecucaoOrcamentariaFormatado(ParametroBusca parametroBusca) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentaria(parametroBusca);

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vResExecOrcGeralPMAV")) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        resultado.getValor().multiply(new BigDecimal(100))
                                .setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                .toString()
                                .replace(".", ",")
                                .concat("%") );


            } else {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(), resultado.getValorString());
            }
        }

        mapResultadoExecucaoOrcamentaria.put("percentualResultadoExecucao", percentualResultadoExecucao(audespResultadoExecucaoOrcamentariaList));
        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, BigDecimal> getAudespResultadoExecucaoOrcamentariaQuadrimestral(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentariaQuadrimestral(codigoIbge, exercicio, mesReferencia);

        List<String> despesaEmpenhada = Arrays.asList(new String[]{"vDespCorExec", "vDespCapExec", "vDespIntraOrcExec"});

        BigDecimal valorDespesaEmpenhada = audespBiService.getValorDespesaEmpenhada(codigoIbge, exercicio, mesReferencia);

        Map<String, BigDecimal> mapResultadoExecucaoOrcamentaria = new HashMap<>();

        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(), resultado.getValor());
            }
        }

        BigDecimal despesaEmpenhadaTotal = new BigDecimal(0);

        mapResultadoExecucaoOrcamentaria.put("valorDespesaEmpenhada", valorDespesaEmpenhada);

        //Calcula Despesas Empenhadas - Item 5.1 RI Quadrimestral
        for(AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if(resultado.getValor() != null && despesaEmpenhada.contains(resultado.getNomeParametroAnalise())){
                despesaEmpenhadaTotal = despesaEmpenhadaTotal.add(resultado.getValor());
            }
        }
        mapResultadoExecucaoOrcamentaria.put("despesaEmpenhadaTotal", despesaEmpenhadaTotal);
        // Fim

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, String> getAudespResultadoExecucaoOrcamentariaQuadrimestralFormatado(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentariaQuadrimestral(codigoIbge, exercicio, mesReferencia);

        //BigDecimal valorDespesaEmpenhada = audespBiService.getValorDespesaEmpenhada(codigoIbge, exercicio, mesReferencia);

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();

        List<String> parametrosValores = Arrays.asList(new String[]{"vRecRealEnt","vDedRecRealEnt","vDespLiqEnt",
                "vRepDuodExec","vRepDuodCM","vDevDuod","vTransfFinAdmIndExec","vResExeOrcLiq", "vRecArrec",
                "vSubTotRecRealPM", "vDespCorExec", "vDespCapExec", "vDespIntraOrcExec"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vPercResExecOrc"});

        List<String> despesaEmpenhada = Arrays.asList(new String[]{"vDespCorExec", "vDespCapExec", "vDespIntraOrcExec"});

        List<String> soma = Arrays.asList(new String[]{"vSubTotRecRealPM", "vDevDuod"});
        List<String> sub = Arrays.asList(new String[]{"vRepDuodCM", "vTransfFinAdmIndExec"});

        BigDecimal despesaEmpenhadaTotal = new BigDecimal(0);

        BigDecimal resultadoExecucaoOrcamentaria = new BigDecimal(0);



        //Calcula Despesas Empenhadas - Item 5.1 RI Quadrimestral
        for(AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if(resultado.getValor() != null && despesaEmpenhada.contains(resultado.getNomeParametroAnalise())){
                despesaEmpenhadaTotal = despesaEmpenhadaTotal.add(resultado.getValor());
            }
        }
        mapResultadoExecucaoOrcamentaria.put("despesaEmpenhadaTotal",
                FormatadorDeDados.formatarMoeda(despesaEmpenhadaTotal, true, false));
        // Fim

        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false));
            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));
            }
        }

        for(AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if(resultado.getValor() != null &&  soma.contains(resultado.getNomeParametroAnalise())){
                resultadoExecucaoOrcamentaria = resultadoExecucaoOrcamentaria.add(resultado.getValor());
            }
            if(resultado.getValor() != null &&  sub.contains(resultado.getNomeParametroAnalise())){
                resultadoExecucaoOrcamentaria = resultadoExecucaoOrcamentaria.subtract(resultado.getValor());
            }
        }

        resultadoExecucaoOrcamentaria = resultadoExecucaoOrcamentaria.subtract(despesaEmpenhadaTotal);


        mapResultadoExecucaoOrcamentaria.put("resultadoExecucaoOrcamentaria",
                FormatadorDeDados.formatarMoeda(resultadoExecucaoOrcamentaria, true, false));


        BigDecimal percentualExecucaoOrcamentaria = new BigDecimal(0);

        for(AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if(resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vSubTotRecRealPM")
                    && resultado.getValor().compareTo(new BigDecimal(0)) != 0){
                percentualExecucaoOrcamentaria = resultadoExecucaoOrcamentaria
                        .divide(resultado.getValor(), 6, BigDecimal.ROUND_DOWN);
            }
        }

        mapResultadoExecucaoOrcamentaria.put("percentualExecucaoOrcamentaria",
                FormatadorDeDados.formatarPercentual(percentualExecucaoOrcamentaria, true, false));

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, BigDecimal> getAudespResultadoExecucaoOrcamentaria(ParametroBusca parametroBusca) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentaria(parametroBusca);

        Map<String, BigDecimal> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(), resultado.getValor());
        }

        return mapResultadoExecucaoOrcamentaria;

    }


    private String percentualResultadoExecucao(List<AudespResultadoExecucaoOrcamentaria> audespResultadoExecucaoOrcamentariaList) {

        AudespResultadoExecucaoOrcamentaria vRecArrec = null;
        AudespResultadoExecucaoOrcamentaria vResExecOrcGeralPM = null;

        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getNomeParametroAnalise() != null && resultado.getNomeParametroAnalise().equals("vRecArrec")) {
                vRecArrec = resultado;
            }
            if (resultado.getNomeParametroAnalise() != null && resultado.getValor() != null
                    && resultado.getNomeParametroAnalise().equals("vResExecOrcGeralPM")) {
                vResExecOrcGeralPM = resultado;
            }
        }

        String percentualResultadoExecucao = "";
        if (vRecArrec != null && vRecArrec.getValor() != null && vResExecOrcGeralPM.getValor() != null &&
                vResExecOrcGeralPM.getValor().compareTo(new BigDecimal(0)) != 0) {
            BigDecimal resultado = vResExecOrcGeralPM.getValor().divide(vRecArrec.getValor(), 6, BigDecimal.ROUND_DOWN);
            if (resultado != null) {
                percentualResultadoExecucao = FormatadorDeDados.formatarPercentual(resultado, true, false);
            }
        }

        return percentualResultadoExecucao;


    }

    public Map<String, String> getEquilibrioOrcamentarioReceitasFormatado(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentariaReceitasNaoAmparado(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosValores = Arrays.asList(new String[]{"vRecCorPrev", "vRecCorReal", "vRecCapPrev", "vRecCapReal",
                "vRecIntraOrcPrev", "vRecIntraOrcReal", "vDedRecPrev", "vDedRecReal", "vSubTotRecPrevPM", "vSubTotRecRealPM",
                "vTotRecPrevPM", "vTotRecRealPM", "vResExecOrcRecPM"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vRecCorAH", "vRecCorPMAV", "vRecCapOpCredAH", "vRecCapOpCredAV",
                "vRecIntraOrcAH", "vRecIntraOrcPMAV", "vDedRecAH", "vDedRecPMAV",
                "vSubTotRecPMAH", "vSubTotRecPMAV", "vTotRecPMAV", "vResExecOrcRecPMAV"});

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false));


            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));

            }
        }

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, BigDecimal> getEquilibrioOrcamentarioReceitas(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentariaReceitasNaoAmparado(codigoIbge, exercicio, mesReferencia);

        Map<String, BigDecimal> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(), resultado.getValor());
            }
        }

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, BigDecimal> getEquilibrioOrcamentarioDespesas(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentariaDespesasNaoAmparado(codigoIbge, exercicio, mesReferencia);

        Map<String, BigDecimal> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(), resultado.getValor());
            }
        }

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, String> getEquilibrioOrcamentarioDespesasFormatado(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentariaDespesasNaoAmparado(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosValores = Arrays.asList(new String[]{"vDespCorFix", "vDespCorExec", "vDespCapAmortFix",
                "vDespCapAmortExec", "vResCont", "vDespIntraOrcFix", "vDespIntraOrcExec", "vRepDuodFix",
                "vRepDuodExec", "vDevDuod", "vTransfFinAdmIndFix", "vTransfFinAdmIndExec", "vSubTotDespFixPM",
                "vSubTotDespExecPM", "vTotDespFixPM", "vEconOrc", "vResExecOrcGeralPM", "vTotDespExecPM"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vDespCorAH", "vDespCorPMAV", "vDespCapAmortAH", "vDespCapAmortAV",
                "vDespIntraOrcAH", "vDespIntraOrcPMAV", "vRepDuodAH", "vRepDuodAV", "vDevDuodAV",
                "vTransfFinAdmIndAH", "vTransfFinAdmIndAV", "vSubTotDespPMAH", "vSubTotDespPMAV",
                "vTotDespPMAV", "vEconOrcAV", "vResExecOrcGeralPMAV"});

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false));


            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));

            }
        }

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, String> getAudespResultadoExecucaoOrcamentariaUltimosTresExercicios(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoExecucaoOrcamentariaUltimosTresExercicios(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vResExecOrcGeralPMAV"});

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false));
                String superavitDeficit = resultado.getValor().toString().charAt(0) == '-' ? "Déficit de" : "Superávit de";
                mapResultadoExecucaoOrcamentaria.put("superavitDeficit" + resultado.getExercicio(), superavitDeficit);
            }
        }

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, String> getAudespResultadoFinanceiroEconomicoSaldoPatrimonial(ParametroBusca parametroBusca) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoFinanceiroEconomicoSaldoPatrimonial(parametroBusca);

        List<String> parametrosValores = Arrays.asList(new String[]{"vResultadoFinanceiro", "vResultadoEconomico",
                "vResultadoPatrimonial", "vSalPatrAtu", "vSalPatrAnt", "vRecArrec"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vResFinanceiroAH", "vResEconomicoAH", "vResPatrimonialAH"});

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));
            } else if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false));

            }
        }

        return mapResultadoExecucaoOrcamentaria;

    }

    public Map<String, String> getAudespResultadoInfluenciaOrcamentarioFinanceiro(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoInfluenciaOrcamentarioFinanceiro(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosMoeda = Arrays.asList(new String[]{"vResFinAnt", "vAjuVarAtiv", "vAjuVarPas",
                "vResFinAntAju", "vResMovOrc", "vResFinAtuApu"});

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosMoeda.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));
            }
        }
        return mapResultadoExecucaoOrcamentaria;
    }

    public Map<String, String> getAudespResultadoRepassesCamara(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getAudespResultadoRepassesCamara(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosMoeda = Arrays.asList(new String[]{"vCamUtil", "vCamDespInativos", "vCamSubTotal", "vArt29ATotal"});
        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vCamPercRepasse"});

        Map<String, String> mapResultadoExecucaoOrcamentaria = new HashMap<>();
        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosMoeda.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));
            } else if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapResultadoExecucaoOrcamentaria.put(resultado.getNomeParametroAnalise() + resultado.getExercicio(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false));
            }
        }
        return mapResultadoExecucaoOrcamentaria;
    }

    public Map<String, String> getIndiceDeLiquidezImediata(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespResultadoExecucaoOrcamentaria>
                audespResultadoExecucaoOrcamentariaList = audespResultadoExecucaoOrcamentariaRepository
                .getIndiceDeLiquidezImediata(codigoIbge, exercicio, mesReferencia);

        List<String> parametrosMoeda = Arrays.asList(new String[]{"vSalFinAtivoDispAjuAud", "vPassivoCircAju"});
        List<String> parametrosPercentuais = Arrays.asList(new String[]{""});

        BigDecimal vSalFinAtivoDispAjuAud = null;
        BigDecimal vPassivoCircAju = null;
        BigDecimal indiceLiquidez = null;

        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vSalFinAtivoDispAjuAud")) {
                vSalFinAtivoDispAjuAud = resultado.getValor();
            }

            if (resultado.getValor() != null && resultado.getNomeParametroAnalise().equals("vPassivoCircAju")) {
                vPassivoCircAju = resultado.getValor();
            }
        }

        if (vSalFinAtivoDispAjuAud != null && vPassivoCircAju != null && vPassivoCircAju.compareTo(new BigDecimal(0)) != 0) {
            indiceLiquidez = vSalFinAtivoDispAjuAud.divide(vPassivoCircAju, 4, RoundingMode.DOWN);
        }

        Map<String, String> mapa = new HashMap<>();
        if(vPassivoCircAju == null || vSalFinAtivoDispAjuAud == null || vPassivoCircAju.compareTo(BigDecimal.ZERO) == 0 )
            return mapa;

        DecimalFormat df = new DecimalFormat("#.####");

        mapa.put("indiceLiquidezImediata", df.format(indiceLiquidez).replace(".",",") );

        for (AudespResultadoExecucaoOrcamentaria resultado : audespResultadoExecucaoOrcamentariaList) {
            if (resultado.getValor() != null && parametrosMoeda.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarMoeda(resultado.getValor(), true, false));
            } else if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise(),
                        FormatadorDeDados.formatarPercentual(resultado.getValor(), true, false));
            }
        }


        return mapa;
    }

}