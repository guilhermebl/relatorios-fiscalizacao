package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AudespFase3QuadroPessoalKey implements Serializable {

    @Column(name = "exercicio")
    private Integer exercicio;

    @Column(name = "tipo_vaga")
    private Integer tipoVaga;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AudespFase3QuadroPessoalKey)) return false;
        AudespFase3QuadroPessoalKey that = (AudespFase3QuadroPessoalKey) o;
        return Objects.equals(exercicio, that.exercicio) &&
                Objects.equals(tipoVaga, that.tipoVaga);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exercicio, tipoVaga);
    }
}

