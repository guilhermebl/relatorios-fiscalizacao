package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TaxaInvestimentoKey implements Serializable {

    @Column(name = "codigo_ibge", nullable = false)
    private Integer codigoIbge;

    @Column(name = "exercicio", nullable = false)
    private int exercicio;

    public Integer getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(Integer codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public int getExercicio() {
        return exercicio;
    }

    public void setExercicio(int exercicio) {
        this.exercicio = exercicio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaxaInvestimentoKey)) return false;
        TaxaInvestimentoKey that = (TaxaInvestimentoKey) o;
        return getExercicio() == that.getExercicio() &&
                Objects.equals(getCodigoIbge(), that.getCodigoIbge());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigoIbge(), getExercicio());
    }
}

