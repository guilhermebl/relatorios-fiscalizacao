package br.gov.sp.tce.relatoriosfiscalizacao.controller.certidao;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.*;
import br.gov.sp.tce.relatoriosfiscalizacao.controller.docx.elementos.DocxElement;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacaoDocumento;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.DadosResolucao_43_21_Senado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.MunicipioIbge;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class Resolucao_43_2001_SenadoMipController {

    private XWPFDocument document;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private DocxElement docx;

    private Integer exercicio;

    private Integer codigoIBGE;

    FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");

    @Autowired
    private IegmService iegmService;

    private ResultadoIegm resultadoIegm2018;

    @Autowired
    private ParecerPrefeituraService parecerPrefeituraService;

    private List<ParecerPrefeitura> pareceresPrefeiturasList;

    @Autowired
    private TabelasService tabelasService;

    private TabelasProtocolo tabelasProtocolo;

    @Autowired
    private AudespEnsinoService audespEnsinoService;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    @Autowired
    private AudespPublicacaoService audespPublicacaoService;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    @Autowired
    private AudespSaudeService audespSaudeService;

    @Autowired
    private AudespLimiteLrfService audespLimiteLrfService;

    @Autowired
    private AudespService audespService;

    @Autowired
    private AudespAlertasService audespAlertasService;

    @Autowired
    private ApontamentosODSService apontamentosODSService;

    private Map<String, List<ApontamentoODS>> apontamentosODS;

    @Autowired
    private ResourceLoader resourceLoader;

    private Map<Integer, NotaIegm> notasIegm;

    private List<AudespResponsavel> responsavelPrefeitura;

    private Map<String,List<AudespResponsavel>> responsavelSubstitutoPrefeitura;

    private Map<String, String> audespResultadoExecucaoOrcamentaria;

    private Map<String, String> audespResultadoExecucaoOrcamentariaExcercicioAnt;

    @Autowired
    private AudespOperacoesDeCreditoService audespOperacoesCredito;

    @Autowired
    private AudespEntidadeService audespEntidadeService;

    private AudespEntidade audespEntidade;

    @Autowired
    private DemonstrativosRaeeService demonstrativosRaeeService;

    private  Map<String,String> anexo14AMap;

    private Map<String, String> audespEnsinoFundeb;

    private Map<String, String> audespSaude;

    private Map<String, String> audespDespesaPessoalMap;

    private Map<String, String> quadroGeralEnsinoMap;

    private Map<String, String> aplicacoesEmSaude;

    @Autowired
    private ApontamentosFOService apontamentosFOService;

    private Map<Integer, List<ApontamentoFO>> apontamentoFOMap;

    @Autowired
    private AudespDividaAtivaService audespDividaAtivaService;

    private Map<String, String> audespDividaAtivaMap;

    private Map<String,String> audespResultadoExecucaoOrcamentariaMap;

    @Autowired
    private AudespFase3Service audespFase3Service;

    private Map<String, AudespFase3QuadroPessoal> audespFase3QuadroDePessoalMap;

    @Autowired
    private AudespBiService audespBiService;

    @Autowired
    private TcespBiService tcespBiService;

    private Map<Integer, String> valorInvestimentoMunicipioMap;

    private Map<String, String> rclMunicipioDevedoresMap;

    private Map<String, String> limiteLRFMap;

    private Integer quadrimestre;

    private String heading1 = "Seção 1";
    private String heading2 = "Seção 2";
    private String heading3 = "Seção 3";
    private String heading4 = "Seção 4";


    private ResponseEntity<Resource> sendFile() throws IOException {
        // fim --------------------------------------
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-resolucao-43-2001-senado-v1.0.0.docx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }





    private void addSecao(TextoFormatado textoFormatado, String heading) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle(heading);
        paragraph.setSpacingAfter(6 * 20);
        if (heading.equals(this.heading1)) {
            CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill("d9d9d9");
        }

        textoFormatado.setParagraphText(paragraph);
    }

    private void createDocumentStyles() {
        XWPFStyles styles = document.createStyles();


        addCustomHeadingStyle(styles, heading1, 1);
        addCustomHeadingStyle(styles, heading2, 2);
        addCustomHeadingStyle(styles, heading3, 3);
        addCustomHeadingStyle(styles, heading4, 4);
    }


    private void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("Arial");

        CTRPr rpr = CTRPr.Factory.newInstance();
//        rpr.setRFonts(fonts);

        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }

    public byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }

    private TextoFormatado addTab() {
        Formatacao formatacaoTab = formatacaoFactory.getTab();
        return new TextoFormatado("", formatacaoTab).concat("", formatacaoTab);
    }

    private String getQuadrimestreTituloComAno(Integer quadrimestre) {
        String quadrimestreTitulo = "1º/2º Quadrimestre de 2018";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre de 2018";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre de 2018";
        return quadrimestreTitulo;
    }

    private String getQuadrimestreTitulo(Integer quadrimestre) {
        String quadrimestreTitulo = "3º Quadrimestre";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre";
        return quadrimestreTitulo;
    }

    private Integer getMesReferencia(Integer quadrimestre) {
        Integer mesReferencia = 12;
        if (quadrimestre == 1)
            mesReferencia = 4;
        if (quadrimestre == 2)
            mesReferencia = 8;
        return mesReferencia;
    }




    private String formataTcManualRedacao(String tc) {
        while(tc.length() < 13) {
            tc = "0" + tc;
        }
        tc = tc.replaceAll("/",".");
        return tc;
    }

    private XWPFTable addTabela(List<List<TextoFormatado>> dados, FormatacaoTabela formatacaoTabela) {

        int qdtLinhas = dados.size();
        int qtdColunas = 0;

        for (int i = 0; i < dados.size(); i++) {
            if (qtdColunas < dados.get(i).size())
                qtdColunas = dados.get(i).size();
        }

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        formatacaoTabela.formatarTabela(tabela);

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
//            row.setHeight((int) (twipsPerInch * 1 / 5)); //set height 1/10 inch.
//            row.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact"
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }

            for (int j = 0; j < dados.get(i).size(); j++) {
                XWPFTableCell cell = j == 0 ? row.getCell(j) : row.createCell();
                cell.setWidth(formatacaoTabela.getWidths().get(j));
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                if (i == 0 && formatacaoTabela.isFirstLineHeader())
                    cell.getCTTc().addNewTcPr().addNewShd().setFill("cccccc");

                XWPFRun run = cell.getParagraphs().get(0).createRun();

                XWPFParagraph paragrafo = cell.getParagraphs().get(0);
                paragrafo.setSpacingBetween(1);

                dados.get(i).get(j).setParagraphText(paragrafo);

            }

        }

        tabela.removeRow(0);

        return tabela;

    }




    private void addTabelaLRF52EmAtraso(Integer codigoIBGE, Integer exercicio) {
        List<AudespPublicacaoDocumento> docs = audespPublicacaoService.getLRF52EmAtraso(codigoIBGE, exercicio);
        addTabelaPublicacaoEmAtraso(docs);
    }

    private void addTabelagetRGFPublicacaoExecutivoEmAtraso(Integer codigoIBGE, Integer exercicio) {
        List<AudespPublicacaoDocumento> docs = audespPublicacaoService.getRGFPublicacaoExecutivoEmAtraso(codigoIBGE, exercicio);
        addTabelaPublicacaoEmAtraso(docs);
    }

    private void addTabelagetRGFPublicacaoLegislativoEmAtraso(Integer codigoIBGE, Integer exercicio) {
        List<AudespPublicacaoDocumento> docs = audespPublicacaoService.getRGFPublicacaoLegislativoEmAtraso(codigoIBGE, exercicio);
        addTabelaPublicacaoEmAtraso(docs);
    }



    private void addTabelaPublicacaoEmAtraso(List<AudespPublicacaoDocumento> docs) {

        if(docs.size() < 1) return;

        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"40%","10%", "10%", "10%", "10%", "10%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
//        List<TextoFormatado> linha1 = new ArrayList<>();
//        linha1.add(new TextoFormatado("OBRAS PARALISADAS", formatacaoFactory.getBoldCenter(9)));
//        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
//        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
//        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
//        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));
//        linha1.add(new TextoFormatado(" ", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Documento", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Exercício", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Mês Referência", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Data da publicação", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Data da Entrega", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Situação", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("Atrasado", formatacaoFactory.getBoldCenter(9)));
        dados.add(linha2);

        for(AudespPublicacaoDocumento doc : docs) {
            List<TextoFormatado> linha = new ArrayList<>();
            linha.add(new TextoFormatado("" + doc.getNomeTipoDocumento(), formatacaoFactory.getCenter(9)));
            linha.add(new TextoFormatado("" + doc.getExercicio(), formatacaoFactory.getCenter(9)));
            linha.add(new TextoFormatado("" + doc.getMesReferencia(), formatacaoFactory.getCenter(9)));
            linha.add(new TextoFormatado("" + FormatadorDeDados.formatarLocalDate(doc.getDataPublicacao()), formatacaoFactory.getCenter(9)));
            linha.add(new TextoFormatado("" + FormatadorDeDados.formatarLocalDate(doc.getEntregueEm()), formatacaoFactory.getCenter(9)));
            linha.add(new TextoFormatado("" + doc.getDescricaoEstadoPublicacao(), formatacaoFactory.getCenter(9)));
            linha.add(new TextoFormatado(doc.getAtrasado(), formatacaoFactory.getCenter(9)));
            dados.add(linha);
        }

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

//        List<MergePosition> mergePositions = new ArrayList<>();
//        MergePosition mergeH1 = new MergePosition(0, 0);
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 1));
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 2));
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 3));
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 4));
//        mergeH1.addToMergeHorizontal(new MergePosition(0, 5));
//
//        mergePositions.add(mergeH1);
//
//        mergeCells(tabela, mergePositions);

    }

    @Autowired
    private Resolucao_43_21_SenadoService resolucao_43_21_SenadoService;

    @GetMapping("/certidao/codigoibge/{codigoIBGE}/exercicio/{exercicio}")
    public ResponseEntity<Resource> download(@PathVariable Integer codigoIBGE, @PathVariable Integer exercicio)
            throws IOException, InvalidFormatException, Exception {
        document = new XWPFDocument();
        docx = new DocxElement(document);

        DadosResolucao_43_21_Senado dadosResolucao_43_21_senado = resolucao_43_21_SenadoService
                .getDadosResolucao_43_21_Senado(codigoIBGE, exercicio, 1);

        String regraDeOuroSuperiorInferior = resolucao_43_21_SenadoService.getAudespRegraDeOuroSuperioInferior(codigoIBGE, exercicio, 12);



        docx.addParagrafo( new TextoFormatado("MODELO PARA FORNECIMENTO DAS INFORMAÇÕES VISANDO A EXPEDIÇÃO DE " +
                        "CERTIDÕES VOLTADAS AO CUMPRIMENTO DA RESOLUÇÃO 43, DE 2001 DO SENADO FEDERAL COM AS ALTERAÇÕES" +
                        " INTRODUZIDAS PELA RESOLUÇÃO Nº 3, DE 2002.  ", formatacaoFactory.getBoldJustificado(12)));

        docx.addParagrafo(new TextoFormatado("", formatacaoFactory.getFormatacao(12)));
        docx.addParagrafo(new TextoFormatado("Expediente: ", new Formatacao(12).bold()).concat("TC-",
                new Formatacao(12)).concat(dadosResolucao_43_21_senado.getTc(), formatacaoFactory.getJustificado(12)));
        docx.addParagrafo(new TextoFormatado("Prefeitura: ",
                formatacaoFactory.getBold(12)).concat(dadosResolucao_43_21_senado.getNomeOrgao(), formatacaoFactory.getJustificado(12)));
        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("INFORMAÇÕES RELATIVAS AS CONTAS DO ÚLTIMO EXERCÍCIO ANALISADO",
                formatacaoFactory.getBoldJustificado(12)));
        docx.addParagrafo(new TextoFormatado("EXERCÍCIO: ",
                formatacaoFactory.getBold(12)).concat("" + dadosResolucao_43_21_senado.getExercicio(),
                formatacaoFactory.getJustificado(12)));
        docx.addParagrafo(new TextoFormatado("Parecer publicado no D.O.E. de: ",
                formatacaoFactory.getBold(12)).concat(" _ ", formatacaoFactory.getJustificado(12)));
        docx.addParagrafo(new TextoFormatado("Trânsito em julgado publicado no D.O.E. de: "
                + FormatadorDeDados.formatarLocalDate(dadosResolucao_43_21_senado.getDataPublicacaoDoTransitoEmJulgado()) + " ou Em Reexame  ",
                formatacaoFactory.getBold(12)).concat(" _ ", formatacaoFactory.getJustificado(12)));

        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 12 da LRF (Inciso III do Art. 167 da CF)",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getFormatacaoBoldUnderlineCaps(12)));
        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("O montante previsto para as receitas de operações de crédito " +
                "no exercício analisado foi ", formatacaoFactory.getJustificado(12))
                        .concat(regraDeOuroSuperiorInferior, formatacaoFactory.getBold(12))
                        .concat(" ao montante das despesas de capital constante da Lei Orçamentária.",
                                formatacaoFactory.getJustificado(12)));
        docx.addParagrafo(new TextoFormatado("*( superior, inferior)",
            formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("Se superior, (não) há autorização do Legislativo, por maioria " +
                "absoluta, nos termos excetuados pelo inciso III, artigo 167, da Constituição",
            formatacaoFactory.getJustificado(12)));

        docx.addBreak();
        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("Art. 23 ",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("EXECUTIVO",
                formatacaoFactory.getBoldUnderline(12)));

        docx.addBreak();
        docx.addBreak();


        //Gasto de pessoal
        Map<String, String> audespDespesaPessoalMap = audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbgeFechamentoFormatado(codigoIBGE, exercicio, 50);

        String despPessoalPercentual = audespDespesaPessoalMap.get("vPercDespPessoal"+ exercicio +"12");
        String despPessoalValor = audespDespesaPessoalMap.get("vDespPessoalLiq"+ exercicio +"12");
        String despPessoaLimitePermitido = audespDespesaPessoalMap.get("vLimPermitido"+ exercicio +"12");
        String superiorInferiorPessoalExercicio = audespDespesaPessoalService.getAudespDespesaPessoalDiferencaByCodigoIbge(codigoIBGE, exercicio, 12, 50);

        System.out.println(despPessoaLimitePermitido + " <- Executivo ");


        docx.addParagrafo(new TextoFormatado("O Executivo registrou " + despPessoalPercentual + "% com gasto de pessoal, equivalente a " +
                despPessoalValor +" , " + superiorInferiorPessoalExercicio + ", portanto, ao estabelecido no artigo 23 da LRF, no exercício " +
                "de "+ exercicio +".",
            new Formatacao(12).bold()));
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("Se registrar índice superior ao estabelecido, " +
                "indique o quadrimestre. ..............................",
            formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("Indique se a Prefeitura acha-se no período de ajuste para " +
                "regularização do excesso, ou especifique se foi extrapolado o período sem as providências de ajustes cabíveis.",
            formatacaoFactory.getJustificado(12)));

        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("LEGISLATIVO ",
                formatacaoFactory.getBoldUnderline(12)));

        audespDespesaPessoalMap = audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbgeFechamentoFormatado(codigoIBGE, exercicio, 51);

        despPessoalPercentual = audespDespesaPessoalMap.get("vPercDespPessoal"+ exercicio +"12");
        despPessoalValor = audespDespesaPessoalMap.get("vDespPessoalLiq"+ exercicio +"12");
        despPessoaLimitePermitido = audespDespesaPessoalMap.get("vLimPermitido"+ exercicio +"12");
        superiorInferiorPessoalExercicio = audespDespesaPessoalService.getAudespDespesaPessoalDiferencaByCodigoIbge(codigoIBGE, exercicio, 12, 51);

        System.out.println(despPessoaLimitePermitido + " <- Lesgislativo ");
        docx.addParagrafo(new TextoFormatado("O Legislativo registrou " + despPessoalPercentual + "% com gasto de pessoal, equivalente " +
                "a " + despPessoalValor + ", " + superiorInferiorPessoalExercicio +", portanto, ao estabelecido no artigo 23 da LRF, no exercício " +
                "de " + exercicio + ".",
                formatacaoFactory.getBold(12)));

        docx.addParagrafo(new TextoFormatado("Se registrar índice superior ao estabelecido, indique o " +
                "quadrimestre. ..............................",
            formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("Indique se a Câmara acha-se no período de regularização do excesso " +
                "ou especifique se  foi extrapolado sem as providências  de ajustes cabíveis.",
            formatacaoFactory.getJustificado(12)));

        //Art.33 LRF
        docx.addParagrafo(new TextoFormatado("Art. 33 da LRF",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getBoldUnderline(12)));


        String realizouOperacoesCreditoIrregularesString = audespOperacoesCredito.getRealizouOperacoesCreditoIrregulares(codigoIBGE, exercicio, 12);//, 51);

        docx.addParagrafo(new TextoFormatado("O Município (Estado) " + realizouOperacoesCreditoIrregularesString + " operações de " +
                "crédito irregulares, de acordo com os exames realizados.",
                formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("Em caso positivo, informar se efetuou o cancelamento, a amortização " +
                "ou a constituição de reserva para regularização.",
                formatacaoFactory.getJustificado(12)));
        docx.addBreak();
        docx.addBreak();


        //Art.37 LRF
        docx.addParagrafo(new TextoFormatado("Art. 37 da LRF",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo(new TextoFormatado("(*).............................. ocorrências de captação de " +
                "recursos ou assunção de compromissos com características similares às descritas no inciso I a III " +
                "do art. 5º da Resolução nº 43/01, do Senado Federal e no art. 37 da Lei de Responsabilidade Fiscal, " +
                "de acordo com os exames realizados.",
                formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("*( consta, não consta )",
                formatacaoFactory.getJustificado(12)));

        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("Se positivo, indique a data da realização, o valor, o credor e as" +
                " providências adotadas para regularização.  ",
                formatacaoFactory.getJustificado(12)));
        docx.addBreak();

        //Art.52 LRF

        String docsLRF52EmAtrasoMensagem = audespPublicacaoService.getLRF52EmAtrasoMensagem(codigoIBGE, exercicio);

        docx.addParagrafo(new TextoFormatado("Art. 52 da LRF",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getBoldUnderline(12)));


        docx.addParagrafo(new TextoFormatado(docsLRF52EmAtrasoMensagem + " o prazo legal para publicação do " +
                "Relatório Resumido de Execução Orçamentária - R.R.E.O.",
                formatacaoFactory.getJustificado(12)));

//        docx.addParagrafo(new TextoFormatado("*( cumpriu, não cumpriu ) ",
//                formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
        .concat("Caso algum relatório não tenha sido publicado no prazo" +
                " legal, indicar qual bimestre, especificando ainda, se efetuou a publicação a posteriori.",
                formatacaoFactory.getJustificado(12)));

        docx.addBreak();


        addTabelaLRF52EmAtraso(codigoIBGE, exercicio);

        docx.addBreak();
        docx.addBreak();

        //Art. 55 LRF - §2º - Executivo
        String docsRGFPublicacaoExecutivoEmAtrasoMensagem = audespPublicacaoService.getRGFPublicacaoExecutivoEmAtrasoMensagem(codigoIBGE, exercicio);

        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55 – da LRF ",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("EXECUTIVO",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado(docsRGFPublicacaoExecutivoEmAtrasoMensagem + " o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - R.G.F., ", new Formatacao(12).justify())
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12).justify()));

        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat(" Caso algum relatório não tenha sido publicado (ou divulgado) no prazo legal, indicar " +
                        "qual quadrimestre, especificando ainda se efetuou a publicação a posteriori.",
                        formatacaoFactory.getJustificado(12)));

        addTabelagetRGFPublicacaoExecutivoEmAtraso(codigoIBGE, exercicio);

        docx.addBreak();
        docx.addBreak();

        //Art. 55 LRF - §2º - Legislativo
        String docsRGFPublicacaoLegislativoEmAtrasoMensagem = audespPublicacaoService.getRGFPublicacaoLegislativoEmAtrasoMensagem(codigoIBGE, exercicio);

        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55 – da LRF ",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("LEGISLATIVO",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado(docsRGFPublicacaoLegislativoEmAtrasoMensagem + " o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - R.G.F., ", new Formatacao(12).justify())
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12).justify()));

        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat(" Caso algum relatório não tenha sido publicado (ou divulgado) no prazo legal, indicar qual " +
                        "quadrimestre, especificando ainda se efetuou a publicação (ou divulgação) a posteriori.",
                formatacaoFactory.getJustificado(12)));

        addTabelagetRGFPublicacaoLegislativoEmAtraso(codigoIBGE, exercicio);

        docx.addBreak();
        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("INFORMAÇÕES RELATIVAS ÀS CONTAS DOS EXERCÍCIOS AINDA NÃO ANALISADAS (Caso " +
                "haja mais de um exercício não examinado, repetir as informaçôes seguintes " +
                "para cada exercício) ", new Formatacao(12).bold().justify()));
        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("EXERCÍCIO:__________",
                new Formatacao(12).justify().bold()));

//        docx.addParagrafo(new TextoFormatado("Art. 198 da CF", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
//                formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo("O ente aplicou.........................% das receitas de impostos em gastos da saúde, " +
//                "(*).............................,portanto, o Art. 198 da CF. ");
//        docx.addParagrafo("*( cumprindo, não cumprindo )");
//
//        docx.addParagrafo(new TextoFormatado("Art. 212 da CF", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
//                formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo("O ente aplicou.........................% das receitas de impostos em gastos com " +
//                "educação, (*).............................,portanto, o Art. 212 da CF. ");
//        docx.addParagrafo("*( cumprindo, não cumprindo )");
//
//        docx.addBreak();
//
//        // CONTAS DOS EXERCÍCIOS AINDA NÃO ANALISADAS
//        docx.addParagrafo(new TextoFormatado("INFORMAÇÕES RELATIVAS ÀS CONTAS DOS EXERCÍCIOS AINDA NÃO ANALISADAS " +
//                "(Caso haja mais de um exercício não examinado, repetir as informaçôes seguintes para cada exercício)",
//                formatacaoFactory.getBold(12)));
//
//        docx.addParagrafo(new TextoFormatado("EXERCÍCIO: ______", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo(new TextoFormatado("Art. 11 da LRF", formatacaoFactory.getBoldUnderline(12)));
//        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo("(*)..................................o Art. 11 da LRF uma vez que o ente exerceu pleno" +
//                " cumprimento das competências tributárias.");
//
//        docx.addParagrafo("*( cumpriu, não cumpriu )");

        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 12 da LRF (Inciso III do Art. 167 da CF)",
                formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo("O montante previsto para as receitas de operações de crédito no exercício de " +
                "foi (*).............................. ao montante das despesas de capital constante da Lei " +
                "Orçamentária. ");

        docx.addParagrafo("*( superior, inferior)");

        docx.addParagrafo("Se superior, (não) há autorização do Legislativo, por maioria absoluta, nos termos " +
                "excetuados pelo inciso III, artigo 167, da Constituição");

        docx. addBreak();
        // Art. 23 da LRF
        docx.addParagrafo(new TextoFormatado("Art. 23 da LRF", formatacaoFactory.getBoldUnderline(12)));

        // Executivo
        docx.addParagrafo(new TextoFormatado("EXECUTIVO", formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo(new TextoFormatado("O Executivo registrou ______% com gasto de pessoal, equivalente a " +
                "R$_______________, (superior, inferior), portanto, ao estabelecido no artigo 23 da LRF, " +
                "no exercício de ____________.", formatacaoFactory.getBold(12)));

        docx.addParagrafo("Se registrar índice superior ao estabelecido, indique o quadrimestre." +
                " ..............................");

        docx.addParagrafo("Indique se a Prefeitura acha-se no período de ajuste para regularização do excesso, ou" +
                "  se foi extrapolado o período sem as providências  cabíveis.");

        docx.addBreak();
        // Legislativo
        docx.addParagrafo(new TextoFormatado("LEGISLATIVO", formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo(new TextoFormatado("O Legislativo registrou ______% com gasto de pessoal, equivalente a " +
                "R$_______________, (superior, inferior), portanto, ao estabelecido no artigo 23 da LRF, no exercício de" +
                " ____________.", formatacaoFactory.getBold(12)));

        docx.addParagrafo("Se registrar índice superior ao estabelecido, indique o " +
                "quadrimestre. ..............................");

        docx.addParagrafo(new TextoFormatado("Indique se a Câmara acha-se no período de regularização do excesso ou " +
                "especifique se foi, extrapolado sem as providências de ajustes cabíveis."));

        docx.addBreak();
        docx.addBreak();

        // Art. 52 da LRF
        docx.addParagrafo(new TextoFormatado("Art. 52 da LRF", formatacaoFactory.getBoldUnderline(12)));

        // Ente
        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo("(*).............................. o prazo legal para publicação do Relatório Resumido " +
                "de Execução Orçamentária - R.R.E.O., segundo a forma prescrita no art. 52.");

        docx.addParagrafo("*( cumpriu, não cumpriu ) ");

        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat(" Caso algum relatório não tenha sido publicado no prazo legal, indique qual bimestre, " +
                        "especificando ainda se efetuou a publicação a posteriori."));

        // § 2º do Art. 55  da LRF - EXECUTIVO
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55  da LRF ",
                formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo(new TextoFormatado("EXECUTIVO",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("(*).............................. o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - R.G.F. , ")
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12)));

        docx.addParagrafo("*( cumpriu, não cumpriu )");

        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat(" Caso algum relatório não tenha sido publicado (ou divulgado) no prazo legal, indique qual " +
                        "quadrimestre, especificando ainda se efetuou a publicação ou divulgação a posteriori."));

        // LEGISLATIVO
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55  da LRF ",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("LEGISLATIVO", formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("(*).............................. o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - R.G.F., ")
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12)));

        docx.addParagrafo("*( cumpriu, não cumpriu )");

        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat(" Caso algum relatório não tenha sido publicado (ou divulgado) no prazo legal, indique qual " +
                        "quadrimestre, especificando ainda se efetuou a publicação ou divulgação a posteriori.",
                        new Formatacao(12).justify() )
        );


//        // Art. 198 da CF
//        docx.addParagrafo(new TextoFormatado("Art. 198 da CF", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
//                formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo("O ente aplicou.........................% das receitas de impostos em gastos da saúde, " +
//                "(*).............................,portanto, o Art. 198 da CF. ");
//        docx.addParagrafo("*( cumprindo, não cumprindo )");
//        docx.addBreak();
//
//        // Art. 212 da CF
//        docx.addParagrafo(new TextoFormatado("Art. 212 da CF", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
//                formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo("O ente aplicou.........................% das receitas de impostos em gastos com " +
//                "educação, (*).............................,portanto, o Art. 212 da CF. ");
//        docx.addParagrafo("*( cumprindo, não cumprindo )");
//        docx.addBreak();


        // INFORMAÇÕES RELATIVAS ÀS CONTAS DO EXERCÍCIO EM CURSO
        docx.addParagrafo(new TextoFormatado("INFORMAÇÕES RELATIVAS ÀS CONTAS DO EXERCÍCIO EM CURSO",
                formatacaoFactory.getBold(12)));

        docx.addParagrafo(new TextoFormatado("EXERCÍCIO:________",
                formatacaoFactory.getBold(12)));

        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 12 da LRF (Inciso III do Art. 167 da CF)",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getFormatacaoBoldUnderlineCaps(12)));
        docx.addBreak();
        docx.addParagrafo(new TextoFormatado("O montante previsto para as receitas de operações de crédito " +
                "no exercício analisado foi (*)", formatacaoFactory.getJustificado(12))
                .concat(" .............................. ", formatacaoFactory.getBold(12))
                .concat(" ao montante das despesas de capital constante da Lei Orçamentária.",
                        formatacaoFactory.getJustificado(12)));
        docx.addParagrafo(new TextoFormatado("*( superior, inferior)",
                formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("Se superior, (não) há autorização do Legislativo, por maioria " +
                "absoluta, nos termos excetuados pelo inciso III, artigo 167, da Constituição",
                formatacaoFactory.getJustificado(12)));

//        // Art. 23 da LRF
//        docx.addParagrafo(new TextoFormatado("Art. 23 da LRF", formatacaoFactory.getBoldUnderline(12)));
//
//        // Executivo
//        docx.addParagrafo(new TextoFormatado("EXECUTIVO", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo(new TextoFormatado("O Executivo registrou ______% com gasto de pessoal, equivalente a " +
//                "R$_______________, (superior, inferior), portanto, ao estabelecido no artigo 23 da LRF, " +
//                "no exercício de ____________.", formatacaoFactory.getBold(12)));
//
//        docx.addParagrafo("Se registrar índice superior ao estabelecido, indique o quadrimestre." +
//                " ..............................");
//
//        docx.addParagrafo("Indique se a Prefeitura acha-se no período de ajuste para regularização do excesso, ou" +
//                "  se foi extrapolado o período sem as providências  cabíveis.");
//
//        // Legislativo
//        docx.addParagrafo(new TextoFormatado("LEGISLATIVO", formatacaoFactory.getBoldUnderline(12)));
//
//        docx.addParagrafo(new TextoFormatado("O Legislativo registrou ______% com gasto de pessoal, equivalente a " +
//                "R$_______________, (superior, inferior), portanto, ao estabelecido no artigo 23 da LRF, no exercício de" +
//                " ____________.", formatacaoFactory.getBold(12)));
//
//        docx.addParagrafo("Se registrar índice superior ao estabelecido, indique o " +
//                "quadrimestre. ..............................");
//
//        docx.addParagrafo(new TextoFormatado("Indique se a Câmara acha-se no período de regularização do excesso ou " +
//                "especifique se foi, extrapolado sem as providências de ajustes cabíveis.",
//                formatacaoFactory.getJustificado(12)));


        // § 2º do Art. 52  da LRF
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 52  da LRF",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("ENTE (ESTADUAL OU MUNICIPAL)",
                formatacaoFactory.getBoldUnderline(12)));

        docx.addParagrafo(new TextoFormatado("(*).............................. o prazo legal para publicação do " +
                "Relatório Resumido de Execução Orçamentária - R.R.E.O., segundo a forma prescrita no art. 52.",
                formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("*( cumpriu, não cumpriu ) ",
                formatacaoFactory.getJustificado(12)));

        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat("Caso algum relatório não tenha sido publicado no prazo legal, indique qual bimestre, " +
                                "especificando ainda se efetuou a publicação a posteriori.",
                        formatacaoFactory.getJustificado(12)));

        // § 2º do Art. 55  da LRF
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55  da LRF",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("EXECUTIVO",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("(*).............................. o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - R.G.F., ")
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12)));
        docx.addParagrafo(new TextoFormatado("*( cumpriu, não cumpriu ) ",
                formatacaoFactory.getJustificado(12)));
        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat(" Caso algum relatório não tenha sido publicado (ou divulgado) no prazo legal, indique qual " +
                        "quadrimestre, especificando ainda se efetuou a publicação ou divulgação a posteriori."));

        // § 2º do Art. 55  da LRF
        docx.addParagrafo(new TextoFormatado("§ 2º do Art. 55  da LRF",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("LEGISLATIVO",
                formatacaoFactory.getBoldUnderline(12)));
        docx.addParagrafo(new TextoFormatado("(*).............................. o prazo legal para publicação " +
                "(ou divulgação) do Relatório de Gestão Fiscal - R.G.F., ")
                .concat("inclusive por meio eletrônico.", formatacaoFactory.getBold(12)));
        docx.addParagrafo(new TextoFormatado("*( cumpriu, não cumpriu ) ",
                formatacaoFactory.getJustificado(12)));
        docx.addParagrafo(new TextoFormatado("Observação: ", formatacaoFactory.getBoldUnderline(12))
                .concat(" Caso algum relatório não tenha sido publicado no prazo legal, indicar qual quadrimestre, " +
                        "especificando ainda, se efetuou a publicação (ou divulgação) a posteriori."));

        docx.addBreak();

        docx.addParagrafo(new TextoFormatado("Data: " +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                formatacaoFactory.getRight(12)));
        docx.addParagrafo(new TextoFormatado("Despachos dos responsáveis ( Agente, Chefia e Diretor )",
                formatacaoFactory.getRight(12)));

        return sendFile();
    }


}