
package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespHipotese;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespLimiteLrf;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.ValorResult;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespOperacoesCreditoRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespOperacoesDeCreditoService {

    @Autowired
    private AudespOperacoesCreditoRepository audespOperacoesCreditoRepository;

    @Autowired
    private AudespRepository audespRepository;

    public Map<String, String> getOperacoesArt33(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespHipotese> audespHipoteseListGF29 = audespOperacoesCreditoRepository
                .getGF29AnaliseOperacoesDeCredito(codigoIbge, exercicio, mesReferencia);
        List<AudespHipotese> audespHipoteseListGF30 = audespOperacoesCreditoRepository
                .getGF30ARO(codigoIbge, exercicio, mesReferencia);

        Map<String, String> mapOperacoesCredito = new HashMap<>();

        Boolean gf29 = true;
        Boolean gf30 = true;

        if(audespHipoteseListGF29 != null && audespHipoteseListGF29.get(0) != null) {
            AudespHipotese h = audespHipoteseListGF29.get(0);
            if(h.getIdHipotese() == 125) {
                gf29 = true;
            } else {
                gf29 = false;
            }
        }

        if(audespHipoteseListGF30 != null && audespHipoteseListGF30.get(0) != null) {
            AudespHipotese h = audespHipoteseListGF30.get(0);
            if(h.getIdHipotese() == 136) {
                gf30 = true;
            } else {
                gf30 = false;
            }
        }


        if(gf29 && gf30)
            mapOperacoesCredito.put("art33", "não realizou");
        else
            mapOperacoesCredito.put("art33", "realizou");

        return mapOperacoesCredito;

    }

    public Map<String, String> getGF31Garantias(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespHipotese> audespHipoteseListGF31 = audespOperacoesCreditoRepository
                .getGF31Garantias(codigoIbge, exercicio, mesReferencia);

        Map<String, String> mapOperacoesCredito = new HashMap<>();

        Boolean gf31 = false;

        if(audespHipoteseListGF31 != null && audespHipoteseListGF31.get(0) != null) {
            AudespHipotese h = audespHipoteseListGF31.get(0);
            if(h.getIdHipotese() == 143) {
                gf31 = false;
            } else {
                gf31 = true;
            }
        }

        if(gf31)
            mapOperacoesCredito.put("art37", "Consta");
        else
            mapOperacoesCredito.put("art37", "Não consta");

        return mapOperacoesCredito;

    }


    public Map<String, String> getGF38(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        List<AudespHipotese> audespHipoteseListGF38 = audespOperacoesCreditoRepository
                .getGF38(codigoIbge, exercicio, mesReferencia);

        Map<String, String> mapOperacoesCredito = new HashMap<>();

        Boolean gf38 = false;

        if(audespHipoteseListGF38 != null && audespHipoteseListGF38.get(0) != null) {
            AudespHipotese h = audespHipoteseListGF38.get(0);
            if(h.getSituacaoItemId() == 1) {
                gf38 = true;
            } else {
                gf38 = false;
            }
        }

        if(gf38)
            mapOperacoesCredito.put("regraDeOuro", "foi");
        else
            mapOperacoesCredito.put("regraDeOuro", "não foi");

        return mapOperacoesCredito;

    }

    public String getRealizouOperacoesCreditoIrregulares(Integer codigoIbge, Integer exercicio, Integer mesReferencia) {
        ValorResult result = audespRepository
                .getParametroPrefeitura(codigoIbge, exercicio, mesReferencia, "vPercOpCred");

        String realizacao = result.getValor().compareTo(new BigDecimal(16.0)) > 1 ? "realizou" : "não realizou";

        System.out.println(result.getValor());
        return  realizacao;
    }








}