package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.Anexo14A;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespEntidadeRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.DemonstrativosRaeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DemonstrativosRaeeService {

    @Autowired
    private DemonstrativosRaeeRepository demonstrativosRaeeRepository;


    public Map<String, String> getAnexo14A(AudespEntidade entidade, Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        List<Anexo14A> listaAnexo14A = demonstrativosRaeeRepository.getAnexo14A(entidade, codigoIBGE, exercicio, mesReferencia);

        BigDecimal atualRppNplNoo = new BigDecimal(0);
        BigDecimal atualRestosAPagarNaoProcessados = new BigDecimal(0);
        BigDecimal atualDemaisObrigacoes = new BigDecimal(0);
        BigDecimal atualOutros = new BigDecimal(0);
        BigDecimal atualTotalAjustado = new BigDecimal(0);

        BigDecimal anteriorRppNplNoo = new BigDecimal(0);
        BigDecimal anteriorRestosAPagarNaoProcessados = new BigDecimal(0);
        BigDecimal anteriorDemaisObrigacoes = new BigDecimal(0);
        BigDecimal anteriorOutros = new BigDecimal(0);
        BigDecimal anteriorTotalAjustado = new BigDecimal(0);

        for( Anexo14A anexo14A : listaAnexo14A) {

            if(anexo14A.getGrupo1() != null && anexo14A.getGrupo1().equals("Restos a Pagar não Processados") && anexo14A.getDetalhe() == null) {
                atualDemaisObrigacoes = anexo14A.getValorAtual();
                anteriorDemaisObrigacoes = anexo14A.getValorAnterior();
            }

            if(anexo14A.getDetalhe() != null && anexo14A.getDetalhe().equals("Demais Obrigações")) {
                atualDemaisObrigacoes = anexo14A.getValorAtual();
                anteriorDemaisObrigacoes = anexo14A.getValorAnterior();
            }

        }

//        atualTotalAjustado.add("","");


        Map<String, String> map = new HashMap<>();

//        map.put("", );
        return map;
    }

    private String calculaAH(BigDecimal valorAtual, BigDecimal valorAnterior) {
        BigDecimal ah = null;
        if(valorAtual == null || valorAnterior == null || valorAtual.compareTo(new BigDecimal(0) ) == 0 ||
                valorAnterior.compareTo(new BigDecimal(0) ) == 0 ) {
            return "";
        } else {
            ah = (valorAtual.subtract(valorAnterior)).divide(valorAnterior, 4, RoundingMode.HALF_DOWN);
            return FormatadorDeDados.formatarPercentual(ah, true, true);
        }

    }
}
