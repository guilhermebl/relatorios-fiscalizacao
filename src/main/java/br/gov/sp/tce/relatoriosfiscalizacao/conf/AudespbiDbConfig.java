package br.gov.sp.tce.relatoriosfiscalizacao.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "audespbiEntityManagerFactory",
//        transactionManagerRef = "audespbiTransactionManager",
//        basePackages = { "br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.repository" }
//)
public class AudespbiDbConfig {

    @Bean(name = "audespbiDataSource")
    @ConfigurationProperties(prefix = "audespbi.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "audespbiEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    barEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("audespbiDataSource") DataSource dataSource
    ) {
        return
                builder
                        .dataSource(dataSource)
                        .packages("br.gov.sp.tce.relatoriosfiscalizacao.db.audespbi.model")
                        .persistenceUnit("audespbi")
                        .build();
    }

    @Bean(name = "audespbiTransactionManager")
    public PlatformTransactionManager barTransactionManager(
            @Qualifier("audespbiEntityManagerFactory") EntityManagerFactory
                    barEntityManagerFactory
    ) {
        return new JpaTransactionManager(barEntityManagerFactory);
    }
}