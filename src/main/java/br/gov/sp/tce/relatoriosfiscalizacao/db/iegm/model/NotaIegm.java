package br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "dbo.vw_iegm")
@IdClass(NotaIegmKey.class)
public class NotaIegm {

    @Id
    private Integer exercicio;

    @Id
    private Integer cd_municipio_ibge;

    private String ds_municipio = "";

//    @Column(name = "iegm_final")
    private BigDecimal iegm;

//    @Column(name = "faixa_iegm_final")
    private String faixa_iegm = "";

    private BigDecimal ieduc;

//    @Column(name = "faixa_ieduc_final")
    private String faixa_ieduc = "";

    private BigDecimal isaude;

//    @Column(name = "faixa_isaude_final")
    private String faixa_isaude = "";

    private BigDecimal iplanejamento;

//    @Column(name = "faixa_iplanejamento_final")
    private String faixa_iplanejamento = "";

    private BigDecimal ifiscal;

//    @Column(name = "faixa_ifiscal_final")
    private String faixa_ifiscal = "";

    private BigDecimal iamb;

//    @Column(name = "faixa_iamb_final")
    private String faixa_iamb = "";

    private BigDecimal icidade;

//    @Column(name = "faixa_icidade_final")
    private String faixa_icidade = "";

    private BigDecimal igov;

//    @Column(name = "faixa_igov_final")
    private String faixa_igov = "";


    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getCd_municipio_ibge() {
        return cd_municipio_ibge;
    }

    public void setCd_municipio_ibge(Integer cd_municipio_ibge) {
        this.cd_municipio_ibge = cd_municipio_ibge;
    }

    public String getDs_municipio() {
        return ds_municipio;
    }

    public void setDs_municipio(String ds_municipio) {
        this.ds_municipio = ds_municipio;
    }

    public BigDecimal getIegm() {
        return iegm;
    }

    public void setIegm(BigDecimal iegm) {
        this.iegm = iegm;
    }

    public String getFaixa_iegm() {
        return faixa_iegm;
    }

    public void setFaixa_iegm(String faixa_iegm) {
        this.faixa_iegm = faixa_iegm;
    }

    public BigDecimal getIeduc() {
        return ieduc;
    }

    public void setIeduc(BigDecimal ieduc) {
        this.ieduc = ieduc;
    }

    public String getFaixa_ieduc() {
        return faixa_ieduc;
    }

    public void setFaixa_ieduc(String faixa_ieduc) {
        this.faixa_ieduc = faixa_ieduc;
    }

    public BigDecimal getIsaude() {
        return isaude;
    }

    public void setIsaude(BigDecimal isaude) {
        this.isaude = isaude;
    }

    public String getFaixa_isaude() {
        return faixa_isaude;
    }

    public void setFaixa_isaude(String faixa_isaude) {
        this.faixa_isaude = faixa_isaude;
    }

    public BigDecimal getIplanejamento() {
        return iplanejamento;
    }

    public void setIplanejamento(BigDecimal iplanejamento) {
        this.iplanejamento = iplanejamento;
    }

    public String getFaixa_iplanejamento() {
        return faixa_iplanejamento;
    }

    public void setFaixa_iplanejamento(String faixa_iplanejamento) {
        this.faixa_iplanejamento = faixa_iplanejamento;
    }

    public BigDecimal getIfiscal() {
        return ifiscal;
    }

    public void setIfiscal(BigDecimal ifiscal) {
        this.ifiscal = ifiscal;
    }

    public String getFaixa_ifiscal() {
        return faixa_ifiscal;
    }

    public void setFaixa_ifiscal(String faixa_ifiscal) {
        this.faixa_ifiscal = faixa_ifiscal;
    }

    public BigDecimal getIamb() {
        return iamb;
    }

    public void setIamb(BigDecimal iamb) {
        this.iamb = iamb;
    }

    public String getFaixa_iamb() {
        return faixa_iamb;
    }

    public void setFaixa_iamb(String faixa_iamb) {
        this.faixa_iamb = faixa_iamb;
    }

    public BigDecimal getIcidade() {
        return icidade;
    }

    public void setIcidade(BigDecimal icidade) {
        this.icidade = icidade;
    }

    public String getFaixa_icidade() {
        return faixa_icidade;
    }

    public void setFaixa_icidade(String faixa_icidade) {
        this.faixa_icidade = faixa_icidade;
    }

    public BigDecimal getIgov() {
        return igov;
    }

    public void setIgov(BigDecimal igov) {
        this.igov = igov;
    }

    public String getFaixa_igov() {
        return faixa_igov;
    }

    public void setFaixa_igov(String faixa_igov) {
        this.faixa_igov = faixa_igov;
    }

    @Override
    public String toString() {
        return "NotaIegm{" +
                "exercicio=" + exercicio +
                ", cd_municipio_ibge=" + cd_municipio_ibge +
                ", ds_municipio='" + ds_municipio + '\'' +
                ", iegm=" + iegm +
                ", faixa_iegm='" + faixa_iegm + '\'' +
                ", ieduc=" + ieduc +
                ", faixa_ieduc='" + faixa_ieduc + '\'' +
                ", isaude=" + isaude +
                ", faixa_isaude='" + faixa_isaude + '\'' +
                ", iplanejamento=" + iplanejamento +
                ", faixa_iplanejamento='" + faixa_iplanejamento + '\'' +
                ", ifiscal=" + ifiscal +
                ", faixa_ifiscal='" + faixa_ifiscal + '\'' +
                ", iamb=" + iamb +
                ", faixa_iamb='" + faixa_iamb + '\'' +
                ", icidade=" + icidade +
                ", faixa_icidade='" + faixa_icidade + '\'' +
                ", igov=" + igov +
                '}';
    }
}
