package br.gov.sp.tce.relatoriosfiscalizacao.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "iegmEntityManagerFactory",
//        transactionManagerRef = "iegmTransactionManager",
//        basePackages = { "br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.repository" }
//)
public class IegmDbConfig {

    @Bean(name = "iegmDataSource")
    @ConfigurationProperties(prefix = "iegm.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "iegmEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    barEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("iegmDataSource") DataSource dataSource
    ) {
        return
                builder
                        .dataSource(dataSource)
                        .packages("br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model")
                        .persistenceUnit("iegm")
                        .build();
    }

    @Bean(name = "iegmTransactionManager")
    public PlatformTransactionManager barTransactionManager(
            @Qualifier("iegmEntityManagerFactory") EntityManagerFactory
                    barEntityManagerFactory
    ) {
        return new JpaTransactionManager(barEntityManagerFactory);
    }
}