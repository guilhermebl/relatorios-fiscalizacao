package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacao;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespModalidadeLicitacaoRepository;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespRGFRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AudespModalidadeLicitacaoService {

    @Autowired
    private AudespModalidadeLicitacaoRepository audespModalidadeLicitacaoRepository;

    public Map<String, String> getDespesaModalidade(Integer codigoIbge, Integer exercicio, Integer mes, Integer tipoEntidade) {
        List<AudespResultado> audespResultado = audespModalidadeLicitacaoRepository
                .getDespesaModalidade(codigoIbge, exercicio, mes, tipoEntidade);

        Map<String, String> mapa = new HashMap<>();

        List<String> parametrosValores = Arrays.asList(new String[]{"vDespPasLicConcor", "vDespPasLicTomPrec",
                "vDespPasLicConv", "vDespPasLicPreg",
                "vDespPasLicConcur", "vDespPasLicBEC", "vDespPasLicDisp", "vDespPasLicInexig",
                "vDespPasLicOutros", "vDespPasLicRDC", "vTotDespPasLic"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vDespPasLicConcorAV",
                "vDespPasLicTomPrecAV", "vDespPasLicConvAV", "vDespPasLicPregAV",
                "vDespPasLicConcurAV", "vDespPasLicBECAV", "vDespPasLicDispAV", "vDespPasLicInexigAV",
                "vDespPasLicOutrosAV", "vDespPasLicRDCAV"});

        for(AudespResultado resultado : audespResultado) {
            if (resultado.getValor() != null && parametrosPercentuais.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise() + resultado.getExercicio() + resultado.getMes(),
                     FormatadorDeDados.formatarPercentual(resultado.getValor(),true, false, 2));
            } else if (resultado.getValor() != null && parametrosValores.contains(resultado.getNomeParametroAnalise())) {
                mapa.put(resultado.getNomeParametroAnalise() + resultado.getExercicio() + resultado.getMes(),
                     FormatadorDeDados.formatarMoeda(resultado.getValor(),true, false));
            }
        }

        return mapa;
    }




}