package br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ApontamentoODSRepository {

    @PersistenceContext(unitName = "wsquestionario")
    private EntityManager entityManager;

    public List<ApontamentoODS> getApontamentosODS(Integer codigoIBGE, Integer exercicio) {
        Query query = entityManager.createNativeQuery("SELECT resp.id as respid, ods.id as odsid, exercicio, " +
                "e.municipio_id as municipio_ibge, mun.nome as municipio_nome, OBJ.descricao as orgao, " +
                "GP.descricao as iegm_indice, PER.codigo as iegm_pergunta_codigo, " +
                "PER.numero as iegm_pergunta_numero, TP.descricao as iegm_pergunta_tipo, PER.texto as iegm_pergunta_texto, " +
                "OPR.codigo as iegm_opcao_resposta_codigo, " +
                "OPR.descricao as iegm_opcao_resposta_descricao, RESP.data_resposta as iegm_data_resposta, " +
                "CASE " +
                "                WHEN PER.tipo_pergunta_id IN (1, 7)" +
                "                THEN OPR.descricao" +
                "                WHEN PER.tipo_pergunta_id = 6 " +
                "                THEN RESP.valor_texto " +
                "                WHEN PER.tipo_pergunta_id IN (2) " +
                "                THEN cast(RESP.valor_hora as text) " +
                "                WHEN PER.tipo_pergunta_id IN (8) " +
                "                THEN cast(RESP.valor_tempo as text) " +
                "                WHEN PER.tipo_pergunta_id IN (9) " +
                "                THEN to_char(RESP.valor_data, 'YYYY-MM-DD') " +
                "                WHEN PER.tipo_pergunta_id IN ( " +
                "                3 " +
                "                , 4 " +
                "                , 5 " +
                "                , 10 " +
                "                , 14 " +
                "                , 16) " +
                "THEN REPLACE((cast(RESP.valor_numerico as text)), '.', ',' ) " +
                "                WHEN PER.tipo_pergunta_id IN ( " +
                "                11 " +
                "                , 12 " +
                "                , 13 " +
                "                , 17) " +
                "                THEN cast(RESP.valor_inteiro as text) " +
                "                WHEN PER.tipo_pergunta_id IN (15) " +
                "                THEN RESP.valor_texto || ' |#| ' || RESP.comentario " +
                "                WHEN PER.tipo_pergunta_id IN (18) " +
                "                THEN RESP.valor_texto || ' |#| ' || cast(RESP.valor_inteiro as text) " +
                "END AS iegm_valor_resposta " +
                ", RESP.comentario as iegm_valor_comentario, ODS.itens_ods as ods_item, ODS.influencia as ods_influencia, " +
                "  replace(replace(ODS.apontamento, ' IEG-M 2020', ' IEG-M do exercício em exame'), ' IEGM 2020', ' IEG-M do exercício em exame') as ods_apontamento   " +
                "  FROM iegm.resposta_questionario_consolidado_fases RESP " +
                "  INNER JOIN questionario.opcao_resposta OPR ON OPR.id = RESP.opcao_resposta_id " +
                "  INNER JOIN questionario.pergunta PER ON OPR.pergunta_id = PER.id " +
                "  INNER JOIN questionario.objeto_fiscalizacao OBJ ON OBJ.id = RESP.objeto_fiscalizacao_id " +
                "  INNER JOIN questionario.entidade e ON OBJ.entidade_id = e.id " +
                "  INNER JOIN questionario.municipio mun ON e.municipio_id = mun.id " +
                "  LEFT OUTER JOIN iegm.apontamentos_ods ODS ON ODS.codigo_opc_resp = OPR.codigo " +
                "  INNER JOIN questionario.grupo_perguntas GP ON PER.grupo_perguntas_id = GP.id " +
                "  INNER JOIN questionario.tipo_pergunta TP ON PER.tipo_pergunta_id = TP.id  " +
                "  INNER JOIN iegm.exercicio EX ON EX.id = RESP.exercicio_id  " +
                "  WHERE EX.exercicio = :exercicio " +
                "  AND ODS.exercicio_id = :exercicio - 2016 " + //exercício 2021 exercicio_id = 5 para ods, porém se der erro olhar essa linha
                "  AND e.municipio_id = :codigoIBGE   " +
                "  AND ods.influencia = 'negativa' " +
                "  AND (  " +
                "  CASE " +
                "                WHEN ODS.numerico_operador IS NULL " +
                "                THEN 1 " +
                "                WHEN ODS.numerico_operador = '>' AND (coalesce(RESP.valor_inteiro, RESP.valor_numerico) > ODS.numerico) " +
                "                THEN 1 " +
                "                WHEN ODS.numerico_operador = '=' AND (coalesce(RESP.valor_inteiro, RESP.valor_numerico) = ODS.numerico) " +
                "                THEN 1 " +
                "                WHEN ODS.numerico_operador = '<' AND (coalesce(RESP.valor_inteiro, RESP.valor_numerico) < ODS.numerico) " +
                "                THEN 1 " +
                "                ELSE 0 " +
                "                END) = 1 " +
                "  ORDER BY OBJ.id, OPR.id ", ApontamentoODS.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        List<ApontamentoODS> lista = query.getResultList();

        return lista;
    }

    public List<ApontamentoFO> getApontamentosFO2018(Integer codigoIBGE, Integer exercicio) {
        Query query = entityManager.createNativeQuery("select  " +
                "op.id as operacao_id " +
                ",op.nome as operacao_nome " +
                ",op.data_inicio as operacao_data " +
                ",op.descricao as operacao_tema " +
                ",objf.id as objf_id " +
                ",mun.id as municipio_codibge " +
                ",mun.nome as municipio " +
                ",objf.descricao as objf_descricao " +
                ",objf.processo as objf_processo " +
                ",ap.id as apontamento_id " +
                ",              case (select count(*) from questionario.apontamento_substitucao aps2 where aps2.opcao_resposta_apontamento_id = ap.id) " +
                "                when 0 then ap.texto_apontamento " +
                "                else       (               " +
                "                               SELECT format( " +
                "                                               (select REPLACE(ap.texto_apontamento, aps.string, '%s') from questionario.apontamento_substitucao aps where aps.opcao_resposta_apontamento_id = ap.id) " +
                "                                               , VARIADIC ARRAY[( " +
                "                                                               select trim(resp.valor_texto) " +
                "                                                               from questionario.resposta resp " +
                "                                                               inner join questionario.item_pergunta ip on ip.id = resp.item_pergunta_id " +
                "                                                               inner join questionario.apontamento_substitucao aps2 on aps2.opcao_resposta_apontamento_id = ap.id " +
                "                                                               where ip.indice = (select ip2.indice from questionario.item_pergunta ip2 where ip2.id = objfap.item_pergunta_id) " +
                "                                                               and resp.objeto_fiscalizacao_id = objf.id " +
                "                                                               and resp.opcao_resposta_id = aps2.opcao_resposta_id " +
                "                                                               order by aps2.id " +
                "                                               )]) " +
                "                               ) " +
                "                end as apontamento " +
                "from questionario.objeto_fiscalizacao_apontamento objfap " +
                "inner join questionario.objeto_fiscalizacao objf on objf.id = objfap.objeto_fiscalizacao_id " +
                "inner join questionario.operacao op on op.id = objf.operacao_id " +
                "inner join questionario.entidade en on en.id = objf.entidade_id " +
                "inner join questionario.municipio mun on mun.id = en.municipio_id " +
                "inner join questionario.opcao_resposta_apontamento ap on ap.id = objfap.apontamento_id " +
                "where op.nome like '% Fiscalização Ordenada :exercicio' " +
                "and objfap.selecionado is true " +
                "and mun.id = :codigoIBGE " +
                "order by op.id, op.nome, mun.nome, objf.descricao, ap.id ", ApontamentoFO.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        //query.setParameter("exercicio", exercicio);
        List<ApontamentoFO> lista = query.getResultList();

        return lista;
    }


}