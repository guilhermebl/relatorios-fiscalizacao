package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "pareceres_contas_prefeituras")
public class ParecerPrefeitura {

    @Id
    @Column(name = "tk_pareceres_contas_prefeituras")
    private Integer id;
    private String tc;
    private String orgao;
    private Integer exercicio;

    @Column(name = "codigo_ibge")
    private Integer codigoIbge;

    private String municipio;

    @Column(name = "codigo_parecer")
    private Integer codigoParecer;
    private String parecer;
    @Column(name = "data_atualizacao")
    private LocalDateTime dataAtualizacao;
    @Column(name = "parecer_link")
    private String parecerLink;
    @Column(name = "data_transito_julgado")
    private LocalDate dataTransitoJulgado;
    @Column(name = "voto_link")
    private String votoLink;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }

    public String getOrgao() {
        return orgao;
    }

    public void setOrgao(String orgao) {
        this.orgao = orgao;
    }

    public Integer getExercicio() {
        return exercicio;
    }

    public void setExercicio(Integer exercicio) {
        this.exercicio = exercicio;
    }

    public Integer getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(Integer codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Integer getCodigoParecer() {
        return codigoParecer;
    }

    public void setCodigoParecer(Integer codigoParecer) {
        this.codigoParecer = codigoParecer;
    }


    public String getParecer() {
        return parecer;
    }

    public void setParecer(String parecer) {
        this.parecer = parecer;
    }

    public LocalDateTime getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public String getParecerLink() {
        return parecerLink;
    }

    public void setParecerLink(String parecerLink) {
        this.parecerLink = parecerLink;
    }

    public LocalDate getDataTransitoJulgado() {
        return dataTransitoJulgado;
    }

    public String getDataTransitoJulgadoString() {
        if(dataTransitoJulgado == null)
            return "";
        else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            return dataTransitoJulgado.format(formatter);
        }
    }

    public void setDataTransitoJulgado(LocalDate dataTransitoJulgado) {
        this.dataTransitoJulgado = dataTransitoJulgado;
    }

    public String getVotoLink() {
        return votoLink;
    }

    public void setVotoLink(String votoLink) {
        this.votoLink = votoLink;
    }

    @Override
    public String toString() {
        return "PareceresPrefeituras{" +
                "id=" + id +
                ", tc='" + tc + '\'' +
                ", orgao='" + orgao + '\'' +
                ", exercicio=" + exercicio +
                ", codigoIbge=" + codigoIbge +
                ", municipio='" + municipio + '\'' +
                ", codigoParecer=" + codigoParecer +
                ", parecer='" + parecer + '\'' +
                ", dataAtualizacao=" + dataAtualizacao +
                ", parecerLink='" + parecerLink + '\'' +
                ", dataTransitoJulgado=" + dataTransitoJulgado +
                ", votoLink='" + votoLink + '\'' +
                '}';
    }
}
