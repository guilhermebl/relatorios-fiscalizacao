package br.gov.sp.tce.relatoriosfiscalizacao.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "iegmEntityManagerFactory",
//        transactionManagerRef = "iegmTransactionManager",
//        basePackages = { "br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.repository" }
//)
public class TabelasDbConfig {

    @Bean(name = "tabelasDataSource")
    @ConfigurationProperties(prefix = "tabelas.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "tabelasEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    barEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("tabelasDataSource") DataSource dataSource
    ) {
        return
                builder
                        .dataSource(dataSource)
                        .packages("br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model")
                        .persistenceUnit("tabelas")
                        .build();
    }

    @Bean(name = "tabelasTransactionManager")
    public PlatformTransactionManager barTransactionManager(
            @Qualifier("tabelasEntityManagerFactory") EntityManagerFactory
                    barEntityManagerFactory
    ) {
        return new JpaTransactionManager(barEntityManagerFactory);
    }
}