package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEnsino;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespLimiteLrf;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResultado;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespSaude;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AudespEnsinoRepository {

    @PersistenceContext(unitName = "audesp")
    private EntityManager entityManager;

    public List<AudespEnsino> getAudespEnsinoByCodigoIbge(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query = entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id, " +
                " vw_valor_parametro.nm_parametro_analise, " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id, " +
                "ds_parametro_analise from vw_valor_parametro " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise " +
                "where  1 = 1 " +
                "and vw_valor_parametro.ano_exercicio = :exercicio " +
                "and mes_referencia = :mesReferencia " +
                "and parametro_analise_id in (196, 199, 200) " +
                "and vw_valor_parametro.entidade_id = ( " +
                "select entidade.entidade_id " +
                "from municipio " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id " +
                "where entidade.tp_entidade_id = 50 " +
                "and entidade.municipio_id <= 644 " +
                "and municipio.cd_municipio_ibge = :codigoIBGE) " +
                "order by parametro_analise_id", AudespEnsino.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespEnsino> lista = query.getResultList();
        for (int i = lista.size(); i < 3; i++)
            lista.add(new AudespEnsino());
        return lista;
    }

    public List<AudespEnsino> getAudespEnsinoFundeb(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query = entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id, " +
                "    vw_valor_parametro.nm_parametro_analise, " +
                "    ano_exercicio, mes_referencia, valor, parametro_analise_id, " +
                "    ds_parametro_analise from vw_valor_parametro " +
                "    inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise " +
                "    where 1 = 1 " +
                "    and vw_valor_parametro.entidade_id = ( " +
                "    select entidade.entidade_id " +
                "    from municipio " +
                "    inner join entidade on entidade.municipio_id = municipio.municipio_id " +
                "    where entidade.tp_entidade_id = 50 " +
                "    and entidade.municipio_id <= 644 " +
                "    and municipio.cd_municipio_ibge = :codigoIBGE) " +
                "    and parametro_analise.nm_parametro_analise " +
                "    in ('vRecFundeb', 'vPercEmpFundeb', 'vPercEmpFundebMagist', 'vDespPagaAplicFundeb', " +
                "    'vDespPagaAplicFundebMagist','vDespLiqAplicFundebMagist', 'vDespLiqAplicFundeb', " +
                "'vPercEmpFundebOutros', 'vDespLiqAplicFundebOutros', 'vDespPagaAplicFundebOutros', 'vPercEmpEnsino', " +
                "'vPercLiqEnsino', 'vPercPagoEnsino' ) " +
                "    and mes_referencia in ( :mesReferencia ) and ano_exercicio = :exercicio " +
                "    order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespEnsino.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespEnsino> lista = query.getResultList();
        for (int i = lista.size(); i < 10; i++)
            lista.add(new AudespEnsino());
        return lista;
    }


    public List<AudespEnsino> getQuadroGeralEnsino(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query = entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,  " +
                " vw_valor_parametro.nm_parametro_analise,   " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,   " +
                "ds_parametro_analise from vw_valor_parametro   " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise   " +
                "where 1 = 1 " +
                "and vw_valor_parametro.entidade_id = (   " +
                "select entidade.entidade_id   " +
                "from municipio   " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id  " +
                "where entidade.tp_entidade_id = 50   " +
                "and entidade.municipio_id <= 644   " +
                "and municipio.cd_municipio_ibge = :codigoIBGE)  " +
                "and parametro_analise.nm_parametro_analise " +
                "in ( 'vRecImpEducArrec','vTotRecImpEdu',  " +
                "'vFundebRetido','vFundebRecebido', 'vFundebRecApliFin', 'vFundebRecTot', " +
                "'vDespEmpFundebMagist', 'vFundebDespMagTot', 'vFundebDespPerc', " +
                "'vDespEmpFundebOutros', 'vDemDespEduTot', 'vDemDespEduPerc', " +
                "'vDespEduBas', 'vFundebRetido', 'vGanApliFinEdu', 'vFundebRetidoNaoAplicadoEmp', 'vApliArt212Edu', 'vApliArt212EduPerc', " +
                "'vFundebApliTri1', 'vRPNPagoEdu', 'vRecPropFundebAjuAud',  " +
                "'vApliFinEduBas', 'vApliFinEduBasPerc', " +
                "'vRecImpEducPrevAtu', 'vDotAtuEnsino',  'vPercPrevAtuEnsino') " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio = :exercicio " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespEnsino.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespEnsino> lista = query.getResultList();
        for (int i = lista.size(); i < 26; i++)
            lista.add(new AudespEnsino());
        return lista;

    }

    public List<AudespResultado> getPencentualAplicadoEnsino(Integer codigoIBGE, Integer exercicio, Integer mesReferencia) {
        Query query =  entityManager.createNativeQuery("select municipio_id, ds_municipio, vw_valor_parametro.entidade_id,   " +
                " vw_valor_parametro.nm_parametro_analise,    " +
                "ano_exercicio, mes_referencia, valor, parametro_analise_id,    " +
                "ds_parametro_analise from vw_valor_parametro    " +
                "inner join parametro_analise on vw_valor_parametro.nm_parametro_analise = parametro_analise.nm_parametro_analise    " +
                "where 1 = 1  " +
                "and vw_valor_parametro.entidade_id = (    " +
                "select entidade.entidade_id    " +
                "from municipio    " +
                "inner join entidade on entidade.municipio_id = municipio.municipio_id   " +
                "where entidade.tp_entidade_id = 50    " +
                "and entidade.municipio_id <= 644    " +
                "and municipio.cd_municipio_ibge = :codigoIBGE )   " +
                "and parametro_analise.nm_parametro_analise  " +
                "in ('vPercEmpEnsino')  " +
                "and mes_referencia in ( :mesReferencia ) and ano_exercicio = :exercicio  " +
                "order by parametro_analise_id, ano_exercicio, mes_referencia ", AudespResultado.class);
        query.setParameter("codigoIBGE", codigoIBGE);
        query.setParameter("exercicio", exercicio);
        query.setParameter("mesReferencia", mesReferencia);
        List<AudespResultado> lista = query.getResultList();
        for(int i = lista.size(); i < 1 ; i++)
            lista.add(new AudespResultado());
        return lista;
    }



}