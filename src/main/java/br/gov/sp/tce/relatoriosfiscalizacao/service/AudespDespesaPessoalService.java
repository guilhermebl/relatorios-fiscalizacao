package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespDespesaPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespDespesaPessoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespDespesaPessoalService {

    @Autowired
    private AudespDespesaPessoalRepository audespDespesaPessoalRepository;

//    public List<AudespDespesaPessoal> getAudespDespesaPessoalByCodigoIbge(Integer codigoIbge, Integer exercicio,
//    Integer mesReferencia) {
//        List<AudespDespesaPessoal> audespDespesaPessoalList = audespDespesaPessoalRepository
//        .getAudespDespesaPessoalByCodigoIbge(codigoIbge, exercicio, mesReferencia);
//        return audespDespesaPessoalList;
//
//    }

    public Map<String, BigDecimal> getAudespDespesaPessoalByCodigoIbge(Integer codigoIbge, Integer exercicio, Integer codigoOrgao) {
        List<AudespDespesaPessoal> audespDespesaPessoalList =
                audespDespesaPessoalRepository.getAudespDespesaPessoalByCodigoIbge(codigoIbge, exercicio);

        List<String> parametrosValores = Arrays.asList(new String[]{"vDespPessoalLiq", "vRCL"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vLimPermitido", "vPercDespPessoal"});

        Map<String, BigDecimal> mapa = new HashMap<>();

        for (AudespDespesaPessoal pessoal : audespDespesaPessoalList) {
            if (pessoal.getValor() != null && parametrosPercentuais.contains(pessoal.getNomeParametroAnalise())) {
                mapa.put(pessoal.getNomeParametroAnalise() + pessoal.getExercicio() + pessoal.getMes(),
                        pessoal.getValor());
            } else if (pessoal.getValor() != null && parametrosValores.contains(pessoal.getNomeParametroAnalise())) {
                mapa.put(pessoal.getNomeParametroAnalise() + pessoal.getExercicio() + pessoal.getMes(),
                        pessoal.getValor());
            }
        }


        return mapa;
    }





    public String getAudespDespesaPessoalDiferencaByCodigoIbge(Integer codigoIbge, Integer exercicio, Integer mes, Integer codigoOrgao) {
        Map<String, BigDecimal> mapa = getAudespDespesaPessoalByCodigoIbge(codigoIbge, exercicio, codigoOrgao);

        BigDecimal permitido = mapa.get("vLimPermitido" + exercicio + mes);
        BigDecimal actual = mapa.get("vPercDespPessoal" + exercicio + mes);

        String texto = null;
        if(actual != null && permitido != null) {
            if(actual.compareTo(permitido) > 0 ) { texto = "superior"; };
            if(actual.compareTo(permitido) <= 0 ) { texto = "inferior"; };
        }

        return texto;
    }

    public Map<String, String> getAudespDespesaPessoalByCodigoIbgeFechamentoFormatado(Integer codigoIbge,
                                                                                      Integer exercicio, Integer codigoOrgao) {
        List<AudespDespesaPessoal> audespDespesaPessoalList = null;
        if(codigoOrgao == 50) {
            audespDespesaPessoalList = audespDespesaPessoalRepository.getAudespDespesaPessoalByCodigoIbge(codigoIbge, exercicio);
        }
        if (codigoOrgao == 51) {
            audespDespesaPessoalList = audespDespesaPessoalRepository.getAudespDespesaPessoalLegislativoByCodigoIbge(codigoIbge, exercicio);
        }

        List<String> parametrosValores = Arrays.asList(new String[]{"vDespPessoalLiq", "vRCL"});

        List<String> parametrosPercentuais = Arrays.asList(new String[]{"vLimPermitido", "vPercDespPessoal"});

        Map<String, String> mapa = new HashMap<>();

        for (AudespDespesaPessoal pessoal : audespDespesaPessoalList) {
//            if (pessoal.getNomeParametroAnalise().equals(""))
                if (pessoal.getValor() != null && parametrosPercentuais.contains(pessoal.getNomeParametroAnalise())) {
                    mapa.put(pessoal.getNomeParametroAnalise() + pessoal.getExercicio() + pessoal.getMes(),
                            FormatadorDeDados.formatarPercentual(pessoal.getValor(), false, true, 2));
                } else if (pessoal.getValor() != null && parametrosValores.contains(pessoal.getNomeParametroAnalise())) {
                    mapa.put(pessoal.getNomeParametroAnalise() + pessoal.getExercicio() + pessoal.getMes(),
                            FormatadorDeDados.formatarMoeda(pessoal.getValor(), true, true));
                }
        }
        return mapa;
    }
}
