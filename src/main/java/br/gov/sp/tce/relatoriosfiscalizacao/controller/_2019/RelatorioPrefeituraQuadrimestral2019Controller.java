package br.gov.sp.tce.relatoriosfiscalizacao.controller._2019;

import br.gov.sp.tce.relatoriosfiscalizacao.controller.*;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespEntidade;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespFase3QuadroPessoal;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.MunicipioIbge;
import br.gov.sp.tce.relatoriosfiscalizacao.db.iegm.model.NotaIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasProtocolo;
import br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model.ParecerPrefeitura;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoFO;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ApontamentoODS;
import br.gov.sp.tce.relatoriosfiscalizacao.db.wsquestionario.model.ResultadoIegm;
import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class RelatorioPrefeituraQuadrimestral2019Controller {

    private XWPFDocument document;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Integer exercicio;

    private Integer codigoIBGE;

    FormatacaoFactory formatacaoFactory = new FormatacaoFactory("Arial");

    @Autowired
    private IegmService iegmService;

    private ResultadoIegm resultadoIegm2018;

    @Autowired
    private ParecerPrefeituraService parecerPrefeituraService;

    private List<ParecerPrefeitura> pareceresPrefeiturasList;

    @Autowired
    private TabelasService tabelasService;

    private TabelasProtocolo tabelasProtocolo;

    @Autowired
    private AudespEnsinoService audespEnsinoService;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    @Autowired
    private AudespSaudeService audespSaudeService;

    @Autowired
    private AudespLimiteLrfService audespLimiteLrfService;

    @Autowired
    private AudespService audespService;

    @Autowired
    private AudespAlertasService audespAlertasService;

    @Autowired
    private ApontamentosODSService apontamentosODSService;

    private Map<String, List<ApontamentoODS>> apontamentosODS;

    @Autowired
    private ResourceLoader resourceLoader;

    private Map<Integer, NotaIegm> notasIegm;

    private List<AudespResponsavel> responsavelPrefeitura;

    private Map<String,List<AudespResponsavel>> responsavelSubstitutoPrefeitura;

    private MunicipioIbge municipioIegmCodigoIbge;

    private Map<String, String> audespResultadoExecucaoOrcamentaria;

    private Map<String, String> audespResultadoExecucaoOrcamentariaExcercicioAnt;

    @Autowired
    private AudespEntidadeService audespEntidadeService;

    private AudespEntidade audespEntidade;

    @Autowired
    private DemonstrativosRaeeService demonstrativosRaeeService;

    private  Map<String,String> anexo14AMap;

    private Map<String, String> audespEnsinoFundeb;

    private Map<String, String> audespSaude;

    private Map<String, String> audespDespesaPessoalMap;

    private Map<String, String> quadroGeralEnsinoMap;

    private Map<String, String> aplicacoesEmSaude;

    @Autowired
    private ApontamentosFOService apontamentosFOService;

    private Map<Integer, List<ApontamentoFO>> apontamentoFOMap;

    @Autowired
    private AudespDividaAtivaService audespDividaAtivaService;

    private Map<String, String> audespDividaAtivaMap;

    private Map<String,String> audespResultadoExecucaoOrcamentariaMap;

    @Autowired
    private AudespFase3Service audespFase3Service;

    private Map<String, AudespFase3QuadroPessoal> audespFase3QuadroDePessoalMap;

    @Autowired
    private AudespBiService audespBiService;

    @Autowired
    private TcespBiService tcespBiService;

    private Map<Integer, String> valorInvestimentoMunicipioMap;

    private Map<String, String> rclMunicipioDevedoresMap;

    private Map<String, String> limiteLRFMap;

    private Integer quadrimestre;

    private String heading1 = "Seção 1";
    private String heading2 = "Seção 2";
    private String heading3 = "Seção 3";
    private String heading4 = "Seção 4";


    private ResponseEntity<Resource> sendFile() throws IOException {
        // fim --------------------------------------
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-relatorio-q-pm-v1.1.2.docx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }





    private void addSecao(TextoFormatado textoFormatado, String heading) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setStyle(heading);
        paragraph.setSpacingAfter(6 * 20);
        if (heading.equals(this.heading1)) {
            CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill("d9d9d9");
        }

        textoFormatado.setParagraphText(paragraph);
    }

    private void createDocumentStyles() {
        XWPFStyles styles = document.createStyles();


        addCustomHeadingStyle(styles, heading1, 1);
        addCustomHeadingStyle(styles, heading2, 2);
        addCustomHeadingStyle(styles, heading3, 3);
        addCustomHeadingStyle(styles, heading4, 4);
    }


    private void addCustomHeadingStyle(XWPFStyles styles, String strStyleId, int headingLevel) {

        CTStyle ctStyle = CTStyle.Factory.newInstance();
        ctStyle.setStyleId(strStyleId);


        CTString styleName = CTString.Factory.newInstance();
        styleName.setVal(strStyleId);
        ctStyle.setName(styleName);

        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(headingLevel));

        // lower number > style is more prominent in the formats bar
        ctStyle.setUiPriority(indentNumber);

        CTOnOff onoffnull = CTOnOff.Factory.newInstance();
        ctStyle.setUnhideWhenUsed(onoffnull);

        // style shows up in the formats bar
        ctStyle.setQFormat(onoffnull);

        // style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        XWPFStyle style = new XWPFStyle(ctStyle);

        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setAscii("Arial");

        CTRPr rpr = CTRPr.Factory.newInstance();
//        rpr.setRFonts(fonts);

        style.getCTStyle().setRPr(rpr);
        // is a null op if already defined

        style.setType(STStyleType.PARAGRAPH);
        styles.addStyle(style);

    }

    public byte[] hexToBytes(String hexString) {
        HexBinaryAdapter adapter = new HexBinaryAdapter();
        byte[] bytes = adapter.unmarshal(hexString);
        return bytes;
    }

    private TextoFormatado addTab() {
        Formatacao formatacaoTab = formatacaoFactory.getTab();
        return new TextoFormatado("", formatacaoTab).concat("", formatacaoTab);
    }

    private String getQuadrimestreTituloComAno(Integer quadrimestre) {
        String quadrimestreTitulo = "1º/2º Quadrimestre de 2018";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre de 2018";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre de 2018";
        return quadrimestreTitulo;
    }

    private String getQuadrimestreTitulo(Integer quadrimestre) {
        String quadrimestreTitulo = "3º Quadrimestre";
        if (quadrimestre == 1)
            quadrimestreTitulo = "1º Quadrimestre";
        if (quadrimestre == 2)
            quadrimestreTitulo = "2º Quadrimestre";
        return quadrimestreTitulo;
    }

    private Integer getMesReferencia(Integer quadrimestre) {
        Integer mesReferencia = 12;
        if (quadrimestre == 1)
            mesReferencia = 4;
        if (quadrimestre == 2)
            mesReferencia = 8;
        return mesReferencia;
    }

    private void getTabela(XWPFDocument document, String[][] itensTabela, boolean isBold, String fontFamily, int fontSize) {
        XWPFTable table = document.createTable(0, 0);
        table.setWidthType(TableWidthType.PCT);
        table.setWidth("98%");
        table.removeBorders();

        for (int i = 0; i < itensTabela.length; i++) {
            if (table.getRow(i) == null)
                table.createRow();


            for (int x = 0; x < itensTabela[i].length; x++) {
                XWPFTableCell cell;
                // verifica se já existe a cell 0
                if (table.getRow(i).getCell(x) != null)
                    cell = table.getRow(i).getCell(x);
                else
                    cell = table.getRow(i).createCell();

                cell.removeParagraph(0);
                XWPFParagraph cellPar = cell.addParagraph();
                XWPFRun cellParRun = cellPar.createRun();
                cellParRun.setFontFamily(fontFamily);
                cellParRun.setText(itensTabela[i][x]);
                cellParRun.setBold(isBold);
                cellParRun.setFontSize(fontSize);
            }
        }
    }


    private String formataTcManualRedacao(String tc) {
        while(tc.length() < 13) {
            tc = "0" + tc;
        }
        tc = tc.replaceAll("/",".");
        return tc;
    }

    private void addTabelaDadosIniciais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "5%", "75%"}, false);
        formatacaoTabela.setBorder(false);


        String vigenciasPrefeito = "";

        for(int i = 0; i < this.responsavelPrefeitura.size(); i++) {
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataInicioVigenciaFormatado(): "dado não informado";
            vigenciasPrefeito += " a ";
            vigenciasPrefeito += this.responsavelPrefeitura.get(i) != null ? this.responsavelPrefeitura.get(i).getDataFimVigenciaFormatado() :  "dado não informado";
            if(i != this.responsavelPrefeitura.size() - 1){
                vigenciasPrefeito += "; ";
            }
        }

        String vigenciasSubstituto = "";

        AudespResponsavel prefeito = (this.responsavelPrefeitura != null
                && this.responsavelPrefeitura.size() > 0) ? this.responsavelPrefeitura.get(0) : new AudespResponsavel();

        if(prefeito == null)
            prefeito = new AudespResponsavel();

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Processo", formatacaoFactory.getBold(12)));
        linha1.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha1.add(new TextoFormatado("TC-" +  formataTcManualRedacao(tabelasProtocolo.getProcesso()),
                formatacaoFactory.getFormatacao(12)));
        dados.add(linha1);

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Entidade", formatacaoFactory.getBold(12)));
        linha2.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha2.add(new TextoFormatado(tabelasProtocolo.getNomeOrgao(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha2);

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Assunto", formatacaoFactory.getBold(12)));
        linha3.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha3.add(new TextoFormatado("Acompanhamento das Contas Anuais", formatacaoFactory.getFormatacao(12)));
        dados.add(linha3);

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Período examinado", formatacaoFactory.getBold(12)));
        linha4.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha4.add(new TextoFormatado(this.quadrimestre+"º quadrimestre de 2019", formatacaoFactory.getFormatacao(12)));
        dados.add(linha4);

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Prefeito", formatacaoFactory.getBold(12)));
        linha5.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha5.add(new TextoFormatado(prefeito.getNome(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha5);

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
        linha6.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha6.add(new TextoFormatado(prefeito.getCpf(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha6);

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
        linha7.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha7.add(new TextoFormatado(vigenciasPrefeito, formatacaoFactory.getFormatacao(12)));
        dados.add(linha7);

        for (Map.Entry<String, List<AudespResponsavel>> item : this.responsavelSubstitutoPrefeitura.entrySet()) {
            String nome = item.getKey();
            List<AudespResponsavel> listaSubstitutos = item.getValue();
            String vigencias = "";
            for (int i = 0; i < listaSubstitutos.size(); i++) {
                vigencias += listaSubstitutos.get(i).getDataInicioVigenciaFormatado();
                vigencias += " a ";
                vigencias += listaSubstitutos.get(i).getDataFimVigenciaFormatado();
                if (i != listaSubstitutos.size() - 1) {
                    vigencias += "; ";
                }
            }

            AudespResponsavel subs = listaSubstitutos != null && listaSubstitutos.size() > 0 ?
                    listaSubstitutos.get(0) : new AudespResponsavel();


            List<TextoFormatado> linhaSub = new ArrayList<>();
            linhaSub.add(new TextoFormatado("Substituto", formatacaoFactory.getBold(12)));
            linhaSub.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaSub.add(new TextoFormatado(nome, formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaCpf = new ArrayList<>();
            linhaCpf.add(new TextoFormatado("CPF nº", formatacaoFactory.getBold(12)));
            linhaCpf.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaCpf.add(new TextoFormatado(subs.getCpf(), formatacaoFactory.getFormatacao(12)));

            List<TextoFormatado> linhaPeriodo = new ArrayList<>();
            linhaPeriodo.add(new TextoFormatado("Período", formatacaoFactory.getBold(12)));
            linhaPeriodo.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
            linhaPeriodo.add(new TextoFormatado(vigencias, formatacaoFactory.getFormatacao(12)));

            dados.add(linhaSub);
            dados.add(linhaCpf);
            dados.add(linhaPeriodo);
        }


        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("Relatoria", formatacaoFactory.getBold(12)));
        linha11.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha11.add(new TextoFormatado(tabelasProtocolo.getRelator(), formatacaoFactory.getFormatacao(12)));
        dados.add(linha11);

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Instrução", formatacaoFactory.getBold(12)));
        linha12.add(new TextoFormatado(":", formatacaoFactory.getBoldCenter(12)));
        linha12.add(new TextoFormatado(tabelasProtocolo.getSecaoFiscalizadoraContas().trim() + " / " +
                tabelasProtocolo.getDsf(), formatacaoFactory.getFormatacao(12)));

        dados.add(linha12);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void getHeader() throws InvalidFormatException, IOException {
        // Header
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFHeader header = headerFooterPolicy.createHeader(XWPFHeaderFooterPolicy.DEFAULT);

        CTP ctpFooter = CTP.Factory.newInstance();
        CTR ctrFooter = ctpFooter.addNewR();
        CTText ctFooter = ctrFooter.addNewT();

        XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
        footerParagraph.setAlignment(ParagraphAlignment.RIGHT);
        XWPFRun footerRun = footerParagraph.createRun();
        footerRun.setFontFamily("Arial");
        footerRun.setFontSize(8);
        footerRun.getCTR().addNewPgNum();

        XWPFParagraph[] parsFooter = new XWPFParagraph[1];
        parsFooter[0] = footerParagraph;
        headerFooterPolicy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

        XWPFParagraph headerPar = header.createParagraph();
        XWPFRun headerParRun = headerPar.createRun();

        headerParRun.setFontFamily("Arial");
        headerParRun.setFontSize(12);
        headerPar.setAlignment(ParagraphAlignment.RIGHT);

        XWPFTable headerTable = header.createTable(1, 3);
        headerTable.removeBorders();
        headerTable.setWidthType(TableWidthType.PCT);
        headerTable.setWidth("98%");

        XWPFTableRow headerTableRow = headerTable.getRow(0);

        //coluna 1
        headerTableRow.getCell(0).removeParagraph(0);
        XWPFParagraph pImgEsquerda = headerTableRow.getCell(0).addParagraph();
        pImgEsquerda.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgEsquerda = pImgEsquerda.createRun();

        Resource resourceBrasaoSP = resourceLoader.getResource("classpath:static/imagens/brasao_sp.png");

        InputStream brasao_sp = resourceBrasaoSP.getInputStream();
        runPImgEsquerda.addPicture(brasao_sp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_sp.png",
                Units.toEMU(50), Units.toEMU(55));

        //coluna 2
        headerTableRow.getCell(1).removeParagraph(0);
        XWPFParagraph p1 = headerTableRow.getCell(1).addParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p1Run = p1.createRun();

        p1Run.setFontFamily("Arial");
        p1Run.setFontSize(12);
        p1Run.setBold(true);
        p1Run.setText("TRIBUNAL DE CONTAS DO ESTADO DE SÃO PAULO");

        XWPFParagraph p2 = headerTableRow.getCell(1).addParagraph();
        p2.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun p2Run = p2.createRun();
        p2Run.setFontSize(12);
        p2Run.setFontFamily("Arial");
        p2Run.setText(tabelasProtocolo.getDescricaoArea());

        //coluna 3
        headerTableRow.getCell(2).removeParagraph(0);
        XWPFParagraph pImgDireita = headerTableRow.getCell(2).addParagraph();
        pImgDireita.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runPImgDireita = pImgDireita.createRun();
        Resource resourceBrasaoTcesp = resourceLoader.getResource("classpath:static/imagens/brasao_tcesp.png");

        InputStream brasao_tcesp = resourceBrasaoTcesp.getInputStream();
        runPImgDireita.addPicture(brasao_tcesp, XWPFDocument.PICTURE_TYPE_PNG, "brasao_tcesp.png", Units.toEMU(50), Units.toEMU(55));
    }


    private XWPFParagraph getTitulo(XWPFDocument document, String quadrimestre) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = paragraph.createRun();
        run.setBold(true);
        run.setFontFamily("Arial");
        run.setFontSize(12);
        run.setText("RELATÓRIO DE FISCALIZAÇÃO 1º / 2º QUADRIMESTRE\nPREFEITURA MUNICIPAL");
        return paragraph;
    }

    private void underLine(XWPFRun run) {
        run.setUnderline(UnderlinePatterns.SINGLE);
    }

    private XWPFRun addToParagrafoBreak(XWPFParagraph paragrafo, String texto) {
        XWPFRun run = paragrafo.getRuns().get(0);
        run.addBreak();
        run.setText(texto);
        return run;
    }

    private void addToParagrafoRed(XWPFParagraph paragrafo, String texto, String fontStyle, int fontSize) {
        XWPFRun run = paragrafo.createRun();
        run.setText(texto);
        run.setColor("ff0000");
        run.setFontFamily(fontStyle);
        run.setFontSize(fontSize);
        run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
    }

    private XWPFParagraph getParagrafoGrayHeader(XWPFDocument document, String texto, boolean isBold,
                                                 String fontFamily, int fontSize, ParagraphAlignment paragraphAlignment, boolean haveTab) {
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(paragraphAlignment);
        XWPFRun run = paragraph.createRun();
        if (haveTab)
            run.addTab();

        run.setBold(isBold);
        run.setFontFamily(fontFamily);
        run.setFontSize(fontSize);
        run.setText(texto);

        // shading
        //run.setTextHighlightColor(STHighlightColor.LIGHT_GRAY.toString());
        //run.getCTR().addNewRPr().addNewShd();
        CTShd cTShd = paragraph.getCTP().addNewPPr().addNewShd();
        cTShd.setVal(STShd.CLEAR);
        cTShd.setFill("d9d9d9");

        return paragraph;
    }


    //////////////////////
    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();

//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());

        paragraph.setSpacingAfter(6 * 20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafoSpacingAfter(TextoFormatado textoFormatado) {
        XWPFParagraph paragraph = document.createParagraph();
//        XWPFRun run = paragraph.createRun();
//        textoFormatado.setFormatacao(textoFormatado.getFormatacao());
//        paragraph.setSpacingAfter(6*20);
        textoFormatado.setParagraphText(paragraph);

        return paragraph;
    }

    private XWPFParagraph addParagrafo(TextoFormatado textoFormatado, boolean breakPage) {
        XWPFParagraph paragraph = addParagrafo(textoFormatado);
        paragraph.setPageBreak(breakPage);
        return paragraph;
    }

    private XWPFTable addTabela(List<List<TextoFormatado>> dados, FormatacaoTabela formatacaoTabela) {

        int qdtLinhas = dados.size();
        int qtdColunas = 0;

        for (int i = 0; i < dados.size(); i++) {
            if (qtdColunas < dados.get(i).size())
                qtdColunas = dados.get(i).size();
        }

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        formatacaoTabela.formatarTabela(tabela);

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
//            row.setHeight((int) (twipsPerInch * 1 / 5)); //set height 1/10 inch.
//            row.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact"
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }

            for (int j = 0; j < dados.get(i).size(); j++) {
                XWPFTableCell cell = j == 0 ? row.getCell(j) : row.createCell();
                cell.setWidth(formatacaoTabela.getWidths().get(j));
                cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                if (i == 0 && formatacaoTabela.isFirstLineHeader())
                    cell.getCTTc().addNewTcPr().addNewShd().setFill("cccccc");

                XWPFRun run = cell.getParagraphs().get(0).createRun();

                XWPFParagraph paragrafo = cell.getParagraphs().get(0);
                paragrafo.setSpacingBetween(1);

                dados.get(i).get(j).setParagraphText(paragrafo);

            }

        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable addTabelaColunaUnica(List<TextoFormatado> dados, FormatacaoTabela formatacaoTabela) {

        XWPFTable tabela = document.createTable();
        tabela.setWidthType(TableWidthType.PCT);
        tabela.setWidth(formatacaoTabela.getWidthTabela());

        for(int k = 1; k < tabela.getRows().size(); k++ ) {
            tabela.removeRow(k);
        }

        formatacaoTabela.formatarTabela(tabela);

        if(dados.size() == 0) {
            return tabela;
        }

        for (int i = 0; i < dados.size(); i++) {
            XWPFTableRow row = tabela.createRow();

            int twipsPerInch = 1440;
            row.setCantSplitRow(false);

            for (int j = 1; j < row.getTableCells().size(); j++) {
                row.removeCell(j);
            }
            XWPFTableCell cell = null;

            if(row.getTableCells().size() == 0)
                cell = row.createCell();
            else
                cell = row.getCell(0);

            cell.setWidth("100%");
            cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
            if (i % 2 == 0 )
                cell.getCTTc().addNewTcPr().addNewShd().setFill("dce6f1");

            XWPFRun run = cell.getParagraphs().get(0).createRun();

            XWPFParagraph paragrafo = cell.getParagraphs().get(0);
            paragrafo.setSpacingBetween(1);

            dados.get(i).setParagraphText(paragrafo);
        }

        tabela.removeRow(0);

        return tabela;

    }

    private XWPFTable mergeCells(XWPFTable tabela, List<MergePosition> cellsToMerge) {
        tabela = mergeCellsHorizontal(tabela, cellsToMerge);
        tabela = mergeCellsVertical(tabela, cellsToMerge);
        return tabela;

    }

    private void addListaNumerada(List<TextoFormatado> listItens) {

        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();
        //Next we set the AbstractNumId. This requires care.
        //Since we are in a new document we can start numbering from 0.
        //But if we have an existing document, we must determine the next free number first.
        cTAbstractNum.setAbstractNumId(BigInteger.valueOf(0));

        /* Bullet list
          CTLvl cTLvl = cTAbstractNum.addNewLvl();
          cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET);
          cTLvl.addNewLvlText().setVal("•");
        */

        ///* Decimal list
        CTLvl cTLvl = cTAbstractNum.addNewLvl();
        cTLvl.addNewNumFmt().setVal(STNumberFormat.DECIMAL);
        cTLvl.addNewPPr();
        CTInd ind = cTLvl.getPPr().addNewInd(); //Set the indent

        ind.setHanging(BigInteger.valueOf(360*2));
        ind.setLeft(BigInteger.valueOf(360*6));
        cTLvl.addNewLvlText().setVal("%1.");
        cTLvl.addNewStart().setVal(BigInteger.valueOf(1));
        //*/

        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);

        XWPFNumbering numbering = document.createNumbering();

        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);

        BigInteger numID = numbering.addNum(abstractNumID);

        for (TextoFormatado item : listItens) {
            item.setListNumId(numID);
            addParagrafo(item);
        }
    }

    private XWPFTable mergeCellsHorizontal(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTHMerge hMerge = CTHMerge.Factory.newInstance();
            hMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setHMerge(hMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeHorizontal().size(); j++) {
                CTHMerge hToMerge = CTHMerge.Factory.newInstance();
                hToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeHorizontal().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeHorizontal().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setHMerge(hToMerge);
            }
        }

        return tabela;
    }

    private XWPFTable mergeCellsVertical(XWPFTable tabela, List<MergePosition> cellsToMerge) {

        for (int i = 0; i < cellsToMerge.size(); i++) {
            CTVMerge vMerge = CTVMerge.Factory.newInstance();
            vMerge.setVal(STMerge.RESTART);
            tabela.getRow(cellsToMerge.get(i).getLinha()).getCell(cellsToMerge.get(i).getColuna())
                    .getCTTc().getTcPr().setVMerge(vMerge);
            for (int j = 0; j < cellsToMerge.get(i).getToMergeVertical().size(); j++) {
                CTVMerge vToMerge = CTVMerge.Factory.newInstance();
                vToMerge.setVal(STMerge.CONTINUE);
                int linha = cellsToMerge.get(i).getToMergeVertical().get(j).getLinha();
                int coluna = cellsToMerge.get(i).getToMergeVertical().get(j).getColuna();
                tabela.getRow(linha).getCell(coluna).getCTTc().getTcPr().setVMerge(vToMerge);
            }
        }

        return tabela;
    }

    private void addBreak() {
        this.document.createParagraph();//.createRun().addBreak();
    }

    private void addPageBreak() {
        XWPFParagraph paragraph = this.document.createParagraph();
        paragraph.setPageBreak(true);
    }

    public void addSaudacao() {
//        if(secaoFiscalizadoraContas != null && Character.isDigit(secaoFiscalizadoraContas.charAt(0))){
//            return "Senhor(a) Diretor(a) da "+ getDescricaoArea() + ",";
//        }
//        else{
//            return "Senhor(a) Diretor(a) da " + getDescricaoArea() + ",";
//        }
        addParagrafo(new TextoFormatado(tabelasProtocolo.getSaudacao(), formatacaoFactory.getBold(12)));
    }

    private void addTabelaIegm() {

        NotaIegm notaIegm2016 = this.notasIegm.get(2016) != null ? this.notasIegm.get(2016) : new NotaIegm();
        NotaIegm notaIegm2017 = this.notasIegm.get(2017) != null ? this.notasIegm.get(2017) : new NotaIegm();
        //NotaIegm notaIegm2018 = this.notasIegm.get(2018) != null ? this.notasIegm.get(2018) : new NotaIegm();

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"25%", "25%", "25%", "25%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("EXERCÍCIOS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2016", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2017", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));


        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("IEG-M", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(notaIegm2016.getFaixa_iegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(notaIegm2017.getFaixa_iegm(), formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(resultadoIegm2018.getFaixaIegm(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("i-Planejamento", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(notaIegm2016.getFaixa_iplanejamento(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(notaIegm2017.getFaixa_iplanejamento(), formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(resultadoIegm2018.getFaixaIPlanejamento(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("i-Fiscal", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(notaIegm2016.getFaixa_ifiscal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(notaIegm2017.getFaixa_ifiscal(), formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(resultadoIegm2018.getFaixaIFiscal(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("i-Educ", formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(notaIegm2016.getFaixa_ieduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(notaIegm2017.getFaixa_ieduc(), formatacaoFactory.getCenter(9)));
        linha5.add(new TextoFormatado(resultadoIegm2018.getFaixaIEduc(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("i-Saúde", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(notaIegm2016.getFaixa_isaude(), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(notaIegm2017.getFaixa_isaude(), formatacaoFactory.getCenter(9)));
        linha6.add(new TextoFormatado(resultadoIegm2018.getFaixaISaude(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("i-Amb", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(notaIegm2016.getFaixa_iamb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(notaIegm2017.getFaixa_iamb(), formatacaoFactory.getCenter(9)));
        linha7.add(new TextoFormatado(resultadoIegm2018.getFaixaIAmb(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("i-Cidade", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(notaIegm2016.getFaixa_icidade(), formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(notaIegm2017.getFaixa_icidade(), formatacaoFactory.getCenter(9)));
        linha8.add(new TextoFormatado(resultadoIegm2018.getFaixaICidade(), formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("i-Gov-TI", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(notaIegm2016.getFaixa_igov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(notaIegm2017.getFaixa_igov(), formatacaoFactory.getCenter(9)));
        linha9.add(new TextoFormatado(resultadoIegm2018.getFaixaIGov(), formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }


    private void addTabelaDescricaoFonteDado() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "30%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("DESCRIÇÃO", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("FONTE/DATA", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DADO/ANO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("POPULAÇÃO", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado("Site IBGE-Cidades", formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(municipioIegmCodigoIbge.getPopulacao() + " habitantes", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("ARRECADAÇÃO MUNICIPAL", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado("Audesp", formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespResultadoExecucaoOrcamentariaExcercicioAnt.get("vRecArrec"), formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaPareceres() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "30%", "50%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Exercícios", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Processos", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Pareceres", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getTc(), formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(pareceresPrefeiturasList.get(0).getParecer(), formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getTc(), formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(pareceresPrefeiturasList.get(1).getParecer(), formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getExercicio().toString(), formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getTc(), formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(pareceresPrefeiturasList.get(2).getParecer(), formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);
    }

    private void addTabelaExecucaoOrcamentaria() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"60%", "25%", "15%"}, true);

        Map<String, String> audespResultado = this.audespResultadoExecucaoOrcamentaria ;


        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> cabecalhoLinha1 = new ArrayList<>();
        cabecalhoLinha1.add(new TextoFormatado("EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBoldCenter(9)));
        cabecalhoLinha1.add(new TextoFormatado("R$", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> cabecalhoLinha2 = new ArrayList<>();
        cabecalhoLinha2.add(new TextoFormatado("(+) RECEITAS REALIZADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha2.add(new TextoFormatado(audespResultado.get("vSubTotRecRealPM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha3 = new ArrayList<>();
        cabecalhoLinha3.add(new TextoFormatado("(-) DESPESAS EMPENHADAS", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha3.add(new TextoFormatado(audespResultado.get("despesaEmpenhadaTotal"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha4 = new ArrayList<>();
        cabecalhoLinha4.add(new TextoFormatado("(-) REPASSES DE DUODÉCIMOS À CÂMARA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha4.add(new TextoFormatado(audespResultado.get("vRepDuodCM"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha5 = new ArrayList<>();
        cabecalhoLinha5.add(new TextoFormatado("(+) DEVOLUÇÃO DE DUODÉCIMOS DA CÂMARA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha5.add(new TextoFormatado(audespResultado.get("vDevDuod"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha6 = new ArrayList<>();
        cabecalhoLinha6.add(new TextoFormatado("(-) TRANSFERÊNCIAS FINANCEIRAS À ADMINISTRAÇÃO INDIRETA", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha6.add(new TextoFormatado(audespResultado.get("vTransfFinAdmIndExec"), formatacaoFactory.getRight(9)));


        List<TextoFormatado> cabecalhoLinha7 = new ArrayList<>();
        cabecalhoLinha7.add(new TextoFormatado("(+ ou -) AJUSTES DA FISCALIZAÇÃO", formatacaoFactory.getFormatacao(9)));
        cabecalhoLinha7.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> cabecalhoLinha8 = new ArrayList<>();
        cabecalhoLinha8.add(new TextoFormatado("RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA", formatacaoFactory.getBold(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("resultadoExecucaoOrcamentaria"), formatacaoFactory.getBoldRight(9)));
        cabecalhoLinha8.add(new TextoFormatado(audespResultado.get("percentualExecucaoOrcamentaria"), formatacaoFactory.getBoldRight(9)));

        dados.add(cabecalhoLinha1);
        dados.add(cabecalhoLinha2);
        dados.add(cabecalhoLinha3);
        dados.add(cabecalhoLinha4);
        dados.add(cabecalhoLinha5);
        dados.add(cabecalhoLinha6);
        dados.add(cabecalhoLinha7);
        dados.add(cabecalhoLinha8);

        addTabela(dados, formatacaoTabela);
    }

    private void addAnaliseFontesDocumentais() {
        List<TextoFormatado> itensLista = new ArrayList<>();
        itensLista.add(new TextoFormatado("Indicadores finalísticos componentes do IEG-M – Índice de" +
                " Efetividade da Gestão Municipal;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Ações fiscalizatórias desenvolvidas através da seletividade " +
                "(contratos e repasses) e da fiscalização ordenada; ", formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Prestações de contas mensais do exercício em exame, encaminhadas pela " +
                "Chefia do Poder Executivo;", formatacaoFactory.getFormatacao(12)));

        itensLista.add(new TextoFormatado("Resultado do acompanhamento simultâneo do Sistema Audesp, bem como " +
                "acesso aos dados, informações e análises " +
                "disponíveis no referido ambiente;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das denúncias, representações e expedientes diversos; ",
                formatacaoFactory.getFormatacao(12))
                .concat("QUANDO HOUVER",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));
        itensLista.add(new TextoFormatado("Leitura analítica dos três últimos relatórios de fiscalização e" +
                " respectivas decisões desta Corte, sobretudo no tocante a assuntos relevantes nas ressalvas, " +
                "advertências e recomendações;", formatacaoFactory.getFormatacao(12)));
        itensLista.add(new TextoFormatado("Análise das informações disponíveis nos demais " +
                "sistemas de e. Tribunal de Contas do Estado.", formatacaoFactory.getFormatacao(12)));

        addListaNumerada(itensLista);
    }

    private void addTabelaComparativoLimiteLRF() {


        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "20%", "10%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("QUADRO COMPARATIVO COM OS LIMITES DA LRF", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("R$", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("RECEITA CORRENTE LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado(limiteLRFMap.get("vRCL"), formatacaoFactory.getBoldRight(9)));
        linha2.add(new TextoFormatado(limiteLRFMap.get("rclPerc"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DÍVIDA CONSOLIDADA LÍQUIDA",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(limiteLRFMap.get("vDivConsolidLiq"), formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(limiteLRFMap.get("vPercDivConsolidLiq"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Limite Legal - Artigos 3º e 4º. Resolução 40 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(limiteLRFMap.get("divConsolidLimite"), formatacaoFactory.getRight(9)));
        linha5.add(new TextoFormatado(limiteLRFMap.get("divConsolidPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha6.add(new TextoFormatado(limiteLRFMap.get("excessoDividaConsolidada"), formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("CONCESSÕES DE GARANTIAS",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Montante",
                formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(limiteLRFMap.get("vConcGar"), formatacaoFactory.getRight(9))); //VER
        linha8.add(new TextoFormatado(limiteLRFMap.get("vConcGar"), formatacaoFactory.getRight(9))); //VER

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Limite Legal - Artigo 9º. Resolução 43 do Senado", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(limiteLRFMap.get("concGarantiaLimite"), formatacaoFactory.getRight(9)));
        linha9.add(new TextoFormatado(limiteLRFMap.get("concGarantiaPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha10.add(new TextoFormatado(limiteLRFMap.get("excessoConcessaoGarantias"), formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO - Exceto ARO",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(limiteLRFMap.get("vOpCred"), formatacaoFactory.getRight(9)));
        linha12.add(new TextoFormatado(limiteLRFMap.get("opCredPercentual"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("Limite Legal - Artigo 7º, I. Resolução 43 do Senado ",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(limiteLRFMap.get("vLimOpCred"), formatacaoFactory.getRight(9)));
        linha13.add(new TextoFormatado(limiteLRFMap.get("opCreditoExcAroPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha15 = new ArrayList<>();
        linha15.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha15.add(new TextoFormatado(limiteLRFMap.get("excessoOperacoesCredito"), formatacaoFactory.getFormatacao(9)));
        linha15.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha16 = new ArrayList<>();
        linha16.add(new TextoFormatado("DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha16.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha17 = new ArrayList<>();
        linha17.add(new TextoFormatado("Realizadas no Período",
                formatacaoFactory.getFormatacao(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha17.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha18 = new ArrayList<>();
        linha18.add(new TextoFormatado("OPERAÇÕES DE CRÉDITO (Exceto ARO) > DESPESAS DE CAPITAL",
                formatacaoFactory.getBold(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha18.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha19 = new ArrayList<>();
        linha19.add(new TextoFormatado("ANTECIPAÇÃO DE RECEITAS ORÇAMENTÁRIAS - ARO",
                formatacaoFactory.getBold(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha19.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha20 = new ArrayList<>();
        linha20.add(new TextoFormatado("Saldo Devedor",
                formatacaoFactory.getFormatacao(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha20.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha21 = new ArrayList<>();
        linha21.add(new TextoFormatado("Limite Legal - Artigo 10. Resolução 43 do Senado",
                formatacaoFactory.getFormatacao(9)));
        linha21.add(new TextoFormatado(limiteLRFMap.get("opAntecipacaoAroLimite"), formatacaoFactory.getRight(9)));
        linha21.add(new TextoFormatado(limiteLRFMap.get("opAntecipacaoAroPercLimite"), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha22 = new ArrayList<>();
        linha22.add(new TextoFormatado("Excesso a Regularizar",
                formatacaoFactory.getBold(9)));
        linha22.add(new TextoFormatado(limiteLRFMap.get("excessoARO"), formatacaoFactory.getFormatacao(9)));
        linha22.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha23 = new ArrayList<>();
        linha23.add(new TextoFormatado("RECURSOS OBTIDOS COM A ALIENAÇÃO DE ATIVOS",
                formatacaoFactory.getBold(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha23.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha24 = new ArrayList<>();
        linha24.add(new TextoFormatado("Saldo do exercício anterior",
                formatacaoFactory.getBold(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha24.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        List<TextoFormatado> linha25 = new ArrayList<>();
        linha25.add(new TextoFormatado("Valor arrecadado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha25.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha26 = new ArrayList<>();
        linha26.add(new TextoFormatado("Valor aplicado no exercício",
                formatacaoFactory.getFormatacao(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha26.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha27 = new ArrayList<>();
        linha27.add(new TextoFormatado("Saldo a Aplicar",
                formatacaoFactory.getBold(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha27.add(new TextoFormatado(" ", formatacaoFactory.getBold(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);
//        dados.add(linha14);
        dados.add(linha15);
        dados.add(linha16);
        dados.add(linha17);
        dados.add(linha18);
        dados.add(linha19);
        dados.add(linha20);
        dados.add(linha21);
        dados.add(linha22);
        dados.add(linha23);
        dados.add(linha24);
        dados.add(linha25);
        dados.add(linha26);
        dados.add(linha27);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);


        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(2, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(2, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(2, 2));

        MergePosition mergeH2 = new MergePosition(6, 0);
        mergeH2.addToMergeHorizontal(new MergePosition(6, 1));
        mergeH2.addToMergeHorizontal(new MergePosition(6, 2));

        MergePosition mergeH3 = new MergePosition(10, 0);
        mergeH3.addToMergeHorizontal(new MergePosition(10, 1));
        mergeH3.addToMergeHorizontal(new MergePosition(10, 2));

        MergePosition mergeH4 = new MergePosition(14, 0);
        mergeH4.addToMergeHorizontal(new MergePosition(14, 1));
        mergeH4.addToMergeHorizontal(new MergePosition(14, 2));

        MergePosition mergeH5 = new MergePosition(16, 1);
        mergeH5.addToMergeHorizontal(new MergePosition(16, 2));

        MergePosition mergeH6 = new MergePosition(17, 0);
        mergeH6.addToMergeHorizontal(new MergePosition(17, 1));
        mergeH6.addToMergeHorizontal(new MergePosition(17, 2));

        MergePosition mergeH7 = new MergePosition(21, 0);
        mergeH7.addToMergeHorizontal(new MergePosition(21, 1));
        mergeH7.addToMergeHorizontal(new MergePosition(21, 2));

        MergePosition mergeV1 = new MergePosition(21, 2);
        mergeV1.addToMergeVertical(new MergePosition(22, 2));
        mergeV1.addToMergeVertical(new MergePosition(23, 2));
        mergeV1.addToMergeVertical(new MergePosition(24, 2));
        mergeV1.addToMergeVertical(new MergePosition(25, 2));

        mergePositions.add(mergeH1);
        mergePositions.add(mergeH2);
        mergePositions.add(mergeH3);
        mergePositions.add(mergeH4);
        mergePositions.add(mergeH5);
        mergePositions.add(mergeH6);
        mergePositions.add(mergeH7);
        mergePositions.add(mergeV1);
        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesaDePessoalPrimeiroQuadrimestre() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%", "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2019", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("% Permitido Legal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Gasto Informado",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio ) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Gastos Ajustados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receita Corrente Líquida",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RCL Ajustada",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 4), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("% Gasto Informado",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 4), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("% Gasto Ajustado",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 4), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));


        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesaDePessoalSegundoQuadrimestre() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"20%", "20%", "20%", "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Período", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Dez", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Abr", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Ago", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("",
                formatacaoFactory.getBold(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2018", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2019", formatacaoFactory.getBoldCenter(9)));
        linha2.add(new TextoFormatado("2019", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("% Permitido Legal",
                formatacaoFactory.getBold(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha3.add(new TextoFormatado(audespDespesaPessoalMap.get("vLimPermitido" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Gasto Informado",
                formatacaoFactory.getBold(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha4.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha5.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha6.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("Gastos Ajustados",
                formatacaoFactory.getBold(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha7.add(new TextoFormatado(audespDespesaPessoalMap.get("vDespPessoalLiq" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));


        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("Receita Corrente Líquida",
                formatacaoFactory.getBold(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha8.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("Inclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha9.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("Exclusões da Fiscalização",
                formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));
        linha10.add(new TextoFormatado(" ", formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("RCL Ajustada",
                formatacaoFactory.getBold(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 8), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio - 1) + 12), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 4), formatacaoFactory.getBoldRight(9)));
        linha11.add(new TextoFormatado(audespDespesaPessoalMap.get("vRCL" + (exercicio) + 8), formatacaoFactory.getBoldRight(9)));

        List<TextoFormatado> linha12 = new ArrayList<>();
        linha12.add(new TextoFormatado("% Gasto Informado",
                formatacaoFactory.getFormatacao(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));
        linha12.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8), formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha13 = new ArrayList<>();
        linha13.add(new TextoFormatado("% Gasto Ajustado",
                formatacaoFactory.getFormatacao(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 8), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio - 1) + 12), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 4), formatacaoFactory.getBoldCenter(9)));
        linha13.add(new TextoFormatado(audespDespesaPessoalMap.get("vPercDespPessoal" + (exercicio) + 8), formatacaoFactory.getBoldCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados.add(linha5);
        dados.add(linha6);
        dados.add(linha7);
        dados.add(linha8);
        dados.add(linha9);
        dados.add(linha10);
        dados.add(linha11);
        dados.add(linha12);
        dados.add(linha13);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();

        MergePosition mergeV1 = new MergePosition(0, 0);
        mergeV1.addToMergeVertical(new MergePosition(1, 0));


        mergePositions.add(mergeV1);

        mergeCells(tabela, mergePositions);

    }

    private void addTabelaDespesasCfFundeb() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"80%", "20%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 212 da Constituição Federal:", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespEnsinoFundeb.get("vPercLiqEnsino"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA - RECURSO TESOURO (mínimo 25%)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespEnsinoFundeb.get("vPercPagoEnsino"), formatacaoFactory.getRight(9)));

        List<List<TextoFormatado>> dados2 = new ArrayList<>();

        List<TextoFormatado> linha5 = new ArrayList<>();
        linha5.add(new TextoFormatado("FUNDEB:", formatacaoFactory.getBold(9)));
        linha5.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha6 = new ArrayList<>();
        linha6.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha6.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha7 = new ArrayList<>();
        linha7.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha7.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha8 = new ArrayList<>();
        linha8.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 95%)", formatacaoFactory.getFormatacao(9)));
        linha8.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundeb"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha9 = new ArrayList<>();
        linha9.add(new TextoFormatado("DESPESA EMPENHADA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha9.add(new TextoFormatado(audespEnsinoFundeb.get("vPercEmpFundebMagist"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha10 = new ArrayList<>();
        linha10.add(new TextoFormatado("DESPESA LIQUIDADA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha10.add(new TextoFormatado(audespEnsinoFundeb.get("vDespLiqAplicFundebMagist"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha11 = new ArrayList<>();
        linha11.add(new TextoFormatado("DESPESA PAGA - RECURSO FUNDEB (mínimo 60%)", formatacaoFactory.getFormatacao(9)));
        linha11.add(new TextoFormatado(audespEnsinoFundeb.get("vDespPagaAplicFundebMagist"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);
        dados2.add(linha5);
        dados2.add(linha6);
        dados2.add(linha7);
        dados2.add(linha8);
        dados2.add(linha9);
        dados2.add(linha10);
        dados2.add(linha11);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);
        XWPFTable tabela2 = addTabela(dados2, formatacaoTabela);

    }

    private void addTabelaVagasEscolares() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"40%", "20%", "20%", "20%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("NÍVEL", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DEMANDA POR VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("OFERTA DE VAGAS", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("RESULTADO", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Ens. Infantil (Creche)", formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Ens. Infantil (Pré escola)", formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Ens. Fundamental", formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getRight(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaEmpenhadaLiquidadaPaga() {

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"70%", "30%"}, true);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Art. 77, III c/c § 4º do ADCT", formatacaoFactory.getBold(9)));
        linha1.add(new TextoFormatado("%", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("DESPESA EMPENHADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(audespSaude.get("vPercEmpSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("DESPESA LIQUIDADA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha3.add(new TextoFormatado(audespSaude.get("vPercLiqSaude"), formatacaoFactory.getRight(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("DESPESA PAGA (mínimo 15%)", formatacaoFactory.getFormatacao(9)));
        linha4.add(new TextoFormatado(audespSaude.get("vPercPagoSaude"), formatacaoFactory.getRight(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);

        XWPFTable tabela = addTabela(dados, formatacaoTabela);

    }

    private void addTabelaProcessoDeContasAnuais() {
        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"5%", "70%", "25%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();
        linha1.add(new TextoFormatado("Número:", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC-XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Interessado:",
                formatacaoFactory.getCenter(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        List<TextoFormatado> linha3 = new ArrayList<>();
        linha3.add(new TextoFormatado("Objeto:",
                formatacaoFactory.getCenter(9)));
        linha3.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        List<TextoFormatado> linha4 = new ArrayList<>();
        linha4.add(new TextoFormatado("Procedência:",
                formatacaoFactory.getCenter(9)));
        linha4.add(new TextoFormatado(" ", formatacaoFactory.getCenter(9)));

        dados.add(linha1);
        dados.add(linha2);
        dados.add(linha3);
        dados.add(linha4);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);


    }

    private void addTabelaDoisUltimosExerciciosApreciados(ParecerPrefeitura parecerPrefeitura){

        if(parecerPrefeitura == null) {
            addTabelaDoisUltimosExerciciosApreciados();
            return;
        }

        FormatacaoTabela formatacaoTabela = formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%", "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();

        linha1.add(new TextoFormatado("Exercício\n " + parecerPrefeitura.getExercicio(), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC\n " + parecerPrefeitura.getTc(), formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DOE\n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data do Trânsito em julgado\n " + parecerPrefeitura.getDataTransitoJulgadoString(),
                formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Recomendações:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));

        dados.add(linha1);
        dados.add(linha2);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(1, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


        mergePositions.add(mergeH1);


        mergeCells(tabela, mergePositions);
    }

    private void addTabelaDoisUltimosExerciciosApreciados() {
        FormatacaoTabela formatacaoTabela =
                formatacaoFactory.getFormatacaoTabela(new String[]{"15%", "15%", "15%", "55%"}, false);

        List<List<TextoFormatado>> dados = new ArrayList<>();
        List<TextoFormatado> linha1 = new ArrayList<>();






        linha1.add(new TextoFormatado("Exercício \n XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("TC XXXXXX.XXX.XX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("DOE \n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));
        linha1.add(new TextoFormatado("Data do Trânsito em julgado \n XX/XX/XXXX", formatacaoFactory.getBoldCenter(9)));

        List<TextoFormatado> linha2 = new ArrayList<>();
        linha2.add(new TextoFormatado("Recomendações:", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));
        linha2.add(new TextoFormatado(" ", formatacaoFactory.getFormatacao(9)));


        dados.add(linha1);
        dados.add(linha2);


        XWPFTable tabela = addTabela(dados, formatacaoTabela);

        List<MergePosition> mergePositions = new ArrayList<>();
        MergePosition mergeH1 = new MergePosition(1, 0);
        mergeH1.addToMergeHorizontal(new MergePosition(1, 1));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 2));
        mergeH1.addToMergeHorizontal(new MergePosition(1, 3));


        mergePositions.add(mergeH1);


        mergeCells(tabela, mergePositions);

    }

    public ResponseEntity<Resource> download(Integer codigoIBGE, Integer exercicio, Integer quadrimestre)
            throws IOException, InvalidFormatException, Exception {

        this.document = new XWPFDocument();

        this.quadrimestre = quadrimestre;

        this.exercicio = exercicio;
        this.codigoIBGE = codigoIBGE;

        this.pareceresPrefeiturasList = this.parecerPrefeituraService.getParecerByCodigoIbge(codigoIBGE);
        this.audespEntidade = this.audespEntidadeService.getEntidade(codigoIBGE,exercicio);
//        this.apontamentosODS = apontamentosODSService.getApontamentosODS(codigoIBGE, exercicio);
        this.notasIegm = iegmService.getMapNotasByCodigoIbge(codigoIBGE, exercicio-1);
        this.resultadoIegm2018 = iegmService.getNotasByCodigoIbgePorExercicio(codigoIBGE, exercicio-1);
        this.responsavelPrefeitura = audespService.getResponsavelPrefeitura(codigoIBGE,exercicio, quadrimestre);
        this.responsavelSubstitutoPrefeitura = audespService.getResponsavelSubstitutoPrefeitura(codigoIBGE,exercicio, quadrimestre);
        this.municipioIegmCodigoIbge = tcespBiService.getMunicipioByCodigoIbgeExercicio(codigoIBGE, exercicio);
        this.audespResultadoExecucaoOrcamentaria = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestralFormatado(codigoIBGE, exercicio, 4 * quadrimestre);
        this.audespResultadoExecucaoOrcamentariaExcercicioAnt = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentariaQuadrimestralFormatado(codigoIBGE, exercicio-1, 12);
        this.tabelasProtocolo = tabelasService.getTabelasProtocoloPrefeituraByCodigoIbge(codigoIBGE, exercicio);
//        this.anexo14AMap = this.demonstrativosRaeeService.getAnexo14A(audespEntidade, codigoIBGE, exercicio, 12);
        this.audespEnsinoFundeb = audespEnsinoService.getAudespEnsinoFundeb(codigoIBGE, exercicio, 4 * quadrimestre);
        this.audespSaude = audespSaudeService.getAudespSaude(codigoIBGE, exercicio, 4 * quadrimestre);
//        this.aplicacoesEmSaude = audespSaudeService.getAplicacoesEmSaudeFormatado(codigoIBGE, exercicio, 12);
        this.audespDespesaPessoalMap = this.audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbgeFechamentoFormatado(codigoIBGE, exercicio, 50);
//        this.quadroGeralEnsinoMap = this.audespEnsinoService.getQuadroGeralEnsinoFormatado(codigoIBGE,exercicio,12);
//        this.apontamentoFOMap = this.apontamentosFOService.getApontamentosFO(codigoIBGE,exercicio);
//        this.audespDividaAtivaMap = this.audespDividaAtivaService.getDividaAtivaFormatado(codigoIBGE,exercicio,12);
//        this.audespFase3QuadroDePessoalMap = this.audespFase3Service.getQuadroDePessoal(audespEntidade.getEntidadeId(), exercicio);
//        this.valorInvestimentoMunicipioMap = this.audespBiService.getValorInvestimentoMunicipio(codigoIBGE,exercicio, 12);
//        this.rclMunicipioDevedoresMap = this.tcespBiService.getRCLMunicipioFormatado(codigoIBGE, exercicio);
        Integer quantidadeAlertasDesajusteExecucaoOrcamentaria = audespAlertasService.getAlertasDesajusteExecucaoOrcamentaria(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasDespesaPessoal = audespAlertasService.getAlertasDespesaComPessoal(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasEducacao = audespAlertasService.getAlertasEducacao(codigoIBGE, exercicio, 4 * quadrimestre);
        Integer quantidadeAlertasSaude = audespAlertasService.getAlertasSaude(codigoIBGE, exercicio, 4 * quadrimestre);
        this.audespResultadoExecucaoOrcamentariaMap = audespResultadoExecucaoOrcamentariaService
        .getAudespResultadoExecucaoOrcamentariaUltimosTresExercicios(this.codigoIBGE, exercicio, 4 * quadrimestre);
        this.limiteLRFMap = audespLimiteLrfService
                .getAudespResultadoInfluenciaOrcamentarioFinanceiro(codigoIBGE, exercicio, 4 * quadrimestre);


        String quadrimestreTitulo = getQuadrimestreTitulo(quadrimestre);
        String quadrimestreTituloAno = getQuadrimestreTituloComAno(quadrimestre);


        // margin
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        CTPageMar pageMar = sectPr.addNewPgMar();
        pageMar.setLeft(BigInteger.valueOf(1700L));
        pageMar.setRight(BigInteger.valueOf(1700L));
        pageMar.setBottom(BigInteger.valueOf(1700L));

        CTBody body = document.getDocument().getBody();

        if (!body.isSetSectPr()) {
            body.addNewSectPr();
        }
        CTSectPr section = body.getSectPr();

        if(!section.isSetPgSz()) {
            section.addNewPgSz();
        }
        CTPageSz pageSize = section.getPgSz();

        pageSize.setW(BigInteger.valueOf(595*20));
        pageSize.setH(BigInteger.valueOf(842*20));

        createDocumentStyles();

        getHeader();

        addParagrafo(new TextoFormatado("RELATÓRIO DE FISCALIZAÇÃO\nPREFEITURA MUNICIPAL", formatacaoFactory.getBoldCenter(12)));
        addBreak();
        addBreak();

        addTabelaDadosIniciais();

        addBreak();
        addSaudacao();
        addBreak();
        addBreak();

        addParagrafo(addTab().concat("Este relatório consolida o resultado do acompanhamento das informações " +
                        "prestadas a esta e. Corte de Contas pelo órgão, no período em epígrafe.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(addTab().concat("Em atendimento ao TC-A-30973/026/00, registramos a notificação do(s) Sr.(s). ",
                        formatacaoFactory.getJustificado(12))
                        .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(", responsável(is) pelas contas em exame.", formatacaoFactory.getJustificado(12))
                );

        addParagrafo(addTab().concat("Consignamos os dados e índices considerados relevantes para um diagnóstico" +
                        " inicial do município:",
                        formatacaoFactory.getJustificado(12)));

        addTabelaDescricaoFonteDado();

        addBreak();

        addParagrafo(new TextoFormatado("*POPULAÇÃO: buscar a última informação da população estimada para o " +
                "exercício em exame no site do IEG-M ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
                .concat("http://iegm.tce.sp.gov.br/", formatacaoFactory.getBoldItalicUnderlineAzulJustificado(12))
                .concat(" ou no site do IBGE ", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
                .concat("https://cidades.ibge.gov.br/", formatacaoFactory.getBoldItalicUnderlineAzulJustificado(12))
                .concat(" , se disponível.", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("*ARRECADAÇÃO de todo o município, para guardar coerência com o IEG-M. " +
                        "Informar a do exercício anterior, ou seja, valor de um ano “fechado”, tendo em vista ser um " +
                        "dos parâmetros para enquadramento na Validação.",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(addTab().concat("Informamos que o município possui a seguinte série histórica de" +
                        " classificação no Índice de Efetividade da Gestão Municipal-IEG-M:",
                        formatacaoFactory.getJustificado(12)));
        addTabelaIegm();
        addTextoStatusFaseIEGM();

        addParagrafo(new TextoFormatado("O IEG-M INSERIDO NO EXERCÍCIO ANTERIOR SERÁ AQUELE APURADO APÓS A " +
                "VERIFICAÇÃO/VALIDAÇÃO DA FISCALIZAÇÃO.\n CASO NÃO TENHA SIDO CONCLUÍDA A VALIDAÇÃO, CONSTAR O ITEM" +
                        " COMO PREJUDICADO E INSERIR A OBSERVAÇÃO CONSTANTE NA ALTERNATIVA DO QUADRO ACIMA.\n" +
                        "\n",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("A FISCALIZAÇÃO PODE APENAS MENCIONAR OS ÍNDICES EM RELATÓRIO, SENDO " +
                "VEDADA A DIVULGAÇÃO AOS JURISDICIONADOS DOS ÍNDICES AINDA NÃO CHANCELADOS E TORNADOS PÚBLICOS " +
                "PELA DIREÇÃO DA CASA.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("A Prefeitura analisada obteve, nos 03 (três) últimos exercícios" +
                        " apreciados, os seguintes ",
                formatacaoFactory.getJustificado(12))
                .concat("PARECERES", formatacaoFactory.getBold(12))
                .concat(" na apreciação de suas contas:", formatacaoFactory.getJustificado(12))
        );

        addTabelaPareceres();

        addBreak();
        addParagrafo(addTab().concat("A partir de tais premissas, a Fiscalização planejou a execução de seus " +
                        "trabalhos, agregando a análise das seguintes fontes documentais:",
                formatacaoFactory.getJustificado(12)));

        addAnaliseFontesDocumentais();

        addBreak();

        //############################################
        //# DETALHES PARECERES EXERCICIOS ANTERIORES #
        //############################################


        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": UTILIZAR SE FOR O CASO DO 2º QUADRIMESTRE",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addParagrafo(addTab().concat("O relatório do 1º quadrimestre está colacionado no evento ",
                formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" destes autos.", formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": UTILIZAR EM TODOS OS QUADRIMESTRES",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("O presente relatório quadrimestral visa contribuir para a tomada de " +
                        "providências dentro do próprio exercício, possibilitando a correção de eventuais falhas, " +
                        "resultando numa melhoria das contas apresentadas.",
                        formatacaoFactory.getJustificado(12)));
        addParagrafo(addTab().concat("Saliente-se, por oportuno, que os dados poderão ser reavaliados quando da " +
                        "fiscalização do 3º quadrimestre (fechamento do exercício), oportunidade em que todos os balanços" +
                        " contábeis estarão encerrados.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("Juntar apenas documentos/relatórios para comprovar itens irregulares.",
                        formatacaoFactory.getJustificadoVermelhoCinza(12)));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA A: PLANEJAMENTO", formatacaoFactory.getBold(12)), heading1);

        addBreak();
        addSecao(new TextoFormatado("A.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addSecao(new TextoFormatado("A.1.1. CONTROLE INTERNO",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();

        TextoFormatado aquiSeraoTrazidas = new TextoFormatado("AQUI SERÃO TRAZIDAS CONSTATAÇÕES RELEVANTES" +
                " SOBRE O CONTROLE INTERNO E SUAS ATRIBUIÇÕES",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12));
        addParagrafo(aquiSeraoTrazidas);
        addBreak();

        TextoFormatado buscarAferir = new TextoFormatado("BUSCAR AFERIR SE O CONTROLE INTERNO TÊM EXERCIDO " +
                "DE MANEIRA EFETIVA SUAS ATRIBUIÇÕES NO PERÍODO",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12));
        addParagrafo(buscarAferir);
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("A.2. IEG-M – I-PLANEJAMENTO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        TextoFormatado verOrientacoes = new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
                .concat("PM-ORD.-FECH.-VALID.(MODELO DE FECHAMENTO)",
                        formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12));
        addParagrafo(verOrientacoes);
        addBreak();

        TextoFormatado hipoteseSeNaoForem = new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E UTILIZAR" +
                                " O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12));
        addParagrafo(hipoteseSeNaoForem);
        addBreak();

        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa " +
                "dimensão do IEG-M.", formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                        " que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                "ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto de " +
                        "apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA B: GESTÃO FISCAL", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addSecao(new TextoFormatado("B.1. CUMPRIMENTO DE DETERMINAÇÕES CONSTITUCIONAIS E LEGAIS",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();
        addParagrafo(addTab().concat("Face ao contido no art. 1º, § 1º da Lei Complementar Federal nº 101, de " +
                        "4 de maio de 2000 (Lei de Responsabilidade Fiscal), o qual estabelece os pressupostos da " +
                        "responsabilidade da gestão fiscal, passamos a expor o que segue. ",
                formatacaoFactory.getJustificado(12)));
        addBreak();
        addSecao(new TextoFormatado("B.1.1. RESULTADO DA EXECUÇÃO ORÇAMENTÁRIA NO PERÍODO",
                formatacaoFactory.getBold(12)), heading3);

        addBreak();
        addTabelaExecucaoOrcamentaria();

        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO: CONSIDERAR OS DADOS ISOLADOS DA PREFEITURA, CONFORME APURADO " +
                "PELO SISTEMA Audesp, NÃO DEVENDO SER INCLUÍDAS ADMININISTRAÇÃO INDIRETA, FUNDOS PREVIDÊNCIA ETC.",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("Em caso de situação desfavorável, documentar nos autos, e mencionar a " +
                "emissão de alertas, conforme segue.",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp",
                        formatacaoFactory.getBold(12))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Com base nos dados gerados pelo Sistema Audesp, conforme retro apurado, o " +
                        "resultado da execução orçamentária da Prefeitura no período evidenciou um déficit.",
                        formatacaoFactory.getJustificado(12)));

        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, I, da Lei de Responsabilidade Fiscal, " +
                        "o Município foi alertado tempestivamente, por ",
                        formatacaoFactory.getJustificado(12))
                        .concat(quantidadeAlertasDesajusteExecucaoOrcamentaria.toString(), formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(" vezes, sobre desajustes em sua execução orçamentária.", formatacaoFactory.getJustificado(12))
                );

        addParagrafo(new TextoFormatado("OBSERVAR A ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
                .concat("DATA DE EMISSÃO DO ALERTA", formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(" (CONSTANTE NO FINAL DO DOCUMENTO “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, " +
                        "VISTO QUE, NORMALMENTE OS DO FINAL DO EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, " +
                        "SEM EFEITO. FACE À ANÁLISE REALIZADA PELO SISTEMA Audesp, EM REGRA CONSIDERAR APENAS AS " +
                        "ANÁLISES DA RECEITA E DESPESA.", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)) );


        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.1.2. ANÁLISE DOS LIMITES E CONDIÇÕES DA LEI DE RESPONSABILIDADE FISCAL",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO À NT SDG Nº 141, QUANDO DA APURAÇÃO DA RCL, EM ESPECIAL " +
                "EM CASOS DE DESCUMPRIMENTO DOS LIMITES DA LRF.",
                formatacaoFactory.getBoldItalicUnderlineCapsVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineCapsVermelhoAmareloJustificado(12))
                .concat(": CASO ATENDIDOS OS LIMITES ESTABELECIDOS NA LRF USAR O CONTEÚDO ADIANTE.",
                        formatacaoFactory.getBoldItalicCapsVermelhoAmareloJustificado(12)));
        addBreak();


        addParagrafo(addTab().concat("No período, as análises automáticas não identificaram descumprimentos aos" +
                        " limites estabelecidos na Lei de Responsabilidade Fiscal, quanto à Dívida Consolidada " +
                        "Líquida, Concessões de Garantias e Operações de Crédito, inclusive ARO.",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineCapsVermelhoAmareloJustificado(12))
                .concat(": CASO NÃO ATENDIDO ALGUM DOS LIMITES ESTABELECIDOS NA LRF USAR O CONTEÚDO ADIANTE.",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addTabelaComparativoLimiteLRF();

        addBreak();

        addParagrafo(addTab().concat("Verificamos o não atendimento aos limites estabelecidos pela Lei de" +
                        " Responsabilidade Fiscal, isso em decorrência do que segue: ",
                formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("DESCREVER AS IRREGULARIDADES",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addBreak();



        addSecao(new TextoFormatado("B.1.2.1. DESPESA DE PESSOAL",
                formatacaoFactory.getBold(12)), heading4);
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": REGULARIDADE NOS 1º e 2º QUADRIMESTRES",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addBreak();

        addParagrafo(addTab().concat("Conforme Relatórios de Gestão Fiscal emitidos pelo Sistema Audesp, referentes ao ",
                        formatacaoFactory.getJustificado(12))
                        .concat("1º e/ou 2º quadrimestres", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(" do exercício analisado, é possível ver que o Poder Executivo atendeu ao limite " +
                                "da despesa de pessoal previsto no art. 20, III, alínea “b” da Lei de " +
                                "Responsabilidade Fiscal.", formatacaoFactory.getJustificado(12))
                );

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": HAVENDO DESRESPEITO AO LIMITE PARA DESPESAS DE PESSOAL",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("1º QUADRIMESTRE",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addTabelaDespesaDePessoalPrimeiroQuadrimestre();

        addBreak();
        addParagrafo(new TextoFormatado("2º QUADRIMESTRE",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addTabelaDespesaDePessoalSegundoQuadrimestre();

        addBreak();
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO:",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("NÃO É NECESSÁRIO DIGITAR O SINAL DE MENOS NAS EXCLUSÕES",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("ATENÇÃO QUANTO AO AJUSTE DO MÊS DE DEZEMBRO DO EXERCÍCIO ANTERIOR. " +
                "SEMPRE BUSCAR SEGUIR O APURADO PELA FISCALIZAÇÃO ANTERIOR. ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("IMPORTANTE: VERIFICAR A APLICABILIDADE DO ART. 66 DA LRF, DE DUPLICAÇÃO " +
                "DO PRAZO DE RECONDUÇÃO EM CASOS DE BAIXO CRESCIMENTO DO PIB ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": ACIMA DE 95% DE 54% (51,30%), QUANDO SE INICIAM AS VEDAÇÕES DA LRF",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Diante dos elementos apurados acima, verificamos que a despesa total com " +
                        "pessoal não superou o limite previsto no art. 20, III, da Lei de Responsabilidade Fiscal, " +
                        "porém ultrapassou aquele previsto no art. 22, parágrafo único, da Lei supracitada, nos ",
                formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" quadrimestres.", formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Constatamos a infringência do inciso ",
                formatacaoFactory.getJustificado(12))
                .concat("XX", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(", do citado dispositivo, tendo em vista que ", formatacaoFactory.getJustificado(12))
                .concat("relatar as ocorrências.", formatacaoFactory.getJustificadoVermelhoCinza(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal, o " +
                        "Executivo Municipal foi alertado tempestivamente, por  ",
                        formatacaoFactory.getJustificado(12))
                        .concat(quantidadeAlertasDespesaPessoal.toString(), formatacaoFactory.getJustificado(12))
                        .concat(" vezes, quanto à superação de 90% do específico limite da despesa laboral.",
                                formatacaoFactory.getJustificado(12))
                );

        addParagrafo(new TextoFormatado("OBSERVAR A DATA DE EMISSÃO DO ALERTA (CONSTANTE NO FINAL DO DOCUMENTO" +
                " “NOTIFICAÇÃO DE ALERTA”), PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO FINAL DO " +
                "EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": ACIMA DE 54% NO QUADRIMESTRE",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();



        addParagrafo(addTab().concat("É possível ver que a superação do limite da despesa laboral aconteceu no " +
                        "último quadrimestre do exercício, significando ",
                formatacaoFactory.getJustificado(12))
                .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat("% da Receita Corrente Líquida.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(addTab().concat("Com base no art. 59, § 1º, II, da Lei de Responsabilidade Fiscal, " +
                        "o Executivo Municipal" +
                        " foi alertado tempestivamente, por ",
                formatacaoFactory.getJustificado(12))
                .concat("", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(quantidadeAlertasDespesaPessoal + " vezes, quanto à superação de 90% do específico limite" +
                                " da despesa laboral.",
                        formatacaoFactory.getJustificado(12))
        );

        addParagrafo(new TextoFormatado("OBSERVAR A ",
                formatacaoFactory.getBoldItalicCapsVermelhoAmareloJustificado(12))
                .concat("DATA DE EMISSÃO DO ALERTA ",
                        formatacaoFactory.getBoldItalicUnderlineCapsVermelhoAmareloJustificado(12))
                .concat("(CONSTANTE NO FINAL DO DOCUMENTO “NOTIFICAÇÃO DE ALERTA”)," +
                                " PARA CONSIDERÁ-LO TEMPESTIVO, VISTO QUE, NORMALMENTE OS DO FINAL DO" +
                                " EXERCÍCIO SÃO EMITIDOS JÁ NO ANO SEGUINTE, PORTANTO, SEM EFEITO.",
                        formatacaoFactory.getBoldItalicCapsVermelhoAmareloJustificado(12)));
        addBreak();

        addBreak();

        addSecao(new TextoFormatado("B.1.3. PRECATÓRIOS",
                formatacaoFactory.getBold(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAR A NOTA TÉCNICA SDG Nº 142, INCLUSIVE PARA NOTICIAR EVENTUAL " +
                "OCORRÊNCIA NO QUADRIMESTRE SOB ANÁLISE, DE:\n- adesão irregular à nova sistemática trazida pelo EC " +
                "nº 99/2017, considerando que não podem aderir aqueles que estavam em dia com o pagamento (ou " +
                "seja, já enquadráveis no Regime Ordinário);\n- eventuais informações de irregularidades emitidas " +
                "pelo TJ, inclusive quanto ao percentual insuficiente ao pagamento;\n- eventuais irregularidades " +
                "quanto à gestão das fontes adicionais para pagamentos (criação de fundos " +
                "garantidores etc.);\n- desapropriação nos casos vedados;\n- ausência de regulamentação da lei no " +
                "prazo de 120 dias de 1/1/2018, ou seja, 1/5/2018.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));


        addParagrafo(new TextoFormatado("NÃO HAVENDO OCORRÊNCIAS DIGNAS DE NOTA, EXCLUIR O ITEM NO QUADRIMESTRE, " +
                "DEIXANDO SUA ANÁLISE SOMENTE PARA O FECHAMENTO DO EXERCÍCIO.",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));

        addBreak();
        addBreak();

        addSecao(new TextoFormatado("B.2. IEG-M – I-FISCAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo " +
                "PM-ORD.-FECH.-VALID.(MODELO DE FECHAMENTO)",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE: ",
                formatacaoFactory.getBoldItalicUnderlineCapsVermelhoAmareloJustificado(12))
                .concat("SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS, UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa" +
                " dimensão do IEG-M.", formatacaoFactory.getJustificado(12)));
        addBreak();

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                " que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                "ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto de " +
                        "apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();
        addBreak();

        addBreak();

        addSecao(new TextoFormatado("B.3. OUTROS PONTOS DE INTERESSE ",
                formatacaoFactory.getBold(12)).concat("(manter apenas em caso de ocorrência. " +
                "Senão excluir item)", formatacaoFactory.getBoldJustificadoFundoAmarelo(12)), heading3);
        addBreak();

        addParagrafo(new TextoFormatado("OUTRAS ANÁLISES, PREVISTAS NO ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
                .concat("MODELO DE FECHAMENTO, APENAS",
                        formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(" COMPORÃO O RELATÓRIO EM CASO DE ",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
                .concat("IRREGULARIDADES CONSTATADAS POR MEIO DO IEG-M, DENÚNCIAS FORMALIZADAS PERANTE ESTE " +
                                "TCESP, HISTÓRICO DO ÓRGÃO DE DESVIOS OU MALVERSAÇÃO DE RECURSOS",
                        formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(" ACHADOS CUJA RELEVÂNCIA/MATERIALIDADE JUSTIFIQUEM A ATUAÇÃO DO TCESP ETC., " +
                                "CONSIDERANDO QUE O INTUITO DA FISCALIZAÇÃO É EFETUAR ANÁLISES FINALÍSTICAS.",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))

        );

        addParagrafo(new TextoFormatado("PARA TANTO, VERIFICAR MODELOS/SUGESTÕES NO APÊNDICE AO ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
                .concat("MODELO DE FECHAMENTO.",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA C: ENSINO", formatacaoFactory.getBold(12)), heading1);

        addBreak();

        addSecao(new TextoFormatado("C.1. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(addTab().concat("A aplicação de recursos, no período, conforme informado ao Sistema Audesp, " +
                        "apresentou os seguintes resultados:",
                        formatacaoFactory.getJustificado(12)));

        addTabelaDespesasCfFundeb();

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": Em caso de situação desfavorável, documentar nos autos e mencionar a emissão de alertas, conforme segue.",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("Caso sejam realizados ajustes nos índices, relatar.",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp",
                formatacaoFactory.getBoldItalicJustificado(12))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getItalic(12)));
        addBreak();

        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, ",
                        formatacaoFactory.getJustificado(12))
        .concat("foi o Município alertado", formatacaoFactory.getBold(12))
        .concat(", por ", formatacaoFactory.getJustificado(12))
        .concat( "" + quantidadeAlertasEducacao , formatacaoFactory.getJustificadoVermelhoCinza(12))
        .concat(" vezes, consoante Notificações de Alertas juntados no presente evento.",
                formatacaoFactory.getJustificado(12))
        );

        addBreak();

        addParagrafo(new TextoFormatado("ANÁLISE OBRIGATÓRIA",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": OFERTA DE VAGAS NO ENSINO",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));


        addParagrafo(new TextoFormatado("OBSERVAÇÃO: A VERIFICAÇÃO INICIAL DEVERÁ SER EFETUADA " +
                "NA VISITA DO 1º QUADRIMESTRE DE ACOMPANHAMENTO 2019 OU NO FECHAMENTO DE 2018 (ORDINÁRIAS" +
                " E FECHAMENTOS DE ACOMPANHAMENTOS 2018). O ASSUNTO TAMBÉM DEVERÁ CONSTAR DOS RELATÓRIOS" +
                " DE FECHAMENTO DOS ACOMPANHAMENTOS DE 2019 E ORDINÁRIAS A SEREM REALIZADOS NO EXERCÍCIO DE 2020.",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                );

        addBreak();
        addParagrafo(addTab().concat("Não obstante os percentuais apurados, a fiscalização colheu in loco " +
                        "informações sobre a situação da oferta de vagas escolares, com discriminação por faixas " +
                        "etárias, conforme consta da tabela adiante:",
                        formatacaoFactory.getJustificado(12)));
        addBreak();

        addTabelaVagasEscolares();

        addBreak();

        addParagrafo(new TextoFormatado("NÃO SENDO CONSTATADO DÉFICIT EM QUALQUER DOS NÍVEIS DA TABELA ACIMA" +
                "USAR O TEXTO ADIANTE:", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Na verificação das informações fornecidas pelo setor de educação do município, " +
                        "não constatamos a ocorrência de déficit em qualquer dos níveis de ensino discriminados " +
                        "na tabela acima.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("NO CASO DE DÉFICIT ENTRE DEMANDA E OFERTA DE VAGAS, DEVERÁ A " +
                "FISCALIZAÇÃO VERIFICAR:", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(new TextoFormatado("- A RESPOSTA DADA À SEGUINTE QUESTÃO QUE CONSTA DO QUESTIONÁRIO " +
                "DO IEG-M – PERSPECTIVA I-EDUC : A PREFEITURA MUNICIPAL FEZ UMA PESQUISA/ESTUDO PARA LEVANTAR" +
                " O NÚMERO DE CRIANÇAS QUE NECESSITAVAM DE CRECHES, PRÉ-ESCOLA OU ENSINO FUNDAMENTAL?",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("- QUAIS AS MEDIDAS TEM SIDO ADOTADAS PELA PREFEITURA PARA ZERAR O " +
                "DÉFICIT APURADO;",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("- SE HÁ PROJETOS NAS PEÇAS DE PLANEJAMENTO QUE COMTEMPLEM OBRAS DE " +
                "CONTRUÇÃO OU AMPLIAÇÃO DE CRECHES OU ESCOLAS;",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("- SE HÁ OBRAS PARA CONSTRUÇÃO DE CRECHES OU ESCOLAS QUE ESTEJAM " +
                "ATRASADAS OU PARALISADAS, TRAZENDO NOTÍCIAS SOBRE QUAIS AS CAUSAS DE TAL SITUAÇÃO.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addSecao(new TextoFormatado("C.2. IEG-M – I-EDUC",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo " +
                "PM-ORD.-FECH.-VALID.(MODELO DE FECHAMENTO) ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E" +
                                " UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                " que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                "ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto de " +
                        "apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("É possível consultar no link ",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat("https://pne.tce.mg.gov.br/#/public/inicio",
                        formatacaoFactory.getBoldItalicUnderlineAzulJustificado(12))
                .concat(" se os municípios atingiram as metas previstas no Plano Nacional de Educação-PNE. " +
                                "Os resultados se referem ao exercício de 2016.",
                        formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));
        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA D: SAÚDE", formatacaoFactory.getBold(12)), heading1);
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("D.1. APLICAÇÃO POR DETERMINAÇÃO CONSTITUCIONAL E LEGAL",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();



        addParagrafo(addTab().concat("Conforme informado ao Sistema Audesp, a aplicação na Saúde atingiu, no " +
                        "período, os seguintes resultados:",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addTabelaEmpenhadaLiquidadaPaga();

        addBreak();


        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": Em caso de situação desfavorável, documentar nos autos e mencionar a emissão " +
                                "de alertas, conforme segue.",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(new TextoFormatado("Caso sejam realizados ajustes nos índices, relatar.",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("Dados extraídos do Sistema Audesp",
                formatacaoFactory.getBold(12))
                .concat(": Relatório de Instrução juntado neste evento.", formatacaoFactory.getJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Nos termos do art. 59, § 1º, V, da Lei de Responsabilidade Fiscal, ",
                formatacaoFactory.getJustificado(12))
                .concat(" foi o Município foi alertado", formatacaoFactory.getBold(12))
                .concat(", por ", formatacaoFactory.getJustificado(12))
                .concat(quantidadeAlertasSaude.toString(), formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" vezes, consoante Notificações de Alertas juntados no presente evento.", formatacaoFactory.getJustificado(12))
        );


        addBreak();
        addBreak();

        addSecao(new TextoFormatado("D.2. IEG-M – I-SAÚDE",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo " +
                "PM-ORD.-FECH.-VALID.(MODELO DE FECHAMENTO) ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E" +
                                " UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                " que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                "ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto de " +
                        "apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();


        addSecao(new TextoFormatado("PERSPECTIVA E: GESTÃO AMBIENTAL",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("E.1. IEG-M – I-AMB",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo " +
                "PM-ORD.-FECH.-VALID.(MODELO DE FECHAMENTO) ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E" +
                                " UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                " que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                "ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto de " +
                        "apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addSecao(new TextoFormatado("PERSPECTIVA F: GESTÃO DA PROTEÇÃO À CIDADE",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("F.1. IEG-M – I-CIDADE",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo " +
                "PM-ORD.-FECH.-VALID.(MODELO DE FECHAMENTO) ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E" +
                                " UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                " que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                "ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto de " +
                        "apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();



        addSecao(new TextoFormatado("PERSPECTIVA G: TECNOLOGIA DA INFORMAÇÃO",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("G.1. FIDEDIGNIDADE DOS DADOS INFORMADOS AO SISTEMA Audesp",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": SEM DIVERGÊNCIAS",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Nos trabalhos da fiscalização não foram encontradas divergências entre os " +
                        "dados da origem e os prestados ao Sistema Audesp.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": COM DIVERGÊNCIAS",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Como demonstrado no(s) item(ns) ",
                formatacaoFactory.getJustificado(12))
                .concat("xxx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                .concat(" deste relatório, foram constatadas divergências entre os dados informados " +
                                "pela origem e aqueles apurados no Sistema Audesp.",
                        formatacaoFactory.getJustificado(12))
        );

        addBreak();
        addSecao(new TextoFormatado("G.2. IEG-M – I-GOV TI",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("Ver orientações do Apêndice II, ao final do Modelo " +
                "PM-ORD.-FECH.-VALID.(MODELO DE FECHAMENTO) ",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": SE NÃO FOREM DETECTADAS AS OCORRÊNCIAS ACIMA, EXCLUIR O CONTEÚDO E" +
                                " UTILIZAR O SEGUINTE TEXTO:",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Sob amostragem, não constatamos ocorrências dignas de nota nessa dimensão do IEG-M.",
                formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, CASO NÃO HAJA MOTIVOS PARA ANÁLISE NO QUADRIMESTRE.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade" +
                " que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU, PARA O 2º QUADRIMESTRE, CASO JÁ ABORDADO NO 1º.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade que " +
                "ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre. Não obstante, ressaltamos que a matéria foi objeto de " +
                        "apontamento no quadrimestre anterior.", formatacaoFactory.getJustificado(12)));

        addBreak();



        addSecao(new TextoFormatado("PERSPECTIVA H: OUTROS ASPECTOS RELEVANTES",
                formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addSecao(new TextoFormatado("H.1. DENÚNCIAS/REPRESENTAÇÕES/EXPEDIENTES",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();



        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": NA INEXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();
        addParagrafo(addTab().concat("Não chegou ao nosso conhecimento a formalização de denúncias, " +
                        "representações ou expedientes.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();
        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": NA EXISTÊNCIA DE DENÚNCIAS /REPRESENTAÇÕES/ EXPEDIENTES",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("As denúncias / representações / expedientes serão tratados no relatório do 3º " +
                        "quadrimestre do exercício em exame, tendo em vista que, no momento, não concluímos " +
                        "a análise da matéria.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OU", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("Está referenciado ",
                        formatacaoFactory.getJustificado(12))
        .concat("OU", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12))
        .concat(" Estão referenciados ao presente processo de contas anuais, o(s) seguinte(s) protocolado(s):",
                formatacaoFactory.getJustificado(12)) );


        addBreak();

        addTabelaProcessoDeContasAnuais();

        addBreak();

        addParagrafo(addTab().concat("O(s) assunto(s) em tela foi(ram) tratado(s) no item(ns) ",
                        formatacaoFactory.getJustificado(12))
                        .concat("xx", formatacaoFactory.getJustificadoVermelhoCinza(12))
                        .concat(" deste relatório.", formatacaoFactory.getJustificado(12))
                );

        addBreak();

        addSecao(new TextoFormatado("H.2. ATENDIMENTO À LEI ORGÂNICA, INSTRUÇÕES E RECOMENDAÇÕES DO TRIBUNAL " +
                "DE CONTAS DO ESTADO DE SÃO PAULO",
                formatacaoFactory.getBold(12)), heading2);
        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": ATENDIMENTO, ADAPTAR CONFORME O CASO",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Não constatamos, no período, desatendimento à Lei Orgânica, Instruções, " +
                        "e/ou recomendações deste Tribunal.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": CASOS NÃO ATENDIDOS, ADAPTAR CONFORME O CASO",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("Constatamos, no período, desatendimento à Lei Orgânica e às Instruções deste " +
                        "Tribunal, tendo em vista que:",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("Explicitar e documentar as falhas.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("e/ou", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO: Pode-se, desde as análises quadrimestrais, " +
                "apontar desatendimento às recomendações.", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addParagrafo(addTab().concat("Haja vista os dois últimos exercícios apreciados, verificamos que, no " +
                        "período ora em análise, a Prefeitura descumpriu as seguintes recomendações/determinações deste Tribunal:",
                        formatacaoFactory.getJustificado(12)));


        ParecerPrefeitura ultimo = pareceresPrefeiturasList.size() > 0 ? pareceresPrefeiturasList.get(pareceresPrefeiturasList.size()-1) : null;
        ParecerPrefeitura penultimo = pareceresPrefeiturasList.size() > 1 ? pareceresPrefeiturasList.get(pareceresPrefeiturasList.size()-2) : null;

        addTabelaDoisUltimosExerciciosApreciados(ultimo);

        addBreak();

        addTabelaDoisUltimosExerciciosApreciados(penultimo);

        addBreak();


        addBreak();

        addParagrafo(new TextoFormatado("e/ou", formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("OBSERVAÇÃO: Pode-se, desde as análises quadrimestrais, apontar até " +
                "mesmo a perspectiva de desatendimento às recomendações. Isso poderá ocorrer especialmente nos casos " +
                "de itens que se concluem no final do exercício; mas que, ante o pontualmente constatado, tem-se a" +
                " perspectiva de seu descumprimento (p. ex., déficits orçamentário e financeiro). Assim, o relatório" +
                " quadrimestral reforçará o alerta, com a recomendação expedida.",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addParagrafo(addTab().concat("Haja vista os dois últimos exercícios apreciados, verificamos que, face às " +
                        "constatações do período ora em análise, a Prefeitura descumprirá as seguintes " +
                        "recomendações/determinações deste Tribunal:",
                        formatacaoFactory.getJustificado(12)));

        addTabelaDoisUltimosExerciciosApreciados(ultimo);

        addBreak();

        addTabelaDoisUltimosExerciciosApreciados(penultimo);

        addBreak();

        addParagrafo(new TextoFormatado("HIPÓTESE",
                formatacaoFactory.getBoldItalicUnderlineVermelhoAmareloJustificado(12))
                .concat(": CASOS NÃO ELEITO NO PLANEJAMENTO, ADAPTAR CONFORME O CASO",
                        formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));
        addBreak();

        addParagrafo(addTab().concat("No planejamento da fiscalização, não vislumbramos relevância/materialidade " +
                        "que ensejasse o exame ", formatacaoFactory.getJustificado(12))
                .concat("in loco", formatacaoFactory.getItalic(12))
                .concat(" do item neste quadrimestre.", formatacaoFactory.getJustificado(12)));
        addBreak();
        addBreak();

        addSecao(new TextoFormatado("CONCLUSÃO", formatacaoFactory.getBold(12)), heading1);
        addBreak();

        addParagrafo(addTab().concat("Com relação aos assuntos tratados neste relatório, destacamos:",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(new TextoFormatado("Descrever resumidamente as falhas",
                formatacaoFactory.getBoldItalicVermelhoAmareloJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat("À consideração de Vossa Senhoria.",
                        formatacaoFactory.getJustificado(12)));

        addBreak();

        addParagrafo(addTab().concat(tabelasProtocolo.getSecaoFiscalizadoraContas().trim() +
                        ", em ... de ............ de 2019.",
                        formatacaoFactory.getJustificado(12)));


        addBreak();

        addParagrafo(new TextoFormatado("Nome\nAgente da Fiscalização",
                        formatacaoFactory.getBoldItalicCenter(12)));


        return sendFile();

    }

    private void addTextoStatusFaseIEGM() {
        String textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        Integer flagStatusIEGM = resultadoIegm2018.getFlagStatusFase();
        if( flagStatusIEGM == 1) {
            textoFlagStatusIEGM = "Índices do exercício em exame em planejamento, " +
                    "dados podem sofrer alterações.";
        } else if(flagStatusIEGM == 2) {
            textoFlagStatusIEGM = "Índices do exercício em exame em verificação/validação da Fiscalização, " +
                    "dados podem sofrer alterações.";
        } else { // flagStatus == 3
            textoFlagStatusIEGM = "Índices do exercício em exame após verificação/validação da Fiscalização.";
        }
        addParagrafo(new TextoFormatado(textoFlagStatusIEGM,
                formatacaoFactory.getItalic(12)));
    }

}