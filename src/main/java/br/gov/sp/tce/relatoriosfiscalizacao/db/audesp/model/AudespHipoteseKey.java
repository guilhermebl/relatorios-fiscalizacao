package br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AudespHipoteseKey implements Serializable {

    @Column(name = "hipotese_id")
    private Integer idHipotese;

    @Column(name = "item_analise_id")
    private Integer idItemAnalise;

    @Column(name = "entidade_id")
    private Integer idEntidade;

    @Column(name = "ano_exercicio")
    private Integer exercicio;

    @Column(name = "mes_referencia")
    private Integer mes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AudespHipoteseKey)) return false;
        AudespHipoteseKey that = (AudespHipoteseKey) o;
        return Objects.equals(idHipotese, that.idHipotese) &&
                Objects.equals(idItemAnalise, that.idItemAnalise) &&
                Objects.equals(idEntidade, that.idEntidade) &&
                Objects.equals(exercicio, that.exercicio) &&
                Objects.equals(mes, that.mes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idHipotese, idItemAnalise, idEntidade, exercicio, mes);
    }
}

