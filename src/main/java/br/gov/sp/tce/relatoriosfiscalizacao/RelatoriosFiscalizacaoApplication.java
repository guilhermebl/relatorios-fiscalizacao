package br.gov.sp.tce.relatoriosfiscalizacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.Security;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@SpringBootApplication
public class RelatoriosFiscalizacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RelatoriosFiscalizacaoApplication.class, args);
	}
}