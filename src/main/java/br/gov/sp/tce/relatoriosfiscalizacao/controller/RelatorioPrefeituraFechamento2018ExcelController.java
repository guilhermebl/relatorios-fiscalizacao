package br.gov.sp.tce.relatoriosfiscalizacao.controller;

import br.gov.sp.tce.relatoriosfiscalizacao.service.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.tags.Param;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Map;

@RestController
public class RelatorioPrefeituraFechamento2018ExcelController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private XSSFWorkbook xssfWorkbook;

    private Integer exercicio;

    private Integer codigoIBGE;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private AudespDespesaPessoalService audespDespesaPessoalService;

    private Map<String, BigDecimal> audespDespesaPessoalMap;

    @Autowired
    private AudespResultadoExecucaoOrcamentariaService audespResultadoExecucaoOrcamentariaService;

    @Autowired
    private TcespBiService tcespBiService;

    @Autowired
    private AudespEnsinoService audespEnsinoService;

    @Autowired
    private AudespSaudeService audespSaudeService;

    @Autowired
    private AudespDividaAtivaService audespDividaAtivaService;

    private Map<String, BigDecimal> audespResultadoExecucaoOrcamentariaMap;
    private Map<String, BigDecimal> audespEquilibrioOrcamentarioReceitasMap;
    private Map<String, BigDecimal> audespEquilibrioOrcamentarioDespesasMap;
    private Map<String, BigDecimal> rclMunicipioDevedoresMap;
    private Map<String, BigDecimal> quadroGeralEnsinoMap;
    private Map<String, BigDecimal> audespSaudeMap;
    private Map<String, BigDecimal> audespDividaAtivaMap;

    public ResponseEntity<Resource> download(ParametroBusca parametroBusca) throws Exception {
        log.info("excel - municipio: " + codigoIBGE);

        this.exercicio = parametroBusca.getExercicio();
        this.codigoIBGE = parametroBusca.getCodigoIBGE();

        audespDespesaPessoalMap = audespDespesaPessoalService.getAudespDespesaPessoalByCodigoIbge(codigoIBGE, exercicio, 50);
        audespResultadoExecucaoOrcamentariaMap = audespResultadoExecucaoOrcamentariaService
                .getAudespResultadoExecucaoOrcamentaria(parametroBusca);
        audespEquilibrioOrcamentarioReceitasMap = audespResultadoExecucaoOrcamentariaService
                .getEquilibrioOrcamentarioReceitas(this.codigoIBGE, exercicio, 12);
        audespEquilibrioOrcamentarioDespesasMap = audespResultadoExecucaoOrcamentariaService
                .getEquilibrioOrcamentarioDespesas(this.codigoIBGE, exercicio, 12);
        rclMunicipioDevedoresMap = this.tcespBiService.getRCLMunicipio(codigoIBGE, exercicio);
        quadroGeralEnsinoMap = this.audespEnsinoService.getQuadroGeralEnsino(codigoIBGE,exercicio,12);

        audespSaudeMap = this.audespSaudeService.getAplicacoesEmSaude(codigoIBGE, exercicio, 12);
        audespDividaAtivaMap = this.audespDividaAtivaService.getDividaAtiva(codigoIBGE,exercicio,12);

        Resource resourceRelatorioExcel = resourceLoader.getResource("classpath:static/xlsx/pre-relatorio-fechamento-pm-2018.xlsx");

        InputStream isRelatorioExcel = resourceRelatorioExcel.getInputStream();

        this.xssfWorkbook = new XSSFWorkbook(isRelatorioExcel);

        addTabelaDespesaDePessoal();
        addTabelaExecucaoOrcamentaria();
        addTabelaEquilibrioOrcamentario();
        addTabelaPrecatorioApuracaoPagamentoPiso();
        addQuadroGeralEnsino();
        addTabelaAplicaoEmSaude();
        addTabelaDividaAtiva();

        XSSFFormulaEvaluator.evaluateAllFormulaCells(xssfWorkbook);

        return sendFile();
    }

    private void addTabelaDespesaDePessoal() {
        XSSFSheet despesaDePessoalSheet = xssfWorkbook.getSheet("Despesa de Pessoal");

        XSSFRow row = despesaDePessoalSheet.getRow(5);
        XSSFCell gastoInformado2017Dez = row.getCell(1);
        gastoInformado2017Dez.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio - 1) + 12));
        XSSFCell gastoInformado2018Abr = row.getCell(2);
        gastoInformado2018Abr.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio) + 4));
        XSSFCell gastoInformado2018Ago = row.getCell(3);
        gastoInformado2018Ago.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio) + 8));
        XSSFCell gastoInformado2018Dez = row.getCell(4);
        gastoInformado2018Dez.setCellValue(getValor(audespDespesaPessoalMap, "vDespPessoalLiq" + (exercicio) + 12));

        XSSFRow rowRCL = despesaDePessoalSheet.getRow(10);
        XSSFCell rcl2017Dez = rowRCL.getCell(1);
        rcl2017Dez.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio - 1) + 12));
        XSSFCell rcl2018Abr = rowRCL.getCell(2);
        rcl2018Abr.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio) + 4));
        XSSFCell rcl2018Ago = rowRCL.getCell(3);
        rcl2018Ago.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio) + 8));
        XSSFCell rcl2018Dez = rowRCL.getCell(4);
        rcl2018Dez.setCellValue(getValor(audespDespesaPessoalMap, "vRCL" + (exercicio) + 12));
    }

    private void addTabelaExecucaoOrcamentaria() {
        XSSFSheet execucaoOrcamentaria = xssfWorkbook.getSheet("Execução Orçamentária");

        XSSFRow rowExecucaoOrcamentaria = execucaoOrcamentaria.getRow(1);
        XSSFCell receitaRealizada = rowExecucaoOrcamentaria.getCell(1);
        receitaRealizada.setCellValue(getValor(audespResultadoExecucaoOrcamentariaMap, "vTotRecRealPM"));

        XSSFRow rowDespesasEmpenhadas = execucaoOrcamentaria.getRow(2);
        XSSFCell despesasEmpenhadas = rowDespesasEmpenhadas.getCell(1);
        despesasEmpenhadas.setCellValue(getValor(audespResultadoExecucaoOrcamentariaMap, "vTotDespEmpBO"));

        XSSFRow rowRepassesDeDuodecimos = execucaoOrcamentaria.getRow(3);
        XSSFCell repassesDeDuodecimos = rowRepassesDeDuodecimos.getCell(1);
        repassesDeDuodecimos.setCellValue(getValor(audespResultadoExecucaoOrcamentariaMap, "vRepDuodCM"));

        XSSFRow rowDevolucaoDeDuodecimos = execucaoOrcamentaria.getRow(4);
        XSSFCell devolucaoDeDuodecimos = rowDevolucaoDeDuodecimos.getCell(1);
        devolucaoDeDuodecimos.setCellValue(getValor(audespResultadoExecucaoOrcamentariaMap, "vDevDuod"));

        XSSFRow rowTransferenciasFinanceiras = execucaoOrcamentaria.getRow(5);
        XSSFCell tranferenciasFinanceiras = rowTransferenciasFinanceiras.getCell(1);
        tranferenciasFinanceiras.setCellValue(getValor(audespResultadoExecucaoOrcamentariaMap, "vTransfFinAdmIndExec"));

    }


    public void addTabelaEquilibrioOrcamentario() {
        XSSFSheet equilibrioOrcamentario = xssfWorkbook.getSheet("Equilíbrio Orçamentário");

        //Receitas
        XSSFRow rowReceitasCorrentes = equilibrioOrcamentario.getRow(1);
        XSSFCell receitaCorrentePrevisao = rowReceitasCorrentes.getCell(1);
        receitaCorrentePrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vRecCorPrev"));
        XSSFCell receitaCorrenteRealizada = rowReceitasCorrentes.getCell(2);
        receitaCorrenteRealizada.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vRecCorReal"));

        XSSFRow rowReceitasDeCapital = equilibrioOrcamentario.getRow(2);
        XSSFCell rowReceitasDeCapitalPrevisao = rowReceitasDeCapital.getCell(1);
        rowReceitasDeCapitalPrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vRecCapPrev"));
        XSSFCell rowReceitasDeCapitalRealizadas = rowReceitasDeCapital.getCell(2);
        rowReceitasDeCapitalRealizadas.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vRecCapReal"));

        XSSFRow rowReceitasIntraorcamentarias = equilibrioOrcamentario.getRow(3);
        XSSFCell receitasIntraorcamentariasPrevisao = rowReceitasIntraorcamentarias.getCell(1);
        receitasIntraorcamentariasPrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vRecIntraOrcPrev"));
        XSSFCell receitasIntraorcamentariasRealizadas = rowReceitasIntraorcamentarias.getCell(2);
        receitasIntraorcamentariasRealizadas.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vRecIntraOrcReal"));

        XSSFRow rowDeducoesDaReceita = equilibrioOrcamentario.getRow(4);
        XSSFCell deducoesDaReceitaPrevisao = rowDeducoesDaReceita.getCell(1);
        deducoesDaReceitaPrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vDedRecPrev") ); //valor vem negativo do audesp
        XSSFCell deducoesDaReceitaRealizacao = rowDeducoesDaReceita.getCell(2);
        deducoesDaReceitaRealizacao.setCellValue(getValor(audespEquilibrioOrcamentarioReceitasMap, "vDedRecReal")); //valor vem negativo do audesp

        // Despesas Empenhadas
        XSSFRow rowDespesasCorrentes = equilibrioOrcamentario.getRow(12);
        XSSFCell despesaCorrentePrevisao = rowDespesasCorrentes.getCell(1);
        despesaCorrentePrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vDespCorFix"));
        XSSFCell despesaCorrenteRealizada = rowDespesasCorrentes.getCell(2);
        despesaCorrenteRealizada.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vDespCorExec"));

        XSSFRow rowDespesasDeCapital = equilibrioOrcamentario.getRow(13);
        XSSFCell rowDespesasDeCapitalPrevisao = rowDespesasDeCapital.getCell(1);
        rowDespesasDeCapitalPrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vDespCapAmortFix"));
        XSSFCell rowDespesasDeCapitalRealizadas = rowDespesasDeCapital.getCell(2);
        rowDespesasDeCapitalRealizadas.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vDespCapAmortExec"));

        XSSFRow rowReservaDeContingencia = equilibrioOrcamentario.getRow(14);
        XSSFCell reservaDeContingenciaFixacao = rowReservaDeContingencia.getCell(1);
        reservaDeContingenciaFixacao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vResCont"));
//        XSSFCell reservaDeContingenciaExecucao = rowReservaDeContingencia.getCell(2);
//        reservaDeContingenciaExecucao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, ""));

        XSSFRow rowDespesasIntraorcamentarias = equilibrioOrcamentario.getRow(15);
        XSSFCell despesasIntraorcamentariasPrevisao = rowDespesasIntraorcamentarias.getCell(1);
        despesasIntraorcamentariasPrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vDespIntraOrcFix"));
        XSSFCell despesasIntraorcamentariasRealizadas = rowDespesasIntraorcamentarias.getCell(2);
        despesasIntraorcamentariasRealizadas.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vDespIntraOrcExec"));

        XSSFRow rowRepassesDeDuodecimos = equilibrioOrcamentario.getRow(16);
        XSSFCell repassesDeDuodecimosPrevisao = rowRepassesDeDuodecimos.getCell(1);
        repassesDeDuodecimosPrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vRepDuodFix"));
        XSSFCell repassesDeDuodecimosExecucao  = rowRepassesDeDuodecimos.getCell(2);
        repassesDeDuodecimosExecucao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vRepDuodExec"));

        XSSFRow rowTranferenciasAdmIndireta = equilibrioOrcamentario.getRow(17);
        XSSFCell tranferenciasAdmIndiretaPrevisao = rowTranferenciasAdmIndireta.getCell(1);
        tranferenciasAdmIndiretaPrevisao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vTransfFinAdmIndFix"));
        XSSFCell tranferenciasAdmIndiretaExecucao = rowTranferenciasAdmIndireta.getCell(2);
        tranferenciasAdmIndiretaExecucao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vTransfFinAdmIndExec"));

        XSSFRow rowDeducaoDevolucaoDuodecimos = equilibrioOrcamentario.getRow(18);
        XSSFCell deducaoDevolucaoDuodecimosExecucao = rowDeducaoDevolucaoDuodecimos.getCell(2);
        deducaoDevolucaoDuodecimosExecucao.setCellValue(getValor(audespEquilibrioOrcamentarioDespesasMap, "vDevDuod") * -1);



    }

    public void addTabelaPrecatorioApuracaoPagamentoPiso() {
        XSSFSheet pagamentoDoPiso = xssfWorkbook.getSheet("Apuração do Pagamento do Piso");

        XSSFRow rowExercicioEmExame = pagamentoDoPiso.getRow(1);
        XSSFCell alicota = rowExercicioEmExame.getCell(4);
        alicota.setCellValue(getValor(rclMunicipioDevedoresMap, "alicota"));

        XSSFRow rowRcl1 = pagamentoDoPiso.getRow(4);
        XSSFCell rclNov17 = rowRcl1.getCell(1);
        rclNov17.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl201711"));
        XSSFCell rclDez17 = rowRcl1.getCell(2);
        rclDez17.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl201712"));
        XSSFCell rclJan2018 = rowRcl1.getCell(3);
        rclJan2018.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20181"));
        XSSFCell rclDez18 = rowRcl1.getCell(4);
        rclDez18.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20182"));

        XSSFRow rowRcl2 = pagamentoDoPiso.getRow(11);
        XSSFCell rclMar18 = rowRcl2.getCell(1);
        rclMar18.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20183"));
        XSSFCell rclAbr18 = rowRcl2.getCell(2);
        rclAbr18.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20184"));
        XSSFCell rclMai2018 = rowRcl2.getCell(3);
        rclMai2018.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20185"));
        XSSFCell rclJun18 = rowRcl2.getCell(4);
        rclJun18.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20186"));

        XSSFRow rowRcl3 = pagamentoDoPiso.getRow(18);
        XSSFCell rclJun17 = rowRcl3.getCell(1);
        rclJun17.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20187"));
        XSSFCell rclJul18 = rowRcl3.getCell(2);
        rclJul18.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20188"));
        XSSFCell rclSet2018 = rowRcl3.getCell(3);
        rclSet2018.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl20189"));
        XSSFCell rclOut18 = rowRcl3.getCell(4);
        rclOut18.setCellValue(getValor(rclMunicipioDevedoresMap, "rcl201810"));

    }

    private void addQuadroGeralEnsino() {
        XSSFSheet ensino = xssfWorkbook.getSheet("Aplicação em Educação");

        XSSFRow impostosReceitasRow = ensino.getRow(1);
        XSSFCell impostosReceitas = impostosReceitasRow.getCell(7);
        impostosReceitas.setCellValue(getValor(quadroGeralEnsinoMap, "vRecImpEducArrec"));

        XSSFRow totalReceitasImpostosRow = ensino.getRow(3);
        XSSFCell totalReceitasImpostos = totalReceitasImpostosRow.getCell(7);
        totalReceitasImpostos.setCellValue(getValor(quadroGeralEnsinoMap, "vTotRecImpEdu"));

        XSSFRow retencoesRow = ensino.getRow(6);
        XSSFCell retencoes = retencoesRow.getCell(7);
        retencoes.setCellValue(getValor(quadroGeralEnsinoMap, "vFundebRetido"));

        XSSFRow transferenciasRecebidasRow = ensino.getRow(8);
        XSSFCell transferenciasRecebidas = transferenciasRecebidasRow.getCell(7);
        transferenciasRecebidas.setCellValue(getValor(quadroGeralEnsinoMap, "vFundebRecebido"));

        XSSFRow receitasAplicacoesFinanceirasRow = ensino.getRow(9);
        XSSFCell receitasAplicacoesFinanceiras = receitasAplicacoesFinanceirasRow.getCell(7);
        receitasAplicacoesFinanceiras.setCellValue(getValor(quadroGeralEnsinoMap, "vFundebRecApliFin"));

        XSSFRow despesasComMagisterioRow = ensino.getRow(14);
        XSSFCell despesasComMagisterio = despesasComMagisterioRow.getCell(7);
        despesasComMagisterio.setCellValue(getValor(quadroGeralEnsinoMap, "vDespEmpFundebMagist"));

        XSSFRow demaisDespesasRow = ensino.getRow(18);
        XSSFCell demaisDespesas = demaisDespesasRow.getCell(7);
        demaisDespesas.setCellValue(getValor(quadroGeralEnsinoMap, "vDespEmpFundebOutros"));

        XSSFRow educacaoBasicaExcetoFundebRow = ensino.getRow(26);
        XSSFCell educacaoBasicaExcetoFundeb = educacaoBasicaExcetoFundebRow.getCell(7);
        educacaoBasicaExcetoFundeb.setCellValue(getValor(quadroGeralEnsinoMap, "vDespEduBas"));

        XSSFRow deducaoGanhosAplicacoesFincanceirasRow = ensino.getRow(28);
        XSSFCell deducaoGanhosAplicacoesFincanceiras = deducaoGanhosAplicacoesFincanceirasRow.getCell(7);
        deducaoGanhosAplicacoesFincanceiras.setCellValue(getValor(quadroGeralEnsinoMap, "vGanApliFinEdu"));

        XSSFRow deducaoFundebRetidoNaoAplicadoRow = ensino.getRow(29);
        XSSFCell deducaoFundebRetidoNaoAplicado = deducaoFundebRetidoNaoAplicadoRow.getCell(7);
        deducaoFundebRetidoNaoAplicado.setCellValue(getValor(quadroGeralEnsinoMap, "vFundebRetidoNaoAplicadoEmp"));

        XSSFRow acrescimoFundebRow = ensino.getRow(32);
        XSSFCell acrescimoFundeb = acrescimoFundebRow.getCell(7);
        acrescimoFundeb.setCellValue(getValor(quadroGeralEnsinoMap, "vFundebApliTri1"));

        XSSFRow deducaoRestosRecursosPropriosRow = ensino.getRow(33);
        XSSFCell deducaoRestosRecursosProprios = deducaoRestosRecursosPropriosRow.getCell(7);
        deducaoRestosRecursosProprios.setCellValue(getValor(quadroGeralEnsinoMap, "vRPNPagoEdu"));

        XSSFRow receitaPrevistaRow = ensino.getRow(39);
        XSSFCell receitaPrevista = receitaPrevistaRow.getCell(7);
        receitaPrevista.setCellValue(getValor(quadroGeralEnsinoMap, "vRecImpEducPrevAtu"));

        XSSFRow despesaFixadaAtualizadaRow = ensino.getRow(40);
        XSSFCell despesaFixadaAtualizada = despesaFixadaAtualizadaRow.getCell(7);
        despesaFixadaAtualizada.setCellValue(getValor(quadroGeralEnsinoMap, "vDotAtuEnsino"));

    }

    private void addTabelaAplicaoEmSaude() {
        XSSFSheet aplicacaoEmSaude = xssfWorkbook.getSheet("Aplicação em Saúde");

        XSSFRow rowReceitaDeImpostos = aplicacaoEmSaude.getRow(2);
        XSSFCell receitaImpostos = rowReceitaDeImpostos.getCell(6);
        receitaImpostos.setCellValue(getValor(audespSaudeMap, "vTotRecImpSau"));

        XSSFRow rowTotalDespesasEmpenhadasRecursosProprios = aplicacaoEmSaude.getRow(6);
        XSSFCell totalDespesasEmpenhadasRecursosProprios = rowTotalDespesasEmpenhadasRecursosProprios.getCell(6);
        totalDespesasEmpenhadasRecursosProprios.setCellValue(getValor(audespSaudeMap, "vDespTotSau"));

        XSSFRow rowRestosAPagarLiquidadosNaoPagos = aplicacaoEmSaude.getRow(8);
        XSSFCell restosAPagarLiquidadosNaoPagos = rowRestosAPagarLiquidadosNaoPagos.getCell(6);
        restosAPagarLiquidadosNaoPagos.setCellValue(getValor(audespSaudeMap, "vRPNPagoSau"));

        XSSFRow rowReceitaprevistaAtualizada = aplicacaoEmSaude.getRow(14);
        XSSFCell receitaprevistaAtualizada = rowReceitaprevistaAtualizada.getCell(6);
        receitaprevistaAtualizada.setCellValue(getValor(audespSaudeMap, "vRecImpSaudePrevAtu"));

        XSSFRow rowDespesaFixadaAtualizada = aplicacaoEmSaude.getRow(15);
        XSSFCell despesaFixadaAtualizada = rowDespesaFixadaAtualizada.getCell(6);
        despesaFixadaAtualizada.setCellValue(getValor(audespSaudeMap, "vDotAtuSaude"));


    }

    private void addTabelaDividaAtiva() {
        XSSFSheet dividaAtiva = xssfWorkbook.getSheet("Dívida Ativa");

        XSSFRow rowSaldoinciialDividaAtiva = dividaAtiva.getRow(1);
        XSSFCell saldoincialDividaAtivaAnt = rowSaldoinciialDividaAtiva.getCell(5);
        XSSFCell saldoincialDividaAtiva = rowSaldoinciialDividaAtiva.getCell(6);
        saldoincialDividaAtivaAnt.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaTotIni" + (exercicio-1)));
        saldoincialDividaAtiva.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaTotIni" + exercicio));

        XSSFRow rowSaldoinicialProvisaoDePerdas = dividaAtiva.getRow(6);
        XSSFCell saldoinicialProvisaoDePerdasAnt = rowSaldoinicialProvisaoDePerdas.getCell(5);
        XSSFCell saldoinicialProvisaoDePerdas = rowSaldoinicialProvisaoDePerdas.getCell(6);
        saldoinicialProvisaoDePerdasAnt.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaProvPerIni" + (exercicio-1)));
        saldoinicialProvisaoDePerdas.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaProvPerIni" + exercicio));

        XSSFRow rowRecebimentos = dividaAtiva.getRow(14);
        XSSFCell recebimentosAnt = rowRecebimentos.getCell(5);
        XSSFCell recebimentos = rowRecebimentos.getCell(6);
        recebimentosAnt.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaRec" + (exercicio-1)));
        recebimentos.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaRec" + exercicio));

        XSSFRow rowCancelamentos = dividaAtiva.getRow(19);
        XSSFCell cancelamentosAnt = rowCancelamentos.getCell(5);
        XSSFCell cancelamentos = rowCancelamentos.getCell(6);
        cancelamentosAnt.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaCanc" + (exercicio-1)));
        cancelamentos.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaCanc" + exercicio ));

        XSSFRow rowInscricao = dividaAtiva.getRow(27);
        XSSFCell inscricaoAnt = rowInscricao.getCell(5);
        XSSFCell inscricao = rowInscricao.getCell(6);
        inscricaoAnt.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaInsc" + (exercicio-1)));
        inscricao.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaInsc" + exercicio ));

        XSSFRow rowJurosAtualizacaoDaDivida = dividaAtiva.getRow(32);
        XSSFCell jurosAtualizacaoDaDividaAnt = rowJurosAtualizacaoDaDivida.getCell(5);
        XSSFCell jurosAtualizacaoDaDivida = rowJurosAtualizacaoDaDivida.getCell(6);
        jurosAtualizacaoDaDividaAnt.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaAtu" + (exercicio-1)));
        jurosAtualizacaoDaDivida.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaAtu" + exercicio ));

        XSSFRow rowSaldoFinalProvisaoParaPerdas = dividaAtiva.getRow(37);
        XSSFCell saldoFinalProvisaoParaPerdasAnt = rowSaldoFinalProvisaoParaPerdas.getCell(5);
        XSSFCell saldoFinalProvisaoParaPerdas = rowSaldoFinalProvisaoParaPerdas.getCell(6);
        saldoFinalProvisaoParaPerdasAnt.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaProvPerFin" + (exercicio-1)));
        saldoFinalProvisaoParaPerdas.setCellValue(getValor(audespDividaAtivaMap, "vDivAtivaProvPerFin" + exercicio ));


    }



    private Double getValor(Map mapa, String chave) {
        if(mapa != null && chave != null && mapa.get(chave) != null) {
            Object o = mapa.get(chave);
            if(o instanceof BigDecimal) {
                return ((BigDecimal)o).doubleValue();
            }
        }
        return 0.0;
    }

    private ResponseEntity<Resource> sendFile() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        xssfWorkbook.write(byteArrayOutputStream);
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=pre-relatorio-pm-v1.6.xlsx");

        return ResponseEntity.ok()
                .headers(headers)
                //.contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }
}
