package br.gov.sp.tce.relatoriosfiscalizacao.db.tcespbi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "alicotas_entidades_devedoras")
public class AlicotaEntidadeDevedora {

    @Id
    @Column(name = "tk_alicota")
    private Integer id;

    private String entidade;

    private String regime;

    private BigDecimal alicota;

    @Column(name = "tipo_alicota")
    private String tipoAlicota;

    private Integer ano;

    private Integer mes;

    @Column(name = "codigo_ibge")
    private Integer codigoIbge;

    @Column(name = "tipo_entidade")
    private Integer tipoEntidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEntidade() {
        return entidade;
    }

    public void setEntidade(String entidade) {
        this.entidade = entidade;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }

    public BigDecimal getAlicota() {
        return alicota;
    }

    public void setAlicota(BigDecimal alicota) {
        this.alicota = alicota;
    }

    public String getTipoAlicota() {
        return tipoAlicota;
    }

    public void setTipoAlicota(String tipoAlicota) {
        this.tipoAlicota = tipoAlicota;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getCodigoIbge() {
        return codigoIbge;
    }

    public void setCodigoIbge(Integer codigoIbge) {
        this.codigoIbge = codigoIbge;
    }

    public Integer getTipoEntidade() {
        return tipoEntidade;
    }

    public void setTipoEntidade(Integer tipoEntidade) {
        this.tipoEntidade = tipoEntidade;
    }



}
