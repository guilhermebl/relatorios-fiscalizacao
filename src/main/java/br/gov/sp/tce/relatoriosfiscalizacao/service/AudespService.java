package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespResponsavel;
import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.repository.AudespRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AudespService {


    @Autowired
    private AudespRepository audespRepository;

    public AudespResponsavel getAudespResponsavel(Integer codigoIBGE) {
        return audespRepository.getAudespResponsavel(codigoIBGE);
    }

    public List<AudespResponsavel> getResponsavelPrefeitura(Integer codigoIBGE, Integer exercicio, Integer quadrimestre) {
        List<AudespResponsavel> responsavelList = audespRepository.getResponsavelPrefeitura(codigoIBGE, exercicio, quadrimestre);
        return responsavelList;
    }

    public Map<String, List<AudespResponsavel>> getResponsavelSubstitutoPrefeitura(Integer codigoIBGE, Integer exercicio, Integer quadrimestre) {
        List<AudespResponsavel> responsavelList = audespRepository.getResponsavelSubstitutoPrefeitura(codigoIBGE, exercicio, quadrimestre);
        Map<String, List<AudespResponsavel>> mapa = new HashMap<>();


//        LocalDateTime dataInicio = getDataInicio(exercicio, mesReferencia);
//        LocalDateTime dataFinal = getDataFim(exercicio, mesReferencia);

        for(AudespResponsavel responsavel : responsavelList) {
            mapa.put(responsavel.getNome(), new ArrayList<>());
        }

//        for(AudespResponsavel responsavel : responsavelList) {
//            if(responsavel.getDataInicioVigencia() != null && responsavel.getDataInicioVigencia().isBefore(dataInicio)
//                    && responsavel.getDataFimVigencia() != null && responsavel.getDataFimVigencia().isAfter(dataInicio))
//                responsavel.setDataInicioVigencia(dataInicio);
//            if(responsavel.getDataInicioVigencia() != null && responsavel.getDataFimVigencia() != null
//                    && responsavel.getDataFimVigencia().isAfter(dataFinal))
//                responsavel.setDataFimVigencia(dataFinal);
//
//            mapa.get(responsavel.getNome()).add(responsavel);
//        }

        return mapa;
    }



    private LocalDateTime getDataInicio(Integer exercicio, Integer mesReferencia) {
        if(mesReferencia == 4){
            return LocalDateTime.of(exercicio, 1, 1, 0, 0, 0);
        } else if(mesReferencia == 8) {
            return LocalDateTime.of(exercicio, 5, 1, 0, 0, 0);
        } else {
            return LocalDateTime.of(exercicio, 1, 1, 0, 0, 0);
        }
    }

    private LocalDateTime getDataFim(Integer exercicio, Integer mesReferencia) {
        if(mesReferencia == 4){
            return LocalDateTime.of(exercicio, 4, 30, 23, 59, 59);
        } else if(mesReferencia == 8) {
            return LocalDateTime.of(exercicio, 8, 31, 23, 59, 59);
        } else {
            return LocalDateTime.of(exercicio, 12, 31, 23, 59, 59);
        }
    }

}
