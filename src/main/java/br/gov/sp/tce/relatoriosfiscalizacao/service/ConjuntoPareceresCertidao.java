package br.gov.sp.tce.relatoriosfiscalizacao.service;

import br.gov.sp.tce.relatoriosfiscalizacao.db.tabelas.model.TabelasParecer;

import java.util.List;

public class ConjuntoPareceresCertidao {

    private TabelasParecer ultimoExercicio;

    private List<TabelasParecer> exerciciosNaoAnalisados;

    private TabelasParecer exercicioEmCurso;

    public TabelasParecer getUltimoExercicio() {
        return ultimoExercicio;
    }

    public void setUltimoExercicio(TabelasParecer ultimoExercicio) {
        this.ultimoExercicio = ultimoExercicio;
    }

    public List<TabelasParecer> getExerciciosNaoAnalisados() {
        return exerciciosNaoAnalisados;
    }

    public void setExerciciosNaoAnalisados(List<TabelasParecer> exerciciosNaoAnalisados) {
        this.exerciciosNaoAnalisados = exerciciosNaoAnalisados;
    }

    public TabelasParecer getExercicioEmCurso() {
        return exercicioEmCurso;
    }

    public void setExercicioEmCurso(TabelasParecer exercicioEmCurso) {
        this.exercicioEmCurso = exercicioEmCurso;
    }
}
