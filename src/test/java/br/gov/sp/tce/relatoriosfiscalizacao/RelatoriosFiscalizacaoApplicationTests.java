package br.gov.sp.tce.relatoriosfiscalizacao;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RelatoriosFiscalizacaoApplicationTests {

	@Test
	public void contextLoads() {

	}

}

