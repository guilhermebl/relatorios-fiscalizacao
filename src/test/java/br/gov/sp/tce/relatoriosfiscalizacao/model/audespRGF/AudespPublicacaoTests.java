package br.gov.sp.tce.relatoriosfiscalizacao.model.audespRGF;

import br.gov.sp.tce.relatoriosfiscalizacao.db.audesp.model.AudespPublicacao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@SpringBootTest
public class AudespPublicacaoTests {

	private LocalDate dataNoPrazoNovembro;
	private LocalDate dataLimiteNovembro;
	private LocalDate dataLimiteNovembro30Dias;
	private LocalDate dataComAtrasoNovembro;
	private LocalDate dataComMuitoAtrasoNovembro;
	private AudespPublicacao publicacao;

	@BeforeEach
	public void iniciaObjetos() {
		this.dataNoPrazoNovembro = LocalDate.of(2017, 11, 1 );
		this.dataLimiteNovembro = LocalDate.of(2018, 1, 31);
		this.dataLimiteNovembro30Dias = LocalDate.of(2018, 1, 30);
		this.dataComAtrasoNovembro = LocalDate.of(2018, 2, 1 );
		this.dataComMuitoAtrasoNovembro = LocalDate.of(2018, 5, 1);

		this.publicacao =  new AudespPublicacao();
		this.publicacao.setExercicio(2017);
		this.publicacao.setMes(11);
	}

	@Test
	public void dadoPublicacao__retornaSeNoPrazo() {
		publicacao.setDataPublicacao(dataNoPrazoNovembro);
//		publicacao = ReflectionTestUtils.invokeMethod(AudespRGFService.class, "validaDentroDoPrazo", publicacao);
		assertEquals("Publicação fora no prazo", true, publicacao.validaDentroDoPrazo());

		publicacao.setDataPublicacao(dataLimiteNovembro);
		assertEquals("Publicação subsequente a data Limite", false, publicacao.validaDentroDoPrazo());

		publicacao.setDataPublicacao(dataComAtrasoNovembro);
		assertEquals("Publicação com atraso", false, publicacao.validaDentroDoPrazo());

		publicacao.setDataPublicacao(dataComMuitoAtrasoNovembro);
		assertEquals("Publicação com muito atraso", false, publicacao.validaDentroDoPrazo());

		publicacao.setDataPublicacao(null);
		assertEquals("Data Publicação Nula", false, publicacao.validaDentroDoPrazo());
	}

	@Test
	public void dadoPublicacao__retornaSeNoPrazo30Dias() {
		publicacao.setDataPublicacao(dataNoPrazoNovembro);
//		publicacao = ReflectionTestUtils.invokeMethod(AudespRGFService.class, "validaDentroDoPrazo", publicacao);
		assertEquals("Publicação fora no prazo", true, publicacao.validaDentroDoPrazo30dias());

		publicacao.setDataPublicacao(dataLimiteNovembro);
		assertEquals("Publicação na data Limite Mês", false, publicacao.validaDentroDoPrazo30dias());

		publicacao.setDataPublicacao(dataLimiteNovembro30Dias);
		assertEquals("Publicação na data Limite 30 Dias", true, publicacao.validaDentroDoPrazo30dias());

		publicacao.setDataPublicacao(dataComAtrasoNovembro);
		assertEquals("Publicação com atraso", false, publicacao.validaDentroDoPrazo30dias());

		publicacao.setDataPublicacao(dataComMuitoAtrasoNovembro);
		assertEquals("Publicação com muito atraso", false, publicacao.validaDentroDoPrazo30dias());

		publicacao.setDataPublicacao(null);
		assertEquals("Data Publicação Nula", false, publicacao.validaDentroDoPrazo());
	}

	@Test
	public void getLastDayOfMonth() {
		int mes = 2;
		int ano = 2020;

		LocalDate dataReferencia =  LocalDate.of(ano, mes, 1);
		dataReferencia = dataReferencia.with(TemporalAdjusters.lastDayOfMonth());
		assertEquals("fevereiro de ano bissexto", 29, dataReferencia.getDayOfMonth());

		ano = 2019;

		dataReferencia =  LocalDate.of(ano, mes, 1);
		dataReferencia = dataReferencia.with(TemporalAdjusters.lastDayOfMonth());

		assertEquals("fevereiro de ano não bissexto", 28, dataReferencia.getDayOfMonth());
	}

	@Test
	public void dadaData__retornaPrazoLimite() {
		int mes = 2;
		int ano = 2020;

		LocalDate dataReferencia =  LocalDate.of(ano, mes, 1);
		dataReferencia = dataReferencia.with(TemporalAdjusters.lastDayOfMonth());
		LocalDate dataLimite  = dataReferencia.plusDays(30);

		assertEquals("DataLimite de mais 30 dias", 30, dataLimite.getDayOfMonth());


		mes = 1;
		dataReferencia =  LocalDate.of(ano, mes, 1);
		dataReferencia = dataReferencia.with(TemporalAdjusters.lastDayOfMonth());
		dataLimite  = dataReferencia.plusDays(30);

		assertEquals("DataLimite de mais 30 dias - fevereiro Bissexto - dia", 1, dataLimite.getDayOfMonth());
		assertEquals("DataLimite de mais 30 dias - fevereiro Bissexto - mês", 3, dataLimite.getMonthValue());

	}

	@Test
	public void dadaDatas__retornaSeDentroDoPrazo() {
		int mes = 1;
		int ano = 2020;

		LocalDate dataReferencia =  LocalDate.of(ano, mes, 1);
		dataReferencia = dataReferencia.with(TemporalAdjusters.lastDayOfMonth());
		LocalDate dataLimite  = dataReferencia.plusDays(30);

		LocalDate dataPublicacaoDentro = LocalDate.of(2020, 2, 29);
		LocalDate dataPublicacaoExato = LocalDate.of(2020, 3, 1);
		LocalDate dataPublicacaoFora = LocalDate.of(2020, 3, 3);

		assertTrue("DataLimite de mais 30 dias - Dentro do Prazo", dataPublicacaoDentro.isBefore(dataLimite));
		assertTrue("DataLimite de mais 30 dias - Exatamente no Prazo", dataPublicacaoExato.isEqual(dataLimite));
		assertTrue("DataLimite de mais 30 dias - Fora do Prazo", dataPublicacaoFora.isAfter(dataLimite));
	}
}