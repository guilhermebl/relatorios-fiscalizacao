package br.gov.sp.tce.relatoriosfiscalizacao.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class FormatadorDeDadosTests {

//	- modelo numerico

	private BigDecimal valorDecimal;
	private BigDecimal valorDecimalGrande;
	private BigDecimal valorDecimalZero;
	private BigDecimal valorDecimalNegativo;


	@BeforeEach
	public void iniciaObjetos(){
		this.valorDecimal = new BigDecimal(0.0947);
		this.valorDecimalGrande = new BigDecimal(13001043.80);
		this.valorDecimalZero = new BigDecimal(0);
		this.valorDecimalNegativo = new BigDecimal(-0.9491);
	}


	@Test
	public void dadoValorDecimal__valorMonetarioComSimbolo() {
		String resultadoMonetario = FormatadorDeDados.formatarMoeda(valorDecimal, true, false);
		String resultadoMonetarioZero = FormatadorDeDados.formatarMoeda(valorDecimalZero, true, false);
		String resultadoMonetarioNegativo = FormatadorDeDados.formatarMoeda(valorDecimalNegativo, true, false);
		String resultadoMonetarioGrande = FormatadorDeDados.formatarMoeda(valorDecimalGrande, true, false);
		assertEquals("R$ 0,09", resultadoMonetario);
		assertEquals("R$ 0,00", resultadoMonetarioZero);
		assertEquals("R$ -0,94", resultadoMonetarioNegativo);
		assertEquals("R$ 13.001.043,80", resultadoMonetarioGrande);
	}

	@Test
	public void dadoValorDecimal__valorMonetarioSemSimbolo() {
		String resultadoMonetario = FormatadorDeDados.formatarMoeda(valorDecimal, false, false);
		String resultadoMonetarioZero = FormatadorDeDados.formatarMoeda(valorDecimalZero, false, false);
		String resultadoMonetarioNegativo = FormatadorDeDados.formatarMoeda(valorDecimalNegativo, false, false);
		String resultadoMonetarioGrande = FormatadorDeDados.formatarMoeda(valorDecimalGrande, false, false);
		assertEquals("0,09", resultadoMonetario);
		assertEquals("0,00", resultadoMonetarioZero);
		assertEquals("-0,94", resultadoMonetarioNegativo);
		assertEquals("13.001.043,80", resultadoMonetarioGrande);
	}

	@Test
	public void dadoValorDecimal__retornaPercentualSemSimbolo() {
		String resultadoDecimal = FormatadorDeDados.formatarPercentual (valorDecimal, false, false);
		String resultadoDecimalZero = FormatadorDeDados.formatarPercentual (valorDecimalZero, false, false);
		String resultadoDecimalNegativo = FormatadorDeDados.formatarPercentual (valorDecimalNegativo, false, false);
		String resultadoNull = FormatadorDeDados.formatarPercentual (null, false, false);
		assertEquals("9,4700", resultadoDecimal);
		assertEquals("0,0000", resultadoDecimalZero);
		assertEquals("-94,9100", resultadoDecimalNegativo);
		assertEquals("", resultadoNull);
	}


	@Test
	public void dadoValorDecimal__retornaPercentualComSimbolo() {
		String resultadoDecimal = FormatadorDeDados.formatarPercentual (valorDecimal, true, false);
		String resultadoDecimalZero = FormatadorDeDados.formatarPercentual (valorDecimalZero, true, false);
		String resultadoDecimalNegativo = FormatadorDeDados.formatarPercentual (valorDecimalNegativo, true, false);
		String resultadoNull = FormatadorDeDados.formatarPercentual (null, true, false);
		assertEquals("9,4700%", resultadoDecimal);
		assertEquals("0,0000%", resultadoDecimalZero);
		assertEquals("-94,9100%", resultadoDecimalNegativo);
		assertEquals("", resultadoNull);
	}

	@Test
	public void dadoValorNull__hifenIndicandoVazio() {
		String resultadoNull = FormatadorDeDados.formatarPercentual (null, false, true);
		assertEquals("-", resultadoNull);
	}

	@Test
	public void dadoValorNull__StringVazia() {
		String resultadoNull = FormatadorDeDados.formatarPercentual (null, false, false);
		assertEquals("", resultadoNull);
	}



}

