package br.gov.sp.tce.relatoriosfiscalizacao.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RelatoriosFiscalizacaoAudespServiceTests {

	@Autowired
	private AudespEnsinoService audespEnsinoService;

	private Integer codigoIbge = 3539806;
	private Integer exercicio = 2018;
	private Integer mes = 8;

	@Test
	public void dadoMunicipioExercicioMes__retornaDespesaFundeb() {
		Map<String, String> audespEnsinoFundeb = audespEnsinoService.getAudespEnsinoFundeb(codigoIbge, exercicio, mes);
		System.out.println(audespEnsinoFundeb.get("vDespLiqAplicFundeb"));
		assertEquals("94,60", audespEnsinoFundeb.get("vDespLiqAplicFundeb"), "Despesa Liquida Aplicação Fundeb");
	}

}

