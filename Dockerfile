#FROM store/oracle/serverjre:8
FROM openjdk:11
ADD build/libs/relatoriosfiscalizacao-0.0.1-SNAPSHOT.jar relatoriosfiscalizacao-0.0.1-SNAPSHOT.jar
#ADD cacerts /etc/ssl/certs/java/cacerts
#ADD cacerts /usr/java/default/jre/lib/security
ENV HTTPS_PROXY http://do-user:1234@proxyk.tce.sp.gov.br:8080
ENV HTTP_PROXY http://do-user:1234@proxyk.tce.sp.gov.br:8080
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "relatoriosfiscalizacao-0.0.1-SNAPSHOT.jar"]