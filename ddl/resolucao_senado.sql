
DROP TABLE IF EXISTS paineis.resolucao_43_21_senado;
CREATE TABLE paineis.resolucao_43_21_senado 
(
   id serial PRIMARY KEY,
   codigo_ibge integer,
   nome_orgao varchar(100),
   codigo_tipo_orgao integer,
   resultado_julgamento varchar(512),
   tc varchar(20),
   tc_numero bigint,
   exercicio integer,
   transitou_efetivamente varchar(1),
   data_transito_em_julgado timestamp,
   data_publicacao_do_transito_em_julgado timestamp
)
;
alter table paineis.resolucao_43_21_senado owner TO usr_tcesp_bi;
grant select on paineis.resolucao_43_21_senado to usr_tcesp_bi_leitura; 